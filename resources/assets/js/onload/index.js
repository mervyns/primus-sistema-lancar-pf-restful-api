export default function($) {   
    var finalStepLoop;

    $('.input-money-format').mask('#.##0', {reverse: true});
    $('.input-npwp-format').mask('##.###.###.#-###.###');

    const updateTable = function(finalStepLoop) { 
        var id = $( "input[type='hidden'][name='id']" ).val();
        $.ajax({
            url : '/api/final-steps/refresh/'+id,
            success : function (result){
                // console.log(result)
                $( "#final-asset-penghasilan tbody" ).empty();
                $( "#final-asset-penghasilan tbody" ).append(result.penghasilan.html);
                $( "#final-asset-hutang tbody" ).empty();
                $( "#final-asset-hutang tbody" ).append(result.hutang.html);
                $( "#final-asset-harta tbody" ).empty();
                $( "#final-asset-harta tbody" ).append(result.harta.html);
                $('button.btnEditPenghasilanUtama').click(function(e){
                    var income_step = e.currentTarget.value;
                    var split = income_step.split('|');
                    window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                });
                $('button.btnEditHutang').click(function(e){
                    var hutang_step = e.currentTarget.value;
                    var split = hutang_step.split('|');
                    window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                });
                $('button.btnEditHarta').click(function(e){
                    var harta_step = e.currentTarget.value;
                    var split = harta_step.split('|');
                    window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                });
                // console.clear();  
            }                      // pass existing options
        });
        // .then(function() {     
        //     // console.log('masuk loop')      // on completion, restart
        //     finalStepLoop = setTimeout(updateTable, 5000);  // function refers to itself
        //     // console.clear();  
        // });
    };

    $(document).ready( function () {

        var host   = 'ws://127.0.0.1:8889';
        var socket = null;
        try {
            socket = new WebSocket(host);
            socket.onopen = function () {
                console.log('connection is opened');
                return;
            };
            socket.onmessage = function (msg) {
               
                var id = $( "input[type='hidden'][name='id']" ).val();
                if (parseInt(msg.data) == parseInt(id)){
                  
                    $.ajax({
                        url : '/api/final-steps/refresh/'+id,
                        success : function (result){
                            // console.log(result)
                            $( "#final-asset-penghasilan tbody" ).empty();
                            $( "#final-asset-penghasilan tbody" ).append(result.penghasilan.html);
                            $( "#final-asset-hutang tbody" ).empty();
                            $( "#final-asset-hutang tbody" ).append(result.hutang.html);
                            $( "#final-asset-harta tbody" ).empty();
                            $( "#final-asset-harta tbody" ).append(result.harta.html);
                            $('button.btnEditPenghasilanUtama').click(function(e){
                                var income_step = e.currentTarget.value;
                                var split = income_step.split('|');
                                window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                            });
                            $('button.btnEditHutang').click(function(e){
                                var hutang_step = e.currentTarget.value;
                                var split = hutang_step.split('|');
                                window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                            });
                            $('button.btnEditHarta').click(function(e){
                                var harta_step = e.currentTarget.value;
                                var split = harta_step.split('|');
                                window.open("/tax-filling-final-steps/edit/"+split[1]+"/"+split[0])
                            });
                            // console.clear();  
                        }                      // pass existing options
                    })
                }

                return;
            };
            socket.onclose = function () {
                console.log('connection is closed');
                return;
            };
        } catch (e) {
            console.log(e);
        }

        if ($('#close-signal').length > 0){
            window.close();
        }

        if ($('#empty-signal').length > 0){
            var message = $('#empty-signal').val();
            swal({
                title: "Ups!",
                text: message,
                type: "warning"
            }).then(() => {
                window.close();
            });
        }

        var path = (window.location.pathname);
        var segment = path.split('/');

        if(parseInt(segment[2]) === 93){
            updateTable();
        }else{
            clearTimeout(finalStepLoop);
        }

        $('#table_assets_repatriation-container').hide();

        $( "#table_assets-container" ).on( "change", "input", function( event ) {
            event.preventDefault();
            
            var trs = document.querySelectorAll('#table_assets_repatriation > tbody tr');
           
            var index = 0;
            var loop = trs.forEach(function(tr,i) {

                var is_checked = tr.querySelector('input[type="checkbox"]').checked ;
                var asset_id = parseInt((tr.querySelectorAll('td')[7].innerText).replace(/[^0-9\.]+/g, ''))
                var kode_harta = tr.querySelectorAll('td')[1].innerText.replace(/[^0-9\.]+/g, '')
                var nilai = parseInt((tr.querySelectorAll('td')[5].innerText).replace(/[^0-9\.]+/g, ''))
               
                if(is_checked){
                    $('#table_assets_repatriation-container').show();
                   
                    if($("#tab"+asset_id).length == 0) {
                        var api_url = "/api/user-assets/repatriation/"+asset_id;
                        $.ajax({
                            url: api_url,
                        })
                            .done(function( value ) {
                                var table_id = 'table_repatriation_'+asset_id;
                                $("#tabs").append('<li id="li'+asset_id+'"><a href="#tab'+asset_id+'">#'+kode_harta+'</a></li>');
                                $( "#tab-content-repatriation" ).append(value.html);
                                $('#tab-content-repatriation .tab:first-child').addClass('active');
                                $('#table_assets_repatriation-container li:first-child').addClass('active');
                                
                                $('.input-money-format').mask('#.##0', {reverse: true});
                                $('.select2kodegateway_'+asset_id).select2();
                                $('.select2investments_'+asset_id).select2({
                                    width:'resolve'
                                });
                                $('.select2currencies_'+asset_id).select2();

                                var $regexname=/\d{4}\-(0[1-9]|[1][0-2])\-(0[1-9]|[12]\d|3[01])/;

                                $('#datepicker_'+asset_id).datetimepicker({
                                    format: 'DD/MM/YYYY'
                                });

                                // $("#table_repatriation > tbody").html(value.html);
                            });
                    }
                }else{
                    
                    $('.tab-links li#li'+asset_id).remove()
                    $( "#tab"+asset_id).remove();

                }
            });


        });

        $('#tab-content-repatriation').bind("DOMSubtreeModified", function(){
            if(document.querySelectorAll('#tab-content-repatriation .tab').length == 0){
                $('#table_assets_repatriation-container').hide();
            }
        });

        ////////////Button CheckBox////////////
        $('.button-checkbox').each(function () {
    
            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };
    
            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });
    
            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
    
                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");
    
                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);
    
                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }
    
            // Initialization
            function init() {
    
                updateDisplay();
    
                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
      
        ////////////Legacy input listener/////
        var $regexvalue=/^[0-9]*$/;
        $('.tab #prize-lottery-brutto').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#legacy-value').on('focusout',function(){
            // $('#legacy-value').addClass('input-money-format')
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        ////////////End of Legacy input listener/////



        ///////////Pengalihan Hak atas Tanah Bangunan/////////
        $('.tab #transfer-bulding-rights-value').on('focusout', function(e){
         
            var trans_id = e.currentTarget.attributes[1].value
            var netto = document.querySelectorAll('input[name="value['+trans_id+']"]')[0].value
            var tax = netto * 0.025

            if(!isNaN(tax)){
                var received = document.querySelectorAll('input[name="received['+trans_id+']"]')[0].value = Math.floor(netto - tax);
                document.querySelectorAll('input[name="final['+trans_id+']"]')[0].value = Math.floor(netto - received);
            }
            // $('#tab1 #transfer-bulding-rights-received').mask('#.##0', {reverse: true});
            // $('#tab1 #transfer-bulding-rights-final').mask('#.##0', {reverse: true});
        });

        var $regexvalue=/^[0-9]*$/;
        $('.tab #transfer-bulding-rights-value').on('keypress keydown keyup',function(){
           
            var trans_id = e.currentTarget.attributes[1].value

            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('input[name="value['+trans_id+']"]').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
             else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('input[name="value['+trans_id+']"]').css({ 'background-color': "#ffffff" });
            }
        });

        $('.tab #transfer-bulding-rights-value').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('.tab #transfer-bulding-rights-value').on('focusout',function(){
            // $('.tab #transfer-bulding-rights-value').addClass('input-money-format')
            $('.input-money-format').mask('#.##0', {reverse: true});
        });



        ///////////Pesangon/Tunjangan Hari Tua///////////
        var pre_select = document.querySelectorAll('input[name="option_id"]');

        pre_select.forEach(function (element, index){
            var tab_index = index+1
            if([16, 17, 18, 19].includes(parseInt(element.value))){
                document.querySelectorAll('#tab'+tab_index+' select#severance-pay-type')[0].value = 1
            }else if([20, 21, 22, 23, 24].includes(parseInt(element.value))){
                var age =  document.querySelectorAll('#tab'+tab_index+' input[name="age"]')[0].value

                if(parseInt(age) >= 56){
                    document.querySelectorAll('#tab'+tab_index+' select#severance-pay-type')[0].value = 3
                }else{
                    document.querySelectorAll('#tab'+tab_index+' select#severance-pay-type')[0].value = 2
                }

            }
        })

        $('.tab #severance-pay-brutto').on('focusout', function(e){
            var brutto_id = e.currentTarget.attributes[1].value

            var brutto = document.querySelectorAll('#tab'+brutto_id+' #severance-pay-brutto')[0].value
            var select = document.querySelectorAll('#tab'+brutto_id+' option:checked')[0].value
           
            // $('#tab1 #severance-pay-brutto').addClass('input-money-format')
            // $('#tab1 #severance-pay-netto').addClass('input-money-format')
            // $('#tab1 #severance-pay-final').addClass('input-money-format')

            if(brutto.length !== 0 && parseInt(select) !== 0){
                var tax_rate = document.querySelectorAll('#tab'+brutto_id+' #tarif')[0].value
                var tax = brutto * (parseInt(tax_rate)/100)
                if(!isNaN(tax)){
                    var received = document.querySelectorAll('#tab'+brutto_id+' #severance-pay-netto')[0].value = Math.floor(brutto - tax)
                    document.querySelectorAll('#tab'+brutto_id+' #severance-pay-final')[0].value = Math.floor(brutto - received)
                }
            }

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #severance-pay-brutto').on('keypress',function(e){
          
            // var netto = parseInt($(this).val())
            var netto = e.currentTarget.value
            var brutto_id = e.currentTarget.attributes[1].value

            var type = document.querySelectorAll('#tab'+brutto_id+' #severance-pay-type option:checked')[0].value
           
            if(parseInt(type) == 1){
                if(netto < 50000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '0' 
                }else if(netto >= 50000000 && netto < 100000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '5' 
                }else if(netto >= 100000000 && netto < 500000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '15' 
                }else if(netto >= 500000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '25' 
                }else{
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '0' 
                }
            }else if(parseInt(type) == 3){
                if(netto < 50000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '5' 
                }else if(netto >= 50000000 && netto < 250000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '15' 
                }else if(netto >= 250000000 && netto < 500000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '25' 
                }else if(netto >= 500000000){
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '30' 
                }else{
                    document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '5' 
                }    
            }else if(parseInt(type) == 2){
                document.querySelectorAll('#tab'+brutto_id+' #tarif').value = '5' 
            }

        });

        $('.tab #severance-pay-type').on('change', function(e){
            document.querySelectorAll('#severance-pay-brutto')[0].value = ''
            document.querySelectorAll('#severance-pay-netto')[0].value = ''
            document.querySelectorAll('#severance-pay-final')[0].value = ''
        });

        $('.tab #severance-pay-brutto').on('focus',function(){
            $('.input-money-format').unmask();
        });

        //////////////////Awards and Gift///////////////
        $('#awards-and-gift-form input.custom-control-input').on('change', function(e){
        
            var radio_id = e.currentTarget.attributes[0].value
            if(document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input.custom-control-input')[0].checked){
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[0].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[1].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[2].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[3].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[4].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[5].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[6].setAttribute('readonly',true)
            }else{
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[0].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[1].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[2].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[3].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[4].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[5].removeAttribute('readonly')
                // document.querySelectorAll('#awards-and-gift-form #tab1 input[type="text"]')[6].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+radio_id+' input[type="text"]')[8].value = ''
            }
        });

        $('.tab #awards-and-gift-brutto , .tab #awards-and-gift-netto').on('focusout',function(e){
            $('.input-money-format').unmask();
            
            var this_id = e.currentTarget.attributes[0].value

            var type = parseInt(document.querySelector('#tab'+this_id+' input[name="type['+this_id+']"]:checked').value)

            if(type == 2){
                var netto = parseInt(document.querySelectorAll('#tab'+this_id+' #awards-and-gift-netto')[0].value)
                var tax = 5
                if(netto < 50000000){
                    tax = 5
                }else if(netto >= 50000000 && netto < 250000000){
                    tax = 15
                }else if(netto >= 250000000 && netto < 500000000){
                    tax = 25
                }else if(netto >= 500000000){
                    tax = 30
                }    

                document.querySelectorAll('#tab'+this_id+' #awards-and-gift-tarif')[0].value = tax+'%'
                
                if(!isNaN(netto) ){
                    var brutto = document.querySelectorAll('#tab'+this_id+' #awards-and-gift-brutto')[0].value = Math.floor(netto / (1-(tax/100))) 
                    document.querySelectorAll('#tab'+this_id+' #awards-and-gift-taken-pph')[0].value = Math.floor(brutto - netto) 
                } 
            }

            // $('#tab1 #awards-and-gift-brutto').addClass('input-money-format')
            // $('#tab1 #awards-and-gift-taken-pph').addClass('input-money-format')
            // $('#tab1 #awards-and-gift-netto').addClass('input-money-format')
            // $('#tab1 #awards-and-gift-cutter-npwp').addClass('input-npwp-format')

            $('.input-money-format').mask('#.##0', {reverse: true});
            $('.input-npwp-format').mask('##.###.###.#-###.###');
        });

        $('.tab #awards-and-gift-brutto, .tab #awards-and-gift-netto').on('focus',function(){
            $('.input-money-format').unmask();
            $('.input-npwp-format').unmask();
        });

        $('#awards-and-gift-date-pph').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        //////////////////End of Award and Gift///////////////
        
        //////////////////Non Building Transfer/////////
        $('.tab #non-building-transfer-profit-get, .tab #non-building-transfer-profit-trans').on('focusout', function(e){
         
            
            var get_id = e.currentTarget.attributes[1].value
            var trans = parseInt(document.querySelectorAll('input[name="trans_value['+get_id+']"]')[0].value)
            var get = parseInt(document.querySelectorAll('input[name="get_value['+get_id+']"]')[0].value)
           
            // $('#non-building-transfer-profit-get').addClass('input-money-format')
            // $('#non-building-transfer-profit-trans').addClass('input-money-format')
            // $('#non-building-transfer-profit-income').addClass('input-money-format')

            if(!isNaN((trans)) && !isNaN((get))){
                if(get > trans){
                    // $('.btn.btn-info.btn-lg').attr("disabled", true);
                    document.querySelectorAll('input[name="income_value['+get_id+']"]')[0].value = 0
                }else{
                    $('.btn.btn-info.btn-lg').removeAttr('disabled');
                    $('input[name="get_value['+get_id+']"]').css({ 'background-color': "#ffffff" });
                    document.querySelectorAll('input[name="income_value['+get_id+']"]')[0].value = trans - get
                }
            }
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #non-building-transfer-profit-get , .tab #non-building-transfer-profit-trans').on('focus',function(){
            $('.input-money-format').unmask();
        });

        /////////////////Sale of Shares Founder///////////
        $('.tab #sale-of-shares-stock, .tab #sale-of-shares-price').on('focusout', function(e){
           
            var id = e.currentTarget.attributes[0].value
            var stock = parseInt(document.querySelectorAll('#tab'+id+' #sale-of-shares-stock')[0].value)
            var price = parseInt(document.querySelectorAll('#tab'+id+' #sale-of-shares-price')[0].value)
           
            // $('#tab1 #sale-of-shares-stock').addClass('input-money-format')
            // $('#tab1 #sale-of-shares-price').addClass('input-money-format')
            // $('#tab1 #sale-of-shares-profit').addClass('input-money-format')

            if(!isNaN((stock)) && !isNaN((price))){
                if(price < stock){
                    $('.btn.btn-info.btn-lg').attr("disabled", true);
                    document.querySelectorAll('#tab'+id+' #sale-of-shares-profit')[0].value = 0
                }else{
                    $('.btn.btn-info.btn-lg').removeAttr('disabled');
                    $('#sale-of-shares-price').css({ 'background-color': "#ffffff" });
                    document.querySelectorAll('#tab'+id+' #sale-of-shares-profit')[0].value = Math.floor(price - stock)
                }
            }
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #sale-of-shares-stock, .tab #sale-of-shares-price').on('focus', function(e){
            $('.input-money-format').unmask();
        });

        /////////////////Hadiah Undian/////////////
        $('.tab #prize-lottery-brutto').on('focusout', function(e){
          
            //var bruto = parseInt(document.querySelectorAll('#prize-lottery-brutto')[0].value)
            var tab_id = e.currentTarget.attributes[1].value
            var bruto = e.currentTarget.value
            
            // $('input[name="brutto['+tab_id+']"]').addClass('input-money-format')
            // $('input[name="final['+tab_id+']"]').addClass('input-money-format')
            // $('input[name="netto['+tab_id+']"]').addClass('input-money-format')

            if(!isNaN(bruto)){
                var tax = document.querySelectorAll('input[name="final['+tab_id+']"]')[0].value = Math.floor(bruto * 0.25)
                document.querySelectorAll('input[name="netto['+tab_id+']"]')[0].value = Math.floor(bruto - tax)
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });
       
        $('.tab #prize-lottery-brutto').on('focus',function(){
            $('.input-money-format').unmask();
        });

        /////////////////Beasiswa//////////
        var $regexvalue=/^[0-9]*$/;
        $('#tab1 input#scholarship-value').on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#tab1 input#scholarship-value').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
            else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#tab1 input#scholarship-value').css({ 'background-color': "#ffffff" });
            }
        });

        $('#tab1 input#scholarship-value').on('focusout',function(){
            $('#tab1 input#scholarship-value').addClass('input-money-format')
            $('#tab1 input#scholarship-value').mask('#.##0', {reverse: true});
        });

        $('#tab1 input#scholarship-value').on('focus',function(){
            $('.input-money-format').unmask();
        });
        ////////////////////End of Beasiswa//////////

        ///////////////Honorarium Apbdn////////////////

        var pre_select = document.querySelectorAll('#honorarium-apbdn-form input[name="option_id"]');

        pre_select.forEach(function (element, index){
            var tab_index = index+1
            var income_id = element.attributes[1].value
            var option_value = 0
            
            switch (parseInt(income_id)) {
                case 29:
                    option_value = 1
                    break;
                case 30:
                    option_value = 2
                    break;
                case 31:
                    option_value = 3
                    break;
                case 32:
                    option_value = 4
                    break;
            
                default:
                    option_value = 0
                    break;
            }

            document.querySelectorAll('#tab'+tab_index+' #honorarium-apbdn-type')[0].value=option_value;
        })

        $('.tab #honorarium-apbdn-type').on('change',function(e){

            var select_id = e.currentTarget.attributes[0].value
            var tax;
            $('.input-money-format').unmask();
            var type = document.querySelectorAll('#tab'+select_id+' #honorarium-apbdn-type option:checked')[0].value

            if(parseInt(type) == 1 || parseInt(type) == 2){
                document.querySelectorAll('#tab'+select_id+' #tarif')[0].value = '0%' 
                tax = 0
            }else if(parseInt(type) == 3){
                document.querySelectorAll('#tab'+select_id+' #tarif')[0].value = '5%' 
                tax = 5
            }else if(parseInt(type) == 4){
                document.querySelectorAll('#tab'+select_id+' #tarif')[0].value = '15%' 
                tax = 15
            }

            var netto = parseInt(document.querySelectorAll('#tab'+select_id+' #honorarium-apbdn-netto')[0].value)
            
            // $('#tab1 #honorarium-apbdn-netto').addClass('input-money-format')
            // $('#tab1 #honorarium-apbdn-bruto').addClass('input-money-format')
            // $('#tab1 #honorarium-apbdn-final').addClass('input-money-format')

            if(!isNaN(netto)){
                var tax = document.querySelectorAll('#tab'+select_id+' #honorarium-apbdn-final')[0].value = Math.floor(netto * (tax/100))
                document.querySelectorAll('#tab'+select_id+' #honorarium-apbdn-bruto')[0].value = Math.floor(netto + tax)
            } 

        });

        $('.tab #honorarium-apbdn-netto').on('focusout',function(e){
            
            var netto_id = e.currentTarget.attributes[0].value
            var netto = parseInt(document.querySelectorAll('#tab'+netto_id+' #honorarium-apbdn-netto')[0].value)
            var tax = parseInt(document.querySelectorAll('#tab'+netto_id+' #tarif')[0].value)
            
            // $('#tab1 #honorarium-apbdn-netto').addClass('input-money-format')
            // $('#tab1 #honorarium-apbdn-bruto').addClass('input-money-format')
            // $('#tab1 #honorarium-apbdn-final').addClass('input-money-format')

            if(!isNaN(netto) && !isNaN(tax)){
                var tax = document.querySelectorAll('#tab'+netto_id+' #honorarium-apbdn-final')[0].value = Math.floor(netto * (tax/100))
                document.querySelectorAll('#tab'+netto_id+' #honorarium-apbdn-bruto')[0].value = Math.floor(netto + tax)
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #honorarium-apbdn-netto').on('focus',function(){
            $('.input-money-format').unmask();
        });

        //////////////End of Honorarium Apbdn

        //////////////Honorarium Comissioner//////////
        $('.tab #honorarium-commissioner-brutto, .tab #honorarium-commissioner-pph').on('focusout',function(e){
            
            var brutto_id = e.currentTarget.attributes[0].value
            var brutto = parseInt(document.querySelectorAll('#tab'+brutto_id+' #honorarium-commissioner-brutto')[0].value)
            var pph = parseInt(document.querySelectorAll('#tab'+brutto_id+' #honorarium-commissioner-pph')[0].value)
            
            // $('#tab1 #honorarium-commissioner-brutto').addClass('input-money-format')
            // $('#tab1 #honorarium-commissioner-pph').addClass('input-money-format')
            // $('#tab1 #honorarium-commissioner-netto').addClass('input-money-format')

            if(!isNaN(brutto) && !isNaN(pph)){
                if(brutto >= pph){
                    document.querySelectorAll('#tab'+brutto_id+' #honorarium-commissioner-netto')[0].value = Math.floor(brutto - pph)
                }else{
                    document.querySelectorAll('#tab'+brutto_id+' #honorarium-commissioner-netto')[0].value = Math.floor(0)
                }
            } 
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #honorarium-commissioner-cutter-npwp').on('keypress keydown keyup',function(e){
            var brutto_id = e.currentTarget.attributes[0].value
            $('#tab'+brutto_id+' .input-npwp-format').mask('##.###.###.#-###.###');
        });

        $('.tab #honorarium-commissioner-brutto , .tab #honorarium-commissioner-pph').on('focus',function(e){
            var brutto_id = e.currentTarget.attributes[0].value
            $('#tab'+brutto_id+' .input-money-format').unmask();
        });

        // $('.tab #honorarium-commissioner-brutto , .tab #honorarium-commissioner-pph').on('focusout',function(e){
        //     var brutto_id = e.currentTarget.attributes[0].value
        //     $('#tab'+brutto_id+' .input-money-format').mask();
        // });

        $('#honorarium-commissioner-pph-date').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        //////////////End of Honorarium Commissioner////////////

        //////////////Casual laborer//////////
        $('.tab #casual-laborer-brutto').on('focusout',function(e){
         
            var brutto_id = e.currentTarget.attributes[0].value
            var brutto = parseInt(document.querySelectorAll('#tab'+brutto_id+' #casual-laborer-brutto')[0].value)
            var pph = parseInt(document.querySelectorAll('#tab'+brutto_id+' #casual-laborer-pph')[0].value)
          
            // $('#tab1 #casual-laborer-brutto').addClass('input-money-format')
            // $('#tab1 #casual-laborer-pph').addClass('input-money-format')
            // $('#tab1 #casual-laborer-netto').addClass('input-money-format')

            if(!isNaN(brutto) && !isNaN(pph)){
                if(brutto >= pph){
                    document.querySelectorAll('#tab'+brutto_id+' #casual-laborer-netto')[0].value = Math.floor(brutto - pph)
                }else{
                    document.querySelectorAll('#tab'+brutto_id+' #casual-laborer-netto')[0].value = Math.floor(0)
                }
            } 

            // $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('.tab #casual-laborer-brutto , .tab #casual-laborer-pph').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('.tab #casual-laborer-pph-date').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        //////////////End of Casual Laborer////////////
    });

    ////////////Help or donation/////
    var $regexvalue=/^[0-9]*$/;
    $('.tab #help-or-donation-value').on('keypress',function(){
        if (!$(this).val().match($regexvalue)) {
        // there is a mismatch, hence show the error message
            $('.btn.btn-info.btn-lg').attr("disabled", true);
            $('.tab #help-or-donation-value').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
        }
    else{
            // else, do not display message
            $('.btn.btn-info.btn-lg').removeAttr('disabled');
            $('.tab #help-or-donation-value').css({ 'background-color': "#ffffff" });
        }
    });

    $('.tab #help-or-donation-value').on('focusout',function(){
        $('.tab #help-or-donation-value').addClass('input-money-format')
        $('.input-money-format').mask('#.##0', {reverse: true});
    });

    $('.tab #help-or-donation-value').on('focus',function(){
        $('.input-money-format').unmask();
    });
    ////////////End of Help or donation/////

    /////////////Accepted Building//////////
    $('.tab #accepted-building-njop').on('focusout',function(e){
       
        var njop_id = e.currentTarget.attributes[0].value
        var njop = parseInt(document.querySelectorAll('#tab'+njop_id+' #accepted-building-njop')[0].value)
        var tax = parseInt(document.querySelectorAll('#tab'+njop_id+' #tarif')[0].value)

        // $('#tab1 #accepted-building-njop').addClass('input-money-format')
        // $('#tab1 #accepted-building-final').addClass('input-money-format')

        if(!isNaN(njop)){
            document.querySelectorAll('#tab'+njop_id+' #accepted-building-final')[0].value = Math.floor(njop * tax/100)
        } 

        $('.input-money-format').mask('#.##0', {reverse: true});
    });

    $('.tab #accepted-building-njop').on('keypress',function(e){
        var njop_id = e.currentTarget.attributes[0].value
        if (!$(this).val().match($regexvalue)) {
        // there is a mismatch, hence show the error message
            $('.btn.btn-info.btn-lg').attr("disabled", true);
            $('#tab'+njop_id+' #accepted-building-njop').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
        }
        else{
            // else, do not display message
            $('.btn.btn-info.btn-lg').removeAttr('disabled');
            $('#tab'+njop_id+' #accepted-building-njop').css({ 'background-color': "#ffffff" });
        }
    });

    $('#tab1 #accepted-building-njop').on('click', 'a#remove-tab-legacy', function(e){
        removeTab(e, this);
    });

    $('.tab #accepted-building-njop').on('focus',function(){
        $('.tab #accepted-building-njop').unmask();
    });

    /////////////End of Accepted Building////////////

    //////////////////Another Once Earning///////////////

    // var radios = document.querySelectorAll('input[name="radio_id"]');

    // radios.forEach(function (element, index){
    //     var tab_index = index+1
    //     if(parseInt(element.value)== 1){
    //         document.querySelectorAll('#tab'+tab_index+' input[type="radio"]')[0].checked = true
    //     }else if((parseInt(element.value)== 2)){
    //         document.querySelectorAll('#tab'+tab_index+' input[type="radio"]')[1].checked = true
    //     }
    // })

    // $('#another-earning-form input.custom-control-input').on('change', function(e){
       
    //     var radio_value = e.currentTarget.attributes[3].value
    //     var radio_id = e.currentTarget.attributes[0].value

    //     if(parseInt(radio_value) == 1){
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[2].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[3].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[4].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[5].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[6].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[7].setAttribute('readonly',true)
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[8].setAttribute('readonly',true)
    //     }else if(parseInt(radio_value) == 2){
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[2].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[3].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[4].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[5].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[6].removeAttribute('readonly')
    //         // document.querySelectorAll('#awards-and-gift-form #tab1 input[type="text"]')[6].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[7].removeAttribute('readonly')
    //         document.querySelectorAll('#another-earning-form #tab'+radio_id+' input[type="text"]')[8].removeAttribute('readonly')
    //     }
    // });

    // $('.tab #another-earning-brutto , .tab #another-earning-netto, .tab #another-earning-pph-taken').on('focusout',function(e){

    //     var this_id = e.currentTarget.attributes[0].value

    //     $('.input-money-format').unmask();
        
    //     var type = parseInt(document.querySelector('#another-earning-form #tab'+this_id+' input[name="type['+this_id+']"]:checked').value)
        
    //     if(parseInt(type) == 2){
    //         var brutto = parseInt(document.querySelectorAll('#tab'+this_id+' #another-earning-brutto')[0].value)
    //         var netto = parseInt(document.querySelectorAll('#tab'+this_id+' #another-earning-netto')[0].value)
            
    //         if(!isNaN(netto) && !isNaN(brutto)){
    //             if(brutto >= netto){
    //                 document.querySelectorAll('#tab'+this_id+' #another-earning-pph-taken')[0].value = Math.floor(brutto - netto) 
    //             }else{
    //                 document.querySelectorAll('#tab'+this_id+' #another-earning-pph-taken')[0].value = 0 
    //             }
    //         } 
    //     }

    //     // $('#tab1 #another-earning-brutto').addClass('input-money-format')
    //     // $('#tab1 #another-earning-pph-taken').addClass('input-money-format')
    //     // $('#tab1 #another-earning-netto').addClass('input-money-format')
    //     // $('#tab1 #another-earning-cutter-npwp').addClass('input-npwp-format')

    //     $('.input-money-format').mask('#.##0', {reverse: true});
    //     $('.input-npwp-format').mask('##.###.###.#-###.###');
    // });

    // $('.tab #another-earning-brutto, .tab #another-earning-netto').on('focus',function(e){
    //     $('.input-money-format').unmask();
    //     $('.input-npwp-format').unmask();
    // });

    // $('#another-earning-pph-date').datetimepicker({
    //     format: 'DD/MM/YYYY'
    // });
    /////////////End of Another Once Earning////////////

    //////////////Credit Card////////////
    // pindah ke eventArgument

    //////////////Sticky Notes///////////
    $('div#post_it input').each(function (index, value)
        {
            // $('div.note_cnt textarea').append((index + 1)+'. '+value.value+'&#13;&#10;')
            $('div.cnt ol').append('<li>'+value.value+'</li>')
            $('div.nttoogle span').attr('data-badge',(index + 1))
        }
    );
}