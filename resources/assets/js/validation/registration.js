export default function($){
    $('form#registration').validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        }
    });
}