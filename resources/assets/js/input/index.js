export default function($) {
    $('.bank-name').select2();
    $('.charity-name').select2();
    $('.income-name').select2();
    $('.select2-single').select2();
    $('.input-npwp-format').mask('##.###.###.#-###.###');
    $('.input-money-format').mask('#.##0', {reverse: true});
    $('.input-kurs-format').mask('#.##0', {reverse: true});
    $('.input-picker-format').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('form.main-form').on('submit', function(e){
        $('.input-npwp-format').unmask();
        $('.input-money-format').unmask();
        $('.input-kurs-format').unmask();
    });

    $('div.float input[type="text"].form-control, div.float input[type="password"].form-control').focus(function () {
        $(this).closest('.float').addClass('fill focus');
    }).blur(function() {
        const parentElem = $(this).closest('.float');
        if(parentElem.hasClass('fill focus') && $(this).val() == '') parentElem.removeClass('fill focus');
    });

    $('div.float input.form-control').focus(function () {
        $(this).closest('.float').addClass('fill focus');
    }).blur(function() {
        const parentElem = $(this).closest('.float');
        if(parentElem.hasClass('fill focus') && $(this).val() == '') parentElem.removeClass('fill focus');
    });


//#region commend

    //js for credit card
    // $('.currency').change(function(){
    //     const currentTime = new Date();
    //     const prevYear = currentTime.getFullYear() - 1;
    //     const prevMonth = '12';
    //     const prevDate = '31';

    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const kursPajakId = 'kurspajak-' + numberId;
    //     const valasId = 'liability-foreign-value-' + numberId;
    //     const hiddenIdrId = 'hidden-liability-value-' + numberId;
    //     const idrId = 'liability-value-' + numberId;
    //     let valasValue = $('#'+valasId).val();
    //     let kursPajakValue = 0;
    //     let idrValue = 0;

    //     if(isNaN(valasValue)){
    //         valasValue = 0.00;
    //     }

    //     const currencyName = $('#'+inputId+' option:selected').text();

    //     if(currencyName != 'IDR'){
    //         $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = result.kurspajak_value;
    //                 idrValue = (kursPajakValue * valasValue).toFixed(2);

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //         });
    //     }
    //     else {
    //         kursPajakValue = 1;
    //         idrValue = valasValue;

    //         $('#'+kursPajakId).val(kursPajakValue);
    //         $('#'+hiddenIdrId).val(idrValue);
    //         $('#'+idrId).val(idrValue);
    //     }

    // });

    // $('.liability-foreign-value').change(function(){
    //     const currentTime = new Date();
    //     const prevYear = currentTime.getFullYear() - 1;
    //     const prevMonth = '12';
    //     const prevDate = '31';

    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const currencyId = 'currency-' + numberId;
    //     const kursPajakId = 'kurspajak-' + numberId;
    //     const valasId = 'liability-foreign-value-' + numberId;
    //     const hiddenIdrId = 'hidden-liability-value-' + numberId;
    //     const idrId = 'liability-value-' + numberId;
    //     let valasValue = $('#'+valasId).val();

    //     const currencyName = $('#'+currencyId+' option:selected').text();
    //     let kursPajakValue = 0;
    //     let idrValue = 0;

    //     if(currencyName != 'IDR'){
    //         $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = result.kurspajak_value;
    //                 idrValue = (kursPajakValue * valasValue).toFixed(2);

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //         });
    //     }
    //     else {
    //         kursPajakValue = 1;
    //         idrValue = valasValue;

    //         $('#'+kursPajakId).val(kursPajakValue);
    //         $('#'+hiddenIdrId).val(idrValue);
    //         $('#'+idrId).val(idrValue);
    //     }

    // });


    //js for vehicle
    /*
    $('.vehicle-currency').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const acquisitionYearId = 'acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
        const idrId = 'vehicle-value-' + numberId;
        let valasValue = $('#'+valasId).val();
        let kursPajakValue = 0;
        let idrValue = 0;

        let currencyName = $('#'+inputId+' option:selected').text();

        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+inputId).val(1);
            currencyName = $('#'+inputId+' option:selected').text();
        }

        if(isNaN(valasValue)){
            valasValue = 0.00;
        }

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });

    $('.acquisition-year').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const vehicleCurrencyId = 'vehicle-currency-' + numberId;
        const acquisitionYearId = 'acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
        const idrId = 'vehicle-value-' + numberId;
        let valasValue = $('#'+valasId).val();

        let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+vehicleCurrencyId).val(1);
            currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        }

        let kursPajakValue = 0;
        let idrValue = 0;

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });

    $('.vehicle-foreign-value').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const vehicleCurrencyId = 'vehicle-currency-' + numberId;
        const acquisitionYearId = 'acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
        const idrId = 'vehicle-value-' + numberId;
        let valasValue = $('#'+valasId).val();

        let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+vehicleCurrencyId).val(1);
            currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        }

        let kursPajakValue = 0;
        let idrValue = 0;

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });

    $('.vehicle-type').change(function(){
        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        if($('#'+inputId).val() == 18){
            $('#clone-'+numberId).find('#police-number-'+numberId).attr('readonly', 'readonly');
            $('#clone-'+numberId).find('#bpkb-number-'+numberId).attr('readonly', 'readonly');
        } else{
            $('#clone-'+numberId).find('#police-number-'+numberId).removeAttr('readonly');
            $('#clone-'+numberId).find('#bpkb-number-'+numberId).removeAttr('readonly');
        }
    });
    */


//js for vehicle-liability
/** 
    $('.vehicle-liability-currency').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-liability-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
        const idrId = 'vehicle-liability-value-' + numberId;
        let valasValue = $('#'+valasId).val();
        let kursPajakValue = 0;
        let idrValue = 0;

        let currencyName = $('#'+inputId+' option:selected').text();

        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+inputId).val(1);
            currencyName = $('#'+inputId+' option:selected').text();
        }

        if(isNaN(valasValue)){
            valasValue = 0.00;
        }

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });

    $('.vehicle-acquisition-year').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const vehicleCurrencyId = 'vehicle-liability-currency-' + numberId;
        const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-liability-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
        const idrId = 'vehicle-liability-value-' + numberId;
        let valasValue = $('#'+valasId).val();

        let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+vehicleCurrencyId).val(1);
            currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        }

        let kursPajakValue = 0;
        let idrValue = 0;

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });

    $('.vehicle-liability-foreign-value').change(function(){
        const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';

        const inputId = $(this).attr('id');
        const parts = inputId.split('-');
        const numberId = parts.pop();

        const vehicleCurrencyId = 'vehicle-liability-currency-' + numberId;
        const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
        const kursPajakId = 'kurspajak-' + numberId;
        const valasId = 'vehicle-liability-foreign-value-' + numberId;
        const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
        const idrId = 'vehicle-liability-value-' + numberId;
        let valasValue = $('#'+valasId).val();

        let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        if(
            (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                && currencyName != 'IDR'
            )
        {
            alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
            $('#'+vehicleCurrencyId).val(1);
            currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
        }

        let kursPajakValue = 0;
        let idrValue = 0;

        if(currencyName != 'IDR'){
            $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = result.kurspajak_value;
                    idrValue = (kursPajakValue * valasValue).toFixed(2);

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
            });
        }
        else {
            kursPajakValue = 1;
            idrValue = valasValue;

            $('#'+kursPajakId).val(kursPajakValue);
            $('#'+hiddenIdrId).val(idrValue);
            $('#'+idrId).val(idrValue);
        }

    });
*/

//js for property
    // $('.property-currency').change(function(){
    //     const currentTime = new Date();
    //     const prevYear = currentTime.getFullYear() - 1;
    //     const prevMonth = '12';
    //     const prevDate = '31';

    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //     const kursPajakId = 'kurspajak-' + numberId;
    //     const valasId = 'property-foreign-value-' + numberId;
    //     const hiddenIdrId = 'hidden-property-value-' + numberId;
    //     const idrId = 'property-value-' + numberId;
    //     let valasValue = $('#'+valasId).val();
    //     let kursPajakValue = 0;
    //     let idrValue = 0;

    //     let currencyName = $('#'+inputId+' option:selected').text();

    //     if(
    //         (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //             && currencyName != 'IDR'
    //         )
    //     {
    //         alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //         $('#'+inputId).val(1);
    //         currencyName = $('#'+inputId+' option:selected').text();
    //     }

    //     if(isNaN(valasValue)){
    //         valasValue = 0.00;
    //     }

    //     if(currencyName != 'IDR'){
    //         $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = result.kurspajak_value;
    //                 idrValue = (kursPajakValue * valasValue).toFixed(2);

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //         });
    //     }
    //     else {
    //         kursPajakValue = 1;
    //         idrValue = valasValue;

    //         $('#'+kursPajakId).val(kursPajakValue);
    //         $('#'+hiddenIdrId).val(idrValue);
    //         $('#'+idrId).val(idrValue);
    //     }

    // });

    // $('.property-acquisition-year').change(function(){
    //     const currentTime = new Date();
    //     const prevYear = currentTime.getFullYear() - 1;
    //     const prevMonth = '12';
    //     const prevDate = '31';

    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const currencyId = 'property-currency-' + numberId;
    //     const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //     const kursPajakId = 'kurspajak-' + numberId;
    //     const valasId = 'property-foreign-value-' + numberId;
    //     const hiddenIdrId = 'hidden-property-value-' + numberId;
    //     const idrId = 'property-value-' + numberId;
    //     let valasValue = $('#'+valasId).val();

    //     let currencyName = $('#'+currencyId+' option:selected').text();
    //     if(
    //         (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //             && currencyName != 'IDR'
    //         )
    //     {
    //         alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //         $('#'+currencyId).val(1);
    //         currencyName = $('#'+currencyId+' option:selected').text();
    //     }

    //     let kursPajakValue = 0;
    //     let idrValue = 0;

    //     if(currencyName != 'IDR'){
    //         $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = result.kurspajak_value;
    //                 idrValue = (kursPajakValue * valasValue).toFixed(2);

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //         });
    //     }
    //     else {
    //         kursPajakValue = 1;
    //         idrValue = valasValue;

    //         $('#'+kursPajakId).val(kursPajakValue);
    //         $('#'+hiddenIdrId).val(idrValue);
    //         $('#'+idrId).val(idrValue);
    //     }

    // });

    // $('.property-foreign-value').change(function(){
    //     const currentTime = new Date();
    //     const prevYear = currentTime.getFullYear() - 1;
    //     const prevMonth = '12';
    //     const prevDate = '31';

    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const currencyId = 'property-currency-' + numberId;
    //     const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //     const kursPajakId = 'kurspajak-' + numberId;
    //     const valasId = 'property-foreign-value-' + numberId;
    //     const hiddenIdrId = 'hidden-property-value-' + numberId;
    //     const idrId = 'property-value-' + numberId;
    //     let valasValue = $('#'+valasId).val();

    //     let currencyName = $('#'+currencyId+' option:selected').text();
    //     if(
    //         (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //             && currencyName != 'IDR'
    //         )
    //     {
    //         alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //         $('#'+currencyId).val(1);
    //         currencyName = $('#'+currencyId+' option:selected').text();
    //     }

    //     let kursPajakValue = 0;
    //     let idrValue = 0;

    //     if(currencyName != 'IDR'){
    //         $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = result.kurspajak_value;
    //                 idrValue = (kursPajakValue * valasValue).toFixed(2);

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //         });
    //     }
    //     else {
    //         kursPajakValue = 1;
    //         idrValue = valasValue;

    //         $('#'+kursPajakId).val(kursPajakValue);
    //         $('#'+hiddenIdrId).val(idrValue);
    //         $('#'+idrId).val(idrValue);
    //     }

    // });

    // $('.property-type').change(function(){
    //     const inputId = $(this).attr('id');
    //     const parts = inputId.split('-');
    //     const numberId = parts.pop();

    //     const propertyType = $('#property-type-'+numberId).val();

    //     if(propertyType == 'Apartemen'){
    //         $('#property-function-'+numberId).html(
    //                 '<option value="Untuk Tempat Tinggal">Untuk Tempat Tinggal</option>'
    //             );
    //     } else {
    //         $('#property-function-'+numberId).html(
    //                 '<option value="Untuk Tempat Tinggal">Untuk Tempat Tinggal</option>' +
    //                 '<option value="Untuk Usaha">Untuk Usaha</option>'
    //             );
    //     }
    // });



    

// //js for property-liability
//     $('.property-liability-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'property-liability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
//         const idrId = 'property-liability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.property-liability-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'property-liability-currency-' + numberId;
//         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'property-liability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
//         const idrId = 'property-liability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.property-liability-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'property-liability-currency-' + numberId;
//         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'property-liability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
//         const idrId = 'property-liability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.property-liability-bank-name').change(function(){
//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const bankName = $('#property-liability-bank-name-'+numberId).val();

//         if(bankName == 'Lainnya'){
//             const otherField = $('#property-liability-other-bank-name-field-'+numberId).show();
//             otherField.find('input').attr('required', 'required');
//         } else {
//             const otherField = $('#property-liability-other-bank-name-field-'+numberId).hide();
//             otherField.find('input').removeAttr('required');
//         }

//     });


// //js for precious-assets-detail
//     $('.precious-assets-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'precious-assets-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
//         const idrId = 'precious-assets-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.precious-assets-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'precious-assets-currency-' + numberId;
//         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'precious-assets-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
//         const idrId = 'precious-assets-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.precious-assets-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'precious-assets-currency-' + numberId;
//         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'precious-assets-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
//         const idrId = 'precious-assets-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });


// //js for tradeable-stocks
//     $('.tradeable-stocks-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//         const idrId = 'tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.tradeable-stocks-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'tradeable-stocks-currency-' + numberId;
//         const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//         const idrId = 'tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.tradeable-stocks-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'tradeable-stocks-currency-' + numberId;
//         const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//         const idrId = 'tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });


// //js for mutual-funds
//     $('.mutual-funds-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'mutual-funds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//         const idrId = 'mutual-funds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.mutual-funds-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'mutual-funds-currency-' + numberId;
//         const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'mutual-funds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//         const idrId = 'mutual-funds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.mutual-funds-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'mutual-funds-currency-' + numberId;
//         const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'mutual-funds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//         const idrId = 'mutual-funds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }
//     });


// //js for insurance
//     $('.insurance-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'insurance-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//         const idrId = 'insurance-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.insurance-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'insurance-currency-' + numberId;
//         const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'insurance-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//         const idrId = 'insurance-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.insurance-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'insurance-currency-' + numberId;
//         const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'insurance-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//         const idrId = 'insurance-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for non-tradeable-stocks
//     $('.non-tradeable-stocks-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//         const idrId = 'non-tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.non-tradeable-stocks-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'non-tradeable-stocks-currency-' + numberId;
//         const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//         const idrId = 'non-tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.non-tradeable-stocks-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'non-tradeable-stocks-currency-' + numberId;
//         const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//         const idrId = 'non-tradeable-stocks-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for insurance-claim
//     $('.insurance-claim-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'insurance-claim-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-insurance-claim-value-' + numberId;
//         const idrId = 'insurance-claim-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.insurance-claim-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'insurance-claim-currency-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'insurance-claim-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-insurance-claim-value-' + numberId;
//         const idrId = 'insurance-claim-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.insurance-claim-type').change(function(){
//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const insuranceClaimType = $('#insurance-claim-type-'+numberId).val();

//         if(insuranceClaimType == 'Kecelakaan'){
//             $('#insurance-claim-purpose-'+numberId).removeAttr('readonly');
//             $('#insurance-claim-purpose-'+numberId).html(
//                     '<option value="Rumah">Rumah</option>' +
//                     '<option value="Kendaraan">Kendaraan</option>'
//                 );
//         } else {
//             $('#insurance-claim-purpose-'+numberId).attr('readonly', 'readonly');
//             $('#insurance-claim-purpose-'+numberId).html(
//                 '<option value="">--</option>'
//             );
//         }
//     });

// //js for capital-investment-companies
//     $('.cic-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'cic-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-cic-value-' + numberId;
//         const idrId = 'cic-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.cic-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'cic-currency-' + numberId;
//         const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'cic-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-cic-value-' + numberId;
//         const idrId = 'cic-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.cic-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'cic-currency-' + numberId;
//         const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'cic-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-cic-value-' + numberId;
//         const idrId = 'cic-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for corporate-bonds
//     $('.corporate-bonds-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'corporate-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//         const idrId = 'corporate-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.corporate-bonds-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'corporate-bonds-currency-' + numberId;
//         const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'corporate-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//         const idrId = 'corporate-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.corporate-bonds-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'corporate-bonds-currency-' + numberId;
//         const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'corporate-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//         const idrId = 'corporate-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });


// //js for government-bonds
//     $('.government-bonds-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'government-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//         const idrId = 'government-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.government-bonds-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'government-bonds-currency-' + numberId;
//         const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'government-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//         const idrId = 'government-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.government-bonds-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'government-bonds-currency-' + numberId;
//         const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'government-bonds-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//         const idrId = 'government-bonds-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-debentures
//     $('.other-debentures-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-debentures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//         const idrId = 'other-debentures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.other-debentures-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'other-debentures-currency-' + numberId;
//         const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-debentures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//         const idrId = 'other-debentures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.other-debentures-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'other-debentures-currency-' + numberId;
//         const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-debentures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//         const idrId = 'other-debentures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-investments
//     $('.other-investments-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-investments-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//         const idrId = 'other-investments-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.other-investments-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'other-investments-currency-' + numberId;
//         const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-investments-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//         const idrId = 'other-investments-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.other-investments-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'other-investments-currency-' + numberId;
//         const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'other-investments-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//         const idrId = 'other-investments-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-cash-equivalents
//     $('.oce-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oce-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oce-value-' + numberId;
//         const idrId = 'oce-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.oce-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'oce-currency-' + numberId;
//         const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oce-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oce-value-' + numberId;
//         const idrId = 'oce-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.oce-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'oce-currency-' + numberId;
//         const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oce-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oce-value-' + numberId;
//         const idrId = 'oce-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for special-transportations
//     $('.spctrans-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'spctrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//         const idrId = 'spctrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.spctrans-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'spctrans-currency-' + numberId;
//         const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'spctrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//         const idrId = 'spctrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.spctrans-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'spctrans-currency-' + numberId;
//         const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'spctrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//         const idrId = 'spctrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });


// //js for other-transportations
//     $('.othertrans-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'othertrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//         const idrId = 'othertrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.othertrans-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'othertrans-currency-' + numberId;
//         const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'othertrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//         const idrId = 'othertrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.othertrans-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'othertrans-currency-' + numberId;
//         const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'othertrans-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//         const idrId = 'othertrans-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for electrnics-furnitures
//     $('.electronicsfurnitures-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//         const idrId = 'electronicsfurnitures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.electronicsfurnitures-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'electronicsfurnitures-currency-' + numberId;
//         const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//         const idrId = 'electronicsfurnitures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.electronicsfurnitures-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'electronicsfurnitures-currency-' + numberId;
//         const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//         const idrId = 'electronicsfurnitures-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-immovable-property
//     $('.oip-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oip-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oip-value-' + numberId;
//         const idrId = 'oip-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.oip-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'oip-currency-' + numberId;
//         const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oip-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oip-value-' + numberId;
//         const idrId = 'oip-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.oip-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'oip-currency-' + numberId;
//         const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'oip-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-oip-value-' + numberId;
//         const idrId = 'oip-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-movable-property
//     $('.omp-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'omp-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-omp-value-' + numberId;
//         const idrId = 'omp-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.omp-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'omp-currency-' + numberId;
//         const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'omp-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-omp-value-' + numberId;
//         const idrId = 'omp-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.omp-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'omp-currency-' + numberId;
//         const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'omp-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-omp-value-' + numberId;
//         const idrId = 'omp-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for account-receivable
//     $('.accreceivable-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'accreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//         const idrId = 'accreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.accreceivable-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'accreceivable-currency-' + numberId;
//         const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'accreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//         const idrId = 'accreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.accreceivable-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'accreceivable-currency-' + numberId;
//         const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'accreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//         const idrId = 'accreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for affiliate-receivable
//     $('.affreceivable-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//         const idrId = 'affreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.affreceivable-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'affreceivable-currency-' + numberId;
//         const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//         const idrId = 'affreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.affreceivable-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'affreceivable-currency-' + numberId;
//         const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//         const idrId = 'affreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for financial-institution-liability
//     $('.fil-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'fil-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-fil-value-' + numberId;
//         const idrId = 'fil-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.fil-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'fil-currency-' + numberId;
//         const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'fil-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-fil-value-' + numberId;
//         const idrId = 'fil-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.fil-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'fil-currency-' + numberId;
//         const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'fil-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-fil-value-' + numberId;
//         const idrId = 'fil-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-receivable
//     $('.otherreceivable-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//         const idrId = 'otherreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = parseFloat(result.kurspajak_value);
//                     idrValue = kursPajakValue * valasValue;

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.otherreceivable-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'otherreceivable-currency-' + numberId;
//         const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//         const idrId = 'otherreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = parseFloat(result.kurspajak_value);
//                     idrValue = kursPajakValue * valasValue;

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.otherreceivable-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'otherreceivable-currency-' + numberId;
//         const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherreceivable-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//         const idrId = 'otherreceivable-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = parseFloat(result.kurspajak_value);
//                     idrValue = kursPajakValue * valasValue;

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for affiliate-liability
//     $('.affliability-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//         const idrId = 'affliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.affliability-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'affliability-currency-' + numberId;
//         const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//         const idrId = 'affliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.affliability-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'affliability-currency-' + numberId;
//         const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'affliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//         const idrId = 'affliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

// //js for other-liability
//     $('.otherliability-currency').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//         const idrId = 'otherliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();
//         let kursPajakValue = 0;
//         let idrValue = 0;

//         let currencyName = $('#'+inputId+' option:selected').text();

//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+inputId).val(1);
//             currencyName = $('#'+inputId+' option:selected').text();
//         }

//         if(isNaN(valasValue)){
//             valasValue = 0.00;
//         }

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.otherliability-acquisition-year').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'otherliability-currency-' + numberId;
//         const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//         const idrId = 'otherliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });

//     $('.otherliability-foreign-value').change(function(){
//         const currentTime = new Date();
//         const prevYear = currentTime.getFullYear() - 1;
//         const prevMonth = '12';
//         const prevDate = '31';

//         const inputId = $(this).attr('id');
//         const parts = inputId.split('-');
//         const numberId = parts.pop();

//         const currencyId = 'otherliability-currency-' + numberId;
//         const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//         const kursPajakId = 'kurspajak-' + numberId;
//         const valasId = 'otherliability-foreign-value-' + numberId;
//         const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//         const idrId = 'otherliability-value-' + numberId;
//         let valasValue = $('#'+valasId).val();

//         let currencyName = $('#'+currencyId+' option:selected').text();
//         if(
//             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                 && currencyName != 'IDR'
//             )
//         {
//             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//             $('#'+currencyId).val(1);
//             currencyName = $('#'+currencyId+' option:selected').text();
//         }

//         let kursPajakValue = 0;
//         let idrValue = 0;

//         if(currencyName != 'IDR'){
//             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                 , function(result, status){
//                     result = jQuery.parseJSON(result);
//                     kursPajakValue = result.kurspajak_value;
//                     idrValue = (kursPajakValue * valasValue).toFixed(2);

//                     $('#'+kursPajakId).val(kursPajakValue);
//                     $('#'+hiddenIdrId).val(idrValue);
//                     $('#'+idrId).val(idrValue);
//             });
//         }
//         else {
//             kursPajakValue = 1;
//             idrValue = valasValue;

//             $('#'+kursPajakId).val(kursPajakValue);
//             $('#'+hiddenIdrId).val(idrValue);
//             $('#'+idrId).val(idrValue);
//         }

//     });
//#endregion

    $('#image_invoice_product').on('change', function(evt) {
        var selectedImage = evt.currentTarget.files[0];
        var imageWrapper = document.querySelector('.image-wrapper');
        var theImage = document.createElement('img');
        imageWrapper.innerHTML = '';
    
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test(selectedImage.name.toLowerCase())) {
            if (typeof(FileReader) != 'undefined') {
                var reader = new FileReader();
                reader.onload = function(e) {
                    theImage.id = 'new-selected-image';
                    theImage.src = e.target.result;
                    imageWrapper.appendChild(theImage);
                    $('.image-wrapper img').attr('width', '300px');
                }
                reader.readAsDataURL(selectedImage);
            } else {
                console.log('browser support issue');
            }
        } else {
        $(this).prop('value', null);
        console.log('please select and image file');
        }
    });


/**
 * 
 * JS FOR TAX AND PROFILE
 * 
 */

//#region my tax profile
    $('#my-tax-profile').on('change', '#klu', function(){
        const number = $('option:selected', this).attr('data-number');
        $('#my-tax-profile #klunumber').val(number);
    });
    
    $('#my-tax-profile').on('change', '#fillnpwp', function(e){
        if (this.checked){
            $('#my-tax-profile #npwp').val('');
            $('#my-tax-profile #npwp').prop("disabled", true);
            $('#my-tax-profile #npwp').prop('required', false);

        }else{
            $('#my-tax-profile #npwp').prop("disabled", false);
            $('#my-tax-profile #npwp').prop('required', true);
        }
    });

    $('#my-tax-profile').on('change', '#fillefin', function(e){
        if (this.checked){
            $('#my-tax-profile #efin').val('');
            $('#my-tax-profile #efin').prop('disabled', true);
            $('#my-tax-profile #efin').prop('required', false);
        }else{
            $('#my-tax-profile #efin').prop('disabled', false);
            $('#my-tax-profile #efin').prop('required', true);
        }
    });
//#endregion

//#region my wife tax profile
    $('#wife-tax-profile').on('change', '#fillktp', function(){
        if (this.checked){
            $('input#nik').val('');
            $('input#nik').prop('disabled', true);
            $('input#nik').prop('required', false);
        }else{
            $('input#nik').prop('disabled', false);
            $('input#nik').prop('required', true);
        }
    });

    $('#wife-tax-profile').on('click', 'input#jobstatus', function(){
        const val = $(this).val();
        console.log(val);
        if(val > 1){
            $('#klu').removeAttr('disabled');

            if(val == 5 || val == 4 || val == 3){  //disable final, enable non-final
                $('#companion-final-income').attr('disabled', 'disabled');
                $('#companion-final-income').val('');
                $('#companion-nonfinal-income').removeAttr('disabled');
                $('#klu').removeAttr('disabled');
                if (val == 5){
                    $('#modal_my_pair_tax_profile_popup').modal('show');
                }
            } else {                   //disable nonfinal, enable final
                $('#companion-final-income').removeAttr('disabled');
                $('#companion-nonfinal-income').attr('disabled', 'disabled');
                $('#companion-nonfinal-income').val('');
                $('#klu').removeAttr('disabled');
            }
        } else {
            $('#companion-final-income').attr('disabled', 'disabled');
            $('#companion-final-income').val('');
            $('#companion-nonfinal-income').attr('disabled', 'disabled');
            $('#companion-nonfinal-income').val('');
            $('#klu').val('');
            $('#klu').attr('disabled', 'disabled');
        }
    });

//#endregion

//#region my husband tax profile
    $('#husband-tax-profile').on('change', '#fillnik', function(){
        if (this.checked){
            $('input#nik').val('');
            $('input#nik').prop('disabled', true);
            $('input#nik').prop('required', false);
        }else{
            $('input#nik').prop('disabled', false);
            $('input#nik').prop('required', true);
        }
    });
//#endregion

//#region my spouse profile
    $('#spouse-tax-profile').on('change', '#klu', function(){
        const number = $('option:selected', this).attr('data-number');
        $('#spouse-tax-profile #klunumber').val(number);
    });

    $('#spouse-tax-profile').on('change', '#fillnpwp', function(e){
        if (this.checked){
            $('#spouse-tax-profile #npwp').val('');
            $('#spouse-tax-profile #npwp').prop("disabled", true);
            $('#spouse-tax-profile #npwp').prop('required', false);

        }else{
            $('#spouse-tax-profile #npwp').prop("disabled", false);
            $('#spouse-tax-profile #npwp').prop('required', true);
        }
    });
//#endregion

//#region dependant
    $(".dependant-relation").change(function(){
        var id = $(this).attr("data-id");
        var value = $('#dependant_relation_' + id.toString()).val();
        $('#job_' + id.toString()).children('option:not(:first)').remove();
        if (value){
            if (value == 8 || value == 5){
                $("#job_" + id.toString()).append('<option value="Pelajar">Pelajar</option>');
                $("#job_" + id.toString()).append('<option value="Tidak Bekerja">Tidak Bekerja</option>');
            }else{
                $("#job_" + id.toString()).append('<option value="Tidak Bekerja/Pensiun">Tidak Bekerja/Pensiun</option>');
                $("#job_" + id.toString()).append('<option value="Pekerja Bebas">Pekerja Bebas</option>');
            }
        }
    });

    $('#dependants-form').on('change', '.does-not-have-ktp', function(e){
        const id = $(this).attr('data-id');
        if (this.ckecked){
            $('#nik_' + id.toString()).val('');
            $('#nik_' + id.toString()).prop('readonly', true);
            $("#nothavektp_status_" + id.toString()).val(1);
        }else{
            $('#nik_' + id.toString()).val('');
            $('#nik_' + id.toString()).prop('readonly', false);
            $("#nothavektp_status_" + id.toString()).val(0);
        }
    });
//#endregion

//#region overseas income
$('#overseas-income').on('change', '#income-type', function(){
    const type = $(this).val();
    const current_tab = $(this).closest('div .tab');

    if (type == 'Lain-lain'){
        current_tab.find('#other-income-type').show();
    }else{
        current_tab.find('#other-income-type').val('');
        current_tab.find('#other-income-type').hide();
    }
});

$('#overseas-income').on('change', '#gross-value', function (){
    const current_tab = $(this).closest('div .tab');

    let bruto = parseFloat(current_tab.find('#gross-value').cleanVal());
    let tara = parseFloat(current_tab.find('#tara-value').cleanVal());

    if (isNaN(bruto)) bruto = 0;
    if (isNaN(tara)) tara = 0;

    let netto = bruto - tara;
    if (netto < 0 ) netto = 0;
    current_tab.find('#net-value').val(netto).trigger('change');
});

$('#overseas-income').on('change', '#tara-value', function () {
    const current_tab = $(this).closest('div .tab');

    let bruto = parseFloat(current_tab.find('#gross-value').cleanVal());
    let tara = parseFloat(current_tab.find('#tara-value').cleanVal());

    if (isNaN(bruto)) bruto = 0;
    if (isNaN(tara)) tara = 0;

    const netto = bruto - tara;
    current_tab.find('#net-value').val(netto).trigger('change');
});

//#endregion

//#region asset and liability credite vehicel
    $('#vehicle-liability').on('change', '#vehicle-number', function (e){
        const current_tab = $(this).closest('div .tab');
        const vehicle_merk = current_tab.find('#vehicle-number option:selected').attr('data-merk');
        current_tab.find('#vehicle-merk').val(vehicle_merk);
    });
//#endregion

//#region asset and liability property
    $('#property').on('change', '.property-type', function(){
        const current_tab = $(this).closest('div .tab');
        const value = current_tab.find('#property-type option:selected').attr('data-id');
        current_tab.find('#property-function').children('option:not(:first)').remove();
        current_tab.find('#certificate-type').children('option:not(:first)').remove();
        if (value == 1){ //tanah kosong
            current_tab.find('#property-function').append('<option value="28">Untuk Tempat Tinggal</option>');
            current_tab.find('#property-function').append('<option value="30">Untuk Usaha (Toko,pabrik,Gudang, dll)</option>');
            current_tab.find('#property-function').append('<option value="29">Untuk Usaha (Pertaniaan, perkebunan, perikanan, dll)</option>');
        }else if (value == 2){ //tanah dan bangunan
            current_tab.find('#property-function').append('<option value="28">Untuk Tempat Tinggal</option>');
            current_tab.find('#property-function').append('<option value="30">Untuk Usaha (Toko,pabrik,Gudang, dll)</option>');
        }else if (value == 3){ //apartemen
            current_tab.find('#property-function').append('<option value="28">Untuk Tempat Tinggal</option>');
        }

    });

    $('#property').on('change', '.property-function', function(){
        const current_tab = $(this).closest('div .tab');
        const value = current_tab.find('#property-function option:selected').val();
        const value_type = current_tab.find('#property-type option:selected').attr('data-id');

        current_tab.find('#certificate-type').children('option:not(:first)').remove();
        if (value != null || value != ''){
            if (value == 28){
                if (value_type == 3){
                    current_tab.find("#certificate-type").append('<option value="SKGB">SKGB (Sertifikat Kepemilikan Bangunan Gedung)</option>');
                }else{
                    current_tab.find("#certificate-type").append('<option value="HM">HM</option>');
                    current_tab.find("#certificate-type").append('<option value="HGB">HGB</option>');
                    current_tab.find("#certificate-type").append('<option value="Letter C">Letter C</option>');
                    current_tab.find("#certificate-type").append('<option value="Letter D">Letter D</option>');
                    if (value_type == 2){
                        current_tab.find("#certificate-type").append('<option value="SHKRS">SHKRS (Sertifikat Hak Kepemilikan Rumah Susun)</option>');
                    }
                }
            }else{
                current_tab.find("#certificate-type").append('<option value="HM">HM</option>');
                current_tab.find("#certificate-type").append('<option value="HGB">HGB</option>');
                current_tab.find("#certificate-type").append('<option value="Letter C">Letter C</option>');
                current_tab.find("#certificate-type").append('<option value="Letter D">Letter D</option>');
            }
        }
    });
//#endregion

//#region klaim asuransi
    $('#insurance-claim').on('change', '#account-number', function (){
        const current_tab = $(this).closest('div .tab');
        const user_asset = current_tab.find('#account-number option:selected').val();
        const asset = current_tab.find('#account-number option:selected').attr('data-asset');
        const company = current_tab.find('#account-number option:selected').attr('data-company');
        console.log(asset);
        console.log(company);
        current_tab.find('#company').val(company);
        current_tab.find('#asset-id').val(asset);
    })
//#endregion


/**
 * 
 * JS FOR ADDITIONAL INCOME
 * 
 */


//#region land and building (additional income)
    $('#land-and-building').on('change', '#penanggung', function(e){
        const current_tab = $(this).closest('div .tab');
        const penanggung = current_tab.find('input[id="penanggung"]:checked').val();

        let price = parseFloat(current_tab.find('.rent-price').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let value = 0;
        let result = 0;

        if (isNaN(price)) price = 0;
        if (isNaN(tarif)) tarif = 0;

        if (penanggung == 'Penyewa'){
            if (tarif > 0){
                value = Math.floor(price / tarif);
                result = value - price;
            }
        }else{
            if (tarif > 0){
                value = Math.floor(price * tarif);
                result = price - value;
            }
        }

        current_tab.find('#rent-price-received').val(value).trigger('input');
        current_tab.find('#tax-final').val(result).trigger('input');

    });

    $('#land-and-building').on('change', '#rent-price', function(){
        const current_tab = $(this).closest('div .tab');
        const penanggung = current_tab.find('input[id="penanggung"]:checked').val();

        let price = parseFloat(current_tab.find('.rent-price').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let value = 0;
        let result = 0;

        if (isNaN(price)) price = 0;
        if (isNaN(tarif)) tarif = 0;

        if (penanggung == 'Penyewa'){
            if (tarif > 0){
                value = Math.floor(price / tarif);
                result = value - price;
            }
        }else{
            if (tarif > 0){
                value = Math.floor(price * tarif);
                result = price - value;
            }
        }

        current_tab.find('#rent-price-received').val(value).trigger('input');
        current_tab.find('#tax-final').val(result).trigger('input');
    });

    $('#land-and-building').on('change', '#from-asset', function(e){
        const current_tab = $(this).closest('div .tab');
        const country = current_tab.find('#from-asset option:selected').attr('data-country');
        const asset = current_tab.find('#from-asset option:selected').attr('data-asset');
        current_tab.find('input[id="country_id"]').val(country);
        current_tab.find('input[id="asset_id"]').val(asset);

    });
    
//#endregion

//#region devidend from investation (additional income)
    $('#dividend-from-investation').on('change', '#deviden-netto', function(e){
        const current_tab = $(this).closest('div .tab');
        
        let netto = parseFloat(current_tab.find('.deviden-netto').cleanVal())
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;
        let gross = 0;
        let tara = 0;

        if (tarif > 0 ){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#deviden-bruto').val(gross).trigger('input');
        current_tab.find('#tax-final').val(tara).trigger('input');
    });

    $('#dividend-from-investation').on('change', '#from-stock', function (e){
        const current_tab = $(this).closest('div .tab');
        const country = current_tab.find('#from-stock option:selected').attr('data-country');
        const asset = current_tab.find('#from-stock option:selected').attr('data-asset');
        current_tab.find('input[id="country_id"]').val(country);
        current_tab.find('input[id="asset_id"]').val(asset);
    });
//#endregion

//#region commanditaire profit member
    $('#profit-member').on('change', '#user-asset', function(e){
        const current_tab = $(this).closest('div .tab');
        const country = current_tab.find('#user-asset option:selected').attr('data-country');
        const asset = current_tab.find('#user-asset option:selected').attr('data-asset');
        current_tab.find('input[id="country_id"]').val(country);
        current_tab.find('input[id="asset_id"]').val(asset);
    });
//#endregion

//#region additional income sbi sbn
    $('#discount-rate').on('change', '#net-value', function (e){
        const current_tab = $(this).closest('div  .tab');
        let netto = parseFloat(current_tab.find('.net-value').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let gross = 0;
        let tara = 0;

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;

        if (tarif > 0){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
        current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');

    });
//#endregion

//#region additional income obligasi
    $('#discount-rate-bond').on('change', '#net-value', function (e){
        const current_tab = $(this).closest('div  .tab');
        let netto = parseFloat(current_tab.find('.net-value').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let gross = 0;
        let tara = 0;

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;

        if (tarif > 0){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
        current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');

    });
//#endregion

//#region additioal income sale of shares
    $('#sale-of-shares').on('change', '#net-value', function (e){
        const current_tab = $(this).closest('div  .tab');
        let netto = parseFloat(current_tab.find('.net-value').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let gross = 0;
        let tara = 0;

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;

        if (tarif > 0){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
        current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');
    });

    $('#sale-of-shares').on('change', '#founder', function (e){
        const current_tab = $(this).closest('div  .tab');
        const founder = current_tab.find('input[id="founder"]:checked').val();
        if (founder == 'Ya'){
            current_tab.find('#tax_rate').val('0.005');
            current_tab.find('#tarif').val('0.5%');
        }else{
            current_tab.find('#tax_rate').val('0.001');
            current_tab.find('#tarif').val("0.1%");
        }

        let netto = parseFloat(current_tab.find('.net-value').cleanVal());
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
        let gross = 0;
        let tara = 0;

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;

        if (tarif > 0){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
        current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');
    });
//#endregion

//#region additional income intellectual property
    $('#intellectual-property').on('change', '#net-value', function(e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 'Yes'){
            let netto = parseFloat(current_tab.find('.net-value').cleanVal());
            let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
            let gross = 0;
            let tara = 0;

            if (isNaN(netto)) netto = 0;
            if (isNaN(tarif)) tarif = 0;

            if (tarif > 0){
                gross = Math.floor(netto / tarif);
                tara = gross - netto;
            }

            current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
            current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');
        }
    });

    $('#intellectual-property').on('change', '#taxes', function (e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 'Yes'){
            current_tab.find('#net-value').prop('readonly', false);
            current_tab.find('#gross-value').prop('readonly', true);
            current_tab.find('#withholder-name').prop('readonly', false);
            current_tab.find('#withholder-npwp').prop('readonly', false);
            current_tab.find('#withholding-slip-number').prop('readonly', false);
            current_tab.find('#withholding-slip-date').prop('readonly', false);

            current_tab.find('#gross-value').val('');
        }else{
            
            current_tab.find('#net-value').prop('readonly', true);
            current_tab.find('#gross-value').prop('readonly', false);
            current_tab.find('#withholder-name').prop('readonly', true);
            current_tab.find('#withholder-npwp').prop('readonly', true);
            current_tab.find('#withholding-slip-number').prop('readonly', true);
            current_tab.find('#withholding-slip-date').prop('readonly', true);

            current_tab.find('#net-value').val('');
            current_tab.find('#withholder-name').val('');
            current_tab.find('#withholder-npwp').val('');
            current_tab.find('#withholding-slip-number').val('');
            current_tab.find('#withholding-slip-date').val('');
            current_tab.find('#tax-final').val('');
        }
    });
//#endregion

//#region additional income sewa asset dari luar tanah dan bangunan
    $('#lease-asset-outside-buildings').on('change', '#net-value', function(e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 'Yes'){
            let netto = parseFloat(current_tab.find('.net-value').cleanVal());
            let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
            let gross = 0;
            let tara = 0;

            if (isNaN(netto)) netto = 0;
            if (isNaN(tarif)) tarif = 0;

            if (tarif > 0){
                gross = Math.floor(netto / tarif);
                tara = gross - netto;
            }

            current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
            current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');
        }
    });

    $('#lease-asset-outside-buildings').on('change', '#taxes', function (e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 'Yes'){
            current_tab.find('#net-value').prop('readonly', false);
            current_tab.find('#gross-value').prop('readonly', true);
            current_tab.find('#withholder-name').prop('readonly', false);
            current_tab.find('#withholder-npwp').prop('readonly', false);
            current_tab.find('#withholding-slip-number').prop('readonly', false);
            current_tab.find('#withholding-slip-date').prop('readonly', false);

            current_tab.find('#gross-value').val('');
        }else{
            
            current_tab.find('#net-value').prop('readonly', true);
            current_tab.find('#gross-value').prop('readonly', false);
            current_tab.find('#withholder-name').prop('readonly', true);
            current_tab.find('#withholder-npwp').prop('readonly', true);
            current_tab.find('#withholding-slip-number').prop('readonly', true);
            current_tab.find('#withholding-slip-date').prop('readonly', true);

            current_tab.find('#net-value').val('');
            current_tab.find('#withholder-name').val('');
            current_tab.find('#withholder-npwp').val('');
            current_tab.find('#withholding-slip-number').val('');
            current_tab.find('#withholding-slip-date').val('');
            current_tab.find('#tax-final').val('');
        }
    });
//#endregion

//#region additional income bunga dari pembayaran hutang
    $('#interest-from-debt').on('change', '#net-value', function(e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 'Yes'){
            let netto = parseFloat(current_tab.find('.net-value').cleanVal());
            let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());
            let gross = 0;
            let tara = 0;

            if (isNaN(netto)) netto = 0;
            if (isNaN(tarif)) tarif = 0;

            if (tarif > 0){
                gross = Math.floor(netto / tarif);
                tara = gross - netto;
            }

            current_tab.find('#tax-final').val(Math.round(tara)).trigger('input');
            current_tab.find('#gross-value').val(Math.round(gross)).trigger('input');
        }
    });

    $('#interest-from-debt').on('change', '#pasal-pph', function (e){
        const current_tab = $(this).closest('div .tab');
        const pph = current_tab.find('#pasal-pph option:selected').val();
        current_tab.find('#pasal-pph-value').val(pph);
    });

    $('#interest-from-debt').on('change', '#taxes', function (e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 'Yes'){
            current_tab.find('#net-value').prop('readonly', false);
            current_tab.find('#gross-value').prop('readonly', true);
            current_tab.find('#withholder-name').prop('readonly', false);
            current_tab.find('#withholder-npwp').prop('readonly', false);
            current_tab.find('#withholding-slip-number').prop('readonly', false);
            current_tab.find('#withholding-slip-date').prop('readonly', false);

            current_tab.find('#pasal-pph').prop('disabled', false);

            current_tab.find('#gross-value').val('');
        }else{
            
            current_tab.find('#net-value').prop('readonly', true);
            current_tab.find('#gross-value').prop('readonly', false);
            current_tab.find('#withholder-name').prop('readonly', true);
            current_tab.find('#withholder-npwp').prop('readonly', true);
            current_tab.find('#withholding-slip-number').prop('readonly', true);
            current_tab.find('#withholding-slip-date').prop('readonly', true);
            current_tab.find('#pasal-pph').prop('readonly', false);

            current_tab.find('#net-value').val('');
            current_tab.find('#withholder-name').val('');
            current_tab.find('#withholder-npwp').val('');
            current_tab.find('#withholding-slip-number').val('');
            current_tab.find('#withholding-slip-date').val('');
            current_tab.find('#tax-final').val('');

            current_tab.find('#pasal-pph').val('');
            current_tab.find('#pasal-pph-value').val('');
            current_tab.find('#pasal-pph').prop('disabled', true);
        }
    });
//#endregion

//#region additional income imbalan kepada tenaga ahli
    $('#rewards-to-experts').on('change', '#gross-value', function(e){
        const current_tab = $(this).closest('div .tab');

        let gross = parseFloat((current_tab.find('#gross-value').val() == '') ? 0 : current_tab.find('.gross-value').cleanVal());
        let pph_final = parseFloat((current_tab.find('#tax-final').val() == '') ? 0 : current_tab.find('.tax-final').cleanVal());

        if (isNaN(gross)) gross = 0;
        if (isNaN(pph_final)) pph_final = 0;
        
        const netto = gross - pph_final;

        current_tab.find('#net-value').val(netto).trigger('input');
        
    });

    $('#rewards-to-experts').on('change', '#tax-final', function(e){
        const current_tab = $(this).closest('div .tab');

        let gross = parseFloat((current_tab.find('#gross-value').val() == '') ? 0 : current_tab.find('.gross-value').cleanVal());
        let pph_final = parseFloat((current_tab.find('#tax-final').val() == '') ? 0 : current_tab.find('.tax-final').cleanVal());

        if (isNaN(gross)) gross = 0;
        if (isNaN(pph_final)) pph_final = 0;
        
        const netto = gross - pph_final;

        current_tab.find('#net-value').val(netto).trigger('input');
        
    });

    

//#endregion

//#region additional income other regular income
    $('#other-regular-income').on('change', '#taxes', function (e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 'Yes'){
            current_tab.find('#net-value').prop('readonly', false);
            current_tab.find('#gross-value').prop('readonly', true);
            current_tab.find('#withholder-name').prop('readonly', false);
            current_tab.find('#withholder-npwp').prop('readonly', false);
            current_tab.find('#withholding-slip-number').prop('readonly', false);
            current_tab.find('#withholding-slip-date').prop('readonly', false);
            current_tab.find('#tax-final').prop('readonly', false);

            current_tab.find('#pasal-pph').prop('disabled', false);

            current_tab.find('#gross-value').val('');
        }else{
            
            current_tab.find('#net-value').prop('readonly', true);
            current_tab.find('#gross-value').prop('readonly', false);
            current_tab.find('#withholder-name').prop('readonly', true);
            current_tab.find('#withholder-npwp').prop('readonly', true);
            current_tab.find('#withholding-slip-number').prop('readonly', true);
            current_tab.find('#withholding-slip-date').prop('readonly', true);
            current_tab.find('#pasal-pph').prop('readonly', false);
            current_tab.find('#tax-final').prop('readonly', true);

            current_tab.find('#net-value').val('');
            current_tab.find('#withholder-name').val('');
            current_tab.find('#withholder-npwp').val('');
            current_tab.find('#withholding-slip-number').val('');
            current_tab.find('#withholding-slip-date').val('');
            current_tab.find('#tax-final').val('');

            current_tab.find('#pasal-pph').val('');
            current_tab.find('#pasal-pph-value').val('');
            current_tab.find('#pasal-pph').prop('disabled', true);
        }
    });

    $('#other-regular-income').on('change', '#net-value', function(e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 'Yes'){
            let pph_final = parseFloat((current_tab.find('#tax-final').val() == '') ? 0 : current_tab.find('.tax-final').cleanVal());
            let netto = ((current_tab.find('#net-value').val() == '') ? 0 : current_tab.find('.net-value').cleanVal());
            if (isNaN(netto)) gross = 0;
            if (isNaN(pph_final)) pph_final = 0;
            
            const gross = parseFloat(netto) + parseFloat(pph_final);

            current_tab.find('#gross-value').val(gross).trigger('input');
        }
    });

    $('#other-regular-income').on('change', '#tax-final', function(e){
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 'Yes'){
            let pph_final = parseFloat((current_tab.find('#tax-final').val() == '') ? 0 : current_tab.find('.tax-final').cleanVal());
            let netto = ((current_tab.find('#net-value').val() == '') ? 0 : current_tab.find('.net-value').cleanVal());
            if (isNaN(netto)) gross = 0;
            if (isNaN(pph_final)) pph_final = 0;
            
            const gross = parseFloat(netto) + parseFloat(pph_final);

            current_tab.find('#gross-value').val(gross).trigger('input');
        }
    });

    $('#other-regular-income').on('change', '#pasal-pph', function (e){
        const current_tab = $(this).closest('div .tab');
        const pph = current_tab.find('#pasal-pph option:selected').val();
        current_tab.find('#pasal-pph-value').val(pph);
    });
//#endregion

//#region another once earning income
    $('#another-earning-form').on('change', '#taxes', function (e) {
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 2) {
            current_tab.find('#another-earning-netto').prop('readonly', false);
            current_tab.find('#another-earning-brutto').prop('readonly', true);
            current_tab.find('#another-earning-cutter-name').prop('readonly', false);
            current_tab.find('#another-earning-cutter-npwp').prop('readonly', false);
            current_tab.find('#another-earning-pph-proof').prop('readonly', false);
            current_tab.find('#another-earning-pph-date').prop('readonly', false);
            current_tab.find('#another-earning-pph-taken').prop('readonly', false);

            current_tab.find('#pasal-pph').prop('disabled', false);

            current_tab.find('#another-earning-brutto').val('');
        }else{
            current_tab.find('#another-earning-netto').prop('readonly', true);
            current_tab.find('#another-earning-brutto').prop('readonly', false);
            current_tab.find('#another-earning-cutter-name').prop('readonly', true);
            current_tab.find('#another-earning-cutter-npwp').prop('readonly', true);
            current_tab.find('#another-earning-pph-proof').prop('readonly', true);
            current_tab.find('#another-earning-pph-date').prop('readonly', true);
            current_tab.find('#pasal-pph').prop('readonly', false);
            current_tab.find('#another-earning-pph-taken').prop('readonly', true);

            current_tab.find('#another-earning-netto').val('');
            current_tab.find('#another-earning-cutter-name').val('');
            current_tab.find('#another-earning-cutter-npwp').val('');
            current_tab.find('#another-earning-pph-proof').val('');
            current_tab.find('#another-earning-pph-date').val('');
            current_tab.find('#another-earning-pph-taken').val('');

            current_tab.find('#pasal-pph').val('');
            current_tab.find('#pasal-pph-value').val('');
            current_tab.find('#pasal-pph').prop('disabled', true);
        }
    });

    $('#another-earning-form').on('change', '#pasal-pph', function(e) {
        const current_tab = $(this).closest('div .tab');
        const pph = current_tab.find('#pasal-pph option:selected').val();
        current_tab.find('#pasal-pph-value').val(pph);
    });

    $('#another-earning-form').on('change', '#another-earning-pph-taken', function (e) {
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();

        if (taxes == 2) {
            let pph_final = parseFloat((current_tab.find('#another-earning-pph-taken').val() == '') ? 0 : current_tab.find('.another-earning-pph-taken').cleanVal());
            let netto = ((current_tab.find('#another-earning-netto').val() == '') ? 0 : current_tab.find('.another-earning-netto').cleanVal());

            if (isNaN(netto)) gross = 0;
            if (isNaN(pph_final)) pph_final = 0;
            const gross = parseFloat(netto) + parseFloat(pph_final);
            current_tab.find('#another-earning-brutto').val(gross).trigger('input');
        }
    });

    $('#another-earning-form').on('change', '#another-earning-netto', function(e) {
        const current_tab = $(this).closest('div  .tab');
        const taxes = current_tab.find('input[id="taxes"]:checked').val();
        if (taxes == 2) {
            let pph_final = parseFloat((current_tab.find('#another-earning-pph-taken').val() == '') ? 0 : current_tab.find('.another-earning-pph-taken').cleanVal());
            let netto = ((current_tab.find('#another-earning-netto').val() == '') ? 0 : current_tab.find('.another-earning-netto').cleanVal());
            if (isNaN(netto)) gross = 0;
            if (isNaN(pph_final)) pph_final = 0;
            const gross = parseFloat(netto) + parseFloat(pph_final);
            current_tab.find('#another-earning-brutto').val(gross).trigger('input');
        }
    });

//#endregion

//#region bukti potong atas pembelian barang mewah
    $('#luxury-goods').on('change', '.income-name', function(){
        const current_tab = $(this).closest('div .tab');
        const tax_rate = parseFloat(current_tab.find('.income-name option:selected').attr('data-taxrate'));
        const tarif = (tax_rate * 100).toString() + "%";

        current_tab.find('#tarif').val(tarif);
        current_tab.find('#tax_rate').val(tax_rate.toString());
    });

    $('#luxury-goods').on('change', '#ppnbm', function(){
        const current_tab = $(this).closest('div .tab');
        let tax_rate = parseFloat(current_tab.find('#tax_rate').val());
        let gross = parseFloat(current_tab.find('#ppnbm').cleanVal());

        if (isNaN(tax_rate)) tax_rate = 0;
        if (isNaN(gross)) gross = 0;

        const netto = gross * tax_rate;
        current_tab.find('#tax-final').val(netto).trigger('input');
        
    });

//#endregion

    ////////////////////////TA//////////////////////////
    $('#asset_category_select').on('change', function(value){

        var params = $(this).val().split("|");
        
        var api_url = "/api/user-assets/"+params[1]+"/category/"+params[0];

        if(params[0] !== '0'){
            $.ajax({
                url: api_url,
              })
                .done(function( value ) {
                    $('#table_assets-container').html(value.html);
                });
        }
       
    });

    $('#asset_rep_category_select').on('change', function(value){
        
        $('#table_assets_repatriation-container').hide();
        $( "#table_repatriation_body").empty();
        var params = $(this).val().split("|");
        
        var api_url = "/api/user-assets-repatriation/"+params[1]+"/category/"+params[0];

        if(params[0] !== '0'){
            $.ajax({
                url: api_url,
              })
                .done(function( value ) {
                    $('#table_assets-container').html(value.html);
                    $('.input-money-format').mask();
                });
        }
       
    });


    $("#ta_revealed").submit(function( event ) {
        event.preventDefault();
        var revealed_assets_temp = [];
        var trs = document.querySelectorAll('#table_assets tr');
        var index = 0;
       
        trs.forEach(function(tr,i) {
            if(i !== 0){
                var is_checked = tr.querySelector('input[type="checkbox"]').checked ;
                if(is_checked){
                    revealed_assets_temp[index] = {
                        user_id : parseInt((tr.querySelectorAll('td')[8].innerText).replace(/[^0-9\.]+/g, '')),
                        asset_id : parseInt((tr.querySelectorAll('td')[7].innerText).replace(/[^0-9\.]+/g, '')),
                        currency : parseInt((tr.querySelectorAll('td')[9].innerText).replace(/[^0-9\.]+/g, '')),
                        value : parseInt((tr.querySelectorAll('td')[5].innerText).replace(/[^0-9\.]+/g, '')),
                    };
                    // revealed_assets_temp[i]['user_id'] = parseInt((tr.querySelectorAll('td')[7].innerText).replace(/[^0-9\.]+/g, ''));
                    // revealed_assets_temp[i]['asset_id'] = parseInt((tr.querySelectorAll('td')[8].innerText).replace(/[^0-9\.]+/g, ''));
                    // revealed_assets_temp[i]['currency'] = parseInt((tr.querySelectorAll('td')[9].innerText).replace(/[^0-9\.]+/g, ''));
                    // revealed_assets_temp[i]['value'] = parseInt((tr.querySelectorAll('td')[5].innerText).replace(/[^0-9\.]+/g, ''));
                    index++
                }
            }
        });
       
        $('#revealed_params').val(JSON.stringify(revealed_assets_temp));
       
        $(this).unbind('submit').submit();
    });

    $("#ta_repatriation").submit(function( event ) {
        event.preventDefault();
        $('.input-money-format').unmask();
        $(this).unbind('submit').submit();
    });
    
    $("#additional-earning-step").submit(function( event ) {
        event.preventDefault();
        $(this).unbind('submit').submit();
    });

    $('form[id^="hutangDelete"]').on('submit', function(e) {
        e.preventDefault();
        console.log($(this).attr('action'))
        console.log($(this).attr('method'))
        console.log($(this).serialize());
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize()
        })
            .done(function (value) {
                if (value.status == true) {
                    location.reload(1)
                } else {
                    alert('something went wrong')
                }
            });
    })

    $('form[id^="assetDelete"]').on('submit', function (e) {
        e.preventDefault();
        console.log($(this).attr('action'))
        console.log($(this).attr('method'))
        console.log($(this).serialize());
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize()
        })
            .done(function (value) {
                if (value.status == true) {
                    location.reload(1)
                } else {
                    alert('something went wrong')
                }
            });
    })

    $('#continuelater').submit(function(e) {
        e.preventDefault();
        var data = $('#formdatadiri, #continuelater').serialize();
        $('#continuelater').append('<div style="display:none;" id="tambahan"></div>')
        $('#formdatadiri').children().clone().appendTo('#continuelater #tambahan')
        $(this).unbind('submit').submit();
    })
}