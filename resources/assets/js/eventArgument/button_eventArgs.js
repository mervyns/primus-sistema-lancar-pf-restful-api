
import { calculationBtnClickHandler } from "../modalBox/taxCalculationTable";
//#region acquicition years
const changeAcquisitionYear = function (e) {
    const current_tab = $(this).closest('div .tab');
    const currency = current_tab.find('#currency option:selected').text();
    const acquisition_year = parseInt(current_tab.find('#acquisition-year').val());
    const currentTime = new Date();
    const prev_year = currentTime.getFullYear() - 1;

    if (acquisition_year < prev_year && currency != 'IDR'){
        swal('Ups!', 'Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya', 'warning');
        current_tab.find('#currency').val(1).trigger('change');
    }
};

const changeAcquisitionYear_eventHandler = function ($){
    $('input[id^="acquisition-year"]').on('change', changeAcquisitionYear);
};

//#endregion

//#region tab events arguments
const createTab = function (e) {
    e.preventDefault();

    const current_form = $(this).closest('form');
    const template_name = '#' + current_form.attr('id') + '-form-template';
    const ai = parseInt(current_form.find('#add-tab').attr('data-ai')) + 1;

    const tab_content = 'tab' + ai.toString();
    const tab_header = 'li' + ai.toString();

    current_form.find('#tabs')
        .append('<li id="' + tab_header + '"><a href="#' + tab_content + '"># ' + ai.toString() + '</a></li>');
    current_form.find('#tab-content')
        .append('<div id="' + tab_content + '" class="tab"> </div>');
    $(template_name).clone().removeAttr('id').show().appendTo('#' + tab_content);
    current_form.find('#add-tab').attr('data-ai', ai.toString());

    const current_tab = current_form.find('div#' + tab_content);
    current_tab.find('#remove-tab').attr('data-ai', ai.toString());
    current_tab.find('#remove-tab').bind('click', removeTab);

    // masked for input on the new tab
    current_tab.find('.input-money-format').mask('#.##0', {reverse: true});
    current_tab.find('.input-kurs-format').mask('#.##0', {reverse: true});
    current_tab.find('.input-npwp-format').mask('##.###.###.#-###.###');
    current_tab.find('.input-picker-format').datetimepicker({format: 'DD/MM/YYYY'});
    
    // indexing radio button if on the new tab have radio button
    const radio_index = ai - 1;
    if (current_form.attr('id') == 'bank'){
        current_tab.find('#bank-type').attr('name', 'bank-type[' + radio_index.toString() + ']');
        current_tab.find('a.btn-calculation-modal').bind('click', calculationBtnClickHandler);

        current_tab.find('#bank-name').addClass('bank-name');
        current_tab.find('#bank-name').attr('id', 'bank-name-'+ai.toString());
        $('.bank-name').select2();
    }
    if (current_form.attr('id') == 'deposit'){
        current_tab.find('#deposit-type').attr('name', 'deposit-type[' + radio_index.toString() + ']');
        current_tab.find('#rollover').attr('name', 'rollover[' + radio_index.toString() + ']');

        current_tab.find('#bank-name').addClass('bank-name');
        current_tab.find('#bank-name').attr('id', 'bank-name-'+ai.toString());
        $('.bank-name').select2();
    }
    if (current_form.attr('id') == 'credit-card'){
        current_tab.find('#bank-name').addClass('bank-name');
        current_tab.find('#bank-name').attr('id', 'bank-name-'+ai.toString());
        current_tab.find('#bank-name-'+ai.toString()).attr('select-id', ai.toString());

        $('form#credit-card .tab select').on('change',function(e){
            var select2_value = (e.currentTarget.value)
            var select2_id = (e.currentTarget.attributes[5].value)
            var bank_name = document.querySelectorAll('#bank-name-'+select2_id+' option[value="'+select2_value+'"]')[0].innerText
            $('form#credit-card #tab'+select2_id).append('<input type="hidden" name="select_value['+select2_id+']" value="'+bank_name+'">');
        });

        $('.bank-name').select2();
    }
    if (current_form.attr('id') == 'vehicle'){
        current_tab.find('#vehicle-credit').attr('name', 'vehicle-credit[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'property'){
        current_tab.find('#property-credit').attr('name', 'property-credit[' + ai.toString() + ']');
    }
    if (current_form.find('id') == 'property-liability'){
        current_tab.find('#bank-name').addClass('bank-name');
        current_tab.find('#bank-name').attr('id', 'bank-name-'+ai.toString());
        $('.bank-name').select2();
    }
    if (current_form.attr('id') == 'land-and-building'){
        current_tab.find('#penanggung').attr('name', 'penanggung[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'income-deposit'){
        current_tab.find('#deposit-type').attr('name', 'deposit-type[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'sale-of-shares'){
        current_tab.find('#founder').attr('name', 'founder[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'intellectual-property'){
        current_tab.find('#taxes').attr('name', 'taxes[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'lease-asset-outside-buildings'){
        current_tab.find('#taxes').attr('name', 'taxes[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'interest-from-debt'){
        current_tab.find('#taxes').attr('name', 'taxes[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'other-regular-income'){
        current_tab.find('#taxes').attr('name', 'taxes[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'another-earning-form') {
        current_tab.find('#taxes').attr('name', 'type[' + radio_index.toString() + ']');
    }
    if (current_form.attr('id') == 'charity'){
        current_tab.find('#charity-name').addClass('charity-name');
        current_tab.find('#charity-name').attr('id', 'charity-name-'+ai.toString());
        $('.charity-name').select2();
    }
    if (current_form.attr('id') == 'luxury-goods'){
        current_tab.find('#income-name').addClass('income-name');
        current_tab.find('#income-name').attr('id', 'income-name-'+ai.toString());
        $('.income-name').select2();
    }

    if (current_tab.find('select#currency').children().length > 0){
        current_tab.find('select#currency').bind('change', taxRateValue);
    }

    current_tab.find('.balance').bind('change', taxRateValue);
    current_tab.find('#country').bind('change', countryOnChange);
    current_tab.find('.bank-name').bind('change', bankNameOnChange);
    current_tab.find('.charity-name').bind('change', charityNameOnChange);
    current_tab.find('.preview-image').bind('click', previewImageOnClick);
    current_tab.find('.company-reksadana').autocomplete({
        source: function (request, response) {
            autocompleteReksadana(request, response);
        }
    });
};

const removeTab = function (e) {
    e.preventDefault();
    
    const current_form = $(this).closest('form');
    const ai = parseInt($(this).attr('data-ai'));
    const child = current_form.find('ul#tabs').children();
    const child_length = parseInt(child.length);
    
    if (isNaN(child_length) || child_length <= 1){
        swal("Ups!", "Minimal harus berisi 1 data sebagai data utama.","warning");
    }else{
        $.each (child, function (index, value) {
            const current_tab = $(value).closest('li');
            
            if (current_tab.attr('id') == ('li' + ai.toString())){
                let key = index - 1;
                if (index == 0){
                    key = index + 1;
                }

                const deleting_id = parseInt(current_form.find(current_tab.find('a').attr('href')).find('input#id').val());
                if (!isNaN(deleting_id)){
                    current_form.find('div#deleted').append('<input type="hidden" name="deleted_id[]" value="'+deleting_id.toString()+'">')
                }

                 //remove tab page and tab content
                 current_form.find(current_tab.find('a').attr('href')).remove();
                 current_tab.remove();

                //set active tab page and tab content
                const current_content = $(child[key]).closest('li');
                current_content.addClass('active');
                current_form.find(current_content.find('a').attr('href')).show().siblings().hide();

                return false; 
            }
        });
    }
};

const createTab_eventHandler = function($) {
    $('#add-tab').on('click', createTab);
};

const removeTab_eventHandler = function ($) {
    $('a[id^="remove-tab"]').on('click', removeTab);
};

const tabClick_eventHandler = function ($) {
    $('.main-form').on('click', '.tabs .tab-links a', function(e) {
            e.preventDefault();
            var currentAttrValue = jQuery(this).attr('href');
            jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
            jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
    });
};

//#endregion

//#region kurs pajak
const taxRate = function (currency){
    const currentTime = new Date();
        const prevYear = currentTime.getFullYear() - 1;
        const prevMonth = '12';
        const prevDate = '31';
        const url = '/api/kurspajak';

        return jQuery.parseJSON($.ajax({
            type : 'GET',
            url : url,
            data : {
                currency : currency,
                year : prevYear,
                month : prevMonth,
                date : prevDate
            },
            async: false
        }).responseText);

};


const taxRateValue = function (e){
    const current_tab = $(this).closest('div .tab');
    let kurs_value = 1;
    const currency = current_tab.find('#currency option:selected').text();

    if (current_tab.find('#acquisition-year').length > 0){
        const acquisition_year = parseInt(current_tab.find('#acquisition-year').val());
        const currentTime = new Date();
        const prev_year = currentTime.getFullYear() - 1;
        // if (acquisition_year < prev_year && currency != 'IDR'){
        //     swal('Ups!', 'Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya', 'warning');
        //     current_tab.find('#currency').val(1);
        // }
    }

    if (currency != 'IDR'){
        kurs_value = parseFloat(taxRate(currency).kurspajak_value);
    }

    let balance = parseFloat(current_tab.find('input.balance').cleanVal());
    if (isNaN(balance)){
        balance = '0';
    }

    let kurs_result = (kurs_value * balance).toFixed(2);

    current_tab.find('#kurs_pajak').val(Math.round(kurs_value)).trigger('input');
    current_tab.find('#jumlah_rupiah').val(Math.round(kurs_result)).trigger('input');

    //for income from deposit (additional income)
    const current_form = $(this).closest('form');
    if (current_form.attr('id') == 'income-deposit'){
        let netto = parseFloat(Math.round(kurs_result));
        let tarif = 1 - parseFloat(current_tab.find('#tax_rate').val());

        if (isNaN(netto)) netto = 0;
        if (isNaN(tarif)) tarif = 0;

        let gross = 0;
        let tara = 0;

        if (tarif > 0 ){
            gross = Math.floor(netto / tarif);
            tara = gross - netto;
        }

        current_tab.find('#gross-value').val(gross).trigger('input');
        current_tab.find('#tax-final').val(tara).trigger('input');
    }
}

const countryOnChange = function (e) {
    const current_tab = $(this).closest('div .tab');
    const value = current_tab.find('#country').val();
    current_tab.find('#currency').val(value).trigger('change');
};

const changeCurrency_eventHandler = function ($) {
    $('select[id^="currency"]').on('change', taxRateValue);
};

const changeBalance_eventHandler = function ($) {
    $('input.balance').on('change', taxRateValue);
};

const changeCountry_eventHandler = function ($) {
    $('select[id^="country"]').on('change', countryOnChange);
};

//#endregion

//#region dropdown bank/koperasi/leasing

const bankNameOnChange = function (e){
    const current_tab = $(this).closest('div .tab');
    const value = current_tab.find('.bank-name option:selected').val();

    if (value != -1){
        current_tab.find('input#other-bank').val('');
        current_tab.find('input#other-bank').hide();
    }else{
        current_tab.find('input#other-bank').show();
    }

};

const changeBankName_eventHandler = function ($) {
    $('select.bank-name').on('change', bankNameOnChange);
};

//#endregion

//#region charity change
const charityNameOnChange = function (e){
    const current_tab = $(this).closest('div .tab');
    const value = current_tab.find('.charity-name option:selected').val();

    if (value != -1){
        current_tab.find('input#other-charity').val('');
        current_tab.find('input#other-charity').hide();
    }else{
        current_tab.find('input#other-charity').show();
    }
};

const changeCharityName_eventHandler = function ($){
    $('select.charity-name').on('change', charityNameOnChange);
}

//#endregion

//#region button preview image 
const previewImageOnClick = function (e){
    const image_location = $(this).attr('data-image');
    const title = $(this).attr('data-title');
    $('#display-image').prop('src', image_location);
    $('#caption-preview-image').text(title);
    $('#modal_preview_image').modal('show');
};

const clickPreviewImage_eventHandler = function ($){
    $('.preview-image').on('click', previewImageOnClick);
};

const previewImageOnClosing = function(e){
    $('#display-image').prop('src', '');
    $('#caption-preview-image').text('');
};

const closePreviewImage_eventHandler = function ($){
    $('#modal_preview_image').on('hidden.bs.modal', previewImageOnClosing);
};

//#endregion

//#region button popup information step
const popupInformationOnClick = function (e){
    const message = $(this).attr('data-message');
    const title = $(this).attr('data-title');
    if (message){
        swal(title, message, 'info');
    }
};

const clickPopupInformation_eventHandler = function ($){
    $('.btn-popup-information').on('click', popupInformationOnClick);
};
//#endregion

//#region autocomplete reksadana
const autocompleteReksadana = function (request, response) {    
    $.ajax({
        url: '/api/companies/reksadana',
        type: 'get',
        data: {term: request.term},
        success: function (data) {
            var push_data = [];
            $.each(data.companies, function (index, value) {
                var item = {};
                item['value'] = value.name;
                item['label'] = value.label; 
                push_data.push(item);
            });
            response(push_data);
        }
    });
}

const autocompleteReksadana_eventHandler = function($) {
    $('.company-reksadana').autocomplete({
        source: function (request, response) {
            autocompleteReksadana(request, response);
        }
    });
}
//#endregion

//#region autocomplete sekuritas
const autocompleteSekuritas = function (request, response) {
    $.ajax({
        url: '/api/companies/sekuritas',
        type: 'get',
        data: {term: request.term},
        success: function (data) {
            var push_data = [];
            $.each(data.companies, function (index, value) {
                var item = {};
                item['value'] = value.name;
                item['label'] = value.label; 
                push_data.push(item);
            });
            response(push_data);
        }
    });
}

const autocompleteSekuritas_eventHandler = function ($) {
    $('.company-sekuritas').autocomplete({
        source: function (request, response) {
            autocompleteSekuritas(request, response);
        }
    });
}
//#endregion

export const initTable = function($) {
    createTab_eventHandler($);
    removeTab_eventHandler($);
    tabClick_eventHandler($);
    changeCurrency_eventHandler($);
    changeBalance_eventHandler($);
    changeCountry_eventHandler($);
    changeBankName_eventHandler($);
    changeCharityName_eventHandler($);
    changeAcquisitionYear_eventHandler($);
    clickPreviewImage_eventHandler($);
    closePreviewImage_eventHandler($);
    clickPopupInformation_eventHandler($);
    autocompleteReksadana_eventHandler($);
    autocompleteSekuritas_eventHandler($);
}
export default {
    initTable,
};
