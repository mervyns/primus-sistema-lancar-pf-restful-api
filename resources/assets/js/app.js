import 'jquery-validation';
import 'jquery-ui';
import 'jquery-ui/ui/widgets/autocomplete';
import inputInit from './input';
import buttonInit from './button';
import onloadInit from './onload';
import registrationValidation from './validation/registration';
import { initTable as initInterestTaxTable } from './modalBox/taxCalculationTable';
import {checkboxListener} from './input/ta-repatriation'
import { initTable as buttonEventArg } from './eventArgument/button_eventArgs';

const App = function($){
    $(window).on('load', function () {
        inputInit($);
        buttonInit($);
        onloadInit($);
        registrationValidation($);
        initInterestTaxTable($);
        checkboxListener($);
        buttonEventArg($);
    });
};

export default App(jQuery);