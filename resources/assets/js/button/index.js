export default function($) {

    const setBtnGroupEnabled = function (groupId) {
        $('.btn-group-single-select-'+groupId+' button')
            .removeAttr('disabled')
            .removeClass('btn-outline-default')
            .addClass('btn-outline-info');
    };
    const setBtnGroupDisabled = function(groupId) {
        $('.btn-group-single-select-'+groupId+' button')
            .attr('disabled', 'disabled')
            .removeClass('btn-outline-info')
            .removeClass('active')
            .addClass('btn-outline-default');
        $('.btn-group-single-select-'+groupId+' input[type="hidden"]').val('');
    };
    const maritalStatus = $('input[name="status"]').val();

    switch (maritalStatus) {
        case '1':
            setBtnGroupDisabled(2);
            setBtnGroupDisabled(3);
            break;

        case '2':
            setBtnGroupEnabled(2);
            setBtnGroupEnabled(3);
            break;

        case '3':
            setBtnGroupDisabled(2);
            setBtnGroupDisabled(3);
            break;

        default:
            setBtnGroupDisabled(2);
            setBtnGroupDisabled(3);
            break;
    }

    const tax_amnesty = $('input[name="2016_ta_participation"]').val();
    switch (tax_amnesty){
        case '1' :
            setBtnGroupEnabled(6);
            break;
        default :
            setBtnGroupDisabled(6);
            break;
    }

    const removeTab = function (e) {

        e.preventDefault();
        
        const current_form = $(e.target).closest('form');
        const ai = parseInt($(e.target).attr('data-ai'));
        const child = current_form.find('ul#tabs').children();
        const child_length = parseInt(child.length);

        console.log(current_form)
        console.log(ai)
        console.log(child)

        if (isNaN(child_length) || child_length <= 1){
            swal("Ups!", "Minimal harus berisi 1 data sebagai data utama.","warning");
        }else{
            $.each (child, function (index, value) {
                const current_tab = $(value).closest('li');
                
                if (current_tab.attr('id') == ('li' + ai.toString())){
                    let key = index - 1;
                    if (index == 0){
                        key = index + 1;
                    }

                    const deleting_id = parseInt(current_form.find(current_tab.find('a').attr('href')).find('input#id').val());
                    if (!isNaN(deleting_id)){
                        current_form.find('div#deleted').append('<input type="hidden" name="deleted_id[]" value="'+deleting_id.toString()+'">')
                    }

                    //remove tab page and tab content
                    current_form.find(current_tab.find('a').attr('href')).remove();
                    current_tab.remove();

                    //set active tab page and tab content
                    const current_content = $(child[key]).closest('li');
                    current_content.addClass('active');
                    current_form.find(current_content.find('a').attr('href')).show().siblings().hide();

                    return false; 
                }
            });
        }
    };

     $('.remove').click(function () {
        $('.note').hide("puff");
        $('.nttoogle').show("puff");
     });

     $('.nttoogle').click(function () {
        $('.note').show("puff");
        $('.nttoogle').hide("puff");
     });

    $('.btn-group-single-select-1 button').click(function() {
        const val = $(this).data('value');

        $('input#spouse_has_tax_id').val('');
        $('input#spouse_taxing_status').val('');

        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#status').val('');
        } else {
            $(this).addClass('active');
            $('input#status').val(val);
            $('.btn-group-single-select-1 button').not(this).removeClass('active');
            if (val === 2){
                setBtnGroupEnabled(2);
                setBtnGroupEnabled(3);
            }else{
                setBtnGroupDisabled(2);
                setBtnGroupDisabled(3);
            }
        }
    });

    $('.btn-group-single-select-2 button').click(function() {
        const val = $(this).data('value');
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#spouse_has_tax_id').val('');
        } else {
            $(this).addClass('active');
            $('input#spouse_has_tax_id').val(val);
            $('.btn-group-single-select-2 button').not(this).removeClass('active');    
        }
        
        setBtnGroupDisabled('single-npwp');
        setBtnGroupDisabled('dual-npwp');
        if (val == 0){
            setBtnGroupEnabled('single-npwp');
        }else if (val == 1){
            setBtnGroupEnabled('dual-npwp');
        }
        $('input#spouse_taxing_status').val('');
    });

    $('.btn-group-single-select-3 button').click(function() {
        const val = $(this).data('value');
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#spouse_taxing_status').val('');
        } else {
            $(this).addClass('active');
            $('input#spouse_taxing_status').val(val);
            $('.btn-group-single-select-3 button').not(this).removeClass('active');
        }
    });

    $('.btn-group-single-select-4 button').click(function() {
        const val = $(this).data('value');
        $('input#pengungkapan').val('');
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#tax-amnesty').val('');
        } else {
            $(this).addClass('active');
            $('input#tax-amnesty').val(val);
            $('.btn-group-single-select-4 button').not(this).removeClass('active');
            if (val == '1'){
                setBtnGroupEnabled(6);
                $('.btn-group-single-select-6').show();
            }else{
                setBtnGroupDisabled(6);
                $('.btn-group-single-select-6').hide();
            }
        }
    });

    $('.btn-group-single-select-6 button').click(function() {
        const val = $(this).data('value');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#pengungkapan').val('');
        }else{
            $(this).addClass('active');
            $('input#pengungkapan').val(val);
            $('.btn-group-single-select-6 button').not(this).removeClass('active');
        }
    });

    $('.btn-group-single-select-7 button').click(function() {
        const val = $(this).data('value');
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('input#have_foreign_asset').val('');
        }else{
            $(this).addClass('active');
            $('input#have_foreign_asset').val(val);
            $('.btn-group-single-select-7 button').not(this).removeClass('active');
        }
    });

    $('.btn-group-multi-select button').click(function() {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).siblings('input').val('');
        } else {
            $(this).addClass('active');
            $(this).siblings('input').val($(this).data('value'));
        }
    });


    $('.btn-group-single-select-reporting button').click(function(){
        const val = $(this).data('value');
        if ($(this).hasClass('active')){
            $(this).removeClass('active');
            $('input#precious-asset').val('');
        }else{
            $(this).addClass('active');
            $('input#precious-asset').val(val);
            $('.btn-group-single-select-reporting button').not(this).removeClass('active');
        }
    });

    $('#user-product').on('click', '.btn-group-single-select-5 button', function(e){
        const url = window.location.host.toString() + '/product/invite';
        const protocol = window.location.protocol.toString();

        const value = $(this).data('value');
        const promo = $(this).data('promo');
        const promo_description = $(this).data('promo-description');
        if ($(this).hasClass('active')){
            $(this).removeClass('active');
            $('input#productid').val('');
            $('input#promo').val(0);
        }else{
            $(this).addClass('active');
            $('input#productid').val(value);
            $('input#promo').val(promo);
            $('.btn-group-single-select-5 button').not(this).removeClass('active');
            setBtnGroupDisabled(5);
            $.ajaxSetup({
                headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
            });
            $.ajax({
                url: '/product/check',
                type: 'post',
                dataType: 'json',
                success : function (status){
                    if (status == 1){
                        swal('Peringatan!', 'Anda diundang oleh teman anda. Jika anda ingin memilihh produk sendiri, anda bisa menghubungi teman anda untuk menghapus akun anda dari subscription. Atau anda bisa mengubungi kontak SPTGO.', 'warning');
                    }else if (status == 2){
                        swal('Peringatan!', 'Maaf, saat ini anda sudah berlangganan produk SPTGO.', 'warning');
                    }else{
                        $.ajax({
                            url: '/product/select',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                product_id: value,
                                promo: promo,
                                promo_description: promo_description
                            },
                            success : function (result){
                                if (result.status == true){
                                    window.location.href = protocol + '//' + url;
                                }else{
                                    console.log(result.message);
                                }
                            }
                        });
                    }
                }
            });
            
        }
        // const url = window.location.host.toString() + '/product/invite';
        // const protocol = window.location.protocol.toString();

        // const val = $(this).attr('data-value');
        // const promo = $(this).attr('data-promo');
        // const promo_description = $(this).attr('data-promo-description');

        // if ($(this).hasClass('active')){
        //     $(this).removeClass('active');
        //     $('input#productid').val('');
        //     $('input#promo').val(0);
        // }else{
        //     $(this).addClass('active');
        //     $('input#productid').val(val);
        //     $('input#promo').val(promo);
        //     $('.btn-group-single-select-5 button').not(this).removeClass('active');
        //     $.ajaxSetup({
        //         headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        //     });
        //     $.ajax({
        //         url : '/product/select',
        //         data : {
        //             product_id : val,
        //             promo : promo,
        //             promo_description : promo_description
        //         },
        //         type : 'post',
        //         dataType : 'json',
        //         success : function (result){
        //             if (result.status == true){
        //                 window.location.href = protocol + '//' + url;
        //             }
        //         }

        //     });
        // }
    });

    $('.remove-partner-subscription').click(function(){
        const id = $(this).attr('data-id');
        swal({
            title: "Konfirmasi Hapus",
            text: "Data yang dihapus tidak dapat dipulihkan kembali. Tetap lanjutkan ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete){
                $.ajaxSetup({
                    headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
                });
                $.ajax({
                    url: '/product/delete',
                    type: 'delete',
                    data: {user_product_id: id},
                    success : function (result){
                        if (result == true){
                            $('#email-subscription-' + id.toString()).remove();
                            swal('Sukses', 'Berhasil menghapus data.', 'info');
                        }else{
                            swal('Ups!', 'Gagal menghapus data.', 'warning');
                        }
                    }
                });
            }
        });
    });

    $('.change-product-sptgo').click(function(){
        $('#product-subscription').hide();
        $('#product-change').show();
    });

//#endregion

//#region user product invite
    $('form#user-product-invite').on('click', 'a#send-another', function(){
        $('#send-invitation').val('');
        $('#mysouse').prop('checked', false);
        $('#send-invitation').show();
        $('#invitation-layer').show();
        $('#send-another').hide();

        $('#success-message').text('');
        $('#error-message').text('');
        $('#success-message').hide();
        $('#error-message').hide();

        $('#email-friend').focus();
    });

    $('form#user-product-invite').on('submit', function(e){
        e.preventDefault();

        const email = $('#email-friend').val();
        const spouse = $('#myspouse').prop('checked');
        var email_validation = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        $('#success-massage').text('');
        $('#error-message').text('');
        $('#success-message').hide();
        $('#error-message').hide();

        if (!email_validation.test(email)){
            $('#error-message').text('Email Salah! Mohon isi email dengan benar');
            $('#error-message').show();
            $('#error-message').prop('style', 'color: #d9534f; font-weight: bold;');
            $('#email-friend').focus();
            return;
        }

        console.log(spouse);

        $.ajaxSetup({
            headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        });

        $.ajax({
           url : '/product/invite',
           data : {email : email, spouse : spouse},
           type : 'post',
           dataType : 'json',
           success : function (result){
               if (result.status == false){
                   $('#error-message').text(result.message);
                   $('#error-message').show();
                   $('#error-message').prop('style', 'color: #d9534f; font-weight: bold;');
               }else{
                    $('#success-message').text(result.message);
                    $('#success-message').show();
                    $('#success-message').prop('style', 'color: #5cb85c; font-weight: bold;');

                    $('#send-invitation').hide();
                    $('#invitation-layer').hide();
                    $('#send-another').show();

                    $('#email-friend').val('');
                    $('#myspouse').prop('checked', false);

                    const partner = parseInt(result.data.partners.length ) + 1;
                    const product = parseInt(result.data.data.product.invites) - 1;
                    const sisa = product - partner;

                    $('#invites-rest').text('You have '+ sisa.toString() +' invites left.');
                    $('#invites-rest').prop('style', 'font-weight: bold;');
               }
           } 
        });

        

    });
//#endregion


//#region old version - comment by halim
    //js for credit card
    // $('a.remove-credit-card').click(function (){
    //     const id = $(this).attr('id');
    //     const parts = id.split('-');
    //     const idNumber = parts.pop();

    //     const elemTarget = $('#clone-'+idNumber);
    //     elemTarget.find('input[name^=deleted]').val(true);
    //     elemTarget.find('input').removeAttr('required');
    //     elemTarget.find('select').removeAttr('required');
    //     elemTarget.hide();
    // });

    //  $('a#add-credit-card').click(function (){
    //     const currencyId = $('.cloned-liability-data').find('.currency').last().attr('id');
    //     const parts = currencyId.split('-');
    //     let lastCurrencyId = parseInt(parts.pop()) + 1;
    //     $('#clone-'+ (lastCurrencyId-1)).find('.remove-credit-card').show();

    //     lastCurrencyId = lastCurrencyId.toString();

    //     const newElem = $('.liability-data').clone().attr({
    //             id: "clone-"+lastCurrencyId,
    //             class: "col-md-12 col-md-offset-2 cloned-liability-data"
    //     }).css("display", "block").appendTo('.credit-card-form-container');

    //     newElem.prepend('<hr class="col-md-12">');

    //     newElem.find('#currency').attr('id', 'currency-'+lastCurrencyId);
    //     newElem.find('#liability-foreign-value').attr('id', 'liability-foreign-value-'+lastCurrencyId);
    //     newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
    //     newElem.find('#hidden-liability-value').attr('id', 'hidden-liability-value-'+lastCurrencyId);
    //     newElem.find('#liability-value').attr('id', 'liability-value-'+lastCurrencyId);
    //     newElem.find('#remove-credit-card').attr('id', 'remove-credit-card-'+lastCurrencyId);
    //     newElem.find('.remove-credit-card').show();

    //     newElem.find('input').attr('required', 'required');
    //     newElem.find('select').attr('required', 'required');

    //     newElem.find('.currency').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-liability-value-' + numberId;
    //         const idrId = 'liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(isNaN(valasValue)){
    //             valasValue = 0.00;
    //         }

    //         const currencyName = $('#'+inputId+' option:selected').text();

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = parseFloat(result.kurspajak_value);
    //                 idrValue = kursPajakValue * valasValue;

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.liability-foreign-value').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'currency-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-liability-value-' + numberId;
    //         const idrId = 'liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         const currencyName = $('#'+currencyId+' option:selected').text();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //             , function(result, status){
    //                 result = jQuery.parseJSON(result);
    //                 kursPajakValue = parseFloat(result.kurspajak_value);
    //                 idrValue = kursPajakValue * valasValue;

    //                 $('#'+kursPajakId).val(kursPajakValue);
    //                 $('#'+hiddenIdrId).val(idrValue);
    //                 $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.remove-credit-card').bind( "click", function() {
    //         const id = $(this).attr('id');
    //         const parts = id.split('-');
    //         const idNumber = parts.pop();

    //         const elemTarget = $('#clone-'+idNumber);
    //         elemTarget.find('input[name^=deleted]').val(true);
    //         elemTarget.find('input').removeAttr('required');
    //         elemTarget.find('select').removeAttr('required');
    //         elemTarget.hide();
    //     });

    // });


    //js for vehicle
    /* 
    $('a.remove-vehicle').click(function (){
        const id = $(this).attr('id');
        const parts = id.split('-');
        const idNumber = parts.pop();

        const elemTarget = $('#clone-'+idNumber);
        elemTarget.find('input[name^=deleted]').val(true);
        elemTarget.find('input').removeAttr('required');
        elemTarget.find('select').removeAttr('required');
        elemTarget.hide();
    });
    
     $('a#add-vehicle').click(function (){
        const currencyId = $('.cloned-vehicle-data').find('.vehicle-currency').last().attr('id');
        const parts = currencyId.split('-');
        let lastCurrencyId = parseInt(parts.pop()) + 1;
        $('#clone-'+ (lastCurrencyId-1)).find('.remove-vehicle').show();

        lastCurrencyId = lastCurrencyId.toString();

        const newElem = $('.vehicle-data').clone().attr({
                id: "clone-"+lastCurrencyId,
                class: "col-md-12 col-md-offset-2 cloned-vehicle-data"
        }).css("display", "block").appendTo('.vehicle-form-container');

        newElem.prepend('<hr class="col-md-12">');

        newElem.find('#vehicle-type').attr('id', 'vehicle-type-'+lastCurrencyId);
        newElem.find('#acquisition-year').attr('id', 'acquisition-year-'+lastCurrencyId);
        newElem.find('#vehicle-currency').attr('id', 'vehicle-currency-'+lastCurrencyId);
        newElem.find('#vehicle-foreign-value').attr('id', 'vehicle-foreign-value-'+lastCurrencyId);
        newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
        newElem.find('#hidden-vehicle-value').attr('id', 'hidden-vehicle-value-'+lastCurrencyId);
        newElem.find('#vehicle-value').attr('id', 'vehicle-value-'+lastCurrencyId);
        newElem.find('#police-number').attr('id', 'police-number-'+lastCurrencyId);
        newElem.find('#bpkb-number').attr('id', 'bpkb-number-'+lastCurrencyId);
        newElem.find('#remove-vehicle').attr('id', 'remove-vehicle-'+lastCurrencyId);
        newElem.find('.remove-vehicle').show();

        newElem.find('input').attr('required', 'required');
        newElem.find('select').attr('required', 'required');

        newElem.find('#bpkb-number-'+lastCurrencyId).removeAttr('required');

        newElem.find('.vehicle-currency').bind( "change", function() {
            const currentTime = new Date();
            const prevYear = currentTime.getFullYear() - 1;
            const prevMonth = '12';
            const prevDate = '31';

            const inputId = $(this).attr('id');
            const parts = inputId.split('-');
            const numberId = parts.pop();

            const acquisitionYearId = 'acquisition-year-' + numberId;
            const kursPajakId = 'kurspajak-' + numberId;
            const valasId = 'vehicle-foreign-value-' + numberId;
            const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
            const idrId = 'vehicle-value-' + numberId;
            let valasValue = $('#'+valasId).val();
            let kursPajakValue = 0;
            let idrValue = 0;

            let currencyName = $('#'+inputId+' option:selected').text();

            if(
                (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                    && currencyName != 'IDR'
                )
            {
                alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
                $('#'+inputId).val(1);
                currencyName = $('#'+inputId+' option:selected').text();
            }

            if(isNaN(valasValue)){
                valasValue = 0.00;
            }

            if(currencyName != 'IDR'){
                $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = parseFloat(result.kurspajak_value);
                    idrValue = kursPajakValue * valasValue;

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
                });
            }
            else {
                kursPajakValue = 1;
                idrValue = valasValue;

                $('#'+kursPajakId).val(kursPajakValue);
                $('#'+hiddenIdrId).val(idrValue);
                $('#'+idrId).val(idrValue);
            }
        });

        newElem.find('.acquisition-year').bind( "change", function() {
            const currentTime = new Date();
            const prevYear = currentTime.getFullYear() - 1;
            const prevMonth = '12';
            const prevDate = '31';

            const inputId = $(this).attr('id');
            const parts = inputId.split('-');
            const numberId = parts.pop();

            const vehicleCurrencyId = 'vehicle-currency-' + numberId;
            const acquisitionYearId = 'acquisition-year-' + numberId;
            const kursPajakId = 'kurspajak-' + numberId;
            const valasId = 'vehicle-foreign-value-' + numberId;
            const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
            const idrId = 'vehicle-value-' + numberId;
            let valasValue = $('#'+valasId).val();

            let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
            if(
                (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                    && currencyName != 'IDR'
                )
            {
                alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
                $('#'+vehicleCurrencyId).val(1);
                currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
            }

            let kursPajakValue = 0;
            let idrValue = 0;

            if(currencyName != 'IDR'){
                $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                    , function(result, status){
                        result = jQuery.parseJSON(result);
                        kursPajakValue = parseFloat(result.kurspajak_value);
                        idrValue = kursPajakValue * valasValue;

                        $('#'+kursPajakId).val(kursPajakValue);
                        $('#'+hiddenIdrId).val(idrValue);
                        $('#'+idrId).val(idrValue);
                });
            }
            else {
                kursPajakValue = 1;
                idrValue = valasValue;

                $('#'+kursPajakId).val(kursPajakValue);
                $('#'+hiddenIdrId).val(idrValue);
                $('#'+idrId).val(idrValue);
            }
        });

        newElem.find('.vehicle-foreign-value').bind( "change", function() {
            const currentTime = new Date();
            const prevYear = currentTime.getFullYear() - 1;
            const prevMonth = '12';
            const prevDate = '31';

            const inputId = $(this).attr('id');
            const parts = inputId.split('-');
            const numberId = parts.pop();

            const vehicleCurrencyId = 'vehicle-currency-' + numberId;
            const acquisitionYearId = 'acquisition-year-' + numberId;
            const kursPajakId = 'kurspajak-' + numberId;
            const valasId = 'vehicle-foreign-value-' + numberId;
            const hiddenIdrId = 'hidden-vehicle-value-' + numberId;
            const idrId = 'vehicle-value-' + numberId;
            let valasValue = $('#'+valasId).val();

            let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
            if(
                (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
                    && currencyName != 'IDR'
                )
            {
                alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
                $('#'+vehicleCurrencyId).val(1);
                currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
            }

            let kursPajakValue = 0;
            let idrValue = 0;

            if(currencyName != 'IDR'){
                $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
                , function(result, status){
                    result = jQuery.parseJSON(result);
                    kursPajakValue = parseFloat(result.kurspajak_value);
                    idrValue = kursPajakValue * valasValue;

                    $('#'+kursPajakId).val(kursPajakValue);
                    $('#'+hiddenIdrId).val(idrValue);
                    $('#'+idrId).val(idrValue);
                });
            }
            else {
                kursPajakValue = 1;
                idrValue = valasValue;

                $('#'+kursPajakId).val(kursPajakValue);
                $('#'+hiddenIdrId).val(idrValue);
                $('#'+idrId).val(idrValue);
            }
        });

        newElem.find('.vehicle-type').bind( "click", function() {
            const inputId = $(this).attr('id');
            const parts = inputId.split('-');
            const numberId = parts.pop();

            if($('#'+inputId).val() == 18){
                $('#clone-'+numberId).find('#police-number-'+numberId).attr('readonly', 'readonly');
                $('#clone-'+numberId).find('#bpkb-number-'+numberId).attr('readonly', 'readonly');
            } else{
                $('#clone-'+numberId).find('#police-number-'+numberId).removeAttr('readonly');
                $('#clone-'+numberId).find('#bpkb-number-'+numberId).removeAttr('readonly');
            }
        });

        newElem.find('.remove-vehicle').bind( "click", function() {
            const id = $(this).attr('id');
            const parts = id.split('-');
            const idNumber = parts.pop();

            const elemTarget = $('#clone-'+idNumber);
            elemTarget.find('input[name^=deleted]').val(true);
            elemTarget.find('input').removeAttr('required');
            elemTarget.find('select').removeAttr('required');
            elemTarget.hide();
        });

    });
    */

    //js for vehicle-liability
    // $('a.remove-vehicle-liability').click(function (){
    //     const id = $(this).attr('id');
    //     const parts = id.split('-');
    //     const idNumber = parts.pop();

    //     const elemTarget = $('#clone-'+idNumber);
    //     elemTarget.find('input[name^=deleted]').val(true);
    //     elemTarget.find('input').removeAttr('required');
    //     elemTarget.find('select').removeAttr('required');
    //     elemTarget.hide();
    // });

    // $('a#add-vehicle-liability').click(function (){
    //     const currencyId = $('.cloned-vehicle-liability-data').find('.vehicle-liability-currency').last().attr('id');
    //     const parts = currencyId.split('-');
    //     let lastCurrencyId = parseInt(parts.pop()) + 1;
    //     $('#clone-'+ (lastCurrencyId-1)).find('.remove-vehicle-liability').show();

    //     lastCurrencyId = lastCurrencyId.toString();

    //     const newElem = $('.vehicle-liability-data').clone().attr({
    //             id: "clone-"+lastCurrencyId,
    //             class: "col-md-12 col-md-offset-2 cloned-vehicle-liability-data"
    //     }).css("display", "block").appendTo('.vehicle-liability-form-container');

    //     newElem.prepend('<hr class="col-md-12">');

    //     newElem.find('#vehicle-acquisition-year').attr('id', 'vehicle-acquisition-year-'+lastCurrencyId);
    //     newElem.find('#vehicle-liability-currency').attr('id', 'vehicle-liability-currency-'+lastCurrencyId);
    //     newElem.find('#vehicle-liability-foreign-value').attr('id', 'vehicle-liability-foreign-value-'+lastCurrencyId);
    //     newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
    //     newElem.find('#hidden-vehicle-liability-value').attr('id', 'hidden-vehicle-liability-value-'+lastCurrencyId);
    //     newElem.find('#vehicle-liability-value').attr('id', 'vehicle-liability-value-'+lastCurrencyId);
    //     newElem.find('#remove-vehicle-liability').attr('id', 'remove-vehicle-liability-'+lastCurrencyId);
    //     newElem.find('.remove-vehicle-liability').show();

    //     newElem.find('input').attr('required', 'required');
    //     newElem.find('select').attr('required', 'required');

    //     newElem.find('.vehicle-liability-currency').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'vehicle-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
    //         const idrId = 'vehicle-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         let currencyName = $('#'+inputId+' option:selected').text();

    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+inputId).val(1);
    //             currencyName = $('#'+inputId+' option:selected').text();
    //         }

    //         if(isNaN(valasValue)){
    //             valasValue = 0.00;
    //         }

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.vehicle-acquisition-year').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const vehicleCurrencyId = 'vehicle-liability-currency-' + numberId;
    //         const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'vehicle-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
    //         const idrId = 'vehicle-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+vehicleCurrencyId).val(1);
    //             currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.vehicle-liability-foreign-value').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const vehicleCurrencyId = 'vehicle-liability-currency-' + numberId;
    //         const acquisitionYearId = 'vehicle-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'vehicle-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-vehicle-liability-value-' + numberId;
    //         const idrId = 'vehicle-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+vehicleCurrencyId).val(1);
    //             currencyName = $('#'+vehicleCurrencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.remove-vehicle-liability').bind( "click", function() {
    //         const id = $(this).attr('id');
    //         const parts = id.split('-');
    //         const idNumber = parts.pop();

    //         const elemTarget = $('#clone-'+idNumber);
    //         elemTarget.find('input[name^=deleted]').val(true);
    //         elemTarget.find('input').removeAttr('required');
    //         elemTarget.find('select').removeAttr('required');
    //         elemTarget.hide();
    //     });

    // });


//js for property
    // $('a.remove-property').click(function (){
    //     const id = $(this).attr('id');
    //     const parts = id.split('-');
    //     const idNumber = parts.pop();

    //     const elemTarget = $('#clone-'+idNumber);
    //     elemTarget.find('input[name^=deleted]').val(true);
    //     elemTarget.find('input').removeAttr('required');
    //     elemTarget.find('textarea').removeAttr('required');
    //     elemTarget.find('select').removeAttr('required');
    //     elemTarget.hide();
    // });

    // $('a#add-property').click(function (){
    //     const currencyId = $('.cloned-property-data').find('.property-currency').last().attr('id');
    //     const parts = currencyId.split('-');
    //     let lastCurrencyId = parseInt(parts.pop()) + 1;
    //     $('#clone-'+ (lastCurrencyId-1)).find('.remove-property').show();

    //     lastCurrencyId = lastCurrencyId.toString();

    //     const newElem = $('.property-data').clone().attr({
    //             id: "clone-"+lastCurrencyId,
    //             class: "col-md-12 col-md-offset-2 cloned-property-data"
    //     }).css("display", "block").appendTo('.property-form-container');

    //     newElem.prepend('<hr class="col-md-12">');

    //     newElem.find('#property-type').attr('id', 'property-type-'+lastCurrencyId);
    //     newElem.find('#property-function').attr('id', 'property-function-'+lastCurrencyId);
    //     newElem.find('#property-acquisition-year').attr('id', 'property-acquisition-year-'+lastCurrencyId);
    //     newElem.find('#property-currency').attr('id', 'property-currency-'+lastCurrencyId);
    //     newElem.find('#property-foreign-value').attr('id', 'property-foreign-value-'+lastCurrencyId);
    //     newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
    //     newElem.find('#hidden-property-value').attr('id', 'hidden-property-value-'+lastCurrencyId);
    //     newElem.find('#property-value').attr('id', 'property-value-'+lastCurrencyId);
    //     newElem.find('#remove-property').attr('id', 'remove-property-'+lastCurrencyId);
    //     newElem.find('.remove-property').show();

    //     newElem.find('input').attr('required', 'required');
    //     newElem.find('textarea').attr('required', 'required');
    //     newElem.find('select').attr('required', 'required');

    //     newElem.find('.property-currency').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-value-' + numberId;
    //         const idrId = 'property-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         let currencyName = $('#'+inputId+' option:selected').text();

    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+inputId).val(1);
    //             currencyName = $('#'+inputId+' option:selected').text();
    //         }

    //         if(isNaN(valasValue)){
    //             valasValue = 0.00;
    //         }

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-acquisition-year').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'property-currency-' + numberId;
    //         const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-value-' + numberId;
    //         const idrId = 'property-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-foreign-value').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'property-currency-' + numberId;
    //         const acquisitionYearId = 'property-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-value-' + numberId;
    //         const idrId = 'property-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-type').bind( "change", function() {
    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const propertyType = $('#property-type-'+numberId).val();

    //         if(propertyType == 'Apartemen'){
    //             $('#property-function-'+numberId).html(
    //                     '<option value="Untuk Tempat Tinggal">Untuk Tempat Tinggal</option>'
    //                 );
    //         } else {
    //             $('#property-function-'+numberId).html(
    //                     '<option value="Untuk Tempat Tinggal">Untuk Tempat Tinggal</option>' +
    //                     '<option value="Untuk Usaha">Untuk Usaha</option>'
    //                 );
    //         }
    //     });

    //     newElem.find('.remove-property').bind( "click", function() {
    //         const id = $(this).attr('id');
    //         const parts = id.split('-');
    //         const idNumber = parts.pop();

    //         const elemTarget = $('#clone-'+idNumber);
    //         elemTarget.find('input[name^=deleted]').val(true);
    //         elemTarget.find('input').removeAttr('required');
    //         elemTarget.find('textarea').removeAttr('required');
    //         elemTarget.find('select').removeAttr('required');
    //         elemTarget.hide();
    //     });

    // });

//js for property-liability
    // $('a.remove-property-liability').click(function (){
    //     const id = $(this).attr('id');
    //     const parts = id.split('-');
    //     const idNumber = parts.pop();

    //     const elemTarget = $('#clone-'+idNumber);
    //     elemTarget.find('input[name^=deleted]').val(true);
    //     elemTarget.find('input').removeAttr('required');
    //     elemTarget.find('select').removeAttr('required');
    //     elemTarget.hide();
    // });

    // $('a#add-property-liability').click(function (){
    //     const currencyId = $('.cloned-property-liability-data').find('.property-liability-currency').last().attr('id');
    //     const parts = currencyId.split('-');
    //     let lastCurrencyId = parseInt(parts.pop()) + 1;
    //     $('#clone-'+ (lastCurrencyId-1)).find('.remove-property-liability').show();

    //     lastCurrencyId = lastCurrencyId.toString();

    //     const newElem = $('.property-liability-data').clone().attr({
    //             id: "clone-"+lastCurrencyId,
    //             class: "col-md-12 col-md-offset-2 cloned-property-liability-data"
    //     }).css("display", "block").appendTo('.property-liability-form-container');

    //     newElem.prepend('<hr class="col-md-12">');

    //     newElem.find('#property-liability-bank-name').attr('id', 'property-liability-bank-name-'+lastCurrencyId);
    //     newElem.find('#property-liability-other-bank-name-field').attr('id', 'property-liability-other-bank-name-field-'+lastCurrencyId);
    //     newElem.find('#property-liability-acquisition-year').attr('id', 'property-liability-acquisition-year-'+lastCurrencyId);
    //     newElem.find('#property-liability-currency').attr('id', 'property-liability-currency-'+lastCurrencyId);
    //     newElem.find('#property-liability-foreign-value').attr('id', 'property-liability-foreign-value-'+lastCurrencyId);
    //     newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
    //     newElem.find('#hidden-property-liability-value').attr('id', 'hidden-property-liability-value-'+lastCurrencyId);
    //     newElem.find('#property-liability-value').attr('id', 'property-liability-value-'+lastCurrencyId);
    //     newElem.find('#remove-property-liability').attr('id', 'remove-property-liability-'+lastCurrencyId);
    //     newElem.find('.remove-property-liability').show();

    //     newElem.find('input').attr('required', 'required');
    //     newElem.find('textarea').attr('required', 'required');
    //     newElem.find('select').attr('required', 'required');

    //     newElem.find('.property-liability-currency').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
    //         const idrId = 'property-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         let currencyName = $('#'+inputId+' option:selected').text();

    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+inputId).val(1);
    //             currencyName = $('#'+inputId+' option:selected').text();
    //         }

    //         if(isNaN(valasValue)){
    //             valasValue = 0.00;
    //         }

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-liability-acquisition-year').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'property-liability-currency-' + numberId;
    //         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
    //         const idrId = 'property-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-liability-foreign-value').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'property-liability-currency-' + numberId;
    //         const acquisitionYearId = 'property-liability-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'property-liability-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-property-liability-value-' + numberId;
    //         const idrId = 'property-liability-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.property-liability-bank-name').bind( "change", function() {
    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const bankName = $('#property-liability-bank-name-'+numberId).val();

    //         if(bankName == 'Lainnya'){
    //             const otherField = $('#property-liability-other-bank-name-field-'+numberId).show();
    //             otherField.find('input').attr('required', 'required');
    //         } else {
    //             const otherField = $('#property-liability-other-bank-name-field-'+numberId).hide();
    //             otherField.find('input').removeAttr('required');
    //         }
    //     });

    //     newElem.find('.remove-property-liability').bind( "click", function() {
    //         const id = $(this).attr('id');
    //         const parts = id.split('-');
    //         const idNumber = parts.pop();

    //         const elemTarget = $('#clone-'+idNumber);
    //         elemTarget.find('input[name^=deleted]').val(true);
    //         elemTarget.find('input').removeAttr('required');
    //         elemTarget.find('textarea').removeAttr('required');
    //         elemTarget.find('select').removeAttr('required');
    //         elemTarget.hide();
    //     });

    // });


//js for precious-assets-detail
    // $('a.remove-precious-assets').click(function (){
    //     const id = $(this).attr('id');
    //     const parts = id.split('-');
    //     const idNumber = parts.pop();

    //     const elemTarget = $('#clone-'+idNumber);
    //     elemTarget.find('input[name^=deleted]').val(true);
    //     elemTarget.find('input').removeAttr('required');
    //     elemTarget.find('textarea').removeAttr('required');
    //     elemTarget.find('select').removeAttr('required');
    //     elemTarget.hide();
    // });

    // $('a#add-precious-assets').click(function (){
    //     const currencyId = $('.cloned-precious-assets-detail-data').find('.precious-assets-currency').last().attr('id');
    //     const parts = currencyId.split('-');
    //     let lastCurrencyId = parseInt(parts.pop()) + 1;
    //     $('#clone-'+ (lastCurrencyId-1)).find('.remove-precious-assets').show();

    //     lastCurrencyId = lastCurrencyId.toString();

    //     const newElem = $('.precious-assets-detail-data').clone().attr({
    //             id: "clone-"+lastCurrencyId,
    //             class: "col-md-12 col-md-offset-2 cloned-precious-assets-detail-data"
    //     }).css("display", "block").appendTo('.precious-assets-detail-form-container');

    //     newElem.prepend('<hr class="col-md-12">');

    //     newElem.find('#precious-assets-acquisition-year').attr('id', 'precious-assets-acquisition-year-'+lastCurrencyId);
    //     newElem.find('#precious-assets-currency').attr('id', 'precious-assets-currency-'+lastCurrencyId);
    //     newElem.find('#precious-assets-foreign-value').attr('id', 'precious-assets-foreign-value-'+lastCurrencyId);
    //     newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
    //     newElem.find('#hidden-precious-assets-value').attr('id', 'hidden-precious-assets-value-'+lastCurrencyId);
    //     newElem.find('#precious-assets-value').attr('id', 'precious-assets-value-'+lastCurrencyId);
    //     newElem.find('#remove-precious-assets').attr('id', 'remove-precious-assets-'+lastCurrencyId);
    //     newElem.find('.remove-precious-assets').show();

    //     newElem.find('input').attr('required', 'required');
    //     newElem.find('textarea').attr('required', 'required');
    //     newElem.find('select').attr('required', 'required');

    //     newElem.find('.precious-assets-currency').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'precious-assets-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
    //         const idrId = 'precious-assets-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();
    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         let currencyName = $('#'+inputId+' option:selected').text();

    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+inputId).val(1);
    //             currencyName = $('#'+inputId+' option:selected').text();
    //         }

    //         if(isNaN(valasValue)){
    //             valasValue = 0.00;
    //         }

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.precious-assets-acquisition-year').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'precious-assets-currency-' + numberId;
    //         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'precious-assets-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
    //         const idrId = 'precious-assets-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.precious-assets-foreign-value').bind( "change", function() {
    //         const currentTime = new Date();
    //         const prevYear = currentTime.getFullYear() - 1;
    //         const prevMonth = '12';
    //         const prevDate = '31';

    //         const inputId = $(this).attr('id');
    //         const parts = inputId.split('-');
    //         const numberId = parts.pop();

    //         const currencyId = 'precious-assets-currency-' + numberId;
    //         const acquisitionYearId = 'precious-assets-acquisition-year-' + numberId;
    //         const kursPajakId = 'kurspajak-' + numberId;
    //         const valasId = 'precious-assets-foreign-value-' + numberId;
    //         const hiddenIdrId = 'hidden-precious-assets-value-' + numberId;
    //         const idrId = 'precious-assets-value-' + numberId;
    //         let valasValue = $('#'+valasId).val();

    //         let currencyName = $('#'+currencyId+' option:selected').text();
    //         if(
    //             (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
    //                 && currencyName != 'IDR'
    //             )
    //         {
    //             alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
    //             $('#'+currencyId).val(1);
    //             currencyName = $('#'+currencyId+' option:selected').text();
    //         }

    //         let kursPajakValue = 0;
    //         let idrValue = 0;

    //         if(currencyName != 'IDR'){
    //             $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
    //                 , function(result, status){
    //                     result = jQuery.parseJSON(result);
    //                     kursPajakValue = parseFloat(result.kurspajak_value);
    //                     idrValue = kursPajakValue * valasValue;

    //                     $('#'+kursPajakId).val(kursPajakValue);
    //                     $('#'+hiddenIdrId).val(idrValue);
    //                     $('#'+idrId).val(idrValue);
    //             });
    //         }
    //         else {
    //             kursPajakValue = 1;
    //             idrValue = valasValue;

    //             $('#'+kursPajakId).val(kursPajakValue);
    //             $('#'+hiddenIdrId).val(idrValue);
    //             $('#'+idrId).val(idrValue);
    //         }
    //     });

    //     newElem.find('.remove-precious-assets').bind( "click", function() {
    //         const id = $(this).attr('id');
    //         const parts = id.split('-');
    //         const idNumber = parts.pop();

    //         const elemTarget = $('#clone-'+idNumber);
    //         elemTarget.find('input[name^=deleted]').val(true);
    //         elemTarget.find('input').removeAttr('required');
    //         elemTarget.find('textarea').removeAttr('required');
    //         elemTarget.find('select').removeAttr('required');
    //         elemTarget.hide();
    //     });

    // });


// //js for tradeable-stocks
//     $('a.remove-tradeable-stocks').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-tradeable-stocks').click(function (){
//         const currencyId = $('.cloned-tradeable-stocks-data').find('.tradeable-stocks-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-tradeable-stocks').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.tradeable-stocks-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-tradeable-stocks-data"
//         }).css("display", "block").appendTo('.tradeable-stocks-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#tradeable-stocks-acquisition-year').attr('id', 'tradeable-stocks-acquisition-year-'+lastCurrencyId);
//         newElem.find('#tradeable-stocks-currency').attr('id', 'tradeable-stocks-currency-'+lastCurrencyId);
//         newElem.find('#tradeable-stocks-foreign-value').attr('id', 'tradeable-stocks-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-tradeable-stocks-value').attr('id', 'hidden-tradeable-stocks-value-'+lastCurrencyId);
//         newElem.find('#tradeable-stocks-value').attr('id', 'tradeable-stocks-value-'+lastCurrencyId);
//         newElem.find('#remove-tradeable-stocks').attr('id', 'remove-tradeable-stocks-'+lastCurrencyId);
//         newElem.find('.remove-tradeable-stocks').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.tradeable-stocks-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//             const idrId = 'tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.tradeable-stocks-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'tradeable-stocks-currency-' + numberId;
//             const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//             const idrId = 'tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.tradeable-stocks-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'tradeable-stocks-currency-' + numberId;
//             const acquisitionYearId = 'tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-tradeable-stocks-value-' + numberId;
//             const idrId = 'tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-tradeable-stocks').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for mutual-funds
//     $('a.remove-mutual-funds').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-mutual-funds').click(function (){
//         const currencyId = $('.cloned-mutual-funds-data').find('.mutual-funds-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-mutual-funds').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.mutual-funds-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-mutual-funds-data"
//         }).css("display", "block").appendTo('.mutual-funds-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#mutual-funds-acquisition-year').attr('id', 'mutual-funds-acquisition-year-'+lastCurrencyId);
//         newElem.find('#mutual-funds-currency').attr('id', 'mutual-funds-currency-'+lastCurrencyId);
//         newElem.find('#mutual-funds-foreign-value').attr('id', 'mutual-funds-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-mutual-funds-value').attr('id', 'hidden-mutual-funds-value-'+lastCurrencyId);
//         newElem.find('#mutual-funds-value').attr('id', 'mutual-funds-value-'+lastCurrencyId);
//         newElem.find('#remove-mutual-funds').attr('id', 'remove-mutual-funds-'+lastCurrencyId);
//         newElem.find('.remove-mutual-funds').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.mutual-funds-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'mutual-funds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//             const idrId = 'mutual-funds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.mutual-funds-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'mutual-funds-currency-' + numberId;
//             const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'mutual-funds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//             const idrId = 'mutual-funds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.mutual-funds-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'mutual-funds-currency-' + numberId;
//             const acquisitionYearId = 'mutual-funds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'mutual-funds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-mutual-funds-value-' + numberId;
//             const idrId = 'mutual-funds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-mutual-funds').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });


// //js for insurance
//     $('a.remove-insurance').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-insurance').click(function (){
//         const currencyId = $('.cloned-insurance-data').find('.insurance-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-insurance').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.insurance-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-insurance-data"
//         }).css("display", "block").appendTo('.insurance-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#insurance-acquisition-year').attr('id', 'insurance-acquisition-year-'+lastCurrencyId);
//         newElem.find('#insurance-currency').attr('id', 'insurance-currency-'+lastCurrencyId);
//         newElem.find('#insurance-foreign-value').attr('id', 'insurance-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-insurance-value').attr('id', 'hidden-insurance-value-'+lastCurrencyId);
//         newElem.find('#insurance-value').attr('id', 'insurance-value-'+lastCurrencyId);
//         newElem.find('#remove-insurance').attr('id', 'remove-insurance-'+lastCurrencyId);
//         newElem.find('.remove-insurance').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.insurance-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'insurance-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//             const idrId = 'insurance-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.insurance-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'insurance-currency-' + numberId;
//             const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'insurance-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//             const idrId = 'insurance-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.insurance-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'insurance-currency-' + numberId;
//             const acquisitionYearId = 'insurance-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'insurance-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-insurance-value-' + numberId;
//             const idrId = 'insurance-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-insurance').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for non-tradeable-stocks
//     $('a.remove-non-tradeable-stocks').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-non-tradeable-stocks').click(function (){
//         const currencyId = $('.cloned-non-tradeable-stocks-data').find('.non-tradeable-stocks-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-non-tradeable-stocks').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.non-tradeable-stocks-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-non-tradeable-stocks-data"
//         }).css("display", "block").appendTo('.non-tradeable-stocks-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#non-tradeable-stocks-acquisition-year').attr('id', 'non-tradeable-stocks-acquisition-year-'+lastCurrencyId);
//         newElem.find('#non-tradeable-stocks-currency').attr('id', 'non-tradeable-stocks-currency-'+lastCurrencyId);
//         newElem.find('#non-tradeable-stocks-foreign-value').attr('id', 'non-tradeable-stocks-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-non-tradeable-stocks-value').attr('id', 'hidden-non-tradeable-stocks-value-'+lastCurrencyId);
//         newElem.find('#non-tradeable-stocks-value').attr('id', 'non-tradeable-stocks-value-'+lastCurrencyId);
//         newElem.find('#remove-non-tradeable-stocks').attr('id', 'remove-non-tradeable-stocks-'+lastCurrencyId);
//         newElem.find('.remove-non-tradeable-stocks').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.non-tradeable-stocks-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//             const idrId = 'non-tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.non-tradeable-stocks-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'non-tradeable-stocks-currency-' + numberId;
//             const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//             const idrId = 'non-tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.non-tradeable-stocks-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'non-tradeable-stocks-currency-' + numberId;
//             const acquisitionYearId = 'non-tradeable-stocks-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'non-tradeable-stocks-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-non-tradeable-stocks-value-' + numberId;
//             const idrId = 'non-tradeable-stocks-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-non-tradeable-stocks').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for insurance-claim
//     $('a.remove-insurance-claim').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-insurance-claim').click(function (){
//         const currencyId = $('.cloned-insurance-claim-data').find('.insurance-claim-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-insurance-claim').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.insurance-claim-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-insurance-claim-data"
//         }).css("display", "block").appendTo('.insurance-claim-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#insurance-claim-currency').attr('id', 'insurance-claim-currency-'+lastCurrencyId);
//         newElem.find('#insurance-claim-foreign-value').attr('id', 'insurance-claim-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-insurance-claim-value').attr('id', 'hidden-insurance-claim-value-'+lastCurrencyId);
//         newElem.find('#insurance-claim-value').attr('id', 'insurance-claim-value-'+lastCurrencyId);
//         newElem.find('#insurance-claim-type').attr('id', 'insurance-claim-type-'+lastCurrencyId);
//         newElem.find('#insurance-claim-purpose').attr('id', 'insurance-claim-purpose-'+lastCurrencyId);
//         newElem.find('#remove-insurance-claim').attr('id', 'remove-insurance-claims-'+lastCurrencyId);
//         newElem.find('.remove-insurance-claim').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.insurance-claim-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'insurance-claim-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-insurance-claim-value-' + numberId;
//             const idrId = 'insurance-claim-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.insurance-claim-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'insurance-claim-currency-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'insurance-claim-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-insurance-claim-value-' + numberId;
//             const idrId = 'insurance-claim-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }

//         });

//         newElem.find('.insurance-claim-type').bind( "change", function() {
//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const insuranceClaimType = $('#insurance-claim-type-'+numberId).val();

//             if(insuranceClaimType == 'Kecelakaan'){
//                 $('#insurance-claim-purpose-'+numberId).removeAttr('readonly');
//                 $('#insurance-claim-purpose-'+numberId).html(
//                         '<option value="Rumah">Rumah</option>' +
//                         '<option value="Kendaraan">Kendaraan</option>'
//                     );
//             } else {
//                 $('#insurance-claim-purpose-'+numberId).attr('readonly', 'readonly');
//                 $('#insurance-claim-purpose-'+numberId).html(
//                         '<option value="">--</option>'
//                     );
//             }
//         });

//         newElem.find('.remove-insurance-claim').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for capital-investment-companies
//     $('a.remove-cic').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-cic').click(function (){
//         const currencyId = $('.cloned-cic-data').find('.cic-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-cic').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.cic-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-cic-data"
//         }).css("display", "block").appendTo('.cic-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#cic-acquisition-year').attr('id', 'cic-acquisition-year-'+lastCurrencyId);
//         newElem.find('#cic-currency').attr('id', 'cic-currency-'+lastCurrencyId);
//         newElem.find('#cic-foreign-value').attr('id', 'cic-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-cic-value').attr('id', 'hidden-cic-value-'+lastCurrencyId);
//         newElem.find('#cic-value').attr('id', 'cic-value-'+lastCurrencyId);
//         newElem.find('#remove-cic').attr('id', 'remove-cic-'+lastCurrencyId);
//         newElem.find('.remove-cic').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.cic-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'cic-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-cic-value-' + numberId;
//             const idrId = 'cic-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.cic-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'cic-currency-' + numberId;
//             const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'cic-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-cic-value-' + numberId;
//             const idrId = 'cic-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.cic-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'cic-currency-' + numberId;
//             const acquisitionYearId = 'cic-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'cic-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-cic-value-' + numberId;
//             const idrId = 'cic-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-cic').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for indonesian-bank-certificate
//     $('a.remove-sbi').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-sbi').click(function (){
//         const clonedId = $('.cloned-sbi-data').last().attr('id');
//         const parts = clonedId.split('-');
//         let lastClonedId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastClonedId-1)).find('.remove-sbi').show();

//         lastClonedId = lastClonedId.toString();

//         const newElem = $('.sbi-data').clone().attr({
//                 id: "clone-"+lastClonedId,
//                 class: "col-md-12 col-md-offset-2 cloned-sbi-data"
//         }).css("display", "block").appendTo('.sbi-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#remove-sbi').attr('id', 'remove-sbi-'+lastClonedId);
//         newElem.find('.remove-sbi').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.remove-sbi').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });


// //js for corporate-bonds
//     $('a.remove-corporate-bonds').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-corporate-bonds').click(function (){
//         const currencyId = $('.cloned-corporate-bonds-data').find('.corporate-bonds-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-corporate-bonds').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.corporate-bonds-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-corporate-bonds-data"
//         }).css("display", "block").appendTo('.corporate-bonds-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#corporate-bonds-acquisition-year').attr('id', 'corporate-bonds-acquisition-year-'+lastCurrencyId);
//         newElem.find('#corporate-bonds-currency').attr('id', 'corporate-bonds-currency-'+lastCurrencyId);
//         newElem.find('#corporate-bonds-foreign-value').attr('id', 'corporate-bonds-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-corporate-bonds-value').attr('id', 'hidden-corporate-bonds-value-'+lastCurrencyId);
//         newElem.find('#corporate-bonds-value').attr('id', 'corporate-bonds-value-'+lastCurrencyId);
//         newElem.find('#remove-corporate-bonds').attr('id', 'remove-corporate-bonds-'+lastCurrencyId);
//         newElem.find('.remove-corporate-bonds').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.corporate-bonds-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'corporate-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//             const idrId = 'corporate-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.corporate-bonds-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'corporate-bonds-currency-' + numberId;
//             const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'corporate-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//             const idrId = 'corporate-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.corporate-bonds-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'corporate-bonds-currency-' + numberId;
//             const acquisitionYearId = 'corporate-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'corporate-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-corporate-bonds-value-' + numberId;
//             const idrId = 'corporate-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-corporate-bonds').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for government-bonds
//     $('a.remove-government-bonds').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-government-bonds').click(function (){
//         const currencyId = $('.cloned-government-bonds-data').find('.government-bonds-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-government-bonds').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.government-bonds-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-government-bonds-data"
//         }).css("display", "block").appendTo('.government-bonds-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#government-bonds-acquisition-year').attr('id', 'government-bonds-acquisition-year-'+lastCurrencyId);
//         newElem.find('#government-bonds-currency').attr('id', 'government-bonds-currency-'+lastCurrencyId);
//         newElem.find('#government-bonds-foreign-value').attr('id', 'government-bonds-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-government-bonds-value').attr('id', 'hidden-government-bonds-value-'+lastCurrencyId);
//         newElem.find('#government-bonds-value').attr('id', 'government-bonds-value-'+lastCurrencyId);
//         newElem.find('#remove-government-bonds').attr('id', 'remove-government-bonds-'+lastCurrencyId);
//         newElem.find('.remove-government-bonds').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.government-bonds-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'government-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//             const idrId = 'government-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.government-bonds-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'government-bonds-currency-' + numberId;
//             const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'government-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//             const idrId = 'government-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.government-bonds-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'government-bonds-currency-' + numberId;
//             const acquisitionYearId = 'government-bonds-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'government-bonds-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-government-bonds-value-' + numberId;
//             const idrId = 'government-bonds-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-government-bonds').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-debentures
//     $('a.remove-other-debentures').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-other-debentures').click(function (){
//         const currencyId = $('.cloned-other-debentures-data').find('.other-debentures-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-other-debentures').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.other-debentures-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-other-debentures-data"
//         }).css("display", "block").appendTo('.other-debentures-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#other-debentures-acquisition-year').attr('id', 'other-debentures-acquisition-year-'+lastCurrencyId);
//         newElem.find('#other-debentures-currency').attr('id', 'other-debentures-currency-'+lastCurrencyId);
//         newElem.find('#other-debentures-foreign-value').attr('id', 'other-debentures-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-other-debentures-value').attr('id', 'hidden-other-debentures-value-'+lastCurrencyId);
//         newElem.find('#other-debentures-value').attr('id', 'other-debentures-value-'+lastCurrencyId);
//         newElem.find('#remove-other-debentures').attr('id', 'remove-other-debentures-'+lastCurrencyId);
//         newElem.find('.remove-other-debentures').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.other-debentures-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-debentures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//             const idrId = 'other-debentures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.other-debentures-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'other-debentures-currency-' + numberId;
//             const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-debentures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//             const idrId = 'other-debentures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.other-debentures-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'other-debentures-currency-' + numberId;
//             const acquisitionYearId = 'other-debentures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-debentures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-debentures-value-' + numberId;
//             const idrId = 'other-debentures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-other-debentures').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-investments
//     $('a.remove-other-investments').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-other-investments').click(function (){
//         const currencyId = $('.cloned-other-investments-data').find('.other-investments-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-other-investments').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.other-investments-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-other-investments-data"
//         }).css("display", "block").appendTo('.other-investments-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#other-investments-acquisition-year').attr('id', 'other-investments-acquisition-year-'+lastCurrencyId);
//         newElem.find('#other-investments-currency').attr('id', 'other-investments-currency-'+lastCurrencyId);
//         newElem.find('#other-investments-foreign-value').attr('id', 'other-investments-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-other-investments-value').attr('id', 'hidden-other-investments-value-'+lastCurrencyId);
//         newElem.find('#other-investments-value').attr('id', 'other-investments-value-'+lastCurrencyId);
//         newElem.find('#remove-other-investments').attr('id', 'remove-other-investments-'+lastCurrencyId);
//         newElem.find('.remove-other-investments').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.other-investments-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-investments-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//             const idrId = 'other-investments-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.other-investments-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'other-investments-currency-' + numberId;
//             const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-investments-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//             const idrId = 'other-investments-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.other-investments-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'other-investments-currency-' + numberId;
//             const acquisitionYearId = 'other-investments-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'other-investments-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-other-investments-value-' + numberId;
//             const idrId = 'other-investments-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-other-investments').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-cash-equivalents
//     $('a.remove-oce').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-oce').click(function (){
//         const currencyId = $('.cloned-oce-data').find('.oce-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-oce').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.oce-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-oce-data"
//         }).css("display", "block").appendTo('.oce-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#oce-acquisition-year').attr('id', 'oce-acquisition-year-'+lastCurrencyId);
//         newElem.find('#oce-currency').attr('id', 'oce-currency-'+lastCurrencyId);
//         newElem.find('#oce-foreign-value').attr('id', 'oce-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-oce-value').attr('id', 'hidden-oce-value-'+lastCurrencyId);
//         newElem.find('#oce-value').attr('id', 'oce-value-'+lastCurrencyId);
//         newElem.find('#remove-oce').attr('id', 'remove-oce-'+lastCurrencyId);
//         newElem.find('.remove-oce').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.oce-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oce-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oce-value-' + numberId;
//             const idrId = 'oce-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.oce-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'oce-currency-' + numberId;
//             const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oce-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oce-value-' + numberId;
//             const idrId = 'oce-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.oce-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'oce-currency-' + numberId;
//             const acquisitionYearId = 'oce-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oce-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oce-value-' + numberId;
//             const idrId = 'oce-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-oce').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for special-transportations
//     $('a.remove-spctrans').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-spctrans').click(function (){
//         const currencyId = $('.cloned-spctrans-data').find('.spctrans-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-spctrans').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.spctrans-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-spctrans-data"
//         }).css("display", "block").appendTo('.spctrans-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#spctrans-acquisition-year').attr('id', 'spctrans-acquisition-year-'+lastCurrencyId);
//         newElem.find('#spctrans-currency').attr('id', 'spctrans-currency-'+lastCurrencyId);
//         newElem.find('#spctrans-foreign-value').attr('id', 'spctrans-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-spctrans-value').attr('id', 'hidden-spctrans-value-'+lastCurrencyId);
//         newElem.find('#spctrans-value').attr('id', 'spctrans-value-'+lastCurrencyId);
//         newElem.find('#remove-spctrans').attr('id', 'remove-spctrans-'+lastCurrencyId);
//         newElem.find('.remove-spctrans').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.spctrans-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'spctrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//             const idrId = 'spctrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.spctrans-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'spctrans-currency-' + numberId;
//             const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'spctrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//             const idrId = 'spctrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.spctrans-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'spctrans-currency-' + numberId;
//             const acquisitionYearId = 'spctrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'spctrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-spctrans-value-' + numberId;
//             const idrId = 'spctrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-spctrans').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-transportations
//     $('a.remove-othertrans').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-othertrans').click(function (){
//         const currencyId = $('.cloned-othertrans-data').find('.othertrans-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-othertrans').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.othertrans-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-othertrans-data"
//         }).css("display", "block").appendTo('.othertrans-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#othertrans-acquisition-year').attr('id', 'othertrans-acquisition-year-'+lastCurrencyId);
//         newElem.find('#othertrans-currency').attr('id', 'othertrans-currency-'+lastCurrencyId);
//         newElem.find('#othertrans-foreign-value').attr('id', 'othertrans-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-othertrans-value').attr('id', 'hidden-othertrans-value-'+lastCurrencyId);
//         newElem.find('#othertrans-value').attr('id', 'othertrans-value-'+lastCurrencyId);
//         newElem.find('#remove-othertrans').attr('id', 'remove-othertrans-'+lastCurrencyId);
//         newElem.find('.remove-othertrans').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.othertrans-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'othertrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//             const idrId = 'othertrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.othertrans-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'othertrans-currency-' + numberId;
//             const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'othertrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//             const idrId = 'othertrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.othertrans-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'othertrans-currency-' + numberId;
//             const acquisitionYearId = 'othertrans-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'othertrans-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-othertrans-value-' + numberId;
//             const idrId = 'othertrans-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-othertrans').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for electronic-furnitures
//     $('a.remove-electronicsfurnitures').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-electronicsfurnitures').click(function (){
//         const currencyId = $('.cloned-electronicsfurnitures-data').find('.electronicsfurnitures-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-electronicsfurnitures').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.electronicsfurnitures-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-electronicsfurnitures-data"
//         }).css("display", "block").appendTo('.electronicsfurnitures-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#electronicsfurnitures-acquisition-year').attr('id', 'electronicsfurnitures-acquisition-year-'+lastCurrencyId);
//         newElem.find('#electronicsfurnitures-currency').attr('id', 'electronicsfurnitures-currency-'+lastCurrencyId);
//         newElem.find('#electronicsfurnitures-foreign-value').attr('id', 'electronicsfurnitures-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-electronicsfurnitures-value').attr('id', 'hidden-electronicsfurnitures-value-'+lastCurrencyId);
//         newElem.find('#electronicsfurnitures-value').attr('id', 'electronicsfurnitures-value-'+lastCurrencyId);
//         newElem.find('#remove-electronicsfurnitures').attr('id', 'remove-electronicsfurnitures-'+lastCurrencyId);
//         newElem.find('.remove-electronicsfurnitures').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.electronicsfurnitures-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//             const idrId = 'electronicsfurnitures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.electronicsfurnitures-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'electronicsfurnitures-currency-' + numberId;
//             const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//             const idrId = 'electronicsfurnitures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.electronicsfurnitures-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'electronicsfurnitures-currency-' + numberId;
//             const acquisitionYearId = 'electronicsfurnitures-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'electronicsfurnitures-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-electronicsfurnitures-value-' + numberId;
//             const idrId = 'electronicsfurnitures-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-electronicsfurnitures').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-immovable-property
//     $('a.remove-oip').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-oip').click(function (){
//         const currencyId = $('.cloned-oip-data').find('.oip-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-oip').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.oip-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-oip-data"
//         }).css("display", "block").appendTo('.oip-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#oip-acquisition-year').attr('id', 'oip-acquisition-year-'+lastCurrencyId);
//         newElem.find('#oip-currency').attr('id', 'oip-currency-'+lastCurrencyId);
//         newElem.find('#oip-foreign-value').attr('id', 'oip-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-oip-value').attr('id', 'hidden-oip-value-'+lastCurrencyId);
//         newElem.find('#oip-value').attr('id', 'oip-value-'+lastCurrencyId);
//         newElem.find('#remove-oip').attr('id', 'remove-oip-'+lastCurrencyId);
//         newElem.find('.remove-oip').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.oip-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oip-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oip-value-' + numberId;
//             const idrId = 'oip-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.oip-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'oip-currency-' + numberId;
//             const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oip-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oip-value-' + numberId;
//             const idrId = 'oip-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.oip-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'oip-currency-' + numberId;
//             const acquisitionYearId = 'oip-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'oip-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-oip-value-' + numberId;
//             const idrId = 'oip-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-oip').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-movable-property
//     $('a.remove-omp').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-omp').click(function (){
//         const currencyId = $('.cloned-omp-data').find('.omp-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-omp').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.omp-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-omp-data"
//         }).css("display", "block").appendTo('.omp-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#omp-acquisition-year').attr('id', 'omp-acquisition-year-'+lastCurrencyId);
//         newElem.find('#omp-currency').attr('id', 'omp-currency-'+lastCurrencyId);
//         newElem.find('#omp-foreign-value').attr('id', 'omp-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-omp-value').attr('id', 'hidden-omp-value-'+lastCurrencyId);
//         newElem.find('#omp-value').attr('id', 'omp-value-'+lastCurrencyId);
//         newElem.find('#remove-omp').attr('id', 'remove-omp-'+lastCurrencyId);
//         newElem.find('.remove-omp').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.omp-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'omp-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-omp-value-' + numberId;
//             const idrId = 'omp-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.omp-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'omp-currency-' + numberId;
//             const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'omp-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-omp-value-' + numberId;
//             const idrId = 'omp-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.omp-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'omp-currency-' + numberId;
//             const acquisitionYearId = 'omp-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'omp-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-omp-value-' + numberId;
//             const idrId = 'omp-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-omp').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for account-receivable
//     $('a.remove-accreceivable').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-accreceivable').click(function (){
//         const currencyId = $('.cloned-accreceivable-data').find('.accreceivable-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-accreceivable').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.accreceivable-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-accreceivable-data"
//         }).css("display", "block").appendTo('.accreceivable-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#accreceivable-acquisition-year').attr('id', 'accreceivable-acquisition-year-'+lastCurrencyId);
//         newElem.find('#accreceivable-currency').attr('id', 'accreceivable-currency-'+lastCurrencyId);
//         newElem.find('#accreceivable-foreign-value').attr('id', 'accreceivable-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-accreceivable-value').attr('id', 'hidden-accreceivable-value-'+lastCurrencyId);
//         newElem.find('#accreceivable-value').attr('id', 'accreceivable-value-'+lastCurrencyId);
//         newElem.find('#remove-accreceivable').attr('id', 'remove-accreceivable-'+lastCurrencyId);
//         newElem.find('.remove-accreceivable').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.accreceivable-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'accreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//             const idrId = 'accreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.accreceivable-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'accreceivable-currency-' + numberId;
//             const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'accreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//             const idrId = 'accreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.accreceivable-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'accreceivable-currency-' + numberId;
//             const acquisitionYearId = 'accreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'accreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-accreceivable-value-' + numberId;
//             const idrId = 'accreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-accreceivable').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for affiliate-receivable
//     $('a.remove-affreceivable').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-affreceivable').click(function (){
//         const currencyId = $('.cloned-affreceivable-data').find('.affreceivable-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-affreceivable').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.affreceivable-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-affreceivable-data"
//         }).css("display", "block").appendTo('.affreceivable-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#affreceivable-acquisition-year').attr('id', 'affreceivable-acquisition-year-'+lastCurrencyId);
//         newElem.find('#affreceivable-currency').attr('id', 'affreceivable-currency-'+lastCurrencyId);
//         newElem.find('#affreceivable-foreign-value').attr('id', 'affreceivable-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-affreceivable-value').attr('id', 'hidden-affreceivable-value-'+lastCurrencyId);
//         newElem.find('#affreceivable-value').attr('id', 'affreceivable-value-'+lastCurrencyId);
//         newElem.find('#remove-affreceivable').attr('id', 'remove-affreceivable-'+lastCurrencyId);
//         newElem.find('.remove-affreceivable').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.affreceivable-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//             const idrId = 'affreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.affreceivable-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'affreceivable-currency-' + numberId;
//             const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//             const idrId = 'affreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.affreceivable-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'affreceivable-currency-' + numberId;
//             const acquisitionYearId = 'affreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affreceivable-value-' + numberId;
//             const idrId = 'affreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-affreceivable').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-receivable
//     $('a.remove-otherreceivable').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-otherreceivable').click(function (){
//         const currencyId = $('.cloned-otherreceivable-data').find('.otherreceivable-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-otherreceivable').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.otherreceivable-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-otherreceivable-data"
//         }).css("display", "block").appendTo('.otherreceivable-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#otherreceivable-acquisition-year').attr('id', 'otherreceivable-acquisition-year-'+lastCurrencyId);
//         newElem.find('#otherreceivable-currency').attr('id', 'otherreceivable-currency-'+lastCurrencyId);
//         newElem.find('#otherreceivable-foreign-value').attr('id', 'otherreceivable-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-otherreceivable-value').attr('id', 'hidden-otherreceivable-value-'+lastCurrencyId);
//         newElem.find('#otherreceivable-value').attr('id', 'otherreceivable-value-'+lastCurrencyId);
//         newElem.find('#remove-otherreceivable').attr('id', 'remove-otherreceivable-'+lastCurrencyId);
//         newElem.find('.remove-otherreceivable').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.otherreceivable-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//             const idrId = 'otherreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.otherreceivable-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'otherreceivable-currency-' + numberId;
//             const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//             const idrId = 'otherreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.otherreceivable-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'otherreceivable-currency-' + numberId;
//             const acquisitionYearId = 'otherreceivable-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherreceivable-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherreceivable-value-' + numberId;
//             const idrId = 'otherreceivable-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-otherreceivable').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for financial-institution-liability
//     $('a.remove-fil').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-fil').click(function (){
//         const currencyId = $('.cloned-fil-data').find('.fil-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-fil').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.fil-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-fil-data"
//         }).css("display", "block").appendTo('.fil-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#fil-acquisition-year').attr('id', 'fil-acquisition-year-'+lastCurrencyId);
//         newElem.find('#fil-currency').attr('id', 'fil-currency-'+lastCurrencyId);
//         newElem.find('#fil-foreign-value').attr('id', 'fil-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-fil-value').attr('id', 'hidden-fil-value-'+lastCurrencyId);
//         newElem.find('#fil-value').attr('id', 'fil-value-'+lastCurrencyId);
//         newElem.find('#remove-fil').attr('id', 'remove-fil-'+lastCurrencyId);
//         newElem.find('.remove-fil').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.fil-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'fil-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-fil-value-' + numberId;
//             const idrId = 'fil-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.fil-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'fil-currency-' + numberId;
//             const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'fil-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-fil-value-' + numberId;
//             const idrId = 'fil-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.fil-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'fil-currency-' + numberId;
//             const acquisitionYearId = 'fil-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'fil-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-fil-value-' + numberId;
//             const idrId = 'fil-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-fil').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for affiliate-liability
//     $('a.remove-affliability').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-affliability').click(function (){
//         const currencyId = $('.cloned-affliability-data').find('.affliability-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-affliability').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.affliability-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-affliability-data"
//         }).css("display", "block").appendTo('.affliability-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#affliability-acquisition-year').attr('id', 'affliability-acquisition-year-'+lastCurrencyId);
//         newElem.find('#affliability-currency').attr('id', 'affliability-currency-'+lastCurrencyId);
//         newElem.find('#affliability-foreign-value').attr('id', 'affliability-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-affliability-value').attr('id', 'hidden-affliability-value-'+lastCurrencyId);
//         newElem.find('#affliability-value').attr('id', 'affliability-value-'+lastCurrencyId);
//         newElem.find('#remove-affliability').attr('id', 'remove-affliability-'+lastCurrencyId);
//         newElem.find('.remove-affliability').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.affliability-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//             const idrId = 'affliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.affliability-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'affliability-currency-' + numberId;
//             const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//             const idrId = 'affliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.affliability-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'affliability-currency-' + numberId;
//             const acquisitionYearId = 'affliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'affliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-affliability-value-' + numberId;
//             const idrId = 'affliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-affliability').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });

// //js for other-liability
//     $('a.remove-otherliability').click(function (){
//         const id = $(this).attr('id');
//         const parts = id.split('-');
//         const idNumber = parts.pop();

//         const elemTarget = $('#clone-'+idNumber);
//         elemTarget.find('input[name^=deleted]').val(true);
//         elemTarget.find('input').removeAttr('required');
//         elemTarget.find('textarea').removeAttr('required');
//         elemTarget.find('select').removeAttr('required');
//         elemTarget.hide();
//     });

//     $('a#add-otherliability').click(function (){
//         const currencyId = $('.cloned-otherliability-data').find('.otherliability-currency').last().attr('id');
//         const parts = currencyId.split('-');
//         let lastCurrencyId = parseInt(parts.pop()) + 1;
//         $('#clone-'+ (lastCurrencyId-1)).find('.remove-otherliability').show();

//         lastCurrencyId = lastCurrencyId.toString();

//         const newElem = $('.otherliability-data').clone().attr({
//                 id: "clone-"+lastCurrencyId,
//                 class: "col-md-12 col-md-offset-2 cloned-otherliability-data"
//         }).css("display", "block").appendTo('.otherliability-form-container');

//         newElem.prepend('<hr class="col-md-12">');

//         newElem.find('#otherliability-acquisition-year').attr('id', 'otherliability-acquisition-year-'+lastCurrencyId);
//         newElem.find('#otherliability-currency').attr('id', 'otherliability-currency-'+lastCurrencyId);
//         newElem.find('#otherliability-foreign-value').attr('id', 'otherliability-foreign-value-'+lastCurrencyId);
//         newElem.find('#kurspajak').attr('id', 'kurspajak-'+lastCurrencyId);
//         newElem.find('#hidden-otherliability-value').attr('id', 'hidden-otherliability-value-'+lastCurrencyId);
//         newElem.find('#otherliability-value').attr('id', 'otherliability-value-'+lastCurrencyId);
//         newElem.find('#remove-otherliability').attr('id', 'remove-otherliability-'+lastCurrencyId);
//         newElem.find('.remove-otherliability').show();

//         newElem.find('input').attr('required', 'required');
//         newElem.find('textarea').attr('required', 'required');
//         newElem.find('select').attr('required', 'required');

//         newElem.find('.otherliability-currency').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//             const idrId = 'otherliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();
//             let kursPajakValue = 0;
//             let idrValue = 0;

//             let currencyName = $('#'+inputId+' option:selected').text();

//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+inputId).val(1);
//                 currencyName = $('#'+inputId+' option:selected').text();
//             }

//             if(isNaN(valasValue)){
//                 valasValue = 0.00;
//             }

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.otherliability-acquisition-year').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'otherliability-currency-' + numberId;
//             const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//             const idrId = 'otherliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.otherliability-foreign-value').bind( "change", function() {
//             const currentTime = new Date();
//             const prevYear = currentTime.getFullYear() - 1;
//             const prevMonth = '12';
//             const prevDate = '31';

//             const inputId = $(this).attr('id');
//             const parts = inputId.split('-');
//             const numberId = parts.pop();

//             const currencyId = 'otherliability-currency-' + numberId;
//             const acquisitionYearId = 'otherliability-acquisition-year-' + numberId;
//             const kursPajakId = 'kurspajak-' + numberId;
//             const valasId = 'otherliability-foreign-value-' + numberId;
//             const hiddenIdrId = 'hidden-otherliability-value-' + numberId;
//             const idrId = 'otherliability-value-' + numberId;
//             let valasValue = $('#'+valasId).val();

//             let currencyName = $('#'+currencyId+' option:selected').text();
//             if(
//                 (parseInt($('#'+acquisitionYearId).val()) < parseInt(prevYear))
//                     && currencyName != 'IDR'
//                 )
//             {
//                 alert('Tahun perolehan lebih kecil dari tahun pajak, mohon isi dengan mata uang IDR sesuai dengan SPT sebelumnya');
//                 $('#'+currencyId).val(1);
//                 currencyName = $('#'+currencyId+' option:selected').text();
//             }

//             let kursPajakValue = 0;
//             let idrValue = 0;

//             if(currencyName != 'IDR'){
//                 $.get(apiUrl + '/api/kurspajak?month='+prevMonth+'&date='+prevDate+'&year='+prevYear+'&currency='+currencyName
//                     , function(result, status){
//                         result = jQuery.parseJSON(result);
//                         kursPajakValue = parseFloat(result.kurspajak_value);
//                         idrValue = kursPajakValue * valasValue;

//                         $('#'+kursPajakId).val(kursPajakValue);
//                         $('#'+hiddenIdrId).val(idrValue);
//                         $('#'+idrId).val(idrValue);
//                 });
//             }
//             else {
//                 kursPajakValue = 1;
//                 idrValue = valasValue;

//                 $('#'+kursPajakId).val(kursPajakValue);
//                 $('#'+hiddenIdrId).val(idrValue);
//                 $('#'+idrId).val(idrValue);
//             }
//         });

//         newElem.find('.remove-otherliability').bind( "click", function() {
//             const id = $(this).attr('id');
//             const parts = id.split('-');
//             const idNumber = parts.pop();

//             const elemTarget = $('#clone-'+idNumber);
//             elemTarget.find('input[name^=deleted]').val(true);
//             elemTarget.find('input').removeAttr('required');
//             elemTarget.find('textarea').removeAttr('required');
//             elemTarget.find('select').removeAttr('required');
//             elemTarget.hide();
//         });

//     });
    
//#endregion

    $('#btn-show-security-question').click (function () {
        $('#modal_confirm_password').modal('show');
    });

    $('#form_security_question').submit(function(e) {
        e.preventDefault();
        $('#form_security_question').find('button[type=submit]').text('Please wait...');
        $('#form_security_question').find('button[type=submit]').prop('disabled', true);

        const password = $('#form_security_question').find('#password').val();
        $.ajaxSetup({
            headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        });
        $.ajax({
           url: '/profile/ajax/securityquestion',
           data: {password: password},
           type: 'post',
           dataType: 'json',
           success: function (result) {
               if (result == true) {
                    $('#modal_confirm_password').modal('hide');
                    $('#btn-show-security-question').hide();
                    $('#security_question').show(200);
               }
           },
           complete: function() {
                $('#form_security_question').find('#password').val('');
                $('#form_security_question').find('button[type=submit]').text('Konfirmasi');
                $('#form_security_question').find('button[type=submit]').prop('disabled', false);
           }
        });
        
    });


    $('a.btn-remove-user').click(function(){
        const id = $(this).attr('data-id');
        const name = $(this).attr('data-name');
        $.ajaxSetup({
            headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        });
        $.ajax({
            url : '/admin/users/delete',
            data : {id : id},
            type : 'delete',
            dataType : 'json',
            success : function(result){
                if (result.status == true){
                    location.reload();
                }
            }
        });
    });

//product
    $('a.btn-remove-product').click(function(){
        const id = $(this).attr('data-id');
        const name = $(this).attr('data-name');
        swal({
            title: "Konfirmasi Hapus",
            text: "Data yang dihapus tidak dapat dipulihkan kembali. Tetap lanjutkan mengapus " + name + "?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete){
                $.ajaxSetup({
                    headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
                });
                $.ajax({
                    url : '/admin/product/delete',
                    data : {id : id},
                    type : 'delete',
                    dataType : 'json',
                    success : function (result){
                        if (result.status == true){
                            location.reload();
                        }
                    }
                });
            }
        });
    });
    
    $('#btn_description_product').click(function(){
        var input = '<div class="form-group row"><div class="col-sm-12"><input type="text" id="description[]" name="description[]" class="form-control form-control-sm"></div></div>';
        $("#new_description").append(input);
    });

    $('a.btn-remove-bank-account').click(function(){
        const id = $(this).attr('data-id');

        swal({
            title: "Konfirmasi Hapus",
            text: "Data yang dihapus tidak dapat dipulihkan kembali. Tetap lanjutkan mengapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajaxSetup({
                    headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
                });
                $.ajax({
                    url : '/admin/bank-account/delete',
                    data : {id : id},
                    type : 'delete',
                    dataType : 'json',
                    success : function(result){
                        if (result.status == true){
                            location.reload();
                        }
                    }
               });
            }
        });
    });

    $('a.btn-remove-tooltip').click(function () {
        const id = $(this).attr('data-id');

        swal({
            title: "Konfirmasi Hapus",
            text: "Data yang dihapus tidak dapat dipulihkan kembali. Tetap lanjutkan menghapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajaxSetup({
                        headers: { "X-CSRF-Token": $("meta[name=csrf-token]").attr("content") }
                    });
                    $.ajax({
                        url: '/admin/tooltip-feature/delete',
                        data: { id: id },
                        type: 'delete',
                        dataType: 'json',
                        success: function (result) {
                            if (result.status == true) {
                                location.reload();
                            }
                        }
                    });
                }
            });
    });

    $('.btn-view-user-order-preview').click(function(){
        const id = $(this).attr('data-id');
        $.ajax({
            url : '/admin/user-order/show',
            data : {id:id},
            type : 'get',
            dataType :'json',
            success : function(result){
                if (result.status == true){
                    if (result.data.status != 'pending'){
                        $('#btn_accept_user_order_preview').hide();
                        $('#btn_reject_user_order_preview').hide();
                    }else{
                        $('#btn_accept_user_order_preview').show();
                        $('#btn_reject_user_order_preview').show();
                    }
                    $("#user_name").text(result.data.user_products.users.name);
                    $("#user_email").text(result.data.user_products.users.email);
                    $("#product_name").text(result.data.user_products.product.name);
                    $("#product_price").text("Rp. " + result.data.user_products.product.price.toString().replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                    $("#bank_name").text(result.data.bank_accounts.banks.name);
                    $("#account_number").text(result.data.bank_accounts.account_number);
                    $("#amount").text("Rp. " + result.data.amount.toString().replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                    $("#total_amount").text("Rp. " + result.data.total_amount.toString().replace(".", ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                    $("#promo").text((result.data.promo == 1) ? 'Ya' : 'Tidak');
                    $("#promo_description").text(result.data.promo_description);
                    $("#years").text(result.data.user_products.years.toString() + " Tahun");
                    $("#invites").text(result.data.user_products.product.invites.toString() + " Orang");
                    $("#id").val(id);
                    $("#unique").text(result.data.unique_code);
                    $("#year").val(result.data.user_products.years);
                    $(".image-wrapper").empty();
                    $('.image-wrapper').append('<img src="/invoice/'+ result.data.attachment +'" width="300px" />');
                    $('#modal_user_order_preview').modal('show');
                }
            }
        });
    });

    $('#btn_accept_user_order_preview').click(function(){
        $.ajaxSetup({
            headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        });
        $.ajax({
            url : '/admin/user-order/approve',
            type : 'post',
            data : {id : $("#id").val(), years : $("#year").val(), status : 'approved'},
            dataType : 'json',
            success : function (result){
                if (result.status == true){
                    location.reload();
                }
            }
        });
    });

    $('#btn_reject_user_order_preview').click(function(){
        $.ajaxSetup({
            headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
        });
        $.ajax({
            url : '/admin/user-order/approve',
            type : 'post',
            data : {id : $("#id").val(), years : $("#year").val(), status : 'rejected'},
            dataType : 'json',
            success : function (result){
                if (result.status == true){
                    location.reload();
                }
            }
        });
    });

    $('.btn-user-order-subscription').click(function(){
        $('#product_name').text('');
        $("#payment_status").text('');
        $("#product_price").text('');
        $("#quota_member").text('');

        $('#table_user_product_subscription tbody tr').remove();
        const id = $(this).attr('data-id');
        $("#parent_id").val(id);
        $.ajax({
            url : '/admin/users/partner',
            data : {id : id},
            type : 'get',
            dataType : 'json',
            success : function (result){
                if (result.status == true){
                    $("#product_name").text(result.data.data.product.name);
                    $("#quota_member").text(result.data.data.product.invites);
                    $("#product_price").text(result.data.data.product.price);
                    if (result.data.order.status != 'rejected'){
                        $("#payment_status").text(result.data.order.status);
                    }else{
                        $("#payment_status").text(' - ');
                    }
                }

                var isValid = true;
                var invites;
                if (result.data.data){
                    invites = result.data.data.product.invites;
                }
                if (result.status == true){
                    if (!invites){
                        isValid = false;
                    }else{
                        var quota = parseInt(result.data.data.product.invites) - 1; 
                        if (quota > 0){
                            for (var i = 0; i < quota; i++){
                                var button = '', email = '', id = 0, status = '', textbox = '', checkbox = '';
                                if (result.data.partner[i]){
                                    if (result.data.partner[i].email){
                                        email = result.data.partner[i].email;
                                    }
                                    if (result.data.partner[i].id){
                                        id = result.data.partner[i].id;
                                    }
                                    button = '<a href="javascript:void(0)" id="btn_user_product_invite_'+ i.toString() +'" class="btn btn-danger btn-sm btn-user-product-invite" data-id="'+ i.toString() +'" data-value="1" data-userproductid="'+ id +'"><i class="fa fa-close"></i></a>';
                                    textbox = '<input type="email" class="form-control form-control-sm" name="email_'+ i.toString() +'" id="email_'+ i.toString() +'" value="'+ email +'" disabled>';
                                    checkbox = '<input type="checkbox" name="wife_or_husband_'+ i.toString() +'" id="wife_or_husband_'+ i.toString() +'" disabled> Suami/Istri Saya';
                                }else{
                                    button = '<a href="javascript:void(0)" id="btn_user_product_invite_'+ i.toString() +'" class="btn btn-info btn-sm btn-user-product-invite" data-id="'+ i.toString() +'" data-value="0" data-userproductid="'+ id +'"><i class="fa fa-plus"></i></a>';
                                    textbox = '<input type="email" class="form-control form-control-sm" name="email_'+ i.toString() +'" id="email_'+ i.toString() +'" value="'+ email +'">';
                                    checkbox = '<input type="checkbox" name="wife_or_husband_'+ i.toString() +'" id="wife_or_husband_'+ i.toString() +'"> Suami/Istri Saya';
                                }
                                var html = '<tr>' +
                                    '<td width="5%">' +
                                        button +
                                    '</td>' +
                                    '<td width="20%">' +
                                        textbox +
                                    '</td>' +
                                    '<td width="10%">' +
                                        checkbox +
                                    '</td>' +
                                    '</tr>';
                                $("#table_user_product_subscription tbody").append(html);
                                
                            }
                        }else{
                            isValid = false; 
                        }
                    }
                    if (isValid == true){
                        $('#modal_user_product_subscription').modal('show');
                    }
                }
            }
        });
    });

    $('#table_user_product_subscription tbody').on('click', '.btn-user-product-invite', function(){
        var id = $(this).attr('data-id');
        var value = $(this).attr('data-value');
        var user_product_id = $(this).attr('data-userproductid');
        //var parent_id = $(this)
        var user_parent_id = $("#parent_id").val();

        var email = 'email_'+ id.toString();
        var couple = 'wife_or_husband_'+ id.toString();
        var button = $(this).attr('id');
        
        if (value == '1'){
            swal({
                title: "Konfirmasi Hapus",
                text: "Data yang dihapus tidak dapat dipulihkan kembali. Tetap lanjutkan mengapus?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajaxSetup({
                        headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
                    });
                    $.ajax({
                        url : '/admin/users/delete-partner',
                        data : {user_product_id : user_product_id},
                        type : 'delete',
                        dataType : 'json',
                        success : function(result){
                            if (result.status == true){
                                $("#"+button).attr('data-value', '0');
                                $("#"+button).removeClass('btn-danger');
                                $("#"+button).addClass('btn-info');
                                $("#"+button).empty();
                                $("#"+button).append('<i class="fa fa-plus"></i>');
                                $("#"+email).val('');
                                // $("#wife_or_husband_"+ id.toString()).removeAttr('disabled');
                                // $('#email_'+ id.toString()).removeAttr('readonly');
                                $("#"+email).prop('disabled', true);
                                $("#"+couple).prop('disabled', true)
                            }
                        }
                    });
                }
            });
        }else{
            var filterEmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            var inputEmail = $("#"+email).val();
            if ($.trim(inputEmail).length == 0){
                swal('Ups!', 'Silahkan masukkan email.', 'warning');
                return;
            }

            if (!filterEmail.test(inputEmail)){
                swal('Ups!', 'Format email salah.', 'warning');
                return;
            }

            var wife_or_husband = 0;
            if ($('#'+ couple).is(":checked"))
            {
                wife_or_husband = 1;
            }

            $.ajaxSetup({
                headers: { "X-CSRF-Token" : $("meta[name=csrf-token]").attr("content") }
            });
            $.ajax({
                url : '/admin/users/new-partner',
                data : {user_parent_id : user_parent_id, wife_or_husband : wife_or_husband, email : inputEmail},
                type : 'post',
                dataType : 'json',
                success : function (result){
                    if (result.status == true){
                        $("#"+button).attr('data-value', '1');
                        $("#"+button).removeClass('btn-info');
                        $("#"+button).addClass('btn-danger');
                        $("#"+button).empty();
                        $("#"+button).append('<i class="fa fa-close"></i>');

                        $("#"+email).prop('disabled', true);
                        $("#"+couple).prop('disabled', true)
                        
                        swal('Sukses!', result.message, 'success');
                    }else{
                        if (result.error){
                            swal('Ups!', result.message, 'danger');
                        }else{
                            swal('Ups!', result.message, 'warning');
                        }
                    }
                }
            });
            
        }

    });

//#region my tax profile
    $("#my-tax-profile #btn_mytax_info_popup").click(function(){
       $('#modal_my_tax_profile_popup').modal('show');
    });
//#endregion

//#region spouse indicator
    $('#spouse-indicator').on('click', '#yes', function(){
        $('#invite-spouse#spouse-email').val('');
        $('#modal_spouse_invite').modal('show');
    });

    $('#invite-spouse').on('submit', function(e){
        e.preventDefault();
        $email = $('#invite-spouse#spouse-email').val();
        var filterEmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (!filterEmail.test(email)){
            swal('Ups!', 'Format email salah.', 'warning');
            return;
        }
        
    });
//#endregion

//#region other information - bukti potong atas pembelian barang mewah
    $('#luxury-goods').on('click', '#btn-luxury-goods-yes', function(e){
        e.preventDefault();
        $('#form-luxury-goods-notification').hide();
        $('#form-luxury-goods-input').show();
    });
//#endregion

//#region pph 25 information
    $('#pph25-information1').on('click', '#btn-pph25-information1-yes', function(e){
        e.preventDefault();
        $('#form-pph25-information1-notification').hide();
        $('#form-pph25-information1-input').show();
    });
//#endregion

//#region spouse tax profile
    $('#spouse-tax-profile #btn_mytax_info_popup').click(function(){
        $('#modal_my_spouse_profile_popup').modal('show');
    });
//#endregion region

//#js for additioal income sale of shares
    $('#sale-of-shares').on('click', '#btn_popup_founder', function(e){
        swal('Saham Pendiri', 'Yang termasuk dalam pengertian saham pendiri adalah saham yang dimiliki oleh para pendiri yang tercantum dalam anggaran dasar pada saat perusahaan didirikan.', 'info');
    });
//#endregion

//#region spouse income option 4
    $('#invite-spouse-option-4').on('submit', function(e){
        e.preventDefault();
        $('#modal_spouse_invite_option_4').modal('hide'); 
    });

    $('#invite-spouse-button').click(function(e){
        e.preventDefault();
        $('#email-spouse').val('');
        $('#modal_spouse_invite_option_4').modal('show');
    });

    $('#fill-spouse-income-button').click(function(e){
        e.preventDefault();
        $('#send-notification-form').hide();
        $('#fill-spouse-income-form').show();
    });
//#endregion

    ///////////////warisan atau hibah/////////////
    $('a#add-tab-legacy').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        console.log(next_tab_id);
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#legacy-form-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#legacy-value').attr('name', 'nilai['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#legacy-giver').attr('name', 'pemberi['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' input.form-control')[0].setAttribute('id','legacy-value_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        var $regexvalue=/^[0-9]*$/;


        $('#legacy-value_'+next_tab_id).on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#legacy-value_'+next_tab_id).css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
        else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#legacy-value_'+next_tab_id).css({ 'background-color': "#ffffff" });
            }
        });
        $('#legacy-value_'+next_tab_id).on('focusout',function(){
            $('#legacy-value_'+next_tab_id).addClass('input-money-format')
            $('#legacy-value_'+next_tab_id).mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });
    
    ///////////////////end of warisan///////////////




    /////////////////Pengalihan hak tanah bangunan///////
    $('a#add-tab-transfer-building-rights').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-transfer-building-rights-form-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#transfer-bulding-rights-value').attr('name', 'value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#transfer-bulding-rights-address').attr('name', 'address['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#transfer-bulding-rights-accepted').attr('name', 'accepted['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#transfer-bulding-rights-final').attr('name', 'final['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)
       
        $('#tab'+next_tab_id+' #transfer-bulding-rights-value').on('focusout', function(e){
            var netto = document.querySelectorAll('#tab'+next_tab_id+' #transfer-bulding-rights-value')[0].value
            var tax = netto * 0.025
           
            $('#tab'+next_tab_id+' #transfer-bulding-rights-received').addClass('input-money-format')
            $('#tab'+next_tab_id+' #transfer-bulding-rights-final').addClass('input-money-format')

            if(!isNaN(tax)){
                var received = document.querySelectorAll('#tab'+next_tab_id+' #transfer-bulding-rights-received')[0].value = Math.floor(netto - tax);
                document.querySelectorAll('#tab'+next_tab_id+' #transfer-bulding-rights-final')[0].value = Math.floor(netto - received);
            }

            $('#tab'+next_tab_id+' #transfer-bulding-rights-received').mask('#.##0', {reverse: true});
            $('#tab'+next_tab_id+' #transfer-bulding-rights-final').mask('#.##0', {reverse: true});
        });

        var $regexvalue=/^[0-9]*$/;
        $('#tab'+next_tab_id+' #transfer-bulding-rights-value').on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#tab'+next_tab_id+' #transfer-bulding-rights-value').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
        else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#tab'+next_tab_id+' #transfer-bulding-rights-value').css({ 'background-color': "#ffffff" });
            }
        });

        $('#tab'+next_tab_id+' #transfer-bulding-rights-value').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' #transfer-bulding-rights-value').on('focusout',function(){
            $('#tab'+next_tab_id+' #transfer-bulding-rights-value').addClass('input-money-format')
            $('#tab'+next_tab_id+' #transfer-bulding-rights-value').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    ///////////////Scholarship////////////////
    $('a#add-tab-scholarship').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#scholarship-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#scholarship-value').attr('name', 'value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#scholarship-giver').attr('name', 'giver['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        var $regexvalue=/^[0-9]*$/;
        $('#tab'+next_tab_id+' input#scholarship-value').on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#tab'+next_tab_id+' input#scholarship-value').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
            else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#tab'+next_tab_id+' input#scholarship-value').css({ 'background-color': "#ffffff" });
            }
        });

        $('#tab'+next_tab_id+' input#scholarship-value').on('focusout',function(){
            $('#tab'+next_tab_id+' input#scholarship-value').addClass('input-money-format')
            $('#tab'+next_tab_id+' input#scholarship-value').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' input#scholarship-value').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    ///////////////Honorarium Apbdn////////////////
    $('a#add-tab-honorarium-apbdn').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#honorarium-apbdn-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-apbdn-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-apbdn-name').attr('name', 'name['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-apbdn-type').attr('name', 'type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-apbdn-bruto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-apbdn-final').attr('name', 'final['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#tab'+next_tab_id+' #honorarium-apbdn-type').on('change',function(){
            var tax;
            $('.input-money-format').unmask();
            var type = document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-type option:checked')[0].value

            if(parseInt(type) == 1 || parseInt(type) == 2){
                document.querySelector('#tab'+next_tab_id+' #tarif').value = '0%' 
                tax = 0
            }else if(parseInt(type) == 3){
                document.querySelector('#tab'+next_tab_id+' #tarif').value = '5%' 
                tax = 5
            }else if(parseInt(type) == 4){
                document.querySelector('#tab'+next_tab_id+' #tarif').value = '15%' 
                tax = 15
            }

            var netto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-netto')[0].value)
            
            $('#tab'+next_tab_id+' #honorarium-apbdn-netto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-apbdn-bruto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-apbdn-final').addClass('input-money-format')

            if(!isNaN(netto)){
                var tax = document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-final')[0].value = Math.floor(netto * (tax/100))
                document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-bruto')[0].value = Math.floor(netto + tax)
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #honorarium-apbdn-netto').on('focusout',function(){
            var netto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-netto')[0].value)
            var tax = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #tarif')[0].value)
            
            $('#tab'+next_tab_id+' #honorarium-apbdn-netto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-apbdn-bruto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-apbdn-final').addClass('input-money-format')

            if(!isNaN(netto)){
                var tax = document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-final')[0].value = Math.floor(netto * (tax/100))
                document.querySelectorAll('#tab'+next_tab_id+' #honorarium-apbdn-bruto')[0].value = Math.floor(netto + tax)
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #honorarium-apbdn-netto').on('focus',function(){
            $('.input-money-format').unmask();
        });
        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });
    ///////////////End Honorarium Apbdn//////////

    /////////////Honorarium comissioner////////////
    $('a#add-honorarium-commissioner').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-honorarium-commissioner').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-pph').attr('name', 'pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-cutter-name').attr('name', 'cutter-name['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-cutter-npwp').attr('name', 'npwp['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-pph-proof').attr('name', 'proof['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#honorarium-commissioner-pph-date').attr('name', 'date['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#tab'+next_tab_id+' #honorarium-commissioner-brutto , #tab'+next_tab_id+' #honorarium-commissioner-pph').on('focusout',function(){
            var brutto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #honorarium-commissioner-brutto')[0].value)
            var pph = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #honorarium-commissioner-pph')[0].value)
            
            $('#tab'+next_tab_id+' #honorarium-commissioner-brutto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-commissioner-pph').addClass('input-money-format')
            $('#tab'+next_tab_id+' #honorarium-commissioner-netto').addClass('input-money-format')

            if(!isNaN(brutto) && !isNaN(pph)){
                if(brutto >= pph){
                    document.querySelectorAll('#tab'+next_tab_id+' #honorarium-commissioner-netto')[0].value = Math.floor(brutto - pph)
                }else{
                    document.querySelectorAll('#tab'+next_tab_id+' #honorarium-commissioner-netto')[0].value = Math.floor(0)
                }
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #honorarium-commissioner-cutter-npwp').on('keypress keydown keyup',function(){
            $('#tab'+next_tab_id+' #honorarium-commissioner-cutter-npwp').addClass('input-npwp-format')
            $('.input-npwp-format').mask('##.###.###.#-###.###');
        });

        $('#tab'+next_tab_id+' #honorarium-commissioner-brutto , #tab'+next_tab_id+' #honorarium-commissioner-pph').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' #honorarium-commissioner-pph-date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });
    ///////////////End Honorarium comissioner//////////

    /////////////Casual laborer////////////
    $('a#add-casual-laborer').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-casual-laborer').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-pph').attr('name', 'pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-cutter-name').attr('name', 'cutter-name['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-cutter-npwp').attr('name', 'npwp['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-pph-proof').attr('name', 'proof['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#casual-laborer-pph-date').attr('name', 'date['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#tab'+next_tab_id+' #casual-laborer-brutto , #tab'+next_tab_id+' #casual-laborer-pph').on('focusout',function(){
            var brutto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #casual-laborer-brutto')[0].value)
            var pph = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #casual-laborer-pph')[0].value)
            
            $('#tab'+next_tab_id+' #casual-laborer-brutto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #casual-laborer-pph').addClass('input-money-format')
            $('#tab'+next_tab_id+' #casual-laborer-netto').addClass('input-money-format')

            if(!isNaN(brutto) && !isNaN(pph)){
                if(brutto >= pph){
                    document.querySelectorAll('#tab'+next_tab_id+' #casual-laborer-netto')[0].value = Math.floor(brutto - pph)
                }else{
                    document.querySelectorAll('#tab'+next_tab_id+' #casual-laborer-netto')[0].value = Math.floor(0)
                }
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #casual-laborer-cutter-npwp').on('keypress keydown keyup',function(){
            $('#tab'+next_tab_id+' #casual-laborer-cutter-npwp').addClass('input-npwp-format')
            $('.input-npwp-format').mask('##.###.###.#-###.###');
        });

        $('#tab'+next_tab_id+' #casual-laborer-brutto , #tab'+next_tab_id+' #casual-laborer-pph').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' #casual-laborer-pph-date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });
    ///////////////End Casual laborer//////////

    ///////////////Bantuan / Sumbangan////////////////
    $('a#add-help-or-donation').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#help-or-donation-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#help-or-donation-value').attr('name', 'value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#help-or-donation-name').attr('name', 'giver['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        var $regexvalue=/^[0-9]*$/;
        $('#tab'+next_tab_id+' #help-or-donation-value').on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#tab'+next_tab_id+' #help-or-donation-value').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
        else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#tab'+next_tab_id+' #help-or-donation-value').css({ 'background-color': "#ffffff" });
            }
        });

        $('#tab'+next_tab_id+' #help-or-donation-value').on('focusout',function(){
            $('#tab'+next_tab_id+' #help-or-donation-value').addClass('input-money-format')
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #help-or-donation-value').on('focus',function(){
            $('.input-money-format').unmask();
        });
        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    

    ///////////////Pesangon/Tunjangan Hari Tua///////////
    $('a#add-tab-severance-pay').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-severance-pay-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        // $('#tab'+next_tab_id+' input[type="radio"]').attr('name', 'role['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#severance-pay-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#severance-pay-corp').attr('name', 'corporation['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#severance-pay-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#severance-pay-final').attr('name', 'final['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#tarif').attr('name', 'tarif['+next_tab_id+']');
        // $('#tab'+next_tab_id+' input#transfer-bulding-rights-final').attr('name', 'final['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[0].setAttribute('id','severance-pay-brutto_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[2].setAttribute('id','severance-pay-netto_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[4].setAttribute('id','severance-pay-final_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[3].setAttribute('id','tarif_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' select')[0].setAttribute('id','severance-pay-type_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#severance-pay-type_'+next_tab_id).attr('name', 'type['+next_tab_id+']');

        $('#severance-pay-type_'+next_tab_id).on('change', function(e){
            document.querySelectorAll('#severance-pay-brutto_'+next_tab_id)[0].value = ''
            document.querySelectorAll('#severance-pay-netto_'+next_tab_id)[0].value = ''
            document.querySelectorAll('#severance-pay-final_'+next_tab_id)[0].value = ''
        });

        $('#severance-pay-brutto_'+next_tab_id).on('keypress keydown keyup',function(){
            
            var netto = parseInt($(this).val())

            var type = document.querySelectorAll('#severance-pay-type_'+next_tab_id+' option:checked')[0].value
           
            if(parseInt(type) == 1){
                if(netto < 50000000){
                    document.getElementById('tarif_'+next_tab_id).value = '0' 
                }else if(netto >= 50000000 && netto < 100000000){
                    document.getElementById('tarif_'+next_tab_id).value = '5' 
                }else if(netto >= 100000000 && netto < 500000000){
                    document.getElementById('tarif_'+next_tab_id).value = '15' 
                }else if(netto >= 500000000){
                    document.getElementById('tarif_'+next_tab_id).value = '25' 
                }else{
                    document.getElementById('tarif_'+next_tab_id).value = '0' 
                }
            }else if(parseInt(type) == 3){
                if(netto < 50000000){
                    document.getElementById('tarif_'+next_tab_id).value = '5' 
                }else if(netto >= 50000000 && netto < 250000000){
                    document.getElementById('tarif_'+next_tab_id).value = '15' 
                }else if(netto >= 250000000 && netto < 500000000){
                    document.getElementById('tarif_'+next_tab_id).value = '25' 
                }else if(netto >= 500000000){
                    document.getElementById('tarif_'+next_tab_id).value = '30' 
                }else{
                    document.getElementById('tarif_'+next_tab_id).value = '5' 
                }    
            }else if(parseInt(type) == 2){
                document.getElementById('tarif_'+next_tab_id).value = '5' 
            }
        
        });

        $('#tab'+next_tab_id+' #severance-pay-brutto_'+next_tab_id).on('focusout', function(e){
            var brutto = document.querySelectorAll('#tab'+next_tab_id+' #severance-pay-brutto_'+next_tab_id)[0].value
            var select = document.querySelectorAll('#tab'+next_tab_id+' option:checked')[0].value

            $('#tab'+next_tab_id+' #severance-pay-brutto_'+next_tab_id).addClass('input-money-format')
            $('#tab'+next_tab_id+' #severance-pay-netto_'+next_tab_id).addClass('input-money-format')
            $('#tab'+next_tab_id+' #severance-pay-final_'+next_tab_id).addClass('input-money-format')

            if(brutto.length !== 0 && parseInt(select) !== 0){
                var tax_rate = document.querySelectorAll('#tab'+next_tab_id+' #tarif_'+next_tab_id)[0].value
                var tax = brutto * (parseInt(tax_rate)/100)
                var received = document.querySelectorAll('#tab'+next_tab_id+' #severance-pay-netto_'+next_tab_id)[0].value = Math.floor(brutto - tax)
                document.querySelectorAll('#tab'+next_tab_id+' #severance-pay-final_'+next_tab_id)[0].value = Math.floor(brutto - received)
            }
            
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#severance-pay-brutto_'+next_tab_id).on('focus',function(){
            $('.input-money-format').unmask();
        });
        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    ///////////////Hadiah dan Undian////////
    $('a#add-tab-prize-lottery').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-prize-lottery-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input#prize-lottery-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#prize-lottery-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#prize-lottery-name').attr('name', 'name['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#prize-lottery-final').attr('name', 'final['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#tab'+next_tab_id+' #prize-lottery-brutto').on('focusout', function(e){
            var bruto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #prize-lottery-brutto')[0].value)
           
            $('#tab'+next_tab_id+' #prize-lottery-brutto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #prize-lottery-final').addClass('input-money-format')
            $('#tab'+next_tab_id+' #prize-lottery-netto').addClass('input-money-format')

            if(!isNaN(bruto)){
                var tax = document.querySelectorAll('#tab'+next_tab_id+' #prize-lottery-final')[0].value = Math.floor(bruto * 0.25)
                document.querySelectorAll('#tab'+next_tab_id+' #prize-lottery-netto')[0].value = Math.floor(bruto - tax)
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #prize-lottery-brutto').on('focus',function(){
            $('.input-money-format').unmask();
        });
        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });


    ///////////////Penghargaan dan Hadiah/////////////
    $('a#add-awards-and-gift-form').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#awards-and-gift-form-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input.custom-control-input').attr('name', 'type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-cutter').attr('name', 'cutter['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-cutter-npwp').attr('name', 'cutter-npwp['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-proof-pph').attr('name', 'proof_pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-date-pph').attr('name', 'date_pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-type-pph').attr('name', 'type_pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-taken-pph').attr('name', 'taken_pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#awards-and-gift-tarif').attr('name', 'tarif['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)
        // document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[0].setAttribute('id','severance-pay-netto_'+next_tab_id)
        // document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[3].setAttribute('id','tarif_'+next_tab_id)

        $('#awards-and-gift-form #tab'+next_tab_id+' input.custom-control-input').on('change', function(e){
            if(document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input.custom-control-input')[0].checked){
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[0].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[1].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[2].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[3].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[4].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[5].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[6].setAttribute('readonly',true)
            }else{
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[0].setAttribute('readonly',true)
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[1].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[2].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[3].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[4].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[5].removeAttribute('readonly')
                // document.querySelectorAll('#awards-and-gift-form #tab1 input[type="text"]')[6].removeAttribute('readonly')
                document.querySelectorAll('#awards-and-gift-form #tab'+next_tab_id+' input[type="text"]')[8].value = ''
            }
        });

        $('#tab'+next_tab_id+' #awards-and-gift-brutto , #tab'+next_tab_id+' #awards-and-gift-netto, #tab'+next_tab_id+' #awards-and-gift-taken-pph').on('focusout',function(){
            $('.input-money-format').unmask();
            
            var type = parseInt(document.querySelector('#tab'+next_tab_id+' input[name="type['+next_tab_id+']"]:checked').value)
            
            if(type == 2){
                var netto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #awards-and-gift-netto')[0].value)
                var tax = 5
                if(netto < 50000000){
                    tax = 5
                }else if(netto >= 50000000 && netto < 250000000){
                    tax = 15
                }else if(netto >= 250000000 && netto < 500000000){
                    tax = 25
                }else if(netto >= 500000000){
                    tax = 30
                }    

                document.querySelectorAll('#tab'+next_tab_id+' #awards-and-gift-tarif')[0].value = tax+'%'
                
                if(!isNaN(netto) ){
                    var brutto = document.querySelectorAll('#tab'+next_tab_id+' #awards-and-gift-brutto')[0].value = Math.floor(netto / (1-(tax/100))) 
                    document.querySelectorAll('#tab'+next_tab_id+' #awards-and-gift-taken-pph')[0].value = Math.floor(brutto - netto) 
                } 
            }

            $('#tab'+next_tab_id+' #awards-and-gift-brutto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #awards-and-gift-taken-pph').addClass('input-money-format')
            $('#tab'+next_tab_id+' #awards-and-gift-netto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #awards-and-gift-cutter-npwp').addClass('input-npwp-format')

        
            $('.input-npwp-format').mask('##.###.###.#-###.###');
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' #awards-and-gift-brutto, #tab'+next_tab_id+' #awards-and-gift-netto').on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' #awards-and-gift-date-pph').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });
 
    ///////////////Non Building Transfer////////////
    $('a#add-tab-non-building-transfer-profit').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#non-building-transfer-profit-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[0].setAttribute('id','non-building-transfer-profit-trans_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[1].setAttribute('id','non-building-transfer-profit-get_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[2].setAttribute('id','non-building-transfer-profit-income_'+next_tab_id)
        document.querySelectorAll('#tab'+next_tab_id+' input[type="text"]')[3].setAttribute('id','non-building-transfer-profit-name_'+next_tab_id)
        $('#tab'+next_tab_id+' input#non-building-transfer-profit-trans_'+next_tab_id).attr('name', 'trans_value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#non-building-transfer-profit-get_'+next_tab_id).attr('name', 'get_value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#non-building-transfer-profit-income_'+next_tab_id).attr('name', 'income_value['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#non-building-transfer-profit-name_'+next_tab_id).attr('name', 'name['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $("#non-building-transfer-profit-get_"+next_tab_id+", #non-building-transfer-profit-trans_"+next_tab_id).on('focusout', function(e){
            var trans = parseInt(document.querySelectorAll('#non-building-transfer-profit-trans_'+next_tab_id)[0].value)
            var get = parseInt(document.querySelectorAll('#non-building-transfer-profit-get_'+next_tab_id)[0].value)
           
            $('#non-building-transfer-profit-get_'+next_tab_id).addClass('input-money-format')
            $('#non-building-transfer-profit-trans_'+next_tab_id).addClass('input-money-format')
            $('#non-building-transfer-profit-income_'+next_tab_id).addClass('input-money-format')

            if(!isNaN((trans)) && !isNaN((get))){
                if(get > trans){
                    // $('.btn.btn-info.btn-lg').attr("disabled", true);
                    document.querySelectorAll('#non-building-transfer-profit-income_'+next_tab_id)[0].value = 0
                }else{
                    $('.btn.btn-info.btn-lg').removeAttr('disabled');
                    $('#non-building-transfer-profit-get_'+next_tab_id).css({ 'background-color': "#ffffff" });
                    document.querySelectorAll('#non-building-transfer-profit-income_'+next_tab_id)[0].value = trans - get
                }
            }
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $("#non-building-transfer-profit-get_"+next_tab_id+" ,#non-building-transfer-profit-trans_"+next_tab_id).on('focus',function(){
            $('.input-money-format').unmask();
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });


    ///////////////Sale of shares////////////
    $('a#add-sale-of-shares').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#sale-of-shares-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input#sale-of-shares-stock').attr('name', 'stock['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#sale-of-shares-price').attr('name', 'price['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#sale-of-shares-profit').attr('name', 'profit['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#sale-of-shares-corporation').attr('name', 'corporation['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#tab'+next_tab_id+' #sale-of-shares-stock, #tab'+next_tab_id+' #sale-of-shares-price').on('focusout', function(e){
            var stock = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #sale-of-shares-stock')[0].value)
            var price = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #sale-of-shares-price')[0].value)
           
            $('#tab'+next_tab_id+' #sale-of-shares-stock').addClass('input-money-format')
            $('#tab'+next_tab_id+' #sale-of-shares-price').addClass('input-money-format')
            $('#tab'+next_tab_id+' #sale-of-shares-profit').addClass('input-money-format')

            if(!isNaN((stock)) && !isNaN((price))){
                if(price < stock){
                    document.querySelectorAll('#tab'+next_tab_id+' #sale-of-shares-profit')[0].value =0
                }else{
                    $('.btn.btn-info.btn-lg').removeAttr('disabled');
                    $('#tab'+next_tab_id+' #sale-of-shares-price').css({ 'background-color': "#ffffff" });
                    document.querySelectorAll('#tab'+next_tab_id+' #sale-of-shares-profit')[0].value = Math.floor(price - stock)
                }
            } 

            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#sale-of-shares-stock, #sale-of-shares-price').on('focus', function(e){
            $('.input-money-format').unmask();
        });
        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    ////////////////accepted building/////////////
    $('a#add-tab-accepted-building').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-accepted-building-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input#accepted-building-final').attr('name', 'final['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#accepted-building-njop').attr('name', 'njop['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#accepted-building-address').attr('name', 'address['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        var $regexvalue=/^[0-9]*$/;

        $('#tab'+next_tab_id+' #accepted-building-njop').on('keypress keydown keyup',function(){
            if (!$(this).val().match($regexvalue)) {
            // there is a mismatch, hence show the error message
                $('.btn.btn-info.btn-lg').attr("disabled", true);
                $('#tab'+next_tab_id+' #accepted-building-njop').css({ 'background-color': "rgba(255, 0, 0, 0.1)" });
            }
        else{
                // else, do not display message
                $('.btn.btn-info.btn-lg').removeAttr('disabled');
                $('#tab'+next_tab_id+' #accepted-building-njop').css({ 'background-color': "#ffffff" });
            }
        });

        $('#tab'+next_tab_id+' #accepted-building-njop').on('focusout',function(){
       
            var njop = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #accepted-building-njop')[0].value)
            var tax = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #tarif')[0].value)
            console.log(njop)
    
            $('#tab'+next_tab_id+' #accepted-building-njop').addClass('input-money-format')
            $('#tab'+next_tab_id+' #accepted-building-final').addClass('input-money-format')
    
            if(!isNaN(njop)){
                document.querySelectorAll('#tab'+next_tab_id+' #accepted-building-final')[0].value = Math.floor(njop * tax/100)
            } 
    
            $('.input-money-format').mask('#.##0', {reverse: true});
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });

        $('#tab'+next_tab_id+' #accepted-building-njop').on('focus',function(){
            $('#tab'+next_tab_id+' #accepted-building-njop').unmask();
        });
    });
    //////////////End of accepted building////////////

    /////////////another once earning////////////
    $('a#add-tab-another-once-earning').click(function(e){
        var last_tab_id = document.querySelectorAll('.tab').length;
        var next_tab_id = parseInt((document.querySelectorAll('#tabs li')[last_tab_id - 1].innerText).replace( /^\D+/g, ''));
        var next_tab_id = next_tab_id + 1;
        $("#tabs").append('<li id="li'+next_tab_id+'"><a href="#tab'+next_tab_id+'">#'+next_tab_id+'</a></li>');
        $('#tab-content').append('<div id="tab'+next_tab_id+'" class="tab"> </div>');
        $('#tab-another-earning-template').clone().removeAttr('id').show().appendTo('#tab'+next_tab_id);
        $('#tab'+next_tab_id+' input.custom-control-input').attr('name', 'type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-type').attr('name', 'earning_type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-brutto').attr('name', 'brutto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-netto').attr('name', 'netto['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-cutter-name').attr('name', 'cutter-name['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-cutter-npwp').attr('name', 'cutter-npwp['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-pph-proof').attr('name', 'pph-proof['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-pph-date').attr('name', 'pph-date['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-type-pph').attr('name', 'type_pph['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-pph-type').attr('name', 'pph-type['+next_tab_id+']');
        $('#tab'+next_tab_id+' input#another-earning-pph-taken').attr('name', 'pph-taken['+next_tab_id+']');
        document.querySelectorAll('#tab'+next_tab_id+' #remove_tab')[0].setAttribute('data-ai',next_tab_id)

        $('#another-earning-form #tab'+next_tab_id+' input.custom-control-input').on('change', function(e){
            if(document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input.custom-control-input')[0].checked){
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[2].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[3].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[4].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[5].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[6].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[7].setAttribute('readonly',true)
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[8].setAttribute('readonly',true)
            }else{
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[2].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[3].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[4].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[5].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[6].removeAttribute('readonly')
                // document.querySelectorAll('#awards-and-gift-form #tab1 input[type="text"]')[6].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[7].removeAttribute('readonly')
                document.querySelectorAll('#another-earning-form #tab'+next_tab_id+' input[type="text"]')[8].removeAttribute('readonly')
            }
        });

        $('#tab'+next_tab_id+' #another-earning-brutto , #tab'+next_tab_id+' #another-earning-netto, #tab'+next_tab_id+' #another-earning-pph-taken').on('focusout',function(){
            $('.input-money-format').unmask();
            
            var type = parseInt(document.querySelector('#another-earning-form #tab'+next_tab_id+' input[name="type['+next_tab_id+']"]:checked').value)
            console.log(type)
            if(type == 2){
                var brutto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #another-earning-brutto')[0].value)
                var netto = parseInt(document.querySelectorAll('#tab'+next_tab_id+' #another-earning-netto')[0].value)
                
                if(!isNaN(netto) && !isNaN(brutto)){
                    if(brutto >= netto){
                        document.querySelectorAll('#tab'+next_tab_id+' #another-earning-pph-taken')[0].value = Math.floor(brutto - netto) 
                    }else{
                        document.querySelectorAll('#tab'+next_tab_id+' #another-earning-pph-taken')[0].value = 0 
                    }
                } 
            }
    
            $('#tab'+next_tab_id+' #another-earning-brutto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #another-earning-pph-taken').addClass('input-money-format')
            $('#tab'+next_tab_id+' #another-earning-netto').addClass('input-money-format')
            $('#tab'+next_tab_id+' #another-earning-cutter-npwp').addClass('input-npwp-format')
    
            $('.input-money-format').mask('#.##0', {reverse: true});
            $('.input-npwp-format').mask('##.###.###.#-###.###');
        });
    
        $('#tab'+next_tab_id+' #another-earning-brutto, #tab'+next_tab_id+' #another-earning-netto').on('focus',function(){
            $('.input-money-format').unmask();
            $('.input-npwp-format').unmask();
        });
    
        $('#tab'+next_tab_id+' #another-earning-pph-date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#tab'+next_tab_id+' a#remove_tab').click(function(e){
            removeTab(e)
        });
    });

    /////////////////Another one time earning//////////////
    $('form#additional-earning-step').on('click','#earning-info',function(e){
        console.log(e)
        var current_button = $(e.target).parents('div.col-md-4');
        var input = current_button.find('.button-checkbox input')
        var id = parseInt(input[0].name)
        
        switch (id) {
            case 71:
                swal("Warisan / Hibah","Warisan / Hibah Undang-undang Republik Indonesia Nomor 36 Tahun 2008 tentang Pajak Penghasilan " +
                "sebagaimana tertuang dalam Undang-Undang Nomor 7 tahun 1983 menyebutkan bahwa yang menjadi " +
                "subjek pajak adalah harta waris yang belum dibagi (harta waris yang menghasilkan tambahan ekonomi). " +
                "Dalam aturan tersebut harta hibah tidak menjadi objek pajak dan harta waris yang sudah dibagi kepada ahli " +
                "warisnya juga tidak menjadi objek pajak. Namun, ahli waris diharuskan untuk mengurus surat hibah atau " +
                "waris tersebut.")
            break;

            case 72:
                swal("Pengalihan Hak atas Tanah dan Bangunan","Pengalihan Hak atas Tanah dan Bangunan adalah pendapatan yang dihasilkan dari penjualan tanah.")
            break;

            case 73:
                swal("Keuntungan dari Pengalihan Harta Selain Tanah dan Bangunan","Keuntungan dari Pengalihan Harta Selain Tanah dan Bangunan misal penjualan Mesin, mobil, barang antik")
            break;

            case 75:
                swal("Hadiah & undian","Hadiah Undian adalah hadiah dengan nama dan dalam bentuk apapun yang diberikan melalui undian.")
            break;

            case 76:
                swal("Beasiswa","Beasiswa adalah pemberian berupa bantuan keuangan yang diberikan kepada perorangan yang bertujuan "+
                    "untuk digunakan demi keberlangsungan pendidikan yang ditempuh")
            break;

            case 77:
                swal("Penjualan Saham Pendiri","Penjualan Saham Pendiri adalah hasil dari dari keuntungan penjualan sahan non bursa efek")
            break;

            case 78:
                swal("Honorarium atas Beban APBN/APBD","Honorarium atas Beban APBN/APBD adalah penghasilan berupa imbalan yang diterima oleh Pejabat " +
                "Negara. Pegawai Negeri Sipil, Anggota TNI/ POLRI dan Pensiunan yang dibebankan kepada keuangan " +
                "negara/daerah sehubungan dengan pekerjaan, jasa dan kegiatan berdasarkan Peraturan Pemerintah " +
                "Nomor 80 Tahun 2010.")
            break;

            case 81:
                swal("Penghargaan dan Hadiah","Penghargaan dan Hadiah adalah adiah atau penghargaan yang diberikan melalui suatu perlombaan, adu " +
                "ketangkasan atau penghargaan atas jasa seseorang.")
            break;

            case 82:
                swal("Bantuan/Sumbangan","Bantuan/Sumbangan adalah pemberian seseorang bersifat sosial")
            break;
        
            default:
                swal("Penghasilan Tambahan","Penghasilan tambahan sekali terima")
                break;
        }
    });

    // button edit pada step pass final
    $('button[id^="btneditpassfinalaset"]').click(function (e) {
        var data = $(this).attr('data')
        data = JSON.parse(data)
        var txtid = "<input type='hidden' name='id' value='" + data.id + "' />"
        $('#modalassetpassfinal').append(txtid)
        $('#modalassetpassfinal #flag').val(data.flag)
        $('#modalassetpassfinal #category').val(data.category)
        $('#modalassetpassfinal #type').val(data.type)
        $('#modalassetpassfinal #nominal').val(data.nominal)
        $('#modalassetpassfinal #country_id').val(data.country_id)
        $('#modalassetpassfinal #address').val(data.address)
        $('#modalassetpassfinal #name').val(data.name)
        $('#modalassetpassfinal #npwp').val(data.NPWP)
        $('#modalassetpassfinal #supporting_document').val(data.supporting_document)
        $('#modalassetpassfinal #acquisition_description').val(data.acquisition_description)
        $('#modalassetpassfinal #document_type').val(data.document_type)
        $('#modalassetpassfinal #detail').val(data.detail)
        $('#modalassetpassfinal #acquisition_year').val(data.acquisition_year)
        $('#modalassetpassfinal').modal('show')
    })
    $('button[id^="btneditpassfinalhutang"]').click(function (e) {
        var data = $(this).attr('data')
        data = JSON.parse(data)
        var txtid = "<input type='hidden' name='id' value='"+data.id+"' />"
        $('#form_create_hutang_pass_final').append(txtid)
        $('#form_create_hutang_pass_final #flag').val(data.flag)
        $('#form_create_hutang_pass_final #category').val(data.category)
        $('#form_create_hutang_pass_final #type').val(data.type)
        $('#form_create_hutang_pass_final #acquisition_year').val(data.acquisition_year)
        $('#form_create_hutang_pass_final #nominal').val(data.nominal)
        $('#form_create_hutang_pass_final #assesment_document_number').val(data.assesment_document_number)
        $('#form_create_hutang_pass_final #country_id').val(data.country_id)
        $('#form_create_hutang_pass_final #address').val(data.address)
        $('#form_create_hutang_pass_final #name').val(data.name)
        $('#form_create_hutang_pass_final #npwp').val(data.NPWP)
        $('#form_create_hutang_pass_final #document_type').val(data.document_type)
        $('#form_create_hutang_pass_final #document_number').val(data.document_number)
        $('#form_create_hutang_pass_final #qty_assets').val(data.qty_assets)
        $('#form_create_hutang_pass_final #unit').val(data.unit)
        $('#form_create_hutang_pass_final #detail').val(data.detail)
        $('#modalhutangpassfinal').modal('show')
    })

    $('#btnAddAsset').click(function() {
        // $('#form_create_assets').reset()
        var attr = $('input[name="ikutta"]:checked').val()
        if (attr === undefined) {
            alert ('isi terlebih dahulu form paling atas')
        } else {
            $('#modalassetpassfinal #category').empty()
            var str = '<option value="C1 – Harta dengan Nilai yang Tidak Sesuai">C1 – Harta dengan Nilai yang Tidak Sesuai</option>'                    
            if (attr === 'ya') {
                str += '<option value = "B1 - Harta Tidak Dilaporkan SPT" class="opt following" > B1 - Harta Tidak Dilaporkan SPT</option >'
                $('#modalassetpassfinal #category').append(str)
            } else if (attr === 'tidak') {
                str += '<option value="A1 - Harta Tidak Dilaporkan SPT" class="opt notfollowing">A1 - Harta Tidak Dilaporkan SPT</option>'
                $('#modalassetpassfinal #category').append(str)
            }
            $('#modalassetpassfinal').modal('show')
        }
    })
    $('#btnAddHutang').click(function() {
        // $('#form_create_hutang_pass_final').reset()
        var attr = $('input[name="ikutta"]:checked').val()
        if (attr === undefined) {
            alert('isi terlebih dahulu form paling atas')
        } else {
            $('#modalhutangpassfinal #category').empty()
            var str = '<option value="C2 - Hutang dengan Nilai yang Tidak Sesuai">C2 - Hutang dengan Nilai yang Tidak Sesuai</option>'
            if (attr === 'ya') {
                str += '<option value = "B2 - Hutang Tidak Dilaporkan SPT" > B2 - Hutang Tidak Dilaporkan SPT</option>'
                $('#modalhutangpassfinal #category').append(str)
            } else if (attr === 'tidak') {
                str += '<option value="A2 - Hutang Tidak Dilaporkan SPT">A2 - Hutang Tidak Dilaporkan SPT</option>'
                $('#modalhutangpassfinal #category').append(str)
            }
            $('#modalhutangpassfinal').modal('show')
        }
    })
    ////////////End of Another one time earning/////////

    $('#btnAddPenghasilanUtama').click(function() {
        // $('#form_create_assets').reset()
        console.log('add penghasilan utama')
        $('#modalpendapatanfinalsteps').modal('show')
        $('#modalpendapatanfinalsteps').on('change','#kategori-penghasilan',function(e){
            var kategori_penghasilan = $('#kategori-penghasilan option:selected').val();
            
            $('#form_create_penghasilan .modal-body div').remove();
            console.log(kategori_penghasilan)
            if(kategori_penghasilan == 0){
                // $('#main-income-form-template').clone().removeAttr('id').show().appendTo('#form_create_penghasilan .modal-body');
                // $('#remove-tab').closest('div').remove();
                // $('.preview-image').closest('div').remove();
                $('#modalpendapatanfinalsteps button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/9")');
            }else if(kategori_penghasilan == 1){
                $('<div></div>').appendTo('#form_create_penghasilan .modal-body');
                $('#kategori-penghasilan-rutin').clone().appendTo('#form_create_penghasilan .modal-body');
                $('<div class="form-container"></div>').insertAfter( "#kategori-penghasilan-rutin" );
                $('#modalpendapatanfinalsteps').on('change','select#select-kategori-penghasilan-rutin',function(e){
                    $('#form_create_penghasilan .modal-body .form-container').empty();
                    var kategori_penghasilan_rutin = $('select#select-kategori-penghasilan-rutin option:selected').val();
                    console.log(kategori_penghasilan_rutin)
                    $('#modalpendapatanfinalsteps button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+kategori_penghasilan_rutin+'")');
                }); 
            }else if(kategori_penghasilan == 2){
                $('<div></div>').appendTo('#form_create_penghasilan .modal-body');
                $('#kategori-penghasilan-sekali-terima').clone().appendTo('#form_create_penghasilan .modal-body');
                $('<div class="form-container"></div>').insertAfter( "#kategori-penghasilan-sekali-terima" );
                $('#modalpendapatanfinalsteps').on('change','select#select-kategori-penghasilan-sekali-terima',function(e){
                    $('#form_create_penghasilan .modal-body .form-container').empty();
                    var kategori_penghasilan_sekali_terima = $('select#select-kategori-penghasilan-sekali-terima option:selected').val();
                    $('#modalpendapatanfinalsteps button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+kategori_penghasilan_sekali_terima+'")');
                }); 
            }
            // main-income-form-template
        });
    });

    $('#btnAddHutangFinalStep').click(function(e) {
        $('#modalhutangfinalstep').modal('show')
        $('#modalhutangfinalstep').on('change','select#kategori-hutang',function(e){
            console.log(e)
            var page = e.currentTarget.value
            $('#modalhutangfinalstep button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+page+'")');
        });
    });

    $('#btnAddHutangFinalStep').click(function(e) {
        $('#modalhutangfinalstep').modal('show')
        $('#modalhutangfinalstep').on('change','select#kategori-hutang',function(e){
            console.log(e)
            var page = e.currentTarget.value
            $('#modalhutangfinalstep button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+page+'")');
        });
    });

    $('#btnAddHartaFinalStep').click(function(e) {
        $('#modalhartafinalstep').modal('show') 
        $('#modalhartafinalstep').on('change','#kategori-harta',function(e){
            var kategori_harta = $('#kategori-harta option:selected').val();
            
            $('#modalhartafinalstep .modal-body div').remove();
            console.log(kategori_harta)

            if(kategori_harta == 0){
                $('<div></div>').appendTo('#modalhartafinalstep .modal-body');
                $('#harta-umum').clone().appendTo('#modalhartafinalstep .modal-body');
                $('<div class="form-container"></div>').insertAfter( "#kategori-harta-umum" );
                $('#harta-umum').css("display", "block")
                $('#modalhartafinalstep').on('change','select#select-kategori-harta-umum',function(e){
                    $('#modalhartafinalstep .modal-body .form-container').empty();
                    var kategori_harta_umum = $('select#select-kategori-harta-umum option:selected').val();
                    console.log(kategori_harta_umum)
                    $('#modalhartafinalstep button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+kategori_harta_umum+'")');
                }); 
            }else if(kategori_harta == 1){
                $('<div></div>').appendTo('#modalhartafinalstep .modal-body');
                $('#investasi-finansial').clone().appendTo('#modalhartafinalstep .modal-body');
                $('<div class="form-container"></div>').insertAfter( "#kategori-investasi-finansial" );
                $('#investasi-finansial').css("display", "block")
                $('#modalhartafinalstep').on('change','select#select-kategori-investasi-finansial',function(e){
                    $('#modalhartafinalstep .modal-body .form-container').empty();
                    var kategori_investasi_finansial = $('select#select-kategori-investasi-finansial option:selected').val();
                    $('#modalhartafinalstep button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+kategori_investasi_finansial+'")');
                }); 
            }else if(kategori_harta == 2){
                $('<div></div>').appendTo('#modalhartafinalstep .modal-body');
                $('#harta-lainnya').clone().appendTo('#modalhartafinalstep .modal-body');
                $('<div class="form-container"></div>').insertAfter( "#kategori-harta-lainnya" );
                $('#harta-lainnya').css("display", "block")
                $('#modalhartafinalstep').on('change','select#select-kategori-harta-lainnya',function(e){
                    $('#modalhartafinalstep .modal-body .form-container').empty();
                    var kategori_harta_lainnya = $('select#select-kategori-harta-lainnya option:selected').val();
                    $('#modalhartafinalstep button[type="submit"]').attr('onclick', 'window.open("/tax-filling-final-steps/'+kategori_harta_lainnya+'")');
                }); 
            }
            // main-income-form-template
        });
    });

    $('#final-step-form-container button.btn-warning').on('click',function(e){
        console.log(e)
        socket.send('edit final step');
        // var income_id = $('select#select-kategori-penghasilan-sekali-terima option:selected').val();
    });
}
