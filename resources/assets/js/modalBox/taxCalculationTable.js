import formatCurrency from 'format-currency';

let currentForm = null;

const precisionRound = function (number, precision) {
    const factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
};

const calculateTotalColumn = function($, type) {
  const colType = typeof type === 'undefined' ? 'interest' : type;
  let total = 0;

  $('input[name^="' + colType + '-"]').each(function() {
      let inputVal = parseInt($(this).val());
      total += inputVal >= 0 ? inputVal : 0;
  });

  $('#total-' + colType + '-text').text(formatCurrency(total, { format: '%s%v', symbol: 'IDR ', locale: 'de-DE' }));
  $('#total-' + colType).val(total);
};

const resetTableState = function() {
    $('#calculation_table_modal').find('input[name^="interest-"]').each(function() { $(this).val(0); });
    $('#calculation_table_modal').find('input[name^="tax-"]').each(function() { $(this).val(0); });

    $('#total-interest-text, #total-tax-text').text('0');
    $('#total-interest, #total-tax').val('0');

    $('input[name="20-percent-tax-rate"]').prop('checked', false);

    currentForm = null;
};

const handleModalTriggerButton = function ($) {
  $('a.btn-calculation-modal').on('click', calculationBtnClickHandler);
}

const handleInterestChange = function($) {
  $('input[name^="interest-"]').on('change', function() {
    calculateTotalColumn($, 'interest');
  });
};

const handleTaxChange = function($) {
  $('input[name^="tax-"]').on('change', function() {
    calculateTotalColumn($, 'tax');
  });
};

const handleAutomaticTaxCalculation = function($) {
    const checkboxElem = $('input[name="20-percent-tax-rate"]');

    checkboxElem.on('change', function() {
        if (checkboxElem.prop('checked')) {
            $('input[name^="interest-"]').each(function (n) {
                const taxInputElem = $(this).parent('td').siblings('td').find('input[name^="tax"]');
                if (taxInputElem.length > 0) {
                    let interest = Number($(this).val());
                    let tax = precisionRound(interest * 0.2, 2);

                    $(taxInputElem).val(tax);
                }
            });
        } else {
            $('input[name^="interest-"]').each(function (n) {
                const taxInputElem = $(this).parent('td').siblings('td').find('input[name^="tax"]');
                $(taxInputElem).val('0');
            });
        }

        calculateTotalColumn($, 'interest');
        calculateTotalColumn($, 'tax');
    });
};

const handleModalClosed = function ($) {
    $('#calculation_table_modal').on('hidden.bs.modal', function() {
        const interestData = [];
        const taxData = [];

        $('#calculation_table_modal').find('table input[name^="interest-"]').each(function() {
            interestData.push(Number($(this).val()));
        });
        $('#calculation_table_modal').find('table input[name^="tax-"]').each(function() {
            taxData.push(Number($(this).val()));
        });

        currentForm.find('input[name^="interest-data"]').val(JSON.stringify(interestData));
        currentForm.find('input[name^="tax-data"]').val(JSON.stringify(taxData));
        // currentForm.find('input[name^="calculated-interest"]')
        //     .val($('#total-interest-text').text().replace('IDR', '').replace(',00', '').trim());
        // currentForm.find('input[name^="calculated-tax"]')
        //     .val($('#total-tax-text').text().replace('IDR', '').replace(',00', '').trim());
        currentForm.find('input[name^="interests"]').val($('input#total-interest').val()).trigger('input');
        currentForm.find('input[name^="taxes"]').val($('input#total-tax').val()).trigger('input');

        resetTableState();
    });
}

export const calculationBtnClickHandler = function (e) {
    e.preventDefault();

    $('#calculation_table_modal').modal('show').find('.modal-title').text('Detail Bunga Sebelum Pajak dalam 1 Tahun');
    $('#calculation_table_modal').find('table').data('mode', 'interest');

    currentForm = $(this).closest('div.bank-form');

    const interestFieldData = currentForm.find('input[name^="interest-data"]').val();
    const taxFieldData = currentForm.find('input[name^="tax-data"]').val();
    const interestData = interestFieldData ? JSON.parse(interestFieldData) : [];
    const taxData = taxFieldData ? JSON.parse(taxFieldData) : [];

    $('#calculation_table_modal').find('table input[name^="interest-"]').each(function(i) {
        $(this).val(interestData[i]);
    });
    $('#calculation_table_modal').find('table input[name^="tax-"]').each(function(i) {
        $(this).val(taxData[i]);
    });

    calculateTotalColumn($, 'interest');
    calculateTotalColumn($, 'tax');
};

export const currencyInputSelectChangeHandler = function (e) {
    e.preventDefault();


}

export const initTable = function($) {
    handleInterestChange($);
    handleTaxChange($);
    handleAutomaticTaxCalculation($);
    handleModalClosed($);
    handleModalTriggerButton($);
};

export default {
    initTable,
    calculationBtnClickHandler,
};
