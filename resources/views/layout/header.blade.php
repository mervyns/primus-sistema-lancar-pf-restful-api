<header class="header banner navbar-mobile--show-x">
    <nav class="navbar navbar-main">
        <div class="container max-w-xxl">
            <a class="navbar-brand brand" href="/">
                <img class="brand-img brand-img--h" src="/images/brand-h-2x.png" srcSet="/images/brand-h-2x.png" alt="SPT Go"/>
            </a>
            <div class="d-flex flex-row ml-auto mr-0 collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-toggleable-md navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li class="nav-item menu-item"><a class="nav-link" href="{{ url('/login') }}">Masuk</a></li>
                        <li class="nav-item menu-item my-account"><a class="nav-link" href="{{ url('/') }}">Daftar</a></li>
                    @else
                        @if(Auth::user()->roles()->where('alias', '=', 'admin')->count() === 0)
                            <li class="nav-item dropdown">
                                <a href="{{ url('/home') }}" class="nav-link">
                                    Home
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ url('/tax-filling?redirect=true') }}">
                                    Isi SPT
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="{{ url('/product') }}">
                                    Harga
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="{{ url('/pass-final') }}" class="nav-link">
                                    Pass Final
                                </a>
                            </li>
                        @endif
                        
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="userProfileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <!--({{ Auth::user()->email }})&nbsp;-->
                                <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="userProfileMenu">
                                <a class="dropdown-item" href="{{ url('/profile/me') }}">
                                    Edit profile
                                </a>
                                <a class="dropdown-item"
                                   href="{{ url('/logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @if (!Auth::guest())
        @if(Auth::user()->roles()->where('alias', '=', 'admin')->count() > 0)
        <nav class="navbar navbar-dark bg-primary navbar-admin">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/admin') }}">Dashboard <span class="sr-only">(current)</span></a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Users
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="userProfileMenu">
                        <a class="dropdown-item" href="{{ url('/profile/me') }}">
                            Edit profile
                        </a>
                    </div>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link dropdown-toggle" href="#" id="settingDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Setting
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="bankAccountMenu">
                        <a class="dropdown-item" href="{{ url('/admin/bank-account') }}">
                            Bank Account
                        </a>
                        <a class="dropdown-item" href="{{ url('/admin/product') }}">
                            Promo
                        </a>
                        <a class="dropdown-item" href="{{ url('/admin/user-order') }}">
                            Product Order
                        </a>
                        {{-- <a class="dropdown-item" href="{{ url('/admin/tootltip-feature') }}">
                            Tooltip Feature
                        </a> --}}
                    </div>
                    
                </li>
                <li class="nav-item">
                    <a class="nav-link dropdown-toggle" href="#" id="settingDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Feature
                        <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="bankAccountMenu">
                        <a class="dropdown-item" href="{{ url('/admin/tooltip-feature') }}">
                            Tooltip Feature
                        </a>
                    </div>
                    
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/admin/users') }}">Users</a>
                </li>
            </ul>
        </nav>
        @endif
    @endif
</header>
