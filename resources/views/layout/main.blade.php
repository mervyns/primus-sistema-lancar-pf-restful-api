<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon/favicon-16x16.png">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SPT Go') }} | {{ isset($title) ? $title : 'Selamat Datang di SPT Go' }}</title>

    <link rel="stylesheet" type="text/css" href="/styles.css?{{ time() }}">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css"> -->
    <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!}

    var apiUrl = window.location.origin;
    </script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    
    <script type="text/javascript" src="/main.js?{{ time() }}"></script>
    <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script> -->
    @yield('header_scripts')
</head>
<body>
    <main class="main bg-light main-app-container">
        @include('layout.header')
        <?php $uri = explode('/',$_SERVER['REQUEST_URI']);?>
        @if(!empty($uri[1]) && $uri[1] == 'tax-filling')
            @if(!empty($uri[2]) && !in_array($uri[2],[2,3,4,8]))
                @include('layout.notes')
            @endif
        @endif
        @yield('content')
        @include('layout.footer')
    </main>
    <div id="post_it" style="display:none;">
        @if(!empty(\Session::get('my_husband')))
            <input type="hidden" id="my_husband" value="Surat Keterangan Suami Tidak Bekerja : {{\Session::get('my_husband')}}">
        @endif
        @if(!empty(\Session::get('main_income')))
            <input type="hidden" id="main_income" value="Bukti Potong Form 1721-A1/1721-A2 (Tahunan) - ({{\Session::get('main_income')}}x) - atas penghasilan dari gaji">
        @endif
        @if(!empty(\Session::get('overseas_income')))
            <input type="hidden" id="overseas_income" value="Bukti Potong PPh 24 - ({{\Session::get('overseas_income')}}x) - atas penghasilan dari luar negeri">
        @endif
        @if(!empty(\Session::get('intelectual_income')))
            <input type="hidden" id="intelectual_income" value="Bukti Potong PPh 23 - ({{\Session::get('intelectual_income')}}x) - atas penghasilan dari Hak Kekayaan Intelektual">
        @endif
        @if(!empty(\Session::get('expert_reward_income')))
            <input type="hidden" id="expert_reward_income" value="Bukti Potong PPh 21 (Non Final) - ({{\Session::get('expert_reward_income')}}x) - atas penghasilan dari Tenaga Ahli">
        @endif
        @if(!empty(\Session::get('luxury_goods')))
            <input type="hidden" id="luxury_goods" value="Bukti Potong PPh 22 - ({{\Session::get('luxury_goods')}}x) - atas pembelian barang mewah">
        @endif
    </div>
    @yield('footer_scripts')
</body>
</html>
