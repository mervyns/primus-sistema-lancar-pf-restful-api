<!DOCTYPE html>
<html>
<head>
	<title>Berhasil Aktivasi Akun SPTGO</title>
	<style>
        body {
            min-height: 90vh;
			background-color: #ddd;
        }
		
		p {
			padding: 0;
			margin: 0;
			font-size: 14px;
		}
    </style>
</head> 
<body>
	<br><br>
	<div class="content">
		<center>
			<img style="bottom:5px;" src="https://thumb.ibb.co/dzWNo0/brand-v-2x.png" alt="brand-v-2x" border="0">
		</center>
		<br><br><br>
		<p>Congrats,  <b>{{ $name }} !</b></p>
		<br><br>

		<p>You have registered in  SPTGO. <br> Here’s the complete information related to your account:</p>
		<br><br>
		
		<p>User ID : <b>{{ $email }}</b><br><br>Thank you for choosing SPTGO, <br>We will facilitate your tax reporting</p>

		<br><br>
		<p>Regards, <br>SPTGO</p>
		<center>
			<img style="margin-top: -20px; margin-left: 600px; width: 50px;" src="https://thumb.ibb.co/m2bGy0/mini-toolbar.png" alt="mini-toolbar" border="0">
			<br>
			<p style="margin-top: -10px; font-weight: bold; color: #02346c; font-size: 20px;">TAXES JUST GOT EASIER</p>
		</center>
	</div>
</body>
</html>