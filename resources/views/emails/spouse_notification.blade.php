<html>
<head>
    <title>{{ $parameter['title'] }}</title>
</head>
<body>
<h3>{{ $parameter['subtitle'] }}</h3>
<hr>

<p>{{ $parameter['message'] }}</p>

</body>
</html>