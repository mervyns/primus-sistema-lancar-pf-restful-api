<!-- <html>
<head>
    <title>Aktivasi Akun SPTGO</title>
</head>
<body>
<h3>Aktivasi akun {{ $email }}</h3>
<hr>
<p><b>Silahkan klik link dibawah untuk melakukan aktivasi akun anda</b></p>
<a href="{{url('/')}}/activate?id={{ $id }}">{{url('/')}}/activate?id={{ $id }}</a>
</body>
</html> -->

<!-- <!DOCTYPE html>
<html>
<head>
	<title>Aktivasi Akun SPTGO</title>
<style type="text/css">
	.btn {
		background: #219c0e;
		background-image: -webkit-linear-gradient(top, #219c0e, #1d8a07);
		background-image: -moz-linear-gradient(top, #219c0e, #1d8a07);
		background-image: -ms-linear-gradient(top, #219c0e, #1d8a07);
		background-image: -o-linear-gradient(top, #219c0e, #1d8a07);
		background-image: linear-gradient(to bottom, #219c0e, #1d8a07);
		-webkit-border-radius: 28;
		-moz-border-radius: 28;
		border-radius: 28px;
		font-family: Arial;
		color: #ffffff;
		font-size: 20px;
		padding: 10px 20px 10px 20px;
		text-decoration: none;
	}

	.panel {
	  padding: 15px;
	  margin-bottom: 20px;
	  background-color: #ffffff;
	  border: 1px solid #dddddd;
	  border-radius: 4px;
	  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
	  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
	}

	.center {
	    margin: auto;
	    width: 50%;
	    /*border: 3px solid green;*/
	    padding: 10px;
	}

	.text-center {
	    text-align: center;
	}

</style>

</head>
<body>

<div class="center">
	<div class="panel">
		<div class="panel-body">
			<center>
				<h3 class="text-center">Welcome To SPTGO, {{ $name }}</h3>
				<p class="text-center">Please verify your account by clicking the link bellow</p>
				<br><br>
				<a href="{{url('/')}}/activate?id={{ $id }}" class="btn">Verify your account</a>
				<p>Or verify by copy  pasting this link into your web browser :</p>
				<a href="{{url('/')}}/activate?id={{ $id }}">{{url('/')}}/activate?id={{ $id }}</a>
				<br><br><br>
				<p>If you did not create on SPTGO account, please contact us at : <a href="javascript:void(0)">primus.smg@gmail.com</a></p>
			</center>
		</div>
	</div>
</div>

</body>
</html> -->

<!DOCTYPE html>
<html>
<head>
    <title>Aktivasi Akun SPTGO</title>
    
    <style>
        body {
            min-height: 90vh;
			background-color: #ddd;
        }
		
		p {
			padding: 0;
			margin: 0;
			font-size: 14px;
		}

		.content {
			margin: 10px 150px;
			border-radius: 5px;
		}
		
        .btn {
            background: #219c0e;
            background-image: -webkit-linear-gradient(top, #219c0e, #1d8a07);
            background-image: -moz-linear-gradient(top, #219c0e, #1d8a07);
            background-image: -ms-linear-gradient(top, #219c0e, #1d8a07);
            background-image: -o-linear-gradient(top, #219c0e, #1d8a07);
            background-image: linear-gradient(to bottom, #219c0e, #1d8a07);
            -webkit-border-radius: 28;
            -moz-border-radius: 28;
            border-radius: 28px;
            font-family: Arial;
            color: #fff;
            font-size: 14px;
            padding: 10px 25px;
            text-decoration: none;
        }
    </style>
</head>
<body>
	<div class="content">
		<center>
			<img src="https://thumb.ibb.co/dzWNo0/brand-v-2x.png" alt="brand-v-2x" border="0">
			<br><br>
			<p>Welcome to SPTGO, <b>{{ $name }}</b></p>
			<p>Please verify your account by clicking the link below.</p>
			<br>
			<a style="color: #fff;" href="{{url('/')}}/activate?id={{ $id }}" class="btn">Verify Your Account</a>
			<br><br>
			<p>Or verify by copy pasting this link into your web browser:</p>
			<br>
			<a href="{{url('/')}}/activate?id={{ $id }}">{{url('/')}}/activate?id={{ $id }}</a>
			<br><br>
			<p>If you did not create an SPTGO account, please contact us at <a href="javascript:void(0)"><b>primus.smg@gmail.com</b></a></p>
			
			<img style="margin-top: -20px; margin-left: 600px; width: 50px;" src="https://thumb.ibb.co/m2bGy0/mini-toolbar.png" alt="mini-toolbar" border="0">
			<br>
			<p style="margin-top: -10px; font-weight: bold; color: #02346c; font-size: 20px;">TAXES JUST GOT EASIER</p>
		</center>
	</div>
    
    
    
</body>
</html>