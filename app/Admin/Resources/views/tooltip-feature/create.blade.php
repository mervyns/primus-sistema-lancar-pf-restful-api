@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">{{ $title }}</h3>
                    <div class="boxed-content">{{ $sub_title }}</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="form_create_tooltip" class="" method="post" action="{{ url('/admin/tooltip-feature/add') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Step</label>
                                        <div class="col-sm-5">
                                            <select id="step" name="step" class="form-control select2">
                                                @foreach ($steps as $step)
                                                    <option value="{{ $step->id }}"> {{ $step->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Judul
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" name="judul" id="judul" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Link
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" name="link" id="link" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Content
                                        </label>
                                        <div class="col-sm-5">
                                            <textarea class="form-control form-control-sm" name="content" id="content" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            <a href="/admin/tooltip-feature"><button type="button" class="btn btn-default">Kembali</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection