@extends('layout.main')

@section('content')
<section id="admin-page-users" class="admin-content-container container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-gray">{{ $title }}</h4>
            <p>{{ $sub_title }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <a href="{{ url('admin/tooltip-feature/add') }}" class="btn btn-info"><i class="fa fa-plus"></i>  Tambah</a>
            <br>
        </div>
        <div class="col-md-3 text-right">
            <form method="get" action="">
                <input type="text" name="search" id="search" placeholder="Enter judul atau content tooltip" class="form-control">
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center" width="4%">No</th>
                            <th scope="col" class="text-center">Judul</th>
                            <th scope="col" class="text-center" width="20%">Link</th>
                            <th scope="col" class="text-center" width="20%">Content</th>
                            <th scope="col" class="text-center" width="20%">Step</th>
                            <th scope="col" class="text-center" width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($tooltips as $tooltip)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $tooltip->judul }}</td>
                            <td class="text-center">{{ $tooltip->link }}</td>
                            <td class="text-center">{{ $tooltip->content }}</td>
                            <td class="text-center">{{ $tooltip->systemStepConfig->title }}</td>
                            <td class="text-center">
                                <a class="btn btn-success btn-sm" href="/admin/tooltip-feature/edit/{{ $tooltip->id }}"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm btn-remove-tooltip" data-id="{{ $tooltip->id }}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada data.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $tooltips->links() }}
            </div>
        </div>
    </div>
</section>

@endsection
