@extends('layout.main')

@section('content')
<section id="admin-page-users" class="admin-content-container container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-gray">{{ $title }}</h4>
            <p>{{ $sub_title }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('admin/product/add') }}" class="btn btn-info"><i class="fa fa-plus"></i>  Tambah</a>
            <br>
        </div>
        
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    {{--<caption>Daftar user</caption>--}}
                    <thead>
                        <tr>
                            <th scope="col" class="text-center" width="4%">No</th>
                            <th scope="col" class="text-center">Nama</th>
                            <th scope="col" class="text-center" width="20%">Deskripsi</th>
                            <th scope="col" class="text-center" width="8%">Jumlah</th>
                            <th scope="col" class="text-center" width="8%">Price</th>
                            <th scope="col" class="text-center" width="8%">Promo Status</th>
                            <th scope="col" class="text-center" width="20%">Deskripsi Promo</th>
                            <th scope="col" class="text-center" width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($products as $product)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $product->name }}</td>
                            <td class="text-left">
                                @if (count(json_decode($product->description)) > 0)
                                    <ul>
                                    @foreach (json_decode($product->description) as $desc)
                                        <li>{{ $desc }}</li>
                                    @endforeach
                                    <ul>
                                @endif
                            </td>
                            <td class="text-center">{{ $product->invites }}</td>
                            <td class="text-center">{{ number_format($product->price,0,',','.') }}</td>
                            <td class="text-center">
                                @if ($product->promo == 1)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-secondary">Non-Active</span>
                                @endif
                            </td>
                            <td class="text-center">{{ $product->promo_description }}</td>
                            <td class="text-center">
                                <a class="btn btn-success btn-sm btn-edit-product" href="/admin/product/edit/{{ $product->id }}"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm btn-remove-product" data-id="{{ $product->id }}" data-name="{{ $product->name }}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada data produk.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection
