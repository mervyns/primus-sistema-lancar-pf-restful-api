@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">{{ $title }}</h3>
                    <div class="boxed-content"> {{ $sub_title }}</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="form_create_product" class="" method="post" action="{{ url('/admin/product/update') }}">
                                    {{ csrf_field() }}
                                    
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Nama
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" id="product_name" name="product_name" value="{{ $product->name }}" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="description" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Deskripsi Produk
                                        </label>
                                        <div class="col-sm-5">
                                            <div id="new_description">
                                                @if (!empty($product->description))
                                                    @foreach (json_decode($product->description) as $row)
                                                    <div class="form-group row"><div class="col-sm-12"><input type="text" id="description[]" name="description[]" class="form-control form-control-sm" value="{{ $row }}"></div></div>
                                                    @endforeach
                                                @endif 
                                            </div>
                                            <a href="javascript:void(0)" class="btn btn-info btn-sm" id="btn_description_product"><i class="fa fa-plus"></i> Tambah Deskripsi</a>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="invites" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Jumlah
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="number" style="text-align:right;" class="form-control form-control-sm" id="invites" name="invites" value="{{ $product->invites }}" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="price" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Harga
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" style="text-align:right;" class="form-control form-control-sm" id="price" name="price" value="{{ $product->price }}" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="promo" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Promo Status
                                        </label>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="promo" id="promo" value="1" checked required><span class="custom-control-indicator"></span><span class="custom-control-description">Active</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px; margin-left: -20px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="promo" id="promo" value="0" required><span class="custom-control-indicator"></span><span class="custom-control-description">Non-Active</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="promo_description" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Deskripsi Promo
                                        </label>
                                        <div class="col-sm-5">
                                            <textarea id="promo_description" name="promo_description" class="form-control form-control-sm" placeholder="" rows="4">{{ (empty($product->promo_description)) ? '' : $product->promo_description }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="col-sm-6">
                                            <input type="hidden" name="id" id="id" value="{{ $product->id }}">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            <a href="/admin/product"><button type="button" class="btn btn-default">Kembali</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection