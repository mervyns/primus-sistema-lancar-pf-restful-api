@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">{{ $title }}</h3>
                    <div class="boxed-content">{{ $sub_title }}</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="form_create_bank_account" class="" method="post" action="{{ url('/admin/bank-account/add') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Bank</label>
                                        <div class="col-sm-5">
                                            <select id="bank" name="bank" class="form-control select2">
                                                @foreach ($banks as $bank)
                                                    <option value="{{ $bank->id }}"> {{ $bank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            No. Rekening
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" name="account_number" id="account_number" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Atas Nama
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" name="account_under_name" id="account_under_name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="isActive" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Status
                                        </label>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="status" id="status" value="1" checked required><span class="custom-control-indicator"></span><span class="custom-control-description">Active</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px; margin-left: -20px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="status" id="status" value="0" required><span class="custom-control-indicator"></span><span class="custom-control-description">Non-Active</span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            <a href="/admin/bank-account"><button type="button" class="btn btn-default">Kembali</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection