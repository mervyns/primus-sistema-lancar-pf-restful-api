@extends('layout.main')

@section('content')
<section id="admin-page-users" class="admin-content-container container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-gray">{{ $title }}</h4>
            <p>{{ $sub_title }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <a href="{{ url('admin/bank-account/add') }}" class="btn btn-info"><i class="fa fa-plus"></i>  Tambah</a>
            <br>
        </div>
        <div class="col-md-3 text-right">
            <form method="get" action="">
                <input type="text" name="search" id="search" placeholder="Enter Nomor Rekening atau nama bank" class="form-control">
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    {{--<caption>Daftar user</caption>--}}
                    <thead>
                        <tr>
                            <th scope="col" class="text-center" width="4%">No</th>
                            <th scope="col" class="text-center">Nama Bank</th>
                            <th scope="col" class="text-center" width="20%">Nomor Rekening</th>
                            <th scope="col" class="text-center" width="20%">Atas Nama</th>
                            <th scope="col" class="text-center" width="20%">Status</th>
                            <th scope="col" class="text-center" width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($bank_accounts as $bank_account)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $bank_account->banks->name }}</td>
                            <td class="text-center">{{ $bank_account->account_number }}</td>
                            <td class="text-center">{{ $bank_account->account_under_name }}</td>
                            <td class="text-center">
                                @if ($bank_account->status == 1)
                                    <span class="badge badge-danger">active</span>
                                @else
                                    <span class="badge badge-secondary">non-active</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-success btn-sm" href="/admin/bank-account/edit/{{ $bank_account->id }}"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm btn-remove-bank-account" data-id="{{ $bank_account->id }}"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada data.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $bank_accounts->links() }}
            </div>
        </div>
    </div>
</section>

@endsection
