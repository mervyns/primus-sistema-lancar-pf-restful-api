@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Tambah User</h3>
                    <div class="boxed-content">Halaman ini digunakan untuk menambahkan user baru.</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="form_create_user" class="" method="post" action="{{ url('/admin/users/add') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label for="name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Nama
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   class="form-control form-control-sm"
                                                   id="name" name="name" value="" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Email
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="email"
                                                   class="form-control form-control-sm"
                                                   id="email" name="email" value="" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Password
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="password"
                                                   class="form-control form-control-sm"
                                                   id="password" name="password" value="" placeholder="" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="gender" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="L" required><span class="custom-control-indicator"></span><span class="custom-control-description">Laki-laki</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px; margin-left: -20px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="P" required><span class="custom-control-indicator"></span><span class="custom-control-description">Perempuan</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="birthdate" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Tanggal Lahir
                                        </label>
                                        <div class="col-sm-3">
                                            <input type="date" name="birthdate" id="birthdate" value="" class="form-control" style="width: 100%;" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobilenumber" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            No Handphone
                                        </label>
                                        <div class="col-sm-3">
                                            <input type="tel" name="mobilenumber" id="mobilenumber" value="" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="homenumber" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            No Telepon
                                        </label>
                                        <div class="col-sm-3">
                                            <input type="tel" name="homenumber" id="homenumber" value="" class="form-control" autocomplete="off" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="faxnumber" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            No Fax
                                        </label>
                                        <div class="col-sm-3">
                                            <input type="tel" name="faxnumber" id="faxnumber" value="" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Role</label>
                                        <div class="col-sm-5">
                                            <select id="roleid" name="roleid" class="form-control">
                                                @foreach ($roles as $role)
                                                    <option value="{{ $role->id }}"> {{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="col-sm-6">
                                            <button type="submit" class="btn btn-success">Simpan</button>
                                            <a href="/admin/users"><button type="button" class="btn btn-default">Kembali</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection