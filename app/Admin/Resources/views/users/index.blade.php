@extends('layout.main')

@section('content')
<section id="admin-page-users" class="admin-content-container container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-gray">{{ $title }}</h4>
            <p>{{ $sub_title }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <a href="{{ url('admin/users/add') }}" class="btn btn-info"><i class="fa fa-plus"></i>  Tambah</a>
            <br>
        </div>
        <div class="col-md-3 text-right">
            <form method="get" action="">
                <input type="text" name="search" id="search" placeholder="Enter Email or Name" class="form-control">
            </form>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    {{--<caption>Daftar user</caption>--}}
                    <thead>
                        <tr>
                            <th scope="col" class="text-center" width="4%">No</th>
                            <th scope="col" class="text-center">Nama</th>
                            <th scope="col" class="text-center" width="20%">Email</th>
                            <th scope="col" class="text-center" width="10%">Role</th>
                            <th scope="col" class="text-center" width="10%">Active [?]</th>
                            <th scope="col" class="text-center" width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="text-center">{{ $user->name }}</td>
                            <td class="text-center">{{ $user->email }}</td>
                            <td class="text-center">
                                @if (count($user->roles) > 0)
                                    {{ $user->roles[0]->name }}
                                @else
                                    {{ '-' }}
                                @endif
                            </td>

                            <td class="text-center">
                                @if ($user->isActive == 1)
                                    <a href="javascript:void(0)">active</a>
                                @else
                                    <a href="javascript:void(0)">non-active</a>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="btn btn-success btn-sm btn-edit-user" href="/admin/users/edit/{{ $user->id }}"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm btn-remove-user" data-id="{{ $user->id }}"><i class="fa fa-trash"></i></a>
                                <a class="btn btn-info btn-sm btn-user-order-subscription" data-id="{{ $user->id }}"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6" class="text-center">Belum ada data user.</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_user_product_subscription" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Product Subscription</h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <form id="form_user_product_subscription" method="post">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="30%">Nama Produk</td>
                                        <td>:</td>
                                        <td width="70%"><div id="product_name"></div></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Quota</td>
                                        <td>:</td>
                                        <td width="70%"><div id="quota_member"></div></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Harga</td>
                                        <td>:</td>
                                        <td width="70%"><div id="product_price"></div></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Status Pembayaran</td>
                                        <td>:</td>
                                        <td width="70%"><div id="payment_status"></div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <input type="hidden" name="parent_id" id="parent_id">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="table_user_product_subscription">
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
