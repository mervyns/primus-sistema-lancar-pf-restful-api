@extends('layout.main')

@section('content')
<section id="admin-page-users" class="admin-content-container container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h4 class="text-gray">{{ $title }}</h4>
            <p>{{ $sub_title }}</p>
        </div>
    </div>
    
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center" width="4%">No</th>
                            <th scope="col" class="text-center" width="15%">User</th>
                            <th scope="col" class="text-center" width="15%">Produk</th>
                            <th scope="col" class="text-center" width="10%">Bank</th>
                            <th scope="col" class="text-center" width="10%">Unique Code</th>
                            <th scope="col" class="text-center" width="15%">Total Amount</th>
                            <th scope="col" class="text-center" width="20%">Status</th>
                            <th scope="col" class="text-center" width="20%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($user_orders as $user_order)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td class="text-center">{{ $user_order->users->name }}</td>
                                <td class="text-center">{{ $user_order->userProducts->product->name }}</td>
                                <td class="text-center">{{ $user_order->bankAccounts->banks->name }}</td>
                                <td class="text-center">{{ $user_order->unique_code }}</td>
                                <td class="text-center">{{ $user_order->total_amount }}</td>
                                <td class="text-center">
                                    @if ($user_order->status == 'approved')
                                        <span class="badge badge-success">{{ $user_order->status }}</span>
                                    @elseif ($user_order->status == 'rejected')
                                        <span class="badge badge-danger">{{ $user_order->status }}</span>
                                    @else
                                        <span class="badge badge-info">{{ $user_order->status }}</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <!-- <a onclick="alert('ok')" id="btn_view_user_order_preview" class="btn btn-info btn-sm" data-toggle="tooltip" title="Accept" data-id="{{ $user_order->id }}"><i class="fa fa-eye"></i></a> -->
                                    <button id="btn_view_user_order_preview" class="btn btn-info btn-sm btn-view-user-order-preview" data-toggle="tooltip" title="Lihat detail" data-id="{{ $user_order->id }}"><i class="fa fa-eye"></i></button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" class="text-center">Belum ada data.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_user_order_preview" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Order</h5>
        <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close" aria-hidden="true"></i>
                </button>
        </div>
        <form id="form_user_order_preview" method="post">
            {{ csrf_field() }}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <h6 class="text-center">INFORMASI</h6>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="30%">Nama</td>
                                <td>:</td>
                                <td width="70%"><div id="user_name"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Email</td>
                                <td>:</td>
                                <td width="70%"><div id="user_email"> </div></td>
                            </tr>
                            <tr>
                                <td width="30%">Produk</td>
                                <td>:</td>
                                <td width="70%"><div id="product_name"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Quota</td>
                                <td>:</td>
                                <td width="70%"><div id="invites"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Harga</td>
                                <td>:</td>
                                <td width="70%"><div id="product_price"> </div></td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <h6 class="text-center">PEMBAYARAN</h6>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td width="30%">Nama Bank</td>
                                <td>:</td>
                                <td width="70%"><div id="bank_name"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">No Rekening</td>
                                <td>:</td>
                                <td width="70%"><div id="account_number"> </div></td>
                            </tr>
                            <tr>
                                <td width="30%">Promo</td>
                                <td>:</td>
                                <td width="70%"><div id="promo"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Deskripsi Promo</td>
                                <td>:</td>
                                <td width="70%"><div id="promo_description"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Durasi</td>
                                <td>:</td>
                                <td width="70%"><div id="years"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Unique Code</td>
                                <td>:</td>
                                <td width="70%"><div id="unique"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Jumlah</td>
                                <td>:</td>
                                <td width="70%"><div id="amount"></div></td>
                            </tr>
                            <tr>
                                <td width="30%">Total</td>
                                <td>:</td>
                                <td width="70%"><div id="total_amount"></div></td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                <div class="col-md-6">
                    <h6 class="text-center">ATTACHMENT</h6>
                    <br>
                    <div class="image-wrapper" style="padding:5px; height auto; width: 300px">
                        
                    </div>
                    
                </div>
            </div>
           
            
        </div>
        <div class="modal-footer">
            <input type="hidden" name='id' id="id">
            <input type="hidden" name='year' id="year">
            <button type="button" id="btn_accept_user_order_preview" class="btn btn-primary">Approve</button>
            <button type="button" id="btn_reject_user_order_preview" class="btn btn-danger">Reject</button>
        </div>
        </form>
    </div>
  </div>
</div>
@endsection
