<?php

$router->prefix('admin')
       ->group(function() use ($router) {
	    	$router->get( '/', 'AdminController@index' );

			$router->prefix('users')
				->group(function() use ($router) {
					$router->get('/', '\WebApp\Admin\Controllers\UserController@index');
					$router->get('/add', '\WebApp\Admin\Controllers\UserController@add');
					$router->post('/add', '\WebApp\Admin\Controllers\UserController@store');
					$router->get('/edit/{id}', '\WebApp\Admin\Controllers\UserController@edit');
					$router->post('/update', '\WebApp\Admin\Controllers\UserController@update');
					$router->delete('/delete', '\WebApp\Admin\Controllers\UserController@delete');
					$router->get('/partner', '\WebApp\Admin\Controllers\UserController@getPartner');
					$router->post('/new-partner', '\WebApp\Admin\Controllers\UserController@newPartner');
					$router->post('/update-partner', '\WebApp\Admin\Controllers\UserController@updatePartner');
					$router->delete('/delete-partner', '\WebApp\Admin\Controllers\UserController@deletePartner');
				});

			$router->prefix('bank-account')
				->group(function() use ($router){
					$router->get('/', '\WebApp\Admin\Controllers\BankAccountController@index');
					$router->get('/add', '\WebApp\Admin\Controllers\BankAccountController@add');
					$router->post('/add', '\WebApp\Admin\Controllers\BankAccountController@create');
					$router->get('/edit/{id}', '\WebApp\Admin\Controllers\BankAccountController@edit');
					$router->post('/update', '\WebApp\Admin\Controllers\BankAccountController@update');
					$router->delete('/delete', '\WebApp\Admin\Controllers\BankAccountController@delete');
				});

			$router->prefix('user-order')
				->group(function() use ($router){
					$router->get('/', '\WebApp\Admin\Controllers\UserOrderController@index');
					$router->get('/show', '\WebApp\Admin\Controllers\UserOrderController@show');
					$router->post('/approve', '\WebApp\Admin\Controllers\UserOrderController@approve');
				});

			$router->prefix('product')
				->group(function() use ($router){
					$router->get('/', '\WebApp\Admin\Controllers\ProductController@index');
					$router->get('/add', '\WebApp\Admin\Controllers\ProductController@add');
					$router->post('/add', '\WebApp\Admin\Controllers\ProductController@create');
					$router->get('/edit/{id}', '\WebApp\Admin\Controllers\ProductController@edit');
					$router->post('/update', '\WebApp\Admin\Controllers\ProductController@update');
					$router->delete('/delete', '\WebApp\Admin\Controllers\ProductController@delete');
				});

			$router->prefix('tooltip-feature')
				->group(function() use ($router){
					$router->get('/', '\WebApp\Admin\Controllers\TooltipController@index');
					$router->get('/add', '\WebApp\Admin\Controllers\TooltipController@add');
					$router->post('/add', '\WebApp\Admin\Controllers\TooltipController@create');
					$router->get('/edit/{id}', '\WebApp\Admin\Controllers\TooltipController@edit');
					$router->post('/update', '\WebApp\Admin\Controllers\TooltipController@update');
					$router->delete('/delete', '\WebApp\Admin\Controllers\TooltipController@delete');
				});
       });

/**
 * End of file
 */