<?php

namespace WebApp\Admin\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;

use Api\BankAccounts\Services\BankAccountService;
use Api\BankAccounts\Repositories\BankAccountRepository;
use Api\BankAccounts\Models\BankAccount;

use Api\Banks\Services\BankService;
use Api\Banks\Repositories\BankRepository;

class BankAccountController extends Controller{
	private $bankAccountService;
	private $bankService;
	private $repositories;

	public function __construct(BankAccountService $bankAccountService, BankService $bankService){
		$this->middleware('auth');
		$this->bankAccountService = $bankAccountService;
		$this->bankService = $bankService;
		$this->repositories['bank_account'] = new BankAccountRepository();
		$this->repositories['bank'] = new BankRepository();
	}

	public function index(Request $request){
		$search = (!empty($request->search)) ? $request->search : '';
		$limit = (!empty($request->limit)) ? $request->limit : 10;
		$bank_accounts = $this->repositories['bank_account']->getModel()
			->where('account_number','like',"%$search%")
            ->orWhere('account_under_name', 'like', "%$search%")
            ->paginate($limit);
		//$bank_accounts = $this->bankAccountService->getAll();
		return view('bank-account.index', array(
			'bank_accounts' => $bank_accounts,
			'title' => 'Bank Accounts',
			'sub_title' => 'Melalui halaman ini admin dapat menampilkan, edit, tambah dan hapus rekening bank untuk pembayaran.'
		));
	}

	public function add(){
		$bank = $this->bankService->getAll();
		return view('bank-account.create', array(
			'banks' => $bank,
			'title' => 'Tambah Bank Account',
			'sub_title' => 'Melalui halaman ini admin dapat menambahkan rekening bank baru.'
		));
	}

	public function create(Request $request){
		try{
			$submit_bank_account['account_number'] = $request->input('account_number');
			$submit_bank_account['account_under_name'] = $request->input('account_under_name');
			$submit_bank_account['bank_id'] = $request->input('bank');
			$submit_bank_account['status'] = $request->input('status');

			$this->bankAccountService->create($submit_bank_account);
			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data berhasil disimpan!');
		}catch (\Exception $ex){
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal menyimpan data!');
		}
		return redirect()->back();
	}

	public function edit(int $id){
    	$bankAccount = $this->bankAccountService->getById($id);
    	$bank = $this->bankService->getAll();
    	return view ('bank-account.edit', [
    		'title' => 'Edit Bank Account',
    		'sub_title' => 'Melalui halaman ini admin dapat mengubah data rekening bank.',
    		'bank_account' => $bankAccount,
    		'banks' => $bank
    	]);
	}

	public function update(Request $request){
		try {
			$submit_bank_account['account_number'] = $request->input('account_number');
			$submit_bank_account['account_under_name'] = $request->input('account_under_name');
			$submit_bank_account['bank_id'] = (int)$request->input('bank');
			$submit_bank_account['status'] = $request->input('status');
			$bankAccountId = (int)$request->input('id');

			$this->bankAccountService->update($bankAccountId, $submit_bank_account);
			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data berhasil diubah!');
		} catch (\Exception $ex) {
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal mengubah data!');
		}
		return redirect()->back();
	}

	public function delete(Request $request){
        try{
            $deleteBankAccount = $this->bankAccountService->delete($request->input('id'));
            return response()->json(['status' => true, 'message' => 'Berhasil menghapus rekening bank.']);
        }catch (\Exception $ex){
            return response()->json(["status" => false, "message" => "Maaf, gagal menghapus user.", "error" => $ex->getMessage()]);
        }
    }

}