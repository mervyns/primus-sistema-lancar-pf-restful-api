<?php

namespace WebApp\Admin\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;
use Illuminate\Support\Facades\Log;

use Api\UserOrders\Services\UserOrderService;
use Api\UserOrders\Repositories\UserOrderRepository;
use Api\UserOrders\Models\UserOrder;

use Api\UserProducts\Services\UserProductService;
use Api\UserProducts\Repositories\UserProductRepository;

class UserOrderController extends Controller{
	private $userOrderService;
	private $userProductService;
	private $repositories;

	public function __construct(UserOrderService $userOrderService, UserProductService $userProductService){
		$this->middleware('auth');
		$this->userOrderService = $userOrderService;
		$this->userProductService = $userProductService;
		$repositories['user_order'] = new UserOrderRepository();
	}

	public function index(Request $request){
		$user_orders = $this->userOrderService->getAll();
		// $search = (!empty($request->search)) ? $request->search : '';
		// $limit = (!empty($request->limit)) ? $request->limit : 10;
		// $user_orders = UserOrder::with('bankAccounts')->with('userProducts')
		// 	->with('bankAccounts.banks')->with('userProducts.users')
		// 	->paginate($limit);
		// $user_orders->withPath('?search='.$search.'&limit='.$limit);
		return view('user-order.index', [
			'title' => 'User Orders',
			'sub_title' => 'Melalui halaman ini admin dapat mengkonfirmasi atau menolak pembayaran user.',
			'user_orders' => $user_orders
		]);
	}

	public function show(Request $request){
		$id = (int)$request->id;
		try{
			$user_order = $this->userOrderService->getById($id);
			return response()->json(array('status' => true, 'data' => $user_order));
		}catch (\Exception $ex){
			return response()->json(array('status' => false, 'data' => array()));
		}
	}
	
	public function approve(Request $request){
		$mailMessage = '';
		
		$id = (int)$request->id;
		$years = (int)$request->years;
		$status = $request->status;

		if ($status == 'approved'){
			$mailMessage = 'Terima kasih telah menggunakan produk SPT kami. Pembyaran produk telah disetujui.';
		}else{
			$mailMessage = 'Pembayaran anda ditolak.';
		}

		\DB::beginTransaction();
		try{
			$productOrder = $this->userOrderService->getById($id);
			$paramEmail = array('email' => $productOrder->userProducts->users->email , 'message' => $mailMessage);
			$submit_user_order['status'] = $status;

			$submit_user_product['type'] = 'paid';
			$submit_user_product['start_at'] = date("Y-m-d");
			$submit_user_product['expired_at'] = date('Y-m-d', strtotime('+'.$years.' years'));
			$userOrder = $this->userOrderService->update($id, $submit_user_order, $paramEmail);
			$this->userProductService->update($userOrder->user_product_id, $submit_user_product);

			\DB::commit();
			return response()->json(array('status' => true));
		}catch (\Exception $ex){
			\DB::rollBack();
			return response()->json(array('status' => false, 'message' => $ex->getMessage()));
		}
		return response()->json(array('status' => true)); 
	}

	public function invite(Request $request){
		$email = $request->useremail;
	}

	public function getPartner(Request $request){
		$id = (int)$request->id;
		try{
			$checkUserProduct = $this->userProductService->checkProductSubscription($id);
			return response()->json(array('status' => true, 'data' => $checkUserProduct));
		}catch (\Exception $ex){
			return response()->json(array('status' => false, 'data' => array(), 'message' => $ex->getMessage()));
		}
	}

	public function newPartner(Request $request){

	}

	public function deletePartner(Request $request){

	}
}