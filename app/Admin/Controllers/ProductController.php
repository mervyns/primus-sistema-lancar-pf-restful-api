<?php

namespace WebApp\Admin\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Products\Services\ProductService;
//use Api\Products\Repositories\ProductRepository;
//use WebApp\Admin\Requests\ProductRequest;

class ProductController extends Controller{
	private $productService;

	public function __construct(ProductService $productService){
		$this->middleware('auth');
		$this->productService = $productService;
	}
	
	public function index(){
		$product = $this->productService->getAll();
		return view('products.index', [
			'title' => 'Produk dan Promo',
			'sub_title' => 'Melalui halaman ini, admin bisa menambah, mengubah, menghapus data produk dan mengaktifkan promo produk.',
			'products' => $product
		]);
	}

	public function add(){
		return view('products.create', [
			'title' => 'Tambah Produk dan Promo',
			'sub_title' => 'Melalui halaman ini, admin dapat menambahkan data produk dan promo baru.'
		]);
	}

	public function create(Request $request){
		try{
			$arrDescription = array();
			if (!empty($request->input('description'))){
				foreach ($request->input('description') as $row){
					if (empty($row)) continue;
					$arrDescription[] = $row;
				}
			}

			$product_submit['name'] = $request->input('product_name');
			$product_submit['description'] = (count($arrDescription) > 0) ? json_encode($arrDescription) : "";
			$product_submit['invites'] = $request->input('invites');
			$product_submit['price'] = $request->input('price');
			$product_submit['promo'] = $request->input('promo');
			$product_submit['promo_description'] = $request->input('promo_description');

			$this->productService->create($product_submit);
			
			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data produk berhasil disimpan!');
		}catch (\Exception $ex){
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal menyimpan data product!');
		}
		return redirect()->back();
	}

	public function edit($id){
		$product = $this->productService->getById($id);
		return view('products.edit', [
			'title' => 'Edit Produk dan Promo',
			'sub_title' => 'Melalui halaman ini, admin dapat mengubah data produk dan promo.',
			'product' => $product
		]);
	}

	public function update(Request $request){
		try{
			$arrDescription = array();
			if (!empty($request->input('description'))){
				foreach ($request->input('description') as $row){
					if (empty($row)) continue;
					$arrDescription[] = $row;
				}
			}

			$product_submit['name'] = $request->input('product_name');
			$product_submit['description'] = (count($arrDescription) > 0) ? json_encode($arrDescription) : "";
			$product_submit['invites'] = $request->input('invites');
			$product_submit['price'] = $request->input('price');
			$product_submit['promo'] = $request->input('promo');
			$product_submit['promo_description'] = $request->input('promo_description');

			$this->productService->update($request->input('id'), $product_submit);

			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data produk berhasil disimpan!');
		}catch (\Exception $ex){
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal menyimpan data product!');
		}
		return redirect()->back();
	}

	public function delete(Request $request){
        try{
            $deleteProduk = $this->productService->delete($request->input('id'));
            return response()->json(["status" => true, "message" => "Berhasil menghapus produk."]);
        }catch (\Exception $ex){
            return response()->json(["status" => false, "message" => "Maaf, gagal menghapus produk.", "error" => $ex->getMessage()]);
		}
		
    }


}