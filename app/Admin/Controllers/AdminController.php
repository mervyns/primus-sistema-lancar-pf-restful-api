<?php

namespace WebApp\Admin\Controllers;

use Infrastructure\Http\Controller;

class AdminController extends Controller
{
    public function __construct()
    {
	    $this->middleware('auth');
    }

    public function index()
    {
	    return view('admin-home');
    }

}
