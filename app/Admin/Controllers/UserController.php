<?php

namespace WebApp\Admin\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;
use Api\Users\Services\UserService;
use Api\UserSecurityQuestions\Repositories\UserSecurityQuestionRepository;
use Api\SecurityQuestions\Repositories\SecurityQuestionRepository;
use Api\Users\Repositories\UserRepository;
use Api\Roles\Services\RoleService;
use Api\Users\Models\User;
use WebApp\Admin\Requests\UserRequest;

use Api\UserProducts\Repositories\UserProductRepository;
use Api\UserProducts\Services\UserProductService;
use Api\UserOrders\Repositories\UserOrderRepository;
use Api\UserOrders\Services\UserOrderService;
use Api\UserProducts\Services\UserInviteService;
use Api\UserProducts\Repositories\UserInviteRepository;

class UserController extends Controller
{
	private $userService;
    private $roleService;
    private $userOrderService;
    private $userProductService;
    private $userInviteService;

    private $repositories;
    public function __construct(UserService $userService, RoleService $roleService, UserProductService $userProductService, UserOrderService $userOrderService, UserInviteService $userInviteService)
    {
	    $this->middleware('auth');

	    $this->userService = $userService;
        $this->roleService = $roleService;
        $this->userOrderService = $userOrderService;
        $this->userProductService = $userProductService;
        $this->userInviteService = $userInviteService;

        $this->repositories['user'] = new UserRepository();
        $this->repositories['user_order'] = new UserOrderRepository();
        $this->repositories['user_product'] = new UserProductRepository();
        $this->repositories['user_invite'] = new UserInviteRepository();
    }

    public function index(Request $request)
    {
        $search = (!empty($request->search)) ? $request->search : '';
        $limit = (!empty($request->limit)) ? $request->limit : 10;
        $users = $this->repositories['user']->getModel()
            ->where('name','like',"%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->paginate($limit);
        $users->withPath('?search='.$search.'&limit='.$limit);
        //$users = $this->userService->getAll();
	    return view('users.index', [
	    	'title' => "User Management",
		    'sub_title' => "Melalui halaman ini admin bisa menampilkan, edit, aktif/nonaktif-kan, dan menghapus user.",
		    'users' => $users,
	    ]);
    }

    public function add(){
        $roles = $this->roleService->getAll();
        return view('users.create', ['roles' => $roles]);
    }

    public function store(UserRequest $request){
        \DB::beginTransaction();
        try{
            $submit_user['name'] = $request->input('name');
            $submit_user['gender'] = $request->input('gender');
            $submit_user['birthdate'] = $request->input('birthdate');
            $submit_user['mobilenumber'] = $request->input('mobilenumber');
            $submit_user['homenumber'] = $request->input('homenumber');
            $submit_user['faxnumber'] = $request->input('faxnumber');
            $submit_user['password'] = $request->input('password');
            $submit_user['email'] = $request->input('email');
            $submit_user['isActive'] = 0; //$request->input('isActive');
            $submit_user['password'] = $request->input('password');

            $user = $this->userService->create($submit_user);

            $this->userService->assignToRoles($user->id, array($request->input('roleid')));

            DB::commit();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data user berhasil disimpan!');
        }catch (\Exception $ex){
            \DB::rollBack();
            //dd($ex->getMessage());
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal menyimpan data user!');
        }
        
        return redirect()->back();
    }

    public function edit($id){
    	$userProfile = $this->userService->getById($id);
        //dd($userProfile->roles[0]->id);
        $roles = $this->roleService->getAll();
    	return view('users.edit', [
    		'user_profile' => $userProfile,
            'roles' => $roles
    	]);
    }

    public function update(UserRequest $request){
        \DB::beginTransaction();
        try{
            $submit_user['name'] = $request->input('name');
            $submit_user['email'] = $request->input('email');
            $submit_user['gender'] = $request->input('gender');
            $submit_user['birthdate'] = $request->input('birthdate');
            $submit_user['mobilenumber'] = $request->input('mobilenumber');
            $submit_user['homenumber'] = $request->input('homenumber');
            $submit_user['faxnumber'] = $request->input('faxnumber');
            $submit_user['password'] = $request->input('password');
            $submit_user['isActive'] = $request->input('isActive');

            $this->userService->updateProfile($request->input('id'), $submit_user);
            $this->userService->assignToRoles($request->input('id'), array($request->input('roleid')));

            \DB::commit();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data user berhasil diupdate!');
        }catch (\Exception $ex){
            \DB::rollBack();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal mengupdate data user!');
        }

        return redirect()->back();
    }

    public function delete(Request $request){
        try{
            $deleteUser = $this->userService->delete($request->input('id'));
            return response()->json(["status" => true, "message" => "Berhasil menghapus user."]);
        }catch (\Exception $ex){
            return response()->json(["status" => false, "message" => "Maaf, gagal menghapus user.", "error" => $ex->getMessage()]);
        }
    }

    public function getPartner(Request $request){
        $id = (int)$request->id; //user id
        try{
            $data = $this->userProductService->checkProductSubscription($id);
            if (empty($data)){
                return response()->json(array('status' => false, 'message' => 'Maaf, user produk tidak tersedia.'));
            }else{
                if (empty($data->parent_id)){
                    return response()->json(array("status" => true, 'data' => $data));
                }else{
                    return response()->json(array('status' => false, 'message' => 'Maaf, user produk tidak tersedia'));
                }
            }
        }catch (\Exception $ex){
            return response()->json(array("status" => false, 'message' => $ex->getMessage()));
        }
    }

    public function newPartner(Request $request){
        $email = $request->email;
        $user_parent_id = $request->user_parent_id;
        $wife_or_husband = $request->wife_or_husband;
        
        try{
            $checkEmailExists = $this->userInviteService->checkEmailInvited($email);
            if ($checkEmailExists['status']['invited']['status'] == true){
                return response()->json(array('status' => false, 'message' => $checkEmailExists['status']['invited']['message']));
            }
            if ($checkEmailExists['status']['member']['status'] == true){
                return response()->json(array('status' => false, 'message' => $checkEmailExists['status']['member']['message']));
            }

            $checkProduct = $this->userProductService->checkProductSubscription($user_parent_id);
            
            $submit_user_invite['product_id'] = $checkProduct['data']->product_id;
            $submit_user_invite['years'] = $checkProduct['data']->years;
            $submit_user_invite['type'] = $checkProduct['data']->type;
            $submit_user_invite['user_parent_id'] = $user_parent_id;
            $submit_user_invite['email'] = $email;
            $submit_user_invite['wife_or_husband'] = $wife_or_husband;
            $submit_user_invite['promo'] = $checkProduct['data']->promo;
            $submit_user_invite['promo_description'] = $checkProduct['data']->promo_description;
            $submit_user_invite['parent_id'] = $checkProduct['data']->id;
            if (!empty($checkProduct['data']->start_at)){
                $submit_user_invite['start_at'] = $checkProduct['data']->start_at;
            }
            if (!empty($checkProduct['data']->expired_at)){
                $submit_user_invite['expired_at'] = $checkProduct['data']->expired_at;
            }
            
            $sender['id'] = $user_parent_id;
            $sender['email'] = $checkProduct['data']->users->email;

            $receiver['email'] = $email;

            $this->userProductService->create($submit_user_invite, $sender, $receiver, true);

            return response()->json(array('status' => true, 'message' => 'Berhasil invite produk user.'));
        }catch (\Exception $ex){
            return response()->json(array('status' => false, 'message' => 'Maaf, Gagal invite produk user.', 'error' => $ex->getMessage()));
        }
    }

    public function deletePartner(Request $request){
        $result = array();
        $id = $request->user_product_id;
        try{
            $this->userProductService->delete($id);
            return response()->json(array('status' => true, 'message' => 'Data berhasil dihapus.'));
        }catch (\Exception $ex){
            return response()->json(array('status' => false, 'message' => 'Maaf, gagal menghapus data.', 'error' => $ex->getMessage()));
        }
    }

}
