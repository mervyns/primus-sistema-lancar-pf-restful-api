<?php

namespace WebApp\Admin\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;

use Api\Tooltips\Services\TooltipService;
use Api\Tooltips\Repositories\TooltipRepository;
use Api\Tooltips\Models\Tooltips;
use Api\System\Services\SystemStepConfigService;
use Api\System\Repositories\SystemStepConfigRepository;

class TooltipController extends Controller{
	private $tooltipService;
	private $systemStepConfigService;
	private $repositories;

	public function __construct(SystemStepConfigService $systemStepConfigService, TooltipService $tooltip){
		$this->middleware('auth');
		$this->tooltipService = $tooltip;
		$this->systemStepConfigService = $systemStepConfigService;
		$this->repositories['systemStepConfig'] = new SystemStepConfigRepository();
		$this->repositories['tooltip'] = new TooltipRepository();
	}

	public function index(Request $request){
		$search = (!empty($request->search)) ? $request->search : '';
		$limit = (!empty($request->limit)) ? $request->limit : 10;
		$tooltip = $this->repositories['tooltip']->getModel()
			->with('systemStepConfig')
			->where('judul','like',"%$search%")
            ->orWhere('content', 'like', "%$search%")
			->paginate($limit);
		// dd($tooltip);
		return view('tooltip-feature.index', array(
			'tooltips' => $tooltip,
			'title' => 'Tooltips Feature',
			'sub_title' => 'Melalui halaman ini admin dapat menampilkan, edit, tambah dan hapus tooltip.'
		));
	}

	public function add(){
		$steps = $this->systemStepConfigService->getAll();
		return view('tooltip-feature.create', array(
			'steps' => $steps,
			'title' => 'Tambah Tooltips',
			'sub_title' => 'Melalui halaman ini admin dapat menambahkan tooltip baru.'
		));
	}

	public function create(Request $request){
		try{
			$submit_tooltip['judul'] = $request->input('judul');
			$submit_tooltip['link'] = $request->input('link');
			$submit_tooltip['content'] = $request->input('content');
			$submit_tooltip['step_config_id'] = $request->input('step');

			$this->tooltipService->create($submit_tooltip);
			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data berhasil disimpan!');
		}catch (\Exception $ex){
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal menyimpan data!');
		}
		return redirect()->back();
	}

	public function edit(int $id){
		$tooltip = $this->tooltipService->getById($id);
    	$steps = $this->systemStepConfigService->getAll();
    	return view ('tooltip-feature.edit', [
    		'title' => 'Edit Bank Account',
    		'sub_title' => 'Melalui halaman ini admin dapat mengubah data tooltip.',
    		'tooltip' => $tooltip,
    		'steps' => $steps
    	]);
	}

	public function update(Request $request){
		try {
			$submit_tooltip['judul'] = $request->input('judul');
			$submit_tooltip['link'] = $request->input('link');
			$submit_tooltip['content'] = $request->input('content');
			$submit_tooltip['step_config_id'] = $request->input('step');
			$tooltipId = (int)$request->input('id');

			$this->tooltipService->update($tooltipId, $submit_tooltip);
			$request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Data berhasil diubah!');
		} catch (\Exception $ex) {
			$request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal mengubah data!');
		}
		return redirect()->back();
	}

	public function delete(Request $request){
        try{
            $deletetooltip = $this->tooltipService->delete($request->input('id'));
            return response()->json(['status' => true, 'message' => 'Berhasil menghapus rekening bank.']);
        }catch (\Exception $ex){
            return response()->json(["status" => false, "message" => "Maaf, gagal menghapus user.", "error" => $ex->getMessage()]);
        }
    }

}