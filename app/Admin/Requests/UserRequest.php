<?php

namespace WebApp\Admin\Requests;

use Infrastructure\Http\WebRequest;

class UserRequest extends WebRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'gender' => 'required|string',
            'birthdate' => 'required|date_format:Y-m-d',
            'mobilenumber' => 'required|numeric',
            'homenumber' => 'required|numeric',
            'faxnumber' => 'required|numeric',
            'email' => 'required|string'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'fullname',
            'gender' => 'gender',
            'birthdate' => 'birth date',
            'mobilenumber' => 'mobile number',
            'homenumber' => 'home number',
            'faxnumber' => 'fax number',
            'email' => 'email'
        ];
    }
}
