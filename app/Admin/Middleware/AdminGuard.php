<?php
namespace WebApp\Admin\Middleware;

use Closure;
use Auth;

class AdminGuard
{

	public function handle($request, Closure $next)
	{
		$user = Auth::user();

		if (strpos($request->path(), 'profile/me') !== false || strpos($request->path(), 'logout') !== false) {
			return $next($request);
		}

		if ($userRoles = isset($user->roles) ? $user->roles() : null) {
			$isAdmin = $userRoles->where('alias', '=', 'admin')->count() > 0;
			$isAdminPage = strpos($request->path(), 'admin') !== false;

			if (!$isAdmin && $isAdminPage) {
				return redirect('/unauthorized');
			}

			if ($isAdmin) {
				return !$isAdminPage ? redirect('/admin') : $next($request);
			}
		}

		return $next($request);
	}
}