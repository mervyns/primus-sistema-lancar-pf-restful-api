<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class CreditCard {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		$data['credit_card'] = $this->repositories['user']->liability()->where('liability_id', '=', 2)->get();

		for ($i = 0; $i < count($data['credit_card']); $i++){
			$detail = json_decode($data['credit_card'][$i]['detail'], true);
			
			$data['credit_card'][$i]['country'] = $detail['country'];
			$data['credit_card'][$i]['currency'] = $detail['currency'];
			$data['credit_card'][$i]['currency_value'] = $detail['currency_value'];
			$data['credit_card'][$i]['kurs_value'] = $detail['kurs_value'];
			$data['credit_card'][$i]['bank_id'] = $detail['bank_id'];
			$data['credit_card'][$i]['credit_card_name'] = $detail['credit_card_name'];
			$data['credit_card'][$i]['credit_card_type'] = $detail['credit_card_type'];
			$data['credit_card'][$i]['other_bank'] = $detail['other_bank'];

			$bank = $bankRepository->getModel()->find($data['credit_card'][$i]['bank_id']);
			$data['credit_card'][$i]['bank_name'] = !empty($bank->name) ? $bank->name : null;
		}
		
		return $data;
		
		// $userLiability['credit_card'] = $this->repositories['user']->liability()->where('liability_id', '=', 2)->get();

		// $userLiability['tax_profile'] = $this->repositories['user']->taxProfile;
		
		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userLiability['credit_card']->isEmpty()){
		// 	$i = 0; //start decode detail
		// 	foreach ($userLiability['credit_card'] as $liability) {
		// 		$detail = json_decode( $liability['detail'], true );

		// 		$userLiability['credit_card'][$i]['country'] = $detail['country'];
		// 		$userLiability['credit_card'][$i]['currency'] = $detail['currency'];
		// 		$userLiability['credit_card'][$i]['liability_foreign_value'] = $detail['liability_foreign_value'];
		// 		$userLiability['credit_card'][$i]['bank_name'] = $detail['bank_name'];
		// 		$userLiability['credit_card'][$i]['bank_branch'] = $detail['bank_branch'];
		// 		$userLiability['credit_card'][$i]['bank_city'] = $detail['bank_city'];
		// 		$userLiability['credit_card'][$i]['credit_card_name'] = $detail['credit_card_name'];
		// 		$userLiability['credit_card'][$i]['credit_card_type'] = $detail['credit_card_type'];
		// 		$i++;
		// 	}
		// 	$userLiability['credit_card'] = $userLiability['credit_card']->toArray();
		// } else{
		// 	$userLiability['credit_card'] = [];
		// }

		// if(!empty($userLiability['tax_profile'])){
		// 	$userLiability['tax_profile'] = $userLiability['tax_profile']->toArray();
		// } else{
		// 	$userLiability['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userLiability, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = null;
		$data['country'] = [];
		$data['bank_list'] = [];
		$data['credit_card'] = [];

		if(!empty($id)){
			$countryRepository = new CountryRepository();
			$data['country'] = $countryRepository->get()->toArray();

			$bankRepository = new BankRepository();
			$data['bank_list'] = $bankRepository->get()->toArray(); 

			$data['credit_card'] = $this->repositories['user']->liability()->where('liability_id', '=', 2)->get();

			for ($i = 0; $i < count($data['credit_card']); $i++){
				$detail = json_decode($data['credit_card'][$i]['detail'], true);
				$data['credit_card'][$i]['country'] = $detail['country'];
				$data['credit_card'][$i]['currency'] = $detail['currency'];
				$data['credit_card'][$i]['currency_value'] = $detail['currency_value'];
				$data['credit_card'][$i]['kurs_value'] = $detail['kurs_value'];
				$data['credit_card'][$i]['bank_id'] = $detail['bank_id'];
				$data['credit_card'][$i]['credit_card_name'] = $detail['credit_card_name'];
				$data['credit_card'][$i]['credit_card_type'] = $detail['credit_card_type'];
				$data['credit_card'][$i]['other_bank'] = $detail['other_bank'];
				$data['credit_card'][$i]['active'] = $data['credit_card'][$i]['id'] == $id ? true : false;

				$bank = $bankRepository->getModel()->find($data['credit_card'][$i]['bank_id']);
				$data['credit_card'][$i]['bank_name'] = !empty($bank->name) ? $bank->name : null;
				
			}
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'bank_list' => [],
				'credit_card' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}

	private function mergeBankAssetsData($localBanks, $foreignBanks)
	{
		return array_merge($this->rebuildBankAssetsData($localBanks), $this->rebuildBankAssetsData($foreignBanks, 'foreign'));
	}

	private function rebuildBankAssetsData($banks, $type = 'local')
	{
		$banksData = [];

		foreach ($banks as $bank) {
			$banksData[] = array_merge($bank, ['type' => $type]);
		}

		return $banksData;
	}

}
