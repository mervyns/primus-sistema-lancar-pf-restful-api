<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;
use Api\Incomes\Repositories\IncomeRepository;

class IncomeFromDeposit {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'income' => new IncomeRepository,
		];
	}

	public function get()
	{
        $data = [];
        
        $countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		$data['user_income'] = $this->repositories['user']->income()->where('income_id', 38)->get();
		$data['income'] = $this->repositories['income']->getById(38);

        if (isset($data['user_income'])){
            for ($i = 0; $i < count($data['user_income']); $i++){
				$detail = json_decode($data['user_income'][$i]['detail'], true);
				
				$data['user_income'][$i]['currency'] = $detail['currency'];
				$data['user_income'][$i]['type'] = $detail['type'];
				$data['user_income'][$i]['tax_final'] = $detail['tax_final'];
				$data['user_income'][$i]['kurs_value'] = $detail['kurs_value'];
				$data['user_income'][$i]['tax_rate'] = $detail['tax_rate'];
				$data['user_income'][$i]['deposit_number'] = $detail['deposit_number'];
				$data['user_income'][$i]['currency_value'] = $detail['currency_value'];
            }
		}
		// dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;

		$data['user_income'] = [];
		$data['income'] = $this->repositories['income']->getById(38);

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		if(!empty($id)){

			$data['user_income'] = $this->repositories['user']->income()->where('income_id', 38)->get();
			$data['income'] = $this->repositories['income']->getById(38);

			if (isset($data['user_income'])){
				for ($i = 0; $i < count($data['user_income']); $i++){
					$detail = json_decode($data['user_income'][$i]['detail'], true);
					
					$data['user_income'][$i]['currency'] = $detail['currency'];
					$data['user_income'][$i]['type'] = $detail['type'];
					$data['user_income'][$i]['tax_final'] = $detail['tax_final'];
					$data['user_income'][$i]['kurs_value'] = $detail['kurs_value'];
					$data['user_income'][$i]['tax_rate'] = $detail['tax_rate'];
					$data['user_income'][$i]['deposit_number'] = $detail['deposit_number'];
					$data['user_income'][$i]['currency_value'] = $detail['currency_value'];
					$data['user_income'][$i]['active'] = $data['user_income'][$i]->id == $id ? true : false; ;
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'income' => $data['income'],
				'country' => $data['country'],
				'bank_list' => $data['bank_list'],
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}

}