<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserAssets\Services\UserAssetService;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class SaleOfShares {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		
		$data['user_income'] = $this->repositories['user']->income()->where('income_id', 5)->get();
        if (isset($data['user_income'])){
            for ($i = 0; $i < count($data['user_income']); $i++){
                $detail = json_decode($data['user_income'][$i]['detail'], true);

                $data['user_income'][$i]['tax_final'] = isset($detail['tax_final']) && $detail['tax_final'];
                $data['user_income'][$i]['tax_rate'] = isset($detail['tax_rate']) && $detail['tax_rate'];
                $data['user_income'][$i]['founder'] = isset($detail['founder']) && $detail['founder'];
                $data['user_income'][$i]['company_name'] = isset($detail['company_name']) && $detail['company_name'];
            }
		}
		// dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;
		$data['user_income'] = [];

		if(!empty($id)){
			$data['user_income'] = $this->repositories['user']->income()->where('income_id', 5)->get();
			if (isset($data['user_income'])){
				for ($i = 0; $i < count($data['user_income']); $i++){
					$detail = json_decode($data['user_income'][$i]['detail'], true);

					$data['user_income'][$i]['tax_final'] = $detail['tax_final'];
					$data['user_income'][$i]['tax_rate'] = $detail['tax_rate'];
					$data['user_income'][$i]['founder'] = $detail['founder'];
					$data['user_income'][$i]['company_name'] = $detail['company_name'];
					$data['user_income'][$i]['active'] = $data['user_income'][$i]->id == $id ? true : false; 
				
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}

}
