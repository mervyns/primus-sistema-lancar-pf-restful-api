<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class AdditionalRegularIncomesProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = $detail;
		}
		
		return $data;
		
		// $userAdditionalProfile['additional_profile'] = $this->repositories['user']->additionalProfile;

		// $userAdditionalProfile['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!empty($userAdditionalProfile['additional_profile'])){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAdditionalProfile['additional_profile'] as $uAP) {
		// 		$detail = json_decode( $uAP['detail'], true );

		// 		if(isset($detail['diskonto_sbi']))
		// 		$userAdditionalProfile['additional_profile'][$i]['diskonto_sbi'] = $detail['diskonto_sbi'];

		// 		if(isset($detail['diskonto_obligasi']))
		// 		$userAdditionalProfile['additional_profile'][$i]['diskonto_obligasi'] = $detail['diskonto_obligasi'];

		// 		if(isset($detail['sewa_tanah_bangunan']))
		// 		$userAdditionalProfile['additional_profile'][$i]['sewa_tanah_bangunan'] = $detail['sewa_tanah_bangunan'];

		// 		if(isset($detail['dividen']))
		// 		$userAdditionalProfile['additional_profile'][$i]['dividen'] = $detail['dividen'];

		// 		if(isset($detail['saham_bursa_efek']))
		// 		$userAdditionalProfile['additional_profile'][$i]['saham_bursa_efek'] = $detail['saham_bursa_efek'];

		// 		if(isset($detail['royalti_haki']))
		// 		$userAdditionalProfile['additional_profile'][$i]['royalti_haki'] = $detail['royalti_haki'];

		// 		if(isset($detail['sewa_diluar_tanah']))
		// 		$userAdditionalProfile['additional_profile'][$i]['sewa_diluar_tanah'] = $detail['sewa_diluar_tanah'];

		// 		if(isset($detail['bunga_pengembalian_hutang']))
		// 		$userAdditionalProfile['additional_profile'][$i]['bunga_pengembalian_hutang'] = $detail['bunga_pengembalian_hutang'];

		// 		if(isset($detail['perseroan_komanditer']))
		// 		$userAdditionalProfile['additional_profile'][$i]['perseroan_komanditer'] = $detail['perseroan_komanditer'];

		// 		if(isset($detail['honorarium_komisaris']))
		// 		$userAdditionalProfile['additional_profile'][$i]['honorarium_komisaris'] = $detail['honorarium_komisaris'];

		// 		if(isset($detail['upah_pegawai_tidak_tetap']))
		// 		$userAdditionalProfile['additional_profile'][$i]['upah_pegawai_tidak_tetap'] = $detail['upah_pegawai_tidak_tetap'];

		// 		if(isset($detail['imbalan_tenaga_ahli']))
		// 		$userAdditionalProfile['additional_profile'][$i]['imbalan_tenaga_ahli'] = $detail['imbalan_tenaga_ahli'];

		// 		if(isset($detail['penghasilan_lainnya']))
		// 		$userAdditionalProfile['additional_profile'][$i]['penghasilan_lainnya'] = $detail['penghasilan_lainnya'];

		// 		$i++;
		// 	}
		// 	$userAdditionalProfile['additional_profile'] = $userAdditionalProfile['additional_profile']->toArray();
		// } else{
		// 	$userAdditionalProfile['additional_profile'] = [];
		// }

		// if(!empty($userAdditionalProfile['tax_profile'])){
		// 	$userAdditionalProfile['tax_profile'] = $userAdditionalProfile['tax_profile']->toArray();
		// } else{
		// 	$userAdditionalProfile['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAdditionalProfile, $country);

		// return $mergedData ? $mergedData : [];
	}
}
