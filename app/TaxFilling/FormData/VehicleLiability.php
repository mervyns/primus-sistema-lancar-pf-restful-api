<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class VehicleLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'bank' => new BankRepository,
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$vehicle = $this->repositories['user']->asset()->whereIn('asset_id', ['18', '19', '20'])->get();
		$liability =  $this->repositories['user']->liability()->where('liability_id', 1)->get();
		$data['vehicle'] = $this->parseVehicle($vehicle);
		$data['country'] = $this->repositories['country']->get();
		$data['user_liability'] =$this->parseData($liability);

		if (isset($data['user_liability'])){
			for ($i = 0; $i < count($data['user_liability']); $i++){
				$detail = json_decode($data['user_liability'][$i]['detail'], true);

				$data['user_liability'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
				$data['user_liability'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
				$data['user_liability'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
				$data['user_liability'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
				$data['user_liability'][$i]['vehicle_id'] = $detail['vehicle_id'];
				$data['user_liability'][$i]['vehicle_merk'] = $detail['vehicle_merk'];
				$data['user_liability'][$i]['credite_duration'] = $detail['credite_duration'];
				$data['user_liability'][$i]['paid_off_date'] = $detail['paid_off_date'];
				$data['user_liability'][$i]['yearly_interest'] = $detail['yearly_interest'];
			}
		}
		
		return $data;
	}

	private function parseData($data){
		$result = [];
		if (isset($data)){
			foreach ($data as $key => $value){
				$detail = json_decode($value['detail'], true);
				
				if (isset($detail['credite_type']) && $detail['credite_type'] == 'vehicle'){
					$result[] = $value;
				}
			}
		}
		return $result;
	}

	private function parseVehicle($data){
		$result = [];

		if (isset($data)){
			foreach ($data as $key => $value){
				$detail = json_decode($value['detail'], true);
				if (isset($detail['credite_type']) && $detail['credite_type'] == 'Ya'){
					$plat = $detail['plate_number'];
					$bpkb = $detail['bpkb_number'];
					if (!empty($bpkb)) $plat .= ' / '. $bpkb;

					$result[] = [
						'id' => $value['id'],
						'asset_id' => $value['asset_id'],
						'merk' => $detail['vehicle_merk'],
						'value' => $plat,
					];
				}
			}
		}

		return $result;
	}

}
