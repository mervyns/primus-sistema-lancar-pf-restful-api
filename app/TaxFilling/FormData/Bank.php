<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class Bank {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];

		$localBankAssets = $this->repositories['user']->asset()->where('asset_id', '=', 2)->get()->toArray(); // original filter = 2
		$foreignBankAssets = $this->repositories['user']->foreignAsset()->where('asset_id', '=', 2)->get()->toArray(); // original filter = 2
		
		$data['banks'] = $this->mergeBankAssetsData($localBankAssets, $foreignBankAssets);

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		for ($i=0; $i < count($data['banks']); $i++) {
			$detail = json_decode($data['banks'][$i]['detail']);

			$data['banks'][$i]['currency'] = isset($detail->currency) ? $detail->currency : '';
			$data['banks'][$i]['currency_value'] = isset($detail->currency_value) ? $detail->currency_value : '';
			$data['banks'][$i]['kurs_value'] = isset($detail->kurs_value) ? $detail->kurs_value : '';
			$data['banks'][$i]['bank_type'] = isset($detail->bank_type) ? $detail->bank_type : '';
			$data['banks'][$i]['bank_id'] = isset($detail->bank_id) ? $detail->bank_id : '';
			$data['banks'][$i]['bank_number'] = isset($detail->bank_number) ? $detail->bank_number : '';
			$data['banks'][$i]['bank_city'] = isset($detail->bank_city) ? $detail->bank_city : '';
			$data['banks'][$i]['bank_branch'] = isset($detail->bank_branch) ? $detail->bank_branch : '';
			$data['banks'][$i]['interests'] = isset($detail->interests) ? $detail->interests : '';
			$data['banks'][$i]['taxes'] = isset($detail->taxes) ? $detail->taxes : '';
			$data['banks'][$i]['tax_data'] = isset($detail->tax_data) ? "[".implode(',',$detail->tax_data)."]" : '';
			$data['banks'][$i]['interest_data'] = isset($detail->interest_data) ? "[".implode(',', $detail->interest_data)."]" : '';
			$data['banks'][$i]['other_bank'] = isset($detail->other_bank) ? $detail->other_bank : '';
			$data['banks'][$i]['country'] = isset($detail->country) ? $detail->country : '';
		}
		
		//dd($data);
		return $data;
	}

	private function mergeBankAssetsData($localBanks, $foreignBanks)
	{
		return array_merge($this->rebuildBankAssetsData($localBanks), $this->rebuildBankAssetsData($foreignBanks, 'foreign'));
	}

	private function rebuildBankAssetsData($banks, $type = 'local')
	{
		$banksData = [];

		foreach ($banks as $bank) {
			$banksData[] = array_merge($bank, ['type' => $type]);
		}

		return $banksData;
	}
}
