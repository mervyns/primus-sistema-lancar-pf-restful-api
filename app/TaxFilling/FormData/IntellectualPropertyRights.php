<?php
namespace WebApp\TaxFilling\FormData;
use Api\Incomes\Repositories\IncomeRepository;

use Auth;

class IntellectualPropertyRights {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'income' => new IncomeRepository,
		];
	}

	public function get()
	{
		$data = [];
		$data['user_income'] = [];
		try{
			$userIncome =  $this->repositories['user']->income()->where('income_id', 7)->get()->toArray();
			if (isset($userIncome) == true){
				for ($i = 0; $i < count($userIncome); $i++){
					$detail = json_decode($userIncome[$i]['detail'], true);

					$userIncome[$i]['tax_rate'] = $detail['tax_rate'];
					$userIncome[$i]['taxes_type'] = $detail['taxes_type'];

					$userWithHoldingTax = $this->repositories['user']->income()
						->where( 'income_id', '=', 7)->where('id', '=', $userIncome[$i]['id'])
						->first()->withHoldingTax;
					if (isset($userWithHoldingTax)){
						$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax->toArray());
					}else{
						$data['user_income'][$i] = $userIncome[$i];
						$data['user_income'][$i]['user_income_id'] = $userIncome[$i]['id'];
					}
								
				}
			}
		}catch (\Exception $ex){
			dd($ex->getMessage());
		}
		// dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;

		$data['user_income'] = [];

		if(!empty($id)){
			$userIncome =  $this->repositories['user']->income()->where('income_id', 7)->get()->toArray();
			if (isset($userIncome) == true){
				for ($i = 0; $i < count($userIncome); $i++){

					$detail = json_decode($userIncome[$i]['detail'], true);

					$userIncome[$i]['tax_rate'] = $detail['tax_rate'];
					$userIncome[$i]['taxes_type'] = $detail['taxes_type'];

					$userWithHoldingTax = $this->repositories['user']->income()
						->where( 'income_id', '=', 7)->where('id', '=', $userIncome[$i]['id'])
						->first()->withHoldingTax;
					if (isset($userWithHoldingTax)){
						$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax->toArray());
					}else{
						$data['user_income'][$i] = $userIncome[$i];
						$data['user_income'][$i]['user_income_id'] = $userIncome[$i]['id'];
					}
					$data['user_income'][$i]['active'] = $userIncome[$i]['id'] == $id ? true : false; 	
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}
}