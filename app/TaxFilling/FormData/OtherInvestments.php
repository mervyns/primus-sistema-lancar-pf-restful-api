<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class OtherInvestments {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		$userAsset = $this->repositories['user']->asset()->where('asset_id', 17)->get();
		for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
			$userAsset[$i]['investment_form'] = $detail['investment_form'];
        }

		$data['user_asset'] = $userAsset;
		return $data;
		// $userAsset['other_investments'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 17)
		// 	->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['other_investments']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAsset['other_investments'] as $otherinvestments) {
		// 		$detail = json_decode( $otherinvestments['detail'], true );

		// 		$userAsset['other_investments'][$i]['other_investments_country'] = $detail['other_investments_country'];
		// 		$userAsset['other_investments'][$i]['other_investments_currency'] = $detail['other_investments_currency'];
		// 		$userAsset['other_investments'][$i]['other_investments_foreign_value'] = $detail['other_investments_foreign_value'];
		// 		$userAsset['other_investments'][$i]['other_investments_type'] = $detail['other_investments_type'];

		// 		$i++;
		// 	}
		// 	$userAsset['other_investments'] = $userAsset['other_investments']->toArray();
		// } else{
		// 	$userAsset['other_investments'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}
}
