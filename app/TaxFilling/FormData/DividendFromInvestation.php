<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Incomes\Repositories\IncomeRepository;

class DividendFromInvestation {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'income' => new IncomeRepository
		];
	}

	public function get()
	{
		$data = [];
		
		$data['assets'] = $this->repositories['user']->asset()
			->whereIn('asset_id', [9, 10])->get();

		if (isset($data['assets'])){
			for ($i = 0; $i < count($data['assets']); $i++){
				$detail = json_decode($data['assets'][$i]['detail']);
				$data['assets'][$i]['country_id'] = $detail->currency;
				$data['assets'][$i]['company_name'] = $detail->company_name;
			}
		}

		$data['dividend'] = $this->repositories['user']->income()->where('income_id', 3)->get();
		$data['income'] = $this->repositories['income']->getById(3);

		if (isset($data['dividend'])){
			for ($i = 0; $i < count($data['dividend']); $i++){
				$detail = json_decode($data['dividend'][$i]['detail'], true);

				$data['dividend'][$i]['tax_rate'] = $detail['tax_rate'];
				$data['dividend'][$i]['company_name'] = $detail['company_name'];
				$data['dividend'][$i]['asset_id'] = $detail['asset_id'];
				$data['dividend'][$i]['tax_final'] = $detail['tax_final'];
				
			}
		}
		// dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;
		$data['assets'] = [];
		$data['dividend'] = [];
		$data['income'] = [];

		$data['assets'] = $this->repositories['user']->asset()
			->whereIn('asset_id', [9, 10])->get();

		if (isset($data['assets'])){
			for ($i = 0; $i < count($data['assets']); $i++){
				$detail = json_decode($data['assets'][$i]['detail']);
				$data['assets'][$i]['country_id'] = $detail->currency;
				$data['assets'][$i]['company_name'] = $detail->company_name;
			}
		}

		if(!empty($id)){
			

			$data['dividend'] = $this->repositories['user']->income()->where('income_id', 3)->get();
			$data['income'] = $this->repositories['income']->getById(3);

			if (isset($data['dividend'])){
				for ($i = 0; $i < count($data['dividend']); $i++){
					$detail = json_decode($data['dividend'][$i]['detail'], true);

					$data['dividend'][$i]['tax_rate'] = $detail['tax_rate'];
					$data['dividend'][$i]['company_name'] = $detail['company_name'];
					$data['dividend'][$i]['asset_id'] = $detail['asset_id'];
					$data['dividend'][$i]['tax_final'] = $detail['tax_final'];
					
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'income' => $data['income'],
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}
}