<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MainIncome {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		try{
			$data = [];
			$userIncome = $this->repositories['user']->income()->where('income_id', '=', 1)->get()->toArray();
			foreach ($userIncome as $key => $val){
				$id = $val['id'];
				$userWithHoldingTax = $this->repositories['user']->income()
					->where( 'income_id', '=', 1 )->where('id', '=', $id)
					->first()->withHoldingTax->toArray();
				
				if (!empty($userWithHoldingTax)){
					$data[$key] = array_merge($val, $userWithHoldingTax);
				}
			}
			return $data;
		}catch(\Exception $e)
		{
			return [];
		}
	}

	public function getFinalSteps($id)
	{	
		$data = [];

		if(!empty($id)){
			$userIncome = $this->repositories['user']->income()->where('income_id', '=', 1)->get()->toArray();
			foreach ($userIncome as $key => $val){
				$id = $val['id'];
				$userWithHoldingTax = $this->repositories['user']->income()
					->where( 'income_id', '=', 1 )->where('id', '=', $id)
					->first()->withHoldingTax->toArray();
				
				if (!empty($userWithHoldingTax)){
					$data[$key] = array_merge($val, $userWithHoldingTax);
				}
			}
		}

		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}