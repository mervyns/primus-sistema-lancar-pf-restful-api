<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class PreviewSpt {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
        return [];	
	}
}
