<?php
namespace WebApp\TaxFilling\FormData;

use Auth;


class LiabilitiesProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = $detail;
		}
		return $data;
	}
}
