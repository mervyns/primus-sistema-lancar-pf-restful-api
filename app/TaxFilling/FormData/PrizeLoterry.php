<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserAssets\Services\UserAssetService;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;


class PrizeLoterry {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_income' => new UserIncomeRepository,
		];
	}

	public function get()
	{
		// $self_step_index = config('constants')['steps']['PRIZE_LOTERRY'];
        // $steps = new UserTaxFillingStepRepository();
        // $steps = $steps->setDirection($this->repositories['user']->id,$self_step_index);
        
		// return [];

		$user_income = $this->repositories['user']->income()->where('income_id', 10)->get()->toArray();
		if (count($user_income) > 0) {
			for ($i = 0; $i < count($user_income); $i++) {
				$detail = json_decode($user_income[$i]['detail'], true);
				$detail_values = array();
				foreach ($detail as $key => $val) {
					$detail_values[$key] = $val;
				}

				$user_income[$i]['active'] = $i == 0 ? true : false;
				$user_income[$i]['detail'] = $detail_values;
			}
		}
		
		return array(
			'data' => $user_income
		);
	}

	public function getFinalSteps($id)
	{

		$existing = null;

		if(!empty($id)){
			$prizes = $this->repositories['user_income']->getModel()
						->where('user_id',$this->repositories['user']->id)
						->whereIn('income_id',[10])
						->get();
			
			if(!$prizes->isEmpty()){
				foreach ($prizes as $l_key => $prize) {
					if(!empty($prize->detail)){
						$data_deatils = [];
						$details = json_decode($prize->detail,true);
	
						foreach ($details as $key => $detail) {
							$data_deatils[$key] = $detail;
						}
	
						$prize->detail = $data_deatils;
					}
					$prize->active = $prize->id == $id ? true : false; 
					$existing[$l_key] = $prize->toArray();
				}
			}
		}

		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'is_final_steps_submited' => true
			];
		}
       
		return [
			'data' => $existing
		];
	}
}
