<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class RewardsToExperts {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$data['user_income'] = [];
		try{
			$userIncome =  $this->repositories['user']->income()->where('income_id', 45)->get()->toArray();
			if (isset($userIncome) == true){
				for ($i = 0; $i < count($userIncome); $i++){
					$detail = json_decode($userIncome[$i]['detail'], true);

					$userWithHoldingTax = $this->repositories['user']->income()
						->where( 'income_id', '=', 45)->where('id', '=', $userIncome[$i]['id'])->first()->withHoldingTax;
					if (isset($userWithHoldingTax)){
						$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax->toArray());
					}else{
						$data['user_income'][$i] = $userIncome[$i];
						$data['user_income'][$i]['user_income_id'] = $userIncome[$i]['id'];
					}
				}
			}
		}catch (\Exception $ex){
			dd($ex);
		}
		// dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = [];
		$data['user_income'] = [];

		if(!empty($id)){
			$userIncome =  $this->repositories['user']->income()->where('income_id', 45)->get()->toArray();
			if (isset($userIncome) == true){
				for ($i = 0; $i < count($userIncome); $i++){
					$detail = json_decode($userIncome[$i]['detail'], true);

					$userWithHoldingTax = $this->repositories['user']->income()
						->where( 'income_id', '=', 45)->where('id', '=', $userIncome[$i]['id'])->first()->withHoldingTax;
					if (isset($userWithHoldingTax)){
						$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax->toArray());
					}else{
						$data['user_income'][$i] = $userIncome[$i];
						$data['user_income'][$i]['user_income_id'] = $userIncome[$i]['id'];
					}
					$data['user_income'][$i]['active'] = $userIncome[$i]['id'] == $id ? true : false; ;
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		// dd($data);
		return $data;
	}

}