<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserAssets\Services\UserAssetService;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Legacy {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_income' => new UserIncomeRepository,
		];
	}

	public function get()
	{
		// $self_step_index = config('constants')['steps']['LEGACY'];
        // $steps = new UserTaxFillingStepRepository();
		// $steps = $steps->setDirection($this->repositories['user']->id,$self_step_index);
       
		// return [];

		$legacy = $this->repositories['user']->income()->whereIn('income_id', [25, 26])->get()->toArray();
		if (count($legacy) > 0) {
			for ($i = 0; $i < count($legacy); $i++) {
				$detail = json_decode($legacy[$i]['detail'], true);
				$detail_values = array();
				$legacy[$i]['active'] = $i == 0 ? true : false;
				
				foreach ($detail as $key => $val) {
					$detail_values[$key] = $val;
				}

				$legacy[$i]['detail'] = $detail_values;
			}
		}
		$data = array(
			'data' => $legacy
		);
		return $data;
	}

	public function getFinalSteps($id)
	{
		$existing = null;

		if(!empty($id)){
			$legacies = $this->repositories['user_income']->getModel()
						->where('user_id',$this->repositories['user']->id)
						->whereIn('income_id',[25,26])
						->get();
			if(!$legacies->isEmpty()){
				foreach ($legacies as $l_key => $legacy) {
					if(!empty($legacy->detail)){
						$data_deatils = [];
						$details = json_decode($legacy->detail,true);
	
						foreach ($details as $key => $detail) {
							$data_deatils[$key] = $detail;
						}
	
						$legacy->detail = $data_deatils;
					}
					$legacy->active = $legacy->id == $id ? true : false; 
					$existing[$l_key] = $legacy->toArray();
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'is_final_steps_submited' => true,
			];
		}
       
		return [
			'data' => $existing
		];
	}

}
