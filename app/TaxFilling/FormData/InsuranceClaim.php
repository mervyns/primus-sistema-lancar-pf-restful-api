<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class InsuranceClaim {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = $this->getInsurance();
		$data['user_income'] = [];
		
		$userIncome = $this->repositories['user']->income()->where('income_id', 40)->get();
		if (!empty($userIncome)){
			for ($i = 0; $i < count($userIncome); $i++){
				$detail = json_decode($userIncome[$i]['detail'], true);

				$userIncome[$i]['currency'] = $detail['currency'];
				$userIncome[$i]['currency_value'] = $detail['currency_value'];
				$userIncome[$i]['kurs_value'] = $detail['kurs_value'];
				$userIncome[$i]['insurance_type'] = $detail['insurance_type'];
				$userIncome[$i]['company_name'] = $detail['company_name'];
			}
		}

		$data['user_income'] = $userIncome;
		return $data;
	}

	private function getInsurance(){
		$result = [];
		$userAsset = $this->repositories['user']->asset()->where('asset_id', 32)->get();
		if (!empty($userAsset)){
			foreach ($userAsset as $row){
				$detail = json_decode($row->detail, true);
				$result[] = [
					'id' => $row->id,
					'account_number' => $detail['account_number'],
					'company_name' => $detail['company_name'],
					'asset_id' => $row->asset_id
				];
			}
		}
		return $result;
	}

}
