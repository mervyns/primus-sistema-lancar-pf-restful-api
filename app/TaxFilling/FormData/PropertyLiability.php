<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class PropertyLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'bank' => new BankRepository,
			'country' => new CountryRepository
		];
	}

	public function get()
	{

		$data[] = [];
		$property = $this->repositories['user']->asset()->whereIn('asset_id', ['28', '29', '30'])->get();
		$liability = $this->repositories['user']->liability()->where('liability_id', 1)->get();
		$data['property']  = $this->parseProperty($property);
		$data['user_liability'] = $this->parseData($liability);
		$data['country'] = $this->repositories['country']->get();
		$data['bank'] = $this->repositories['bank']->get()->toArray();

		if (isset($data['user_liability'])){
			for ($i = 0; $i < count($data['user_liability']); $i++){
				$detail = json_decode($data['user_liability'][$i]['detail'], true);

				$data['user_liability'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
				$data['user_liability'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
				$data['user_liability'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
				$data['user_liability'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
				$data['user_liability'][$i]['property_id'] = $detail['property_id'];
				$data['user_liability'][$i]['credite_duration'] = $detail['credite_duration'];
				$data['user_liability'][$i]['paid_off_date'] = $detail['paid_off_date'];
				$data['user_liability'][$i]['yearly_interest'] = $detail['yearly_interest'];
				$data['user_liability'][$i]['bank_id'] = $detail['bank_id'];
				$data['user_liability'][$i]['other_bank'] = $detail['other_bank'];
			}
		}
		//dd($data);
		return $data;
		

		// $userLiability['property_liability'] = $this->repositories['user']->liability()->where('liability_id', '=', 5)->get();
		// $userLiability['property_asset'] = $this->repositories['user']->asset()
		// 		->where(function ($query) {
		//     		$query->where('asset_id', '=', 28)
		//     		->orWhere('asset_id', '=', 29);
		// 		})->get();

		// $userLiability['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// $bankRepository = new BankRepository();
		// $bank['bank'] = $bankRepository->get()->toArray();

		// if(!$userLiability['property_liability']->isEmpty()){
		// 	$i = 0; //start decode detail
		// 	foreach ($userLiability['property_liability'] as $liability) {
		// 		$detail = json_decode( $liability['detail'], true );

		// 		$userLiability['property_liability'][$i]['property_certificate_number'] = $detail['property_certificate_number'];
		// 		$userLiability['property_liability'][$i]['property_liability_country'] = $detail['property_liability_country'];
		// 		$userLiability['property_liability'][$i]['property_liability_currency'] = $detail['property_liability_currency'];
		// 		$userLiability['property_liability'][$i]['property_liability_foreign_value'] = $detail['property_liability_foreign_value'];
		// 		$userLiability['property_liability'][$i]['property_liability_bank_name'] = $detail['property_liability_bank_name'];
		// 		$userLiability['property_liability'][$i]['property_liability_other_bank_name'] = $detail['property_liability_other_bank_name'];
		// 		$userLiability['property_liability'][$i]['property_liability_country'] = $detail['property_liability_country'];
		// 		$i++;
		// 	}
		// 	$userLiability['property_liability'] = $userLiability['property_liability']->toArray();
		// } else{
		// 	$userLiability['property_liability'] = [];
		// }

		// if(!$userLiability['property_asset']->isEmpty()){
		// 	$i = 0; //start decode detail
		// 	foreach ($userLiability['property_asset'] as $asset) {
		// 		$detail = json_decode( $asset['detail'], true );

		// 		$userLiability['property_asset'][$i]['property_certificate_number'] = $detail['property_certificate_number'];
		// 		$i++;
		// 	}
		// 	$userLiability['property_asset'] = $userLiability['property_asset']->toArray();
		// } else{
		// 	$userLiability['property_asset'] = [];
		// }

		// if(!empty($userLiability['tax_profile'])){
		// 	$userLiability['tax_profile'] = $userLiability['tax_profile']->toArray();
		// } else{
		// 	$userLiability['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userLiability, $country, $bank);

		// //dd($mergedData);

		// return $mergedData ? $mergedData : [];
	}

	private function parseData($data){
		$result = [];
		if (isset($data)){
			foreach ($data as $key => $value){
				$detail = json_decode($value['detail'], true);
				if (isset($detail['credite_type']) && $detail['credite_type'] == 'property'){
					$result[] = $value;
				}
			}
		}
		return $result;
	}

	private function parseProperty($data){
		$result = [];

		if (isset($data)){
			foreach ($data as $key => $value){
				$detail = json_decode($value['detail'], true);
				
				if (isset($detail['credite_type']) && $detail['credite_type'] == 'Ya'){
					$result[] = [
						'id' => $value['id'],
						'asset_id' => $value['asset_id'],
						'certificate' => $detail['certificate_type'],
						'other_certificate_type' => $detail['other_certificate_type']
					];
				}
			}
		}

		return $result;
	}
}
