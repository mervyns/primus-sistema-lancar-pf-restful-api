<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class Cash {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		
		$userAsset['cash_asset'] = $this->repositories['user']->asset()->where('asset_id', '=', 1)->get();
		
		$countryRepository = new CountryRepository();
		$country['country'] = $countryRepository->get()->toArray();
		
		if(!$userAsset['cash_asset']->isEmpty()){
			$i = 0; //start decode detail
			foreach ($userAsset['cash_asset'] as $cashAsset) {
				$detail = json_decode( $cashAsset['detail'], true );
				$userAsset['cash_asset'][$i]['currency'] = isset($detail['currency']) == true ? $detail['currency'] : '';
				 $userAsset['cash_asset'][$i]['currency_value'] = isset($detail['currency_value']) == true ? $detail['currency_value'] : '';
				$userAsset['cash_asset'][$i]['kurs_value'] = isset($detail['kurs_value']) == true ? $detail['kurs_value'] : '';
				$i++;
			}
		} else {
			$userAsset['cash_asset'] = [];
		}
		

		$mergedData = array_merge($userAsset, $country);
		
		return $mergedData ? $mergedData : [];
	}
}
