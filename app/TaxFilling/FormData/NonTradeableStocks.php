<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class NonTradeableStocks {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		$userAsset = $this->repositories['user']->asset()->where('asset_id', 10)->get();
		for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
			$userAsset[$i]['serial_number'] = $detail['serial_number'];
			$userAsset[$i]['company_name'] = $detail['company_name'];
			$userAsset[$i]['description'] = $detail['description'];
        }

		$data['user_asset'] = $userAsset;
		return $data;

		// $userAsset['non_tradeable_stocks'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 10)
		// 	->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['non_tradeable_stocks']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAsset['non_tradeable_stocks'] as $tradeableStocks) {
		// 		$detail = json_decode( $tradeableStocks['detail'], true );

		// 		$userAsset['non_tradeable_stocks'][$i]['non_tradeable_stocks_country'] = $detail['non_tradeable_stocks_country'];
		// 		$userAsset['non_tradeable_stocks'][$i]['non_tradeable_stocks_currency'] = $detail['non_tradeable_stocks_currency'];
		// 		$userAsset['non_tradeable_stocks'][$i]['non_tradeable_stocks_foreign_value'] = $detail['non_tradeable_stocks_foreign_value'];
		// 		$userAsset['non_tradeable_stocks'][$i]['non_tradeable_stocks_companies'] = $detail['non_tradeable_stocks_companies'];
		// 		$userAsset['non_tradeable_stocks'][$i]['non_tradeable_stocks_detail'] = $detail['non_tradeable_stocks_detail'];

		// 		$i++;
		// 	}
		// 	$userAsset['non_tradeable_stocks'] = $userAsset['non_tradeable_stocks']->toArray();
		// } else{
		// 	$userAsset['non_tradeable_stocks'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		if(!empty($id)){
			$userAsset = $this->repositories['user']->asset()->where('asset_id', 10)->get();
			for ($i = 0; $i < count($userAsset); $i++){
				$detail = json_decode($userAsset[$i]['detail'], true);

				$userAsset[$i]['currency'] = $detail['currency'];
				$userAsset[$i]['country'] = $detail['country'];
				$userAsset[$i]['currency_value'] = $detail['currency_value'];
				$userAsset[$i]['kurs_value'] = $detail['kurs_value'];
				$userAsset[$i]['serial_number'] = $detail['serial_number'];
				$userAsset[$i]['company_name'] = $detail['company_name'];
				$userAsset[$i]['description'] = $detail['description'];
			}

			$data['user_asset'] = $userAsset;
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
