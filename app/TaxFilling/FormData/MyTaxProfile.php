<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MyTaxProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$taxProfile = $this->repositories['user']->taxProfile;
		return $taxProfile ? $taxProfile->toArray(): [];
		
		//npwp format
		// if (!empty($taxProfile->npwp)){
		// 	$npwp = $taxProfile->npwp;
		// 	if (strlen($npwp) == 15){
		// 		$str1 = substr($npwp, 0, 2);
		// 		$str2 = substr($npwp, 2, 3);
		// 		$str3 = substr($npwp, 5, 3);
		// 		$str4 = substr($npwp, 8, 1);
		// 		$str5 = substr($npwp, 9, 3);
		// 		$str6 = substr($npwp, 12);

		// 		$newNPWP = $str1.'.'.$str2.'.'.$str3.'.'.$str4.'-'.$str5.'.'.$str6;
		// 		$taxProfile->npwp = $newNPWP;
		// 	}
		// }

		
	}
}
