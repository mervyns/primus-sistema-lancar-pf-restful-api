<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class OverseasIncome {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$data['user_income'] = [];

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$userIncome =  $this->repositories['user']->income()->where('income_id', 52)->get()->toArray();
		
		for ($i = 0; $i < count($userIncome); $i++){
			$detail = json_decode($userIncome[$i]['detail'], true);

			$userIncome[$i]['currency'] = $detail['currency'];
			$userIncome[$i]['country'] = $detail['country'];
			$userIncome[$i]['kurs_value'] = $detail['kurs_value'];
			$userIncome[$i]['currency_value'] = $detail['currency_value'];
			$userIncome[$i]['income_type'] = $detail['income_type'];
			$userIncome[$i]['other_income_type'] = $detail['other_income_type'];

			$userWithHoldingTax = $this->repositories['user']->income()
				->where('id', '=', $userIncome[$i]['id'])
				->first()->withHoldingTax->toArray();
			$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax);
		}

		return $data;
	}
	
}
