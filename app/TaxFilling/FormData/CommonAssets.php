<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class CommonAssets {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$result = [];
		$reset = false;

		$taxProfile = $this->repositories['user']->taxProfile;
		if (!empty($taxProfile)){
			$result = $taxProfile->toArray();

			/** RESET INPUTAN */
			if (
				$result['have_deposit'] == 1 || 
				$result['have_credit_card'] == 1 ||
				$result['have_jewelry'] == 1 || 
				$result['have_vehicle'] == 1 || 
				$result['have_property']
			) {
				$reset = true;
			}
		}

		$result['reset'] = $reset;
		return $result;
	}
}