<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class OtherLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		$userLiability = $this->repositories['user']->liability()->where('liability_id', 4)->get();
		for ($i = 0; $i < count($userLiability); $i++){
            $detail = json_decode($userLiability[$i]['detail'], true);

            $userLiability[$i]['currency'] = $detail['currency'];
            $userLiability[$i]['country'] = $detail['country'];
            $userLiability[$i]['currency_value'] = $detail['currency_value'];
			$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
			$userLiability[$i]['city'] = $detail['city'];
        }

		$data['user_liability'] = $userLiability;
		return $data;
		// $userLiability['otherliability'] = $this->repositories['user']->liability()
		// 	->where('liability_id', '=', 4)
		// 	->get();

		// $userLiability['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userLiability['otherliability']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userLiability['otherliability'] as $otherliability) {
		// 		$detail = json_decode( $otherliability['detail'], true );

		// 		$userLiability['otherliability'][$i]['otherliability_country'] = $detail['otherliability_country'];
		// 		$userLiability['otherliability'][$i]['otherliability_currency'] = $detail['otherliability_currency'];
		// 		$userLiability['otherliability'][$i]['otherliability_foreign_value'] = $detail['otherliability_foreign_value'];
		// 		$userLiability['otherliability'][$i]['otherliability_lender_name'] = $detail['otherliability_lender_name'];
		// 		$userLiability['otherliability'][$i]['otherliability_lender_npwp'] = $detail['otherliability_lender_npwp'];
		// 		$userLiability['otherliability'][$i]['otherliability_lender_address'] = $detail['otherliability_lender_address'];

		// 		$i++;
		// 	}
		// 	$userLiability['otherliability'] = $userLiability['otherliability']->toArray();
		// } else{
		// 	$userLiability['otherliability'] = [];
		// }

		// if(!empty($userLiability['tax_profile'])){
		// 	$userLiability['tax_profile'] = $userLiability['tax_profile']->toArray();
		// } else{
		// 	$userLiability['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userLiability, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		if(!empty($id)){
			$userLiability = $this->repositories['user']->liability()->where('liability_id', 4)->get();
			for ($i = 0; $i < count($userLiability); $i++){
				$detail = json_decode($userLiability[$i]['detail'], true);

				$userLiability[$i]['currency'] = $detail['currency'];
				$userLiability[$i]['country'] = $detail['country'];
				$userLiability[$i]['currency_value'] = $detail['currency_value'];
				$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
				$userLiability[$i]['city'] = $detail['city'];
				$userLiability[$i]['active'] = $userLiability[$i]['id'] == $id ? true : false;
			}

		$data['user_liability'] = $userLiability;
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_liability' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
