<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class PreciousAssetsProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = '';

		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = array_key_exists('precious_assets_profile', $detail) == true ? $detail['precious_assets_profile'] : '';
		}

		return $data;

		// $userAdditionalProfile['additional_profile'] = $this->repositories['user']->additionalProfile;
		// if(!empty($userAdditionalProfile['additional_profile'])){
		// 	$i = 0; //start decode detail
		// 	foreach ($userAdditionalProfile['additional_profile'] as $uAP) {
		// 		$detail = json_decode( $uAP['detail'], true );

		// 		$userAdditionalProfile['additional_profile'][$i]['precious_assets_profile'] = $detail['precious_assets_profile'];

		// 		$i++;
		// 	}
		// 	$userAdditionalProfile['additional_profile'] = $userAdditionalProfile['additional_profile']->toArray();
		// } else{
		// 	$userAdditionalProfile['additional_profile'] = [];
		// }
		// dd($userAdditionalProfile);
		// return $userAdditionalProfile;
	}

	public function getFinalSteps($id)
	{
		$data = '';		

		
		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
	
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = array_key_exists('precious_assets_profile', $detail) == true ? $detail['precious_assets_profile'] : '';
		}
		
		
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
