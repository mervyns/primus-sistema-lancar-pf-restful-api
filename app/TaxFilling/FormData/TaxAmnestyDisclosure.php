<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserAssets\Services\UserAssetService;


class TaxAmnestyDisclosure {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$dropdown_list = [];
		$user_assets = $this->repositories['user']->asset()->where('is_ta',0)->get();
		
		foreach ($user_assets as $ua_key => $user_asset) {
			if(!($user_asset->asset->category->get()->isEmpty())){
				$dropdown_list[$user_asset->asset->category->id] = $user_asset->asset->category->toArray();
				$dropdown_list[$user_asset->asset->category->id]['user_id'] = $this->repositories['user']->id;
			}
		}
		
		return [
			'asset_category' => $dropdown_list
		];
	}

}
