<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Incomes\Repositories\IncomeRepository;

class DiscountRateBond {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'income' => new IncomeRepository,
		];
	}

	public function get()
	{
		$data = [];
		
		$data['user_income'] = $this->repositories['user']->income()->where('income_id', 42)->get();
		$data['income'] = $this->repositories['income']->getById(42);

        if (isset($data['user_income'])){
            for ($i = 0; $i < count($data['user_income']); $i++){
                $detail = json_decode($data['user_income'][$i]['detail'], true);

                $data['user_income'][$i]['tax_final'] = $detail['tax_final'];
                $data['user_income'][$i]['tax_rate'] = $detail['tax_rate'];
                $data['user_income'][$i]['description'] = $detail['description'];
            }
		}
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;

		$data['user_income'] = null;
		$data['income'] = $this->repositories['income']->getById(42);

		if(!empty($id)){
			$data['user_income'] = $this->repositories['user']->income()->where('income_id', 42)->get();
			$data['income'] = $this->repositories['income']->getById(42);
			if (isset($data['user_income'])){
				for ($i = 0; $i < count($data['user_income']); $i++){
					$detail = json_decode($data['user_income'][$i]['detail'], true);
					$data['user_income'][$i]['tax_final'] = $detail['tax_final'];
					$data['user_income'][$i]['tax_rate'] = $detail['tax_rate'];
					$data['user_income'][$i]['description'] = $detail['description'];
					$data['user_income'][$i]['active'] = $data['user_income'][$i]->id == $id ? true : false; 
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'income' => $data['income'],
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}

}