<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class CapitalInvestmentCompanies {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		$userAsset = $this->repositories['user']->asset()->where('asset_id', 16)->get();
		for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
			$userAsset[$i]['company_name'] = $detail['company_name'];
			$userAsset[$i]['description'] = $detail['description'];
        }

		$data['user_asset'] = $userAsset;
		return $data;
		// $userAsset['capital_investment_companies'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 16)
		// 	->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['capital_investment_companies']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAsset['capital_investment_companies'] as $cic) {
		// 		$detail = json_decode( $cic['detail'], true );

		// 		$userAsset['capital_investment_companies'][$i]['cic_country'] = $detail['cic_country'];
		// 		$userAsset['capital_investment_companies'][$i]['cic_currency'] = $detail['cic_currency'];
		// 		$userAsset['capital_investment_companies'][$i]['cic_foreign_value'] = $detail['cic_foreign_value'];
		// 		$userAsset['capital_investment_companies'][$i]['cic_name'] = $detail['cic_name'];
		// 		$userAsset['capital_investment_companies'][$i]['cic_detail'] = $detail['cic_detail'];

		// 		$i++;
		// 	}
		// 	$userAsset['capital_investment_companies'] = $userAsset['capital_investment_companies']->toArray();
		// } else{
		// 	$userAsset['capital_investment_companies'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		if(!empty($id)){
			$userAsset = $this->repositories['user']->asset()->where('asset_id', 16)->get();
			for ($i = 0; $i < count($userAsset); $i++){
				$detail = json_decode($userAsset[$i]['detail'], true);

				$userAsset[$i]['currency'] = $detail['currency'];
				$userAsset[$i]['country'] = $detail['country'];
				$userAsset[$i]['currency_value'] = $detail['currency_value'];
				$userAsset[$i]['kurs_value'] = $detail['kurs_value'];
				$userAsset[$i]['company_name'] = $detail['company_name'];
				$userAsset[$i]['description'] = $detail['description'];
				$userAsset[$i]['active'] = $userAsset[$i]['id'] == $id ? true : false;
			}

			$data['user_asset'] = $userAsset;
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
