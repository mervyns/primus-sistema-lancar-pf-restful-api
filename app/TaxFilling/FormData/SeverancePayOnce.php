<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Carbon\Carbon;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Services\UserAssetService;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

use Api\Incomes\Repositories\IncomeRepository;

class SeverancePayOnce {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_income' => new UserIncomeRepository,
			'income' => new IncomeRepository,
		];
	}

	public function get()
	{
		// $self_step_index = config('constants')['steps']['SEVERANCE_PAY_ONCE'];
		// $age = Carbon::now('Asia/Jakarta')->diffInYears(Carbon::parse($this->repositories['user']->birthdate));
        // $steps = new UserTaxFillingStepRepository();
        // $steps = $steps->setDirection($this->repositories['user']->id,$self_step_index);
		
		// return [
		// 	'age' => $age
		// ];

		$age = Carbon::now('Asia/Jakarta')->diffInYears(Carbon::parse($this->repositories['user']->birthdate));
		$income = $this->repositories['income']->getModel()->whereBetween('id', [16, 24])->get()->toArray();
		$user_income = $this->repositories['user']->income()->whereBetween('income_id', [16, 24])->get()->toArray();

		if (count($user_income) > 0) {
			for ($i = 0; $i < count($user_income); $i++) {
				$detail = json_decode($user_income[$i]['detail'], true);
				$detail_values = array();
				foreach ($detail as $key => $val) {
					$detail_values[$key] = $val;
				}

				$user_income[$i]['active'] = $i == 0 ? true : false;
				$user_income[$i]['detail'] = $detail_values;
			}
		}

		return array(
			'age' => $age,
			'income' => $income,
			'data' => $user_income
		);
	}

	public function getFinalSteps($id)
	{

		$existing = null;

		if(!empty($id)){
			$incomes = $this->repositories['user_income']->getModel()
						->where('user_id',$this->repositories['user']->id)
						->whereIn('income_id',[16,17,18,19,20,21,22,23,24])
						->get();
			
			if(!$incomes->isEmpty()){
				foreach ($incomes as $l_key => $income) {
					if(!empty($income->detail)){
						$data_deatils = [];
						$details = json_decode($income->detail,true);
	
						foreach ($details as $key => $detail) {
							$data_deatils[$key] = $detail;
						}
	
						$income->detail = $data_deatils;
					}
					$income->active = $income->id == $id ? true : false; 
					$existing[$l_key] = $income->toArray();
				}
			}
		}

		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}

		$age = Carbon::now('Asia/Jakarta')->diffInYears(Carbon::parse($this->repositories['user']->birthdate));
		
		if(\Session::get('is_final_step_submited')){
			return [
				'age' => $age,
				'is_final_steps_submited' => true
			];
		}

		return [
			'data' => $existing,
			'age' => $age
		];
	}

}
