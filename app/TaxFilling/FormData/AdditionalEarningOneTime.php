<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserAssets\Services\UserAssetService;


class AdditionalEarningOneTime {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = $detail;
		}
		
		return $data;
		
		// dd(1);
		// $dropdown_list = [];
		// $user_assets = $this->repositories['user']->asset()->where('is_ta',0)->get();
		
		// foreach ($user_assets as $ua_key => $user_asset) {
		// 	if(!($user_asset->asset->category->get()->isEmpty())){
		// 		$dropdown_list[$user_asset->asset->category->id] = $user_asset->asset->category->toArray();
		// 		$dropdown_list[$user_asset->asset->category->id]['user_id'] = $this->repositories['user']->id;
		// 	}
		// }
		
		//return [];
	}

}
