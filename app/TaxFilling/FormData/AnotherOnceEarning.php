<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

use Api\UserAssets\Models\UserAsset; 
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserAssets\Services\UserAssetService;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxRateTypes\Repositories\TaxRateTypeRepository;


class AnotherOnceEarning {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_income' => new UserIncomeRepository,
			'tax_rate' => new TaxRateTypeRepository
		];
	}

	public function get()
	{
        // $steps = new UserTaxFillingStepRepository();
        // $steps = $steps->setDirection($this->repositories['user']->id,76);
        
		// return [];

		$tax_rate = $this->repositories['tax_rate']->getModel()->whereIn('id', [3, 1, 2, 6])->get()->toArray();
		$income = $this->repositories['user_income']->getModel()
			->where('user_id', $this->repositories['user']->id)
			->where('income_id', 55)
			->get()
			->toArray();
		$data = [];
		if (isset($income) == true) {
			foreach ($income as $key => $val) {
				if (!empty($income['detail'])) {
					
				}
			}
		}

		return array(
			'tax_rate' => $tax_rate,
			'data' => []
		);
	}

	public function getFinalSteps($id)
	{

		$existing = null;

		if(!empty($id)){
			$incomes = $this->repositories['user_income']->getModel()
						->where('user_id',$this->repositories['user']->id)
						->whereIn('income_id',[55])
						->get();
			
			if(!$incomes->isEmpty()){
				foreach ($incomes as $l_key => $income) {
					if(!empty($income->detail)){
						$data_deatils = [];
						$details = json_decode($income->detail,true);
	
						foreach ($details as $key => $detail) {
							$data_deatils[$key] = $detail;
						}
	
						$income->detail = $data_deatils;
					}
					$income->active = $income->id == $id ? true : false; 
					$existing[$l_key] = $income->toArray();
				}
			}
		}

		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'is_final_steps_submited' => true
			];
		}

		return [
			'data' => $existing,
		];
	}

}
