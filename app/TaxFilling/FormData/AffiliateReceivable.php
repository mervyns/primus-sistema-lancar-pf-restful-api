<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class AffiliateReceivable {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		$userAsset = $this->repositories['user']->asset()->where('asset_id', 7)->get();
		for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

			$userAsset[$i]['active'] = $i == 0 ? true : false;
            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
			$userAsset[$i]['identification_number'] = $detail['identification_number'];
        }

		$data['user_asset'] = $userAsset;
		return $data;

		// $userAsset['affreceivable'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 7)
		// 	->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['affreceivable']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAsset['affreceivable'] as $affreceivable) {
		// 		$detail = json_decode( $affreceivable['detail'], true );

		// 		$userAsset['affreceivable'][$i]['affreceivable_country'] = $detail['affreceivable_country'];
		// 		$userAsset['affreceivable'][$i]['affreceivable_currency'] = $detail['affreceivable_currency'];
		// 		$userAsset['affreceivable'][$i]['affreceivable_foreign_value'] = $detail['affreceivable_foreign_value'];
		// 		$userAsset['affreceivable'][$i]['affreceivable_id_number'] = $detail['affreceivable_id_number'];

		// 		$i++;
		// 	}
		// 	$userAsset['affreceivable'] = $userAsset['affreceivable']->toArray();
		// } else{
		// 	$userAsset['affreceivable'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		if(!empty($id)){
			$userAsset = $this->repositories['user']->asset()->where('asset_id', 7)->get();
			for ($i = 0; $i < count($userAsset); $i++){
				$detail = json_decode($userAsset[$i]['detail'], true);

				$userAsset[$i]['currency'] = $detail['currency'];
				$userAsset[$i]['country'] = $detail['country'];
				$userAsset[$i]['currency_value'] = $detail['currency_value'];
				$userAsset[$i]['kurs_value'] = $detail['kurs_value'];
				$userAsset[$i]['identification_number'] = $detail['identification_number'];
				$userAsset[$i]['active'] = $userAsset[$i]['id'] == $id ? true : false;
			}

			$data['user_asset'] = $userAsset;
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
