<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MySpouseIncomeOption1 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];

		$user_income = $this->repositories['user']->income()->where('income_id', 37)->first();
		$user_income_with_holding = [];

		if (!empty($user_income)){
			$userWithHoldingTax = $this->repositories['user']->income()
				->where( 'income_id', '=', 37 )->where('id', '=', $user_income->id)
				->first()->withHoldingTax->toArray();
			if (!empty($userWithHoldingTax)){
				$user_income_with_holding = array_merge($user_income->toArray(), $userWithHoldingTax);
			}
		}

		$data['spouse'] = $this->repositories['user']->spouseTaxProfile()->first();
		$data['user_income'] = $user_income_with_holding;
		return $data;
	}
}