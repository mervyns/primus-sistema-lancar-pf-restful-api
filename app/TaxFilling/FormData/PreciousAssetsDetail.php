<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class PreciousAssetsDetail {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get();
		$data['user_asset'] = $this->repositories['user']->asset()->whereIn('asset_id', [22, 23, 24])->get();

		if (!is_null($data['user_asset'])){
			for ($i = 0; $i < count($data['user_asset']); $i++){
				$detail = json_decode($data['user_asset'][$i]['detail'], true);

				$data['user_asset'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
				$data['user_asset'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
				$data['user_asset'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
				$data['user_asset'][$i]['kurs_pajak'] = isset($detail['kurs_pajak']) ? $detail['kurs_pajak'] : '';
				$data['user_asset'][$i]['description'] = isset($detail['description']) ? $detail['description'] : '';

			}
		}
		return $data;

		// $userAsset['precious_assets'] = $this->repositories['user']->asset()
		// ->where(function ($query) {
    	// 	$query->where('asset_id', '=', 22)
    	// 	->orWhere('asset_id', '=', 23)
    	// 	->orWhere('asset_id', '=', 24);
		// })->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['precious_assets']->isEmpty()){
		// 	$i = 0; //start decode detail
		// 	foreach ($userAsset['precious_assets'] as $preciousAssets) {
		// 		$detail = json_decode( $preciousAssets['detail'], true );

		// 		$userAsset['precious_assets'][$i]['precious_assets_country'] = $detail['precious_assets_country'];
		// 		$userAsset['precious_assets'][$i]['precious_assets_currency'] = $detail['precious_assets_currency'];
		// 		$userAsset['precious_assets'][$i]['precious_assets_foreign_value'] = $detail['precious_assets_foreign_value'];
		// 		$userAsset['precious_assets'][$i]['precious_assets_description'] = $detail['precious_assets_description'];

		// 		$i++;
		// 	}
		// 	$userAsset['precious_assets'] = $userAsset['precious_assets']->toArray();
		// } else{
		// 	$userAsset['precious_assets'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get();

		// if(!empty($id)){
			$data['user_asset'] = $this->repositories['user']->asset()->whereIn('asset_id', [22, 23, 24])->get();
			if (!is_null($data['user_asset'])){
				for ($i = 0; $i < count($data['user_asset']); $i++){
					$detail = json_decode($data['user_asset'][$i]['detail'], true);
	
					$data['user_asset'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
					$data['user_asset'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
					$data['user_asset'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
					$data['user_asset'][$i]['kurs_pajak'] = isset($detail['kurs_pajak']) ? $detail['kurs_pajak'] : '';
					$data['user_asset'][$i]['description'] = isset($detail['description']) ? $detail['description'] : '';
	
				}
			}
		// }
		
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
