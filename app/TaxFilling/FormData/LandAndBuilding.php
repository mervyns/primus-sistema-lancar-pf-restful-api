<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Incomes\Repositories\IncomeRepository;

class LandAndBuilding {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'income' => new IncomeRepository,
		];
	}

	public function get()
	{
		$data = [];
		$data['properties'] = $this->repositories['user']->asset()
			->whereIn('asset_id', [28, 29, 30])->get();

		if (isset($data['properties'])){
			for ($i = 0; $i < count($data['properties']); $i++){
				$detail = json_decode($data['properties'][$i]['detail']);
				if(!empty($detail)){
					$data['properties'][$i]['country_id'] = $detail->currency;
					$data['properties'][$i]['property_type'] = $detail->property_type;
					$data['properties'][$i]['city'] = $detail->city;
				}
			}
		}
		$data['building'] = $this->repositories['user']->income()->where('income_id', 2)->get();
		$data['income'] = $this->repositories['income']->getById(2);


		if (isset($data['building'])){
			for ($i = 0; $i < count($data['building']); $i++){
				$detail = json_decode($data['building'][$i]['detail'], true);

				$data['building'][$i]['type'] = $detail['type'];
				$data['building'][$i]['tax_rate'] = $detail['tax_rate'];
				$data['building'][$i]['rent_price_received'] = $detail['rent_price_received'];
				$data['building'][$i]['asset_id'] = $detail['asset_id'];
				
			}
		}
	
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;
		$data['properties'] = [];
		$data['building'] = [];
		$data['income'] = [];

		if(!empty($id)){
			$data['properties'] = $this->repositories['user']->asset()
			->whereIn('asset_id', [28, 29, 30])->get();
			
			if (!$data['properties']->isEmpty()){
				for ($i = 0; $i < count($data['properties']); $i++){
					$detail = json_decode($data['properties'][$i]['detail']);
					
					if(!empty($detail)){
						$data['properties'][$i]['country_id'] = $detail->currency;
						$data['properties'][$i]['property_type'] = $detail->property_type;
						$data['properties'][$i]['city'] = $detail->city;
					}
					
				}
			}else{
				$data['properties'] = null;
			}
			$data['building'] = $this->repositories['user']->income()->where('income_id', 2)->get();
			$data['income'] = $this->repositories['income']->getById(2);


			if (isset($data['building'])){
				for ($i = 0; $i < count($data['building']); $i++){
					$detail = json_decode($data['building'][$i]['detail'], true);

					$data['building'][$i]['type'] = $detail['type'];
					$data['building'][$i]['tax_rate'] = $detail['tax_rate'];
					$data['building'][$i]['rent_price_received'] = $detail['rent_price_received'];
					$data['building'][$i]['asset_id'] = $detail['asset_id'];
					
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'income' => $data['income'],
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
	
		return $data;
	}
}