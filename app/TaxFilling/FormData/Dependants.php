<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\DependantRelations\Repositories\DependantRelationRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class Dependants {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$dependant['spouse'] = $this->repositories['user']->dependant()
				->where(function ($query) {
		    		$query->where('dependant_relation_id', '=', 1)
		    		->orWhere('dependant_relation_id', '=', 2);
				})->first();
		$dependant['dependant'] = $this->repositories['user']->dependant()
				->where([
						['dependant_relation_id', '<>', 1],
						['dependant_relation_id', '<>', 2]
				])->get();

		$dependantRelationRepository = new DependantRelationRepository();
		$dependantRelation['dependant_relation'] = $dependantRelationRepository->get()->toArray();

		if(!is_null($dependant['spouse'])){
			$dependant['spouse'] = $dependant['spouse']->toArray();
		} else {
			$dependant['spouse'] = [];
		}

		if(!is_null($dependant['dependant'])){
			$dependant['dependant'] = $dependant['dependant']->toArray();
		} else {
			$dependant['dependant'] = [];
		}

		$mergedData = array_merge($dependant, $dependantRelation);
		return $mergedData ? $mergedData : [];
	}
}
