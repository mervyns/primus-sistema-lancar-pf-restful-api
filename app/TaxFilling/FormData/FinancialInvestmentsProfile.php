<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class FinancialInvestmentsProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$list = [
			'exchange_stock',
			'mutual_funds',
			'insurance',
			'indonesian_bank_certificate',
			'goverment_bonds',
			'corporate_bonds',
			'non_tradeable_stock',
			'capital_investment_companies',
			'other_debentures',
			'other_investments'
		];
		$data = [];
		$reset = false;
		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			foreach ($detail as $key => $val) {
				if (!in_array($key, $list)) continue;
				if ($val == true) {
					$reset = true;
					break;
				}
			}
			$data = $detail;
		}

		$data['reset'] = $reset;
		return $data;
	}
}
