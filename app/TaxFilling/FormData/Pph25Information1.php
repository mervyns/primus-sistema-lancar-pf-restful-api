<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\UserDependants\Repositories\UserDependantRepository;
use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;
use Api\Users\Repositories\UserRepository;

use Infrastructure\Helper\TaxFillingHelper;

class Pph25Information1 {
    private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'help' => new TaxFillingHelper,
            'user_income' => new UserIncomeRepository,
            'user_holding' => new UserWithholdingTaxRepository,
            'user_profile' => new UserTaxProfileRepository,
            'user_dependant' => new UserDependantRepository,
            'user_spouse' => new SpouseTaxProfileRepository,
		];
	}

	public function get()
	{
        $userId = Auth::user()->id;
        $data = $this->PKPSummary($userId);

        return $data;
        
    }

    private function PKPSummary($userId) {
        $my_data = $this->repositories['help']->GetAllUserData($userId);
        $pph25netto = $this->repositories['help']->PPh25Netto($my_data);
        $ptkp = $this->repositories['help']->PTKP($my_data['tax_profile']['marital_status'], $my_data['tax_profile']['tax_responsibility_status_id'], count($my_data['dependant']));
        $pkp = (double)array_sum($pph25netto) - (double)$ptkp['value'];
        if ($pkp < 0) $pkp = 0;
        $progresif = (double)$this->repositories['help']->ProgressiveSummary($pkp)['value'];
        $overseas = $this->repositories['help']->PPh24Dikreditkan($my_data);
        $pph25holding = $this->repositories['help']->PPh25Holding($my_data);

        $userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();

        $status = false;
        $yearly = 0;
        if (!empty($userAdditionalProfile)) {
            $detail = json_decode($userAdditionalProfile['detail'], true);
            $yearly = isset($detail['pph25_yearly']) ? $detail['pph25_yearly'] : 0;
            $status = isset($detail['pph25_yearly']);
        }
        $monthly = floor(($progresif['value'] + $overseas - (double)(array_sum($pph25holding))) / 12);
        
        return array(
            'exists' => $status,
            'yearly_value' => $yearly,
            'monthly_value' => $monthly
        );
    }
}
