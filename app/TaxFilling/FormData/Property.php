<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class Property {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get();
		$data['user_asset'] = $this->repositories['user']->asset()->whereIn('asset_id', ['28', '29', '30'])->get();
		if (isset($data['user_asset'])){
			for ($i = 0; $i < count($data['user_asset']); $i++){
				$detail = json_decode($data['user_asset'][$i]['detail'], true);
				
				$data['user_asset'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
				$data['user_asset'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
				$data['user_asset'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
				$data['user_asset'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
				$data['user_asset'][$i]['credite_type'] = isset($detail['credite_type']) ? $detail['credite_type'] : '';
				$data['user_asset'][$i]['property_type'] = isset($detail['property_type']) ? $detail['property_type'] : '';
				$data['user_asset'][$i]['certificate_type'] = isset($detail['certificate_type']) ? $detail['certificate_type'] : '';
				$data['user_asset'][$i]['other_certificate_type'] = isset($detail['other_certificate_type']) ? $detail['other_certificate_type'] : '';
				$data['user_asset'][$i]['location'] = isset($detail['location']) ? $detail['location'] : '';
				$data['user_asset'][$i]['city'] = isset($detail['city']) ? $detail['city'] : '';
				$data['user_asset'][$i]['optional_property_type'] = isset($detail['optional_property_type']) ? $detail['optional_property_type'] : '';
				$data['user_asset'][$i]['property_large'] = isset($detail['property_large']) ? $detail['property_large'] : '';
			}
		}
		
		return $data;
		// $data = [];
		// $data['vehicles'] = $this->repositories['user']->asset()->whereIn('asset_id', ['18', '19', '20'])->get();
		// $data['country'] = $this->repositories['country']->get();
		
		// if (isset($data['vehicles'])){
		// 	for ($i = 0; $i < count($data['vehicles']); $i++){
		// 		$detail = json_decode($data['vehicles'][$i]['detail'], true);

		// 		$data['vehicles'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
		// 		$data['vehicles'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
		// 		$data['vehicles'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
		// 		$data['vehicles'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
		// 		$data['vehicles'][$i]['credite_type'] = isset($detail['credite_type']) ? $detail['credite_type'] : '';
		// 		$data['vehicles'][$i]['vehicle_merk'] = isset($detail['vehicle_merk']) ? $detail['vehicle_merk'] : '';
		// 		$data['vehicles'][$i]['plate_number'] = isset($detail['plate_number']) ? $detail['plate_number'] : '';
		// 		$data['vehicles'][$i]['bpkb_number'] = isset($detail['bpkb_number']) ? $detail['bpkb_number'] : '';
		// 		$data['vehicles'][$i]['expired_date'] = isset($detail['expired_date']) ? $detail['expired_date'] : '';
		// 	}
		// }
		// return $data;



		// $userAsset['user_asset'] = $this->repositories['user']->asset()
		// 		->where(function ($query) {
		//     		$query->where('asset_id', '=', 28)
		//     		->orWhere('asset_id', '=', 29);
		// 		})->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['user_asset']->isEmpty()){
		// 	$i = 0; //start decode detail
		// 	foreach ($userAsset['property'] as $vehicle) {
		// 		$detail = json_decode( $vehicle['detail'], true );

		// 		$userAsset['user_asset'][$i]['property_type'] = $detail['property_type'];
		// 		$userAsset['user_asset'][$i]['property_function'] = $detail['property_function'];
		// 		$userAsset['user_asset'][$i]['property_country'] = $detail['property_country'];
		// 		$userAsset['user_asset'][$i]['property_currency'] = $detail['property_currency'];
		// 		$userAsset['user_asset'][$i]['property_foreign_value'] = $detail['property_foreign_value'];
		// 		$userAsset['user_asset'][$i]['property_certificate_type'] = $detail['property_certificate_type'];
		// 		$userAsset['user_asset'][$i]['property_certificate_number'] = $detail['property_certificate_number'];
		// 		$userAsset['user_asset'][$i]['property_address'] = $detail['property_address'];
		// 		$userAsset['user_asset'][$i]['sppt_number'] = $detail['sppt_number'];
		// 		$userAsset['user_asset'][$i]['property_area'] = $detail['property_area'];

		// 		$i++;
		// 	}
		// 	$userAsset['user_asset'] = $userAsset['user_asset']->toArray();
		// } else{
		// 	$userAsset['user_asset'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get();
		$data['user_asset'] = $this->repositories['user']->asset()->whereIn('asset_id', ['28', '29', '30'])->get();

		if(!empty($id)){
			if (isset($data['user_asset'])){
				for ($i = 0; $i < count($data['user_asset']); $i++){
					$detail = json_decode($data['user_asset'][$i]['detail'], true);
					
					$data['user_asset'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
					$data['user_asset'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
					$data['user_asset'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
					$data['user_asset'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
					$data['user_asset'][$i]['credite_type'] = isset($detail['credite_type']) ? $detail['credite_type'] : '';
					$data['user_asset'][$i]['property_type'] = isset($detail['property_type']) ? $detail['property_type'] : '';
					$data['user_asset'][$i]['certificate_type'] = isset($detail['certificate_type']) ? $detail['certificate_type'] : '';
					$data['user_asset'][$i]['other_certificate_type'] = isset($detail['other_certificate_type']) ? $detail['other_certificate_type'] : '';
					$data['user_asset'][$i]['location'] = isset($detail['location']) ? $detail['location'] : '';
					$data['user_asset'][$i]['city'] = isset($detail['city']) ? $detail['city'] : '';
					$data['user_asset'][$i]['optional_property_type'] = isset($detail['optional_property_type']) ? $detail['optional_property_type'] : '';
					$data['user_asset'][$i]['property_large'] = isset($detail['property_large']) ? $detail['property_large'] : '';
					$data['user_asset'][$i]['active'] = $data['user_asset'][$i]['id'] == $id ? true : false;
				}
			}
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
