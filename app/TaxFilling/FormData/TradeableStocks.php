<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class TradeableStocks {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$userAsset['tradeable_stocks'] = $this->repositories['user']->asset()
			->where('asset_id', '=', 9)
			->get();

		$userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		$countryRepository = new CountryRepository();
		$country['country'] = $countryRepository->get()->toArray();

		if(!$userAsset['tradeable_stocks']->isEmpty()){
			$i = 0; //start decode detail

			foreach ($userAsset['tradeable_stocks'] as $tradeableStocks) {
				$detail = json_decode( $tradeableStocks['detail'], true );

				$userAsset['tradeable_stocks'][$i]['tradeable_stock_country'] = $detail['tradeable_stock_country'];
				$userAsset['tradeable_stocks'][$i]['tradeable_stock_currency'] = $detail['tradeable_stock_currency'];
				$userAsset['tradeable_stocks'][$i]['tradeable_stock_foreign_value'] = $detail['tradeable_stock_foreign_value'];
				$userAsset['tradeable_stocks'][$i]['securities_companies'] = $detail['securities_companies'];
				$userAsset['tradeable_stocks'][$i]['trader_account_number'] = $detail['trader_account_number'];

				$i++;
			}
			$userAsset['tradeable_stocks'] = $userAsset['tradeable_stocks']->toArray();
		} else{
			$userAsset['tradeable_stocks'] = [];
		}

		if(!empty($userAsset['tax_profile'])){
			$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		} else{
			$userAsset['tax_profile'] = [];
		}

		$mergedData = array_merge($userAsset, $country);

		return $mergedData ? $mergedData : [];
	}
}
