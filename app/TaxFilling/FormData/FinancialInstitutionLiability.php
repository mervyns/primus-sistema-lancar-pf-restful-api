<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class FinancialInstitutionLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		$userLiability = $this->repositories['user']->liability()->where('liability_id', 1)->get();
		for ($i = 0; $i < count($userLiability); $i++){
            $detail = json_decode($userLiability[$i]['detail'], true);

            $userLiability[$i]['currency'] = $detail['currency'];
            $userLiability[$i]['country'] = $detail['country'];
            $userLiability[$i]['currency_value'] = $detail['currency_value'];
			$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
			$userLiability[$i]['city'] = isset($detail['city']) && $detail['city'];
        }

		$data['user_liability'] = $userLiability;
		// dd($data);
		return $data;
		// $userLiability['fil'] = $this->repositories['user']->liability()
		// 	->where('liability_id', '=', 1)
		// 	->get();

		// $userLiability['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userLiability['fil']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userLiability['fil'] as $fil) {
		// 		$detail = json_decode( $fil['detail'], true );

		// 		$userLiability['fil'][$i]['fil_country'] = $detail['fil_country'];
		// 		$userLiability['fil'][$i]['fil_currency'] = $detail['fil_currency'];
		// 		$userLiability['fil'][$i]['fil_foreign_value'] = $detail['fil_foreign_value'];
		// 		$userLiability['fil'][$i]['fil_companies'] = $detail['fil_companies'];
		// 		$userLiability['fil'][$i]['fil_companies_address'] = $detail['fil_companies_address'];
		// 		$userLiability['fil'][$i]['fil_companies_city'] = $detail['fil_companies_city'];

		// 		$i++;
		// 	}
		// 	$userLiability['fil'] = $userLiability['fil']->toArray();
		// } else{
		// 	$userLiability['fil'] = [];
		// }

		// if(!empty($userLiability['tax_profile'])){
		// 	$userLiability['tax_profile'] = $userLiability['tax_profile']->toArray();
		// } else{
		// 	$userLiability['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userLiability, $country);

		// return $mergedData ? $mergedData : [];
	}
	
	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		if(!empty($id)){
			$userLiability = $this->repositories['user']->liability()->where('liability_id', 1)->get();
			for ($i = 0; $i < count($userLiability); $i++){
				$detail = json_decode($userLiability[$i]['detail'], true);
				$userLiability[$i]['currency'] = $detail['currency'];
				$userLiability[$i]['country'] = $detail['country'];
				$userLiability[$i]['currency_value'] = $detail['currency_value'];
				$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
				$userLiability[$i]['city'] = $detail['city'];
				$userLiability[$i]['active'] = $userLiability[$i]['id'] == $id ? true : false;
			}

			$data['user_liability'] = $userLiability;
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_liability' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
