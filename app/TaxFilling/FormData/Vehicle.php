<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class Vehicle {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['vehicles'] = $this->repositories['user']->asset()->whereIn('asset_id', ['19', '20'])->get();
		$data['country'] = $this->repositories['country']->get();
		
		if (isset($data['vehicles'])){
			for ($i = 0; $i < count($data['vehicles']); $i++){
				$detail = json_decode($data['vehicles'][$i]['detail'], true);

				$data['vehicles'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
				$data['vehicles'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
				$data['vehicles'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
				$data['vehicles'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
				$data['vehicles'][$i]['credite_type'] = isset($detail['credite_type']) ? $detail['credite_type'] : '';
				$data['vehicles'][$i]['vehicle_merk'] = isset($detail['vehicle_merk']) ? $detail['vehicle_merk'] : '';
				$data['vehicles'][$i]['plate_number'] = isset($detail['plate_number']) ? $detail['plate_number'] : '';
				$data['vehicles'][$i]['bpkb_number'] = isset($detail['bpkb_number']) ? $detail['bpkb_number'] : '';
				$data['vehicles'][$i]['expired_date'] = isset($detail['expired_date']) ? $detail['expired_date'] : '';
			}
		}
		return $data;
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['vehicles'] = $this->repositories['user']->asset()->whereIn('asset_id', ['19', '20'])->get();
		$data['country'] = $this->repositories['country']->get();

		if(!empty($id)){
			if (isset($data['vehicles'])){
				for ($i = 0; $i < count($data['vehicles']); $i++){
					$detail = json_decode($data['vehicles'][$i]['detail'], true);
	
					$data['vehicles'][$i]['currency'] = isset($detail['currency']) ? $detail['currency'] : '';
					$data['vehicles'][$i]['country'] = isset($detail['country']) ? $detail['country'] : '';
					$data['vehicles'][$i]['currency_value'] = isset($detail['currency_value']) ? $detail['currency_value'] : '';
					$data['vehicles'][$i]['kurs_value'] = isset($detail['kurs_value']) ? $detail['kurs_value'] : '';
					$data['vehicles'][$i]['credite_type'] = isset($detail['credite_type']) ? $detail['credite_type'] : '';
					$data['vehicles'][$i]['vehicle_merk'] = isset($detail['vehicle_merk']) ? $detail['vehicle_merk'] : '';
					$data['vehicles'][$i]['plate_number'] = isset($detail['plate_number']) ? $detail['plate_number'] : '';
					$data['vehicles'][$i]['bpkb_number'] = isset($detail['bpkb_number']) ? $detail['bpkb_number'] : '';
					$data['vehicles'][$i]['expired_date'] = isset($detail['expired_date']) ? $detail['expired_date'] : '';
					$data['vehicles'][$i]['active'] = $data['vehicles'][$i]['id'] == $id  ? true : false;
				}
			}
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'vehicles' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
