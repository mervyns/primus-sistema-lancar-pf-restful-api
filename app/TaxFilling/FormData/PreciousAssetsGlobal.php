<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class PreciousAssetsGlobal {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get();
		$data['user_asset'] = [];

		$Logam = $this->repositories['user']->asset()->where('asset_id', 22)->first();
		$batu = $this->repositories['user']->asset()->where('asset_id', 23)->first();
		$antik = $this->repositories['user']->asset()->where('asset_id', 24)->first();

		if (!is_null($Logam)){
			$detailLogal = json_decode($Logam['detail'], true);
			
			$Logam['currency'] = isset($detailLogal['currency']) ? $detailLogal['currency'] : '';
			$Logam['currency_value'] = isset($detailLogal['currency_value']) ? $detailLogal['currency_value'] : '1';
			$Logam['kurs_pajak'] = isset($detailLogal['kurs_pajak']) ? $detailLogal['kurs_pajak'] : '0';
		}

		if (!is_null($batu)){
			$detailBatu = json_decode($batu['detail'], true);

			$batu['currency'] = isset($detailBatu['currency']) ? $detailBatu['currency'] : '';
			$batu['currency_value'] = isset($detailBatu['currency_value']) ? $detailBatu['currency_value'] : '1';
			$batu['kurs_pajak'] = isset($detailBatu['kurs_pajak']) ? $detailBatu['kurs_pajak'] : '0';
		}

		if (!is_null($antik)){
			$detailAntik = json_decode($antik['detail'], true);

			$antik['currency'] = isset($detailAntik['currency']) ? $detailAntik['currency'] : '';
			$antik['currency_value'] = isset($detailAntik['currency_value']) ? $detailAntik['currency_value'] : '1';
			$antik['kurs_pajak'] = isset($detailAntik['kurs_pajak']) ? $detailAntik['kurs_pajak'] : '0';
		}
		
		$data['user_asset'] = [$Logam, $batu, $antik];
		
		return $data;

		// $userAsset['gold_assets'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 22)->first();

		// $userAsset['jewel_assets'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 23)->first();

		// $userAsset['antique_assets'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 24)->first();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!empty($userAsset['gold_assets'])){
		// 	//start decode detail
		// 	$userAsset['gold_assets'] = $userAsset['gold_assets']->toArray();
		// 	$detail = json_decode( $userAsset['gold_assets']['detail'], true );

		// 	if(isset($detail['gold_country']))
		// 		$userAsset['gold_assets']['gold_country'] = $detail['gold_country'];

		// } else{
		// 	$userAsset['gold_assets'] = [];
		// }

		// if(!empty($userAsset['jewel_assets'])){
		// 	$userAsset['jewel_assets'] = $userAsset['jewel_assets']->toArray();
		// 	$detail = json_decode( $userAsset['jewel_assets']['detail'], true );

		// 	if(isset($detail['jewel_country']))
		// 		$userAsset['jewel_assets']['jewel_country'] = $detail['jewel_country'];

		// } else{
		// 	$userAsset['jewel_assets'] = [];
		// }

		// if(!empty($userAsset['antique_assets'])){
		// 	$userAsset['antique_assets'] = $userAsset['antique_assets']->toArray();
		// 	$detail = json_decode( $userAsset['antique_assets']['detail'], true );

		// 	if(isset($detail['antique_country']))
		// 		$userAsset['antique_assets']['antique_country'] = $detail['antique_country'];

		// } else{
		// 	$userAsset['antique_assets'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// //dd($mergedData);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{

		$additional_profile = $this->repositories['user']->additionalProfile()->first();

		if(!empty($additional_profile)){
			$details = (json_decode($additional_profile->detail,true));
			if(!empty($details['precious_assets_profile']) && $details['precious_assets_profile'] != 'Pelaporan Global'){
				$this->repositories['user']->asset()->whereIn('asset_id', [22, 23, 24])->delete();
			}
		}

		$data = [];
		$data['country'] = $this->repositories['country']->get();
		$data['user_asset'] = [];

		// if(!empty($id)){
			$Logam = $this->repositories['user']->asset()->where('asset_id', 22)->first();
			$batu = $this->repositories['user']->asset()->where('asset_id', 23)->first();
			$antik = $this->repositories['user']->asset()->where('asset_id', 24)->first();

			if (!is_null($Logam)){
				$detailLogal = json_decode($Logam['detail'], true);
				
				$Logam['currency'] = isset($detailLogal['currency']) ? $detailLogal['currency'] : '';
				$Logam['currency_value'] = isset($detailLogal['currency_value']) ? $detailLogal['currency_value'] : '1';
				$Logam['kurs_pajak'] = isset($detailLogal['kurs_pajak']) ? $detailLogal['kurs_pajak'] : '0';
			}

			if (!is_null($batu)){
				$detailBatu = json_decode($batu['detail'], true);

				$batu['currency'] = isset($detailBatu['currency']) ? $detailBatu['currency'] : '';
				$batu['currency_value'] = isset($detailBatu['currency_value']) ? $detailBatu['currency_value'] : '1';
				$batu['kurs_pajak'] = isset($detailBatu['kurs_pajak']) ? $detailBatu['kurs_pajak'] : '0';
			}

			if (!is_null($antik)){
				$detailAntik = json_decode($antik['detail'], true);

				$antik['currency'] = isset($detailAntik['currency']) ? $detailAntik['currency'] : '';
				$antik['currency_value'] = isset($detailAntik['currency_value']) ? $detailAntik['currency_value'] : '1';
				$antik['kurs_pajak'] = isset($detailAntik['kurs_pajak']) ? $detailAntik['kurs_pajak'] : '0';
			}
			
			$data['user_asset'] = [$Logam, $batu, $antik];
		// }
		
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'bank_list' => [],
				'credit_card' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
