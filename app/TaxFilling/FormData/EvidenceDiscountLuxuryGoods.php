<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Incomes\Repositories\IncomeRepository;


class EvidenceDiscountLuxuryGoods {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'income' => new IncomeRepository,
		];
	}

	public function get()
	{
		$data = [];
		$data['income'] = $this->repositories['income']->getModel()->whereBetween('id', [46, 51])->get();
		$data['user_income'] = [];
		$userIncome = $this->repositories['user']->income()->whereBetween('income_id', [46, 51])->get()->toArray();
		
		if (isset($userIncome) && count($userIncome) > 0){
			for ($i = 0; $i < count($userIncome); $i++){
				$detail = json_decode($userIncome[$i]['detail'], true);
				$userIncome[$i]['tax_rate'] = $detail['tax_rate'];

				$userWithHoldingTax = $this->repositories['user']->income()
					->where('id', '=', $userIncome[$i]['id'])
					->first()->withHoldingTax->toArray();
				$data['user_income'][$i] = array_merge($userIncome[$i], $userWithHoldingTax);
			}
		}

		//dd($data);
        return $data;
	}
}
