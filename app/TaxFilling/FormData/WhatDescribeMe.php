<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class WhatDescribeMe {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user()
		];
	}

	public function get()
	{
		$taxProfile = $this->repositories['user']->taxProfile;
		return $taxProfile ? $taxProfile->toArray(): [];
	}
}