<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MyWifeMainIncome {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$userIncome = $this->repositories['user']->income()->where('income_id', '<>', 1)->first();

		try{
			$userWithHoldingTax = $userIncome->withHoldingTax;
		} catch (\Exception $error) {
	    	$userWithHoldingTax = false;
	    }

	    if(!is_null($userIncome))
			$data1 = $userIncome->toArray();
		else
			$data1 = [];

		if($userWithHoldingTax != false){
			$data2 = $userWithHoldingTax->toArray();
			$data3 = $this->repositories['user']->spouseTaxProfile()->first()->toArray();
			if(!is_null($userIncome)){
				$merged_array = array_merge($data1, $data2, $data3);
			}
			else{
				$merged_array = array_merge($data2, $data3);
			}
		} else{
			$data3 = $this->repositories['user']->spouseTaxProfile()->first()->toArray();
			if(!is_null($userIncome)){
				$merged_array = array_merge($data1, $data3);
			}
			else{
				$merged_array = $data3;
			}
		}

		return $merged_array;
	}
}
