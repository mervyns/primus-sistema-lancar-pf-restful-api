<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Charities\Repositories\CharityRepository;

class Charity {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'charity' => new CharityRepository,
		];
	}

	public function get()
	{
		$data = [];
		
		$userIncome = $this->repositories['user']->income()->where('income_id', '=', 53)->get()->toArray();

		$data['user_charities'] = $userIncome;
		$data['charities'] = $this->repositories['charity']->get()->toArray();

		for ($i = 0; $i < count($data['user_charities']); $i++){
			$detail = json_decode($data['user_charities'][$i]['detail']);

			$data['user_charities'][$i]['charity_id'] = $detail->charity_id;
			$data['user_charities'][$i]['other_charity'] = $detail->other_charity;
		}

        return $data;
	}
}
