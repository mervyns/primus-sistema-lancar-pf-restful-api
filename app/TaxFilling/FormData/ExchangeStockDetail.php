<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class ExchangeStockDetail {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
        $data['user_asset'] = [];
        
        $userAsset = $this->repositories['user']->asset()->where('asset_id', 9)->get();

        for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
            $userAsset[$i]['account_number'] = $detail['account_number'];
            $userAsset[$i]['company_name'] = $detail['company_name'];
            $userAsset[$i]['stock_name'] = isset($detail['stock_name']) ? $detail['stock_name'] : '';
        }

        $data['user_asset'] = $userAsset;
		return $data;

	}
}
