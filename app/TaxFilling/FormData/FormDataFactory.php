<?php
namespace WebApp\TaxFilling\FormData;

use Exception;

class FormDataFactory {
	public static function build ( $source ) {
		$name = __NAMESPACE__ . '\\' . $source;

		if ( class_exists( $name ) ) {
			return new $name();
		} else {
			throw new Exception("Invalid form data type given. Form data class: {$name}");
		}
	}
}