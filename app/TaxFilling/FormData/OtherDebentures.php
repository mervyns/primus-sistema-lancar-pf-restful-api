<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class OtherDebentures {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		$userAsset = $this->repositories['user']->asset()->where('asset_id', 13)->get();
		for ($i = 0; $i < count($userAsset); $i++){
            $detail = json_decode($userAsset[$i]['detail'], true);

            $userAsset[$i]['currency'] = $detail['currency'];
            $userAsset[$i]['country'] = $detail['country'];
            $userAsset[$i]['currency_value'] = $detail['currency_value'];
            $userAsset[$i]['kurs_value'] = $detail['kurs_value'];
			$userAsset[$i]['borrower'] = $detail['borrower'];
        }

		$data['user_asset'] = $userAsset;
		return $data;
		// $userAsset['other_debentures'] = $this->repositories['user']->asset()
		// 	->where('asset_id', '=', 13)
		// 	->get();

		// $userAsset['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userAsset['other_debentures']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userAsset['other_debentures'] as $otherDebentures) {
		// 		$detail = json_decode( $otherDebentures['detail'], true );

		// 		$userAsset['other_debentures'][$i]['other_debentures_country'] = $detail['other_debentures_country'];
		// 		$userAsset['other_debentures'][$i]['other_debentures_currency'] = $detail['other_debentures_currency'];
		// 		$userAsset['other_debentures'][$i]['other_debentures_foreign_value'] = $detail['other_debentures_foreign_value'];
		// 		$userAsset['other_debentures'][$i]['other_debentures_nik'] = $detail['other_debentures_nik'];
		// 		$userAsset['other_debentures'][$i]['other_debentures_name'] = $detail['other_debentures_name'];

		// 		$i++;
		// 	}
		// 	$userAsset['other_debentures'] = $userAsset['other_debentures']->toArray();
		// } else{
		// 	$userAsset['other_debentures'] = [];
		// }

		// if(!empty($userAsset['tax_profile'])){
		// 	$userAsset['tax_profile'] = $userAsset['tax_profile']->toArray();
		// } else{
		// 	$userAsset['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userAsset, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_asset'] = [];

		if(!empty($id)){
			$userAsset = $this->repositories['user']->asset()->where('asset_id', 13)->get();
			for ($i = 0; $i < count($userAsset); $i++){
				$detail = json_decode($userAsset[$i]['detail'], true);

				$userAsset[$i]['currency'] = $detail['currency'];
				$userAsset[$i]['country'] = $detail['country'];
				$userAsset[$i]['currency_value'] = $detail['currency_value'];
				$userAsset[$i]['kurs_value'] = $detail['kurs_value'];
				$userAsset[$i]['borrower'] = $detail['borrower'];
				$userAsset[$i]['active'] = $userAsset[$i]['id'] == $id ? true : false;
			}

			$data['user_asset'] = $userAsset;
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
