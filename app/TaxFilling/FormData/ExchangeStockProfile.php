<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class ExchangeStockProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = '';

		$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		
		if (!empty($userAdditionalProfile)){
			$detail = json_decode($userAdditionalProfile['detail'], true);
			$data = array_key_exists('exchange_stock_profile', $detail) == true ? $detail['exchange_stock_profile'] : '';
		}
		
		return $data;
	}

	public function getFinalSteps($id)
	{
		$data = '';

		if(!empty($id)){
			$userAdditionalProfile = $this->repositories['user']->additionalProfile()->first();
		
			if (!empty($userAdditionalProfile)){
				$detail = json_decode($userAdditionalProfile['detail'], true);
				$data = array_key_exists('exchange_stock_profile', $detail) == true ? $detail['exchange_stock_profile'] : '';
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_asset' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
