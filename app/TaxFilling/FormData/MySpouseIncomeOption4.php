<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\UserProducts\Repositories\UserProductRepository;

class MySpouseIncomeOption4 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_product' => new UserProductRepository,
			''
		];
	}

	public function get()
	{
		$status = 1;
		$product_subscription = $this->repositories['user_product']->checkProductSubscription(Auth::user()->id);
		$companion = Auth::user()->companion;
		$spouse_income = $this->repositories['user']->income()->where('income_id', 36)->first();

		$user_income = [];
	
		if (empty($companion)){
			$product_subscription = $this->repositories['user_product']->checkProductSubscription(Auth::user()->id);
			if (!empty($product_subscription)){
				foreach ($product_subscription['partner'] as $row){
					if ($row->wife_or_husband == 1){
						if (!empty($spouse_income)){
							$spouse_income_withholding = $this->repositories['user']->income()
								->where( 'income_id', '=', 36 )->where('id', '=', $spouse_income->id)
								->first()->withHoldingTax->toArray();
							if (!empty($spouse_income_withholding)){
								$user_income = array_merge($spouse_income->toArray(), $spouse_income_withholding);
								
								$detail = !empty($user_income) ? json_decode($user_income['detail'], true) : [];
								$user_income['domestic_income_from_business'] = isset($detail['domestic_income_from_business']) ? $detail['domestic_income_from_business'] : '';
								$user_income['domestic_income_from_jobs'] = isset($detail['domestic_income_from_jobs']) ? $detail['domestic_income_from_jobs'] : '';
								$user_income['overseas_income'] = isset($detail['overseas_income']) ? $detail['overseas_income'] : '';
								$user_income['charity'] = isset($detail['charity']) ? $detail['charity'] : '';
								$user_income['compensation'] = isset($detail['compensation']) ? $detail['compensation'] : '';
							}
						}
						$status = 2;
					}
				}
			}
		}else{
			if (!empty($spouse_income)){
				$spouse_income_withholding = $this->repositories['user']->income()
					->where( 'income_id', '=', 36 )->where('id', '=', $spouse_income->id)
					->first()->withHoldingTax->toArray();
				if (!empty($spouse_income_withholding)){
					$user_income = array_merge($spouse_income->toArray(), $spouse_income_withholding);
					
					$detail = !empty($user_income) ? json_decode($user_income['detail'], true) : [];
					$user_income['domestic_income_from_business'] = isset($detail['domestic_income_from_business']) ? $detail['domestic_income_from_business'] : '';
					$user_income['domestic_income_from_jobs'] = isset($detail['domestic_income_from_jobs']) ? $detail['domestic_income_from_jobs'] : '';
					$user_income['overseas_income'] = isset($detail['overseas_income']) ? $detail['overseas_income'] : '';
					$user_income['charity'] = isset($detail['charity']) ? $detail['charity'] : '';
					$user_income['compensation'] = isset($detail['compensation']) ? $detail['compensation'] : '';
				}
				$status = 3;
			}else{
				$status = 4;
			}
		}

		$data['status'] = $status;
		$data['user_income'] = $user_income;
		return $data;
	}
}