<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MySpouseIncomeOption3 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		try{
			$data = [];
			$userIncome = $this->repositories['user']->income()->where('income_id', '=', 36)->get()->toArray();
			foreach ($userIncome as $key => $val){
				$id = $val['id'];
				$userWithHoldingTax = $this->repositories['user']->income()
					->where( 'income_id', '=', 36 )->where('id', '=', $id)
					->first()->withHoldingTax->toArray();
				
				if (!empty($userWithHoldingTax)){
					$data[$key] = array_merge($val, $userWithHoldingTax);
				}
			}
			return $data;
		}catch(\Exception $e)
		{
			return [];
		}
	}
}