<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;

class AffiliateLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'country' => new CountryRepository
		];
	}

	public function get()
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		$userLiability = $this->repositories['user']->liability()->where('liability_id', 3)->get();
		for ($i = 0; $i < count($userLiability); $i++){
            $detail = json_decode($userLiability[$i]['detail'], true);

            $userLiability[$i]['currency'] = $detail['currency'];
            $userLiability[$i]['country'] = $detail['country'];
            $userLiability[$i]['currency_value'] = $detail['currency_value'];
			$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
			$userLiability[$i]['lender_npwp'] = $detail['lender_npwp'];
        }

		$data['user_liability'] = $userLiability;
		return $data;
		// $userLiability['affliability'] = $this->repositories['user']->liability()
		// 	->where('liability_id', '=', 3)
		// 	->get();

		// $userLiability['tax_profile'] = $this->repositories['user']->taxProfile;

		// $countryRepository = new CountryRepository();
		// $country['country'] = $countryRepository->get()->toArray();

		// if(!$userLiability['affliability']->isEmpty()){
		// 	$i = 0; //start decode detail

		// 	foreach ($userLiability['affliability'] as $affliability) {
		// 		$detail = json_decode( $affliability['detail'], true );

		// 		$userLiability['affliability'][$i]['affliability_country'] = $detail['affliability_country'];
		// 		$userLiability['affliability'][$i]['affliability_currency'] = $detail['affliability_currency'];
		// 		$userLiability['affliability'][$i]['affliability_foreign_value'] = $detail['affliability_foreign_value'];
		// 		$userLiability['affliability'][$i]['affliability_companies'] = $detail['affliability_companies'];
		// 		$userLiability['affliability'][$i]['affliability_companies_npwp'] = $detail['affliability_companies_npwp'];
		// 		$userLiability['affliability'][$i]['affliability_companies_address'] = $detail['affliability_companies_address'];

		// 		$i++;
		// 	}
		// 	$userLiability['affliability'] = $userLiability['affliability']->toArray();
		// } else{
		// 	$userLiability['affliability'] = [];
		// }

		// if(!empty($userLiability['tax_profile'])){
		// 	$userLiability['tax_profile'] = $userLiability['tax_profile']->toArray();
		// } else{
		// 	$userLiability['tax_profile'] = [];
		// }

		// $mergedData = array_merge($userLiability, $country);

		// return $mergedData ? $mergedData : [];
	}

	public function getFinalSteps($id)
	{
		$data = [];
		$data['country'] = $this->repositories['country']->get()->toArray();
		$data['user_liability'] = [];

		if(!empty($id)){
			$userLiability = $this->repositories['user']->liability()->where('liability_id', 3)->get();
			for ($i = 0; $i < count($userLiability); $i++){
				$detail = json_decode($userLiability[$i]['detail'], true);

				$userLiability[$i]['currency'] = $detail['currency'];
				$userLiability[$i]['country'] = $detail['country'];
				$userLiability[$i]['currency_value'] = $detail['currency_value'];
				$userLiability[$i]['kurs_value'] = $detail['kurs_value'];
				$userLiability[$i]['lender_npwp'] = $detail['lender_npwp'];
				$userLiability[$i]['active'] = $userLiability[$i]['id'] == $id ? true : false;
			}

			$data['user_liability'] = $userLiability;
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'country' => [],
				'user_liability' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}
}
