<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Illuminate\Http\RedirectResponse;
class FillRequiredData1 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user()
        ];
        
        
    }

	public function get()
	{
        
       $userProfile = $this->repositories['user']->taxProfile()->first();
       $userSpouse = $this->repositories['user']->spouseTaxProfile()->first();

       $data['marital'] = false;
       $data['me'] = [
           'npwp'       => $userProfile->npwp,
           'efin'       => $userProfile->efin,
           'nik'        => $userProfile->nik
       ];

       if (!empty($userSpouse) && $userProfile->marital_status == 2 && ($userProfile->tax_responsibility_status_id == 3 || $userProfile->tax_responsibility_status_id == 4)){
           $data['spouse'] = [
               'npwp'   => $userSpouse->npwp,
               'nik'    => $userSpouse->nik
           ];
           $data['marital'] = true;
       }
       
    //    if (!empty($data['me']['npwp']) && !empty($data['me']['efin']) && !empty($data['me']['nik'])) {
    //        return redirect('/home');
    //    }else{
    //        return $data;
    //    }
        
       return $data;
    }

}
