<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Carbon\Carbon;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\System\Repositories\SystemStepConstantRepository;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;
use Api\KursPajak\Controllers\KursPajakController;



class FinalSteps {
	private $repositories,$productService;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_incomes' => new UserIncomeRepository,
			'user_assets' => new UserAssetRepository,
			'user_liabilities' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'system_step' => new SystemStepConstantRepository
		];
	}

	public function get()
	{
		
		try{
			
			$id = $this->repositories['user']->id;
			$name = $this->repositories['user']->name;
			$tax_profile = $this->repositories['user']->taxProfile->toArray();
            $additional_profile = $this->repositories['user']->additionalProfile->toArray();
            empty($additional_profile['detail']) ? null : $additional_profile['detail'] = json_decode($additional_profile['detail'],true);
            
            if(!empty($additional_profile['detail']['pph25_monthly'])){
                $additional_profile['detail']['pph25_pay'] = $additional_profile['detail']['pph25_yearly'] - ($additional_profile['detail']['pph25_monthly'] * 12);
                
                $additional_profile['detail']['pph25_payment_status'] = 'over';
                
                if($additional_profile['detail']['pph25_pay'] < 0){
                    $additional_profile['detail']['pph25_payment_status'] = 'insufficient';
                }
                
                 $additional_profile['detail']['pph25_monthly'] = number_format((abs($additional_profile['detail']['pph25_monthly'])),2,',','.');
                 $additional_profile['detail']['pph25_pay'] = number_format((abs($additional_profile['detail']['pph25_pay'])),2,',','.');
            }
           
			$current_step = $this->repositories['system_step']->getModel()->where('name','FINAL_STEPS')->first();
			
			$result = [
				'penghasilan' => null,
				'harta' => null,
				'hutang' => null,
				'id' => null,
				'name' => $name,
				'tax_profile' => $tax_profile,
                'additional_profile' => $additional_profile,
				'is_ta' => $tax_profile['following_tax_amnesty_2016'] == 1 ? true : false
			];
			
			$result['id'] = $id;

			$countryRepository = new CountryRepository();
			$bankRepository = new BankRepository();
			$user_incomes = $this->repositories['user_incomes']->getModel()->incomesQuery($id);
			$user_assets = $this->repositories['user_assets']->getModel()->assetsQuery($id);
			$user_liabilities = $this->repositories['user_liabilities']->getModel()->liabilitiesQuery($id);
		
			if(!$user_incomes->get()->isEmpty()){
				$penghasilan = $this->repositories['user_incomes']->getModel()->incomesQuery($id)->get();
				$penghasilan = !$penghasilan->isEmpty() ?  $penghasilan->toArray() : [] ;
				
				$penghasilan = ($this->mapingData($penghasilan));
				
				// dd($penghasilan);
				$result['penghasilan'] = $penghasilan;
			}
			
			if(!$user_assets->get()->isEmpty()){
				$result['harta'] = $user_assets->get()->toArray();
				$result['harta'] = $this->mapingData($result['harta']);
			}
			
			if(!$user_liabilities->get()->isEmpty()){
				$result['hutang'] = $user_liabilities->get()->toArray();
				$result['hutang'] = $this->mapingData($result['hutang']);
			}

		}catch(\Exception $e){
			dd($e);
		}
		// dd($result);
		$userStep = $this->repositories['user_step'];
        $userStep->saveOrUpdate($id, [ 'current_step' => $current_step->id]);
		
		return [
			'data' => $result
		];
	}

	public function getDataPenghasilan(){
		$status = 'success';
		return response()->json([
			'status' => $status,
			'data' => []
		]);
	}

	private function checkDetail($string){
		$array_check = [
			'coorporation',
			'name',
			'giver'
		];

		foreach ($array_check as $i_key => $item) {
			$sim = similar_text($item,$string,$perc);
			echo($item);
			echo('<br>');
			echo($string);
			echo('<br>');
			echo($perc);
			echo('<br>');
		}
	}

	private function toString(array $array){
		$str = '<ul>';
		foreach ($array as $v_key => $value) {
			$str = $str.'<li>'.$v_key.' : '.$value.'</li>';
		}
		$str = $str.'</ul>';
		return $str;
	}

	private function getCurrency($code){
        // $kurs = new KursPajakController();
        // $request = new \Illuminate\Http\Request();
        // $request->merge(['month' => 12]);
        // $request->merge(['date' => 31]);
        // $request->merge(['year' => Carbon::now('Asia/Jakarta')->subYears(1)->format('Y')]);
        // $request->merge(['currency' => $code]);
        
        // $result = json_decode($kurs->getKurs($request),true);

        // if(!empty($result)){
        //     return $result;
        // }

        return false;
    }

    public function mapingData($object){
    	$countryRepository = new CountryRepository();
		$bankRepository = new BankRepository();
    	$result = [];
    	try{
	    	foreach($object as $i_key => $item){
	    		
				if(!empty($item->detail)){
					$details_array = [];
					$item_details = (json_decode($item->detail,true));
					
					foreach ($item_details as $v_key => $value) {

		                if(empty($value)){
		                    continue;
		                }

		                if(in_array($v_key, ['currency','kurs_value','country'])){
		                    $getData = $countryRepository->getById($value);

		                    if(!empty($getData)){
		                        switch ($v_key) {
		                            case $v_key == 'currency':
		                                $value = $getData->currency;
		                                break;

		                            case $v_key == 'country':
		                                $value = $getData->name;
		                                break;

		                            case $v_key == 'kurs_value':
		                                $kurs_value = $this->getCurrency($getData->currency);
		                               
		                                if(!empty($kurs_value)){
		                                    $value = $kurs_value['kurspajak_value'];
		                                }else{
		                                   continue 2;
		                                }

		                                break;
		                            
		                            default:
		                                continue 2;
		                                break;
		                        }
		                    }else{
		                         continue;
		                    }
		                }

		                if(in_array($v_key, ['bank_id'])){
		                    $bank = $bankRepository->getById($value);
		                  
		                    if(!empty($bank)){
		                        $value = $bank->name;
		                    }else{
		                        continue;
		                    }
		                }

		                if(preg_match('/^\d+$/', $value) > 0 && preg_match('/npwp/', $value) < 1){
		                    $value = number_format($value,2,',','.');
		                }

						$key = str_replace('_',' ',$v_key);
						$details_array[$key] = $value;
						// $details_array[$v_key][1] = $value;
				}
					$item->detail = $details_array;
				}else{
					$item->detail = '-';
		        }
		        $result[$i_key] = $item;	
		       
			}
			return ($result);
		}catch(\Exception $e){
			return $result;
    	}
    }

    public function mapingDataForAjax($object,$name){
        $result[$name] = [];

        try{
        	$countryRepository = new CountryRepository();
			$bankRepository = new BankRepository();

            if(!$object->get()->isEmpty()){
                $data = $object->get()->toArray();
              
                foreach($data as $i_key => $item){

                	$data_array = (array)$item;

                	$filtered = array_filter($data_array, function ($key) {
                    	return (in_array($key, ['liability_name','income_name','asset_name']));
					},ARRAY_FILTER_USE_KEY);

                    $filtered = array_values($filtered);

                    $item->name = $filtered[0];

                    if(!empty($item->detail)){
                        $details_array = [];
                        $item_details = (json_decode($item->detail,true));
                      
                        foreach ($item_details as $v_key => $value) {

                            if(empty($value)){
                                continue;
                            }

                            if(in_array($v_key, ['currency','kurs_value','country'])){
                                $getData = $countryRepository->getById($value);

                                if(!empty($getData)){
                                    switch ($v_key) {
                                        case $v_key == 'currency':
                                            $value = $getData->currency;
                                            break;

                                        case $v_key == 'country':
                                            $value = $getData->name;
                                            break;

                                        case $v_key == 'kurs_value':
                                            $kurs_value = $this->getCurrency($getData->currency);
                                           
                                            if(!empty($kurs_value)){
                                                $value = $kurs_value['kurspajak_value'];
                                            }else{
                                               continue 2;
                                            }

                                            break;
                                        
                                        default:
                                            continue;
                                            break;
                                    }
                                }else{
                                     continue;
                                }
                            }

                            if(in_array($v_key, ['bank_id'])){
                                $bank = $bankRepository->getById($value);
                              
                                if(!empty($bank)){
                                    $value = $bank->name;
                                }else{
                                    continue;
                                }
                            }

                            if(preg_match('/^\d+$/', $value) > 0 && preg_match('/npwp/', $value) < 1){
                                $value = number_format($value,2,',','.');
                            }

                            $key = str_replace('_',' ',$v_key);
                            $details_array[$key] = $value;
                            // $details_array[$v_key][1] = $value;
                        }
                        $item->detail = $details_array;
                    }else{
                        $item->detail = '-';
                    }
                    
                 	
                    if(!array_key_exists($item->name, $result[$name])){
                        $result[$name][$item->name] = null;
                        $result[$name][$item->name][$i_key] = $item;
                        $result[$name][$item->name] = array_values($result[$name][$item->name]);
                    }else{
                        $result[$name][$item->name][$i_key] = $item;
                        $result[$name][$item->name] = array_values($result[$name][$item->name]);
                    }

                  
                }
               	
                // $result['hutang'] = $hutang;
            }
          
            return $result;
        }catch(\Exception $e){
            return $result;
        }    
    }
}
