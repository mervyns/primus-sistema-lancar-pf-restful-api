<?php
namespace WebApp\TaxFilling\FormData;

use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;

class Deposit {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
        $data = [];
        
		$data['deposits'] = $this->repositories['user']->asset()->where('asset_id', '=', 4)->get();

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		for ($i=0; $i < count($data['deposits']); $i++) {
			$detail = json_decode($data['deposits'][$i]['detail']);

			$data['deposits'][$i]['active'] = $i == 0 ? true : false;
			$data['deposits'][$i]['currency'] = isset($detail->currency) ? $detail->currency : '';
			$data['deposits'][$i]['currency_value'] = isset($detail->currency_value) ? $detail->currency_value : '';
			$data['deposits'][$i]['kurs_value'] = isset($detail->kurs_value) ? $detail->kurs_value : '';
			$data['deposits'][$i]['deposit_type'] = isset($detail->deposit_type) ? $detail->deposit_type : '';
			$data['deposits'][$i]['bank_id'] = isset($detail->bank_id) ? $detail->bank_id : '';
			$data['deposits'][$i]['deposit_number'] = isset($detail->deposit_number) ? $detail->deposit_number : '';
			$data['deposits'][$i]['bank_city'] = isset($detail->bank_city) ? $detail->bank_city : '';
			$data['deposits'][$i]['bank_branch'] = isset($detail->bank_branch) ? $detail->bank_branch : '';
			$data['deposits'][$i]['expired_date'] = isset($detail->expired_date) ? $detail->expired_date : '';
			$data['deposits'][$i]['deposit_period'] = isset($detail->deposit_period) ? $detail->deposit_period : '';
			$data['deposits'][$i]['deposit_interest'] = isset($detail->deposit_interest) ? $detail->deposit_interest : '';
			$data['deposits'][$i]['deposit_rollover'] = isset($detail->deposit_rollover) ? $detail->deposit_rollover : '';
			$data['deposits'][$i]['other_bank'] = isset($detail->other_bank) ? $detail->other_bank : '';
			$data['deposits'][$i]['country'] = isset($detail->country) ? $detail->country : '';
		}
		
		return $data;
	}

	public function getFinalSteps($id)
	{
		$data = [];
        
		$data['deposits'] = $this->repositories['user']->asset()->where('asset_id', '=', 4)->get();

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		if(!empty($id)){
			for ($i=0; $i < count($data['deposits']); $i++) {
				$detail = json_decode($data['deposits'][$i]['detail']);
				$data['deposits'][$i]['currency'] = isset($detail->currency) ? $detail->currency : '';
				$data['deposits'][$i]['currency_value'] = isset($detail->currency_value) ? $detail->currency_value : '';
				$data['deposits'][$i]['kurs_value'] = isset($detail->kurs_value) ? $detail->kurs_value : '';
				$data['deposits'][$i]['deposit_type'] = isset($detail->deposit_type) ? $detail->deposit_type : '';
				$data['deposits'][$i]['bank_id'] = isset($detail->bank_id) ? $detail->bank_id : '';
				$data['deposits'][$i]['deposit_number'] = isset($detail->deposit_number) ? $detail->deposit_number : '';
				$data['deposits'][$i]['bank_city'] = isset($detail->bank_city) ? $detail->bank_city : '';
				$data['deposits'][$i]['bank_branch'] = isset($detail->bank_branch) ? $detail->bank_branch : '';
				$data['deposits'][$i]['expired_date'] = isset($detail->expired_date) ? $detail->expired_date : '';
				$data['deposits'][$i]['deposit_period'] = isset($detail->deposit_period) ? $detail->deposit_period : '';
				$data['deposits'][$i]['deposit_interest'] = isset($detail->deposit_interest) ? $detail->deposit_interest : '';
				$data['deposits'][$i]['deposit_rollover'] = isset($detail->deposit_rollover) ? $detail->deposit_rollover : '';
				$data['deposits'][$i]['other_bank'] = isset($detail->other_bank) ? $detail->other_bank : '';
				$data['deposits'][$i]['country'] = isset($detail->country) ? $detail->country : '';
				$data['deposits'][$i]['active'] = $data['deposits'][$i]['id'] == $id ? true : false;
			}
		}
		// dd($data);
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'deposits' => [],
				'country' => [],
				'bank_list' => [],
				'is_final_steps_submited' => true,
			];
		}
		
		return $data;
	}

}
