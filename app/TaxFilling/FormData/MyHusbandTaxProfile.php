<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class MyHusbandTaxProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$spouseTaxProfile = $this->repositories['user']->spouseTaxProfile;
		return $spouseTaxProfile ? $spouseTaxProfile->toArray(): [];
	}
}