<?php
namespace WebApp\TaxFilling\FormData;

use Auth;

class CommanditaireProfitMember {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		
		$data['assets'] = $this->repositories['user']->asset()
			->where('asset_id', 16)->get();

            if (isset($data['assets'])){
                for ($i = 0; $i < count($data['assets']); $i++){
                    $detail = json_decode($data['assets'][$i]['detail']);
					$data['assets'][$i]['country_id'] = $detail->currency;
					$data['assets'][$i]['company_name'] = $detail->company_name;
                }
            }

        $data['user_income'] = $this->repositories['user']->income()->where('income_id', 9)->get();
        if (isset($data['user_income'])){
            for ($i = 0; $i < count($data['user_income']); $i++){
                $detail = json_decode($data['user_income'][$i]['detail'], true);
                $data['user_income'][$i]['perseroan_name'] = $detail['perseroan_name'];
            }
        }
        // dd($data);
		return $data;
	}

	public function getFinalSteps($id)
	{

		$data = null;

		$data['user_income'] = [];
		$data['assets'] = [];

		if(!empty($id)){
			$data['assets'] = $this->repositories['user']->asset()
			->where('asset_id', 16)->get();

            if (isset($data['assets'])){
                for ($i = 0; $i < count($data['assets']); $i++){
                    $detail = json_decode($data['assets'][$i]['detail']);
					$data['assets'][$i]['country_id'] = $detail->currency;
					$data['assets'][$i]['company_name'] = $detail->company_name;
                }
            }

			$data['user_income'] = $this->repositories['user']->income()->where('income_id', 9)->get();
			if (isset($data['user_income'])){
				for ($i = 0; $i < count($data['user_income']); $i++){
					$detail = json_decode($data['user_income'][$i]['detail'], true);
					$data['user_income'][$i]['perseroan_name'] = $detail['perseroan_name'];
				}
			}
		}
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);

		if($prev_url_piece[2] == 93){
			\Session::put('is_final_step',true);
		}
		
		if(\Session::get('is_final_step_submited')){
			return [
				'assets' => $data['assets'],
				'user_income' => $data['user_income'],
				'is_final_steps_submited' => true
			];
		}
		
		return $data;
	}

}