<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class ExchangeStockDetail {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];
                
				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
                    'kurs_value' 				=> $data['kurs_pajak'][$i],
                    'account_number'            => $data['account-number'][$i],
                    'company_name'              => $data['company-sekuritas'][$i],
                    'stock_name'                => $data['stock-name'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 9;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		
	}
}
