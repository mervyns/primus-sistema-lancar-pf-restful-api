<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class LandAndBuilding {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        //dd($data);
        $type = [];
		foreach ($data['penanggung'] as $row){
			$type[] = $row;
		}
		try{
            for ($i = 0; $i < count($data['income_id']); $i++){
                if (isset($data['income_id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income_id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }

                $detail = [
                    'type'                  => $type[$i],
                    'tax_rate'              => $data['tax_rate'][$i],
                    'rent_price_received'   => $data['rent-price-received'][$i],
                    'asset_id'              => $data['asset_id'][$i],
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 2;
                $userIncome->country_id = $data['country_id'][$i];
                $userIncome->net_value = $data['tax-final'][$i];
                $userIncome->gross_value = $data['rent-price'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->user_asset_id = $data['from-asset'][$i];
                $userIncome->save();
            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
        }catch (\Exception $ex){
            //dd($ex->getMessage());
            return false;
        }

	}
}