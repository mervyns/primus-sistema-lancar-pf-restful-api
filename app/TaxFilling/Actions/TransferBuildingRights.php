<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class TransferBuildingRights {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['TRANSFER_BUILDING_RIGHTS'];

            empty($data['value']) ? null : $data['value'] = array_values($data['value']);
            empty($data['address']) ? null : $data['address'] = array_values($data['address']);
            empty($data['role']) ? null : $data['role'] = array_values($data['role']);
            empty($data['final']) ? null : $data['final'] = array_values($data['final']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);

            if(!empty($data['value'])){
                foreach ($data['value'] as $e_key => $element){

                    if(!empty($data['id'][$e_key])){
                        
                        $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                
                        $user_income->net_value = $element - (int)$data['final'][$e_key];
                        $user_income->gross_value = $element;
                        $user_income->detail = json_encode([
                            'alamat' => $data['address'][$e_key],
                            'pajak_ditanggung' => $data['role'][$e_key]
                        ]);
                        
                    }else{

                        $user_income = $this->repositories['user_income']->getModel();
                        $user_income->user_id = $userId;
                        $user_income->income_id = 33;
                        $user_income->country_id = 1;
                        $user_income->net_value = $element - (int)$data['final'][$e_key];
                        $user_income->gross_value = $element;
                        $user_income->detail = json_encode([
                            'alamat' => $data['address'][$e_key],
                            'pajak_ditanggung' => $data['role'][$e_key]
                        ]);

                    }
                    $user_income->save();
                }
                $result = true;
            }

            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            return false;
        }
        
       
        return $result;
        
	}
}
