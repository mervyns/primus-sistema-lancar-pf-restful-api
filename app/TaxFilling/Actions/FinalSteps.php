<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class FinalSteps {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        $userStep = $this->repositories['user_step'];
        $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
        return true;
	}
}
