<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;

class CommonAssets {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_tax_profile' => new UserTaxProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'user_asset' => new UserAssetRepository,
			'user_liability' => new UserLiabilityRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if ($data['submit'] == 'skip' || $data['submit'] == 'reset'){
			$data['vehicle'] = null;
			$data['property'] = null;
			$data['card'] = null;
			$data['jewelry'] = null;
			$data['deposit'] = null;
		}

		if ($data['submit'] == 'reset') $this->resetOptions($userId);
		
		$taxProfile = $this->repositories['user_tax_profile']->getModel();

		$existingProfile = $taxProfile->where('user_id', '=', $userId)->first();
		if ($existingProfile) {
			$taxProfile = $existingProfile;
		}
		
        $taxProfile->user_id = $userId;
        $taxProfile->have_vehicle = $data['vehicle'] == 'have_vehicle' ? 1 : 0;
        $taxProfile->have_property = $data['property'] == 'have_property' ? 1 : 0;
        $taxProfile->have_credit_card = $data['card'] == 'have_card' ? 1 : 0;
        $taxProfile->have_jewelry = $data['jewelry'] == 'have_jewelry' ? 1 : 0;
        $taxProfile->have_deposit = $data['deposit'] == 'have_deposit' ? 1 : 0;
        
		if($taxProfile->save()) {
			$this->parseDirection($data, $step);
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);

			return $taxProfile;
		}

		return false;
	}

	private function resetOptions($userId) {
		$id_assets = [4, 19, 20, 22, 23, 24, 28, 29, 30];
		$id_liabilities = [1, 2];

		$delete_assets = $this->repositories['user_asset']->getModel()
			->where('user_id', $userId)
			->whereIn('asset_id', $id_assets)
			->delete();
		
		$delete_liabilities = $this->repositories['user_liability']->getModel()
			->where('user_id', $userId)
			->whereIn('liability_id', $id_liabilities)
			->delete();
	}

	private function parseDirection($data, $step){
		$common = [19, 20, 21, 22, 23, 24, 25, 26, 27];
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $val => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				if ($data['deposit'] == 'have_deposit') $rebuildStep[] = 19;
				if ($data['card'] == 'have_card') $rebuildStep[] = 20;
				if ($data['vehicle'] == 'have_vehicle') $rebuildStep[] = 21;
				if ($data['property'] == 'have_property') $rebuildStep[] = 23;
				if ($data['jewelry'] == 'have_jewelry') $rebuildStep[] = 25;
			}
		}
		
		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

	}

}