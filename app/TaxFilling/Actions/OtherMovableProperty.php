<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class OtherMovableProperty {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];
                
				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
                    'kurs_value' 				=> $data['kurs_pajak'][$i],
					'description'           	=> $data['description'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 27;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;

		// foreach ($data['omp-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id = 27;
		// 		$userAsset->acquisition_year = $data['omp-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-omp-value'][$i];

		// 		if(!isset($data['omp-country'][$i])){
		// 			$data['omp-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'omp_country' => (integer) $data['omp-country'][$i],
		// 			'omp_currency' => (integer) $data['omp-currency'][$i],
		// 			'omp_foreign_value' => (double) $data['omp-foreign-value'][$i],
		// 			'omp_name' => $data['omp-name'][$i],
		// 			'omp_id_number' => $data['omp-id-number'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}
}
