<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserAssets\Repositories\UserAssetRepository;

class FinancialInvestmentsProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'user_income' => new UserIncomeRepository,
			'user_asset' => new UserAssetRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if ($data['submit'] == 'skip' || $data['submit'] == 'reset'){
			for ($i = 0; $i < count($data['invesment']); $i++){
				$data['invesment'][$i] = null;
			}
		}

		try{
			if ($data['submit'] == 'reset') $this->resetOptions($userId);

			$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
			$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();
			
			$newDetail = [];
			
			if ($existingProfile){
				$newDetail = json_decode($existingProfile['detail'], true);
				$userAdditionalProfile = $existingProfile;
			}

			$newDetail['exchange_stock'] = isset($data['invesment'][0]) ? (bool)$data['invesment'][0] : false;
			$newDetail['mutual_funds'] = isset($data['invesment'][1]) ? (bool)$data['invesment'][1] : false;
			$newDetail['insurance'] = isset($data['invesment'][2]) ? (bool)$data['invesment'][2] : false;
			$newDetail['indonesian_bank_certificate'] = isset($data['invesment'][3]) ? (bool)$data['invesment'][3] : false;
			$newDetail['goverment_bonds'] = isset($data['invesment'][4]) ? (bool)$data['invesment'][4] : false;
			$newDetail['corporate_bonds'] = isset($data['invesment'][5]) ? (bool)$data['invesment'][5] : false;
			$newDetail['non_tradeable_stock'] = isset($data['invesment'][6]) ? (bool)$data['invesment'][6] : false;
			$newDetail['capital_investment_companies'] = isset($data['invesment'][7]) ? (bool)$data['invesment'][7] : false;
			$newDetail['other_debentures'] = isset($data['invesment'][8]) ? (bool)$data['invesment'][8] : false;
			$newDetail['other_investments'] = isset($data['invesment'][9]) ? (bool)$data['invesment'][9] : false;

			$userAdditionalProfile->user_id = $userId;
			$userAdditionalProfile->detail = json_encode($newDetail);
			$userAdditionalProfile->save();
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection($data['invesment'], $step);
			return true;
		}catch(\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
	}

	private function resetOptions($userId) {
		$id_assets = [9, 14, 32, 33, 12, 11, 10, 16, 13, 17];
		$id_incomes = [40];

		$delete_income = $this->repositories['user_income']->getModel()
			->where('user_id', $userId)
			->whereIn('income_id',$id_incomes)
			->delete();

		$delete_asset = $this->repositories['user_asset']->getModel()
			->where('user_id', $userId)
			->whereIn('asset_id', $id_assets)
			->delete();		
	}

	private function parseDirection($data, $step){
		$common = [29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42];
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				foreach ($data as $val){
					if ((bool)$val == true){
						$rebuildStep[] = (int)$val;
						if ($val == 33){
							$rebuildStep[] = 34;
						}
					}
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

	}

}
