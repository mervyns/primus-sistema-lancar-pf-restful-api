<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class OtherCashEquivalents {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];
                
				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
                    'kurs_value' 				=> $data['kurs_pajak'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 5;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		// $i = 0;

		// foreach ($data['oce-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id = 5;
		// 		$userAsset->acquisition_year = $data['oce-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-oce-value'][$i];

		// 		if(!isset($data['oce-country'][$i])){
		// 			$data['oce-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'oce_country' => (integer) $data['oce-country'][$i],
		// 			'oce_currency' => (integer) $data['oce-currency'][$i],
		// 			'oce_foreign_value' => (double) $data['oce-foreign-value'][$i],
		// 			'oce_name' => $data['oce-name'][$i],
		// 			'oce_id_number' => $data['oce-id-number'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}
}
