<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class MutualFunds {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];
                
				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
                    'kurs_value' 				=> $data['kurs_pajak'][$i],
                    'account_number'            => $data['account-number'][$i],
                    'company_name'              => $data['company-reksadana'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 14;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;

		// foreach ($data['mutual-funds-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id = 14;
		// 		$userAsset->acquisition_year = $data['mutual-funds-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-mutual-funds-value'][$i];

		// 		if(!isset($data['mutual-funds-country'][$i])){
		// 			$data['mutual-funds-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'mutual_funds_country' => (integer) $data['mutual-funds-country'][$i],
		// 			'mutual_funds_currency' => (integer) $data['mutual-funds-currency'][$i],
		// 			'mutual_funds_foreign_value' => (double) $data['mutual-funds-foreign-value'][$i],
		// 			'mutual_funds_companies' => $data['mutual-funds-companies'][$i],
		// 			'mutual_funds_account_number' => $data['mutual-funds-account-number'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}
}
