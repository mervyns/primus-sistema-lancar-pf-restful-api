<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class MyTaxProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_tax_profile' => new UserTaxProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$taxProfile = $this->repositories['user_tax_profile']->getModel();
		
		$existingProfile = $taxProfile->where('user_id', '=', $userId)->first();
		if ($existingProfile) {
			$taxProfile = $existingProfile;
		}

		$taxProfile->user_id = $userId;
		$taxProfile->npwp = !isset($data['npwp']) ? '' : $data['npwp'];
		$taxProfile->efin = !isset($data['efin']) ? '' : $data['efin'];
		$taxProfile->job_status_id = $data['jobstatus'];
		$taxProfile->klu_id = $data['klu'];

		if($taxProfile->save()) {
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);

			return $taxProfile;
		}

		return false;
	}
}