<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class HonorariumOfApbdnExpenses {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['HONORARIUM_OF_APBDN_EXPENSES'];
            empty($data['netto']) ? $netto = null : $netto = array_values($data['netto']);        
            empty($data['name']) ? $name = null : $name = array_values($data['name']);        
            empty($data['type']) ? $type = null : $type = array_values($data['type']); 
            empty($data['brutto']) ? $brutto = null : $brutto = array_values($data['brutto']);  
            empty($data['final']) ? $final = null : $final = array_values($data['final']);
            empty($data['tarif']) ? $tarif = null : $tarif = array_values($data['tarif']);       
            empty($data['id']) ? $ids = null : $ids = array_values($data['id']);        
        
            foreach($netto as $t_key => $item){
            
                switch ($type[$t_key]) {
                    case 1:
                        $income_id = 29;
                        break;
                    case 2:
                        $income_id = 30;
                        break;
                    case 3:
                        $income_id = 31;
                        break;
                    case 4:
                        $income_id = 32;
                        break;
                }
              
                if(!empty($ids[$t_key])){
                        
                    $user_income = $this->repositories['user_income']->getModel()->find($ids[$t_key]);
                    
                    $user_income->income_id = $income_id;
                    $user_income->net_value = $item;
                    $user_income->gross_value = $brutto[$t_key];
                    $user_income->detail = json_encode([
                        'nama_pemotong_pajak' => $name[$t_key],
                        'tarif' => $tarif[$t_key],
                    ]);
                    
                }else{
                    $user_income = $this->repositories['user_income']->getModel();
            
                    $user_income->user_id = $userId;
                    $user_income->income_id = $income_id;
                    $user_income->country_id = 1;
                    $user_income->net_value = $item;
                    $user_income->gross_value = $brutto[$t_key];
                    $user_income->detail = json_encode([
                        'nama_pemotong_pajak' => $name[$t_key],
                        'tarif' => $tarif[$t_key],
                    ]);
                }
                $user_income->save();
            }
        
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            $result = true;
        
            return $result;

        }catch(\Exception $e){
            dd($e);
        }
        
	}
}
