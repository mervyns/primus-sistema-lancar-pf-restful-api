<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\Incomes\Repositories\IncomeRepository;

class AwardsAndGift {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository,
            'income' => new IncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
        
            $result = false;
            $self_step_index = config('constants')['steps']['AWARDS_AND_GIFT'];
          
            empty($data['brutto']) ? null : $data['brutto'] = array_values($data['brutto']);
            empty($data['cutter']) ? null : $data['cutter'] = array_values($data['cutter']);
            empty($data['netto']) ? null : $data['netto'] = array_values($data['netto']);
            empty($data['cutter-npwp']) ? null : $data['cutter-npwp'] = array_values($data['cutter-npwp']);
            empty($data['proof_pph']) ? null : $data['proof_pph'] = array_values($data['proof_pph']);
            empty($data['date_pph']) ? null : $data['date_pph'] = array_values($data['date_pph']);
            empty($data['taken_pph']) ? null : $data['taken_pph'] = array_values($data['taken_pph']);
            empty($data['tarif']) ? null : $data['tarif'] = array_values($data['tarif']);
            empty($data['type']) ? null : $data['type'] = array_values($data['type']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
            // dd($data);

            DB::transaction(function () use ($userId, $step, $data) {
                foreach ($data['brutto'] as $e_key => $element){

                    if(!empty($data['id'][$e_key])){

                        if(!empty($data['netto'][$e_key])){
                            $data['tarif'][$e_key] = (int)($data['tarif'][$e_key]);
                            $type = null;
                            $tax_rate =  ($data['tarif'][$e_key]/100);
                        
                            $brutto = $data['brutto'][$e_key];
                            
                            $income = $this->repositories['income']->getModel()
                                                ->where('tax_rate',$tax_rate)
                                                ->whereIn('id', [11, 12, 13, 14])->first();
                        
                            $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                            $user_income->user_id = $userId;
                            $user_income->income_id = $income->id;
                            $user_income->country_id = 1;
                            $user_income->net_value = $data['netto'][$e_key];
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'nama_pemotong' => $data['cutter'][$e_key],
                                'npwp_pemotong' => $data['cutter-npwp'][$e_key],
                                'bukti_potong' => $data['proof_pph'][$e_key],
                                'tanggal_potong' => $data['date_pph'][$e_key],
                                'dipotong_pajak' => 1,
                                'pajak_dibayarkan' =>$data['taken_pph'][$e_key],
                            ]);
                            $user_income->save();
                            
                        }else{
                            $income_id = 14;
                            $income = $this->repositories['income']->getModel()
                                        ->where('lower_limit','<=',$element)
                                        ->where('upper_limit','>=',$element)
                                        ->whereIn('id', [11, 12, 13, 14])->first();
                        
                            if(!empty($income)){
                                $income_id = $income->id;
                            }
    
                            $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                            $user_income->user_id = $userId;
                            $user_income->income_id = $income_id;
                            $user_income->country_id = 1;
                            $user_income->net_value = $element;
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'dipotong_pajak' => 0,
                                'pajak_dibayarkan' =>$data['taken_pph'][$e_key],
                            ]);
                            $user_income->save();
                        }  
                    }else{
                        if(!empty($data['netto'][$e_key])){
                            $data['tarif'][$e_key] = (int)($data['tarif'][$e_key]);
                            $type = null;
                            $tax_rate =  ($data['tarif'][$e_key]/100);
                        
                            $brutto = $data['brutto'][$e_key];
                            
                            $income = $this->repositories['income']->getModel()
                                                ->where('tax_rate',$tax_rate)
                                                ->whereIn('id', [11, 12, 13, 14])->first();
                        
                            $user_income = $this->repositories['user_income']->getModel();
                            $user_income->user_id = $userId;
                            $user_income->income_id = $income->id;
                            $user_income->country_id = 1;
                            $user_income->net_value = $data['netto'][$e_key];
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'nama_pemotong' => $data['cutter'][$e_key],
                                'npwp_pemotong' => $data['cutter-npwp'][$e_key],
                                'bukti_potong' => $data['proof_pph'][$e_key],
                                'tanggal_potong' => $data['date_pph'][$e_key],
                                'dipotong_pajak' => 1,
                                'pajak_dibayarkan' =>$data['taken_pph'][$e_key],
                            ]);
                            $user_income->save();
                            
                        }else{
                            $income_id = 14;
                            $income = $this->repositories['income']->getModel()
                                        ->where('lower_limit','<=',$element)
                                        ->where('upper_limit','>=',$element)
                                        ->whereIn('id', [11, 12, 13, 14])->first();
                        
                            if(!empty($income)){
                                $income_id = $income->id;
                            }
    
                            $user_income = $this->repositories['user_income']->getModel();
                            $user_income->user_id = $userId;
                            $user_income->income_id = $income_id;
                            $user_income->country_id = 1;
                            $user_income->net_value = $element;
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'dipotong_pajak' => 0,
                                'pajak_dibayarkan' =>$data['taken_pph'][$e_key],
                            ]);
                            $user_income->save();
                        }  
                    }

                          
                }
            });
           
            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);
            $result = true;

            return $result;
        }catch(\Exception $e){
            return false;
        }
        
	}
}
