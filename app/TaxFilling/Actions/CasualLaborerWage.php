<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class CasualLaborerWage {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{

            $result = false;
            $self_step_index = config('constants')['steps']['CASUAL_LABORER_WAGE'];
            empty($data['brutto']) ? $brutto = null : $brutto = array_values($data['brutto']);        
            empty($data['pph']) ? $pph = null : $pph = array_values($data['pph']);        
            empty($data['netto']) ? $netto = null : $netto = array_values($data['netto']);        
            empty($data['cutter-name']) ? $cutter_name = null : $cutter_name = array_values($data['cutter-name']);        
            empty($data['npwp']) ? $npwp = null : $npwp = array_values($data['npwp']);        
            empty($data['proof']) ? $proof = null : $proof = array_values($data['proof']);        
            empty($data['date']) ? $date = null : $date = array_values($data['date']); 
            empty($data['id']) ? $ids = null : $ids = array_values($data['id']);        
        
            foreach($brutto as $b_key => $brutto_value){

                if(!empty($ids[$b_key])){
                        
                    $user_income = $this->repositories['user_income']->getModel()->find($ids[$b_key]);
                    
                    $user_income->net_value = $netto[$b_key];
                    $user_income->gross_value = $brutto_value;
                    $user_income->detail = json_encode([
                        'pph' => $pph[$b_key],
                        'nama_pemotong' => $cutter_name[$b_key],
                        'npwp_pemotong' => $npwp[$b_key],
                        'nomor_bukti_potong' => $proof[$b_key],
                        'tanggal_bukti_potong' => $date[$b_key],
                    ]);
                    
                }else{
                    $user_income = $this->repositories['user_income']->getModel();

                    // $user_income = $user_income->firstOrNew(
                    //     ['user_id' => $userId],
                    //     ['income_id' => $type]
                    // );
                
                    $user_income->user_id = $userId;
                    $user_income->income_id = 44;
                    $user_income->country_id = 1;
                    $user_income->net_value = $netto[$b_key];
                    $user_income->gross_value = $brutto_value;
                    $user_income->detail = json_encode([
                        'pph' => $pph[$b_key],
                        'nama_pemotong' => $cutter_name[$b_key],
                        'npwp_pemotong' => $npwp[$b_key],
                        'nomor_bukti_potong' => $proof[$b_key],
                        'tanggal_bukti_potong' => $date[$b_key],
                    ]);
                }
                
                $user_income->save();

            }
        
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            $result = true;
        
            return $result;

        }catch(\Exception $e){
            dd($e);
        }
	}
}
