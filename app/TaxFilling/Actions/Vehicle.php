<?php
namespace WebApp\TaxFilling\Actions;
use Auth;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Vehicle {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$credit = [];
		foreach ($data['vehicle-credit'] as $row){
			$credit[] = $row;
		}
		
		try{
			$parse = false;
			for ($i = 0; $i < count($data['asset-id']); $i++){
				if ($credit[$i] == "Ya") $parse = true;

				if (!empty($data['asset-id'][$i])){
					$userAsset = $this->repositories['user_asset']->getById($data['asset-id'][$i]);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
					'currency' 					=> $data['currency'][$i],
					'country' 					=> $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'credite_type'				=> $credit[$i],
					'vehicle_merk'				=> $data['vehicle-merk'][$i],
					'plate_number'				=> $data['police-number'][$i],
					'bpkb_number'				=> $data['bpkb-number'][$i],
					'expired_date'				=> $data['expired-date'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = $data['vehicle-type'][$i];
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();

			}
			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			$this->parseDirection($parse, $step);
			return true;
		}catch (\Exception $ex){
			//dd($ex);
			return false;
		}

		// $i = 0;
		// $error = 0;

		// foreach($data['vehicle-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id =(integer) $data['vehicle-type'][$i];
		// 		$userAsset->acquisition_year = date("Y") - 1;
		// 		$userAsset->idr_value = (double) $data['hidden-vehicle-value'][$i];

		// 		if(!isset($data['vehicle-country'][$i])){
		// 			$data['vehicle-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'vehicle_country' => (integer) $data['vehicle-country'][$i],
		// 			'vehicle_currency' => (integer) $data['vehicle-currency'][$i],
		// 			'vehicle_foreign_value' => (double) $data['vehicle-foreign-value'][$i],
		// 			'police_number' => $data['police-number'][$i],
		// 			'bpkb_number' => $data['bpkb-number'][$i],
		// 			'vehicle_name' => $data['vehicle-name'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAsset;
	}

	public function parseDirection($parse, $step){
		$taxFillStep = $this->repositories['user_step']->getWhere('user_id', Auth::user()->id)->toArray()[0];
		$tempStep = empty($taxFillStep['temp_steps']) ? [] : json_decode($taxFillStep['temp_steps'], true);
		$arrayKey = array_search(22, $tempStep);
		if ($arrayKey !== false) unset($tempStep[$arrayKey]);
		$tempStep = array_values($tempStep);
		
		$rebuildStep = [];

		if ($parse == true){
			$key = array_search($step, $tempStep);
			if ($key !== false) array_splice($tempStep, (int)$key + 1, 0, array(22));
		}

		$rebuildStep = array_values($tempStep);
		
		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep['id']);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

		
	}

}
