<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class PropertyLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['liability_id']); $i++){
				if (!empty($data['certificate-type'][$i])){
					if ($data['liability_id'][$i] == true){
						$userLiability = $this->repositories['user_liability']->getById($data['liability_id'][$i]);
					}else{
						$userLiability = $this->repositories['user_liability']->getModel();
					}

					$date_split = explode('/', $data['paid-off'][$i]);

					$detail = [
						'currency' 					=> $data['currency'][$i],
						'country' 					=> $data['country'][$i],
						'currency_value' 			=> $data['credite-value'][$i],
						'kurs_value' 				=> $data['kurs_pajak'][$i],
						'property_id'				=> $data['certificate-type'][$i],
						'credite_duration'			=> $data['credit-duration'][$i],
						'paid_off_date'				=> $date_split[2].'-'.$date_split[1].'-'.$date_split[0],
						'yearly_interest'			=> $data['yearly-interest'][$i],
						'credite_type'				=> 'property',
						'bank_id'					=> $data['bank-name'][$i],
						'other_bank'				=> $data['other-bank'][$i]
					];

					$userLiability->user_id = $userId;
					$userLiability->liability_id = 1;
					$userLiability->acquisition_year = (int) $data['acquisition-year'][$i];
					$userLiability->value = (int) $data['jumlah_rupiah'][$i];
					$userLiability->lender_name = '';
					$userLiability->lender_address = $data['lender-address'][$i];
					$userLiability->detail = json_encode($detail);
					$userLiability->save();


				}
			}
			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($data);
			return false;
		}
		// $i = 0;
		// $error = 0;

		// foreach($data['property-liability-id'] as $liabilityId){
		// 	$userLiability = $this->repositories['user_liability']->getModel();
		// 	$existingLiability = $userLiability->where('id', '=', $liabilityId)->first();

		// 	if ($existingLiability) {
		// 		$userLiability = $existingLiability;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userLiability->user_id = $userId;
		// 		$userLiability->liability_id = 5;
		// 		$userLiability->acquisition_year = $data['property-liability-acquisition-year'][$i];
		// 		$userLiability->value = (double) $data['hidden-property-liability-value'][$i];
		// 		if($data['property-liability-bank-name'][$i] != 'Lainnya'){
		// 			$userLiability->lender_name = $data['property-liability-bank-name'][$i];
		// 		}
		// 		else{
		// 			$userLiability->lender_name = $data['property-liability-other-bank-name'][$i];
		// 		}
		// 		$userLiability->lender_address = $data['property-liability-bank-address'][$i];

		// 		if(!isset($data['property-liability-country'][$i])){
		// 			$data['property-liability-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'property_certificate_number' => $data['property-certificate-number'][$i],
		// 			'property_liability_country' => $data['property-liability-country'][$i],
		// 			'property_liability_currency' => $data['property-liability-currency'][$i],
		// 			'property_liability_foreign_value' => $data['property-liability-foreign-value'][$i],
		// 			'property_liability_bank_name' => $data['property-liability-bank-name'][$i],
		// 			'property_liability_other_bank_name' => $data['property-liability-other-bank-name'][$i],
		// 			'property_liability_bank_address' => $data['property-liability-bank-address'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userLiability->detail = $encodeDetail;
		// 		$userLiability->save();

		// 		$i++;
		// 	} else {
		// 		$userLiability->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userLiability;
	}
}
