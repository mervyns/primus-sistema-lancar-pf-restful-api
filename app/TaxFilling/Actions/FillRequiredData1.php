<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;


class FillRequiredData1 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_tax_profile' => new UserTaxProfileRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_spouse_tax' => new SpouseTaxProfileRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $taxProfile = $this->repositories['user_tax_profile']->getModel()->where('user_id', $userId)->first();
            $taxProfile->npwp = $data['my-npwp'];
            $taxProfile->efin = $data['my-efin'];
            $taxProfile->nik = $data['my-nik'];
            $taxProfile->save();

            if (!is_null($data['marital'])){
                $spouseTaxProfile = $this->repositories['user_spouse_tax']->getModel()->where('user_id', $userId)->first();
                $spouseTaxProfile->npwp = $data['spouse-npwp'];
                $spouseTaxProfile->nik = $data['spouse-nik'];
                $spouseTaxProfile->save();
            }
            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            return true;
        }catch (\Exception $ex){
            return false;
        }
    }

    private function parseDirection($userId){
        $taxFillingStep = $this->repositories['user_step']->getWhere('user_id', $userId)->toArray()[0];
        $tempStep = is_null($taxFillingStep['temp_steps']) ? [] : json_decode($taxFillingStep['temp_steps'], true);

        $key = array_search(91, $tempStep);
        if ($key !== false){
            unset($tempStep[$key]);
            $tempStep = array_values($tempStep);
        }

        $updateStep = $this->repositories['user_step']->getModel()->find($taxFillingStep['id']);
        $updateStep->temp_steps = json_encode($tempStep);
        $updateStep->save();
    }

}
