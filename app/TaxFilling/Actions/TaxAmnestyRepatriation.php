<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserRepatriatedAssets\Repositories\UserRepatriatedAssetRepository;
use Api\UserRevealedAssets\Repositories\UserRevealedAssetRepository;

class TaxAmnestyRepatriation {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_repatriated_asset' => new UserRepatriatedAssetRepository,
            'user_revealed_asset' => new UserRevealedAssetRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $input)
	{
        try{

            $result = false;

            $input['user_id'] = empty($input['user_id']) ?  $input['user_id'] : array_values( $input['user_id']);
            $input['asset_id'] = empty($input['asset_id']) ?  $input['asset_id'] : array_values( $input['asset_id']);
            $input['kode_harta'] = empty($input['kode_harta']) ?  $input['kode_harta'] : array_values( $input['kode_harta']);
            $input['nama_harta'] = empty($input['nama_harta']) ?  $input['nama_harta'] : array_values( $input['nama_harta']);
            $input['nilai_dialihkan'] = empty($input['nilai_dialihkan']) ?  $input['nilai_dialihkan'] : array_values( $input['nilai_dialihkan']);
            $input['gateway_id'] = empty($input['gateway_id']) ?  $input['gateway_id'] : array_values( $input['gateway_id']);
            $input['investment_id'] = empty($input['investment_id']) ?  $input['investment_id'] : array_values( $input['investment_id']);
            $input['tanggal_investasi'] = empty($input['tanggal_investasi']) ?  $input['tanggal_investasi'] : array_values( $input['tanggal_investasi']);
            $input['value'] = empty($input['value']) ?  $input['value'] : array_values( $input['value']);
            $input['currency_id'] = empty($input['currency_id']) ?  $input['currency_id'] : array_values( $input['currency_id']);
            $input['keterangan'] = empty($input['keterangan']) ?  $input['keterangan'] : array_values( $input['keterangan']);
           
            if(!empty($input)){

                DB::transaction(function () use ($userId, $step, $input) {
            
                    foreach ($input['nilai_dialihkan'] as $d_key => $data) {
                       
                        $user_revealed_asset_model = $this->repositories['user_revealed_asset']->getModel();
                        $user_revealed_asset_model->user_id = $input['user_id'][$d_key];
                        $user_revealed_asset_model->asset_id = $input['asset_id'][$d_key];
                        $user_revealed_asset_model->country_id = $input['currency_id'][$d_key];
                        $user_revealed_asset_model->value = $input['value'][$d_key];
                        $user_revealed_asset_model->save();
                    
                        if($user_revealed_asset_model){
                            $user_repatriated_asset_model = $this->repositories['user_repatriated_asset']->getModel();
                            $user_repatriated_asset_model->user_revealed_asset_id =$user_revealed_asset_model->id;
                            $user_repatriated_asset_model->investment_id = $input['investment_id'][$d_key];
                            $user_repatriated_asset_model->gateway_id = $input['gateway_id'][$d_key];
                            $user_repatriated_asset_model->will_repatriated_value = $input['value'][$d_key];
                            $user_repatriated_asset_model->has_repatriated_value = $input['nilai_dialihkan'][$d_key];
                            $user_repatriated_asset_model->investment_date = $input['tanggal_investasi'][$d_key];
                            $user_repatriated_asset_model->save();

                            if($user_repatriated_asset_model){
                                $user_asset_model = $this->repositories['user_asset']->getModel();
                                $user_asset_model = $user_asset_model->find($input['asset_id'][$d_key]);
                                if(!empty($user_asset_model)){
                                    $user_asset_model->is_ta = 1;
                                    $user_asset_model->save();
                                }
                            }
                        }

                    }
                    $userStep = $this->repositories['user_step'];
                    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
                    
                });
                

                $result = true;
        
            }
        
            return $result;
        
        }catch(\exception $e){
            dd($e);
        }
	}
}
