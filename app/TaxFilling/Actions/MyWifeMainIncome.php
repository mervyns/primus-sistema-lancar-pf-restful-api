<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class MyWifeMainIncome {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$userIncome = $this->repositories['user_income']->getModel();
		$userWithholdingTax = $this->repositories['user_withholding_tax']->getModel();

		if($data['job_status_id'] == 2){
			$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 34]])->first();

			if (!is_null($existingIncome)) {
				$userIncome = $existingIncome;
				$userIncome->user_id = $userId;
				$userIncome->income_id = 34;
				$userIncome->country_id = 1;
				$userIncome->net_value = $data['wife-income-from-employer'];
				$userIncome->gross_value = $data['wife-income-from-employer'] + $data['wife-income-tax-from-employer'];

				//delete existing withholding tax
				$existingUserWithholdingTax = $userWithholdingTax->where('user_income_id', '=', $userIncome->id)->first();
				if(!is_null($existingUserWithholdingTax)){
					$existingUserWithholdingTax->delete();
				}
			} else{
				$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 33]])->first();

				if (!is_null($existingIncome)) {
					$userIncome = $existingIncome;
					$userIncome->user_id = $userId;
					$userIncome->income_id = 34;
					$userIncome->country_id = 1;
					$userIncome->net_value = $data['wife-income-from-employer'];
					$userIncome->gross_value = $data['wife-income-from-employer'] + $data['wife-income-tax-from-employer'];
				} else {
					$userIncome->user_id = $userId;
					$userIncome->income_id = 34;
					$userIncome->country_id = 1;
					$userIncome->net_value = $data['wife-income-from-employer'];
					$userIncome->gross_value = $data['wife-income-from-employer'] + $data['wife-income-tax-from-employer'];
				}
			}

			if($userIncome->save()) {
				$userStep = $this->repositories['user_step'];
				$userStep->saveOrUpdate($userId, [
					'current_step' => $step
				]);

				return $userIncome;
			}

			return false;
		}


		if($data['job_status_id'] == 4){
			$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 33]])->first();


			if (!is_null($existingIncome)) {
				$userIncome = $existingIncome;
				$userIncome->user_id = $userId;
				$userIncome->income_id = 33;
				$userIncome->country_id = 1;
				$userIncome->net_value = 0;
				$userIncome->gross_value = $data['wife-other-income'];
					
				//delete existing withholding tax
				$existingUserWithholdingTax = $userWithholdingTax->where('user_income_id', '=', $userIncome->id)->first();
				if(!is_null($existingUserWithholdingTax)){
					$existingUserWithholdingTax->delete();
				}
			} else{
				$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 34]])->first();
				
				if (!is_null($existingIncome)) {
					$userIncome = $existingIncome;
					$userIncome->user_id = $userId;
					$userIncome->income_id = 33;
					$userIncome->country_id = 1;
					$userIncome->net_value = 0;
					$userIncome->gross_value = $data['wife-other-income'];

					//delete existing withholding tax
					$existingUserWithholdingTax = $userWithholdingTax->where('user_income_id', '=', $userIncome->id)->first();
					if(!is_null($existingUserWithholdingTax)){
						$existingUserWithholdingTax->delete();
					}
				} else{
					$userIncome->user_id = $userId;
					$userIncome->income_id = 33;
					$userIncome->country_id = 1;
					$userIncome->net_value = 0;
					$userIncome->gross_value = $data['wife-other-income'];
				}
			}

			if($userIncome->save()) {

				return $userIncome;
			}

			return false;
		}

		if($data['job_status_id'] == 3){
			$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 33]])->first();

			if (!is_null($existingIncome)) {
				$userIncome = $existingIncome;
				$userIncome->user_id = $userId;
				$userIncome->income_id = 33;
				$userIncome->country_id = 1;
				$userIncome->net_value = 0;
				$userIncome->gross_value = $data['wife-net-income'];
			} else{
				$existingIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 34]])->first();

				if (!is_null($existingIncome)) {
					$userIncome = $existingIncome;
					$userIncome->user_id = $userId;
					$userIncome->income_id = 33;
					$userIncome->country_id = 1;
					$userIncome->net_value = 0;
					$userIncome->gross_value = $data['wife-net-income'];

					//delete existing withholding tax
					$existingUserWithholdingTax = $userWithholdingTax->where('user_income_id', '=', $userIncome->id)->first();
					if(!is_null($existingUserWithholdingTax)){
						$existingUserWithholdingTax->delete();
					}
				} else{
					$userIncome->user_id = $userId;
					$userIncome->income_id = 33;
					$userIncome->country_id = 1;
					$userIncome->net_value = 0;
					$userIncome->gross_value = $data['wife-net-income'];
				}
			}

			if($userIncome->save()) {
				$userIncome = $userIncome->where([['user_id', '=', $userId],['income_id', '=', 33]])->first();

				$existingUserWithholdingTax = $userWithholdingTax->where('user_income_id', '=', $userIncome->id)->first();

				if(!is_null($existingUserWithholdingTax)){
				$userWithholdingTax = $existingUserWithholdingTax;
				$userWithholdingTax->user_income_id = $userIncome->id;
				$userWithholdingTax->withholding_tax_slip_id = 1;
				$userWithholdingTax->withholder_name = $data['withholder-name'];
				$userWithholdingTax->withholder_npwp = $data['withholder-npwp'];
				$userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'];
				$userWithholdingTax->withholding_slip_date = $data['withholding-slip-date'];
				$userWithholdingTax->value = $data['withholding-value'];
				} else {
					$userWithholdingTax->user_income_id = $userIncome->id;
					$userWithholdingTax->withholding_tax_slip_id = 1;
					$userWithholdingTax->withholder_name = $data['withholder-name'];
					$userWithholdingTax->withholder_npwp = $data['withholder-npwp'];
					$userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'];
					$userWithholdingTax->withholding_slip_date = $data['withholding-slip-date'];
					$userWithholdingTax->value = $data['withholding-value'];
				}

				if($userWithholdingTax->save()) {
					return $userWithholdingTax;
				}

				return false;

			}

			return false;
		}

		return false;
	}
}
