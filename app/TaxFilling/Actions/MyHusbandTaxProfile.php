<?php
namespace WebApp\TaxFilling\Actions;

use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;
use Api\UserDependants\Repositories\UserDependantRepository;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;

class MyHusbandTaxProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_tax_profile' => new UserTaxProfileRepository,
			'spouse_tax_profile' => new SpouseTaxProfileRepository,
			'user_dependant' => new UserDependantRepository,
			'klu' => new KlasifikasiLapanganUsahaRepository,
			'user' => new UserRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$spouseTaxProfile = $this->repositories['spouse_tax_profile']->getModel();
		$userDependant = $this->repositories['user_dependant']->getModel();
		$klu = $this->repositories['klu']->getModel();
		$user = $this->repositories['user']->getModel();

		$userProfile = $user->where('id', '=', $userId)->first();
		$gender = $userProfile['gender'];
		if($gender == 'L')
			$dependantRelationId = 2;
		else
			$dependantRelationId = 1;

		$existingProfile = $spouseTaxProfile->where('user_id', '=', $userId)->first();
		if ($existingProfile) {
			$spouseTaxProfile = $existingProfile;
		}

		$existingDependant = $userDependant->where('dependant_relation_id', '=' ,1)->orWhere('dependant_relation_id', '=' ,2)->first();
		if($existingDependant) {
			$userDependant = $existingDependant;
		}

		$spouseTaxProfile->user_id = $userId;
		$spouseTaxProfile->name = $data['name'];
		$spouseTaxProfile->nik = isset($data['nik']) ? $data['nik'] : '';
		$spouseTaxProfile->job_status_id = $data['jobstatus'];

		$dependantJob = null;
		
		if(isset($data['klu'])){
			$spouseTaxProfile->klu_id = $data['klu'];
			$kluDetail = $klu->where('id', '=', $data['klu'])->first();
			$kluName = $kluDetail['name'];
		} else {
			$spouseTaxProfile->klu_id = null;
			$kluName = 'Tidak Bekerja/Pensiun';
		}

		$userDependant->user_id = $userId;
		$userDependant->dependant_relation_id = $dependantRelationId;
		$userDependant->ktp_number = isset($data['nik']) ? $data['nik'] : '';
		$userDependant->name = $data['name'];
		$userDependant->job = $kluName;

		if($spouseTaxProfile->save()  && $userDependant->save()) {

			\Session::put('my_husband',1);

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection($userId, $step, $data);
			return $spouseTaxProfile;
		}

		return false;
	}

	public function parseDirection($userId, $step, $data){
		$userTaxProfile = $this->repositories['user_tax_profile']->getWhere('user_id', $userId)->toArray()[0];
		$userProfile = $this->repositories['user']->getModel()->where('id', $userId)->first()->toArray();
		//$userProfile = empty($userProfile) ? [] : $userProfile->toArray();
		$taxFillingStep = $this->repositories['user_step']->getWhere('user_id', $userId)->toArray()[0];

		$tempStep = is_null($taxFillingStep['temp_steps']) ? [] : json_decode($taxFillingStep['temp_steps'], true);
		$keys = [10, 11, 12, 13];
		$rebuildStep = [];

		foreach ($keys as $key){
			$search = array_search($key, $tempStep);
			if ($search !== false) unset($tempStep[$search]);
		}
		$tempStep = array_values($tempStep);

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillingStep['id']);
		$updateStep->temp_steps = json_encode($tempStep);
		$updateStep->save();
	}
}