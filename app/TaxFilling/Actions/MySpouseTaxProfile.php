<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;
use Api\UserDependants\Repositories\UserDependantRepository;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class MySpouseTaxProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'spouse_tax_profile' => new SpouseTaxProfileRepository,
			'user_dependant' => new UserDependantRepository,
			'klu' => new KlasifikasiLapanganUsahaRepository,
			'user' => Auth::user(),
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		// $spouseTaxProfile = $this->repositories['spouse_tax_profile']->getModel();
		// $userDependant = $this->repositories['user_dependant']->getModel();
		// $klu = $this->repositories['klu']->getModel();
		// $user = $this->repositories['user']->getModel();

		$spouseTaxProfile = $this->repositories['spouse_tax_profile']->getModel();
		$existingSpouseTaxProfile = $spouseTaxProfile->where('user_id', $userId)->first();
		if ($existingSpouseTaxProfile){
			$spouseTaxProfile = $existingSpouseTaxProfile;
		}

		$spouseTaxProfile->user_id = $userId;
		$spouseTaxProfile->npwp = isset($data['npwp']) ? $data['npwp'] : '';
		$spouseTaxProfile->nik = '';
		$spouseTaxProfile->job_status_id = $data['jobstatus'];
		$spouseTaxProfile->klu_id = $data['klu'];
		$spouseTaxProfile->name = $data['name'];
		if($spouseTaxProfile->save()) {
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			return $spouseTaxProfile;
		}

		return false;
	}
}
