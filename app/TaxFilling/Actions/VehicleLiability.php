<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class VehicleLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['liability_id']); $i++){
				if (!empty($data['vehicle-number'][$i])){
					if ($data['liability_id'][$i] == true){
						$userLiability = $this->repositories['user_liability']->getById($data['liability_id'][$i]);
					}else{
						$userLiability = $this->repositories['user_liability']->getModel();
					}

					$date_split = explode('/', $data['paid-off'][$i]);

					$detail = [
						'currency' 					=> $data['currency'][$i],
						'country' 					=> $data['country'][$i],
						'currency_value' 			=> $data['credite-value'][$i],
						'kurs_value' 				=> $data['kurs_pajak'][$i],
						'vehicle_id'				=> $data['vehicle-number'][$i],
						'vehicle_merk'				=> $data['vehicle-merk'][$i],
						'credite_duration'			=> $data['credit-duration'][$i],
						'paid_off_date'				=> $date_split[2].'-'.$date_split[1].'-'.$date_split[0],
						'yearly_interest'			=> $data['yearly-interest'][$i],
						'credite_type'				=> 'vehicle'
					];

					$userLiability->user_id = $userId;
					$userLiability->liability_id = 1;
					$userLiability->acquisition_year = (int) $data['acquisition-year'][$i];
					$userLiability->value = (int) $data['jumlah_rupiah'][$i];
					$userLiability->lender_name = $data['lender-name'][$i];
					$userLiability->lender_address = $data['lender-address'][$i];
					$userLiability->detail = json_encode($detail);
					$userLiability->save();
				}
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;
		// $error = 0;

		// foreach($data['vehicle-liability-id'] as $liabilityId){
		// 	$userLiability = $this->repositories['user_liability']->getModel();
		// 	$existingLiability = $userLiability->where('id', '=', $liabilityId)->first();

		// 	if ($existingLiability) {
		// 		$userLiability = $existingLiability;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userLiability->user_id = $userId;
		// 		$userLiability->liability_id = 1;
		// 		$userLiability->acquisition_year = (integer) $data['vehicle-acquisition-year'][$i];
		// 		$userLiability->value = (double) $data['hidden-vehicle-liability-value'][$i];
		// 		$userLiability->lender_name = $data['vehicle-liability-bank-name'][$i];
		// 		$userLiability->lender_address = $data['vehicle-liability-bank-address'][$i];

		// 		if(!isset($data['vehicle-liability-country'][$i])){
		// 			$data['vehicle-liability-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'vehicle_liability_country' => (integer) $data['vehicle-liability-country'][$i],
		// 			'vehicle_liability_currency' => (integer) $data['vehicle-liability-currency'][$i],
		// 			'vehicle_liability_foreign_value' => (double) $data['vehicle-liability-foreign-value'][$i],
		// 			'vehicle_liability_bank_name' => $data['vehicle-liability-bank-name'][$i],
		// 			'vehicle_liability_bank_address' => $data['vehicle-liability-bank-address'][$i],
		// 			'police_number' => $data['vehicle-asset-id'][$i]
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userLiability->detail = $encodeDetail;
		// 		$userLiability->save();

		// 		$i++;
		// 	} else {
		// 		$userLiability->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userLiability;
	}
}
