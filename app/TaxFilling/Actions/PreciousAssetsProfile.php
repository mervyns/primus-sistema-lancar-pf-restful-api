<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class PreciousAssetsProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$i = 0;
		$error = 0;

		$additional_profile = $this->repositories['user']->additionalProfile()->first();

		if(!empty($additional_profile)){
			$details = (json_decode($additional_profile->detail,true));
			if(!empty($details['precious_assets_profile']) && $details['precious_assets_profile'] != $data['precious-asset']){
				$this->repositories['user']->asset()->whereIn('asset_id', [22, 23, 24])->delete();
			}
		}

		$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
		$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();

		if ($existingProfile) {
			$userAdditionalProfile = $existingProfile;
		}

		$userAdditionalProfile->user_id = $userId;

		$decodeDetail = json_decode($userAdditionalProfile->detail, true);
		$newDetailRecord = [
			'precious_assets_profile' => $data['precious-asset']
		];

		if(is_null($decodeDetail)){
			$mergedDetail = $newDetailRecord;
		} else{
			$mergedDetail = array_merge($decodeDetail, $newDetailRecord);
		}

		$encodeDetail = json_encode($mergedDetail);

		$userAdditionalProfile->user_id = $userId;
		$userAdditionalProfile->detail = $encodeDetail;
		$userAdditionalProfile->save();

		$userStep = $this->repositories['user_step'];
		$userStep->saveOrUpdate($userId, [
			'current_step' => $step
		]);
		
		$prev_url = str_replace(url('/'), '', url()->previous());
		$prev_url_piece = explode('/',$prev_url);
		
		if($prev_url_piece[1] == 'tax-filling-final-steps'){
			$next_page = 0;
			if($data['precious-asset'] == 'Pelaporan Global'){
				$next_page = 26;
			}else{
				$next_page = 27;
			}
			\Session::put('precious_assets_next',$next_page);
		}

		$this->parseDirection($step, $data['precious-asset']);

		return $userAdditionalProfile;
	}

	public function parseDirection($step, $param){
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);

		$globalKey = array_search(26, $tempStep);
		$detailKey = array_search(27, $tempStep);

		if ($globalKey !== false) unset($tempStep[$globalKey]);
		if ($detailKey !== false) unset($tempStep[$detailKey]);

		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				if ($param == 'Pelaporan Global'){
					$rebuildStep[] = 26;
				}else if ($param == 'Pelaporan Rinci'){
					$rebuildStep[] = 27;
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();
		
	}

}
