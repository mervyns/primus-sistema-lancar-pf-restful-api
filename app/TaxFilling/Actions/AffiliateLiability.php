<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class AffiliateLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['liability-id']); $i++){
				$id = $data['liability-id'][$i];
                
				if (!empty($id)){
					$userLiability = $this->repositories['user_liability']->getById($id);
				}else{
					$userLiability = $this->repositories['user_liability']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'lender_npwp'				=> $data['lender-npwp'][$i]
				];

				$userLiability->user_id = $userId;
				$userLiability->liability_id = 3;
				$userLiability->acquisition_year = $data['acquisition-year'][$i];
				$userLiability->value = $data['jumlah_rupiah'][$i];
				$userLiability->lender_name = $data['lender-name'][$i];
				$userLiability->lender_address = '';
				$userLiability->detail = json_encode($detail);
				$userLiability->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		// $i = 0;

		// foreach ($data['affliability-id'] as $assetId){
		// 	$userLiability = $this->repositories['user_liability']->getModel();
		// 	$existingAsset = $userLiability->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userLiability = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userLiability->user_id = $userId;
		// 		$userLiability->liability_id = 3;
		// 		$userLiability->acquisition_year = $data['affliability-acquisition-year'][$i];
		// 		$userLiability->value = (double) $data['hidden-affliability-value'][$i];
		// 		$userLiability->lender_name = $data['affliability-companies'][$i];
		// 		$userLiability->lender_address = $data['affliability-companies-address'][$i];

		// 		if(!isset($data['affliability-country'][$i])){
		// 			$data['affliability-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'affliability_country' => (integer) $data['affliability-country'][$i],
		// 			'affliability_currency' => (integer) $data['affliability-currency'][$i],
		// 			'affliability_foreign_value' => (double) $data['affliability-foreign-value'][$i],
		// 			'affliability_companies' => $data['affliability-companies'][$i],
		// 			'affliability_companies_npwp' => $data['affliability-companies-npwp'][$i],
		// 			'affliability_companies_address' => $data['affliability-companies-address'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userLiability->detail = $encodeDetail;
		// 		$userLiability->save();

		// 		$i++;
		// 	} else {
		// 		$userLiability->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userLiability;
	}
}
