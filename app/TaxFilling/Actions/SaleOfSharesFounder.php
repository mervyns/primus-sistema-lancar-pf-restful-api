<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class SaleOfSharesFounder {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['SALE_OF_SHARES_FOUNDER'];
          
            empty($data['stock']) ? null : $data['stock'] = array_values($data['stock']);
            empty($data['price']) ? null : $data['price'] = array_values($data['price']);
            empty($data['profit']) ? null : $data['profit'] = array_values($data['profit']);
            empty($data['corporation']) ? null : $data['corporation'] = array_values($data['corporation']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
            
            if(!empty($data['profit'])){
                foreach ($data['profit'] as $e_key => $value){

                    if(!empty($data['id'][$e_key])){
                        $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                
                        $user_income->net_value = $value;
                        $user_income->gross_value = $data['price'][$e_key];
                        $user_income->detail = json_encode([
                            'perusahaan' => $data['corporation'][$e_key]
                        ]);
                        
                    }else{

                        $user_income = $this->repositories['user_income']->getModel();
                        $user_income->user_id = $userId;
                        $user_income->income_id = 27;
                        $user_income->country_id = 1;
                        $user_income->net_value = $value;
                        $user_income->gross_value = $data['price'][$e_key];
                        $user_income->detail = json_encode([
                            'perusahaan' => $data['corporation'][$e_key]
                        ]);

                    }
                    $user_income->save();
                }
                $result = true;
            }

            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            // $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            return false;
        }
        
	}
}
