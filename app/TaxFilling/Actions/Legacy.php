<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class Legacy {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        $result = false;
        $self_step_index = config('constants')['steps']['LEGACY'];
        empty($data['type']) ? $types = null : $types = array_values($data['type']);        
        empty($data['nilai']) ? $values = null : $values = array_values($data['nilai']);        
        empty($data['pemberi']) ? $givers = null : $givers = array_values($data['pemberi']);        
        empty($data['id']) ? $ids = null : $ids = array_values($data['id']);        
        
        foreach($types as $t_key => $type){
            
            if(!empty($ids[$t_key])){
                $user_income = $this->repositories['user_income']->getModel()->find($ids[$t_key]);
        
                $user_income->income_id = $type;
                $user_income->country_id = 1;
                $user_income->net_value = $values[$t_key];
                $user_income->gross_value = $values[$t_key];
                $user_income->detail = json_encode([
                    'pemberi' => $givers[$t_key]
                ]);
            }else{
                $user_income = $this->repositories['user_income']->getModel();
           
                $user_income->user_id = $userId;
                $user_income->income_id = $type;
                $user_income->country_id = 1;
                $user_income->net_value = $values[$t_key];
                $user_income->gross_value = $values[$t_key];
                $user_income->detail = json_encode([
                    'pemberi' => $givers[$t_key]
                ]);
            }

            $user_income->save();

        }
       
        $userStep = $this->repositories['user_step'];
        $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
        
        $steps = new UserTaxFillingStepRepository();
        $steps = $steps->setDirection($userId,$self_step_index);

        $result = true;
       
        return $result;
        
	}
}
