<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class DividendFromInvestation {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
            for ($i = 0; $i < count($data['income_id']); $i++){
                if (isset($data['income_id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income_id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }

                $detail = [
                    'tax_rate'              => $data['tax_rate'][$i],
                    'asset_id'              => $data['asset_id'][$i],
                    'company_name'          => $data['deviden-devide'][$i],
                    'tax_final'             => $data['tax-final'][$i],
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 3;
                $userIncome->country_id = $data['country_id'][$i];
                $userIncome->net_value = $data['deviden-netto'][$i];
                $userIncome->gross_value = $data['deviden-bruto'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->user_asset_id = $data['from-stock'][$i];
                $userIncome->save();
            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
        }catch (\Exception $ex){
            dd($ex->getMessage());
            //return false;
        }

	}
}