<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class InsuranceClaimProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        
        try{
            $action = $data['Action'];

            $taxFillStep = Auth::user()->step;
            $tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);

            $rebuildStep = [];

            $search = array_search(35, $tempStep);
            if ($search === false){
                if ($action == "Ya"){
                    foreach ($tempStep as $key => $row){
                        $rebuildStep[] = $row;
                        if ($row == 34){
                            $rebuildStep[] = 35;
                        }
                    }
                }
            }else{
                if ($action == "Tidak"){
                    unset($tempStep[$search]);
                    $rebuildStep = array_values($tempStep);
                }
            }

            if (count($rebuildStep) == 0){
                $rebuildStep = $tempStep;
            }
            
            $updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
            $updateStep->temp_steps = json_encode($rebuildStep);
			if ($updateStep->save()){
                $userStep = $this->repositories['user_step'];
                $userStep->saveOrUpdate($userId, [
                    'current_step' => $step
                ]);
                return $updateStep;
            }

        } catch (\Exception $ex){
            return false;
        }
        
    }
}