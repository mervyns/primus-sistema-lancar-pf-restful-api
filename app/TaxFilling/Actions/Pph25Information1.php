<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;

class Pph25Information1 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
			$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();

			$newDetail = [];
			
			if ($existingProfile){
				$newDetail = json_decode($existingProfile['detail'], true);
				$userAdditionalProfile = $existingProfile;
			}

			$newDetail['pph25_yearly'] = $data['yearly'];
			$newDetail['pph25_monthly'] = $data['monthly'];

			$userAdditionalProfile->user_id = $userId;
			$userAdditionalProfile->detail = json_encode($newDetail);
			$userAdditionalProfile->save();
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, ['current_step' => $step]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

	}
}
