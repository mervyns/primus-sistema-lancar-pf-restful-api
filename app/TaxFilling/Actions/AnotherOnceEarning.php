<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\Incomes\Repositories\IncomeRepository;

class AnotherOnceEarning {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository,
            'income' => new IncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        empty($data['brutto']) ? null : $data['brutto'] = array_values($data['brutto']);
        empty($data['cutter-name']) ? null : $data['cutter-name'] = array_values($data['cutter-name']);
        empty($data['netto']) ? null : $data['netto'] = array_values($data['netto']);
        empty($data['cutter-npwp']) ? null : $data['cutter-npwp'] = array_values($data['cutter-npwp']);
        empty($data['pph-proof']) ? null : $data['pph-proof'] = array_values($data['pph-proof']);
        empty($data['pph-date']) ? null : $data['pph-date'] = array_values($data['pph-date']);
        empty($data['pasal-pph-value']) ? null : $data['pasal-pph-value'] = array_values($data['pasal-pph-value']);
        empty($data['pph-taken']) ? null : $data['pph-taken'] = array_values($data['pph-taken']);
        empty($data['earning_type']) ? null : $data['earning_type'] = array_values($data['earning_type']);
        empty($data['tarif']) ? null : $data['tarif'] = array_values($data['tarif']);
        empty($data['type']) ? null : $data['type'] = array_values($data['type']);
        empty($data['id']) ? null : $data['id'] = array_values($data['id']);
        return true;
        dd($data);
        DB::transaction(function () use($userId, $step, $data) {
            for ($i = 0; count($data['id']); $i++) {
                $userWitholdingTax = null;
                
            }
        });
        /*
        try{
            // dd($data);
            $result = false;
            $self_step_index = config('constants')['steps']['ANOTHER_ONCE_EARNING'];
           
            empty($data['brutto']) ? null : $data['brutto'] = array_values($data['brutto']);
            empty($data['cutter-name']) ? null : $data['cutter-name'] = array_values($data['cutter-name']);
            empty($data['netto']) ? null : $data['netto'] = array_values($data['netto']);
            empty($data['cutter-npwp']) ? null : $data['cutter-npwp'] = array_values($data['cutter-npwp']);
            empty($data['pph-proof']) ? null : $data['pph-proof'] = array_values($data['pph-proof']);
            empty($data['pph-date']) ? null : $data['pph-date'] = array_values($data['pph-date']);
            empty($data['pph-type']) ? null : P$data['pph-type'] = array_values($data['pph-type']);
            empty($data['pph-taken']) ? null : $data['pph-taken'] = array_values($data['pph-taken']);
            empty($data['earning_type']) ? null : $data['earning_type'] = array_values($data['earning_type']);
            empty($data['tarif']) ? null : $data['tarif'] = array_values($data['tarif']);
            empty($data['type']) ? null : $data['type'] = array_values($data['type']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
           
            DB::transaction(function () use ($userId, $step, $data) {
                foreach ($data['brutto'] as $e_key => $element){
            
                    if(!empty($data['id'][$e_key])){

                        if(!empty($data['netto'][$e_key])){
                            $type = null;
                    
                            $brutto = $data['brutto'][$e_key];
                        
                            $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
            
                            $user_income->net_value = $data['netto'][$e_key];
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'nama_pemotong' => $data['cutter-name'][$e_key],
                                'npwp_pemotong' => $data['cutter-npwp'][$e_key],
                                'bukti_potong' => $data['pph-proof'][$e_key],
                                'tanggal_potong' => $data['pph-date'][$e_key],
                                'tipe_pph' => $data['earning_type'][$e_key],
                                'pasal_pph' => $data['pph-type'][$e_key],
                                'nilai_pph' => $data['pph-taken'][$e_key],
                                'dipotong_pajak' => $data['type'][$e_key]
                            ]);
                           
                        }else{
                            $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);

                            $user_income->net_value = $element;
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'tipe_pph' => $data['earning_type'][$e_key],
                                'dipotong_pajak' => $data['type'][$e_key]
                            ]);
                           
                        }

                    }else{

                        if(!empty($data['netto'][$e_key])){
                            $type = null;
                    
                            $brutto = $data['brutto'][$e_key];
                        
                            $user_income = $this->repositories['user_income']->getModel();
                            $user_income->user_id = $userId;
                            $user_income->income_id = 55;
                            $user_income->country_id = 1;
                            $user_income->net_value = $data['netto'][$e_key];
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'nama_pemotong' => $data['cutter-name'][$e_key],
                                'npwp_pemotong' => $data['cutter-npwp'][$e_key],
                                'bukti_potong' => $data['pph-proof'][$e_key],
                                'tanggal_potong' => $data['pph-date'][$e_key],
                                'tipe_pph' => $data['earning_type'][$e_key],
                                'pasal_pph' => $data['pph-type'][$e_key],
                                'nilai_pph' => $data['pph-taken'][$e_key],
                                'dipotong_pajak' => $data['type'][$e_key]
                            ]);
                           
                        }else{

                            $user_income = $this->repositories['user_income']->getModel();
                            $user_income->user_id = $userId;
                            $user_income->income_id = 55;
                            $user_income->country_id = 1;
                            // $user_income->net_value = $element;
                            $user_income->gross_value = $element;
                            $user_income->detail = json_encode([
                                'tipe_pph' => $data['earning_type'][$e_key],
                                'dipotong_pajak' => $data['type'][$e_key]
                            ]);
                           
                        }

                    }
                    $user_income->save();
                    
                }
            });

            $result = true;
        
            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            dd($e);
            return false;
        }
        */
	}
}
