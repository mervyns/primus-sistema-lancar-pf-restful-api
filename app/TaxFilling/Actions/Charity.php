<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Charity {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            for ($i = 0; $i < count($data['charity-id']); $i++){
                if (!empty($data['charity-id'][$i])){
                    $userIncome = $this->repositories['user_income']->getById($data['charity-id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }

                $detail = [
                    'charity_id'            => $data['charity-name'][$i],
                    'other_charity'         => is_null($data['other-charity'][$i]) ? '' : $data['other-charity'][$i]
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 53;
                $userIncome->country_id = 1;
                $userIncome->net_value = $data['charity-value'][$i];
                $userIncome->gross_value = 0;
                $userIncome->detail = json_encode($detail);
                $userIncome->save();

            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }

            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
            return true;
        }catch(\Exception $ex){
            return false;
        }
	}
}