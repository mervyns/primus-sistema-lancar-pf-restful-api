<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Cash {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				if (isset($data['asset-id'][$i]) == true){
					$userAsset = $this->repositories['user_asset']->getById($data['asset-id'][$i]);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
							'currency' => $data['currency'][$i],
							'currency_value' => $data['cash'][$i],
							'kurs_value' => $data['kurs_pajak'][$i]
						];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 1;
				$userAsset->acquisition_year = date("Y");
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			
			return true;
		}catch (\Exception $ex){
			return false;
		}
		// $data = $this->_buildData($data);
		// foreach ($data as $assetId => $assetData){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}
			
		// 	if ( $assetData['currency'] &&
		// 			$assetData['cash'] &&
		// 			is_int($assetData['cash']) ) {
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id = 1;
		// 		$userAsset->acquisition_year = date("Y");
		// 		$userAsset->idr_value = $assetData['cash'];

		// 		$detail = [ 'currency' => $assetData['currency'] ];
		// 		$encodeDetail = json_encode($detail);

		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();
		// 	}
		// }

		// if (isset($data['deleted_id']) == true){
		// 	for ($i = 0; $i < count($data['deleted_id']); $i++){
		// 		$remove_cash = $this->repositories['user_asset']->delete($data['deleted_id'][$i]);
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}

	private function _buildData($data)
	{
		$totalData = count($data['cash']);

		if ($totalData === 0) {
			return [];
		}

		$procData = [];
		for ($i=0; $i < $totalData; $i++) {
			$assetId = intval($data['asset-id'][$i]) ? $data['asset-id'][$i] : 'new_asset-'.$i;
			$procData[$assetId] = [
				'cash' => intval($data['cash'][$i]),
				'currency' => $data['currency'][$i],
				'deleted' => (bool) $data['deleted'][$i],
			];
		}

		return $procData;
	}
}
