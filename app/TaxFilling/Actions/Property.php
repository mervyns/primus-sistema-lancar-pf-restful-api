<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Property {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$credit = [];
		foreach ($data['property-credit'] as $row){
			$credit[] = $row;
		}
		try{
			$parse = false;
			for ($i = 0; $i < count($data['asset-id']); $i++)
			{
				if ($credit[$i] == "Ya") $parse = true;

				if (isset($data['asset-id'][$i]) == true){
					$userAsset = $this->repositories['user_asset']->getById($data['asset-id'][$i]);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
					'currency' 					=> $data['currency'][$i],
					'country' 					=> $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'credite_type'				=> $credit[$i],
					'property_type'				=> $data['property-type'][$i],
					'certificate_type'			=> $data['certificate-type'][$i],
					'other_certificate_type'	=> $data['other-certificate-type'][$i],
					'location'					=> $data['location'][$i],
					'city'						=> $data['city'][$i],
					'optional_property_type'	=> $data['optional-property-type'][$i],
					'property_large'			=> $data['property-large'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = $data['property-function'][$i];
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();

			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			$this->parseDirection($parse, $step);
			return true;

		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		// $i = 0;
		// $error = 0;

		// foreach($data['property-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;

		// 		$propType = $data['property-type'][$i];
		// 		$propFunction = $data['property-function'][$i];
		// 		$propertyTypeId = 0;
		// 		if($propType == 'Tanah' && $propFunction == 'Untuk Tempat Tinggal'){
		// 			$propertyTypeId = 28;
		// 		} elseif ($propType == 'Tanah' && $propFunction == 'Untuk Usaha') {
		// 			$propertyTypeId = 30;
		// 		} elseif ($propType == 'Tanah dan/atau Bangunan' && $propFunction == 'Untuk Tempat Tinggal') {
		// 			$propertyTypeId = 28;
		// 		} elseif ($propType == 'Tanah dan/atau Bangunan' && $propFunction == 'Untuk Usaha') {
		// 			$propertyTypeId = 29;
		// 		} elseif ($propType == 'Apartemen' && $propFunction == 'Untuk Tempat Tinggal') {
		// 			$propertyTypeId = 28;
		// 		}

		// 		$userAsset->asset_id = $propertyTypeId;
		// 		$userAsset->acquisition_year = $data['property-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-property-value'][$i];

		// 		if(!isset($data['property-country'][$i])){
		// 			$data['property-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'property_type' => $data['property-type'][$i],
		// 			'property_function' => $data['property-function'][$i],
		// 			'property_country' => (integer) $data['property-country'][$i],
		// 			'property_currency' => (integer) $data['property-currency'][$i],
		// 			'property_foreign_value' => (double) $data['property-foreign-value'][$i],
		// 			'property_certificate_type' => $data['property-certificate-type'][$i],
		// 			'property_certificate_number' => $data['property-certificate-number'][$i],
		// 			'property_address' => $data['property-address'][$i],
		// 			'sppt_number' => $data['sppt-number'][$i],
		// 			'property_area' => (double) $data['property-area'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAsset;
	}
	public function parseDirection($parse, $step){
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		$arrayKey = array_search(24, $tempStep);
		if ($arrayKey !== false) unset($tempStep[$arrayKey]);
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		if ($parse == true){
			foreach ($tempStep as $val => $row){
				$rebuildStep[] = $row;
				if ($row == $step) $rebuildStep[] = 24;
			}
		}else{
			$rebuildStep = $tempStep;
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

		
	}
}
