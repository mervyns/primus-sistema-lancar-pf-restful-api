<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Auth;

use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\System\Repositories\SystemStepConstantRepository;

class AdditionalEarningOneTime {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'system_constant' => new SystemStepConstantRepository,
            'user_additional_profile' => new UserAdditionalProfileRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
            $existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();
            
            $newDetail = [];
            
            if ($existingProfile){
                $newDetail = json_decode($existingProfile['detail'], true);
                $userAdditionalProfile = $existingProfile;
            }

            $checkboxes = [];
            foreach($data as $d_key => $value){
                if(preg_match('/\d/',$d_key) > 0){
                    $checkboxes[$d_key] = $d_key;
                }
            }

            $build = array (
                'warisan' => (bool)isset($checkboxes[71]),
                'pesangon_dan_tunjangan' => (bool)isset($checkboxes[74]),
                'beasiswa' => (bool)isset($checkboxes[76]),
                'pengalihan_hak_tanah_bangunan' => (bool)isset($checkboxes[72]),
                'keuntungan_pengalihan_selain_tanah_bangunan' => (bool)isset($checkboxes[73]),
                'penjualan_saham_pendiri' => (bool)isset($checkboxes[77]),
                'hadiah_undian' => (bool)isset($checkboxes[75]),
                'penghargaan_dan_hadiah' => (bool)isset($checkboxes[81]),
                'sumbangan' => (bool)isset($checkboxes[82]),
                'honorarium_apbn_apbd' => (bool)isset($checkboxes[78]),
                'upah_pegawai_lepas' => (bool)isset($checkboxes[79]),
                'honorarium_komisaris' => (bool)isset($checkboxes[80]),
                'bangunan_guna_serah' => (bool)isset($checkboxes[83]),
                'sekali_terima_lainnya' => (bool)isset($checkboxes[84])
            );

            $result = array_merge($newDetail, $build);

            $userAdditionalProfile->user_id = $userId;
            $userAdditionalProfile->detail = json_encode($result);
            $userAdditionalProfile->save();
            
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [
                'current_step' => $step
            ]);
            $this->parseDirection($checkboxes, $step);
            return true;
        }catch(\Exception $ex){
            return false;
        }



        //DI KOMEN DULU YA KANG

        // try{
        //     $array_checkbox = [];
        //     $last_direction = 87;
        //     $result = false;

        //     $user = $this->repositories['user']->getModel()->where('id',$userId)->first();
        //     $system_constant = $this->repositories['system_constant']->getModel()->get();
            
        //     if(!empty($system_constant)){

        //         if($user->taxProfile->following_tax_amnesty_2016){
        //             $last_direction = 85;
        //         }
                
        //         $current_step = $system_constant->where('name','ADDITIONAL_EARNING_ONE_TIME')->first()->step;
        //         $choice_limit_step = $last_direction;
        //         // $choice_limit_step = $system_constant->where('name','TAX_AMNESTY_DISCLOSURE')->first()->step;
            
        //         $check_user = $this->repositories['user_step']->getModel();
        //         $user = $check_user->where('user_id',$userId)->first();

        //         if(!empty($user)){
        //             foreach($data as $d_key => $value){
        //                 if(preg_match('/\d/',$d_key) > 0){
        //                     $array_checkbox[$d_key] = $d_key;
        //                 }
        //             }

        //             $array_checkbox = array_merge($array_checkbox,[$last_direction => $last_direction]);
        //             $existing = json_decode($user->temp_steps);
                
        //             if(!empty($existing)){
        //                 $limit_index = null;
        //                 $this_step_index = array_search($current_step,$existing);
        //                 $choice_limit_step_index = array_search($choice_limit_step,$existing);
                      
        //                 if(!$choice_limit_step_index){
        //                     $limit_index = end($existing);
        //                     $limit_index = key($existing); 
        //                 }else{
        //                     $limit_index = $choice_limit_step_index;
        //                 }
                        
        //                 $length = $limit_index - $this_step_index;
        //                 $chopped = array_slice($existing, ($this_step_index + 1), $length);
        //                 $existing = array_diff($existing, $chopped);
        //                 $array_checkbox = array_merge($existing,$array_checkbox);
        //                 empty($array_checkbox) ? null : $array_checkbox = array_values($array_checkbox);
        //                 $user->temp_steps = empty($array_checkbox) ? $user->temp_steps : json_encode($array_checkbox);
        //             }
                   
        //             $user->save();
        //             $result = true;
        //         }
        //     }
        // }catch(\Exception $e){
        //     dd($e);
        // }
        
        // $userStep = $this->repositories['user_step'];
        // $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
       
		// return $result;
    }

    private function parseDirection($data, $step) {
        $common = [71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84];
        $taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
        $tempStep = array_values($tempStep);
        
		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				foreach ($data as $val){
                    $rebuildStep[] = (int)$val;
				}
			}
        }

        $updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();
    }
}
