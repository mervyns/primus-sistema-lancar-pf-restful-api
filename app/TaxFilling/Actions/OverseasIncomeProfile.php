<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class OverseasIncomeProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' => Auth::user(),
            'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        
        try{
            $action = $data['Action'];

            $taxProfile = Auth::user()->taxProfile;
            
            $taxFillStep = Auth::user()->step;
            $tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);

            $rebuildStep = [];

            $search = array_search(15, $tempStep);
            if ($search === false){
                if ($action == "Ya"){
                    foreach ($tempStep as $key => $row){
                        $rebuildStep[] = $row;
                        if ($row == 14){
                            $rebuildStep[] = 15;
                        }
                    }
                }
            }else{
                if ($action == "Tidak"){
                    unset($tempStep[$search]);
                    $rebuildStep = array_values($tempStep);
                }
            }

            if (count($rebuildStep) == 0){
                $rebuildStep = $tempStep;
            }

            // if (array_search(16, $rebuildStep) === false) $rebuildStep[] = 16;
            // if (array_search(17, $rebuildStep) === false) $rebuildStep[] = 17;
            // if (array_search(18, $rebuildStep) === false) $rebuildStep[] = 18;
            // if (array_search(28, $rebuildStep) === false) $rebuildStep[] = 28;
            // if (array_search(43, $rebuildStep) === false) $rebuildStep[] = 43;
            // if (array_search(53, $rebuildStep) === false) $rebuildStep[] = 53;
            // if (array_search(57, $rebuildStep) === false) $rebuildStep[] = 57;
            // if (array_search(70, $rebuildStep) === false) $rebuildStep[] = 70;

            if ($taxProfile->following_tax_amnesty_2016 == 1){
                if (array_search(85, $rebuildStep) === false) $rebuildStep[] = 85;
                if ($taxProfile->ta_disclosure){
                    if (array_search(86, $rebuildStep) === false) $rebuildStep[] = 86;
                }
            }

            // if (array_search(87, $rebuildStep) === false) $rebuildStep[] = 87;
            // if (array_search(88, $rebuildStep) === false) $rebuildStep[] = 88;
            // if (array_search(89, $rebuildStep) === false) $rebuildStep[] = 89;
            // if (array_search(91, $rebuildStep) === false) $rebuildStep[] = 91;
            // if (array_search(93, $rebuildStep) === false) $rebuildStep[] = 93;
            
            $updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
            $updateStep->temp_steps = json_encode($rebuildStep);
			if ($updateStep->save()){
                $userStep = $this->repositories['user_step'];
                $userStep->saveOrUpdate($userId, [
                    'current_step' => $step
                ]);
                return $updateStep;
            }

        } catch (\Exception $ex){
            return false;
        }
        
    }
}