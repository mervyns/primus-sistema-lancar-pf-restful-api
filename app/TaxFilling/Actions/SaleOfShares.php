<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class SaleOfShares {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$type = [];
		foreach ($data['founder'] as $row){
			$type[] = $row;
        }
        
        try{
            for ($i = 0; $i < count($data['income_id']); $i++){
                if (isset($data['income_id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income_id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }
                $detail = [
                    'founder'           => $type[$i],
					'tax_final' 	    => $data['tax-final'][$i],
                    'tax_rate'          => $data['tax_rate'][$i],
                    'company_name'      => $data['company-name'][$i],
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 5;
                $userIncome->country_id = 1;
                $userIncome->net_value = $data['net-value'][$i];
                $userIncome->gross_value = $data['gross-value'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->save();
            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
			return true;
        }catch (\Exception $ex){
            dd($ex->getMessage());
            return false;
        }

	}

}
