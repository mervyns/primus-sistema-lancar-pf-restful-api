<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class OtherLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['liability-id']); $i++){
				$id = $data['liability-id'][$i];
                
				if (!empty($id)){
					$userLiability = $this->repositories['user_liability']->getById($id);
				}else{
					$userLiability = $this->repositories['user_liability']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'city'						=> $data['lender-city'][$i]
				];

				$userLiability->user_id = $userId;
				$userLiability->liability_id = 4;
				$userLiability->acquisition_year = $data['acquisition-year'][$i];
				$userLiability->value = $data['jumlah_rupiah'][$i];
				$userLiability->lender_name = $data['lender-name'][$i];
				$userLiability->lender_address = $data['lender-address'][$i];
				$userLiability->detail = json_encode($detail);
				$userLiability->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
	}
}
