<?php
namespace WebApp\TaxFilling\Actions;
use Api\PassFinal\Repositories\PassFinalRepository;


class PassFinalContinued {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'passfinal' => new PassFinalRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		\Session::put('data_pass_final', json_encode($data));
		return response()->json(['data' =>  $data, 'step' => $step ]);
	}
}
