<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserRevealedAssets\Repositories\UserRevealedAssetRepository;

class TaxAmnestyDisclosure {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_revealed_asset' => new UserRevealedAssetRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        
        $result = false;
       
        if(!empty($data['revealed_params'])){
           
            $data['revealed_params'] = json_decode($data['revealed_params'],true);

            DB::transaction(function () use ($userId, $step, $data) {
                foreach ($data['revealed_params'] as $d_key => $data) {
                    $user_revealed_asset_model = $this->repositories['user_revealed_asset']->getModel();
                    $user_revealed_asset_model->user_id = $data['user_id'];
                    $user_revealed_asset_model->asset_id = $data['asset_id'];
                    $user_revealed_asset_model->country_id = $data['currency'];
                    $user_revealed_asset_model->value = $data['value'];
                    $user_revealed_asset_model->save();

                    if($user_revealed_asset_model){
                        $user_revealed_asset_model = $this->repositories['user_asset']->getModel();
                        $user_revealed_asset_model = $user_revealed_asset_model->find($data['asset_id']);
                        if(!empty($user_revealed_asset_model)){
                            $user_revealed_asset_model->is_ta = 1;
                            $user_revealed_asset_model->save();
                        }
                    }

                }
                $userStep = $this->repositories['user_step'];
		        $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            });

            $result = true;
      
        }
       
		return $result;
	}
}
