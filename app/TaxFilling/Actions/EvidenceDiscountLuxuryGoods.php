<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class EvidenceDiscountLuxuryGoods {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user_income' => new UserIncomeRepository,
            'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{  

        try{
            for ($i = 0; $i < count($data['income-id']); $i++){
                if (!empty($data['income-id'][$i])){
                    $userIncome = $this->repositories['user_income']->getById($data['income-id'][$i]);
                    $userWithholdingTax = $this->repositories['user_withholding_tax']->getModel()->where('user_income_id', $data['income-id'][$i])->first();
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                    $userWithholdingTax = $this->repositories['user_withholding_tax']->getModel();
                }
                
                $detail = [
                    'tax_rate' => $data['tax_rate'][$i]
                ];
                
                $userIncome->user_id = $userId;
                $userIncome->income_id = $data['income-name'][$i];
                $userIncome->country_id = 1;
                $userIncome->net_value = (float)$data['ppnbm'][$i] - (float)$data['tax-final'][$i];
                $userIncome->gross_value = $data['ppnbm'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->save();

                $slip_date = explode('/', $data['withholding-slip-date'][$i]);

                $userWithholdingTax->user_income_id = $userIncome->id;
                $userWithholdingTax->withholding_tax_slip_id = 6;
                $userWithholdingTax->withholder_name = $data['withholder-name'][$i];
                $userWithholdingTax->withholder_npwp = $data['withholder-npwp'][$i];
                $userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'][$i];
                $userWithholdingTax->withholding_slip_date = $slip_date[2].'-'.$slip_date[1].'-'.$slip_date[0];
                $userWithholdingTax->value = $data['tax-final'][$i];
                $userWithholdingTax->save();

            }

            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
                    $userWithholdingTax = $this->repositories['user_withholding_tax']
                        ->getWhere('user_income_id',  $data['deleted_id'][$i])->first();
                    if ($userWithholdingTax){
                        $this->repositories['user_withholding_tax']->deleteWhere('user_income_id', $data['deleted_id'][$i]);
                    }
                    $this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }

            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            return true;
        }catch (\Exception $ex){ 
            return false;
        }
	}

}
