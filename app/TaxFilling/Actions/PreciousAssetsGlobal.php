<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class PreciousAssetsGlobal {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-type']); $i++){
				
				if (is_null($data['acquisition-year'][$i])) continue;
				$asset = $data['asset-type'][$i];
				$id = $data['asset-id'][$i];

				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
					'currency' 					=> $data['currency'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = $asset;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		
		// $i = 0;
		// $error = 0;

		// //gold
		// $userAsset = $this->repositories['user_asset']->getModel();
		// $existingAsset = $userAsset->where('id', '=', $data['gold-id'])->first();

		// if ($existingAsset) {
		// 	$userAsset = $existingAsset;
		// }

		// $userAsset->user_id = $userId;
		// $userAsset->asset_id = 22;
		// $userAsset->acquisition_year = date("Y") - 1;
		// $userAsset->idr_value = (double) $data['gold-total-value'];

		// if(!isset($data['gold-country'][$i])){
		// 	$data['gold-country'][$i] = 1;
		// }

		// $detail = [
		// 	'gold_country' => (integer) $data['gold-country'],
		// ];
		// $encodeDetail = json_encode($detail);
		// $userAsset->detail = $encodeDetail;
		// $userAsset->save();

		// //jewel
		// $userAsset = $this->repositories['user_asset']->getModel();
		// $existingAsset = $userAsset->where('id', '=', $data['jewel-id'])->first();

		// if ($existingAsset) {
		// 	$userAsset = $existingAsset;
		// }

		// $userAsset->user_id = $userId;
		// $userAsset->asset_id = 23;
		// $userAsset->acquisition_year = date("Y") - 1;
		// $userAsset->idr_value = (double) $data['jewel-total-value'];

		// if(!isset($data['jewel-country'][$i])){
		// 	$data['jewel-country'][$i] = 1;
		// }

		// $detail = [
		// 	'jewel_country' => (integer) $data['jewel-country'],
		// ];
		// $encodeDetail = json_encode($detail);
		// $userAsset->detail = $encodeDetail;
		// $userAsset->save();

		// //antique
		// $userAsset = $this->repositories['user_asset']->getModel();
		// $existingAsset = $userAsset->where('id', '=', $data['antique-id'])->first();

		// if ($existingAsset) {
		// 	$userAsset = $existingAsset;
		// }

		// $userAsset->user_id = $userId;
		// $userAsset->asset_id = 24;
		// $userAsset->acquisition_year = date("Y") - 1;
		// $userAsset->idr_value = (double) $data['antique-total-value'];

		// if(!isset($data['antique-country'][$i])){
		// 	$data['antique-country'][$i] = 1;
		// }

		// $detail = [
		// 	'antique_country' => (integer) $data['antique-country'],
		// ];
		// $encodeDetail = json_encode($detail);
		// $userAsset->detail = $encodeDetail;
		// $userAsset->save();

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAsset;
	}
}
