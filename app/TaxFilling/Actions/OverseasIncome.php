<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class OverseasIncome {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user_income' => new UserIncomeRepository,
			'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        
        try{
            for ($i = 0; $i < count($data['income-id']); $i++){
                if (!empty($data['income-id'][$i])){
                    $userIncome = $this->repositories['user_income']->getModel()
						->where('id', '=', $data['income-id'][$i])
                        ->first();
                    $userWithholdingTax = $this->repositories['user_withholding_tax']->getModel()
						->where('user_income_id', '=', $data['income-id'][$i])
						->first();
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
					$userWithholdingTax = $this->repositories['user_withholding_tax']->getModel();
                }

                $detail = [
                    'currency'              => $data['currency'][$i],
                    'country'			    => $data['country'][$i],
                    'kurs_value' 		    => $data['kurs_pajak'][$i],
                    'currency_value' 	    => $data['jumlah_rupiah'][$i],
                    'income_type'           => $data['income-type'][$i],
                    'other_income_type'     => $data['other-income-type'][$i],
                ];

                $userIncome->user_id = $userId;
				$userIncome->income_id = 52;
				$userIncome->country_id = $data['country'][$i];
				$userIncome->net_value = $data['net-value'][$i];
                $userIncome->gross_value = $data['gross-value'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->save();
                
                $slip_date = explode('/', $data['withholding-slip-date'][$i]);

                $userWithholdingTax->user_income_id = $userIncome->id;
				$userWithholdingTax->withholding_tax_slip_id = 7;
				$userWithholdingTax->withholder_name = $data['withholder-name'][$i];
				$userWithholdingTax->withholder_npwp = '';
				$userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'][$i];
				$userWithholdingTax->withholding_slip_date = $slip_date[2].'-'.$slip_date[1].'-'.$slip_date[0];
				$userWithholdingTax->value = $data['tara-value'][$i];
				$userWithholdingTax->save();

            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$remove_tax = $this->repositories['user_withholding_tax']->deleteWhere('user_income_id', $data['deleted_id'][$i]);
					$remove_income = $this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, ['current_step' => $step]);
            
            return true;
        }catch (\Exception $ex){
            dd($ex->getMessage());
            return false;
        }
        
    }
}