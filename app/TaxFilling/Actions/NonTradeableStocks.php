<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class NonTradeableStocks {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];
                
				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
                    'kurs_value' 				=> $data['kurs_pajak'][$i],
					'serial_number'            	=> $data['serial-number'][$i],
					'description'				=> $data['description'][$i],
					'company_name'				=> $data['company'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 10;
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;

		// //dd($data);

		// foreach ($data['non-tradeable-stocks-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id = 10;
		// 		$userAsset->acquisition_year = $data['non-tradeable-stocks-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-non-tradeable-stocks-value'][$i];

		// 		if(!isset($data['non-tradeable-stocks-country'][$i])){
		// 			$data['non-tradeable-stocks-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'non_tradeable_stocks_country' => (integer) $data['non-tradeable-stocks-country'][$i],
		// 			'non_tradeable_stocks_currency' => (integer) $data['non-tradeable-stocks-currency'][$i],
		// 			'non_tradeable_stocks_foreign_value' => (double) $data['non-tradeable-stocks-foreign-value'][$i],
		// 			'non_tradeable_stocks_companies' => $data['non-tradeable-stocks-companies'][$i],
		// 			'non_tradeable_stocks_detail' => $data['non-tradeable-stocks-detail'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}
}
