<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class LeaseAssetOutsideBuildings {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user_income' => new UserIncomeRepository,
            'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{  
		$type = [];
		foreach ($data['taxes'] as $row){
			$type[] = $row;
        }
        
        try{
            for ($i = 0; $i < count($data['income_id']); $i++){
                $userWithholdingTax = null;
                if (isset($data['income_id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income_id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }
                $detail = [
                    'tax_rate'      => $data['tax_rate'][$i],
                    'taxes_type'    => $type[$i],
                    'asset_name'    => $data['asset-name'][$i],
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 8;
                $userIncome->country_id = 1;
                $userIncome->net_value = !empty($data['net-value'][$i]) ? $data['net-value'][$i] : 0;
                $userIncome->gross_value = !empty($data['gross-value'][$i]) ? $data['gross-value'][$i] : 0;
                $userIncome->detail = json_encode($detail);
                $userIncome->save();

                if ($type[$i] == 'Yes'){
                    if (isset($data['income_id'][$i]) == true){
                        $userWithholdingTax = $this->repositories['user_withholding_tax']
                            ->getWhere('user_income_id', $data['income_id'][$i])->first();
                    }else{
                        $userWithholdingTax = $this->repositories['user_withholding_tax']->getModel();
                    }

                    $slip_date = explode('/', $data['withholding-slip-date'][$i]);

                    $userWithholdingTax->user_income_id = (isset($data['income_id'][$i]) == true) ? $data['income_id'][$i] : $userIncome->id;
                    $userWithholdingTax->withholding_tax_slip_id = 4;
                    $userWithholdingTax->withholder_name = $data['withholder-name'][$i];
                    $userWithholdingTax->withholder_npwp = $data['withholder-npwp'][$i];
                    $userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'][$i];
                    $userWithholdingTax->withholding_slip_date = $slip_date[2].'-'.$slip_date[1].'-'.$slip_date[0];
                    $userWithholdingTax->value = !empty($data['tax-final'][$i]) ? $data['tax-final'][$i] : 0;
                    $userWithholdingTax->save();
                }else{
                    if (isset($data['income_id'][$i]) == true){
                        $userWithholdingTax = $this->repositories['user_withholding_tax']
                            ->getWhere('user_income_id', $data['income_id'][$i])->first();
                        if ($userWithholdingTax){
                            $this->repositories['user_withholding_tax']->deleteWhere('user_income_id', $data['income_id']);
                        }
                    }
                }
            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
                    $userWithholdingTax = $this->repositories['user_withholding_tax']
                        ->getWhere('user_income_id',  $data['deleted_id'][$i])->first();
                    if ($userWithholdingTax){
                        $this->repositories['user_withholding_tax']->deleteWhere('user_income_id', $data['deleted_id'][$i]);
                    }
                    $this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
			return true;
        }catch (\Exception $ex){
            dd($ex->getMessage());
            //return false;
        }

	}

}
