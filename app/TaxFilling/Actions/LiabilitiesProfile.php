<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class LiabilitiesProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if ($data['submit'] == 'skip'){
			for ($i = 0; $i < count($data['liability']); $i++){
				$data['liability'][$i] = null;
			}
		}
		try{
			$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
			$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();
			
			$newDetail = [];
			
			if ($existingProfile){
				$newDetail = json_decode($existingProfile['detail'], true);
				$userAdditionalProfile = $existingProfile;
			}

			$newDetail['financial_institution_liability'] = (bool)$data['liability'][0];
			$newDetail['affiliate_liability'] = (bool)$data['liability'][1];
			$newDetail['other_liability'] = (bool)$data['liability'][2];

			$userAdditionalProfile->user_id = $userId;
			$userAdditionalProfile->detail = json_encode($newDetail);
			$userAdditionalProfile->save();
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection($data['liability'], $step);
			return true;
		}catch(\Exception $ex){
			return false;
		}

		// $i = 0;
		// $error = 0;

		// $hutangBankNonBank = false;
		// $hutangAfiliasi = false;
		// $hutangLainnya = false;

		// if(!empty($data['liability-profile'])){
		// 	foreach($data['liability-profile'] as $liabilityProfile){
		// 		if($liabilityProfile == 1){
		// 			$hutangBankNonBank = true;
		// 		} elseif ($liabilityProfile == 3) {
		// 			$hutangAfiliasi = true;
		// 		} elseif ($liabilityProfile == 4) {
		// 			$hutangLainnya = true;
		// 		}
		// 	}
		// }

		// $userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
		// $existingProfile = $userAdditionalProfile->first();

		// if ($existingProfile) {
		// 	$userAdditionalProfile = $existingProfile;
		// }

		// $userAdditionalProfile->user_id = $userId;

		// $decodeDetail = json_decode($userAdditionalProfile->detail, true);
		// $newDetailRecord = [
		// 	'hutang_bank_non_bank' => $hutangBankNonBank,
		// 	'hutang_afiliasi' => $hutangAfiliasi,
		// 	'hutang_lainnya' => $hutangLainnya
		// ];

		// if(is_null($decodeDetail)){
		// 	$mergedDetail = $newDetailRecord;
		// } else{
		// 	$mergedDetail = array_merge($decodeDetail, $newDetailRecord);
		// }

		// $encodeDetail = json_encode($mergedDetail);

		// $userAdditionalProfile->detail = $encodeDetail;
		// $userAdditionalProfile->save();

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAdditionalProfile;
	}

	private function parseDirection($data, $step){
		$common = [54, 55, 56];
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				foreach ($data as $val){
					if ((bool)$val == true){
						$rebuildStep[] = (int)$val;
					}
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

	}

}
