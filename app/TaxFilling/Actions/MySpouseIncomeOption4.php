<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class MySpouseIncomeOption4 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			$check = $this->repositories['user_income']->getModel()
				->where('user_id', $userId)->where('income_id', 36)->first();
			if ($check){
				$userIncome = $this->repositories['user_income']->getById($check->id);
				$withHolding = $this->repositories['user_withholding_tax']->getModel()
					->where('user_income_id', $check->id)->first();
			}else{
				$userIncome = $this->repositories['user_income']->getModel();
				$withHolding = $this->repositories['user_withholding_tax']->getModel();
			}
			
			$domesticFromBusiness = isset($data['business-income']) ? $data['business-income'] : 0;
			$domesticFromJobs = isset($data['job-income']) ? $data['job-income'] : 0;
			$overseasIncome = isset($data['overseas-income']) ? $data['overseas-income'] : 0;
			$charity = isset($data['charity-value']) ? $data['charity-value'] : 0;
			$compensation = isset($data['compensation-value']) ? $data['compensation-value'] : 0;
			
			$totalIncome = (float)$domesticFromBusiness + (float)$domesticFromJobs + (float)$overseasIncome + (float)$compensation - (float)$charity;
			
            $detail = [
				'currency'							=> 1,
				'currency_value'					=> $totalIncome,
				'kurs_value'						=> 1,
                'domestic_income_from_business'     => $domesticFromBusiness,
                'domestic_income_from_jobs'         => $domesticFromJobs,
                'overseas_income'                   => $overseasIncome,
                'charity'                           => $charity,
                'compensation'                      => $compensation
			];
			
			$userIncome->user_id = $userId;
			$userIncome->income_id = 36;
			$userIncome->country_id = 1;
			$userIncome->net_value = $totalIncome;
            $userIncome->gross_value = 0;
            $userIncome->detail = json_encode($detail);
			$userIncome->save();

			$withHolding->user_income_id = $userIncome->id;
			$withHolding->withholding_tax_slip_id = ($data['klu-id'] == 1) ? 2 : 1;
			$withHolding->withholder_name = '';
			$withHolding->withholder_npwp = '';
			$withHolding->withholding_slip_number = '';
			$withHolding->withholding_slip_date = date("Y-m-d");
			$withHolding->value = 0;
			$withHolding->save();

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, ['current_step' => $step]);
			return true;
		}catch (\Exception $ex){
			//dd($ex->getMessage());
			return false;
		}
	}
}
