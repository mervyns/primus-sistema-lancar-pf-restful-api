<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class NonBuildingTransferProfit {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['NON_BUILDING_TRANSFER_PROFIT'];
       
            empty($data['trans_value']) ? null : $data['trans_value'] = array_values($data['trans_value']);
            empty($data['get_value']) ? null : $data['get_value'] = array_values($data['get_value']);
            empty($data['income_value']) ? null : $data['income_value'] = array_values($data['income_value']);
            empty($data['name']) ? null : $data['name'] = array_values($data['name']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
            
            if(!empty($data['trans_value'])){
                foreach ($data['trans_value'] as $e_key => $value){

                    if(!empty($data['id'][$e_key])){
                        $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                
                        $user_income->net_value = $data['income_value'][$e_key];
                        $user_income->gross_value = $value;
                        $user_income->detail = json_encode([
                            'nama' => $data['name'][$e_key],
                        ]);

                    }else{

                        $user_income = $this->repositories['user_income']->getModel();
                        $user_income->user_id = $userId;
                        $user_income->income_id = 34;
                        $user_income->country_id = 1;
                        $user_income->net_value = $data['income_value'][$e_key];
                        $user_income->gross_value = $value;
                        $user_income->detail = json_encode([
                            'nama' => $data['name'][$e_key],
                        ]);

                    }
                    $user_income->save();
                }
                $result = true;
            }

            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            return false;
        }
        
       
        return $result;
        
	}
}
