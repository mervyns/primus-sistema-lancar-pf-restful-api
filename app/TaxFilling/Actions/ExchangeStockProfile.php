<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class ExchangeStockProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$i = 0;
		$error = 0;

		$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
		$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();

		if ($existingProfile) {
			$userAdditionalProfile = $existingProfile;
		}

		$userAdditionalProfile->user_id = $userId;

		$decodeDetail = json_decode($userAdditionalProfile->detail, true);
		$newDetailRecord = [
			'exchange_stock_profile' => $data['precious-asset']
		];

		if(is_null($decodeDetail)){
			$mergedDetail = $newDetailRecord;
		} else{
			$mergedDetail = array_merge($decodeDetail, $newDetailRecord);
		}

		$encodeDetail = json_encode($mergedDetail);

		$userAdditionalProfile->user_id = $userId;
		$userAdditionalProfile->detail = $encodeDetail;
		$userAdditionalProfile->save();

		$userStep = $this->repositories['user_step'];
		$userStep->saveOrUpdate($userId, [
			'current_step' => $step
		]);

		$this->parseDirection($step, $data['precious-asset']);

		return $userAdditionalProfile;
	}

	public function parseDirection($step, $param){
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);

		$globalKey = array_search(30, $tempStep);
		$detailKey = array_search(31, $tempStep);

		if ($globalKey !== false) unset($tempStep[$globalKey]);
		if ($detailKey !== false) unset($tempStep[$detailKey]);

		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				if ($param == 'Pelaporan Global'){
					$rebuildStep[] = 30;
				}else if ($param == 'Pelaporan Rinci'){
					$rebuildStep[] = 31;
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();
		
	}

}
