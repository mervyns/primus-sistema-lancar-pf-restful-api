<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class AcceptedBuilding {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{

        $result = false;
        $self_step_index = config('constants')['steps']['ACCEPTED_BUILDING'];
        empty($data['njop']) ? $njop = null : $njop = array_values($data['njop']);        
        empty($data['address']) ? $address = null : $address = array_values($data['address']);        
        empty($data['final']) ? $final = null : $final = array_values($data['final']);  
        empty($data['id']) ? $ids = null : $ids = array_values($data['id']);        
       
        foreach($njop as $n_key => $njop_item){

            if(!empty($ids[$n_key])){
                        
                $user_income = $this->repositories['user_income']->getModel()->find($ids[$n_key]);
                $user_income->gross_value = $njop_item;
                $user_income->net_value = $njop_item;
                $user_income->detail = json_encode([
                    'alamat' => $address[$n_key],
                    'pajak_final' => $final[$n_key],
                ]);
                
            }else{
                $user_income = $this->repositories['user_income']->getModel();

                // $user_income = $user_income->firstOrNew(
                //     ['user_id' => $userId],
                //     ['income_id' => $type]
                // );
            
                $user_income->user_id = $userId;
                $user_income->income_id = 35;
                $user_income->country_id = 1;
                $user_income->gross_value = $njop_item;
                $user_income->net_value = $njop_item;
                $user_income->detail = json_encode([
                    'alamat' => $address[$n_key],
                    'pajak_final' => $final[$n_key],
                ]);
            }
            
        
            $user_income->save();

        }
       
        $userStep = $this->repositories['user_step'];
        $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
        
        $steps = new UserTaxFillingStepRepository();
        $steps = $steps->setDirection($userId,$self_step_index);

        $result = true;
       
        return $result;
        
	}
}
