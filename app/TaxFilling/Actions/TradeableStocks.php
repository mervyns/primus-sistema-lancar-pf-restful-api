<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class TradeableStocks {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$i = 0;

		foreach ($data['tradeable-stocks-id'] as $assetId){
			$userAsset = $this->repositories['user_asset']->getModel();
			$existingAsset = $userAsset->where('id', '=', $assetId)->first();

			if ($existingAsset) {
				$userAsset = $existingAsset;
			}

			if($data['deleted'][$i] != 'true'){
				$userAsset->user_id = $userId;
				$userAsset->asset_id = 9;
				$userAsset->acquisition_year = $data['tradeable-stocks-acquisition-year'][$i];
				$userAsset->idr_value = (double) $data['hidden-tradeable-stocks-value'][$i];

				if(!isset($data['tradeable-stocks-country'][$i])){
					$data['tradeable-stocks-country'][$i] = 1;
				}

				$detail = [
					'tradeable_stock_country' => (integer) $data['tradeable-stocks-country'][$i],
					'tradeable_stock_currency' => (integer) $data['tradeable-stocks-currency'][$i],
					'tradeable_stock_foreign_value' => (double) $data['tradeable-stocks-foreign-value'][$i],
					'securities_companies' => $data['securities-companies'][$i],
					'trader_account_number' => $data['trader-account-number'][$i],
				];
				$encodeDetail = json_encode($detail);
				$userAsset->detail = $encodeDetail;
				$userAsset->save();

				$i++;
			} else {
				$userAsset->delete();

				$i++;
			}
		}

		$userStep = $this->repositories['user_step'];
		$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		return $userAsset;
	}
}
