<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class InsuranceClaim {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			
			for ($i = 0; $i < count($data['income-id']); $i++){
				if (isset($data['income-id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income-id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
				}

				$detail = [
					'currency' => $data['currency'][$i],
					'currency_value' => $data['jumlah_rupiah'][$i],
					'kurs_value' => $data['kurs_pajak'][$i],
					'company_name' => $data['company'][$i],
					'insurance_type' => $data['insurance-type'][$i]
				];

				$userIncome->user_id = $userId;
                $userIncome->income_id = 40;
                $userIncome->country_id = 1;
                $userIncome->net_value = $data['acquisition-value'][$i];
                $userIncome->gross_value = 0;
                $userIncome->detail = json_encode($detail);
				$userIncome->user_asset_id = $data['account-number'][$i];
				$userIncome->asset_id = $data['asset-id'][$i];
                $userIncome->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}



		// $i = 0;

		// foreach ($data['insurance-claim-id'] as $incomeId){
		// 	$userIncome = $this->repositories['user_income']->getModel();
		// 	$existingIncome = $userIncome->where('id', '=', $incomeId)->first();

		// 	if ($existingIncome) {
		// 		$userIncome = $existingIncome;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userIncome->user_id = $userId;
		// 		$userIncome->income_id = 37;
		// 		$userIncome->country_id = 1;
		// 		$userIncome->net_value = (double) $data['hidden-insurance-claim-value'][$i];

		// 		$detail = [
		// 			'insurance_claim_type' => $data['insurance-claim-type'][$i],
		// 			'insurance_claim_purpose' => $data['insurance-claim-purpose'][$i],
		// 			'insurance_claim_currency' => (integer) $data['insurance-claim-currency'][$i],
		// 			'insurance_claim_foreign_value' => (double) $data['insurance-claim-foreign-value'][$i],
		// 			'insurance_claim_companies' => $data['insurance-claim-companies'][$i],
		// 			'insurance_claim_account_number' => $data['insurance-claim-account-number'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userIncome->detail = $encodeDetail;
		// 		$userIncome->save();

		// 		$i++;
		// 	} else {
		// 		$userIncome->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userAsset;
	}
}
