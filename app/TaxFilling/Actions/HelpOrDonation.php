<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class HelpOrDonation {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
       
        $result = false;
        $self_step_index = config('constants')['steps']['HELP_OR_DONATION'];
        empty($data['value']) ? $values = null : $values = array_values($data['value']);        
        empty($data['giver']) ? $givers = null : $givers = array_values($data['giver']);  
        empty($data['id']) ? null : $data['id'] = array_values($data['id']);      
       
        foreach($values as $v_key => $value){
            
            if(!empty($data['id'][$v_key])){
                $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$v_key]);
        
                $user_income->gross_value = $value;
                $user_income->net_value = 0;
                $user_income->detail = json_encode([
                    'pemberi_bantuan' => $givers[$v_key]
                ]);
                
            }else{
                $user_income = $this->repositories['user_income']->getModel();

                // $user_income = $user_income->firstOrNew(
                //     ['user_id' => $userId],
                //     ['income_id' => $type]
                // );
            
                $user_income->user_id = $userId;
                $user_income->income_id = 15;
                $user_income->country_id = 1;
                $user_income->gross_value = $value;
                $user_income->net_value = 0;
                $user_income->detail = json_encode([
                    'pemberi_bantuan' => $givers[$v_key]
                ]);
            }

            
            $user_income->save();

        }
       
        $userStep = $this->repositories['user_step'];
        $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
        
        $steps = new UserTaxFillingStepRepository();
        $steps = $steps->setDirection($userId,$self_step_index);

        $result = true;
       
        return $result;
        
	}
}
