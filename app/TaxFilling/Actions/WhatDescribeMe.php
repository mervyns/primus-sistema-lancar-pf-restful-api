<?php
namespace WebApp\TaxFilling\Actions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use Auth;

use Api\Users\Repositories\UserRepository;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserCompanions\Repositories\UserCompanionRepository;
use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Infrastructure\Helper\TaxFillingHelper;

class WhatDescribeMe {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_tax_profile' => new UserTaxProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'user_companion' => new UserCompanionRepository,
			'spouse_tax_profile' => new SpouseTaxProfileRepository,
			'user_income' => new UserIncomeRepository,
			'user_asset' => new UserAssetRepository,
			'user' => Auth::user(),
			'help' => new TaxFillingHelper(),
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$taxProfile = $this->repositories['user_tax_profile']->getModel();

		$existingProfile = $taxProfile->where('user_id', '=', $userId)->first();
		if ($existingProfile) {
			$taxProfile = $existingProfile;
		}

		if ($data['status'] != 2) $this->deleteSpouseWhenChangeStatus($userId);
		
		$taxProfile->user_id = $userId;
		$taxProfile->marital_status = intval($data['status']);
		$taxProfile->companion_has_npwp = $data['spouse_has_tax_id'];
		$taxProfile->tax_responsibility_status_id = !empty($data['spouse_taxing_status']) ? intval($data['spouse_taxing_status']) : null;
		$taxProfile->following_tax_amnesty_2016   = intval($data['2016_ta_participation']);
		$taxProfile->ta_disclosure = $data['pengungkapan'];
		$taxProfile->have_foreign_asset =  intval($data['have_foreign_asset']);
		
		if($taxProfile->save()) {
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			
			$this->parseDirection(['user_id' => $userId, 'step' => $step, 'ta' => intval($data['2016_ta_participation']), 'ta_d' => $data['pengungkapan']]);
			
			return $taxProfile;
		}
		return false;
	}

	private function deleteSpouseWhenChangeStatus($userId) {
		$user = new UserRepository();
		$my_data = $user->getModel()->with('companion')->with('spouseTaxProfile')->with('income')->find($userId);
		$my_data = empty($my_data) ? [] : $my_data->toArray();

		if (!empty($my_data['spouse_tax_profile'])) {
			$delete_spouse_tax_profile = $this->repositories['spouse_tax_profile']->getModel()
				->where('id', $my_data['spouse_tax_profile']['id'])
				->delete();
			$delete_spouse_income = $this->repositories['user_income']->getModel()
				->whereIn('income_id', [36, 37])
				->delete();
		}
		if (!empty($my_data['companion'])){
			$spouse_user_id = intval($my_data['companion']['companion_id']);
			$delete_companion = $this->repositories['user_companion']->getModel()
				->where('id', $my_data['companion']['id'])
				->delete();
			$spouse_data = $this->repositories['help']->GetAllUserData($spouse_user_id);
			
			if (count($spouse_data['income']) > 0) {
				$delete_user_income = $this->repositories['user_income']->getModel()
					->where('user_id', $spouse_user_id)
					->delete();
			}
			if (count($spouse_data['asset']) > 0) {
				$delete_user_asset = $this->repositories['user_asset']->getModel()
					->where('user_id', $spouse_user_id)
					->delete();
			}
		}
	
	}

	private function parseDirection(array $param){
		$userId = $param['user_id'];
		$step = $param['step'];
		$ta = $param['ta']; //tax amnesty
		$ta_d = $param['ta_d']; //tax amnesty pengungkapan

		$userTaxProfile = $this->repositories['user_tax_profile']->getWhere('user_id', $userId)->toArray()[0];
		$userProfile = $this->repositories['user']->where('id', $userId)->first()->toArray();
		$taxFillingStep = $this->repositories['user_step']->getWhere('user_id', $userId)->toArray()[0];

		$tempStep = is_null($taxFillingStep['temp_steps']) ? [] : json_decode($taxFillingStep['temp_steps'], true);
		$keys = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
		$rebuildStep = [];

		//unset items if exists
		if (count($tempStep) > 0){
			foreach ($keys as $key){
				$search = array_search($key, $tempStep);
				if ($search !== false) unset($tempStep[$search]);
			}
			$tempStep = array_values($tempStep);
		}

		if ($userTaxProfile['marital_status'] == 2){
			if ($userTaxProfile['companion_has_npwp'] == 0 && $userTaxProfile['tax_responsibility_status_id'] == 1){
				//Kepala Keluarga
				if ($userProfile['gender'] == 'P') {
					$rebuildStep = [1, 2, 3, 5, 8, 9, 14];
				}else{
					$rebuildStep = [1, 2, 3, 4, 8, 9, 14];
				}
			}else if ($userTaxProfile['companion_has_npwp'] == 1 && ($userTaxProfile['tax_responsibility_status_id'] == 3 || $userTaxProfile['tax_responsibility_status_id'] == 4)){
				//Pisah Harta & Management Teripsah
				if ($userProfile['gender'] == 'P') {
					$rebuildStep = [1, 2, 3, 9, 6, 7, 13, 14];
				}else{
					$rebuildStep = [1, 2, 3, 8, 9, 6, 7, 14];
				}
			}else if ($userTaxProfile['companion_has_npwp'] == 1 && ($userTaxProfile['tax_responsibility_status_id'] == 5 || $userTaxProfile['tax_responsibility_status_id'] == 2)){
				//Hidup Berpisah & Lapor Terpisah
				if ($userProfile['gender'] == 'P') {
					$rebuildStep = [1, 2, 3, 9, 14];
				}else{
					$rebuildStep = [1, 2, 3, 4, 8, 9, 14];
				}
			}
		}else{
			$rebuildStep = [1, 2, 3, 8, 9, 14];
		}

		array_splice($tempStep, 0, 0, $rebuildStep);

		if (array_search(16, $tempStep) === false) array_push($tempStep, 16);
		if (array_search(17, $tempStep) === false) array_push($tempStep, 17);
		if (array_search(18, $tempStep) === false) array_push($tempStep, 18);
		if (array_search(28, $tempStep) === false) array_push($tempStep, 28);
		if (array_search(43, $tempStep) === false) array_push($tempStep, 43);
		if (array_search(53, $tempStep) === false) array_push($tempStep, 53);
		if (array_search(57, $tempStep) === false) array_push($tempStep, 57);
		if (array_search(70, $tempStep) === false) array_push($tempStep, 70);

		if (array_search(87, $tempStep) === false) array_push($tempStep, 87);
		if (array_search(88, $tempStep) === false) array_push($tempStep, 88);
		if (array_search(89, $tempStep) === false) array_push($tempStep, 89);
		if (array_search(91, $tempStep) === false) array_push($tempStep, 91);
		if (array_search(93, $tempStep) === false) array_push($tempStep, 93);
		if (array_search(94, $tempStep) === false) array_push($tempStep, 94);

		asort($tempStep);
		$detail_step = $this->BuildDetailStep(is_null($taxFillingStep['detail_step']) ? [] : json_decode($taxFillingStep['detail_step'], true));
		//start process for detail step
		// for ($i = 2; $i <= 14; $i++) {
		// 	$temp_key = array_search($i, $tempStep);
		// 	if ($temp_key === false) unset($detail_step[$i]);
		// }
		//end process detail step
		
		if (!empty($tempStep)){
			$updateStep = $this->repositories['user_step']->getModel()->find($taxFillingStep['id']);
			$updateStep->temp_steps = json_encode($tempStep);
			$updateStep->detail_step = json_encode($detail_step);
			$updateStep->save();
		}
	}

	private function BuildDetailStep($detail) {
		$result = [];
		$steps = config('steps');

		foreach ($steps as $key => $val) {
			$nested = $val['nested'];
			if (empty($nested)) continue;

			$split = explode('.', $nested);
			$key = (count($split) == 3) ? $split[2] : $split[1];

			if (array_key_exists($key, $detail) == false) {
				$result[$key] = false;
			}else{
				$result[$key] = $detail[$key];
			}	
		}

		$result[2] = true;
		ksort($result);
		return $result;
	}

}