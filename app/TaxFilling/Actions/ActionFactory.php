<?php
namespace WebApp\TaxFilling\Actions;

use Exception;

class ActionFactory {
	public static function build ( $source ) {
		$name = __NAMESPACE__ . '\\' . $source;

		if ( class_exists( $name ) ) {
			return new $name();
		} else {
			throw new Exception("Invalid action type given. Action class: {$name}.");
		}
	}
}
