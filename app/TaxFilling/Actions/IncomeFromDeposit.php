<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class IncomeFromDeposit {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        
		$type = [];
		foreach ($data['deposit-type'] as $row){
			$type[] = $row;
        }
        
        try{
            for ($i = 0; $i < count($data['income_id']); $i++){
                if (isset($data['income_id'][$i]) == true){
                    $userIncome = $this->repositories['user_income']->getById($data['income_id'][$i]);
                }else{
                    $userIncome = $this->repositories['user_income']->getModel();
                }
                $detail = [
                    'currency' 			=> $data['currency'][$i],
                    'type'              => $type[$i],
					'tax_final' 	    => $data['tax-final'][$i],
					'kurs_value' 		=> $data['kurs_pajak'][$i],
                    'tax_rate'          => $data['tax_rate'][$i],
                    'deposit_number'    => $data['account-number'][$i],
                    'currency_value'    => $data['net-value'][$i],
                ];

                $userIncome->user_id = $userId;
                $userIncome->income_id = 38;
                $userIncome->country_id = $data['country'][$i];
                $userIncome->net_value = $data['jumlah_rupiah'][$i];
                $userIncome->gross_value = $data['gross-value'][$i];
                $userIncome->detail = json_encode($detail);
                $userIncome->save();
            }
            if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
            }
            $userStep = $this->repositories['user_step'];
            $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
            
			return true;
        }catch (\Exception $ex){
            //dd($ex->getMessage());
            return false;
        }

	}

}
