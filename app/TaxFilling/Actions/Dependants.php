<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserDependants\Repositories\UserDependantRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Dependants {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_dependant' => new UserDependantRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'user' => Auth::user(),
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
				
		try{
			for ($i = 0; $i < count($data['dependant-id']); $i++){
				if (!empty($data['name'][$i])){
					$userDependant = $this->repositories['user_dependant']->getModel();
					$existingProfile = $userDependant->where('id', '=', $data['dependant-id'][$i])->first();
					
					if ($existingProfile){
						$userDependant = $existingProfile;
					}

					$userDependant->user_id = $userId;
					$userDependant->name = $data['name'][$i];
					$userDependant->ktp_number = empty($data['nik'][$i]) ? '' : $data['nik'][$i];
					$userDependant->dependant_relation_id = $data['dependant-relation'][$i];
					$userDependant->job = $data['job'][$i];
					$userDependant->not_have_ktp = $data['nothavektp_status'][$i];
					$userDependant->save();
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection(['user_id' => $userId, 'step' => $step]);
			return true;
		}catch (\Exception $ex){
			//dd($ex->getMessage());
			return false;
		}

		// $i = 0;
		// $error = 0;
		// foreach($data['dependant-id'] as $dependantId){
		// 	$userDependant = $this->repositories['user_dependant']->getModel();
		// 	$existingProfile = $userDependant->where('id', '=', $dependantId)->first();
		// 	if ($existingProfile) {
		// 		$userDependant = $existingProfile;
		// 	}

		// 	if(!is_null($data['name'][$i]) 
		// 		&& !is_null($data['nik'][$i]) 
		// 		&& !is_null($data['dependant-relation'][$i]) 
		// 		&& !is_null($data['job'][$i])
		// 		&& $data['name'][$i] != "" 
		// 		&& $data['nik'][$i] != ""
		// 		&& $data['dependant-relation'][$i] != "" 
		// 		&& $data['job'][$i] != ""
		// 	){
		// 		$userDependant->user_id = $userId;
		// 		$userDependant->name = $data['name'][$i];
		// 		$userDependant->ktp_number = empty($data['nik'][$i]) ? '' : $data['nik'][$i];
		// 		$userDependant->dependant_relation_id = $data['dependant-relation'][$i];
		// 		$userDependant->job = $data['job'][$i];
		// 		$userDependant->not_have_ktp = $data['nothavektp_status'][$i];

		// 		if($userDependant->save()){
		// 			$i++;
		// 		} else{
		// 			$error = 1;
		// 		}
		// 	} else{
		// 		$i++;
		// 	}
		// }

		// if($error != 1){
		// 	$userStep = $this->repositories['user_step'];
		// 	$userStep->saveOrUpdate($userId, [
		// 		'current_step' => $step
		// 	]);

		// 	return $userDependant;
		// }
	
		
		// return false;
	}

	private function parseDirection(array $param){
		$userId = $param['user_id'];
		$step = $param['step'];

		$userProfile = $this->repositories['user']->where('id', $userId)->first();
		$userTaxProfile = $this->repositories['user']->taxProfile()->first();
		$taxFillingStep = $this->repositories['user_step']->getWhere('user_id', $userId);


		$tempStep = json_decode($taxFillingStep[0]->temp_steps, true);
		$currentStep = array_search((int)$step, $tempStep);
		$nextStep = next($tempStep);
		//$prevStep = prev($tempStep);

		$rebuildStep = [];

		if ($userTaxProfile->marital_status == 2){
			
		}else{
			if (!isset($tempStep[$nextStep])){
				$rebuildStep = $tempStep;
				$rebuildStep[] = 9;
				$rebuildStep[] = 14;
			}
		}
		
		if (!empty($rebuildStep)){
			$updateStep = $this->repositories['user_step']->getModel()->find($taxFillingStep[0]->id);
			$updateStep->temp_steps = json_encode($rebuildStep);
			$updateStep->save();

			$this->repositories['user_step']->setDirection($userId, (int)$step);
		}
	}

}