<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;

class PrizeLoterry {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['PRIZE_LOTERRY'];
          
            empty($data['brutto']) ? null : $data['brutto'] = array_values($data['brutto']);
            empty($data['name']) ? null : $data['name'] = array_values($data['name']);
            empty($data['netto']) ? null : $data['netto'] = array_values($data['netto']);
            empty($data['final']) ? null : $data['final'] = array_values($data['final']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
            
            if(!empty($data['netto'])){
                foreach ($data['netto'] as $e_key => $value){

                    if(!empty($data['id'][$e_key])){
                        $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                
                        $user_income->net_value = $value;
                        $user_income->gross_value = $data['brutto'][$e_key];
                        $user_income->detail = json_encode([
                            'nama_pemotong' => $data['name'][$e_key]
                        ]);
                        
                    }else{

                        $user_income = $this->repositories['user_income']->getModel();
                        $user_income->user_id = $userId;
                        $user_income->income_id = 10;
                        $user_income->country_id = 1;
                        $user_income->net_value = $value;
                        $user_income->gross_value = $data['brutto'][$e_key];
                        $user_income->detail = json_encode([
                            'nama_pemotong' => $data['name'][$e_key]
                        ]);
                    }
                    $user_income->save();
                }
                $result = true;
            }

            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            dd($e);
            return false;
        }
        
	}
}
