<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class CreditCard {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$data['select_value'] = !empty($data['select_value']) ? array_values($data['select_value']) : $data['select_value'];
		try{
			for ($i = 0; $i < count($data['liability-id']); $i++){
				if (isset($data['liability-id'][$i]) == true){
					$userLiability = $this->repositories['user_liability']->getById($data['liability-id'][$i]);
				}else{
					$userLiability = $this->repositories['user_liability']->getModel();
				}
				
				$detail = [
					'currency' => $data['currency'][$i],
					'country' => $data['country'][$i],
					'currency_value' => $data['account-balance'][$i],
					'kurs_value' => $data['kurs_pajak'][$i],
					'bank_id' => $data['bank-name'][$i],
					'other_bank' => $data['other-bank'][$i],
					'credit_card_name' => $data['credit-card-name'][$i],
					'credit_card_type' => $data['credit-card-type'][$i]
				];
				
				$userLiability->user_id = $userId;
				$userLiability->liability_id = 2;
				$userLiability->acquisition_year = date("Y") - 1;
				$userLiability->value = $data['jumlah_rupiah'][$i];
				$userLiability->lender_name= empty($data['select_value'][$i]) ? '' : $data['select_value'][$i];
				$userLiability->lender_address = '';
				$userLiability->detail = json_encode($detail);
				
				$userLiability->save();

			}
			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);

			return true;
		}catch (\Exception $ex){
			dd($ex);
			return false;
		}

		// $i = 0;
		// $error = 0;

		// foreach($data['liability-id'] as $liabilityId){
		// 	$userLiability = $this->repositories['user_liability']->getModel();
		// 	$existingLiability = $userLiability->where('id', '=', $liabilityId)->first();

		// 	if ($existingLiability) {
		// 		$userLiability = $existingLiability;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userLiability->user_id = $userId;
		// 		$userLiability->liability_id = 2;
		// 		$userLiability->acquisition_year = date("Y") - 1;
		// 		$userLiability->value = (double) $data['hidden-liability-value'][$i];

		// 		if(!isset($data['country'][$i])){
		// 			$data['country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'country' => (integer) $data['country'][$i],
		// 			'currency' => (integer) $data['currency'][$i],
		// 			'liability_foreign_value' => (double) $data['liability-foreign-value'][$i],
		// 			'bank_name' => $data['bank-name'][$i],
		// 			'bank_branch' => $data['bank-branch'][$i],
		// 			'bank_city' => $data['bank-city'][$i],
		// 			'credit_card_name' => $data['credit-card-name'][$i],
		// 			'credit_card_type' => $data['credit-card-type'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userLiability->detail = $encodeDetail;
		// 		$userLiability->save();

		// 		$i++;
		// 	} else {
		// 		$userLiability->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userLiability;
	}
}
