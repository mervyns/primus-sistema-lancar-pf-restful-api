<?php
namespace WebApp\TaxFilling\Actions;

use DB;
use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\Incomes\Repositories\IncomeRepository;

class SeverancePayOnce {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
            'user' =>  new UserRepository,
            'user_step' => new UserTaxFillingStepRepository,
            'user_income' => new UserIncomeRepository,
            'income' => new IncomeRepository
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
        try{
            $result = false;
            $self_step_index = config('constants')['steps']['SEVERANCE_PAY_ONCE'];
           
            empty($data['type']) ? null : $data['type'] = array_values($data['type']);
            empty($data['brutto']) ? null : $data['brutto'] = array_values($data['brutto']);
            empty($data['corporation']) ? null : $data['corporation'] = array_values($data['corporation']);
            empty($data['netto']) ? null : $data['netto'] = array_values($data['netto']);
            empty($data['tarif']) ? null : $data['tarif'] = array_values($data['tarif']);
            empty($data['final']) ? null : $data['final'] = array_values($data['final']);
            empty($data['id']) ? null : $data['id'] = array_values($data['id']);
            
            if(!empty($data['netto'])){
                foreach ($data['netto'] as $e_key => $element){
                    $type = null;
                    $income_type = $data['type'][$e_key];
                    $tax_rate =  ($data['tarif'][$e_key]/100);
                    $brutto = $data['brutto'][$e_key];
                    
                    switch ($income_type) {
                        case 1:
                            $income = $this->repositories['income']->getModel()
                                        ->where('tax_rate',$tax_rate)
                                        ->whereIn('id', [16, 17, 18, 19])->first();

                            if(!empty($income)){
                                $type = $income->id;
                            }
                            break;

                        case 2:
                        case 3:
                            $income = $this->repositories['income']->getModel()
                                        ->where('tax_rate',$tax_rate)
                                        ->whereIn('id', [20, 21, 22, 23, 24])
                                        ->first();
        
                            if(!empty($income)){
                                $type = $income->id;
                            }
                            break;
                        
                        default:
                            $type = null;
                            break;
                    }
                   
                    if(!empty($data['id'][$e_key])){
                        $user_income = $this->repositories['user_income']->getModel()->find($data['id'][$e_key]);
                
                        $user_income->income_id = $type;
                        $user_income->country_id = 1;
                        $user_income->gross_value = $data['brutto'][$e_key];
                        $user_income->net_value = $element;
                        $user_income->detail = json_encode([
                            'perusahaan' => $data['corporation'][$e_key],
                            'tarif_pajak' => $data['tarif'][$e_key]
                        ]);
                        
                    }else{
                        $user_income = $this->repositories['user_income']->getModel();
                        $user_income->user_id = $userId;
                        $user_income->income_id = $type;
                        $user_income->country_id = 1;
                        $user_income->gross_value = $data['brutto'][$e_key];
                        $user_income->net_value = $element;
                        $user_income->detail = json_encode([
                            'perusahaan' => $data['corporation'][$e_key],
                            'tarif_pajak' => $data['tarif'][$e_key]
                        ]);
                    }

                    
                    $user_income->save();
                }
                $result = true;
            }
           
            $userStep = $this->repositories['user_step'];
		    $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
          
            $steps = new UserTaxFillingStepRepository();
            $steps = $steps->setDirection($userId,$self_step_index);

            return $result;
        }catch(\Exception $e){
            return false;
        }
        
	}
}
