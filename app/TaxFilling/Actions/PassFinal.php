<?php
namespace WebApp\TaxFilling\Actions;
use Api\PassFinal\Repositories\PassFinalRepository;


class PassFinal {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'passfinal' => new PassFinalRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if (isset($data['id'])) {
			$this->repositories['passfinal']->update($data);
		} else {
			$this->repositories['passfinal']->create($data);
		}
		return true;
	}

	public function delete($userId, $step, $data) {
		$delete = $this->repositories['passfinal']->deletedata($data);
		return true;
	}
}
