<?php
namespace WebApp\TaxFilling\Actions;

use Auth;
use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class AdditionalRegularIncomesProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if ($data['submit'] == 'skip'){
			for ($i = 0; $i < count($data['invesment']); $i++){
				$data['invesment'][$i] = null;
			}
		}
		try{
			$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
			$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();
			
			$newDetail = [];
			
			if ($existingProfile){
				$newDetail = json_decode($existingProfile['detail'], true);
				$userAdditionalProfile = $existingProfile;
			}

			$newDetail['sewa_tanah_bangunan'] 		= (bool)$data['invesment'][0];
			$newDetail['dividen_saham'] 			= (bool)$data['invesment'][1];
			$newDetail['laba_anggota'] 				= (bool)$data['invesment'][2];
			$newDetail['penghasilan_deposito'] 		= (bool)$data['invesment'][3];
			$newDetail['bunga_sbi_sbn'] 			= (bool)$data['invesment'][4];
			$newDetail['bunga_obligasi'] 			= (bool)$data['invesment'][5];
			$newDetail['penjualan_saham'] 			= (bool)$data['invesment'][6];
			$newDetail['hak_kekayaan_intelektual'] 	= (bool)$data['invesment'][7];
			$newDetail['sewa_asset'] 				= (bool)$data['invesment'][8];
			$newDetail['bunga_hutang'] 				= (bool)$data['invesment'][9];
			$newDetail['tenaga_ahli'] 				= (bool)$data['invesment'][10];
			$newDetail['penghasilan_tambahan_lain']	= (bool)$data['invesment'][11];

			$userAdditionalProfile->user_id = $userId;
			$userAdditionalProfile->detail = json_encode($newDetail);
			$userAdditionalProfile->save();
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection($data['invesment'], $step);
			return true;

		}catch (\Exception $ex){
			//dd($ex->getMessage());
			return false;
		}

	}

	private function parseDirection($data, $step){
		$common = [58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69];
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				foreach ($data as $val){
					if ((bool)$val == true){
						$rebuildStep[] = (int)$val;
					}
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

	}
}
