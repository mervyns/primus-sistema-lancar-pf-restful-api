<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class MySpouseIncomeOption3 {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_income' => new UserIncomeRepository,
			'user_withholding_tax' => new UserWithholdingTaxRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data["id"]); $i++){
				if (isset($data['id'][$i]) == true){
					$userIncome = $this->repositories['user_income']->getModel()
						->where('id', '=', $data['id'][$i])
						->first();
					$userWithholdingTax = $this->repositories['user_withholding_tax']->getModel()
						->where('user_income_id', '=', $data['id'][$i])
						->first();
				}else{
					$userIncome = $this->repositories['user_income']->getModel();
					$userWithholdingTax = $this->repositories['user_withholding_tax']->getModel();
				}

				$detail = [
					'currency' => 1,
					'currency_value' => $data['net-income'][$i],
					'kurs_value' => 1
				];

				$userIncome->user_id = $userId;
				$userIncome->income_id = 36;
				$userIncome->country_id = 1;
				$userIncome->net_value = $data['net-income'][$i];
				$userIncome->gross_value = (float)$data['net-income'][$i] + (float)$data['withholding-value'][$i];
				$userIncome->detail = json_encode($detail);
				$userIncome->save();

				$slip_date = explode('/', $data['withholding-slip-date'][$i]);

				$userWithholdingTax->user_income_id = (isset($data['id'][$i]) == true) ? $data['id'][$i] : $userIncome->id;
				$userWithholdingTax->withholding_tax_slip_id = 1;
				$userWithholdingTax->withholder_name = $data['withholder-name'][$i];
				$userWithholdingTax->withholder_npwp = $data['withholder-npwp'][$i];
				$userWithholdingTax->withholding_slip_number = $data['withholding-slip-number'][$i];
				$userWithholdingTax->withholding_slip_date = $slip_date[2].'-'.$slip_date[1].'-'.$slip_date[0];
				$userWithholdingTax->value = $data['withholding-value'][$i];
				$userWithholdingTax->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$remove_tax = $this->repositories['user_withholding_tax']->deleteWhere('user_income_id', $data['deleted_id'][$i]);
					$remove_income = $this->repositories['user_income']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, ['current_step' => $step]);
			return true;
		}catch (\Exception $ex){
			return false;
		}
	}
}
