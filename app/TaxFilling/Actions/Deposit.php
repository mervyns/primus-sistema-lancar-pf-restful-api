<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Deposit {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		//dd($data);
		/** REINDEX */
		$type = [];
		foreach ($data['deposit-type'] as $row){
			$type[] = $row;
		}

		$rollover = [];
		foreach ($data['rollover'] as $row){
			$rollover[] = $row;
		}

		try{
			for ($i = 0; $i <count($data['asset-id']); $i++){
				if (isset($data['asset-id'][$i]) == true){
					$userAsset = $this->repositories['user_asset']->getById($data['asset-id'][$i]);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$expired_date = explode('/', $data['expired-date'][$i]);

				$detail = [
					'currency' 			=> $data['currency'][$i],
					'country'			=> $data['country'][$i],
					'currency_value' 	=> $data['deposit-balance'][$i],
					'kurs_value' 		=> $data['kurs_pajak'][$i],
					'deposit_type'		=> $type[$i],
					'bank_id'			=> $data['bank-name'][$i],
					'other_bank'		=> $data['other-bank'][$i],
					'deposit_number'	=> $data['account-number'][$i],
					'bank_city' 		=> $data['bank-city'][$i],
					'bank_branch'		=> $data['bank-branch'][$i],
					'expired_date'		=> $expired_date[2].'-'.$expired_date[1].'-'.$expired_date[0],
					'deposit_period'	=> $data['deposit-period'][$i],
					'deposit_interest' 	=> $data['interest'][$i],
					'deposit_rollover'	=> $rollover[$i],
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 4;
				$userAsset->acquisition_year = date("Y");
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();

			}
			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			//dd($ex->getMessage());
			return false;
		}
	}
}
