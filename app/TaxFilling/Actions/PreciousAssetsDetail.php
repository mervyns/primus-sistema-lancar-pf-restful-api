<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class PreciousAssetsDetail {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['asset-id']); $i++){
				$id = $data['asset-id'][$i];

				if (!empty($id)){
					$userAsset = $this->repositories['user_asset']->getById($id);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				$detail = [
					'country'					=> $data['country'][$i],
					'currency' 					=> $data['currency'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'description'				=> $data['description'][$i]
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = $data['precious-type'][$i];
				$userAsset->acquisition_year = $data['acquisition-year'][$i];
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;
		// $error = 0;

		// foreach($data['precious-assets-id'] as $assetId){
		// 	$userAsset = $this->repositories['user_asset']->getModel();
		// 	$existingAsset = $userAsset->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userAsset = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userAsset->user_id = $userId;
		// 		$userAsset->asset_id =(integer) $data['precious-assets-type'][$i];
		// 		$userAsset->acquisition_year = $data['precious-assets-acquisition-year'][$i];
		// 		$userAsset->idr_value = (double) $data['hidden-precious-assets-value'][$i];

		// 		if(!isset($data['precious-assets-country'][$i])){
		// 			$data['precious-assets-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'precious_assets_country' => (integer) $data['precious-assets-country'][$i],
		// 			'precious_assets_currency' => (integer) $data['precious-assets-currency'][$i],
		// 			'precious_assets_foreign_value' => (double) $data['precious-assets-foreign-value'][$i],
		// 			'precious_assets_description' => $data['precious-assets-description'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userAsset->detail = $encodeDetail;
		// 		$userAsset->save();

		// 		$i++;
		// 	} else {
		// 		$userAsset->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAsset;
	}
}
