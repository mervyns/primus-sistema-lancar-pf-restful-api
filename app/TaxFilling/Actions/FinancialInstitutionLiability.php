<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class FinancialInstitutionLiability {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_liability' => new UserLiabilityRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		try{
			for ($i = 0; $i < count($data['liability-id']); $i++){
				$id = $data['liability-id'][$i];
                
				if (!empty($id)){
					$userLiability = $this->repositories['user_liability']->getById($id);
				}else{
					$userLiability = $this->repositories['user_liability']->getModel();
				}

				$detail = [
                    'currency' 					=> $data['currency'][$i],
                    'country'                   => $data['country'][$i],
					'currency_value' 			=> $data['acquisition-value'][$i],
					'kurs_value' 				=> $data['kurs_pajak'][$i],
					'city'						=> $data['lender-city'][$i]
				];

				$userLiability->user_id = $userId;
				$userLiability->liability_id = 1;
				$userLiability->acquisition_year = $data['acquisition-year'][$i];
				$userLiability->value = $data['jumlah_rupiah'][$i];
				$userLiability->lender_name = $data['lender-name'][$i];
				$userLiability->lender_address = $data['lender-address'][$i];
				$userLiability->detail = json_encode($detail);
				$userLiability->save();
			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_liability']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			dd($ex->getMessage());
			return false;
		}
		
		// $i = 0;

		// foreach ($data['fil-id'] as $assetId){
		// 	$userLiability = $this->repositories['user_liability']->getModel();
		// 	$existingAsset = $userLiability->where('id', '=', $assetId)->first();

		// 	if ($existingAsset) {
		// 		$userLiability = $existingAsset;
		// 	}

		// 	if($data['deleted'][$i] != 'true'){
		// 		$userLiability->user_id = $userId;
		// 		$userLiability->liability_id = 1;
		// 		$userLiability->acquisition_year = $data['fil-acquisition-year'][$i];
		// 		$userLiability->value = (double) $data['hidden-fil-value'][$i];
		// 		$userLiability->lender_name = $data['fil-companies'][$i];
		// 		$userLiability->lender_address = $data['fil-companies-address'][$i].", ".$data['fil-companies-city'][$i];

		// 		if(!isset($data['fil-country'][$i])){
		// 			$data['fil-country'][$i] = 1;
		// 		}

		// 		$detail = [
		// 			'fil_country' => (integer) $data['fil-country'][$i],
		// 			'fil_currency' => (integer) $data['fil-currency'][$i],
		// 			'fil_foreign_value' => (double) $data['fil-foreign-value'][$i],
		// 			'fil_companies' => $data['fil-companies'][$i],
		// 			'fil_companies_address' => $data['fil-companies-address'][$i],
		// 			'fil_companies_city' => $data['fil-companies-city'][$i],
		// 		];
		// 		$encodeDetail = json_encode($detail);
		// 		$userLiability->detail = $encodeDetail;
		// 		$userLiability->save();

		// 		$i++;
		// 	} else {
		// 		$userLiability->delete();

		// 		$i++;
		// 	}
		// }

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);

		// return $userLiability;
	}
}
