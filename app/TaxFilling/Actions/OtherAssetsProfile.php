<?php
namespace WebApp\TaxFilling\Actions;

use Auth;

use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class OtherAssetsProfile {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_additional_profile' => new UserAdditionalProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		if ($data['submit'] == 'skip'){
			for ($i = 0; $i < count($data['other-asset']); $i++){
				$data['other-asset'][$i] = null;
			}
		}
		try{
			$userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
			$existingProfile = $userAdditionalProfile->where('user_id', $userId)->first();
			
			$newDetail = [];
			
			if ($existingProfile){
				$newDetail = json_decode($existingProfile['detail'], true);
				$userAdditionalProfile = $existingProfile;
			}

			$newDetail['other_cash_equivalents'] = isset($data['other-asset'][0]) ? (bool)$data['other-asset'][0] : false;
			$newDetail['special_transportation'] = isset($data['other-asset'][1]) ? (bool)$data['other-asset'][1] : false;
			$newDetail['other_transportation'] = isset($data['other-asset'][2]) ? (bool)$data['other-asset'][2] : false;
			$newDetail['electronics_furnitures'] = isset($data['other-asset'][3]) ? (bool)$data['other-asset'][3] : false;
			$newDetail['other_immovable_property'] = isset($data['other-asset'][4]) ? (bool)$data['other-asset'][4] : false;
			$newDetail['other_movable_property'] = isset($data['other-asset'][5]) ? (bool)$data['other-asset'][5] : false;
			$newDetail['account_receivable'] = isset($data['other-asset'][6]) ? (bool)$data['other-asset'][6] : false;
			$newDetail['affiliate_receivable'] = isset($data['other-asset'][7]) ? (bool)$data['other-asset'][7] : false;
			$newDetail['other_receivable'] = isset($data['other-asset'][8]) ? (bool)$data['other-asset'][8] : false;

			$userAdditionalProfile->user_id = $userId;
			$userAdditionalProfile->detail = json_encode($newDetail);
			$userAdditionalProfile->save();
			
			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [
				'current_step' => $step
			]);
			$this->parseDirection($data['other-asset'], $step);
			return true;
		}catch(\Exception $ex){
			dd($ex->getMessage());
			return false;
		}

		// $i = 0;
		// $error = 0;

		// $setaraKasLainnya = false;
		// $kapalPesiar = false;
		// $alatTransportasiLainnya = false;
		// $peralatanElektronikFurnitur = false;
		// $hartaTidakBergerakLainnya = false;
		// $hartaBergerakLainnya = false;
		// $piutang = false;
		// $piutangAfiliasi = false;
		// $piutangLainnya = false;

		// if(!empty($data['asset-profile'])){
		// 	foreach($data['asset-profile'] as $assetProfile){
		// 		if($assetProfile == 5){
		// 			$setaraKasLainnya = true;
		// 		} elseif ($assetProfile == 25) {
		// 			$kapalPesiar = true;
		// 		} elseif ($assetProfile == 21) {
		// 			$alatTransportasiLainnya = true;
		// 		} elseif ($assetProfile == 26) {
		// 			$peralatanElektronikFurnitur = true;
		// 		} elseif ($assetProfile == 31) {
		// 			$hartaTidakBergerakLainnya = true;
		// 		} elseif ($assetProfile == 27) {
		// 			$hartaBergerakLainnya = true;
		// 		} elseif ($assetProfile == 6) {
		// 			$piutang = true;
		// 		} elseif ($assetProfile == 7) {
		// 			$piutangAfiliasi = true;
		// 		} elseif ($assetProfile == 8) {
		// 			$piutangLainnya = true;
		// 		}
		// 	}
		// }

		// $userAdditionalProfile = $this->repositories['user_additional_profile']->getModel();
		// $existingProfile = $userAdditionalProfile->first();

		// if ($existingProfile) {
		// 	$userAdditionalProfile = $existingProfile;
		// }

		// $userAdditionalProfile->user_id = $userId;

		// $decodeDetail = json_decode($userAdditionalProfile->detail, true);
		// $newDetailRecord = [
		// 	'setara_kas_lainnya' => $setaraKasLainnya,
		// 	'kapal_pesiar' => $kapalPesiar,
		// 	'alat_transportasi_lainnya' => $alatTransportasiLainnya,
		// 	'peralatan_elektronik_furnitur' => $peralatanElektronikFurnitur,
		// 	'harta_tidak_bergerak_lainnya' => $hartaTidakBergerakLainnya,
		// 	'harta_bergerak_lainnya' => $hartaBergerakLainnya,
		// 	'piutang' => $piutang,
		// 	'piutang_afiliasi' => $piutangAfiliasi,
		// 	'piutang_lainnya' => $piutangLainnya
		// ];

		// if(is_null($decodeDetail)){
		// 	$mergedDetail = $newDetailRecord;
		// } else{
		// 	$mergedDetail = array_merge($decodeDetail, $newDetailRecord);
		// }

		// $encodeDetail = json_encode($mergedDetail);

		// $userAdditionalProfile->detail = $encodeDetail;
		// $userAdditionalProfile->save();

		// $userStep = $this->repositories['user_step'];
		// $userStep->saveOrUpdate($userId, [
		// 	'current_step' => $step
		// ]);

		// return $userAdditionalProfile;
	}

	private function parseDirection($data, $step){
		$common = [44, 45, 46, 47, 48, 49, 50, 51, 52];
		$taxFillStep = Auth::user()->step;
		$tempStep = empty($taxFillStep->temp_steps) ? [] : json_decode($taxFillStep->temp_steps, true);
		foreach ($common as $key => $row){
			$stepKey = array_search($row, $tempStep);
			if ($stepKey !== false) unset($tempStep[$stepKey]);
		}
		$tempStep = array_values($tempStep);

		$rebuildStep = [];

		foreach ($tempStep as $key => $row){
			$rebuildStep[] = $row;
			if ($row == $step){
				foreach ($data as $val){
					if ((bool)$val == true){
						$rebuildStep[] = (int)$val;
					}
				}
			}
		}

		$updateStep = $this->repositories['user_step']->getModel()->find($taxFillStep->id);
		$updateStep->temp_steps = json_encode($rebuildStep);
		$updateStep->save();

	}

}
