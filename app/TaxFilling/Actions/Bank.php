<?php
namespace WebApp\TaxFilling\Actions;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;


class Bank {
	private $repositories;

	public function __construct()
	{
		$this->repositories = [
			'user_asset' => new UserAssetRepository,
			'user_step' => new UserTaxFillingStepRepository,
		];
	}

	public function saveOrUpdate($userId, $step, $data)
	{
		$type = [];
		foreach ($data['bank-type'] as $row){
			$type[] = $row;
		}
		try{
			for ($i = 0; $i <count($data['asset-id']); $i++){
				if (isset($data['asset-id'][$i]) == true){
					$userAsset = $this->repositories['user_asset']->getById($data['asset-id'][$i]);
				}else{
					$userAsset = $this->repositories['user_asset']->getModel();
				}

				//parsing interest data json
				$interest = str_replace('[', '', $data['interest-data'][$i]);
				$interest = str_replace(']', '', $interest);
				$arr_interest = explode(',', $interest);

				//parsing tax data json
				$tax = str_replace('[', '', $data['tax-data'][$i]);
				$tax = str_replace(']', '', $tax);
				$arr_tax = explode(',', $tax);

				$detail = [
					'currency' 			=> $data['currency'][$i],
					'country'			=> $data['country'][$i],
					'currency_value' 	=> $data['account-balance'][$i],
					'kurs_value' 		=> $data['kurs_pajak'][$i],
					'bank_type' 		=> $type[$i],
					'bank_id' 			=> $data['bank-name'][$i],
					'other_bank'		=> $data['other-bank'][$i],
					'bank_number'		=> $data['account-number'][$i],
					'bank_city' 		=> $data['bank-city'][$i],
					'bank_branch'		=> $data['bank-branch'][$i],
					'interests' 		=> $data['interests'][$i],
					'interest_data' 	=> (array)$arr_interest,
					'taxes'				=> $data['taxes'][$i],
					'tax_data'			=> (array)$arr_tax
				];

				$userAsset->user_id = $userId;
				$userAsset->asset_id = 2;
				$userAsset->acquisition_year = date("Y");
				$userAsset->idr_value = $data['jumlah_rupiah'][$i];
				$userAsset->is_ta = 0;
				$userAsset->detail = json_encode($detail);
				$userAsset->save();

			}

			if (isset($data['deleted_id']) == true){
				for ($i = 0; $i < count($data['deleted_id']); $i++){
					$this->repositories['user_asset']->delete($data['deleted_id'][$i]);
				}
			}

			$userStep = $this->repositories['user_step'];
			$userStep->saveOrUpdate($userId, [ 'current_step' => $step ]);
			return true;
		}catch (\Exception $ex){
			//dd($ex->getMessage())''
			return false;
		}

	}

}
