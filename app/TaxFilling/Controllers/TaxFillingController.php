<?php
namespace WebApp\TaxFilling\Controllers;

require_once '../vendor/autoload.php';

use Auth;
use Log;
use Validator;

use Illuminate\Http\Request;
use Hoa\Websocket\Client as HoaClient;
use Hoa\Socket\Client as HoaSocketClient;

use Infrastructure\Http\Controller;

use WebApp\TaxFilling\Actions\ActionFactory;
use WebApp\TaxFilling\FormData\FormDataFactory;

use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

use Api\System\Repositories\SystemStepConfigRepository;
use Api\System\Repositories\SystemStepConstantRepository;

use Api\Tooltips\Repositories\TooltipRepository;

use Infrastructure\Helper\TaxFillingHelper;

class TaxFillingController extends Controller
{
	private $repositories;

	private $step_config;

    public function __construct(Request $request)
    {
		$this->middleware('auth');
		// $this->middleware('ctfp');
		
		$this->repositories = [
			'user_incomes' => new UserIncomeRepository,
			'user_tax_profile' => new UserTaxProfileRepository,
			'user_step' => new UserTaxFillingStepRepository,
			'step_config' => new SystemStepConfigRepository,
			'step_constant' => new SystemStepConstantRepository,
			'tooltip' => new TooltipRepository,
			'help' => new TaxFillingHelper,
		];

		$this->step_config = $this->getStepConfig();
		// dd($request);
    }

    public function index(Request $request, $step = 1)
    {
		$this->rebuildNotes();
	    try {
			\Session::forget('is_final_step');
			$this->rebuildStepConfig($step);
			$moveToLastStep = $request->input('redirect', false);
			$lastStep = Auth::user()->step ? Auth::user()->step->current_step : 0;
			$profileUser = Auth::user();

		    if ( $lastStep > 0 && $step != $lastStep && (bool) $moveToLastStep !== false) {
			    return redirect( "tax-filling/{$lastStep}" );
			}
			
			/** set direction */
			
			$className = $this->getClassName( $step );
			$dataClass = $className ? FormDataFactory::build($className) : [];
		    $formData  = $dataClass ? $dataClass->get() : [];
			$steps     = $this->step_config; //config( 'steps' );

			$tooltip = $this->getCurrentToolTip($step);
			$navigation = $this->repositories['help']->Navigation(config('steps'), $step);

			// check or skip page
			if ($step == 91) {
				if (!empty($formData['me']['npwp']) && !empty($formData['me']['efin']) && !empty($formData['me']['nik'])) {
					return redirect('tax-filling/93');
				}
			}
			// end check or skip page
			
		    return view( 'main', [
			    'steps'        	=> $steps,
			    'current_step' 	=> $steps[ $step ],
				'form_data'    	=> $formData,
				'tooltip'		=> $tooltip,
				'options'		=> [
					'tooltip'		=> $tooltip,
					'navigation' => $navigation
				]
		    ] );
	    } catch (\Exception $error) {
			dd($error, $error->getMessage());
		    Log::debug($error);

		    return redirect( "tax-filling/{$step}" )
			    ->withInput();
	    }
    }

	public function indexFinalSteps(Request $request, $step = 1, $income_id = false)
    {
	    try {
			$moveToLastStep = $request->input('redirect', false);
			$lastStep = Auth::user()->step ? Auth::user()->step->current_step : 0;
			$profileUser = Auth::user();

		    if ( $lastStep > 0 && $step != $lastStep && (bool) $moveToLastStep !== false) {
			    return redirect( "tax-filling/{$lastStep}" );
			}
			
			/** set direction */
			
			$className = $this->getClassName( $step );
			// dd($className);
			$dataClass = $className ? FormDataFactory::build($className) : [];
			$formData  = $dataClass ? $dataClass->getFinalSteps($income_id) : [];
			$steps     = $this->step_config; //config( 'steps' );
			//dd($formData);
			if(\Session::get('is_final_step_submited')){
				\Session::forget('is_final_step');
				\Session::forget('is_final_step_submited');
			}

			$tooltip = [];
			$navigation = [];

		    return view( 'main', [
			    'steps'        => $steps,
			    'current_step' => $steps[ $step ],
				'form_data'    => $formData,
				'options'		=> [
					'tooltip'		=> $tooltip,
					'navigation' => $navigation
				]
		    ] );
	    } catch (\Exception $error) {
			dd($error);
		    Log::debug($error);

		    return redirect( "tax-filling/{$step}" )
			    ->withInput();
	    }
    }

	/**
	 * @param $step
	 * @param Request $request
	 * @todo move this method to independent service class
	 * @todo fix the form validation (currently commented below)
	 *
	 * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
    public function stepAction($step, Request $request)
    {
    	try {
			$userProfile = Auth::user();
    		$stepsConfig = $this->step_config; //config( 'steps' );
			$currentStep = $stepsConfig[ $step ];
			
			$attribute_names = empty($currentStep['validations']['att_names']) ? [] : $currentStep['validations']['att_names'];
			
			$validator = Validator::make($request->all(), 
				$currentStep['validations']['rules'], 
				$currentStep['validations']['messages']
			)->setAttributeNames($attribute_names);;
			
		    if ( (bool)$validator->fails() == true ) {
				$encoded_message = ($validator->errors()->first());

				$encoded_message = json_decode($encoded_message,true);
				
				if(!empty($encoded_message)){
					return redirect( "tax-filling/{$step}" )
				    ->with(['encoded_message' => $encoded_message]);
				}
				
			    return redirect( "tax-filling/{$step}" )
				    ->withErrors( $validator )
				    ->withInput();
		    }
		
			$action = ActionFactory::build($this->getClassName( $step ));
			$userId = Auth::id();
			
		    if ( ! $action->saveOrUpdate( $userId, $step, $request->all() ) ) {
			    return redirect( "tax-filling/{$step}" )
				    ->withInput();
			}
			
			if(\Session::get('precious_assets_next')){
				$next = \Session::get('precious_assets_next');
				\Session::forget('precious_assets_next');
				return redirect( "tax-filling-final-steps/{$next}" );
			}
			
			if(\Session::get('is_final_step')){
	
				$client = new HoaClient(
					new HoaSocketClient('ws://127.0.0.1:8889')
				);
			
				$client->connect();
				$client->send((string)$userId);
				$client->close();

				\Session::put('is_final_step_submited',true);
				return redirect( "tax-filling-final-steps/{$step}" );
			}

			$this->rebuildStepConfig($step);

			if ($request->submit == 'save') {
				return redirect("/home");
			}elseif ($request->submit == 'reset') {
				return redirect("tax-filling/{$step}");
			}else{
				return redirect("tax-filling/{$this->step_config[$step]['next']}");
			}
			
	    } catch (\Exception $error) {
			dd($error);
    		Log::debug($error);

		    return redirect( "tax-filling/{$step}" )
			    ->withInput();
	    }
    }

    private function getClassName($step)
    {
		$step = intval($step);
		if ($step > 1){
			$get = $this->repositories['step_constant']->getModel()->get();
			$steps = [];

			foreach ($get->toArray() as $row){
				$steps[$row['name']] = $row['step'];
			}

			$stepsName = array_flip( $steps );
			return studly_case( strtolower( $stepsName[ $step ] ) );
		}

		return null;

		// $step = intval($step);
    	// if ($step > 1) {
		// 	$steps = config( 'constants' )['steps'];
		// 	$stepsName = array_flip( $steps );
		//     return studly_case( strtolower( $stepsName[ $step ] ) );
	    // }

	    // return null;
	}

	private function getStepConfig(){
		$value = $this->repositories['step_config']->get()->toArray();
		$result = [];

		foreach ($value as $key => $row){
			$result[$row['step_id']] = [
				'title' 					=> $row['title'],
				'subtitle' 					=> $row['sub_title'],
				'icon' 						=> $row['icon'],
				'parent' 					=> $row['parent'],
				'display_sticky_note' 		=> $row['display_sticky_note'],
				'display_knowledge_base'	=> $row['display_knowledge_base'],
				'display_progress_bar'		=> $row['display_progress_bar'],
				'previous'					=> $row['previous'],
				'next'						=> $row['next'],
				'view'						=> $row['view'],
				'url'						=> '/tax-filling/'.$row["step_id"],
				'validations'				=> json_decode($row['validation_rules'], true)
			];
		}
		return $result;
	}

	private function getCurrentToolTip($step){
		$columns = ['tooltips.id','tooltips.judul', 'tooltips.content', 'tooltips.link', 'system_steps_config.step_id'];
		$tooltip = $this->repositories['tooltip']->getModel()->select($columns)
			->join('system_steps_config', 'tooltips.step_config_id', 'system_steps_config.id')
			->where('system_steps_config.step_id', $step)
			->orderBy('tooltips.updated_at', 'desc')
			->first();
		
		$result['title'] = empty($tooltip) ? '' : $tooltip['judul'];
		$result['link'] = empty($tooltip) ? '' : $tooltip['link'];
		$result['content'] = empty($tooltip) ? '' : $tooltip['content'];

		return $result;
	}

	private function rebuildStepConfig($step){
		$taxFillStep = $this->repositories['user_step']->getWhere('user_id', Auth::user()->id);
		$tempStep = empty($taxFillStep[0]->temp_steps) ? [] : json_decode($taxFillStep[0]->temp_steps, true);

		if (count($tempStep) > 0){
			$current = array_search((int)$step, $tempStep);
			$next = isset($tempStep[$current + 1]) ? $tempStep[$current + 1] : $tempStep[$current];
			$prev = isset($tempStep[$current - 1]) ? $tempStep[$current - 1] : $tempStep[$current];
			
			$this->step_config[$step]['next'] = $next;
			$this->step_config[$step]['previous'] = $prev;
		}
	}

	private function mappingNavigation($currentStep) {
		$steps = config( 'steps' );
		$const = config( 'constants' );
		
		$menu = array(
			1 => array('name' => 'User Profile', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array()),
			2 => array('name' => 'Penghasilan Utama', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array()),
			3 => array('name' => 'Aset dan Hutang', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array()),
			4 => array('name' => 'Penghasilan Tambahan Rutin', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array()),
			5 => array('name' => 'Penghasilan Sekali Terima', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array()),
			6 => array('name' => 'Tax Amnesty', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => false, 'submenu' => array()),
			7 => array('name' => 'Informasi Lainnya', 'focused' => false, 'link' => 'javascript:void(0)', 'status' => true, 'submenu' => array())
		);

		$focused = false;
		foreach ($steps as $key => $val) {
			$nested = $val['nested'];
			if (!empty($nested)) {
				$exp = explode('.', $nested);
				$id_menu = $exp[0];

				if (array_key_exists($id_menu, $menu)) {
					$id_submenu = $exp[1];
					$title_submenu = $val['title'];
					$sub_submenu = array();
					

					if ($currentStep == $id_submenu) {
						$menu[$id_menu]['focused'] = true;
					}

					if (count($exp) == 3){
						$menu[$id_menu]['submenu'][$id_submenu]['submenu'][$exp[2]] = array(
							'id' => $exp[2],
							'title' => $title_submenu,
							'focused' => $currentStep == $exp[2] ? true : false
						);
					}else {
						$menu[$id_menu]['submenu'][$id_submenu] = array(
							'id' => $id_submenu,
							'title' => $title_submenu,
							'submenu' => $sub_submenu,
							'focused' => $currentStep == $id_submenu ? true : false
						);
					}
					
				}
			}
		}
		return $menu;
	}

	private function rebuildNotes(){
		$taxProfile = Auth::user()->taxProfile;
		$spouseTaxProfile = Auth::user()->spouseTaxProfile;
		$userIncome = $this->repositories['user_incomes']->getModel()->incomesQuery(Auth::user()->id)->get();

		if(!empty($taxProfile) 
			&& !empty($spouseTaxProfile) 
			&& Auth::user()->gender == 'P' 
			&& $taxProfile->marital_status == 2 
			&& $taxProfile->tax_responsibility_status_id == 1
			&& $spouseTaxProfile->job_status_id == 1
		){
			\Session::put('my_husband',1);
		}

		if(!$userIncome->isEmpty()){
			
			$countMainIncome = count($userIncome->where('income_id',1));
			\Session::put('main_income',$countMainIncome);

			$countOverseasIncome = count($userIncome->where('income_id',52));
			\Session::put('overseas_income',$countOverseasIncome);

			$countIntelectualPropertyIncome = count($userIncome->where('income_id',7));
			\Session::put('intelectual_income',$countIntelectualPropertyIncome);

			$countExpertRewardIncome = count($userIncome->where('income_id',45));
			\Session::put('expert_reward_income',$countExpertRewardIncome);

			$countLuxuryGoods = count($userIncome->where('income_categories_id',4));
			\Session::put('luxury_goods',$countLuxuryGoods);
		}
	}

	private function checkOrSkipStep($data, $currentStep) {
		
	}

}

