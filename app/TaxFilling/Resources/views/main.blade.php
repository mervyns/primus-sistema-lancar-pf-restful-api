    @extends('layout.main')

@section('content')
<section id="tax-filling-step" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                    @include('navigation')
                <div class="boxed-header max-w-sm mx-auto" style="padding: 1rem 0 5px 0;">
                    <h3 class="h2 boxed-title text-gray font-responsive">{{ $current_step['title'] }}</h3>
                </div>
                <div class="col-md-12">{!! $current_step['subtitle'] !!}</div>
                
                @include("steps.{$current_step['view']}")
                @if (!empty($options['tooltip']['content']))
                    <div class="toolTip">
                            {{ $options['tooltip']['content'] }} <a href="{{ $options['tooltip']['link'] }}" target="_blank" style="color: blue; text-decoration: underline;">Baca Selengkapnya</a>
                        <div class="tail"></div>
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection