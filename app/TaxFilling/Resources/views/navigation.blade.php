<br><br>
<div class="row">
    <div class="col-md-12">
        <div class="tax-nav">
            <ul>
                @foreach ($options['navigation'] as $val)
                    <li class="{{ $val['status'] == true ? 'filled' : '' }}  {{ $val['focused'] == true ? 'focused' : '' }}">
                        <i class="fa fa-check-circle"></i> 
                        <a href="{{ $val['link'] }}">{{ $val['name'] }}</a>

                        @if (count($val['submenu']) > 0)
                            <ul>
                                @foreach ($val['submenu'] as $v)
                                    @if ($v['hidden'] == false)
                                        <li class="{{ $v['focused'] == true ? 'focused' : '' }}">
                                            <a class="{{ $v['filled'] == true ? 'filled' : '' }}" href="{{ url('/tax-filling/'.$v['id']) }}"> {{ $v['title'] }} {!! count($v['submenu']) > 0 ? '<div class="pull-right"><i style="font-size: 7px;" class="fa fa-play"></i></div>' : ''  !!}</a>
                                            @if (count($v['submenu']) > 0) 
                                                <ul>
                                                    @foreach ($v['submenu'] as $x)
                                                        @if ($x['hidden'] == false)
                                                            <li class="{{ $x['focused'] == true ? 'focused' : '' }}">
                                                                <a href="{{ url('/tax-filling/'.$x['id']) }}"> {{ $x['title'] }}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
            <div class="progress" style="height:12px;">
                <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{ $options['navigation'][1]['progress'] }}%; text-align: center; font-size: 11px; font-weight: 600;">{{ $options['navigation'][1]['progress'] == 0 ? '' : $options['navigation'][1]['progress'].' %' }}</div>
            </div>
        </div>
        
    </div>
</div>
