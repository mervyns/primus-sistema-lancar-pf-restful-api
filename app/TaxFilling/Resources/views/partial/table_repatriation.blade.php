
    @if(!empty($data['assets']))
        
        <tr id='user_assets_{{$data['assets']['id']}}_td'>
            <td>1</td>
            <td>{{$data['assets']['asset_code']}}</td>
            <td>{{$data['assets']['name']}}</td>
            <td><input type="text" name="nilai_dialihkan" value=""></td>
            <td colspan='2'>
                @if(!empty($data['gateways']))
                    <select class="select2kodegateway_{{$data['assets']['id']}}" name="kode-gateway[]" multiple="multiple">
                        @foreach ($data['gateways'] as $g_key => $gateway)
                            <option value="{{$gateway['id']}}">{{$gateway['gateway_code']}} - {{$gateway['name']}}</option>   
                        @endforeach
                    </select>
                @endif
            </td>
            {{-- <td><input type="text" name="nama_gateway"></td> --}}
            <td colspan='2'>
                @if(!empty($data['investments']))
                    <select class="select2investments_{{$data['assets']['id']}}" name="investments[]" multiple="multiple">
                        @foreach ($data['investments'] as $i_key => $investment)
                            <option value="{{$investment['id']}}">{{$investment['investment_code']}} - {{$investment['name']}}</option>   
                        @endforeach
                    </select>
                @endif
            </td> 
            {{-- <td><input type="text" name="bentuk_investasi"></td>  --}}
            <td><input type="text" id='datepicker_{{$data['assets']['id']}}' name="tanggal_investasi" placeholder="yyyy-mm-dd"></td> 
            <td><input type="text" name="nilai"></td> 
            <td>
                @if(!empty($data['currencies']))
                    <select class="select2currencies_{{$data['assets']['id']}}" name="currencies[]" multiple="multiple">
                        @foreach ($data['currencies'] as $c_key => $currency)
                            <option value="{{$currency['id']}}">{{$currency['currency_name']}} - {{$currency['currency']}}</option>   
                        @endforeach
                    </select>
                @endif
            </td> 
            <td><input type="text" name="keterangan"></td> 
            {{-- <td style="display:none;">{{$data['id']}}
            <td style="display:none;">{{$data['user_id']}}
            <td style="display:none;">{{$data['currency']}} --}}
        </tr>
        
    @endif

