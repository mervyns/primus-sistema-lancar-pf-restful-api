
@if(!empty($data['assets']))
    <div id="tab{{$data['assets']['id']}}" class="tab">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <input type="hidden" name="user_id[{{$data['assets']['id']}}]" value="{{$data['assets']['user_id']}}">
                <input type="hidden" name="asset_id[{{$data['assets']['id']}}]" value="{{$data['assets']['id']}}">
                <div class="form-group row">
                    <label for="harta-code" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Kode Harta
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm" id="harta-code" name="kode_harta[{{$data['assets']['id']}}]" value="{{$data['assets']['asset']['asset_code']}}" autocomplete="off" readonly style="text-align: center;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="harta-name" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Nama Harta
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                                <input style="text-align: center;" type="text" class="form-control form-control-sm" id="harta-name" name="nama_harta[{{$data['assets']['id']}}]" value="{{$data['assets']['asset']['name']}}" autocomplete="off" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="value-transfered" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Nilai yang dialihkan ke dalam Negeri
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" style="text-align: center;" class="form-control form-control-sm input-money-format" id="value-transfered" name="nilai_dialihkan[{{$data['assets']['id']}}]" value="" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gateway" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Kode - Nama Gateway
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10" id="selectgateway{{$data['assets']['id']}}" style="text-align:center">
                                @if(!empty($data['gateways']))
                                    <select class="select2kodegateway_{{$data['assets']['id']}}" name="gateway_id[{{$data['assets']['id']}}]">
                                        @foreach ($data['gateways'] as $g_key => $gateway)
                                            <option value="{{$gateway['id']}}">{{$gateway['gateway_code']}} - {{$gateway['name']}}</option>   
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="investment" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Kode - Nama Investasi
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10" style="text-align:center">
                                @if(!empty($data['investments']))
                                    <select class="select2investments_{{$data['assets']['id']}}" name="investment_id[{{$data['assets']['id']}}]">
                                        @foreach ($data['investments'] as $i_key => $investment)
                                            <option value="{{$investment['id']}}">{{$investment['investment_code']}} - {{$investment['name']}}</option>   
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="value-transfered" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Tanggal Mulai Investasi
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" class="form-control form-control-sm" id='datepicker_{{$data['assets']['id']}}' name="tanggal_investasi[{{$data['assets']['id']}}]" placeholder="dd-mm-yyyy" style="text-align: center;" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="value" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Nilai
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                            <input type="text" style="text-align: center;" class="form-control form-control-sm input-money-format" name="value[{{$data['assets']['id']}}]" value="{{$data['assets']['idr_value']}}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="currency" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Mata Uang
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10" style="text-align: center;">
                                @if(!empty($data['currencies']))
                                    <select class="select2currencies_{{$data['assets']['id']}}" name="currency_id[{{$data['assets']['id']}}]">
                                        @foreach ($data['currencies'] as $c_key => $currency)
                                            <option value="{{$currency['id']}}">{{$currency['currency_name']}} - {{$currency['currency']}}</option>   
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="keterangan" class="text-right col-sm-5 col-form-label col-form-label-sm">
                        Ket
                    </label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-10">
                                <input type="text" style="text-align: center;" class="form-control form-control-sm" name="keterangan[{{$data['assets']['id']}}]">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

