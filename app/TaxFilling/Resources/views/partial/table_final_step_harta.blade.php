@if(!empty($data['data']['harta']))
    @foreach ($data['data']['harta'] as $sub_key => $sub_category)
    <tr class="spacer" style="height:20px">
    @php $i = 1; @endphp
    <tr>
        <td colspan="7" style="text-align:center; font-weight:bold;">{{$sub_key}}</td>
    </tr>
        @foreach ($sub_category as $item_key => $item)
            <tr>
                <td>{{$i}}</td>
                <td>{{$item->asset_category_name}}</td>
                <td>{{$item->asset_name}}</td>
                <td>{{$item->acquisition_year}}</td>
                <td>{{number_format($item->idr_value,2,',','.')}}</td>
                <td>
                    @if (is_array($item->detail))
                        <ul>
                        @foreach ($item->detail as $id_key => $item_deatil)
                            <li>{{$id_key}} : {{$item_deatil}}</li>
                        @endforeach
                        </ul>
                    @else
                        -
                    @endif
                </td>
                <td><button class="btn btn-warning pull-right btnEditHarta" value="{{$item->id}}|{{$item->asset_step}}">Edit</button></td>
            </tr>  
            @php $i++; @endphp
        @endforeach
    @endforeach
@endif

