@if(!empty($data['data']['penghasilan']))
    @foreach ($data['data']['penghasilan'] as $sub_key => $sub_category)
    <tr class="spacer" style="height:20px">
        @php $i = 1; @endphp
        <tr>
            <td colspan="7" style="text-align:center; font-weight:bold;">{{$sub_key}}</td>
        </tr>
        @foreach ($sub_category as $item_key => $item)
            <tr>
                <td>{{$i}}</td>
                <td>{{$item->income_name}}</td>
                <td>{{number_format($item->gross_value,2,',','.')}}</td>
                <td>{{number_format(($item->gross_value - $item->net_value),2,',','.')}}</td>
                <td>
                    @if (is_array($item->detail))
                        <ul>
                        @foreach ($item->detail as $id_key => $itemdetail)
                            <li>{{$id_key}} : {{$itemdetail}}</li>
                        @endforeach
                        </ul>
                    @else
                        -
                    @endif
                </td>
                <td><button class="btn btn-warning pull-right btnEditPenghasilanUtama" value="{{$item->id}}|{{$item->income_step}}">Edit</button></td>
            </tr>  
            @php $i++; @endphp
        @endforeach
    @endforeach
@endif

