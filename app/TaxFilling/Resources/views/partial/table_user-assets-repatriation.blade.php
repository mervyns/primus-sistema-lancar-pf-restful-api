<table id="table_assets_repatriation" class="table table-hover" align="center">
    <thead>
        <tr>
            <th>Pilih</th>
            <th>Kode Harta</th>
            <th>Nama Harta</th>
            <th>Tahun Perolehan</th>
            <th>Alamat</th>
            <th>Nilai Harta</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($filtered))
            @foreach($filtered as $f_key => $data)
                <tr id='user_assets_{{$f_key}}'>
                    <td><input class='checkme' type="checkbox" name="checkbox_{{$f_key}}" value=""></td>
                    <td>{{$data['asset']['asset_code']}}</td>
                    <td>{{$data['asset']['name']}}</td>
                    <td>{{empty($data['acquisition_year']) ? '-' : $data['acquisition_year']}}</td>
                    <td>{{empty($data['address']) ? '-' : $data['address']}}</td>
                    <td>{{empty($data['idr_value']) ? '-' : number_format($data['idr_value'] , 0 , '.' , '.' )}}</td>
                    <td>{{empty($data['info']) ? '-' : $data['info']}}</td> 
                    <td style="display:none;">{{$data['id']}}
                    <td style="display:none;">{{$data['user_id']}}
                    <td style="display:none;">{{$data['currency']}}
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
