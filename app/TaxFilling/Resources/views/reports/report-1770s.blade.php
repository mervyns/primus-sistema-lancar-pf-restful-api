@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray"></h3>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <br><br>
                                <!-- <table class="table table-bordered" style="width: 75%">
                                    <tr height="100px">
                                        <td class="vertical-text" width="3%">FORMULIR</td>
                                        <td width="20%" align="center">
                                            <h1>1770 S</h1>
                                            <p><strong>KEMENTRIAN KEUANGAN RI DIREKTORAT JENDRAL PAJAK</strong></p>
                                        </td>
                                        <td width="50%" align="center">
                                            <p><strong>SPT TAHUNAN <br> PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI</strong></p>
                                        </td>
                                        <td width="5%"></td>
                                        <td align="center" width="20%">
                                            <h1>2018</h1>
                                        </td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="5"><strong>Permohonan perubahan data disampaikan terpisah dari pelaporan SPT Tahunan PPh Orang Pribadi ini, dengan menggunakan Formulir Perubahan Data Wajib Pajak dan dilengkapi dokumen yang disyaratkan.</strong></td>
                                    </tr>
                                </table> -->
                                <!-- <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                    <th class="vertical"><div class="vertical">Really long and complex title 1</div></th>
                                    <th class="vertical">haha</th>
                                    <th class="vertical"><div class="vertical">Really long and complex title 3</div></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                    <td>Example</td>
                                    <td>a, b, c</td>
                                    <td>1, 2, 3</td>
                                    </tr>
                                    </tbody>
                                </table> -->
                                <table border="1" style="width: 100%">
                                    <tr height="100px">
                                        <td width="3%" align="left"><div><span class="vertical-txt"><strong>FORMULIR<strong></span></div></td>
                                        <td width="20%" align="center"><h1>1770 S</h1><p><strong>KEMENTERIAN KEUANGAN RI DIREKTORAT JENDERAL PAJAK</strong></p></td>
                                        <td widht="10%"></td>
                                        <td width="3%" align="left"><span class="vertical-txt"><strong>TAHUN PAJAK<strong></span></td>
                                        <td width="20%" align="center"><h1>{{ date('Y') }}</h1><p><strong></strong> <br><br> SPT PEMBETULAN KE - ...</p></td>
                                    </tr>
                                </table>
                                <table class="" style="width: 100%">
                                    <tr>
                                        <td widht="10%"><strong>PERHATIAN</strong></td>
                                        <td width="25%"><ul style="margin-bottom: 0; font-size:9px; margin:0; padding: 0;"><li><small>SEBELUM MENGISI BACA DAHULU PETUNJUK PENGISIAN</small></li></ul></td>
                                        <td width="25%"><ul style="margin-bottom: 0; font-size:9px; margin:0; padding: 0;"><li><small>ISI DENGAN HURUF CETAK /DIKETIK DENGAN TINTA HITAM</small></li></ul></td>
                                        <td width="10%"><ul style="margin-bottom: 0; font-size:9px; margin:0; padding: 0;"><li><small>BERI TANDA "X" PADA</small></li></ul></td>
                                        <td width="30%"><ul style="margin-bottom: 0; font-size:9px; margin:0; padding: 0;"><li><small> (KOTAK PILIHAN) YANG SESUAI</small></li></ul></td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
