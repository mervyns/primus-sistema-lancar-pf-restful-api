<div class="boxed-body w-100 text-left">
        <form id="cash" class="main-form" method="post" action="{{ Request::url() }}">
            {{ csrf_field() }}
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ empty($form_data['cash_asset']) ? '1' : count($form_data['cash_asset']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>

                <div id="cash-form-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="tab-links" id="tabs">
                                    @if(empty($form_data['cash_asset']))
                                        <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                    @else
                                        @foreach($form_data['cash_asset'] as $cashAsset)
                                            <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                                <div class="tab-content" id="tab-content">
                                    @if(empty($form_data['cash_asset']))
                                        <div id="tab1" class="tab active">
                                            <div class="row cash-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="asset-id[]" value="">
                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="cash" class="text-md-right text-sm-left text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Uang Tunai per Akhir Periode
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" id="cash" name="cash[]" value="" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @foreach($form_data['cash_asset'] as $cashAsset)
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                                <div class="row cash-form">
                                                    <div class="col-md-12 col-md-offset-2">
                                                        
                                                            
                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                        <br>

                                                        
                                                        <input type="hidden" id="id" name="asset-id[]" value="{{ $cashAsset['id'] }}">
                                                        <div class="form-group row">
                                                            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Mata Uang
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                                                                    @foreach($form_data['country'] as $country)
                                                                        <option value="{{ $country['id'] }}" {{ isset($cashAsset['currency']) && $cashAsset['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="cash" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Uang Tunai per Akhir Periode
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format balance"
                                                                    id="cash" name="cash[]" value="{{ isset($cashAsset['currency_value']) ? $cashAsset['currency_value'] : '' }}" required="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Kurs Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="kurs_pajak" name="kurs_pajak[]" 
                                                                    value="{{ isset($cashAsset['kurs_value']) ? $cashAsset['kurs_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Jumlah Rupiah
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                                        value="{{ isset($cashAsset['idr_value']) ? $cashAsset['idr_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>

                <div id="deleted">
                    
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
                </div>
            </div>
        </form>
    </div>

    <div id="cash-form-template" class="row cash-form" style="display: none">
        <div class="col-md-12 col-md-offset-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
                </div>
            </div>
            <br>
            <input type="hidden" name="asset-id[]" value="">
            <div class="form-group row">
                <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Mata Uang
                </label>
                <div class="col-sm-5">
                    <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                        @foreach($form_data['country'] as $country)
                            <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="cash" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Uang Tunai per Akhir Periode
                </label>
                <div class="col-sm-5">
                    <input type="text"
                        class="form-control form-control-sm input-money-format balance"
                        id="cash" name="cash[]" value="" required="">
                </div>
            </div>
            <div class="form-group row">
                <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Kurs Pajak
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Jumlah Rupiah
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                </div>
            </div>
            
        </div>
    </div>
    