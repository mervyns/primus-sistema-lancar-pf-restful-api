<div class="boxed-body w-100 text-left">
    <form id="honorarium-commissioner-form" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-honorarium-commissioner" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links" id="tabs">
                            @if (!empty($form_data['data']))  
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @else  
                                        <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @endif
                                @endforeach   
                            @else
                                <li id="li1"><a href="#tab1">#1</a></li>
                            @endif
                        </ul>
                        <div class="tab-content" id="tab-content">
                            @if (!empty($form_data['data']))
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-brutto" class="text-md-right text-sm-left col-sm-5  col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-3">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                                                    data-title="Nilai Bruto">SS Image</a>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm">SS Image</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-pph" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        PPh yang dipungut
                                                    </label>
                                                    <div class="col-sm-3">
                                                            <input type="text"  pph-id="{{$i_key + 1}}" class="form-control input-money-format" id="honorarium-commissioner-pph" name="pph[{{$i_key + 1}}]" value="{{$item['detail']['pph']}}" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/pph_yang_telah_dipotong.PNG') }}"
                                                    data-title="PPh yang dipungut">SS Image</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                            <input type="text" class="form-control input-money-format" id="honorarium-commissioner-netto" name="netto[{{$i_key + 1}}]" value="{{$item['net_value']}}" autocomplete="off" style="width:100%" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemotong
                                                    </label>
                                                    <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="honorarium-commissioner-cutter-name" name="cutter-name[{{$i_key + 1}}]" value="{{$item['detail']['nama_pemotong']}}" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nama_pemotong.PNG') }}"
                                                    data-title="Nama Pemotong">SS Image</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        NPWP Pemotong
                                                    </label>
                                                    <div class="col-sm-3">
                                                            <input type="text" npwp-id="{{$i_key + 1}}" class="form-control input-npwp-format" id="honorarium-commissioner-cutter-npwp" name="npwp[{{$i_key + 1}}]" value="{{$item['detail']['npwp_pemotong']}}" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/npwp_pemotong.PNG') }}"
                                                    data-title="NPWP Pemotong">SS Image</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nomor Bukti PPh
                                                    </label>
                                                    <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="honorarium-commissioner-pph-proof" name="proof[{{$i_key + 1}}]" value="{{$item['detail']['nomor_bukti_potong']}}" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nomor_bukti_potong.PNG') }}"
                                                    data-title="Nomor Bukti PPh">SS Image</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-commissioner-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal Bukti PPh
                                                    </label>
                                                    <div class="col-sm-3">
                                                            <input type="text" class="form-control" id="honorarium-commissioner-pph-date" name="date[{{$i_key + 1}}]" value="{{$item['detail']['tanggal_bukti_potong']}}" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/tanggal_bukti_potong.PNG') }}"
                                                    data-title="Tanggal Bukti PPh">SS Image</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div id="tab1" class="tab active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="id[0]" id="id" value="0">
                                            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-brutto" class="text-md-right text-sm-left col-sm-5  col-form-label col-form-label-sm">
                                                    Nilai Bruto
                                                </label>
                                                <div class="col-sm-3">
                                                        <input brutto-id="1" type="text" class="form-control" id="honorarium-commissioner-brutto" name="brutto[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                                                    data-title="Nilai Bruto">SS Image</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-pph" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    PPh yang dipungut
                                                </label>
                                                <div class="col-sm-3">
                                                        <input type="text" pph-id="1" class="form-control" id="honorarium-commissioner-pph" name="pph[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/pph_yang_telah_dipotong.PNG') }}"
                                                    data-title="PPh yang dipungut">SS Image</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nilai Netto
                                                </label>
                                                <div class="col-sm-5">
                                                        <input type="text" class="form-control" id="honorarium-commissioner-netto" name="netto[0]" value="" autocomplete="off" style="width:100%" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nama Pemotong
                                                </label>
                                                <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="honorarium-commissioner-cutter-name" name="cutter-name[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nama_pemotong.PNG') }}"
                                                    data-title="Nama Pemotong">SS Image</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    NPWP Pemotong
                                                </label>
                                                <div class="col-sm-3">
                                                        <input type="text" npwp-id="1" class="form-control" id="honorarium-commissioner-cutter-npwp" name="npwp[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/npwp_pemotong.PNG') }}"
                                                    data-title="NPWP Pemotong">SS Image</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nomor Bukti PPh
                                                </label>
                                                <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="honorarium-commissioner-pph-proof" name="proof[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nomor_bukti_potong.PNG') }}"
                                                    data-title="Nomor Bukti PPh">SS Image</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-commissioner-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Tanggal Bukti PPh
                                                </label>
                                                <div class="col-sm-3">
                                                        <input type="text" class="form-control" id="honorarium-commissioner-pph-date" name="date[0]" value="" autocomplete="off" style="width:100%" required>
                                                </div>
                                                <div class="col-sm-2">
                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/tanggal_bukti_potong.PNG') }}"
                                                    data-title="Tanggal Bukti PPh">SS Image</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                           
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="deleted">
                
            </div>

            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save</button>
                    {{--@endif</div>--}}
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            @endif
        </div>
    </form>

<div id="tab-honorarium-commissioner" class="tab-transfer-building-rights-form" style="display: none">
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" name="id[0]" id="id" value="0">
            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
            <div class="form-group row">
                <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                </div>
            </div>
            <div class="form-group row">
                                            <label for="honorarium-commissioner-brutto" class="text-md-right text-sm-left col-sm-5  col-form-label col-form-label-sm">
                                                Nilai Bruto
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-brutto" name="brutto[0]" value="" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                                                    data-title="Nilai Bruto">SS Image</a>
                                                    </div>
                                                
                                        </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-pph" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                PPh yang dipungut
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-pph" name="pph[0]" value="" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/pph_yang_telah_dipotong.PNG') }}"
                                                    data-title="PPh yang dipungut">SS Image</a>
                                                    </div>
                                                </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Nilai Netto
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-netto" name="netto[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Nama Pemotong
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-cutter-name" name="cutter-name[0]" value="" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nama_pemotong.PNG') }}"
                                                    data-title="Nama Pemotong">SS Image</a>
                                                    </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                NPWP Pemotong
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-cutter-npwp" name="npwp[0]" value="" autocomplete="off" style="width:100%" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/npwp_pemotong.PNG') }}"
                                                    data-title="NPWP Pemotong">SS Image</a>
                                                    </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Nomor Bukti PPh
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-pph-proof" name="proof[0]" value="" autocomplete="off" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/nomor_bukti_potong.PNG') }}"
                                                    data-title="Nomor Bukti PPh">SS Image</a>
                                                    </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="honorarium-commissioner-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Tanggal Bukti PPh
                                            </label>
                                            <div class="col-sm-3">
                                                    <input type="text" class="form-control" id="honorarium-commissioner-pph-date" name="date[0]" value="" autocomplete="off" required>
                                                    </div>
                                                    <div class="col-sm-2 ">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                    data-image="{{ url('/storages/ss-image/main-income/tanggal_bukti_potong.PNG') }}"
                                                    data-title="Tanggal Bukti PPh">SS Image</a>
                                                    </div>
                                        </div>
        </div>
    </div>
</div>
</div>
<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>