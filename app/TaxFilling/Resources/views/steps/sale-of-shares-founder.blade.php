<div class="boxed-body w-100 text-left">
        <form id="sale-of-shares-form" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if (!empty($form_data['is_final_steps_submited']))
                    <input type="hidden" name="close-signal" id="close-signal" value="1">
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
    
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-sale-of-shares" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                                <ul class="tab-links" id="tabs">
                                    @if (!empty($form_data['data']))  
                                        @foreach ($form_data['data'] as  $i_key =>  $item)
                                            @if ($item['active'])
                                                <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                            @else  
                                                <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                            @endif
                                            
                                        @endforeach   
                                    @else
                                        <li id="li1"><a href="#tab1">#1</a></li>
                                    @endif
                                </ul>
                            <div class="tab-content" id="tab-content">
                                @if (!empty($form_data['data']))
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                        </div>
                                                    </div>
                                                    {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                                    
                                                    <div class="form-group row">
                                                        <label for="sale-of-shares-stock" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Modal Saham 
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" stock-id="{{$i_key + 1}}" class="form-control input-money-format" id="sale-of-shares-stock" name="stock[{{$i_key}}]" value="{{$item['net_value']}}" autocomplete="off"  required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="sale-of-shares-price" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                           Harga Jual Saham
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" stock-id="{{$i_key + 1}}" class="form-control input-money-format" id="sale-of-shares-price" name="price[{{$i_key}}]" value="{{$item['gross_value']}}" autocomplete="off"  required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="sale-of-shares-profit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Laba / kerugian penjualan saham
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" class="form-control input-money-format" id="sale-of-shares-profit" name="profit[{{$i_key}}]" value="{{$item['gross_value'] - $item['net_value']}}" autocomplete="off"  readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="sale-of-shares-corporation" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Perusahaan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" class="form-control" id="sale-of-shares-corporation" name="corporation[{{$i_key}}]" value="{{$item['detail']['perusahaan']}}" autocomplete="off"  required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div id="tab1" class="tab active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id[0]" id="id" value="0">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                                
                                                <div class="form-group row">
                                                    <label for="sale-of-shares-stock" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Modal Saham 
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text"  stock-id="1" class="form-control input-money-format" id="sale-of-shares-stock" name="stock[0]" value="" autocomplete="off"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="sale-of-shares-price" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                       Harga Jual Saham
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" price-id="1" class="form-control input-money-format" id="sale-of-shares-price" name="price[0]" value="" autocomplete="off"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="sale-of-shares-profit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Laba / kerugian penjualan saham
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control input-money-format" id="sale-of-shares-profit" name="profit[0]" value="" autocomplete="off"  readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="sale-of-shares-corporation" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Perusahaan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="sale-of-shares-corporation" name="corporation[0]" value="" autocomplete="off"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="deleted">
                    
                </div>
    
                @if (\Session::get('is_final_step'))
                    <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                        <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save</button>
                {{--@endif</div>--}}
                    </div>
                @else
                    <div class="row">
                        <hr class="col-md-12">
                        <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                        </div>
                    </div>
                @endif
            </div>
        </form>
    </div>
    
    <div id="sale-of-shares-template" class="tab-transfer-building-rights-form" style="display: none">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="id[0]" id="id" value="0">
                {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                <div class="form-group row">
                    <div class="col-sm-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab" >Hapus</a>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sale-of-shares-stock" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Modal Saham 
                    </label>
                    <div class="col-sm-5">
                            <input type="text" class="form-control" id="sale-of-shares-stock" name="stock[0]" value="" autocomplete="off" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sale-of-shares-price" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                       Harga Jual Saham
                    </label>
                    <div class="col-sm-5">
                            <input type="text" class="form-control" id="sale-of-shares-price" name="price[0]" value="" autocomplete="off" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sale-of-shares-profit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Laba / kerugian penjualan saham
                    </label>
                    <div class="col-sm-5">
                            <input type="text" class="form-control" id="sale-of-shares-profit" name="profit[0]" value="" autocomplete="off" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sale-of-shares-corporation" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nama Perusahaan
                    </label>
                    <div class="col-sm-5">
                            <input type="text" class="form-control" id="sale-of-shares-corporation" name="corporation[0]" value="" autocomplete="off" required>
                    </div>
                </div>
            </div>
        </div>        
    </div>