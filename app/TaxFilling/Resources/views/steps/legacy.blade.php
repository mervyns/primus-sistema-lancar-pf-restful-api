<div class="boxed-body w-100 text-left">
        <form id="legacy" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if (!empty($form_data['is_final_steps_submited']))
                    <input type="hidden" name="close-signal" id="close-signal" value="1">
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ empty($form_data['data']) ? '1' : count($form_data['data']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if (!empty($form_data['data']))  
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @else  
                                            <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @endif
                                        
                                    @endforeach   
                                @else
                                    <li id="li1"><a href="#tab1">#1</a></li>
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                            @if (!empty($form_data['data']))  
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                        <div class="row">
                                            <div class="col-md-12 col-md-offset-2">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                    </div>
                                                </div>
                                            <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis
                                                    </label>
                                                    <div class="col-sm-4"> 
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            @if ($item['income_id'] == 25)
                                                                <input type="radio" class="custom-control-input" name="type[{{$i_key}}]" value="25" checked>
                                                            @else
                                                                <input type="radio" class="custom-control-input" name="type[{{$i_key}}]" value="25">
                                                            @endif
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">Warisan</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            @if ($item['income_id'] == 26)
                                                                <input type="radio" class="custom-control-input" name="type[{{$i_key}}]" value="26" checked>
                                                            @else
                                                                <input type="radio" class="custom-control-input" name="type[{{$i_key}}]" value="26">
                                                            @endif
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">Hibah</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group has-error row">
                                                    <label for="legacy-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format" id="legacy-value" name="nilai[{{$i_key}}]" value="{{$item['net_value']}}" autocomplete="off" required>
                                                    </div>
                                                </div>   
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Pemberi Warisan/Hibah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" id="legacy-giver" name="pemberi[{{$i_key}}]" value="{{$item['detail']['pemberi']}}" autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach   
                            @else
                                <div id="tab1" class="tab active">
                                    <div class="row">
                                        <div class="col-md-12 col-md-offset-2">
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="id[0]" id="id" value="0">
                                            <div class="form-group row">
                                                <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis
                                                </label>
                                                <div class="col-sm-5"> 
                                                    <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio" class="custom-control-input" name="type[0]" value="25" checked>
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">Warisan</span>
                                                    </label>
                                                    <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio" class="custom-control-input" name="type[0]" value="26">
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">Hibah</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group has-error row">
                                                <label for="legacy-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nilai
                                                </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control form-control-sm input-money-format" id="legacy-value" name="nilai[0]" value="" autocomplete="off" required>
                                                </div>
                                            </div>       
                                            <div class="form-group row">
                                                <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemberi Warisan/Hibah
                                                </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control form-control-sm" id="legacy-giver" name="pemberi[0]" value="" autocomplete="off" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="deleted">
                    
                </div>
                @if (\Session::get('is_final_step'))
                    <hr class="col-md-12">
                    <div class="col-md-12 center" align="right">
                    {{--@if (!empty($tooltip['content']))<div class="margin">--}}
                        <button type="submit" class="btn btn-info btn-lg">Save</button>
                        {{--@endif</div>--}}
                    </div>
                @else
                    <div class="row">
                        <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                        </div>
                    </div>
                @endif
            </div>
        </form>
    </div>
    
    <div id="legacy-form-template" class="legacy-form" style="display: none">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                    </div>
                </div>
                <input type="hidden" name="id[]" id="id">
                <div class="form-group row">
                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Jenis
                    </label>
                    <div class="col-sm-5"> 
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="type[0]" value="25">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Warisan</span>
                        </label>
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="type[0]" value="26">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Hibah</span>
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai
                    </label>
                    <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm input-money-format" name="nilai[]" value="" autocomplete="off" required>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Nama Pemberi Warisan/Hibah
                    </label>
                    <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm" name="pemberi[]" value="" autocomplete="off" required>
                    </div>
                </div>
            </div>
        </div>
    </div>