<div class="boxed-body w-100 text-left"> 
        <form id="dependants-form" class="" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-12">
                {{ csrf_field() }}
                @if(!empty($form_data['spouse']))
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label>
                                    Nama Lengkap
                                </label>
                                    <input type="text"
                                    class="form-control form-control-sm" 
                                    id="name" name="spouse-name" value="{{ isset($form_data['spouse']['name']) ? $form_data['spouse']['name'] : '' }}" placeholder="" disabled >
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    NIK
                                </label>
                                <input type="text" class="form-control form-control-sm" 
                                id="nik" name="spouse-nik" value="{{ isset($form_data['spouse']['ktp_number']) ? $form_data['spouse']['ktp_number'] : '' }}" placeholder="" disabled >
                            </div>
                            <div class="col-sm-3">
                                <label >
                                Hubungan
                                </label>
                                    <select class="form-control form-control-sm" id="spouse-relation" name="spouse-relation" style="width: 100%;" disabled required>
                                        <option value=""></option>
                                        @if(isset($form_data['dependant_relation']))
                                        @foreach($form_data['dependant_relation'] as $dr)
                                            <option value="{{ $dr['id'] }}" {{ isset($form_data['spouse']['dependant_relation_id']) && $form_data['spouse']['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                Pekerjaan
                                </label>
                                <select class="form-control form-control-sm" id="spouse-job" name="spouse-job" style="width: 100%;" disabled required>
                                    <option value="Pegawai Negeri" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pegawai Negeri Sipil' ? 'selected' : '' }}>Pegawai Negeri Sipil</option>
                                    <option value="Pegawai Swasta" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pegawai Swasta' ? 'selected' : '' }}>Pegawai Swasta</option>
                                    <option value="Pelajar" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                    <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                </select>
                            </div>
                        </div>
                        
                        <hr style="padding-bottom:1%">
                @endif
                @for ($i = 0; $i <  4; $i++)
                <input type="hidden" name="dependant-id[]" value="{{ isset($form_data['dependant'][$i]['id']) ? $form_data['dependant'][$i]['id'] : '' }}">
                    
                        <div class="form-group row">
                            <div class="col-sm-3">
                            <label>
                                Nama Lengkap
                            </label>
                            <input type="text"
                                class="form-control form-control-sm" id="name" name="name[]" 
                                value="{{ isset($form_data['dependant'][$i]['name']) ? $form_data['dependant'][$i]['name'] : '' }}" placeholder="" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }} >
                            </div>
                            <div class="col-sm-3">
                            <label>
                                NIK
                            </label>
                            <input type="text"
                                        class="form-control form-control-sm" 
                                        id="nik_{{ $i }}" name="nik[]" value="{{ isset($form_data['dependant'][$i]['ktp_number']) ? $form_data['dependant'][$i]['ktp_number'] : '' }}" placeholder="" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }}>
                                        <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                            <input type="checkbox"
                                                class="custom-control-input does-not-have-ktp" 
                                                name="nothavektp[]" id="nothavektp_{{ $i }}" data-id="{{ $i }}" 
                                                {{ (isset($form_data['dependant'][$i]['not_have_ktp']) && $form_data['dependant'][$i]['not_have_ktp'] == 1 ) ? 'checked' : '' }}>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description" style="margin-top: 5px;">Tidak memiliki KTP</span>
                                        </label>
                                        <input type="hidden" id="nothavektp_status_{{ $i }}" name="nothavektp_status[]" value="{{ isset($form_data['dependant'][$i]['not_have_ktp']) ? $form_data['dependant'][$i]['not_have_ktp'] : '0' }}">
                            </div>
                            <div class="col-sm-3">
                            <label>
                            Hubungan
                            </label>
                            <select class="form-control form-control-sm dependant-relation" id="dependant_relation_{{ $i }}" name="dependant-relation[]" data-id="{{ $i }}" style="width: 100%;" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }}>
                                        <option value=""></option>
                                        @if(isset($form_data['dependant_relation']))
                                            @foreach($form_data['dependant_relation'] as $dr)
                                                @if($dr['id'] != 1 && $dr['id'] != 2)
                                                <option value="{{ $dr['id'] }}" {{ isset($form_data['dependant'][$i]['dependant_relation_id']) && $form_data['dependant'][$i]['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                            </div>
                            <div class="col-sm-3" >
                            <label>
                            Pekerjaan
                            </label>
                            <select class="form-control form-control-sm job" id="job_{{ $i }}" name="job[]" style="width: 100%;">
                                        <option value=""></option>
                                        @if (!empty($form_data['dependant'][$i]['dependant_relation_id']))
                                            @if ($form_data['dependant'][$i]['dependant_relation_id'] == 5 || $form_data['dependant'][$i]['dependant_relation_id'] == 8)
                                                <option value="Pelajar" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                                <option value="Tidak Bekerja" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Tidak Bekerja' ? 'selected' : '' }}>Tidak Bekerja</option>
                                            @else
                                                <option value="Pekerja Bebas" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Pekerja Bebas' ? 'selected' : '' }}>Pekerja Bebas</option>
                                                <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                            @endif
                                        @endif
                                    </select>

                            </div>
                        </div>
                        <hr style="padding-bottom:1%">
                @endfor
                </div>
                <!--<div class="row scroll">
                    <div class="col-md-12 col-md-offset-2">
                        {{ csrf_field() }}
                        <table class="table table-hover" align="center">
                            <thead>
                                <tr>
                                    <th width="5%">No. </th>
                                    <th width="25%">Nama Lengkap</th>
                                    <th width="20%">NIK</th>
                                    <th width="25%">Hubungan</th>
                                    <th width="20%">Pekerjaan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($form_data['spouse']))
                                <tr>
                                    <td width="5%">1</td>
                                    <td width="25%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="name" name="spouse-name" value="{{ isset($form_data['spouse']['name']) ? $form_data['spouse']['name'] : '' }}" placeholder="" disabled >
                                    </td>
                                    <td width="20%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="nik" name="spouse-nik" value="{{ isset($form_data['spouse']['ktp_number']) ? $form_data['spouse']['ktp_number'] : '' }}" placeholder="" disabled >
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="spouse-relation" name="spouse-relation" style="width: 100%;" disabled required>
                                            <option value=""></option>
                                            @if(isset($form_data['dependant_relation']))
                                                @foreach($form_data['dependant_relation'] as $dr)
                                                    <option value="{{ $dr['id'] }}" {{ isset($form_data['spouse']['dependant_relation_id']) && $form_data['spouse']['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>

                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="spouse-job" name="spouse-job" style="width: 100%;" disabled required>
                                            <option value="Pegawai Negeri" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pegawai Negeri Sipil' ? 'selected' : '' }}>Pegawai Negeri Sipil</option>
                                            <option value="Pegawai Swasta" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pegawai Swasta' ? 'selected' : '' }}>Pegawai Swasta</option>
                                            <option value="Pelajar" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                            <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['spouse']['job']) && $form_data['spouse']['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                        </select>
                                    </td>
                                </tr>
                                @endif
                                @for ($i = 0; $i <4; $i++)
                                <input type="hidden" name="dependant-id[]" value="{{ isset($form_data['dependant'][$i]['id']) ? $form_data['dependant'][$i]['id'] : '' }}">
                                <tr>
                                    <td widht="5%">{{ (empty($form_data['spouse'])) ? $i + 1 : $i + 2 }}</td>
                                    <td width="25%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="name" name="name[]" value="{{ isset($form_data['dependant'][$i]['name']) ? $form_data['dependant'][$i]['name'] : '' }}" placeholder="" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }} >
                                    </td>
                                    <td width="20%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="nik_{{ $i }}" name="nik[]" value="{{ isset($form_data['dependant'][$i]['ktp_number']) ? $form_data['dependant'][$i]['ktp_number'] : '' }}" placeholder="" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }}>
                                            <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                                <input type="checkbox"
                                                    class="custom-control-input does-not-have-ktp" 
                                                    name="nothavektp[]" id="nothavektp_{{ $i }}" data-id="{{ $i }}" 
                                                    {{ (isset($form_data['dependant'][$i]['not_have_ktp']) && $form_data['dependant'][$i]['not_have_ktp'] == 1 ) ? 'checked' : '' }}>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" style="margin-top: 5px;">Tidak memiliki KTP</span>
                                            </label>
                                            <input type="hidden" id="nothavektp_status_{{ $i }}" name="nothavektp_status[]" value="{{ isset($form_data['dependant'][$i]['not_have_ktp']) ? $form_data['dependant'][$i]['not_have_ktp'] : '0' }}">
                                            <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                                <input type="checkbox"
                                                    class="custom-control-input dependant-fill-nik" 
                                                    name="fillnik[]" id="fillnik_{{ $i }}" data-id="{{ $i }}" 
                                                    {{ (isset($form_data['dependant'][$i]['name']) && empty($form_data['dependant'][$i]['ktp_number'])) ? 'checked' : '' }}>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" style="margin-top: 5px;">Saya isi nanti</span>
                                            </label>
                                            <input type="hidden" id="fillnik_status_{{ $i }}" name="fillnik_status[]" value="{{ (isset($form_data['dependant'][$i]['name']) && empty($form_data['dependant'][$i]['ktp_number'])) ? '1' : '0' }}">
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm dependant-relation" id="dependant_relation_{{ $i }}" name="dependant-relation[]" data-id="{{ $i }}" style="width: 100%;" {{ isset($form_data['dependant'][$i]['id']) ? 'required' : '' }}>
                                            <option value=""></option>
                                            @if(isset($form_data['dependant_relation']))
                                                @foreach($form_data['dependant_relation'] as $dr)
                                                    @if($dr['id'] != 1 && $dr['id'] != 2)
                                                    <option value="{{ $dr['id'] }}" {{ isset($form_data['dependant'][$i]['dependant_relation_id']) && $form_data['dependant'][$i]['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm job" id="job_{{ $i }}" name="job[]" style="width: 100%;">
                                            <option value=""></option>
                                            @if (!empty($form_data['dependant'][$i]['dependant_relation_id']))
                                                @if ($form_data['dependant'][$i]['dependant_relation_id'] == 5 || $form_data['dependant'][$i]['dependant_relation_id'] == 8)
                                                    <option value="Pelajar" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                                    <option value="Tidak Bekerja" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Tidak Bekerja' ? 'selected' : '' }}>Tidak Bekerja</option>
                                                @else
                                                    <option value="Pekerja Bebas" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Pekerja Bebas' ? 'selected' : '' }}>Pekerja Bebas</option>
                                                    <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['dependant'][$i]['job']) && $form_data['dependant'][$i]['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                                @endif
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                                @endfor
                                <!-- <tr>
                                    <input type="hidden" name="dependant-id[]" value="{{ isset($form_data['dependant'][0]['id']) ? $form_data['dependant'][0]['id'] : '' }}">
                                    <td width="30%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="name" name="name[]" value="{{ isset($form_data['dependant'][0]['name']) ? $form_data['dependant'][0]['name'] : '' }}" placeholder="" {{ isset($form_data['dependant'][0]['id']) ? 'required' : '' }} >
                                    </td>
                                    <td width="20%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="nik" name="nik[]" value="{{ isset($form_data['dependant'][0]['ktp_number']) ? $form_data['dependant'][0]['ktp_number'] : '' }}" placeholder="" {{ isset($form_data['dependant'][0]['id']) ? 'required' : '' }}>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="dependant-relation" name="dependant-relation[]" style="width: 100%;" {{ isset($form_data['dependant'][0]['id']) ? 'required' : '' }}>
                                            <option value=""></option>
                                            @if(isset($form_data['dependant_relation']))
                                                @foreach($form_data['dependant_relation'] as $dr)
                                                    @if($dr['id'] != 1 && $dr['id'] != 2)
                                                    <option value="{{ $dr['id'] }}" {{ isset($form_data['dependant'][0]['dependant_relation_id']) && $form_data['dependant'][0]['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="job" name="job[]" style="width: 100%;" {{ isset($form_data['dependant'][0]['id']) ? 'required' : '' }}>
                                            <option value=""></option>
                                            <option value="Pegawai Negeri Sipil" {{ isset($form_data['dependant'][0]['job']) && $form_data['dependant'][0]['job'] == 'Pegawai Negeri Sipil' ? 'selected' : '' }}>Pegawai Negeri Sipil</option>
                                            <option value="Pegawai Swasta" {{ isset($form_data['dependant'][0]['job']) && $form_data['dependant'][0]['job'] == 'Pegawai Swasta' ? 'selected' : '' }}>Pegawai Swasta</option>
                                            <option value="Pelajar" {{ isset($form_data['dependant'][0]['job']) && $form_data['dependant'][0]['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                            <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['dependant'][0]['job']) && $form_data['dependant'][0]['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                        </select>
                                </tr>
                                <tr>
                                    <input type="hidden" name="dependant-id[]" value="{{ isset($form_data['dependant'][1]['id']) ? $form_data['dependant'][1]['id'] : '' }}">
                                    <td width="30%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="name" name="name[]" value="{{ isset($form_data['dependant'][1]['name']) ? $form_data['dependant'][1]['name'] : '' }}" placeholder="" >
                                    </td>
                                    <td width="20%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="nik" name="nik[]" value="{{ isset($form_data['dependant'][1]['ktp_number']) ? $form_data['dependant'][1]['ktp_number'] : '' }}" placeholder="" >
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="dependant-relation" name="dependant-relation[]" style="width: 100%;">
                                            <option value=""></option>
                                            @if(isset($form_data['dependant_relation']))
                                                @foreach($form_data['dependant_relation'] as $dr)
                                                    @if($dr['id'] != 1 && $dr['id'] != 2)
                                                    <option value="{{ $dr['id'] }}" {{ isset($form_data['dependant'][1]['dependant_relation_id']) && $form_data['dependant'][1]['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="job" name="job[]" style="width: 100%;">
                                            <option value=""></option>
                                            <option value="Pegawai Negeri Sipil" {{ isset($form_data['dependant'][1]['job']) && $form_data['dependant'][1]['job'] == 'Pegawai Negeri Sipil' ? 'selected' : '' }}>Pegawai Negeri Sipil</option>
                                            <option value="Pegawai Swasta" {{ isset($form_data['dependant'][1]['job']) && $form_data['dependant'][1]['job'] == 'Pegawai Swasta' ? 'selected' : '' }}>Pegawai Swasta</option>
                                            <option value="Pelajar" {{ isset($form_data['dependant'][1]['job']) && $form_data['dependant'][1]['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                            <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['dependant'][1]['job']) && $form_data['dependant'][1]['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <input type="hidden" name="dependant-id[]" value="{{ isset($form_data['dependant'][2]['id']) ? $form_data['dependant'][2]['id'] : '' }}">
                                    <td width="30%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="name" name="name[]" value="{{ isset($form_data['dependant'][2]['name']) ? $form_data['dependant'][2]['name'] : '' }}" placeholder="" >
                                    </td>
                                    <td width="20%">
                                        <input type="text"
                                            class="form-control form-control-sm" 
                                            id="nik" name="nik[]" value="{{ isset($form_data['dependant'][2]['ktp_number']) ? $form_data['dependant'][2]['ktp_number'] : '' }}" placeholder="" >
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="dependant-relation" name="dependant-relation[]" style="width: 100%;">
                                            <option value=""></option>
                                            @if(isset($form_data['dependant_relation']))
                                                @foreach($form_data['dependant_relation'] as $dr)
                                                    @if($dr['id'] != 1 && $dr['id'] != 2)
                                                    <option value="{{ $dr['id'] }}" {{ isset($form_data['dependant'][2]['dependant_relation_id']) && $form_data['dependant'][2]['dependant_relation_id'] == $dr['id'] ? 'selected' : '' }}>{{ $dr['name'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </td>
                                    <td width="20%">
                                        <select class="form-control form-control-sm" id="job" name="job[]" style="width: 100%;">
                                            <option value=""></option>
                                            <option value="Pegawai Negeri Sipil" {{ isset($form_data['dependant'][2]['job']) && $form_data['dependant'][2]['job'] == 'Pegawai Negeri Sipil' ? 'selected' : '' }}>Pegawai Negeri Sipil</option>
                                            <option value="Pegawai Swasta" {{ isset($form_data['dependant'][2]['job']) && $form_data['dependant'][2]['job'] == 'Pegawai Swasta' ? 'selected' : '' }}>Pegawai Swasta</option>
                                            <option value="Pelajar" {{ isset($form_data['dependant'][2]['job']) && $form_data['dependant'][2]['job'] == 'Pelajar' ? 'selected' : '' }}>Pelajar</option>
                                            <option value="Tidak Bekerja/Pensiun" {{ isset($form_data['dependant'][2]['job']) && $form_data['dependant'][2]['job'] == 'Tidak Bekerja/Pensiun' ? 'selected' : '' }}>Tidak Bekerja/Pensiun</option>
                                        </select>
                                    </td>
                                </tr> 
                            </tbody> 
                        </table>
                        <br />
                    </div>
                </div>--> 

                <div class="row">
                    <div class="col-md-12 center" align="right">
                    {{--@if (!empty($tooltip['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                    <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    {{-- <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save &amp; Continue <br class="no">Later</button> --}}
                    {{-- <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Next</a> --}}
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>