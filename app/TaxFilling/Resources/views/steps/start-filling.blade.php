<div class="boxed-body w-100 text-left">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2 text-center">
                <a href="{{ $steps[$current_step['next']]['url'] }}" class="btn btn-link btn-lg">
                    <!-- <i class="fa fa-play-circle-o"></i> -->
                    <img style="width: 300px;" src="{{asset('images/start_filling.png')}}" alt="">
                </a>
            </div>
        </div>
    </div>
</div>