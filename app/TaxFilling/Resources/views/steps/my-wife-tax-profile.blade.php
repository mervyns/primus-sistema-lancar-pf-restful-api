<div class="boxed-body w-100 text-left">
    <form id="wife-tax-profile" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                            Nama Lengkap
                        </label>
                        <div class="col-sm-5">
                            <input type="text"
                            	class="form-control form-control-sm"
                            	id="name" name="name" value="{{ !isset($form_data['name']) ? '' : $form_data['name']}}" placeholder="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                            NIK
                        </label>
                        <div class="col-sm-3">
                            <input type="text"
                            	class="form-control form-control-sm"
                            	id="nik" name="nik" value="{{ !isset($form_data['nik']) ? '' : $form_data['nik']}}" 
                                placeholder="" required="" {{ isset($form_data['nik']) ? (empty($form_data['nik']) ? 'disabled' : '' ) : '' }}>
                        </div>
                        <div class="col-sm-2">
                            <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                <input type="checkbox"
                                    class="custom-control-input" 
                                    name="fillktp" id="fillktp"
                                    {{ isset($form_data['nik']) ? (empty($form_data['nik']) ? 'checked' : '' ) : '' }}>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description" style="margin-top: 5px;">Saya isi nanti</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jobstatus" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                            Status Pekerjaan
                        </label>
                        <div class="col-sm-7">
                        	<label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="jobstatus" id="jobstatus" value="1" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 1 ? 'checked' : ''}} required><span class="custom-control-indicator"></span><span class="custom-control-description">Tidak Bekerja</span>
                                </label>
                            </div>
                            <div class="col-sm-7 offset-sm-5">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio" class="custom-control-input" name="jobstatus" id="jobstatus" value="2" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 2 ? 'checked' : ''}} required><span class="custom-control-indicator"></span><span class="custom-control-description">Bekerja dan mendapat form 1721 A1/A2 dari satu pemberi kerja</span>
                                </label>
                            </div>
                            <div class="col-sm-7 offset-sm-5">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio" class="custom-control-input" name="jobstatus" id="jobstatus" value="3" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 3 ? 'checked' : ''}} required><span class="custom-control-indicator"></span><span class="custom-control-description">Bekerja dan mendapat form 1721 A1/A2 lebih dari satu pemberi kerja</span>
                                </label>
                            </div>
                            <div class="col-sm-7 offset-sm-5">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio" class="custom-control-input" name="jobstatus" id="jobstatus" value="4" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 4 ? 'checked' : ''}} required><span class="custom-control-indicator"></span><span class="custom-control-description">Bekerja tanpa mendapat form 1721 A1/A2</span>
                                </label>
                            </div>
                            <div class="col-sm-7 offset-sm-5">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio" class="custom-control-input" name="jobstatus" id="jobstatus" value="5" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 5 ? 'checked' : ''}} required><span class="custom-control-indicator"></span><span class="custom-control-description">Menjalankan usaha/pekerjaan bebas</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="klu" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                Pekerjaan
                            </label>
                            <div class="col-sm-5">
                                <select class="form-control form-control-sm" id="klu" name="klu" style="width: 100%;" {{ isset($form_data['job_status_id']) && $form_data['job_status_id'] == 1 ? 'disabled' : ''}} required>
                                    <option value=""></option>
                                    <option value="1" {{ isset($form_data['klu_id']) && $form_data['klu_id'] == 1 ? 'selected' : ''}}>Pegawai Negeri Sipil</option>
                                    <option value="2" {{ isset($form_data['klu_id']) && $form_data['klu_id'] == 2 ? 'selected' : ''}}>Pegawai Swasta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>


    <div class="modal fade" id="modal_my_pair_tax_profile_popup" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
                </div>
                <div class="modal-body">
                    <br>
                    <ul>
                        <li align="left">Apabila status pekerjaan anda/istri/suami adalah menjalankan usaha, maka anda wajib menyampaikan SPT menggunakan formulir 1770.</li>
                        <br>
                        <li align="left"><strong>Mohon Maaf</strong> untuk saat ini kami belum menyediakan penyampaian form 1770.</li>
                        <br>
                        <li align="left">Layanan tersebut masih dalam tahap pengembangan, kami akan menginformasikan saat layanan tersebut sudah tersedia.</li>
                    </ul>
                    <br> <br>
                </div>
            </div>
        </div>
    </div>