<div class="boxed-body w-100 text-left">
    <form id="pph25-information1" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            {{ csrf_field() }}
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div id="form-pph25-information1-notification" style="{{ $form_data['exists'] == false ? '' : 'display:none;' }}">
                <div class="col-md-12 col-md-offset-2">
                    <div class="form-group row">
                        <div class="col-md-12" align="center">
                            <label class="col-form-label col-form-label-lg"> Apakah anda melakukan pembayaran setoran setiap bulan (PPh 25) ?</label>
                            <br><br><br>
                            <a href="javascript:void(0)" class="btn btn-info btn-lg" id="btn-pph25-information1-yes" style="width:200px;"> Ya </a>
                            <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-danger btn-lg" id="btn-pph25-information1-no" style="width:200px;"> Tidak </a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="form-pph25-information1-input" style="{{ $form_data['exists'] == true ? '' : 'display:none;' }}">
                <div class="col-md-12 col-md-offset-2" align="center">
                    <div class="form-group row">
                        <div class="col-md-12">
                                <div class="col-md-6">
                                    <label class="col-form-label col-form-label-sm"><br> Berapa Nominalnya (1 Tahun)?</label>
                                    <input type="text" class="form-control form-control-lg input-money-format" id="yearly" name="yearly" value="{{ $form_data['yearly_value'] }}" style="text-align:center;">
                                </div>
                                <div class="col-md-6">
                                    <label class="col-form-label col-form-label-sm"> Anda memilki kewajiban untuk mengangsur <br> PPh 25 setiap bulannya untuk tahun kedepan</label>
                                    <input type="text" class="form-control form-control-lg input-money-format" id="monthly" name="monthly" value="{{ $form_data['monthly_value'] }}" style="text-align:center;" readonly="">
                                </div>
                        </div>
                    </div>
                    <br>
                    <div class="form-group row">
                        <div class="col-md-12" align="center">
                            <label class="col-form-label col-form-label-lg"> Apakah anda ingin menggunakan lampiran tersendiri untuk menghitung PPh 25?</label>
                            <br><br>
                            <a href="javascript:void(0)" class="btn btn-info btn-lg" id="btn-upload-pph25-yes" style="width:200px;"> Ya </a>
                            <a href="javascript:void(0)" class="btn btn-danger btn-lg" id="btn-upload-pph25-no" style="width:200px;"> Tidak </a>
                        </div>
                    </div>
                    <br><br><br><br><br>
                </div>

                <div class="row">
                    <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>


        </div>
    </form>
</div>