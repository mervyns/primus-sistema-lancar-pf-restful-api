<div class="boxed-body w-100 text-left">
    <form method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row" align="center">
                <div class="text-center col-md-12">
                    <div class="btn-group-select btn-group-single-select-reporting">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ url('generate/spt_1770S') }}" target="_blank" class="btn btn-info btn-lg">Preview SPT Anda</a>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ url('product') }}" class="btn btn-info btn-lg">Proceed to payment</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
