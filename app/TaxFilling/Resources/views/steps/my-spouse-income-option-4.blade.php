{{-- <div class="boxed-body w-100 text-left">
        <form id="spouse-income-4" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            
                            <div class="tab-content" id="tab-content">
                                
                                <div id="tab1" class="tab active">
                                    <div class="row spouse-income-4-form">
                                        <div class="col-md-12 col-md-offset-2">                                        
                                            <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">
                                            <div class="form-group row">
                                                <label for="business-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                                    Penghasilan Dalam Negeri dari <br> Usaha dan/atau Pekerjaan Bebas
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                                class="form-control form-control-sm input-money-format" 
                                                                id="business-income" name="business-income" 
                                                                value="{{ isset($form_data['user_income']['domestic_income_from_business']) ? $form_data['user_income']['domestic_income_from_business'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan_bebas.PNG') }}"
                                                            data-title="Penghasilan Dalam Negeri dari Usaha dan/atau Pekerjaan Bebas">
                                                            SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="job-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                                    Penghasilan Dalam Negeri <br> Sehubungan dengan Pekerjaan
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                            class="form-control form-control-sm input-money-format" 
                                                            id="job-income" name="job-income" 
                                                            value="{{ isset($form_data['user_income']['domestic_income_from_jobs']) ? $form_data['user_income']['domestic_income_from_jobs'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                                            data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="overseas-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Penghasilan Luar Negeri
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                                class="form-control form-control-sm input-money-format" 
                                                                id="overseas-income" name="overseas-income" 
                                                                value="{{ isset($form_data['user_income']['overseas_income']) ? $form_data['user_income']['overseas_income'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_luar_negeri.PNG') }}"
                                                            data-title="Penghasilan Luar Negeri">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="charity-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Zakat/Sumbangan Lainnya
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                                class="form-control form-control-sm input-money-format" 
                                                                id="charity-value" name="charity-value" 
                                                                value="{{ isset($form_data['user_income']['charity']) ? $form_data['user_income']['charity'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/zakat_sumbangan_lainnya.PNG') }}"
                                                            data-title="Zakat/Sumbangan Lainnya">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="compensation-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Kompensasi Kehilangan
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                                class="form-control form-control-sm input-money-format" 
                                                                id="compensation-value" name="compensation-value" 
                                                                value="{{ isset($form_data['user_income']['compensation']) ? $form_data['user_income']['compensation'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/kompensasi_kerugian.PNG') }}"
                                                            data-title="Kompensasi Kehilangan">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div> --}}

@if ($form_data['status'] == 1)
<div class="option-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <h6>Pasangan anda belum memiliki akun SPTGO</h6>
                <p>Silahkan undang pasangan anda untuk menggunakan SPTGO</p>
                <br>
                <a href="javascript:void(0)" class="btn btn-info btn-lg" id="invite-spouse-button" style="width:200px;">Invite Now</a>
            </div>
        </div>
    </div>
</div>
@elseif ($form_data['status'] == 2)
<div class="boxed-body w-100 text-left">
    <form id="spouse-income-4" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12" align="center">
                            <p>Pasangan anda tidak melakukan pengisian SPT dengan aplikasi SPTGO. <br>Anda dapat mengisi form ini secara manual namun harus sesuai dengan SPT istri Anda yang akan dilaporkan.</p>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row spouse-income-4-form">
                        <div class="col-md-12 col-md-offset-2">                                        
                            <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">
                            <div class="form-group row">
                                <label for="business-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                    Penghasilan Dalam Negeri dari <br> Usaha dan/atau Pekerjaan Bebas
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="business-income" name="business-income" 
                                                value="{{ isset($form_data['user_income']['domestic_income_from_business']) ? $form_data['user_income']['domestic_income_from_business'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan_bebas.PNG') }}"
                                        data-title="Penghasilan Dalam Negeri dari Usaha dan/atau Pekerjaan Bebas">
                                        SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="job-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                    Penghasilan Dalam Negeri <br> Sehubungan dengan Pekerjaan
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                            class="form-control form-control-sm input-money-format" 
                                            id="job-income" name="job-income" 
                                            value="{{ isset($form_data['user_income']['domestic_income_from_jobs']) ? $form_data['user_income']['domestic_income_from_jobs'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                        data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="overseas-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Penghasilan Luar Negeri
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="overseas-income" name="overseas-income" 
                                                value="{{ isset($form_data['user_income']['overseas_income']) ? $form_data['user_income']['overseas_income'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_luar_negeri.PNG') }}"
                                            data-title="Penghasilan Luar Negeri"
                                            >SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="charity-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Zakat/Sumbangan Lainnya
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="charity-value" name="charity-value" 
                                                value="{{ isset($form_data['user_income']['charity']) ? $form_data['user_income']['charity'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/zakat_sumbangan_lainnya.PNG') }}"
                                            data-title="Zakat/Sumbangan Lainnya"
                                            >SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="compensation-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Kompensasi Kehilangan
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="compensation-value" name="compensation-value" 
                                                value="{{ isset($form_data['user_income']['compensation']) ? $form_data['user_income']['compensation'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/kompensasi_kerugian.PNG') }}"
                                            data-title="Kompensasi Kehilangan"
                                            >SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                {{--@if (!empty($tooltip['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save &amp; Continue <br class="no">Later</button>
                    <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Next</a>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>
@elseif ($form_data['status'] == 3)
<div class="boxed-body w-100 text-left">
    <form id="spouse-income-4" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="row spouse-income-4-form">
                        <div class="col-md-12 col-md-offset-2">                                        
                            <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">
                            <div class="form-group row">
                                <label for="business-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                    Penghasilan Dalam Negeri dari <br> Usaha dan/atau Pekerjaan Bebas
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="business-income" name="business-income" 
                                                value="{{ isset($form_data['user_income']['domestic_income_from_business']) ? $form_data['user_income']['domestic_income_from_business'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                        data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="job-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                    Penghasilan Dalam Negeri <br> Sehubungan dengan Pekerjaan
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                            class="form-control form-control-sm input-money-format" 
                                            id="job-income" name="job-income" 
                                            value="{{ isset($form_data['user_income']['domestic_income_from_jobs']) ? $form_data['user_income']['domestic_income_from_jobs'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                        data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="overseas-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Penghasilan Luar Negeri
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="overseas-income" name="overseas-income" 
                                                value="{{ isset($form_data['user_income']['overseas_income']) ? $form_data['user_income']['overseas_income'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_luar_negeri.PNG') }}"
                                        data-title="Penghasilan Luar Negeri">SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="charity-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Zakat/Sumbangan Lainnya
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="charity-value" name="charity-value" 
                                                value="{{ isset($form_data['user_income']['charity']) ? $form_data['user_income']['charity'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/zakat_sumbangan_lainnya.PNG') }}"
                                        data-title="Zakat/Sumbangan Lainnya">SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="compensation-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Kompensasi Kehilangan
                                </label>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="compensation-value" name="compensation-value" 
                                                value="{{ isset($form_data['user_income']['compensation']) ? $form_data['user_income']['compensation'] : '' }}" autocomplete="off">
                                        </div>
                                        <div class="col-sm-2">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/kompensasi_kerugian.PNG') }}"
                                            data-title="Kompensasi Kehilangan"
                                            >SS Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                    <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-success btn-lg">Next</a>
                </div>
            </div>
        </div>
    </form>
</div>
@elseif ($form_data['status'] == 4)
<div id="send-notification-form">
    <div id="option-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12" align="center">
                    <h6>Pasangan anda tidak melakukan pengisian SPT dengan Aplikasi SPTGO.</h6>
                    <p>Anda dapat membantu mengingatkan pasangan anda agar dapat segera melakukan pengisian SPT <br> <strong>atau</strong> anda isi manual penghasilan pasangan anda.</p>
                    <br>
                    <a href="javascript:void(0)" class="penghasilan btn btn-info btn-lg " id="send-notification-button">Kirim Email Notifikasi ke Pasangan</a>
                    <a href="javascript:void(0)" class="btn btn-primary btn-lg penghasilan" id="fill-spouse-income-button" style="">Isi Manual Penghasilan Pasangan Saya</a>
                </div>      
            </div>
        </div>
    </div>
</div>
<div id="fill-spouse-income-form" style="display:none;">
    <div class="boxed-body w-100 text-left">
        <form id="spouse-income-4" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="row spouse-income-4-form">
                            <div class="col-md-12 col-md-offset-2">                                        
                                <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">
                                <div class="form-group row">
                                    <label for="business-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                        Penghasilan Dalam Negeri dari <br> Usaha dan/atau Pekerjaan Bebas
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <input type="text" 
                                                    class="form-control form-control-sm input-money-format" 
                                                    id="business-income" name="business-income" 
                                                    value="{{ isset($form_data['user_income']['domestic_income_from_business']) ? $form_data['user_income']['domestic_income_from_business'] : '' }}" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                        data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                        data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="job-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm" style="margin-top: -12px;">
                                        Penghasilan Dalam Negeri <br> Sehubungan dengan Pekerjaan
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <input type="text" 
                                                class="form-control form-control-sm input-money-format" 
                                                id="job-income" name="job-income" 
                                                value="{{ isset($form_data['user_income']['domestic_income_from_jobs']) ? $form_data['user_income']['domestic_income_from_jobs'] : '' }}" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_dalam_negeri_dari_usaha_atau_pekerjaan.PNG') }}"
                                            data-title="Penghasilan Dalam Negeri Sehubungan dengan Pekerjaan">SS Image</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="overseas-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Penghasilan Luar Negeri
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <input type="text" 
                                                    class="form-control form-control-sm input-money-format" 
                                                    id="overseas-income" name="overseas-income" 
                                                    value="{{ isset($form_data['user_income']['overseas_income']) ? $form_data['user_income']['overseas_income'] : '' }}" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/penghasilan_luar_negeri.PNG') }}"
                                            data-title="Penghasilan Luar Negeri">SS Image</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="charity-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Zakat/Sumbangan Lainnya
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <input type="text" 
                                                    class="form-control form-control-sm input-money-format" 
                                                    id="charity-value" name="charity-value" 
                                                    value="{{ isset($form_data['user_income']['charity']) ? $form_data['user_income']['charity'] : '' }}" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/zakat_sumbangan_lainnya.PNG') }}"
                                            data-title="Zakat/Sumbangan Lainnya">SS Image</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="compensation-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Kompensasi Kehilangan
                                    </label>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <input type="text" 
                                                    class="form-control form-control-sm input-money-format" 
                                                    id="compensation-value" name="compensation-value" 
                                                    value="{{ isset($form_data['user_income']['compensation']) ? $form_data['user_income']['compensation'] : '' }}" autocomplete="off">
                                            </div>
                                            <div class="col-sm-2">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                            data-image="{{ url('/storages/ss-image/spouse-income-option-4/kompensasi_kerugian.PNG') }}"
                                            data-title="Kompensasi Kehilangan"
                                            >SS Image</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </div>

                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12 center" align="right">
                    {{--@if (!empty($tooltip['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save &amp; Continue <br class="no">Later</button>
                    <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Next</a>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endif

<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_spouse_invite_option_4" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="invite-spouse-option-4" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Invite Istri/Suami Anda</h5>
                    <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
                </div>
                <div class="modal-body">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="email-spouse" class="col-sm-4 col-form-label col-form-label-sm">
                                    Email Istri/Suami
                                </label>
                                <div class="col-sm-8">
                                    <input type="email"
                                        class="form-control form-control-sm"
                                        id="email-spouse" name="email-spouse" value="" placeholder="" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info">Invite Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>