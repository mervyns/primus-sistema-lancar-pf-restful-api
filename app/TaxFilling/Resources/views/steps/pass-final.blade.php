<div class="boxed-body w-100 text-left">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <div id="pass-final-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" id="formdatadiri">
                            <div>
                                <div class="form-group row col-lg-12 form-inline">
                                    <label class="col-lg-2"> NPWP : </label>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;" />
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                </div>
                                <div class="form-group row col-lg-12 form-inline">
                                    <label class="col-lg-2"> NAMA :  </label>
                                    <input class="form-control col-lg-8" name="nama" />
                                </div>
                                <div class="form-group row col-lg-12 form-inline">
                                    <label class="col-lg-2"> ALAMAT : </label>
                                    <input class="form-control col-lg-8" name="alamat" />
                                </div>
                                <div class="form-group row col-lg-12 form-inline">
                                    <label class="col-lg-2"> IKUT TA : </label>
                                    <input type="radio" class="form-control" name="ikutta" value="ya" /> Ya
                                    <input type="radio" class="form-control" name="ikutta" value="tidak" /> Tidak
                                </div>
                            </div>
                        </form>
                         <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Jenis Aset</th>
                                <th>Nama Aset</th>
                                <th>Tahun Perolehan</th>
                                <th>Nominal</th>
                                <th>Kategori</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @foreach($form_data['user_assets'] as $key => $asset)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$asset['asset']['name']}}</td>
                                    <td>{{$asset['asset_name']}}</td>
                                    <td>{{$asset['acquisition_year']}}</td>
                                    <td>{{$asset['nominal']}}</td>
                                    <td>{{$asset['category']}}</td>
                                    <td>
                                        <button class="btn btn-warning" id="btneditpassfinalaset{{$key}}" data='{{json_encode($asset)}}'>Edit</button>
                                        <button class="btn btn-danger">Hapus</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <button class="btn btn-success pull-right"  data-toggle="modal" data-target="#modalassetpassfinal">Add</button>
                         </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Jenis Hutang</th>
                                <th>Nama Hutang</th>
                                <th>Tahun Perolehan</th>
                                <th>Nominal</th>
                                <th>Kategori</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @foreach($form_data['user_hutang'] as $key => $hutang)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$hutang['asset']['name']}}</td>
                                    <td>{{$hutang['asset_name']}}</td>
                                    <td>{{$hutang['acquisition_year']}}</td>
                                    <td>{{$hutang['nominal']}}</td>
                                    <td>{{$hutang['category']}}</td>
                                    <td>
                                        <button class="btn btn-warning" id="btneditpassfinalhutang{{$key}}" data='{{json_encode($hutang)}}'>Edit</button>
                                        <form method="DELETE" id="hutangDelete{{$hutang['id']}}" action="{{ Request::url() }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$hutang['id']}}" />
                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <button class="btn btn-success pull-right"  data-toggle="modal" data-target="#modalhutangpassfinal">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12 center" align="right">
                    @php
                        $next = Request::segment(2) + 1;
                    @endphp
                    <form action="{{url('/tax-filling/' . $next)}}" method="POST" id="continuelater">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$form_data['nominalall']}}" name="nominalall">
                        <button type="submit" class="btn btn-info btn-lg">Save &amp; Continue<br class="no"> Later</button>
                    </form>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="modalassetpassfinal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalassetpassfinaltitle"></h5>
      </div>
      <form id="form_create_assets" class="" method="post" action="{{ Request::url() }}">
      <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Kategori Asset Pas Final</label>
                <div class="col-sm-5">
                    <select name="category" class="form-control select2">
                        @if ($form_data['user']->taxProfile->following_tax_amnesty_2016 == 1)
                            <option value="B1 - Harta Tidak Dilaporkan SPT">B1 - Harta Tidak Dilaporkan SPT</option>
                        @else
                            <option value="A1 - Harta Tidak Dilaporkan SPT">A1 - Harta Tidak Dilaporkan SPT</option>
                        @endif
                        <option value="C1 – Harta dengan Nilai yang Tidak Sesuai">C1 – Harta dengan Nilai yang Tidak Sesuai</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nama Aset</label>
                <div class="col-sm-5">
                    <input class="form-control" id="asset_name" name="asset_name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Jenis Asset</label>
                <div class="col-sm-5">
                    <select name="type" class="form-control select2">
                        @foreach($form_data['liability'] as $key => $htg)
                    <option value="{{$htg['id']}}">{{$htg['liability_code']}} - {{$htg['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Tahun Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="acquisition_year" />
                    <input type="hidden" name="flag" value="1" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nilai Hutang</label>
                <div class="col-sm-5">
                    <input class="form-control" name="nominal" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Negara</label>
                <div class="col-sm-5">
                    <select name="country_id" class="form-control select2">
                        @foreach($form_data['country'] as $key => $country)
                        <option value="{{$country['id']}}">{{$country['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Alamat</label>
                <div class="col-sm-5">
                    <input class="form-control" name="address" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nama Pemberi Hutang</label>
                <div class="col-sm-5">
                    <input class="form-control" name="name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">NPWP</label>
                <div class="col-sm-5">
                    <input class="form-control" name="npwp" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Dokumen Pendukung</label>
                <div class="col-sm-5">
                    <input class="form-control" name="supporting_document"/> 
                </div>
                <button type="button" class="btn btn-success pull-right" href="#" data-toggle="popover" 
                    title="Information" data-content="Nomor register notaris atau bukti pendukung lainnya disertai nama notaris">
                    <i class="fa fa-info"></i>
                </button>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Terkait Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="acquisition_description" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Bentuk Agunan / Jaminan yang di berikan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_type" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Keterangan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="detail" />
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modalhutangpassfinal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalhutangpassfinaltitle"></h5>
      </div>
      <div class="modal-body">
        <form id="form_create_hutang_pass_final" class="" method="post">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Kategori Hutang Pas Final</label>
                <div class="col-sm-5">
                    <input type="hidden" id="flag" name="flag" value="2" />
                    <select name="category" id="category" class="form-control select2">
                        @if ($form_data['user']->taxProfile->following_tax_amnesty_2016 == 1)
                            <option value="B2 - Hutang Tidak Dilaporkan SPT">B2 - Hutang Tidak Dilaporkan SPT</option>
                        @else
                            <option value="A2 - Hutang Tidak Dilaporkan SPT">A2 - Hutang Tidak Dilaporkan SPT</option>
                        @endif
                        <option value="C2 - Hutang dengan Nilai yang Tidak Sesuai">C2 - Hutang dengan Nilai yang Tidak Sesuai</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nama Aset</label>
                <div class="col-sm-5">
                    <input class="form-control" id="asset_name" name="asset_name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Jenis Asset</label>
                <div class="col-sm-5">
                    <select name="type" id="type" class="form-control select2">
                        @foreach($form_data['asset'] as $key => $ast)
                            <option value="{{$ast['id']}}">{{$ast['asset_code']}} - {{$ast['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Tahun Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" id="acquisition_year" name="acquisition_year" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nilai Nominal</label>
                <div class="col-sm-5">
                    <input class="form-control" id="nominal" name="nominal" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Dokumen Penilaian (optional)</label>
                <div class="col-sm-5">
                    <input class="form-control" name="assesment_document_number" id="assesment_document_number" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Negara</label>
                <div class="col-sm-5">
                    <select name="country_id" id="country_id" class="form-control select2">
                        @foreach($form_data['country'] as $key => $country)
                        <option value="{{$country['id']}}">{{$country['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Alamat</label>
                <div class="col-sm-5">
                    <input class="form-control" name="address" id="address"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Atas Nama</label>
                <div class="col-sm-5">
                    <input class="form-control" name="name" id="name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">NPWP</label>
                <div class="col-sm-5">
                    <input class="form-control" name="npwp" id="npwp" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Jenis Dokuman</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_type" id="document_type" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Nomor Dokumen</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_number" id="document_number"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Jumlah Kuantitas Aset</label>
                <div class="col-sm-5">
                    <input class="form-control" name="qty_assets" id="qty_assets" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Satuan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="unit" id="unit" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Keterangan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="detail" id="detail" />
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>