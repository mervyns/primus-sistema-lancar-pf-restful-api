<div class="boxed-body w-100 text-left">
    <form id="what-describe-me" class=".form-data" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="text-center col-lg-2 btn-group-select btn-group-single-select-1">
                    <p><strong>Status Saya</strong></p><br>
                    <div class="row">
                        <div class="col-md-12">
                            <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['marital_status']) ?: $form_data['marital_status'] != '1' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                <i class="fa fa-male" aria-hidden="true" style="font-size: 3em;"></i>
                                <p class="h7">Lajang</p>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button data-value="2" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['marital_status']) ?: $form_data['marital_status'] != '2' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                <i class="fa fa-venus-mars" aria-hidden="true" style="font-size: 3em;"></i>
                                <p style="margin-bottom: 0px; font-size: 1em;">Menikah</p>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button data-value="3" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['marital_status']) ?: $form_data['marital_status'] != '3' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                <i class="fa fa-street-view" aria-hidden="true" style="font-size: 3em;"></i>
                                <p style="margin-bottom: 0px; font-size: 1em;">Cerai</p>
                            </button>
                        </div>
                    </div>
                    <input type="hidden" name="status" id="status" value="{{ isset($form_data['marital_status']) ? $form_data['marital_status'] : '' }}">
                </div>
                <div class="text-center col-lg-2 btn-group-select btn-group-single-select-2">
                    <p><strong>Istri/Suami Saya</strong></p><br>
                    <div class="row">
                        <div class="col-md-12">
                            <button data-value="0" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['companion_has_npwp']) ?: $form_data['companion_has_npwp'] == '0' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                <i class="fa fa-child" aria-hidden="true" style="font-size: 3em;"></i>
                                <p style="margin-bottom: 0px; font-size: 1em;">Tidak Memiliki  <br>NPWP Sendiri</p>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['companion_has_npwp']) ?: $form_data['companion_has_npwp'] == '1' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                <i class="fa fa-users" aria-hidden="true" style="font-size: 3em;"></i>
                                <p style="margin-bottom: 0px; font-size: 1em;">Memiliki NPWP <br> Sendiri</p>
                            </button>
                        </div>
                    </div>
                    <input type="hidden" name="spouse_has_tax_id" id="spouse_has_tax_id" value="{{ isset($form_data['companion_has_npwp']) ? $form_data['companion_has_npwp'] : '' }}">
                </div>
                <div class="text-center col-lg-3 btn-group-select btn-group-single-select-3">
                    <p><strong>Status Kewajiban<br>Perpajakan Suami-Istri</strong></p>
                    <div class="btn-group-single-select-single-npwp">
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['tax_responsibility_status_id']) ?: $form_data['tax_responsibility_status_id'] == '1' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-users" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">KK - Kepala Keluarga</p>
                                </button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="btn-group-single-select-dual-npwp">
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="5" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['tax_responsibility_status_id']) ?: $form_data['tax_responsibility_status_id'] == '5' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-child" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Melaporkan Terpisah</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="2" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['tax_responsibility_status_id']) ?: $form_data['tax_responsibility_status_id'] == '2' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-child" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">HB - Hidup Berpisah</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="3" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['tax_responsibility_status_id']) ?: $form_data['tax_responsibility_status_id'] == '3' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-child" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">PH - Pisah Harta</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="4" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['tax_responsibility_status_id']) ?: $form_data['tax_responsibility_status_id'] == '4' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-child" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">MT - Manajemen Terpisah</p>
                                </button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="spouse_taxing_status" id="spouse_taxing_status" value="{{ isset($form_data['tax_responsibility_status_id']) ? $form_data['tax_responsibility_status_id'] : ''}}">
                </div>

                <div class="text-center col-lg-2">
                    <div class="btn-group-select btn-group-single-select-4">
                        <p><strong>Program Tax Amnesty 2016</strong></p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['following_tax_amnesty_2016']) ?: $form_data['following_tax_amnesty_2016'] != '1' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-institution" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Mengikuti</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="0" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['following_tax_amnesty_2016']) ?: $form_data['following_tax_amnesty_2016'] != '0' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-institution" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Tidak mengikuti</p>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="2016_ta_participation" id="tax-amnesty" value="{{ (isset($form_data['following_tax_amnesty_2016'])) ?  $form_data['following_tax_amnesty_2016'] : ''}}">
                    </div>
                        
                    <div class="btn-group-select btn-group-single-select-6" style="{{ !isset($form_data['following_tax_amnesty_2016']) ? 'display:none;' : ($form_data['following_tax_amnesty_2016'] == 0 ? 'display:none;' : '') }}">
                        <br><br>
                        <p><strong>Tax Amnesty Pengungkapan</strong></p>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['ta_disclosure']) ? '' : $form_data['ta_disclosure'] != 1 ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;" value="">
                                    <i class="fa fa-book" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Pengungkapan</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="0" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['ta_disclosure']) ? '' : $form_data['ta_disclosure'] != 0 ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;" value="">
                                    <i class="fa fa-book" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Pengungkapan <br> & Repatriasi</p>
                                </button>
                            </div>
                        </div>    
                        <input type="hidden" name="pengungkapan" id="pengungkapan" value="{{ !isset($form_data['ta_disclosure']) ? '' : $form_data['ta_disclosure'] }}">
                    </div>
                    <br>
                </div>

                <div class="text-center col-lg-3">
                    <div class="btn-group-select btn-group-single-select-7">
                        <p><strong>Aset Luar Negeri</strong></p>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="1" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['have_foreign_asset']) ?: $form_data['have_foreign_asset'] != '1' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-diamond" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Memiliki Aset Berletak <br> di Luar Negeri <br> (Tabungan, Deposito, dll)</p>
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button data-value="0" type="button" class="btn btn-lg btn-outline-info {{ !isset($form_data['have_foreign_asset']) ?: $form_data['have_foreign_asset'] != '0' ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-dot-circle-o" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Tidak Memiliki Aset Berletak <br> di Luar Negeri <br> (Tabungan, Deposito, dll)</p>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="have_foreign_asset" id="have_foreign_asset" value="{{ (isset($form_data['have_foreign_asset'])) ? $form_data['have_foreign_asset'] : '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <button type="submit" class="btn btn-info btn-lg exc1" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg exc2" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>