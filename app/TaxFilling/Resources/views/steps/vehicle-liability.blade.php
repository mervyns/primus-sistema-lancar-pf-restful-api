<div class="boxed-body w-700 text-left">
    <form id="vehicle-liability" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ (count($form_data['user_liability']) == 0) ? '1' : count($form_data['user_liability']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="vehicle-liability-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_liability']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_liability'] as $row)
                                        <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_liability']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row vehicle-liability-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="liability_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="vehicle-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Polisi/BPKP/STNK
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="vehicle-number" name="vehicle-number[]" style="width: 100%;" required>
                                                            @foreach($form_data['vehicle'] as $vehicle)
                                                                <option value="{{ $vehicle['id'] }}" data-merk="{{ $vehicle['merk'] }}">{{ $vehicle['value'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Merk dan Tipe
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="vehicle-merk" name="vehicle-merk[]" 
                                                            value="{{ isset($form_data['vehicle'][0]) ? $form_data['vehicle'][0]['merk'] : '' }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Tahun Perolehan Kredit
                                                    </label>
                                                    <div class="col-sm-2">
                                                        <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="credite-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nilai Kredit
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                            id="credite-value" name="credite-value[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="lender-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Bank/Leasing
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="lender-name" name="lender-name[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="lender-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Alamat Kreditor
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="lender-address" name="lender-address[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>
                                                
                                                <hr>

                                                <div class="form-group row">
                                                    <label for="credit-duration" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jangka Kredit (Tahun)
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="credit-duration" name="credit-duration[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="paid-off" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal Lunas Kredit
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" 
                                                            id="paid-off" name="paid-off[]" value="{{ date('d/m/Y') }}">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="yearly-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Bunga Pinjaman Per Tahun (%)
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="yearly-interest" name="yearly-interest[]" value="">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['user_liability'] as $row)
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                            <div class="row vehicle-liability-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    
                                                    <input type="hidden" id="id" name="liability_id[]" value="{{ isset($row['id']) ? $row['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="vehicle-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nomor Polisi/BPKP/STNK
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="vehicle-number" name="vehicle-number[]" style="width: 100%;" required>
                                                                @foreach($form_data['vehicle'] as $vehicle)
                                                                    <option value="{{ $vehicle['id'] }}" data-merk="{{ $vehicle['merk'] }}"
                                                                        {{ ($vehicle['id'] == $row['vehicle_id']) ? 'selected' : '' }} }}>{{ $vehicle['value'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Merk dan Tipe
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="vehicle-merk" name="vehicle-merk[]" 
                                                                value="{{ isset($row['vehicle_merk']) ? $row['vehicle_merk'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Negara
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}"
                                                                        {{ $row['country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}"
                                                                    {{ $row['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Tahun Perolehan Kredit
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <input type="number" class="form-control form-control-sm" 
                                                                id="acquisition-year" name="acquisition-year[]" 
                                                                value="{{ isset($row['acquisition_year']) ? $row['acquisition_year'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="credite-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nilai Kredit
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                id="credite-value" name="credite-value[]" 
                                                                value="{{ isset($row['currency_value']) ? $row['currency_value'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="lender-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nama Bank/Leasing
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="lender-name" name="lender-name[]" 
                                                                value="{{ isset($row['lender_name']) ? $row['lender_name'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="lender-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Alamat Kreditor
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="lender-address" name="lender-address[]" 
                                                                value="{{ isset($row['lender_address']) ? $row['lender_address'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                id="kurs_pajak" name="kurs_pajak[]" 
                                                                value="{{ isset($row['kurs_value']) ? $row['kurs_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                                value="{{ isset($row['value']) ? $row['value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>
                                                    
                                                    <hr>

                                                    <div class="form-group row">
                                                        <label for="credit-duration" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jangka Kredit (Tahun)
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="credit-duration" name="credit-duration[]" 
                                                                value="{{ isset($row['credite_duration']) ? $row['credite_duration'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="paid-off" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Lunas Kredit
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" 
                                                                id="paid-off" name="paid-off[]" 
                                                                value="{{ isset($row['paid_off_date']) ? date('d/m/Y', strtotime($row['paid_off_date'])) : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="yearly-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Bunga Pinjaman Per Tahun (%)
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="yearly-interest" name="yearly-interest[]" 
                                                                value="{{ isset($row['yearly_interest']) ? $row['yearly_interest'] : '' }}">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>


<div id="vehicle-liability-form-template" class="row vehicle-liability-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="liability_id[]" value="">
        <div class="form-group row">
            <label for="vehicle-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nomor Polisi/BPKP/STNK
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="vehicle-number" name="vehicle-number[]" style="width: 100%;" required>
                    @foreach($form_data['vehicle'] as $vehicle)
                        <option value="{{ $vehicle['id'] }}" data-merk="{{ $vehicle['merk'] }}">{{ $vehicle['value'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Merk dan Tipe
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="vehicle-merk" name="vehicle-merk[]" 
                    value="{{ isset($form_data['vehicle'][0]) ? $form_data['vehicle'][0]['merk'] : '' }}" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Negara
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Tahun Perolehan Kredit
            </label>
            <div class="col-sm-2">
                <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="credite-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nilai Kredit
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" 
                    id="credite-value" name="credite-value[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="lender-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nama Bank/Leasing
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="lender-name" name="lender-name[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="lender-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Alamat Kreditor
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="lender-address" name="lender-address[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Kurs Pajak
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jumlah Rupiah
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
            </div>
        </div>
        
        <hr>

        <div class="form-group row">
            <label for="credit-duration" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jangka Kredit (Tahun)
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="credit-duration" name="credit-duration[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="paid-off" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Tanggal Lunas Kredit
            </label>
            <div class="col-sm-3">
                <input type="text" class="form-control form-control-sm input-picker-format" 
                    id="paid-off" name="paid-off[]" value="{{ date('d/m/Y') }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="yearly-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Bunga Pinjaman Per Tahun (%)
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="yearly-interest" name="yearly-interest[]" value="">
            </div>
        </div>

    </div>
</div>
