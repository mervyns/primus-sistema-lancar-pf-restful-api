<div class="boxed-body w-100 text-left">
    <form id="precious-asset-global" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>
            <div class="precious-asset-global-form-container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-2">
                        <div id="tab1" class="tab active">
                        <input type="hidden" id="id" name="asset-id[]" value="{{ isset($form_data['user_asset'][0]['id']) ? $form_data['user_asset'][0]['id'] : '' }}">
                        <input type="hidden" id="asset-type" name="asset-type[]" value="22">
                        <div class="form-group row">
                            <label for="gold-total-value" class="text-md-right text-sm-left col-sm-2 col-form-label col-form-label-sm">
                                Logam Mulia (Emas/Perhiasan)
                            </label>
                            <div class="col-sm-2">
                                <label>Mata Uang</label>
                                 <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                    @foreach($form_data['country'] as $country)
                                        <option value="{{ $country['id'] }}" {{ isset($form_data['user_asset'][0]['currency']) && $form_data['user_asset'][0]['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Tahun Perolehan</label>
                                <input type="number" class="form-control form-control-sm" 
                                    id="acquisition-year" name="acquisition-year[]" 
                                    value="{{ isset($form_data['user_asset'][0]['acquisition_year']) ? $form_data['user_asset'][0]['acquisition_year'] : '' }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Nilai Perolehan</label>
                                <input type="text" class="form-control form-control-sm input-money-format balance" 
                                    id="acquisition-value" name="acquisition-value[]" 
                                    value="{{ isset($form_data['user_asset'][0]['currency_value']) ? $form_data['user_asset'][0]['currency_value'] : '' }}" autocomplete="off">
                            </div>
                            <div class="col-sm-2">
                                <label>Kurs Pajak</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="kurs_pajak" name="kurs_pajak[]" 
                                    value="{{ isset($form_data['user_asset'][0]['kurs_value']) ? $form_data['user_asset'][0]['kurs_value'] : '1' }}" readonly="">
                            </div>
                            <div class="col-sm-2">
                                <label>Jumlah Rupiah</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                    value="{{ isset($form_data['user_asset'][0]['idr_value']) ? $form_data['user_asset'][0]['idr_value'] : '0' }}" readonly="">
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-md-offset-2">
                        <div id="tab2" class="tab active">
                        <input type="hidden" id="id" name="asset-id[]" value="{{ isset($form_data['user_asset'][1]['id']) ? $form_data['user_asset'][1]['id'] : '' }}">
                        <input type="hidden" id="asset-type" name="asset-type[]" value="23">
                        <div class="form-group row">
                            <label for="jewel-total-value" class="text-md-right text-sm-left col-sm-2 col-form-label col-form-label-sm">
                                Batu Mulia (Intan/Berlian)
                            </label>
                            <div class="col-sm-2">
                                <label>Mata Uang</label>
                                 <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                    @foreach($form_data['country'] as $country)
                                        <option value="{{ $country['id'] }}" {{ isset($form_data['user_asset'][1]['currency']) && $form_data['user_asset'][1]['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Tahun Perolehan</label>
                                <input type="number" class="form-control form-control-sm" 
                                    id="acquisition-year" name="acquisition-year[]" 
                                    value="{{ isset($form_data['user_asset'][1]['acquisition_year']) ? $form_data['user_asset'][1]['acquisition_year'] : '' }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Nilai Perolehan</label>
                                <input type="text" class="form-control form-control-sm input-money-format balance" 
                                    id="acquisition-value" name="acquisition-value[]" 
                                    value="{{ isset($form_data['user_asset'][1]['currency_value']) ? $form_data['user_asset'][1]['currency_value'] : '' }}" autocomplete="off">
                            </div>
                            <div class="col-sm-2">
                                <label>Kurs Pajak</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="kurs_pajak" name="kurs_pajak[]" 
                                    value="{{ isset($form_data['user_asset'][1]['kurs_value']) ? $form_data['user_asset'][1]['kurs_value'] : '1' }}" readonly="">
                            </div>
                            <div class="col-sm-2">
                                <label>Jumlah Rupiah</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                    value="{{ isset($form_data['user_asset'][1]['idr_value']) ? $form_data['user_asset'][1]['idr_value'] : '0' }}" readonly="">
                            </div>
                        </div>
                        </div>
                    </div>
                     <div class="col-md-12 col-md-offset-2">
                        <div id="tab3" class="tab active">
                        <input type="hidden" id="id" name="asset-id[]" value="{{ isset($form_data['user_asset'][2]['id']) ? $form_data['user_asset'][2]['id'] : '' }}">
                        <input type="hidden" id="asset-type" name="asset-type[]" value="24">
                        <div class="form-group row">
                            <label for="antique-total-value" class="text-md-right text-sm-left col-sm-2 col-form-label col-form-label-sm">
                                Barang Antik
                            </label>
                            <div class="col-sm-2">
                                <label>Mata Uang</label>
                                 <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                    @foreach($form_data['country'] as $country)
                                        <option value="{{ $country['id'] }}" {{ isset($form_data['user_asset'][2]['currency']) && $form_data['user_asset'][2]['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>Tahun Perolehan</label>
                                <input type="number" class="form-control form-control-sm" 
                                    id="acquisition-year" name="acquisition-year[]" 
                                    value="{{ isset($form_data['user_asset'][2]['acquisition_year']) ? $form_data['user_asset'][2]['acquisition_year'] : '' }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Nilai Perolehan</label>
                                <input type="text" class="form-control form-control-sm input-money-format balance" 
                                    id="acquisition-value" name="acquisition-value[]" 
                                    value="{{ isset($form_data['user_asset'][2]['currency_value']) ? $form_data['user_asset'][2]['currency_value'] : '' }}" autocomplete="off">
                            </div>
                            <div class="col-sm-2">
                                <label>Kurs Pajak</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="kurs_pajak" name="kurs_pajak[]" 
                                    value="{{ isset($form_data['user_asset'][2]['kurs_value']) ? $form_data['user_asset'][2]['kurs_value'] : '1' }}" readonly="">
                            </div>
                            <div class="col-sm-2">
                                <label>Jumlah Rupiah</label>
                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                    value="{{ isset($form_data['user_asset'][2]['idr_value']) ? $form_data['user_asset'][2]['idr_value'] : '0' }}" readonly="">
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

    <style>
    @media only screen and (max-width: 480px){
    .col-sm-2, .col-sm-5, .col-sm-3, {
        padding:0;
    }
    }
    </style>