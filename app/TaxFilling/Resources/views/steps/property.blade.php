<div class="boxed-body w-100 text-left">
    <form id="property" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
                <!-- <br> -->

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info"  id="add-tab" data-ai="{{ count($form_data['user_asset']) == 0 ? '1' : count($form_data['user_asset']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br>

            <div id="property-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_asset']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_asset'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['user_asset']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_asset']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row property-form">
                                            <div class="col-md-12 col-md-offset-2">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>

                                                <input type="hidden" id="id" name="asset-id[]" value="">
                                                <div class="form-group row">
                                                    <label for="property-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Properti
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm property-type" id="property-type" name="property-type[]" style="width: 100%;" required>
                                                            <option value="Tanah Kosong" data-id="1">Tanah Kosong</option>
                                                            <option value="Tanah dan Bangunan/Rumah" data-id="2">Tanah dan Bangunan/Rumah</option>
                                                            <option value="Apartemen" data-id="3">Apartemen</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tahun Perolehan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Perolehan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="vehicle-credit" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm required">
                                                        Kredit Properti
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="property-credit[0]" id="property-credit" 
                                                                value="Ya" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Ya</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="property-credit[0]" id="property-credit" 
                                                                value="Tidak"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Tidak</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="property-function" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Penggunaan Properti
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm property-function" id="property-function" name="property-function[]" style="width: 100%;" required>
                                                            <option value=""></option>
                                                            <option value="28">Untuk Tempat Tinggal</option>
                                                            <option value="30">Untuk Usaha (Toko,pabrik,Gudang, dll)</option>
                                                            <option value="29">Untuk Usaha (Pertaniaan, perkebunan, perikanan, dll)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                    
                                                <div class="form-group row">
                                                    <label for="certificate-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Sertifikat / No.
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm" id="certificate-type" name="certificate-type[]" style="width: 100%;" required>
                                                            <option value=""></option>
                                                            {{--<option value="HM">HM</option>
                                                            <option value="HGB">HGB</option>
                                                            <option value="Letter C">Letter C</option>
                                                            <option value="Letter D">Letter D</option>
                                                            <option value="SHKRS">SHKRS (Sertifikat Hak Kepemilikan Rumah Susun)</option>
                                                            <option value="SKGB">SKGB (Sertifikat Kepemilikan Bangunan Gedung)</option>--}}
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control fom-control-sm" id="other-certificate-type" 
                                                        name="other-certificate-type[]" value="" placeholder="Nomor Sertifikat">
                                                    </div>
                                                </div>
                                    
                                                <div class="form-group row">
                                                    <label for="location" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Lokasi
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="location" name="location[]" value="" required>
                                                    </div>
                                                </div>
                                    
                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="form-group row">
                                                    <label for="city" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                                                        Kota
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="city" name="city[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="optional-property-type" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                                                        Tipe Properti
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="optional-property-type" name="optional-property-type[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="property-large" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                                                        Luas
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="property-large" name="property-large[]" value="">
                                                    </div>
                                                </div>
                                            
                                            
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach ($form_data['user_asset'] as $r_key => $row)
                                    @if (isset($row['active']))
                                        @if ($row['active'])
                                            <div id="tab{{$r_key + 1}}" class="tab active">
                                        @else
                                            <div id="tab{{$r_key + 1}}" class="tab">
                                        @endif
                                    @else
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                    @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row property-form">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" id="id" name="asset-id[]" value="{{ isset($row['id']) ? $row['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="property-type" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Jenis Properti
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm property-type" id="property-type" name="property-type[]" style="width: 100%;" required>
                                                                <option value="Tanah Kosong" {{ isset($row['property_type']) && $row['property_type'] == 'Tanah Kosong' ? 'selected' : '' }} data-id="1">Tanah Kosong</option>
                                                                <option value="Tanah dan Bangunan/Rumah" {{ isset($row['property_type']) && $row['property_type'] == 'Tanah dan Bangunan/Rumah' ? 'selected' : '' }} data-id="2">Tanah dan Bangunan/Rumah</option>
                                                                <option value="Apartemen" {{ isset($row['property_type']) && $row['property_type'] == 'Apartemen' ? 'selected' : '' }} data-id="3">Apartemen</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Negara
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ isset($row['country']) && $row['country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ isset($row['currency']) && $row['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="acquisition-year" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Tahun Perolehan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="number" class="form-control form-control-sm" 
                                                                id="acquisition-year" name="acquisition-year[]" 
                                                                value="{{ isset($row['acquisition_year']) ? $row['acquisition_year'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="acquisition-value" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Nilai Perolehan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                id="acquisition-value" name="acquisition-value[]" 
                                                                value="{{ isset($row['currency_value']) ? $row['currency_value'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="vehicle-credit" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Kredit Properti
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="property-credit[{{ $loop->iteration }}]" id="property-credit" 
                                                                    value="Ya" {{ ($row['credite_type'] == 'Ya') ? 'checked' : '' }} checked="">
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Ya</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="property-credit[{{ $loop->iteration }}]" id="property-credit" 
                                                                    value="Tidak" {{ ($row['credite_type'] == 'Tidak') ? 'checked' : '' }}><span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Tidak</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="property-function" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Penggunaan Properti
                                                        </label>
                                                        <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="property-function" name="property-function[]" style="width: 100%;" required>
                                                                <option value=""></option>
                                                                @if (isset($row['property_type']))
                                                                    @if ($row['property_type'] == 'Tanah Kosong')
                                                                    <option value="28" {{ isset($row['asset_id']) && $row['asset_id'] == 28 ? 'selected' : '' }}>Untuk Tempat Tinggal</option>
                                                                    <option value="30" {{ isset($row['asset_id']) && $row['asset_id'] == 30 ? 'selected' : '' }}>Untuk Usaha (Toko,pabrik,Gudang, dll)</option>
                                                                    <option value="29" {{ isset($row['asset_id']) && $row['asset_id'] == 29 ? 'selected' : '' }}>Untuk Usaha (Pertaniaan, perkebunan, perikanan, dll)</option>
                                                                    @elseif ($row['property_type'] == 'Tanah dan Bangunan/Rumah')
                                                                    <option value="28" {{ isset($row['asset_id']) && $row['asset_id'] == 28 ? 'selected' : '' }}>Untuk Tempat Tinggal</option>
                                                                    <option value="30" {{ isset($row['asset_id']) && $row['asset_id'] == 30 ? 'selected' : '' }}>Untuk Usaha (Toko,pabrik,Gudang, dll)</option>
                                                                    @elseif ($row['property_type'] == 'Apartemen')
                                                                    <option value="28" {{ isset($row['asset_id']) && $row['asset_id'] == 28 ? 'selected' : '' }}>Untuk Tempat Tinggal</option>
                                                                    @endif
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="certificate-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Sertifikat / No.
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm" id="certificate-type" name="certificate-type[]" style="width: 100%;" required>
                                                                <option value=""></option>
                                                                @if (isset($row['asset_id']))
                                                                    @if ($row['asset_id'] == 28)
                                                                        @if ($row['property_type'] == 'Apartemen')
                                                                            <option value="SKGB" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'SKGB' ? 'selected' : '' }}>SKGB (Sertifikat Kepemilikan Bangunan Gedung)</option>
                                                                        @else
                                                                            <option value="HM" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'HM' ? 'selected' : '' }}>HM</option>
                                                                            <option value="HGB" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'HGB' ? 'selected' : '' }}>HGB</option>
                                                                            <option value="Letter C" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'Letter C' ? 'selected' : '' }}>Letter C</option>
                                                                            <option value="Letter D" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'Letter D' ? 'selected' : '' }}>Letter D</option>
                                                                            @if ($row['property_type'] == 'Tanah dan Bangunan/Rumah')
                                                                                <option value="SHKRS" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'SHKRS' ? 'selected' : '' }}>SHKRS (Sertifikat Hak Kepemilikan Rumah Susun)</option>
                                                                            @endif
                                                                        @endif
                                                                    @else
                                                                        <option value="HM" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'HM' ? 'selected' : '' }}>HM</option>
                                                                        <option value="HGB" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'HGB' ? 'selected' : '' }}>HGB</option>
                                                                        <option value="Letter C" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'Letter C' ? 'selected' : '' }}>Letter C</option>
                                                                        <option value="Letter D" {{ isset($row['certificate_type']) && $row['certificate_type'] == 'Letter D' ? 'selected' : '' }}>Letter D</option>
                                                                    @endif
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" class="form-control fom-control-sm" 
                                                                id="other-certificate-type" name="other-certificate-type[]" 
                                                                value="{{ isset($row['other_certificate_type']) ? $row['other_certificate_type'] : '' }}" placeholder="Nomor Sertifikat">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="location" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm required">
                                                            Lokasi
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="location" name="location[]" 
                                                                value="{{ isset($row['location']) ? $row['location'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left   col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                id="kurs_pajak" name="kurs_pajak[]" 
                                                                value="{{ isset($row['kurs_value']) ? $row['kurs_value'] : '1' }}" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                            id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                            value="{{ isset($row['idr_value']) ? $row['idr_value'] : '0' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <hr>

                                                    <div class="form-group row">
                                                        <label for="city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kota
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="city" name="city[]" 
                                                                value="{{ isset($row['city']) ? $row['city'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="optional-property-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tipe Properti
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="optional-property-type" name="optional-property-type[]" 
                                                                value="{{ isset($row['other_certificate_type']) ? $row['other_certificate_type'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="property-large" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Luas
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="property-large" name="property-large[]" 
                                                                value="{{ isset($row['property_large']) ? $row['property_large'] : '' }}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <br> -->
            </div>

            <div id="deleted">
                
            </div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<div id="property-form-template" class="row property-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <!-- <br> -->

        <input type="hidden" id="id" name="asset-id[]" value="">
        <div class="form-group row">
            <label for="property-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Jenis Properti
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm property-type" id="property-type" name="property-type[]" style="width: 100%;" required>
                    <option value="Tanah Kosong" data-id="1">Tanah Kosong</option>
                    <option value="Tanah dan Bangunan/Rumah" data-id="2">Tanah dan Bangunan/Rumah</option>
                    <option value="Apartemen" data-id="3">Apartemen</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Negara
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                    <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                    <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Tahun Perolehan
            </label>
            <div class="col-sm-5">
                <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Perolehan
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="vehicle-credit" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm required">
                Kredit Properti
            </label>
            <div class="col-sm-5">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                    class="custom-control-input" name="property-credit[0]" id="property-credit" 
                    value="Ya" checked="">
                    <span class="custom-control-indicator">
                    </span><span class="custom-control-description">Ya</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="property-credit" 
                        value="Tidak"><span class="custom-control-indicator">
                        </span><span class="custom-control-description" checked>Tidak</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="property-function" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Penggunaan Properti
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm property-function" id="property-function" name="property-function[]" style="width: 100%;" required>
                    <option value=""></option>
                    <option value="28">Untuk Tempat Tinggal</option>
                    <option value="30">Untuk Usaha (Toko,pabrik,Gudang, dll)</option>
                    <option value="29">Untuk Usaha (Pertaniaan, perkebunan, perikanan, dll)</option>
                </select>
            </div>
        </div>
                                    
        <div class="form-group row">
            <label for="certificate-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jenis Sertifikat / No.
            </label>
            <div class="col-sm-3">
                <select class="form-control form-control-sm" id="certificate-type" name="certificate-type[]" style="width: 100%;" required>
                    <option value=""></option>
                    {{--<option value="HM">HM</option>
                    <option value="HGB">HGB</option>
                    <option value="Letter C">Letter C</option>
                    <option value="Letter D">Letter D</option>
                    <option value="SHKRS">SHKRS (Sertifikat Hak Kepemilikan Rumah Susun)</option>
                    <option value="SKGB">SKGB (Sertifikat Kepemilikan Bangunan Gedung)</option>--}}
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control fom-control-sm" id="other-certificate-type" 
                name="other-certificate-type[]" value="">
            </div>
        </div>
                                    
        <div class="form-group row">
            <label for="location" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Lokasi
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="location" name="location[]" value="" required>
            </div>
        </div>
                                    
        <div class="form-group row">
            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Kurs Pajak
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jumlah Rupiah
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
            </div>
        </div>

            <hr>

        <div class="form-group row">
            <label for="city" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                Kota
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="city" name="city[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="optional-property-type" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                Tipe Properti
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="optional-property-type" name="optional-property-type[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="property-large" class="text-md-right text-sm-left  col-sm-5 col-form-label col-form-label-sm">
                Luas
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="property-large" name="property-large[]" value="">
            </div>
        </div>

    </div>
</div>