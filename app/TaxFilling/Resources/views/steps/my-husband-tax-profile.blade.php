<div class="boxed-body w-100 text-left">
        <form id="husband-tax-profile" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12 col-md-offset-2">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                Nama Lengkap
                            </label>
                            <div class="col-sm-5">
                                <input type="text"
                                    class="form-control form-control-sm"
                                    id="name" name="name" value="{{isset($form_data['name']) ? $form_data['name'] : ''}}" placeholder="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                NIK
                            </label>
                            <div class="col-sm-3">
                                <input type="text"
                                    class="form-control form-control-sm"
                                    id="nik" name="nik" value="{{ isset($form_data['nik']) ? $form_data['nik'] : '' }}" 
                                    placeholder="" required="" {{ isset($form_data['nik']) ? (empty($form_data['nik']) ? 'disabled' : '' ) : '' }}>
                            </div>
                            <div class="col-sm-2">
                                <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                    <input type="checkbox"
                                        class="custom-control-input" 
                                        name="fillnik" id="fillnik"
                                        {{ isset($form_data['nik']) ? (empty($form_data['nik']) ? 'checked' : '' ) : '' }}>
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description" style="margin-top: 5px;">Saya isi nanti</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jobstatus" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                Status Pekerjaan
                            </label>
                            <div class="col-sm-7">
                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                            <input type="radio"
                                            class="custom-control-input" name="jobstatus" id="jobstatus" value="1" checked required><span class="custom-control-indicator"></span><span class="custom-control-description">Tidak Bekerja</span>
                                        </label>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>
