<div class="boxed-body w-100 text-left">
    <form id="honorarium-apbdn-form" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab-honorarium-apbdn" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links" id="tabs">
                            @if (!empty($form_data['data']))  
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @else  
                                        <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @endif
                                @endforeach   
                            @else
                                <li id="li1"><a href="#tab1">#1</a></li>
                            @endif
                        </ul>
                        <div class="tab-content" id="tab-content">
                            @if (!empty($form_data['data']))
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                <input type="hidden" name="option_id" value="{{$item['income_id']}}">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-apbdn-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                        <input type="text" netto-id='{{$i_key + 1}}' class="form-control input-money-format" id="honorarium-apbdn-netto" name="netto[{{$i_key + 1}}]" value="{{$item['net_value']}}" autocomplete="off"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-apbdn-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                       Nama Pemotong Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="honorarium-apbdn-name" name="name[{{$i_key + 1}}]" value="{{$item['detail']['nama_pemotong_pajak']}}" autocomplete="off"  required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-apbdn-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Golongan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <select select-id='{{$i_key + 1}}' class="form-control" id="honorarium-apbdn-type" name="type[{{$i_key + 1}}]" >
                                                                <option value='0'>Silahkan pilih golongan pekerjaan Anda</option>   
                                                                <option value='1'>Golongan I</option>   
                                                                <option value='2'>Golongan II</option>      
                                                                <option value='3'>Golongan III</option>      
                                                                <option value='4'>Golongan IV</option>      
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="honorarium-apbdn-bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                       Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="honorarium-apbdn-bruto" name="brutto[{{$i_key + 1}}]" value="{{$item['gross_value']}}" autocomplete="off" style="width:89%" readonly>
                                                            <label for="legacy-giver" class="col-form-label input-money-format" style="padding-left:5%"><span style="font-size : 15px; position : absolute">Tarif</span></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                            <div class="row">
                                                                <input id="tarif" type="text" class="form-control" id="honorarium-apbdn-tarif" name="tarif[{{$i_key + 1}}]" value="{{$item['detail']['tarif']}}" autocomplete="off" style="width:50%; text-align:center;" readonly>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="honorarium-apbdn-final" name="final[{{$i_key + 1}}]" value="{{$item['gross_value'] - $item['net_value']}}" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div id="tab1" class="tab active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="id[0]" id="id" value="0">
                                            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-apbdn-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nilai Netto
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <input type="text" netto-id='1' class="form-control input-money-format" id="honorarium-apbdn-netto" name="netto[0]" value="" autocomplete="off"  required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-apbdn-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                   Nama Pemotong Pajak
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <input type="text" class="form-control" id="honorarium-apbdn-name" name="name[0]" value="" autocomplete="off"  required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-apbdn-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Golongan
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <select select-id='1' class="form-control" id="honorarium-apbdn-type" name="type[]" >
                                                            <option value='0'>Silahkan pilih golongan pekerjaan Anda</option>   
                                                            <option value='1'>Golongan I</option>   
                                                            <option value='2'>Golongan II</option>      
                                                            <option value='3'>Golongan III</option>      
                                                            <option value='4'>Golongan IV</option>      
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="honorarium-apbdn-bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                   Nilai Bruto
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <div class="col-sm-9" style="padding:0">    
                                                        <input type="text" class="form-control" id="honorarium-apbdn-bruto" name="brutto[0]" value="" autocomplete="off" readonly>
                                                        </div>
                                                        <div class="col-sm-2 pad">
                                                        <label for="legacy-giver" class="col-form-label input-money-format" ><span style="font-size : 15px; position : absolute">Tarif</span></label>
                                                        </div>
                                                </div>
                                                </div>
                                                <div class="col-sm-1">
                                                            <input id="tarif" type="text" class="form-control" id="honorarium-apbdn-tarif" name="tarif[0]" value="" autocomplete="off"readonly>
                                                        
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Pajak final yang telah dipungut
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <input type="text" class="form-control" id="honorarium-apbdn-final" name="final[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif     
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="deleted">
                
            </div>

            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save</button>
                    {{--@endif</div>--}}
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<div id="honorarium-apbdn-template" class="tab-transfer-building-rights-form" style="display: none">
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" name="id[0]" id="id" value="0">
            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
            <div class="form-group row">
                <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                </div>
            </div>
            <div class="form-group row">
                <label for="honorarium-apbdn-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Nilai Netto
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <input type="text" class="form-control" id="honorarium-apbdn-netto" name="netto[0]" value="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="honorarium-apbdn-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                   Nama Pemotong Pajak
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <input type="text" class="form-control" id="honorarium-apbdn-name" name="name[0]" value="" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="honorarium-apbdn-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Golongan
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <select class="form-control" id="honorarium-apbdn-type" name="type[]">
                            <option value='0'>Silahkan pilih golongan pekerjaan Anda</option>   
                            <option value='1'>Golongan I</option>   
                            <option value='2'>Golongan II</option>      
                            <option value='3'>Golongan III</option>      
                            <option value='4'>Golongan IV</option>      
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="honorarium-apbdn-bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                   Nilai Bruto
                </label>
                <div class="col-sm-5 exc6">
                    <div class="row">
                    <div class="col-sm-9" style="padding:0">
                        <input type="text" class="form-control" id="honorarium-apbdn-bruto" name="brutto[0]" value="" autocomplete="off" readonly>
                        </div>
                        <div class="col-sm-2 pad"> 
                        <label for="legacy-giver" class="col-form-label"><span style="font-size : 15px;">Tarif</span></label>
                        </div>
                        </div>
                        </div>
                <div class="col-sm-1">
                        <div>
                            <input id="tarif" type="text" class="form-control" id="honorarium-apbdn-tarif" name="tarif[0]" value="" autocomplete="off" style="text-align:center;" readonly>
                        </div>
                    </div>
            </div>
            <div class="form-group row">
                <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Pajak final yang telah dipungut
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <input type="text" class="form-control" id="honorarium-apbdn-final" name="final[0]" value="" autocomplete="off"readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>