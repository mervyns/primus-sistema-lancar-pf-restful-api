<div class="boxed-body w-100 text-left">
        <form id="spouse-income-2" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            
                            <div class="tab-content" id="tab-content">
                                
                                <div id="tab1" class="tab active">
                                    <div class="row spouse-income-2-form">
                                        <div class="col-md-12 col-md-offset-2">
                                            <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">                                    
                                            <div class="form-group row">
                                                <label for="net-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Total Penghasilan
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" 
                                                                class="form-control form-control-sm input-money-format" 
                                                                id="net-income" name="net-income" value="{{ isset($form_data['user_income']['net_value']) ? $form_data['user_income']['net_value'] : '' }}" autocomplete="off">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>
    