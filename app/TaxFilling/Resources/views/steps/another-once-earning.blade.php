<div class="boxed-body w-100 text-left">
        <form id="another-earning-form" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if (!empty($form_data['is_final_steps_submited']))
                    <input type="hidden" name="close-signal" id="close-signal" value="1">
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
    
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab-another-once-earning" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if (!empty($form_data['data']))  
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @else  
                                            <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @endif
                                    @endforeach   
                                @else
                                    <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if (!empty($form_data['data']))
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                            <div class="row">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                    <input type="hidden" name="radio_id" value="{{$item['detail']['dipotong_pajak']}}">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-is-pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Dipotong pajak
                                                        </label>
                                                        <div class="col-sm-5"> 
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" id="taxes" value="1">
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">No</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" id="taxes" value="2" checked>
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">Yes</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-error row">
                                                        <label for="another-earning-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Penghasilan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row">
                                                                <div class="col-sm-10">
                                                                    @if(!empty($item['detail']['tipe_pph']))
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-type" name="earning_type[{{$i_key + 1}}]" value="{{$item['detail']['tipe_pph']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-type" name="earning_type[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-error row">
                                                        <label for="another-earning-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Bruto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row">
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['gross_value']))
                                                                        <input type="text" brutto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="another-earning-brutto" name="brutto[{{$i_key + 1}}]" value="{{$item['gross_value']}}" autocomplete="off" required>   
                                                                    @else
                                                                        <input type="text" brutto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="another-earning-brutto" name="brutto[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Netto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['net_value']))
                                                                        <input type="text" netto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm another-earning-netto" id="another-earning-netto" name="netto[{{$i_key + 1}}]" value="{{$item['net_value']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" netto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm another-earning-netto" id="another-earning-netto" name="netto[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['nama_pemotong']))
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-name" name="cutter-name[{{$i_key + 1}}]" value="{{$item['detail']['nama_pemotong']}}" autocomplete="off" required> 
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-name" name="cutter-name[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            NPWP Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['npwp_pemotong']))
                                                                        <input type="text" class="form-control input-npwp-format form-control-sm" id="another-earning-cutter-npwp" name="cutter-npwp[{{$i_key + 1}}]" value="{{$item['detail']['npwp_pemotong']}}" autocomplete="off" required>   
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-npwp" name="cutter-npwp[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['bukti_potong']))
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-proof" name="pph-proof[{{$i_key + 1}}]" value="{{$item['detail']['bukti_potong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-proof" name="pph-proof[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['tanggal_potong']))
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-date" name="pph-date[{{$i_key + 1}}]" value="{{$item['detail']['tanggal_potong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-date" name="pph-date[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-pph-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Pasal PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    {--@if (!empty($item['detail']['pasal_pph']))
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-type" name="pph-type[{{$i_key + 1}}]" value="{{$item['detail']['pasal_pph']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-type" name="pph-type[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif--}
                                                                    <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" style="width: 100%;">
                                                                        <option value=""></option>
                                                                        <option value="4">PPh Pasal 23</option>
                                                                        <option value="5">PPh Pasal 4 Ayat 2</option>
                                                                        <option value="6">PPh Pasal 22</option>
                                                                        <option value="8">PPh Pasal 21</option>
                                                                    </select>
                                                                    <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="">
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="another-earning-pph-taken" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            PPh yang Dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['nilai_pph']))
                                                                        <input type="text" pph-taken-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm another-earning-pph-taken" id="another-earning-pph-taken" name="pph-taken[{{$i_key + 1}}]" value="{{$item['detail']['nilai_pph']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" pph-taken-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm another-earning-pph-taken" id="another-earning-pph-taken" name="pph-taken[{{$i_key + 1}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div id="tab1" class="tab active">
                                        <div class="row">
                                            <div class="col-md-12 col-md-offset-2">
                                                <input type="hidden" name="id[]" id="id" value="">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-is-pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Dipotong pajak
                                                    </label>
                                                    <div class="col-sm-5"> 
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" radio-id='1' class="custom-control-input" name="type[0]" id="taxes" value="1" checked>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">No</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" radio-id='1' class="custom-control-input" name="type[0]" id="taxes" value="2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Yes</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group has-error row">
                                                    <label for="another-earning-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis Penghasilan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" id="another-earning-type" name="earning_type[0]" value="" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group has-error row">
                                                    <label for="another-earning-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control input-money-format form-control-sm" id="another-earning-brutto" name="brutto[0]" value="" autocomplete="off" required>
                                                            
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control input-money-format form-control-sm another-earning-netto" id="another-earning-netto" name="netto[0]" value="" readonly autocomplete="off" required>
                                                            
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-name" name="cutter-name[0]" value="" readonly autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        NPWP Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="another-earning-cutter-npwp" name="cutter-npwp[0]" value="" readonly autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nomor Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="another-earning-pph-proof" name="pph-proof[0]" value="" readonly autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="another-earning-pph-date" name="pph-date[0]" value="" readonly autocomplete="off" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-pph-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis Pasal PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" disabled style="width: 100%;">
                                                            <option value=""></option>
                                                            <option value="4">PPh Pasal 23</option>
                                                            <option value="5">PPh Pasal 4 Ayat 2</option>
                                                            <option value="6">PPh Pasal 22</option>
                                                            <option value="8">PPh Pasal 21</option>
                                                        </select>
                                                        <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="">
                                                                <!-- <input type="text" class="form-control form-control-sm" id="another-earning-pph-type" name="pph-type[0]" value="" autocomplete="off" required> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="another-earning-pph-taken" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        PPh yang Dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control input-money-format form-control-sm another-earning-pph-taken" id="another-earning-pph-taken" name="pph-taken[0]" value="" readonly autocomplete="off" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="deleted">
                    
                </div>
    
                @if (\Session::get('is_final_step'))
                    <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                        <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save</button>
                        {{--@endif</div>--}}
                    </div>
                @else
                    <div class="row">
                        <hr class="col-md-12">
                        <div class="col-md-12 center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                            <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                            <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                            <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                            {{--@endif</div>--}}
                        </div>
                    </div>
                @endif
            </div>
        </form>
    </div>
    
    <div id="tab-another-earning-template" class="tab-legacy-form" style="display: none">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <input type="hidden" name="id[]" id="id" value="">
                <div class="form-group row">
                    <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-is-pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Dipotong pajak
                    </label>
                    <div class="col-sm-5"> 
                        <label class="custom-control custom-radio" style="margin-top: 5px; ">
                                <input type="radio" class="custom-control-input" id="taxes" value="1" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                        </label>
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" id="taxes" value="2">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="form-group has-error row">
                    <label for="another-earning-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Jenis Penghasilan
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" id="another-earning-type" name="earning_type[0]" value="" autocomplete="off" required>
                            
                    </div>
                </div>
                <div class="form-group has-error row">
                    <label for="another-earning-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai Bruto
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" id="another-earning-brutto" name="brutto[0]" value="" autocomplete="off" required>
                           
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai Netto
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm another-earning-netto" id="another-earning-netto" name="netto[0]" value="" readonly autocomplete="off" required>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-cutter-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nama Pemotong
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-name" name="cutter-name[0]" value="" readonly autocomplete="off" required>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        NPWP Pemotong
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" id="another-earning-cutter-npwp" name="cutter-npwp[0]" value="" readonly autocomplete="off" required>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-pph-proof" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nomor Bukti PPh
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" id="another-earning-pph-proof" name="pph-proof[0]" value="" readonly autocomplete="off" required>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-pph-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Tanggal Bukti PPh
                    </label>
                    <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm" id="another-earning-pph-date" name="pph-date[0]" value="" readonly autocomplete="off" required>
                           
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-pph-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Jenis Pasal PPh
                    </label>
                    <div class="col-sm-5">
                        <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" style="width: 100%;" disabled>
                            <option value=""></option>
                            <option value="4">PPh Pasal 23</option>
                            <option value="5">PPh Pasal 4 Ayat 2</option>
                            <option value="6">PPh Pasal 22</option>
                            <option value="8">PPh Pasal 21</option>
                        </select>
                        <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="">
                                <!-- <input type="text" class="form-control form-control-sm" id="another-earning-pph-type" name="pph-type[0]" value="" autocomplete="off" required>       -->
                    </div>
                </div>
                <div class="form-group row">
                    <label for="another-earning-pph-taken" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        PPh yang Dipungut
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm another-earning-pph-taken" id="another-earning-pph-taken" name="pph-taken[0]" value="" readonly autocomplete="off" required>
                    </div>
                </div>
            </div>
        </div>
    </div>