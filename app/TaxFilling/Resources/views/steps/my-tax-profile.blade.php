<div class="boxed-body w-100 text-left">
    <form id="my-tax-profile" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                            NPWP
                        </label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-sm-8" style="padding-left:0;padding-right:0">
                                    <input type="text"
                                        class="form-control form-control-sm input-npwp-format"
                                        id="npwp" name="npwp" value="{{ old('npwp', empty($form_data['npwp']) ? '' : $form_data['npwp']) }}" maxlength="20">
                                </div>
                                <div class="col-sm-4">
                                    <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                        <input type="checkbox"
                                            class="custom-control-input" name="fillnpwp" id="fillnpwp"
                                            >
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description" style="margin-top: 5px;">Saya isi nanti</span>
                                    </label>
                                </div> 
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label for="efin" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                EFIN (Jika Ada)
                            </label>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-8" style="padding-left:0;padding-right:0">
                                        <input type="text"
                                        class="form-control form-control-sm"
                                        id="efin" name="efin" value="{{ old('efin', empty($form_data['efin']) ? '' : $form_data['efin']) }}">
                                        <small id="passwordHelpInline" class="text-muted">
                                            Belum memiliki EFIN? cara aplikasi EFIN <a href="javascript:void(0)" style="color:#34ea93">click Here</a>
                                        </small>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                            <input type="checkbox"
                                                class="custom-control-input" name="fillefin" id="fillefin">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" style="margin-top: 5px;">Saya isi nanti</span>
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jobstatus" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                Menjalankan usaha/pekerjaan bebas
                            </label>
                            <div class="col-sm-7">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio"
                                        class="custom-control-input" name="jobstatus" id="jobstatus" value="2" checked required><span class="custom-control-indicator"></span><span class="custom-control-description">Tidak</span>
                                </label> <a href="javascript:void(0)" class="btn btn-info btn-xs" id="btn_mytax_info_popup"><i class="fa fa-info"></i></a>
                            </div>
                            <div class="col-sm-7 offset-sm-5">
                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                    <input type="radio"
                                        class="custom-control-input" name="jobstatus" id="jobstatus" value="5" disabled required><span class="custom-control-indicator"></span><span class="custom-control-description">Ya</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="klu" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                Pekerjaan
                            </label>
                            <div class="col-sm-5">
                                <select class="form-control form-control-sm" id="klu" name="klu" style="width: 100%;" required>
                                    <option value=""></option>
                                    <option value="1" {{ old('klu' == 1 ? 'selected' : '', isset($form_data['klu_id']) ? $form_data['klu_id'] == 1 ? 'selected' : '' : '') }} data-number="96301">Pegawai Negeri Sipil</option>
                                    <option value="2" {{ old('klu' == 2 ? 'selected' : '', isset($form_data['klu_id']) ? $form_data['klu_id'] == 2 ? 'selected' : '' : '') }} data-number="96304">Pegawai Swasta</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="klunumber" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                KLU
                            </label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm" id="klunumber" name="klunumber" 
                                value="{{ old('klunumber', (!isset($form_data['klu_id'])) ? '' : ($form_data['klu_id'] == 1 ? '96301' : '96304')) }}" readonly="">
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="modal_my_tax_profile_popup" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
                </div>
                <div class="modal-body">
                    <h6>Jika Anda memiliki usaha atau pekerjaan bebas sebagai pekerjaan utama:</h6>
                    <br><br>
                    <ol>
                        <li>
                            <p align="left">Yang dimaksud dengan memiliki usaha : <br> Menjalankan usaha yang <strong>melakukan pembukuan</strong> dan <strong>tidak berbadan hukum</strong>. Sebagai contoh: memiliki usaha dagang jual beli pedagang/pengecer atas nama pribadi atau UD</p>
                        </li>
                        <br>
                        <li>
                            <p align="left">Yang dimaksud dengan pekerjaan bebas : <br> <strong>Pekerjaan yang dilakukan oleh orang pribadi</strong> yang mempunyai <strong>keahlian khusus</strong> sebagai usaha untuk memperoleh penghasilan yang tidak terikat oleh suatu hubungan kerja. Sebagai contoh: akuntan, notaris, olahragawan, seniman, pemain musik</p>
                        </li>
                        <br><br><br>
                        <p align="left">Berdasarkan PP No. 46 tahun 2013 Pasal 1 angka 24</p>
                    </ol>
                </div>
            </div>
        </div>
    </div>