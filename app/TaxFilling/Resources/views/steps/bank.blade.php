<div class="boxed-body w-100 text-left">
        <form id="bank" class="main-form" method="post" action="{{ Request::url() }}">
            {{ csrf_field() }}
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br>
                @endif
                <!-- <br> -->

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ empty($form_data['banks']) ? '1' : count($form_data['banks']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br>
                <div id="bank-form-container">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(empty($form_data['banks']))
                                    <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['banks'] as $bank)
                                        <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(empty($form_data['banks']))
                                    <div id="tab1" class="tab active">
                                        <div class="row bank-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <!-- <br> -->

                                                <input type="hidden" id="id" name="asset-id[]" value="">
                                                <input type="hidden" name="interest-data[]" value="[0,0,0,0,0,0,0,0,0,0,0,0]">
                                                <input type="hidden" name="tax-data[]" value="[0,0,0,0,0,0,0,0,0,0,0,0]">

                                                <div class="form-group row">
                                                    <label for="bank-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Tabungan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="bank-type[0]" id="bank-type" 
                                                                value="Tabungan/Giro Bank" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Tabungan/Giro Bank</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="bank-type[0]" id="bank-type" 
                                                                value="Simpanan Koperasi"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Simpanan Koperasi</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Saldo Per Akhir Tahun
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" id="account-balance" name="account-balance[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Rekening
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="account-number" name="account-number[]" value="" placeholder="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Bank/Koperasi
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm bank-name" id="bank-name-1" name="bank-name[]" index="0" style="width: 100%;" required>
                                                            @foreach($form_data['bank_list'] as $list)
                                                                <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                                                            @endforeach
                                                            <option value="-1">Bank/Koperasi Lainnya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="other-bank" name="other-bank[]" style="display:none;"
                                                                value="" placeholder="Nama Lainnya">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Kota
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="bank-city" name="bank-city[]" value="" placeholder="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Cabang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="bank-branch" name="bank-branch[]" value="" placeholder="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="form-group row">
                                                    <label for="calculated-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Total Bunga
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format"
                                                            id="interest" name="interests[]" value="" readonly="">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="javascript:void(0)" class="btn btn-sm btn-info btn-calculation-modal" title="Perhitungan total bunga sebelum pajak dalam 1 tahun" data-type="interest"><i class="fa fa-calculator"></i></a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="calculated-tax" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Total Pajak Bunga
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format"
                                                            id="tax" name="taxes[]" value="" readonly="">
                                                    </div>
                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @foreach($form_data['banks'] as $bank)
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                                <div class="row bank-form">
                                                    <div class="col-md-12 col-md-offset-2">

                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                        <!-- <br> -->

                                                        <input type="hidden" id="id" name="asset-id[]" value="{{ isset($bank['id']) ? $bank['id'] : '' }}">
                                                        <input type="hidden" name="interest-data[]" value="{{ $bank['interest_data'] }}">
                                                        <input type="hidden" name="tax-data[]" value="{{ $bank['tax_data'] }}">

                                                        <div class="form-group row">
                                                            <label for="bank-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Jenis Tabungan
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    <input type="radio"
                                                                        class="custom-control-input" name="bank-type[{{ $loop->iteration - 1 }}]" id="bank-type" 
                                                                        value="Tabungan/Giro Bank" {{ ($bank['bank_type']) == 'Tabungan/Giro Bank' ? 'checked' : ''  }}>
                                                                        <span class="custom-control-indicator">
                                                                        </span><span class="custom-control-description">Tabungan/Giro Bank</span>
                                                                </label>
                                                                <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    <input type="radio"
                                                                        class="custom-control-input" name="bank-type[{{ $loop->iteration - 1 }}]" id="bank-type" 
                                                                        value="Simpanan Koperasi" {{ ($bank['bank_type']) == 'Simpanan Koperasi' ? 'checked' : ''  }}><span class="custom-control-indicator">
                                                                        </span><span class="custom-control-description">Simpanan Koperasi</span>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Negara
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;">
                                                                    @foreach($form_data['country'] as $country)
                                                                        <option value="{{ $country['id'] }}" {{ ($bank['country']) == $country['id'] ? 'selected' : '' }}>{{ $country['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Mata Uang
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                                                    @foreach($form_data['country'] as $country)
                                                                        <option value="{{ $country['id'] }}" {{ ($bank['currency']) == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Saldo Per Akhir Tahun
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                    id="account-balance" name="account-balance[]" 
                                                                    value="{{ isset($bank['currency_value']) ? $bank['currency_value'] : '' }}" autocomplete="off">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Nomor Rekening
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="account-number" name="account-number[]" 
                                                                    value="{{ isset($bank['bank_number']) ? $bank['bank_number'] : '' }}" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Nama Bank/Koperasi
                                                            </label>
                                                            <div class="col-sm-3">
                                                                <select class="form-control form-control-sm bank-name" id="bank-name-{{ $loop->iteration }}" name="bank-name[]" style="width: 100%;">
                                                                    @foreach($form_data['bank_list'] as $list)
                                                                        <option value="{{ $list['id'] }}" {{ ($bank['bank_id'] == $list['id']) ? 'selected' : '' }}>{{ $list['name'] }}</option>
                                                                    @endforeach
                                                                    <option value="-1" {{ ($bank['bank_id'] == -1) ? 'selected' : '' }}>Bank/Koperasi Lainnya</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <input type="text"
                                                                        class="form-control form-control-sm"
                                                                        id="other-bank" name="other-bank[]" style="{{ ($bank['bank_id'] == -1) ?: 'display:none' }}"
                                                                        value="{{ isset($bank['other_bank']) ? $bank['other_bank'] : '' }}" placeholder="Nama Lainnya">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Kota
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="bank-city" name="bank-city[]" 
                                                                    value="{{ isset($bank['bank_city']) ? $bank['bank_city'] : '' }}" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Cabang
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="bank-branch" name="bank-branch[]" 
                                                                    value="{{ isset($bank['bank_branch']) ? $bank['bank_branch'] : '' }}" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Kurs Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="kurs_pajak" name="kurs_pajak[]" 
                                                                    value="{{ isset($bank['kurs_value']) ? $bank['kurs_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Jumlah Rupiah
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                                    value="{{ isset($bank['idr_value']) ? $bank['idr_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group row">
                                                            <label for="calculated-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Total Bunga
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format"
                                                                    id="interest" name="interests[]" 
                                                                    value="{{ isset($bank['interests']) ? $bank['interests'] : '' }}" readonly="">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-sm btn-info btn-calculation-modal" title="Perhitungan total bunga sebelum pajak dalam 1 tahun" data-type="interest"><i class="fa fa-calculator"></i></a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="calculated-tax" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Total Pajak Bunga
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format"
                                                                    id="tax" name="taxes[]" 
                                                                    value="{{ isset($bank['taxes']) ? $bank['taxes'] : '' }}" readonly="">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <br> -->
                </div>

                <div id="deleted">
                    
                </div>

                <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="bank-form-template" class="row bank-form" style="display: none">
        <div class="col-md-12 col-md-offset-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
                </div>
            </div>
        <!-- <br> -->

            <input type="hidden" id="id" name="asset-id[]" value="">
            <input type="hidden" name="interest-data[]" value="[0,0,0,0,0,0,0,0,0,0,0,0]">
            <input type="hidden" name="tax-data[]" value="[0,0,0,0,0,0,0,0,0,0,0,0]">

            <div class="form-group row">
                <label for="bank-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Jenis Tabungan
                </label>
                <div class="col-sm-5">
                    <label class="custom-control custom-radio" style="margin-top: 5px;">
                        <input type="radio"
                            class="custom-control-input" id="bank-type" 
                            value="Tabungan/Giro Bank" checked="">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Tabungan/Giro Bank</span>
                    </label>
                    <label class="custom-control custom-radio" style="margin-top: 5px;">
                        <input type="radio"
                            class="custom-control-input" id="bank-type" 
                            value="Simpanan Koperasi">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Simpanan Koperasi</span>
                    </label>
                </div>
            </div>      

            <div class="form-group row">
                <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Negara
                </label>
                <div class="col-sm-5">
                    <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                        @foreach($form_data['country'] as $country)
                            <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Mata Uang
                </label>
                <div class="col-sm-5">
                    <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                        @foreach($form_data['country'] as $country)
                            <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Saldo Per Akhir Tahun
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-money-format balance" id="account-balance" name="account-balance[]" value="" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Nomor Rekening
                </label>
                <div class="col-sm-5">
                    <input type="text"
                    class="form-control form-control-sm"
                    id="account-number" name="account-number[]" value="" placeholder="" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Nama Bank/Koperasi
                </label>
                <div class="col-sm-3">
                    <select class="form-control form-control-sm" id="bank-name" name="bank-name[]" style="width: 100%;" required>
                        @foreach($form_data['bank_list'] as $list)
                            <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                        @endforeach
                            <option value="-1">Bank/Koperasi Lainnya</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <input type="text"
                            class="form-control form-control-sm"
                            id="other-bank" name="other-bank[]" style="display:none;"
                            value="" placeholder="Nama Lainnya">
                </div>
            </div>
            <div class="form-group row">
                <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Kota
                </label>
                <div class="col-sm-5">
                    <input type="text"
                        class="form-control form-control-sm"
                        id="bank-city" name="bank-city[]" value="" placeholder="" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Cabang
                </label>
                <div class="col-sm-5">
                    <input type="text"
                        class="form-control form-control-sm"
                        id="bank-branch" name="bank-branch[]" value="" placeholder="" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Kurs Pajak
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" 
                        id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                </div>
            </div>
            <div class="form-group row">
                <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Jumlah Rupiah
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" 
                        id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                </div>
            </div>

            <hr>

            <div class="form-group row">
                <label for="calculated-interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Total Bunga
                </label>
                <div class="col-sm-5">
                    <input type="text"
                        class="form-control form-control-sm input-money-format"
                        id="interests" name="interests[]" value="" readonly="">
                </div>
                <div class="col-sm-2">
                    <a href="javascript:void(0)" class="btn btn-sm btn-info btn-calculation-modal" title="Perhitungan total bunga sebelum pajak dalam 1 tahun" data-type="interest"><i class="fa fa-calculator"></i></a>
                </div>
            </div>
            <div class="form-group row">
                <label for="calculated-tax" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Total Pajak Bunga
                </label>
                <div class="col-sm-5">
                    <input type="text"
                        class="form-control form-control-sm input-money-format"
                        id="tax" name="taxes[]" value="" readonly="">
                </div>
            </div>

            
        </div>
    </div>


    <div class="modal fade" id="calculation_table_modal" tabindex="-7" role="dialog" aria-labelledby="calculation_table_modal_label" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn btn-xs btn-danger exc4" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-sm table-hover" data-mode="interest">
                        <thead>
                            <tr>
                                <th>Bulan</th>
                                <th>Bunga (IDR)</th>
                                <th>Pajak (IDR)</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="text-center">Januari</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-1" id="interest-1" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-1" id="tax-1" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Februari</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-2" id="interest-2" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-2" id="tax-2" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Maret</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-3" id="interest-3" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-3" id="tax-3" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">April</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-4" id="interest-4" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-4" id="tax-4" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Mei</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-5" id="interest-5" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-5" id="tax-5" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Juni</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-6" id="interest-6" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-6" id="tax-6" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Juli</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-7" id="interest-7" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-7" id="tax-7" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Agustus</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-8" id="interest-8" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-8" id="tax-8" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">September</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-9" id="interest-9" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-9" id="tax-9" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Oktober</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-10" id="interest-10" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-10" id="tax-10" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">November</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-11" id="interest-11" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-11" id="tax-11" value="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">Desember</td>
                                <td>
                                    <input type="text" class="text-center width" name="interest-12" id="interest-12" value="0" />
                                </td>
                                <td>
                                    <input type="text" class="text-center width" name="tax-12" id="tax-12" value="0" />
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr style="font-size: 1.2em; font-weight: bold">
                                <td class="text-center">Jumlah</td>
                                <td class="text-center text-info">
                                    <span id="total-interest-text">0</span>
                                    <input type="hidden" name="total-interest" id="total-interest">
                                </td>
                                <td class="text-center text-info">
                                    <span id="total-tax-text">0</span>
                                    <input type="hidden" name="total-tax" id="total-tax">
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="20-percent-tax-rate" id="20-percent-tax-rate" value="1">
                        <span class="custom-control-indicator"></span>
                        <span style="padding-top: 0.3em">Tarif pajak otomatis 20%</span>
                    </label>
                </div>
            </div>
        </div>
    </div>