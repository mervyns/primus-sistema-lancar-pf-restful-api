<div class="boxed-body w-100 text-left">
    <form  method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    
                    @if ($form_data['reset'] == true)
                        <button type="submit" name="submit" value="reset" class="btn btn-danger btn-sm pull-right"> Reset <i class="fa fa-refresh"></i></button>
                    @else
                        <button type="submit" name="submit" value="skip" class="btn btn-danger btn-sm pull-right"> Skip All <i class="fa fa-forward"></i></button>
                    @endif
                </div></div>
                <div class="row">
                    <div class="text-center col-md-12">
                        <div class="btn-group-select btn-group-multi-select">
                            <p><strong>Saya Mempunyai</strong></p><br>
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom:3%">
                                    <button data-value="have_deposit" type="button" 
                                        class="btn btn-lg btn-outline-success btn-click {{ !isset($form_data['have_deposit']) ?: $form_data['have_deposit'] != 1 ?: 'active' }}" 
                                        style="width: 90%; padding-left: 0px; padding-right: 0px; {{ !isset($form_data['have_deposit']) ?: $form_data['have_deposit'] != 1 ?: 'color:#fff;' }}" 
                                        {{ !isset($form_data['have_deposit']) ?: $form_data['have_deposit'] != 1 ?: 'disabled' }}>
                                            <i class="fa fa-bank" aria-hidden="true" style="font-size: 5em;"></i>
                                            <p style="margin-bottom: 0px; font-size: 0.9em;">Deposito</p>
                                    </button>
                                    <input type="hidden" name="deposit" id="deposit" value="{{ !isset($form_data['have_deposit']) ?: $form_data['have_deposit'] != '1' ? : 'have_deposit' }}">
                                </div>
                                <div class="col-md-4" style="margin-bottom:3%">
                                    <button data-value="have_card" type="button" 
                                        class="btn btn-lg btn-outline-success btn-click {{ !isset($form_data['have_credit_card']) ?: $form_data['have_credit_card'] != '1' ?: 'active' }}" 
                                        style="width: 90%; padding-left: 0px; padding-right: 0px; {{ !isset($form_data['have_credit_card']) ?: $form_data['have_credit_card'] != '1' ?: 'color:#fff;' }}" 
                                        {{ !isset($form_data['have_credit_card']) ?: $form_data['have_credit_card'] != '1' ?: 'disabled' }}>
                                            <i class="fa fa-credit-card" aria-hidden="true" style="font-size: 5em;"></i>
                                            <p style="margin-bottom: 0px; font-size: 0.9em;">Kartu Kredit</p>
                                    </button>
                                    <input type="hidden" name="card" id="card" value="{{ !isset($form_data['have_credit_card']) ?: $form_data['have_credit_card'] != '1' ?: 'have_card' }}">
                                </div>
                                <div class="col-md-4" style="margin-bottom:3%">
                                    <button data-value="have_jewelry" type="button" 
                                        class="btn btn-lg btn-outline-success btn-click {{ !isset($form_data['have_jewelry']) ?: $form_data['have_jewelry'] != '1' ?: 'active' }}" 
                                        style="width: 90%; padding-left: 0px; padding-right: 0px; {{ !isset($form_data['have_jewelry']) ?: $form_data['have_jewelry'] != '1' ?: 'color:#fff;' }}" 
                                        {{ !isset($form_data['have_jewelry']) ?: $form_data['have_jewelry'] != '1' ?: 'disabled' }}>
                                            <i class="fa fa-diamond" aria-hidden="true" style="font-size: 5em;"></i>
                                            <p style="margin-bottom: 0px; font-size: 0.9em;">Logam Mulia, Emas, Perhiasan, Antik</p>
                                    </button>
                                    <input type="hidden" name="jewelry" id="jewelry" value="{{ !isset($form_data['have_jewelry']) ?: $form_data['have_jewelry'] != '1' ?: 'have_jewelry' }}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom:3%">
                                    <button data-value="have_vehicle" type="button" 
                                        class="btn btn-lg btn-outline-success btn-click {{ !isset($form_data['have_vehicle']) ?: $form_data['have_vehicle'] != '1' ?: 'active' }}" 
                                        style="width: 90%; padding-left: 0px; padding-right: 0px; {{ !isset($form_data['have_vehicle']) ?: $form_data['have_vehicle'] != '1' ?: 'color: #fff' }}" 
                                        {{ !isset($form_data['have_vehicle']) ?: $form_data['have_vehicle'] != '1' ?: 'disabled' }}>
                                            <i class="fa fa-car" aria-hidden="true" style="font-size: 5em;"></i>
                                            <p style="margin-bottom: 0px; font-size: 0.9em;">Kendaraan</p>
                                    </button>
                                    <input type="hidden" name="vehicle" id="vehicle" value="{{ !isset($form_data['have_vehicle']) ?: $form_data['have_vehicle'] != '1' ?: 'have_vehicle' }}">
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4" style="margin-bottom:3%">
                                    <button data-value="have_property" type="button" 
                                        class="btn btn-lg btn-outline-success btn-click {{ !isset($form_data['have_property']) ?: $form_data['have_property'] != '1' ?: 'active' }}" 
                                        style="width: 90%; padding-left: 0px; padding-right: 0px; {{ !isset($form_data['have_property']) ?: $form_data['have_property'] != '1' ?: 'color:#fff;' }}" 
                                        {{ !isset($form_data['have_property']) ?: $form_data['have_property'] != '1' ?: 'disabled' }}>
                                            <i class="fa fa-building" aria-hidden="true" style="font-size: 5em;"></i>
                                            <p style="margin-bottom: 0px; font-size: 0.9em;">Properti</p>
                                    </button>
                                    <input type="hidden" name="property" id="property" value="{{ !isset($form_data['have_property']) ?: $form_data['have_property'] != '1' ?: 'have_property' }}">
                                </div>
                            </div>
                            <br> 
                        </div>
                        <br><br>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
        </div>
    </form>
</div>