<div class="boxed-body w-100 text-left">
    <form method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row" align="center">
                <div class="text-center col-md-12">
                    
                    <div class="btn-group-select btn-group-single-select-reporting">
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom:2%">
                                <button data-value="Pelaporan Global" type="button" class="btn btn-lg btn-outline-success btn-click {{ $form_data == 'Pelaporan Global' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-eye" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Pelaporan Global</p>
                                </button>
                            </div>
                            <div class="col-md-6" style="margin-bottom:2%">
                                <button data-value="Pelaporan Rinci" type="button" class="btn btn-lg btn-outline-success btn-click {{ $form_data == 'Pelaporan Rinci' ? 'active' : '' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                    <i class="fa fa-edit" aria-hidden="true" style="font-size: 3em;"></i>
                                    <p style="margin-bottom: 0px; font-size: 1em;">Pelaporan Rinci</p>
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="precious-asset" id="precious-asset" value="{{ $form_data }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>