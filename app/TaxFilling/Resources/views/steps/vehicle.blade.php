<div class="boxed-body w-100 text-left">
    <form id="vehicle" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <!-- <br> -->

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ count($form_data['vehicles']) == 0 ? '1' : count($form_data['vehicles']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br>

            <div id="vehicle-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['vehicles']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['vehicles'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['vehicles']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['vehicles']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row vehicle-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <!-- <br> -->

                                                <input type="hidden" id="id" name="asset-id[]" value="">
                                                
                                                <div class="form-group row">
                                                    <label for="vehicle-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Kendaraan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm vehicle-type" id="vehicle-type" name="vehicle-type[]" style="width: 100%;" required>
                                                            <option value="19">Sepeda Motor</option>
                                                            <option value="20">Mobil</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Tahun Perolehan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nilai Perolehan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="vehicle-credit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Kredit Kendaraan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="vehicle-credit[0]" id="vehicle-credit" 
                                                                value="Ya" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Ya</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="vehicle-credit[0]" id="vehicle-credit" 
                                                                value="Tidak"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Tidak</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Merk dan Tipe (Opsional)
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="vehicle-merk" name="vehicle-merk[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="police-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Polisi
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="police-number" name="police-number[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="form-group row">
                                                    <label for="bpkb-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nomor STNK/BPKB
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="bpkb-number" name="bpkb-number[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal berlaku STNK
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" id="expired-date" name="expired-date[]" value="{{ date('d/m/Y') }}">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['vehicles'] as $v_key => $vehicle)
                                    @if (isset($vehicle['active']))
                                        @if ($vehicle['active'])
                                            <div id="tab{{$v_key + 1}}" class="tab active">
                                        @else
                                            <div id="tab{{$v_key + 1}}" class="tab">
                                        @endif
                                    @else
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                    @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row vehicle-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <!-- <br> -->

                                                    <input type="hidden" id="id" name="asset-id[]" value="{{ isset($vehicle['id']) ? $vehicle['id'] : '' }}">
                                                    
                                                    <div class="form-group row">
                                                        <label for="vehicle-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Jenis Kendaraan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm vehicle-type" id="vehicle-type" name="vehicle-type[]" style="width: 100%;" required>
                                                                
                                                                <option value="19" {{ ($vehicle['asset_id'] == 19) ? 'selected' : '' }}>Sepeda Motor</option>
                                                                <option value="20" {{ ($vehicle['asset_id'] == 20) ? 'selected' : '' }}>Mobil</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Negara
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ ($vehicle['country'] == $country['id']) ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ ($vehicle['currency'] == $country['id']) ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Tahun Perolehan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="number" class="form-control form-control-sm" 
                                                                id="acquisition-year" name="acquisition-year[]" 
                                                                value="{{ isset($vehicle['acquisition_year']) ? $vehicle['acquisition_year'] : date('Y') - 1 }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nilai Perolehan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                id="acquisition-value" name="acquisition-value[]" 
                                                                value="{{ isset($vehicle['currency_value']) ? $vehicle['currency_value'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="vehicle-credit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Kredit Kendaraan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="vehicle-credit[{{ $loop->iteration - 1 }}]" id="vehicle-credit" 
                                                                    value="Ya" {{ ($vehicle['credite_type'] == 'Ya') ? 'checked' : '' }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Ya</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="vehicle-credit[{{ $loop->iteration - 1 }}]" id="vehicle-credit" 
                                                                    value="Tidak" {{ ($vehicle['credite_type'] == 'Tidak') ? 'checked' : '' }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Tidak</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Merk dan Tipe (Opsional)
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="vehicle-merk" name="vehicle-merk[]" 
                                                                value="{{ isset($vehicle['vehicle_merk']) ? $vehicle['vehicle_merk'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="police-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nomor Polisi
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="police-number" name="police-number[]" 
                                                                value="{{ isset($vehicle['plate_number']) ? $vehicle['plate_number'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                            id="kurs_pajak" name="kurs_pajak[]" 
                                                            value="{{ isset($vehicle['kurs_value']) ? $vehicle['kurs_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                                value="{{ isset($vehicle['idr_value']) ? $vehicle['idr_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <hr>

                                                    <div class="form-group row">
                                                        <label for="bpkb-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor STNK/BPKB
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="bpkb-number" name="bpkb-number[]" 
                                                                value="{{ isset($vehicle['bpkb_number']) ? $vehicle['bpkb_number'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal berlaku STNK
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" 
                                                                id="expired-date" name="expired-date[]" 
                                                                value="{{ isset($vehicle['expired_date']) ? $vehicle['expired_date'] : '' }}">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <br> -->
            </div>

            <div id="deleted">
                
            </div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                     <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<div id="vehicle-form-template" class="row vehicle-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <!-- <br> -->

        <input type="hidden" id="id" name="asset-id[]" value="">

        <div class="form-group row">
            <label for="vehicle-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Jenis Kendaraan
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm vehicle-type" id="vehicle-type" name="vehicle-type[]" style="width: 100%;" required>
                    <option value="19">Sepeda Motor</option>
                    <option value="20">Mobil</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Negara
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Tahun Perolehan
            </label>
            <div class="col-sm-5">
                <input type="number" class="form-control form-control-sm" id="acquisition-year" name="acquisition-year[]" value="{{ date('Y') - 1 }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nilai Perolehan
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="vehicle-credit" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Kredit Kendaraan
            </label>
            <div class="col-sm-5">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="vehicle-credit" 
                        value="Ya" checked="">
                        <span class="custom-control-indicator">
                        </span><span class="custom-control-description">Ya</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="vehicle-credit" 
                        value="Tidak"><span class="custom-control-indicator">
                        </span><span class="custom-control-description">Tidak</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="vehicle-merk" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Merk dan Tipe (Opsional)
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="vehicle-merk" name="vehicle-merk[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="police-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nomor Polisi
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="police-number" name="police-number[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Kurs Pajak
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jumlah Rupiah
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label for="bpkb-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nomor STNK/BPKB
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="bpkb-number" name="bpkb-number[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Tanggal berlaku STNK
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-picker-format" id="expired-date" name="expired-date[]" value="{{ date('d/m/Y') }}">
            </div>
        </div>
        

        
    </div>
</div>
