<div class="boxed-body w-700 text-left">
    <form id="dividend-from-investation" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if (empty($form_data['assets']))
                <input type="hidden" name="empty-signal" id="empty-signal" value="Mohon untuk mengisi data saham sebelum melanjutkan">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ (count($form_data['dividend']) == 0) ? '1' : count($form_data['dividend']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="dividend-from-investation-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['dividend']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['dividend'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['dividend']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['dividend']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row dividend-from-investation-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="devidennetto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format deviden-netto"
                                                            id="deviden-netto" name="deviden-netto[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="deviden-devide" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Badan yang Membagi Deviden
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="deviden-devide" name="deviden-devide[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="deviden_bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format deviden-bruto"
                                                            id="deviden-bruto" name="deviden-bruto[]" value="" placeholder="" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format"
                                                                    id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="tarif" name="tarif[]" 
                                                                        value="{{ isset($form_data['income']->tax_rate) ? ($form_data['income']->tax_rate * 100) .'%' : '0'  }}" placeholder="" readonly="">
                                                                <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                                                    value="{{ isset($form_data['income']->tax_rate) ? $form_data['income']->tax_rate : '0'  }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Dari Saham
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="from-stock" name="from-stock[]" style="width: 100%;" required>
                                                            @foreach ($form_data['assets'] as $asset)
                                                                <option value="{{ $asset->id }}" data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"> {{ $asset->company_name }} </option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['country_id'] : '' }}">
                                                        <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['asset_id'] : '' }}">
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['dividend'] as $d_key => $dividend)
                                        @if (isset($dividend['active']))
                                            @if ($dividend['active'])
                                                <div id="tab{{$d_key + 1}}" class="tab active">
                                            @else
                                                <div id="tab{{$d_key + 1}}" class="tab">
                                            @endif
                                        @else
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row dividend-from-investation-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="income_id[]" value="{{ isset($dividend['id']) ? $dividend['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="devidennetto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Netto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format deviden-netto"
                                                                id="deviden-netto" name="deviden-netto[]" 
                                                                value="{{ isset($dividend['net_value']) ? $dividend['net_value'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="deviden-devide" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Badan yang Membagi Deviden
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="deviden-devide" name="deviden-devide[]" 
                                                                value="{{ isset($dividend['company_name']) ? $dividend['company_name'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="deviden_bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Bruto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format deviden-bruto"
                                                                id="deviden-bruto" name="deviden-bruto[]" 
                                                                value="{{ isset($dividend['gross_value']) ? $dividend['gross_value'] : '' }}" placeholder="" readonly="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak final yang telah dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <input type="text"
                                                                        class="form-control form-control-sm input-money-format"
                                                                        id="tax-final" name="tax-final[]" 
                                                                        value="{{ isset($dividend['tax_final']) ? $dividend['tax_final'] : '' }}" placeholder="" readonly="">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text"
                                                                        class="form-control form-control-sm"
                                                                        id="tarif" name="tarif[]" value="{{ isset($dividend['tax_rate']) ? ($dividend['tax_rate'] * 100) .'%' : '0' }}" placeholder="" readonly="">
                                                                    <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                                                        value="{{ isset($dividend['tax_rate']) ? $dividend['tax_rate'] : '0' }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Dari Saham
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="from-stock" name="from-stock[]" style="width: 100%;" required>
                                                                @foreach ($form_data['assets'] as $asset)
                                                                    <option value="{{ $asset->id }}" 
                                                                        data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"
                                                                        {{ ($dividend['user_asset_id'] == $asset->id) ? 'selected' : '' }}> {{ $asset->company_name }} </option>
                                                                @endforeach
                                                            </select>
                                                            <input type="hidden" name="country_id[]"  id="country_id" 
                                                                value="{{ isset($dividend['country_id']) ? $dividend['country_id'] : '' }}">
                                                            <input type="hidden" name="asset_id[]" id="asset_id" 
                                                                value="{{ isset($dividend['asset_id']) ? $dividend['asset_id'] : '' }}">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>


<div id="dividend-from-investation-form-template" class="row dividend-from-investation-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="income_id[]" value="">
        <div class="form-group row">
            <label for="devidennetto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Netto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format deviden-netto"
                    id="deviden-netto" name="deviden-netto[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="deviden-devide" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nama Badan yang Membagi Deviden
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="deviden-devide" name="deviden-devide[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="deviden_bruto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Bruto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format deviden-bruto"
                    id="deviden-bruto" name="deviden-bruto[]" value="" placeholder="" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pajak final yang telah dipungut
            </label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-md-8">
                        <input type="text"
                            class="form-control form-control-sm input-money-format"
                            id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                    </div>
                    <div class="col-md-4">
                        <input type="text"
                            class="form-control form-control-sm"
                            id="tarif" name="tarif[]" 
                            value="{{ isset($form_data['income']->tax_rate) ? ($form_data['income']->tax_rate * 100) .'%' : '0'  }}" placeholder="" readonly="">
                        <input type="hidden" id="tax_rate" name="tax_rate[]" value="{{ isset($form_data['income']->tax_rate) ? $form_data['income']->tax_rate : '0'  }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Dari Saham
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="from-stock" name="from-stock[]" style="width: 100%;" required>
                    @foreach ($form_data['assets'] as $asset)
                        <option value="{{ $asset->id }}" data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"> {{ $asset->company_name }} </option>
                    @endforeach
                </select>
                <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['country_id'] : '' }}">
                <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['asset_id'] : '' }}">
            </div>
        </div>
        
        
    </div>
</div>