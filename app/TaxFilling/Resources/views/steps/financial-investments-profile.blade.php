<div class="boxed-body w-100 text-left">
    <form  method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    @if ($form_data['reset'] == true)
                        <button type="submit" name="submit" value="reset" class="btn btn-danger btn-sm pull-right"> Reset <i class="fa fa-refresh"></i></button>
                    @else
                        <button type="submit" name="submit" value="skip" class="btn btn-danger btn-sm pull-right"> Skip All <i class="fa fa-forward"></i></button>
                    @endif
                </div>
            </div>
            <br>
            <div class="row">
                <div class="text-center col-md-12">
                    
                    <div class="btn-group-select btn-group-multi-select">
                        <div class="row">
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="29" type="button" data-color="primary" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['exchange_stock']) && $form_data['exchange_stock'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['exchange_stock']) && $form_data['exchange_stock'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['exchange_stock']) && $form_data['exchange_stock'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Saham Bursa Efek</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="SAHAM BURSA EFEK"
                                    data-message="Saham adalah surat berharga yang menunjukkan bagian kepemilikan atas suatu perusahaan.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['exchange_stock']) && $form_data['exchange_stock'] == true ? '29' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="32" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['mutual_funds']) && $form_data['mutual_funds'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['mutual_funds']) && $form_data['mutual_funds'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['mutual_funds']) && $form_data['mutual_funds'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Reksadana</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="REKSADANA"
                                    data-message="Pengertian Reksa Dana menurut pada Undang-Undang Pasar Modal nomor 8 Tahun 1995 pasal 1, ayat (27): “Reksadana adalah wadah yang dipergunakan untuk menghimpun dana dari masyarakat Pemodal untuk selanjutnya diinvestasikan dalam portofolio Efek oleh Manajer Investasi.”">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['mutual_funds']) && $form_data['mutual_funds'] == true ? '32' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="33" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['insurance']) && $form_data['insurance'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['insurance']) && $form_data['insurance'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['insurance']) && $form_data['insurance'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Asuransi dan Komponen Investasi</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="ASURANSI DAN KOMPONEN INVESTASI"
                                    data-message="Asuransi adalah kontrak tertulis antara perusahaan asuransi (penanggung) dan nasabah (tertanggung) yang berisi pengalihan risiko dan syarat-syarat berlaku (jumlah uang pertanggungan, jenis risiko yang ditanggung, jangka waktu dan lain sebagainya).">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['insurance']) && $form_data['insurance'] == true ? '33' : ''  }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="36" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['indonesian_bank_certificate']) && $form_data['indonesian_bank_certificate'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['indonesian_bank_certificate']) && $form_data['indonesian_bank_certificate'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['indonesian_bank_certificate']) && $form_data['indonesian_bank_certificate'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Sertifikat Bank Indonesia</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="SERTIFIKAT BANK INDONESIA"
                                    data-message="Sertifikat Bank Indonesia (SBI) adalah surat berharga yaitu dikeluarkan oleh Bank Indonesia sebagai pengakuan utang berjangka waktu pendek (1-3 bulan) dengan sistem diskonto/bunga.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['indonesian_bank_certificate']) && $form_data['indonesian_bank_certificate'] == true ? '36' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="37" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['goverment_bonds']) && $form_data['goverment_bonds'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['goverment_bonds']) && $form_data['goverment_bonds'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['goverment_bonds']) && $form_data['goverment_bonds'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Obligasi Ritel Pemerintah<br>(SBN, Ori, Sukuk, dll)</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="OBLIGASI RITEL PEMERINTAH"
                                    data-message="Surat utang jangka Panjang yang di terbitkan Pemerintah">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['goverment_bonds']) && $form_data['goverment_bonds'] == true ? '37' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="38" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['corporate_bonds']) && $form_data['corporate_bonds'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['corporate_bonds']) && $form_data['corporate_bonds'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['corporate_bonds']) && $form_data['corporate_bonds'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Obligasi Perusahaan</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="OBLIGASI PERUSAHAAN"
                                    data-message="Surat utang jangka Panjang yang di terbitkan perusahaan dan di perjual belikan.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['corporate_bonds']) && $form_data['corporate_bonds'] == true ? '38' : ''  }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="39" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['non_tradeable_stock']) && $form_data['non_tradeable_stock'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['non_tradeable_stock']) && $form_data['non_tradeable_stock'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['non_tradeable_stock']) && $form_data['non_tradeable_stock'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Saham Non Bursa Efek</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="SAHAM NON BURSA EFEK"
                                    data-message="Saham yang dibeli untuk dijual kembali (Bursa efek)  adalah sebuah pasar yang berhubungan dengan pembelian dan penjualan efek perusahaan yang sudah terdaftar di bursa itu.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['non_tradeable_stock']) && $form_data['non_tradeable_stock'] == true ? '39' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="40" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['capital_investment_companies']) && $form_data['capital_investment_companies'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['capital_investment_companies']) && $form_data['capital_investment_companies'] == true ? 'color: #fff' : ''  }}"
                                    {{ isset($form_data['capital_investment_companies']) && $form_data['capital_investment_companies'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Penyertaan Modal dalam Perusahaan <br> Lain tidak dalam bentuk saham <br>(CV, Firma, dan sejenisnya)</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="PENYERTAAN MODAL DALAM PERUSAHAAN LAIN NON SAHAM"
                                    data-message="Pernyetaan modal selain dalam bentuk PT (CV, Firma)">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['capital_investment_companies']) && $form_data['capital_investment_companies'] == true ? '40' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%">
                                <button data-value="41" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_debentures']) && $form_data['other_debentures'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['other_debentures']) && $form_data['other_debentures'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['other_debentures']) && $form_data['other_debentures'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Surat Hutang Lainnya</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="SURAT HUTANG LAINNYA"
                                    data-message="Berupa surat hutang">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['other_debentures']) && $form_data['other_debentures'] == true ? '41' : ''  }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4 " style="margin-bottom:3%">
                                <button data-value="42" type="button" 
                                    class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_investments']) && $form_data['other_investments'] == true ? 'active' : ''  }}" 
                                    style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px; {{ isset($form_data['other_investments']) && $form_data['other_investments'] == true ? 'color: #fff;' : ''  }}"
                                    {{ isset($form_data['other_investments']) && $form_data['other_investments'] == true ? 'disabled' : ''  }}>
                                        <p style="margin-bottom: 0px; font-size: 0.9em;">Investasi Lainnya</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="INVESTASI LAINNYA"
                                    data-message="penyertaan modal dalam koperasi, atau lainya">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['other_investments']) && $form_data['other_investments'] == true ? '42' : ''  }}">
                            </div>
                            <div class="col-md-4" style="margin-bottom:3%"></div>
                        </div>
                    </div>
                    <br><br>

                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>