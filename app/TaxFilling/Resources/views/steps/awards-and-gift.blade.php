<div class="boxed-body w-100 text-left">
        <form id="awards-and-gift-form" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if (!empty($form_data['is_final_steps_submited']))
                    <input type="hidden" name="close-signal" id="close-signal" value="1">
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
    
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-awards-and-gift-form" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if (!empty($form_data['data']))  
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @else  
                                            <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @endif
                                        
                                    @endforeach   
                                @else
                                    <li id="li1"><a href="#tab1">#1</a></li>
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if (!empty($form_data['data']))
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <div id="tab{{$i_key + 1}}" class="tab active">
                                        @else
                                            <div id="tab{{$i_key + 1}}" class="tab">
                                        @endif
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Dipotong pajak
                                                        </label>
                                                        <div class="col-sm-5"> 
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    @if(!$item['detail']['dipotong_pajak'])
                                                                        <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" value="1" checked>
                                                                    @else
                                                                        <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" value="1">
                                                                    @endif
                                                                    
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">No</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    @if($item['detail']['dipotong_pajak'])
                                                                        <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" value="2" checked>
                                                                    @else
                                                                        <input type="radio" radio-id='{{$i_key + 1}}' class="custom-control-input" name="type[{{$i_key + 1}}]" value="2">
                                                                    @endif
                                                                   
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">Yes</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="awards-and-gift-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Bruto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row">
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['gross_value'])) 
                                                                        @if($item['detail']['dipotong_pajak'])
                                                                            <input type="text" brutto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="awards-and-gift-brutto" name="brutto[{{$i_key}}]" value="{{$item['gross_value']}}" autocomplete="off" readonly> 
                                                                        @else
                                                                            <input type="text" brutto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="awards-and-gift-brutto" name="brutto[{{$i_key}}]" value="{{$item['gross_value']}}" autocomplete="off" required> 
                                                                        @endif
                                                                    @else
                                                                        <input type="text" brutto-id='{{$i_key + 1}}' class="form-control form-control-sm" id="awards-and-gift-brutto" name="brutto[{{$i_key}}]" value="" autocomplete="off" >    
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label for="awards-and-gift-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Netto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['net_value']))
                                                                        @if($item['detail']['dipotong_pajak'])
                                                                            <input type="text" netto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="awards-and-gift-netto" name="netto[{{$i_key}}]" value="{{$item['net_value']}}" autocomplete="off" required>
                                                                        @else
                                                                        <input type="text" netto-id='{{$i_key + 1}}' class="form-control input-money-format form-control-sm" id="awards-and-gift-netto" name="netto[{{$i_key}}]" value="0" autocomplete="off" readonly>
                                                                        @endif
                                                                    @else
                                                                        <input type="text" netto-id='{{$i_key + 1}}' class="form-control form-control-sm" id="awards-and-gift-netto" name="netto[{{$i_key}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="awards-and-gift-cutter" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['nama_pemotong']))
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter" name="cutter[{{$i_key}}]" value="{{$item['detail']['nama_pemotong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter" name="cutter[{{$i_key}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="awards-and-gift-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            NPWP Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['nama_pemotong']))
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter-npwp" name="cutter-npwp[{{$i_key}}]" value="{{$item['detail']['npwp_pemotong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter-npwp" name="cutter-npwp[{{$i_key}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['bukti_potong']))
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-proof-pph" name="proof_pph[{{$i_key}}]" value="{{$item['detail']['bukti_potong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-proof-pph" name="proof_pph[{{$i_key}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    @if (!empty($item['detail']['tanggal_potong']))
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-date-pph" name="date_pph[{{$i_key}}]" value="{{$item['detail']['tanggal_potong']}}" autocomplete="off" required>
                                                                    @else
                                                                        <input type="text" class="form-control form-control-sm" id="awards-and-gift-date-pph" name="date_pph[{{$i_key}}]" value="" autocomplete="off" readonly>
                                                                    @endif
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Pasal PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control form-control-sm" id="awards-and-gift-type-pph" name="type_pph[{{$i_key}}]" value="PPh Pasal 17" autocomplete="off" readonly>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input id="awards-and-gift-tarif" type="text" class="form-control" id="legacy-giver" name="tarif[{{$i_key}}]" value="5%" autocomplete="off" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            PPh yang Dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class=row>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control form-control-sm" id="awards-and-gift-taken-pph" name="taken_pph[{{$i_key}}]" value="{{$item['detail']['pajak_dibayarkan']}}" autocomplete="off" readonly>        
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                 <div id="tab1" class="tab active">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id[0]" id="id" value="0">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Dipotong pajak
                                                    </label>
                                                    <div class="col-sm-5"> 
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" radio-id='1' class="custom-control-input" name="type[0]" value="1" checked>
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">No</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" radio-id='1' class="custom-control-input" name="type[0]" value="2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Yes</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="awards-and-gift-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" brutto-id='1' class="form-control input-money-format form-control-sm" id="awards-and-gift-brutto" name="brutto[0]" value="" autocomplete="off" required>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="awards-and-gift-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" netto-id='1' class="form-control input-money-format form-control-sm" id="awards-and-gift-netto" name="netto[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="awards-and-gift-cutter" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter" name="cutter[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="awards-and-gift-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        NPWP Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter-npwp" name="cutter-npwp[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nomor Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-proof-pph" name="proof_pph[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-date-pph" name="date_pph[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis Pasal PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-type-pph" name="type_pph[0]" value="PPh Pasal 17" autocomplete="off" readonly>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input id="awards-and-gift-tarif" type="text" class="form-control" id="legacy-giver" name="tarif[0]" value="5%" autocomplete="off"  readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        PPh yang Dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                                <input type="text" brutto-id='1' class="form-control input-money-format form-control-sm" id="awards-and-gift-taken-pph" name="taken_pph[0]" value="" autocomplete="off" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="deleted">
                    
                </div>
    
                @if (\Session::get('is_final_step'))
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                        <button type="submit" class="btn btn-info btn-lg btn-info-nav">Save</button>
                        {{--@endif</div>--}}
                    </div>
                @else
                    <div class="row">
                        <hr class="col-md-12">
                        <div class="col-md-12 center" align="right">
                            {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                            <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                            <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                            <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                            {{--@endif</div>--}}
                        </div>
                    </div>
                @endif
            </div>
        </form>
    </div>
    
    <div id="awards-and-gift-form-template" class="tab-legacy-form" style="display: none">
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="id[0]" id="id" value="0">
                <div class="form-group row">
                    <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Dipotong pajak
                    </label>
                    <div class="col-sm-5 "> 
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="type[0]" value="1" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                        </label>
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="type[0]" value="2">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                        </label>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="awards-and-gift-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai Bruto
                    </label>
                    <div class="col-sm-5">
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-brutto" name="brutto[0]" value="" autocomplete="off" required>
                            
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="awards-and-gift-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai Netto
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-netto" name="netto[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="awards-and-gift-cutter" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nama Pemotong
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter" name="cutter[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="awards-and-gift-cutter-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        NPWP Pemotong
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-cutter-npwp" name="cutter-npwp[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nomor Bukti PPh
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-proof-pph" name="proof_pph[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Tanggal Bukti PPh
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-date-pph" name="date_pph[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
                <div class="form-group row">
                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Jenis Pasal PPh
                    </label>
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-md-8">                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-type-pph" name="type_pph[0]" value="PPh Pasal 17" autocomplete="off" readonly>
                            </div>
                            <div class="col-md-4">
                                <input id="awards-and-gift-tarif" type="text" class="form-control" id="legacy-giver" name="tarif[0]" value="5%" autocomplete="off"readonly>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        PPh yang Dipungut
                    </label>
                    <div class="col-sm-5">
                        
                                <input type="text" class="form-control form-control-sm" id="awards-and-gift-taken-pph" name="taken_pph[0]" value="" autocomplete="off" readonly>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>