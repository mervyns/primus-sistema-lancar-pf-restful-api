<div class="boxed-body w-700 text-left">
    <form id="discount-rate" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ (count($form_data['user_income']) == 0) ? '1' : count($form_data['user_income']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_income']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_income'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['user_income']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_income']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row discount-rate-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format net-value"
                                                            id="net-value" name="net-value[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="description" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Keterangan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="desciption" name="description[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format"
                                                            id="gross-value" name="gross-value[]" value="" placeholder="" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format"
                                                                    id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="tarif" name="tarif[]" value="20%" placeholder="" readonly="">
                                                                <input type="hidden" id="tax_rate" name="tax_rate[]" value="0.2">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['user_income'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <div id="tab{{$r_key + 1}}" class="tab active">
                                            @else
                                                <div id="tab{{$r_key + 1}}" class="tab">
                                            @endif
                                        @else
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        @endif
                                                <div class="row discount-rate-form">
                                                    <div class="col-md-12 col-md-offset-2">

                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                        <br>

                                                        
                                                        <input type="hidden" id="id" name="income_id[]" value="{{ isset($row['id']) ? $row['id'] : '' }}">
                                                        <div class="form-group row">
                                                            <label for="net-value" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                                                Nilai Netto
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format net-value"
                                                                    id="net-value" name="net-value[]" 
                                                                    value="{{ isset($row['net_value']) ? $row['net_value'] : '' }}" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="description" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                                                Keterangan
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="desciption" name="description[]" 
                                                                    value="{{ isset($row['description']) ? $row['description'] : '' }}" placeholder="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="gross-value" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                                                Nilai Bruto
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                    class="form-control form-control-sm input-money-format"
                                                                    id="gross-value" name="gross-value[]" 
                                                                    value="{{ isset($row['gross_value']) ? $row['gross_value'] : '' }}" placeholder="" readonly="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="value" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                                                Pajak final yang telah dipungut
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm input-money-format"
                                                                            id="tax-final" name="tax-final[]" 
                                                                            value="{{ isset($row['tax_final']) ? $row['tax_final'] : '' }}" placeholder="" readonly="">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm"
                                                                            id="tarif" name="tarif[]" value="20%" placeholder="" readonly="">
                                                                        <input type="hidden" id="tax_rate" name="tax_rate[]" value="0.2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>


<div id="discount-rate-form-template" class="row discount-rate-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="income_id[]" value="">
        <div class="form-group row">
            <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Netto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format net-value"
                    id="net-value" name="net-value[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Keterangan
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="desciption" name="description[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Bruto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format"
                    id="gross-value" name="gross-value[]" value="" placeholder="" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pajak final yang telah dipungut
            </label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-md-8">
                        <input type="text"
                            class="form-control form-control-sm input-money-format"
                            id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                    </div>
                    <div class="col-md-4">
                        <input type="text"
                            class="form-control form-control-sm"
                            id="tarif" name="tarif[]" value="20%" placeholder="" readonly="">
                        <input type="hidden" id="tax_rate" name="tax_rate[]" value="0.2">
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>