<div class="boxed-body w-100 text-left">
        <form id="spouse-income-1" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            
                            <div id="tab1" class="tab active">
                                <div class="row spouse-income-1-form">
                                    <div class="col-md-12 col-md-offset-2">                                        
                                        <input type="hidden" id="klu-id" name="klu-id" value="{{ isset($form_data['spouse']->klu_id) ? $form_data['spouse']->klu_id : '' }}">
                                        <div class="form-group row">
                                            <label for="net-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                Penghasilan Netto dari form 1721 A1
                                            </label>
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control form-control-sm input-money-format" 
                                                            id="net-income" name="net-income" 
                                                            value="{{ isset($form_data['user_income']['net_value']) ? $form_data['user_income']['net_value'] : '' }}" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                        data-image="{{ url('/storages/ss-image/spouse-income-option-1/penghasilan_netto_dari_form_1721_a1.PNG') }}"
                                                        data-title="Nama Pemotong Pajak"
                                                        >SS Image</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="withholding-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                Pajak yg telah di pungut pemberi kerja
                                            </label>
                                            <div class="col-sm-4">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control form-control-sm input-money-format" 
                                                            id="withholding-value" name="withholding-value" 
                                                            value="{{ isset($form_data['user_income']['value']) ? $form_data['user_income']['value'] : '' }}" required>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                        data-image="{{ url('/storages/ss-image/spouse-income-option-1/pajak_yang_telah_dipungut_pemberi_kerja.PNG') }}"
                                                        data-title="Nama Pemotong Pajak"
                                                        >SS Image</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>
