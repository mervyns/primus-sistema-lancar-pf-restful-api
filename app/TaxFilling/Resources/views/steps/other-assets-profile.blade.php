<div class="boxed-body w-100 text-left">
    <form  method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="submit" value="skip" class="btn btn-danger btn-sm pull-right"> Skip All <i class="fa fa-forward"></i></button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="text-center col-md-12">
                    <div class="btn-group-select btn-group-multi-select">
                        <div class="row">
                            <div class="text-center col-md-12">
                                <div class="btn-group-select btn-group-multi-select">
                                    <div class="row">
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="44" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_cash_equivalents']) && $form_data['other_cash_equivalents'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Setara Kas</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['other_cash_equivalents']) && $form_data['other_cash_equivalents'] == true ? '44' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="45" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['special_transportation']) && $form_data['special_transportation'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Kendaraan/Peralatan Khusus <br> Kapal Pesiar, Pesawat, Helicopter, <br> Jetsky, dan Peralatan Olahraga Khusus)</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['special_transportation']) && $form_data['special_transportation'] == true ? '45' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="46" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_transportation']) && $form_data['other_transportation'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Alat Transportasi Lainnya</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['other_transportation']) && $form_data['other_transportation'] == true ? '46' : ''  }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-center col-md-12">
                                <div class="btn-group-select btn-group-multi-select">
                                    <div class="row">
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="47" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['electronics_furnitures']) && $form_data['electronics_furnitures'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Peralatan Elektronik dan Furniture</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['electronics_furnitures']) && $form_data['electronics_furnitures'] == true ? '47' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="48" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_immovable_property']) && $form_data['other_immovable_property'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Harta Tidak Bergerak Lainnya</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['other_immovable_property']) && $form_data['other_immovable_property'] == true ? '48' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="49" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_movable_property']) && $form_data['other_movable_property'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Harta Bergerak Lainnya</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['other_movable_property']) && $form_data['other_movable_property'] == true ? '49' : ''  }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-center col-md-12">
                                <div class="btn-group-select btn-group-multi-select">
                                    <div class="row">
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="50" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['account_receivable']) && $form_data['account_receivable'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Piutang</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['account_receivable']) && $form_data['account_receivable'] == true ? '50' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="51" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['affiliate_receivable']) && $form_data['affiliate_receivable'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Piutang Afiliasi</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['affiliate_receivable']) && $form_data['affiliate_receivable'] == true ? '51' : ''  }}">
                                        </div>
                                        <div class="col-md-4" style="margin-bottom:2%">
                                            <button data-value="52" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_receivable']) && $form_data['other_receivable'] == true ? 'active' : ''  }}" style="width: 90%; min-height:90px; padding-left: 0px; padding-right: 0px;">
                                                <p style="margin-bottom: 0px; font-size: 0.9em;">Piutang Lainnya</p>
                                            </button>
                                            <input type="hidden" name="other-asset[]" id="other-asset" 
                                                value="{{ isset($form_data['other_receivable']) && $form_data['other_receivable'] == true ? '52' : ''  }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>