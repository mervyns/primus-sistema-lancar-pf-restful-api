<div class="boxed-body w-100 text-left">
    <form id="deposit" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <!-- <br> -->

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ (count($form_data['deposits']) == 0) ? 1 : count($form_data['deposits']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br>
                <div id="deposit-form-container">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['deposits']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['deposits'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['deposits']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['deposits']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row deposit-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>

                                                <input type="hidden" id="id" name="asset-id[]" value="">

                                                <div class="form-group row">
                                                    <label for="deposit-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Deposito
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="deposit-type[0]" id="deposit-type" 
                                                                value="Deposito Berjangka Bank" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Deposito Berjangka Bank</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="deposit-type[0]" id="deposit-type" 
                                                                value="Deposito Berjangka Koperasi"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Deposito Berjangka Koperasi</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="deposit-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nilai Deposito
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                            id="deposit-balance" name="deposit-balance[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Deposito
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="account-number" name="account-number[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Tanggal Jatuh Tempo
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" 
                                                            id="expired-date" name="expired-date[]"
                                                            value="{{ date('d/m/Y') }}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Bank/Koperasi
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm bank-name" id="bank-name-1" name="bank-name[]" style="width: 100%;" required>
                                                            @foreach($form_data['bank_list'] as $list)
                                                                <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                                                            @endforeach
                                                            <option value="-1">Bank/Koperasi Lainnya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="other-bank" name="other-bank[]" style="display:none;"
                                                                value="" placeholder="Nama Lainnya">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Kota
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="bank-city" name="bank-city[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Cabang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="bank-branch" name="bank-branch[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>

                                                <hr>

                                                <div class="form-group row">
                                                    <label for="deposit-period" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jangka Deposito
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="deposit-period" name="deposit-period[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Bunga Per Tahun
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="interest" name="interest[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="rollover" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Rollover Bunga?
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="rollover[0]" id="rollover" 
                                                                value="Ya" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Ya</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="rollover[0]" id="rollover" 
                                                                value="Tidak"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Tidak</span>
                                                        </label>
                                                    </div>
                                                </div>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['deposits'] as $d_key => $deposit)
                                    @if (isset($deposit['active']))
                                        @if ($deposit['active'])
                                            <div id="tab{{$d_key + 1}}" class="tab active">
                                        @else
                                            <div id="tab{{$d_key + 1}}" class="tab">
                                        @endif
                                    @else
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                    @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row deposit-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" id="id" name="asset-id[]" value="{{ (isset($deposit['id'])) ? $deposit['id'] : '' }}">

                                                    <div class="form-group row">
                                                        <label for="deposit-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Deposito
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="deposit-type[{{ $loop->iteration - 1 }}]" id="deposit-type" 
                                                                    value="Deposito Berjangka Bank" {{ ($deposit['deposit_type']) == 'Deposito Berjangka Bank' ? 'checked' : ''  }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Deposito Berjangka Bank</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="deposit-type[{{ $loop->iteration - 1 }}]" id="deposit-type" 
                                                                    value="Deposito Berjangka Koperasi" {{ ($deposit['deposit_type']) == 'Deposito Berjangka Koperasi' ? 'checked' : ''  }}><span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Deposito Berjangka Koperasi</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Negara
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ ($deposit['country'] == $country['id']) ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ ($deposit['currency'] == $country['id']) ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="deposit-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Deposito
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                id="deposit-balance" name="deposit-balance[]" value="{{ isset($deposit['currency_value']) ? $deposit['currency_value'] : '' }}" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor Deposito
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="account-number" name="account-number[]" value="{{ isset($deposit['deposit_number']) ? $deposit['deposit_number'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Jatuh Tempo
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" id="expired-date" name="expired-date[]" value="{{ isset($deposit['expired_date']) ? date('d-m-Y', strtotime($deposit['expired_date'])) : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Bank/Koperasi
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm bank-name" id="bank-name-{{ $loop->iteration }}" name="bank-name[]" style="width: 100%;">
                                                                @foreach($form_data['bank_list'] as $list)
                                                                    <option value="{{ $list['id'] }}" {{ ($deposit['bank_id'] == $list['id']) ? 'selected' : '' }}>{{ $list['name'] }}</option>
                                                                @endforeach
                                                                <option value="-1" {{ $deposit['bank_id'] == -1 ? 'selected' : '' }}>Bank/Koperasi Lainnya</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="other-bank" name="other-bank[]" style="{{ ($deposit['bank_id'] == -1) ?: 'display:none' }}"
                                                                    value="{{ isset($deposit['other_bank']) ? $deposit['other_bank'] : '' }}" placeholder="Nama Lainnya">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kota
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="bank-city" name="bank-city[]" value="{{ isset($deposit['bank_city']) ? $deposit['bank_city'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Cabang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="bank-branch" name="bank-branch[]" value="{{ isset($deposit['bank_branch']) ? $deposit['bank_branch'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" 
                                                                name="kurs_pajak[]" value="{{ isset($deposit['kurs_value']) ? $deposit['kurs_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" 
                                                            name="jumlah_rupiah[]" value="{{ isset($deposit['idr_value']) ? $deposit['idr_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <hr>

                                                    <div class="form-group row">
                                                        <label for="deposit-period" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jangka Deposito
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="deposit-period" name="deposit-period[]" value="{{ isset($deposit['deposit_period']) ? $deposit['deposit_period'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Bunga Per Tahun
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="interest" name="interest[]" value="{{ isset($deposit['deposit_interest']) ? $deposit['deposit_interest'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="rollover" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Rollover Bunga?
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="rollover[{{ $loop->iteration - 1 }}]" id="rollover" 
                                                                    value="Ya" {{ ($deposit['deposit_rollover']) == 'Ya' ? 'checked' : ''  }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Ya</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="rollover[{{ $loop->iteration - 1 }}]" id="rollover" 
                                                                    value="Tidak" {{ ($deposit['deposit_rollover']) == 'Tidak' ? 'checked' : ''  }}><span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Tidak</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <br> -->
                </div>
            <div id="deleted">
                
            </div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<div id="deposit-form-template" class="row deposit-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>

        <input type="hidden" id="id" name="asset-id[]" value="">
        <div class="form-group row">
            <label for="deposit-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Jenis Deposito
            </label>
            <div class="col-sm-5">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="deposit-type" 
                        value="Deposito Berjangka Bank" checked="">
                        <span class="custom-control-indicator">
                        </span><span class="custom-control-description">Deposito Berjangka Bank</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="deposit-type" 
                        value="Deposito Berjangka Koperasi"><span class="custom-control-indicator">
                        </span><span class="custom-control-description">Deposito Berjangka Koperasi</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Negara
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="deposit-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nilai Deposito
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" 
                    id="deposit-balance" name="deposit-balance[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nomor Deposito
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="account-number" name="account-number[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="expired-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Tanggal Jatuh Tempo
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-picker-format" 
                    id="expired-date" name="expired-date[]"
                    value="{{ date('d/m/Y') }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nama Bank/Koperasi
            </label>
            <div class="col-sm-3">
                <select class="form-control form-control-sm" id="bank-name" name="bank-name[]" style="width: 100%;" required>
                    @foreach($form_data['bank_list'] as $list)
                        <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                    @endforeach
                    <option value="-1">Bank/Koperasi Lainnya</option>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text"
                        class="form-control form-control-sm"
                        id="other-bank" name="other-bank[]" style="display:none;"
                        value="" placeholder="Nama Lainnya">
            </div>
        </div>

        <div class="form-group row">
            <label for="bank-city" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Kota
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="bank-city" name="bank-city[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="bank-branch" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Cabang
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="bank-branch" name="bank-branch[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Kurs Pajak
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jumlah Rupiah
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label for="deposit-period" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jangka Deposito
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="deposit-period" name="deposit-period[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="interest" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Bunga Per Tahun
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="interest" name="interest[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="rollover" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Rollover Bunga?
            </label>
            <div class="col-sm-5">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="rollover" 
                        value="Ya" checked="">
                        <span class="custom-control-indicator">
                        </span><span class="custom-control-description">Ya</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="rollover" 
                        value="Tidak"><span class="custom-control-indicator">
                        </span><span class="custom-control-description">Tidak</span>
                </label>
            </div>
        </div>
    </div>
</div>
    