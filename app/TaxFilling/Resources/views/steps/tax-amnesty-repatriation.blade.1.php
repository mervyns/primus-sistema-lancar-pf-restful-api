<div class="boxed-body w-100 text-left">
    <form id="ta_repatriation" class="form-data" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <input type='hidden' id='revealed_params' name="revealed_params" value="">
        <div class="container">
            @if (!empty(Session::get('encoded_message')))
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach (Session::get('encoded_message') as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div id="cash-form-container">
                <div class="row" align='center'>
                    <div class="col-md-12">
                        <div class="col-sm-5">
                            <select class="form-control" id="asset_rep_category_select" name="" style="width: 100%;">
                                @if (!empty($form_data['asset_category']))
                                    <option value='0|0'>Silahkan pilih kategori asset Anda</option>
                                    @foreach($form_data['asset_category'] as $a_key => $asset_category)
                                        <option value="{{ $asset_category['id'] }}|{{ $asset_category['user_id'] }}">{{ $asset_category['name'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 50px">
                    <div id="table_assets-container" class="col-md-12">
                        
                    </div>
                </div>
                <br><br>
                <div class="row" style="margin-top: 50px">
                    <div id="table_assets_repatriation-container" class="col-md-12">
                        <table id="table_repatriation" class="temp-table table table-hover" align="center">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Harta</th>
                                    <th>Nama Harta</th>
                                    <th>Nilai yang dialihkan ke dalam negri</th>
                                    <th colspan="2">Kode Gateway - Nama Gateway</th>
                                    {{-- <th>Nama Gateway</th> --}}
                                    <th colspan="2">Kode Investasi - Bentuk Investasi</th>
                                    <th>Tanggal Mulai Investasi</th>
                                    <th>Nilai</th>
                                    <th>Mata Uang</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody id="table_repatriation_body"></tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                    <button type="submit" class="btn btn-info btn-lg">Save and Continue Later</button>
                    <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-success btn-lg">Next</a>
                </div>
            </div>
        </div>
    </form>
</div>

