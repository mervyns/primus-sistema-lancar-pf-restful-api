<div class="boxed-body w-700 text-left">
    <form id="other-regular-income" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ (count($form_data['user_income']) == 0) ? '1' : count($form_data['user_income']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_income']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_income'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['user_income']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_income']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row other-regular-income-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="founder" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pendapatan Sudah Dipotong Pajak
                                                    </label>
                                                    <div class="col-sm-2">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="taxes[0]" id="taxes" 
                                                                value="No" checked="">
                                                                <span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">No</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="taxes[0]" id="taxes" 
                                                                value="Yes"><span class="custom-control-indicator">
                                                                </span><span class="custom-control-description">Yes</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis Penghasilan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="income-name" name="income-name[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format gross-value"
                                                            id="gross-value" name="gross-value[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Netto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format net-value"
                                                            id="net-value" name="net-value[]" value="" placeholder="" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="withholder-name" name="withholder-name[]" 
                                                            value="" autocomplete="off" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        NPWP Pemotong
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-npwp-format" 
                                                            id="withholder-npwp" name="withholder-npwp[]" 
                                                            value="" autocomplete="off" maxlength="20" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nomor Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                            id="withholding-slip-number" name="withholding-slip-number[]" 
                                                            value="" autocomplete="off" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Tanggal Bukti PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" 
                                                            id="withholding-slip-date" name="withholding-slip-date[]" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jenis Pasal PPh
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" disabled="" style="width: 100%;">
                                                        <option value=""></option>
                                                            <option value="4">PPh Pasal 23</option>
                                                            <option value="5">PPh Pasal 24 Ayat 2</option>
                                                        </select>
                                                        <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        PPh yang Dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format tax-final"
                                                            id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['user_income'] as $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <div id="tab{{$r_key + 1}}" class="tab active">
                                            @else
                                                <div id="tab{{$r_key + 1}}" class="tab">
                                            @endif
                                        @else
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row other-regular-income-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    
                                                    <input type="hidden" id="id" name="income_id[]" value="{{ isset($row['user_income_id']) ? $row['user_income_id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="founder" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pendapatan Sudah Dipotong Pajak
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="taxes[{{ $loop->iteration - 1 }}]" id="taxes" 
                                                                    value="No" {{ $row['taxes_type'] == 'No' ? 'checked' : '' }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">No</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="taxes[{{ $loop->iteration - 1 }}]" id="taxes" 
                                                                    value="Yes" {{ $row['taxes_type'] == 'Yes' ? 'checked' : '' }}>
                                                                    <span class="custom-control-indicator">
                                                                    </span><span class="custom-control-description">Yes</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Penghasilan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="income-name" name="income-name[]" 
                                                                value="{{ isset($row['income_name']) ? $row['income_name'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Bruto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format gross-value"
                                                                id="gross-value" name="gross-value[]" 
                                                                value="{{ isset($row['gross_value']) ? $row['gross_value'] : '' }}" 
                                                                placeholder="" {{ $row['taxes_type'] == 'Yes' ? 'readonly' : '' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Netto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format net-value"
                                                                id="net-value" name="net-value[]" 
                                                                value="{{ isset($row['net_value']) ? $row['net_value'] : '' }}" 
                                                                placeholder="" {{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="withholder-name" name="withholder-name[]" 
                                                                value="{{ isset($row['withholder_name']) ? $row['withholder_name'] : '' }}" 
                                                                autocomplete="off" {{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            NPWP Pemotong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-npwp-format" 
                                                                id="withholder-npwp" name="withholder-npwp[]" 
                                                                value="{{ isset($row['withholder_npwp']) ? $row['withholder_npwp'] : '' }}" 
                                                                autocomplete="off" maxlength="20"{{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                                id="withholding-slip-number" name="withholding-slip-number[]" 
                                                                value="{{ isset($row['withholding_slip_number']) ? $row['withholding_slip_number'] : '' }}" 
                                                                autocomplete="off" {{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Bukti PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" 
                                                                id="withholding-slip-date" name="withholding-slip-date[]" 
                                                                value="{{ isset($row['withholding_slip_date']) ? date('d/m/Y', strtotime($row['withholding_slip_date'])) : '' }}" 
                                                                {{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Pasal PPh
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" {{ ($row['taxes_type'] == 'No') ? 'disabled' : '' }} style="width: 100%;">
                                                                <option value=""></option>
                                                                <option value="4" {{ isset($row['withholding_tax_slip_id']) ? ($row['withholding_tax_slip_id'] == 4 ? 'selected' : '') : '' }}>PPh Pasal 23</option>
                                                                <option value="5" {{ isset($row['withholding_tax_slip_id']) ? ($row['withholding_tax_slip_id'] == 5 ? 'selected' : '') : '' }}>PPh Pasal 24 Ayat 2</option>
                                                            </select>
                                                            <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="{{ isset($row['withholding_tax_slip_id']) ? $row['withholding_tax_slip_id'] : '' }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            PPh yang Dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format tax-final"
                                                                id="tax-final" name="tax-final[]" 
                                                                value="{{ isset($row['value']) ? $row['value'] : '' }}" 
                                                                placeholder="" {{ $row['taxes_type'] == 'Yes' ? '' : 'readonly' }}>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
            
        </div>
    </form>
</div>


<div id="other-regular-income-form-template" class="row other-regular-income-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="income_id[]" value="">
        <div class="form-group row">
            <label for="founder" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pendapatan Sudah Dipotong Pajak
            </label>
            <div class="col-sm-2">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="taxes" 
                        value="No" checked="">
                        <span class="custom-control-indicator">
                        </span><span class="custom-control-description">No</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="taxes" 
                        value="Yes"><span class="custom-control-indicator">
                        </span><span class="custom-control-description">Yes</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jenis Penghasilan
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="income-name" name="income-name[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Bruto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format gross-value"
                    id="gross-value" name="gross-value[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Netto
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format net-value"
                    id="net-value" name="net-value[]" value="" placeholder="" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nama Pemotong
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="withholder-name" name="withholder-name[]" 
                    value="" autocomplete="off" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                NPWP Pemotong
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-npwp-format" 
                    id="withholder-npwp" name="withholder-npwp[]" 
                    value="" autocomplete="off" maxlength="20" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nomor Bukti PPh
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" 
                    id="withholding-slip-number" name="withholding-slip-number[]" 
                    value="" autocomplete="off" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Tanggal Bukti PPh
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-picker-format" 
                    id="withholding-slip-date" name="withholding-slip-date[]" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jenis Pasal PPh
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="pasal-pph" name="pasal-pph[]" disabled="" style="width: 100%;">
                <option value=""></option>
                    <option value="4">PPh Pasal 23</option>
                    <option value="5">PPh Pasal 24 Ayat 2</option>
                </select>
                <input type="hidden" id="pasal-pph-value" name="pasal-pph-value[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                PPh yang Dipungut
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format tax-final"
                    id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
            </div>
        </div>

    </div>
</div>