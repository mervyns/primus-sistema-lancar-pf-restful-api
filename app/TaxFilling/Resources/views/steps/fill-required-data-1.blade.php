<div class="boxed-body w-100 text-left">
    <form id="fill-required-data1" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif

            <div id="fill-required-data1-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row fill-required-data1-form">
                            <input type="hidden" id="marital" name="marital" value="{{ $form_data['marital'] }}">
                            <div class="col-md-12 col-md-offset-2">
                                <div class="form-group row" style="{{ empty($form_data['me']['npwp']) ? '' : 'display:none;' }}">
                                    <label for="my-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                        NPWP Saya
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control form-control-sm input-npwp-format" 
                                            id="my-npwp" name="my-npwp"
                                            value="{{ $form_data['me']['npwp'] }}"
                                            minlength="20" maxlength="20" min="20" max="20" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-2" style="{{ empty($form_data['me']['efin']) ? '' : 'display:none;' }}">
                                <div class="form-group row">
                                    <label for="my-efin" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                        EFIN Saya
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control form-control-sm" 
                                            id="my-efin" name="my-efin"
                                            value="{{ $form_data['me']['efin'] }}"
                                            maxlength="10" minlength="10" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-md-offset-2" style="{{ empty($form_data['me']['nik']) ? '' : 'display:none;' }}">
                                <div class="form-group row">
                                    <label for="my-nik" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                        NIK Saya
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control form-control-sm"
                                            id="my-nik" name="my-nik"
                                            value="{{ $form_data['me']['nik'] }}"
                                            maxlength="16" minlength="16" required>
                                    </div>
                                </div>
                            </div>
                            @if ($form_data['marital'] == true)
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="form-group row">
                                        <label for="spouse-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                            NPWP Istri/Suami
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm input-npwp-format" id="spouse-npwp" name="spouse-npwp" minlength="20" maxlength="20" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="form-group row">
                                        <label for="spouse-nik" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                            NIK Istri/Suami
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control form-control-sm" id="spouse-nik" name="spouse-nik" minlength="16" minlength="16" required>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>
