
<div class="boxed-body w-100 text-left">
    <div class="container">
        @if ($errors->any())
        <div class="alert alert-danger text-danger">
            <p><strong>Ada kesalahan:</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
                @endforeach
            </ul>
        </div>
        <br>
        @endif
        <div id="final-step-form-container">
            <div class="row">
                <div class="col-md-12">
                    <form id="final-steps" method="post" action="{{ Request::url() }}">
                    {{ csrf_field() }}
                        <div>
                            <div class="form-group row col-lg-12 form-inline">
                                <input class="form-control" name="id" style="width:40px" type="hidden" value='{{$form_data['data']['id']}}' />
                                <label class="col-lg-2"> NPWP : </label>
                                <input class="form-control col-lg-8 input-npwp-format" name="npwp" value="{{$form_data['data']['tax_profile']['npwp']}}" />
                            </div>
                            <div class="form-group row col-lg-12 form-inline">
                                <label class="col-lg-2"> NAMA : </label>
                                <input class="form-control col-lg-8" name="nama" value="{{$form_data['data']['name']}}" />
                            </div>
                            <!-- <div class="form-group row col-lg-12 form-inline">
                                    <label class="col-lg-2"> ALAMAT : </label>
                                    <input class="form-control col-lg-8" name="alamat" />
                                </div> -->
                            <div class="form-group row col-lg-12 form-inline">
                                <label class="col-lg-2"> IKUT TA : </label>

                                @if($form_data['data']['is_ta'])
                                <div class="form-group row col-md-1 form-inline">
                                    <input type="radio" class="form-control" name="ikutta" value="ya" checked /> Ya
                                </div>
                                <div class="form-group row col-md-1 form-inline">
                                    <input type="radio" class="form-control" name="ikutta" value="tidak" /> Tidak
                                </div>
                                @else
                                <div class="form-group row col-md-1 form-inline">
                                    <input type="radio" class="form-control" name="ikutta" value="ya" /> Ya
                                </div>
                                <div class="form-group row col-md-1 form-inline">
                                    <input type="radio" class="form-control" name="ikutta" value="tidak" checked /> Tidak
                                </div>
                                @endif
                            </div>
                        </div>

                    <div style="margin : 100px 20px 20px 20px">
                        <h6>Tabel Penghasilan</h2>
                    </div>
                    <div class="table-responsive" id="final-asset-penghasilan" style="padding-bottom : 100px">
                        <table class="table table-bordered">
                            <thead>
                                <th>No</th>
                                <th>Jenis Penghasilan</th>
                                <th>Penghasilan</th>
                                <th>Pajak yang dibayarkan</th>
                                <th>Keterangan</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @foreach ($form_data['data']['penghasilan'] as $item_key => $item)
                                <tr>
                                    <td>1</td>
                                    <td>{{$item->income_name}}</td>
                                    <td></td>
                                    <td>{{$item->gross_value}}</td>
                                    <td>{{$item->gross_value - $item->net_value}}</td>
                                    <td>
                                        @if (is_array($item->detail))
                                        <ul>
                                            @foreach ($item->detail as $id_key => $itemdetail)
                                            <li>{{$id_key}} : {{$itemdetail}}</li>
                                            @endforeach
                                        </ul>
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td><button class="btn btn-warning pull-right btnEditPenghasilanUtama" value="{{$item->id}}|{{$item->income_step}}">Edit</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7"><button class="btn btn-success pull-right" id="btnAddPenghasilanUtama">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div style="margin : 20px">
                        <h6>Tabel Harta</h2>
                    </div>
                    <div class="table-responsive" id="final-asset-harta" style="padding-bottom : 100px">
                        <table class="table table-bordered">
                            <thead>
                                <th>No</th>
                                <th>Jenis Harta</th>
                                <th>Nama Harta</th>
                                <th>Tahun Perolehan</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @if(!empty($form_data['data']['harta']))
                                @foreach ($form_data['data']['harta'] as $item_key => $item)
                                <tr>
                                    <td>1</td>
                                    <td>{{$item->asset_category_name}}</td>
                                    <td>{{$item->asset_name}}</td>
                                    <td>{{$item->acquisition_year}}</td>
                                    <td>{{$item->idr_value}}</td>
                                    <td>
                                        @if (is_array($item->detail))
                                        <ul>
                                            @foreach ($item->detail as $id_key => $item_deatil)
                                            <li>{{$id_key}} : {{$item_deatil}}</li>
                                            @endforeach
                                        </ul>
                                        @else
                                        '-'
                                        @endif
                                    </td>
                                    <td><button class="btn btn-warning pull-right btnEditHarta" value="{{$item->id}}|{{$item->asset_step}}">Edit</button></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7"><button class="btn btn-success pull-right" id="btnAddHartaFinalStep">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div style="margin : 20px">
                        <h6>Tabel Hutang</h2>
                    </div>
                    <div class="table-responsive" id="final-asset-hutang" style="padding-bottom : 100px">
                        <table class="table table-bordered">
                            <thead>
                                <th>No</th>
                                <th>Jenis Pinjaman</th>
                                <th>Nama Pemberi Pinjaman</th>
                                <th>Tahun Pinjaman</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @if(!empty($form_data['data']['hutang']))
                                @foreach ($form_data['data']['hutang'] as $item_key => $item)
                                <tr>
                                    <td>1</td>
                                    <td>{{$item->liability_name}}</td>
                                    <td>{{$item->lender_name}}</td>
                                    <td>{{$item->acquisition_year}}</td>
                                    <td>{{$item->value}}</td>
                                    <td>
                                        @if (is_array($item->detail))
                                        <ul>
                                            @foreach ($item->detail as $id_key => $item_deatil)
                                            <li>{{$id_key}} : {{$item_deatil}}</li>
                                            @endforeach
                                        </ul>
                                        @else
                                        '-'
                                        @endif
                                    </td>
                                    <td><button class="btn btn-warning pull-right btnEditHutang" value="{{$item->id}}|{{$item->liability_step}}">Edit</button></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7"><button class="btn btn-success pull-right" id="btnAddHutangFinalStep">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    @if (!empty($form_data['data']['additional_profile']['detail']['pph25_monthly']))
                    <div>
                        <h6>Pembayaran PPh 25</h6>

                        <p style="text-align:center; padding-top:30px;">Anda memiliki kewajiban pembayaran tiap bulan sebesar</p>

                        <div class='row' style="padding:10px">
                            <div class='col-md-4 offset-md-4' style="text-align:center;">
                                <button class="btn nominal-button">{{$form_data['data']['additional_profile']['detail']['pph25_monthly']}}</button>
                            </div>
                        </div>

                        <div class='row'>
                            <div class="col-md-6 offset-md-3">
                                <p style="text-align:center; padding-top:10px;">Anda bisa menggunakan SPT GO unutk pembayaran setiap bulan paling lambat tanggal 15 bulan berikutnya untuk masa pajak bulanan dengan menerbitkan IDBilling di menu IDBilling kami</p>
                            </div>
                        </div>

                        <div class='row' style="padding:10px">
                            <div class='col-md-4 offset-md-4' style="text-align:center; margin-top: 20px;  margin-bottom: 20px;">

                                @if($form_data['data']['additional_profile']['detail']['pph25_payment_status'] == 'over')
                                     <h6 style="padding: 20px;">Lebih Bayar</h6>
                                @else
                                     <h6 style="padding: 20px;">Kurang Bayar</h6>
                                @endif
                               
                                <button class="btn nominal-button">{{$form_data['data']['additional_profile']['detail']['pph25_pay']}}</button>
                            </div>
                        </div>

                        @if($form_data['data']['additional_profile']['detail']['pph25_payment_status'] == 'over')
                             <div id='over_payment' class='row' style="padding:20px;">
                                <div class="col-md-12" style="text-align:center;">
                                    <p>Bagaimana perlakuan lebih bayar pajak Anda ?</p>
                                    <div class='row'>
                                        <div class='col-md-4' style="text-align:center;">
                                            <span class="button-checkbox">
                                                    <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 70px; white-space : normal;">Direstitusi</button>
                                                    <input name="" type="checkbox" style="display: none" class="hidden"/>
                                            </span>
                                        </div>
                                        <div class='col-md-4' style="text-align:center;">
                                            <span class="button-checkbox">
                                                    <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 70px; white-space : normal;">Dikembalikan dengan SKKP 17 C</button>
                                                    <input name="" type="checkbox" style="display: none" class="hidden"/>
                                            </span>
                                        </div>
                                        <div class='col-md-4' style="text-align:center;">
                                            <span class="button-checkbox">
                                                    <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 70px; white-space : normal;">Dikembalikan dengan SKPPKP Article 17D</button>
                                                    <input name="" type="checkbox" style="display: none" class="hidden"/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                             <div id='insufficient_payment' class='row' style="padding:20px;">
                                <div class="col-md-6 offset-md-3" style="text-align:center; border:#34ea93; border-style:solid; padding:10px;">
                                    <p>Silahkan melakukan pembayaran Pajak terhutang Anda. Informasi Cara pembayaran dan pelaporan klik disini</p>
                                    <p>Untuk mengunduh dan cetak SPT Anda klik di sini</p>
                                </div>
                            </div>
                        @endif
                    </div>
                    @endif

                </div>
            </div>
            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12 center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalpendapatanfinalsteps" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalassetpassfinaltitle"></h5>
            </div>
            <form id="form_create_penghasilan" class="" method="post" action="{{ Request::url() }}">
                {{ csrf_field() }}
                <div class="form-group row first-select">
                    <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">Kategori Penghasilan</label>
                    <div class="col-sm-5">
                        <input type="hidden" id="flag" name="flag" value="2" />
                        <select name="kategori-penghasilan" id="kategori-penghasilan" class="form-control">
                            <option value="0">Penghasilan Utama</option>
                            <option value="1">Penghasilan Rutin</option>
                            <option value="2">Penghasilan Sekali Terima</option>
                        </select>
                    </div>
                </div>
                <div class="modal-body">
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Go To Page</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalhutangfinalstep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalhutangpassfinaltitle"></h5>
            </div>
            {{-- <form id="form_create_hutang" class="" method="post" action="{{ Request::url() }}">
            {{ csrf_field() }} --}}
            <div class="form-group row first-select">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Kategori Hutang</label>
                <div class="col-sm-5">
                    <input type="hidden" id="flag" name="flag" value="2" />
                    <select name="kategori-hutang" id="kategori-hutang" class="form-control">
                        <option value="54">Institusi Finansial Bank/Non-Bank</option>
                        <option value="55">Hutang Afiliasi</option>
                        <option value="56">Hutang Lainnya</option>
                    </select>
                </div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Go To Page</button>
            </div>
            {{-- </form> --}}
        </div>
    </div>
</div>

<div class="modal fade" id="modalhartafinalstep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalhutangpassfinaltitle"></h5>
            </div>
            {{-- <form id="form_create_hutang" class="" method="post" action="{{ Request::url() }}">
            {{ csrf_field() }} --}}
            <div class="form-group row first-select">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Kategori Harta</label>
                <div class="col-sm-5">
                    <input type="hidden" id="flag" name="flag" value="2" />
                    <select name="kategori-harta" id="kategori-harta" class="form-control">
                        <option value="0">Harta Umum</option>
                        <option value="1">Investasi Finansial</option>
                        <option value="2">Harta Lainnya</option>
                    </select>
                </div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Go To Page</button>
            </div>
            {{-- </form> --}}
        </div>
    </div>
</div>

<div id="penghasilan-sekali-terima" style="display:none">
    <div class="form-group row" id="kategori-penghasilan-sekali-terima">
        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Penghasilan Sekali Terima</label>
        <div class="col-sm-5">
            <select name="select-kategori-penghasilan-sekali-terima" id="select-kategori-penghasilan-sekali-terima" class="form-control">
                <option value="71">Warisan atau Hibah</option>
                <option value="75">Hadiah dan Undian</option>
                <option value="72">Pengalihan Hak Atas Tanah Bangunan</option>
                <option value="73">Keuntungan dari Pengalihan Harta selain Tanah Bangunan</option>
                <option value="74">Pesangon/Tunjangan Hari Tua yang Dibayarkan Sekaligus</option>
                <option value="76">Beasiswa</option>
                <option value="77">Penjualan Saham Pendiri</option>
                <option value="81">Penghargaan dan Hadiah</option>
                <option value="82">Bantuan / Sumbangan</option>
                <option value="78">Honorarium atas Beban APBN/APBD</option>
                <option value="79">Upah pegawai tidak tetap atau tenaga kerja lepas</option>
                <option value="80">Honorarium komisaris yg tdk merangkap pegawai tetap</option>
                <option value="83">Bangunan Yang Diterima dalam Rangka Bangun Guna Serah</option>
                <option value="84">Penghasilan sekali terima lainya</option>
            </select>
        </div>
    </div>
</div>
<div id="penghasilan-rutin" style="display:none">
    <div class="form-group row" id="kategori-penghasilan-rutin">
        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Penghasilan Rutin</label>
        <div class="col-sm-5">
            <select name="select-kategori-penghasilan-rutin" id="select-kategori-penghasilan-rutin" class="form-control">
                <option value="62">Bunga/Diskonto Sertifikat Bank Indonesia dan Surat Berharga Negara</option>
                <option value="63">Bunga/Diskonto Obligasi</option>
                <option value="58">Penghasilan Sewa Tanah/Bangunan</option>
                <option value="59">Dividen</option>
                <option value="64">Penjualan Saham di Bursa Efek</option>
                <option value="65">Royalti dari Hak Kekayaan Intelektual</option>
                <option value="66">Pendapatan Sewa dari Aset Diluar Tanah Bangunan</option>
                <option value="67">Pendapatan Bunga sehubungan dengan Jaminan Pengembalian Hutang</option>
                <option value="60">Bagian Laba Anggota Perseroan Tidak Atas Saham</option>
                <option value="61">Penghasilan atas Deposito</option>
                <option value="68">Imbalan kepada Tenaga Ahli</option>
                <option value="69">Penghasilan Tambahan Lainnya</option>
            </select>
        </div>
    </div>
</div>
<div id="harta-umum" style="display:none">
    <div class="form-group row" id="kategori-harta-umum">
        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Harta Umum</label>
        <div class="col-sm-5">
            <select name="select-kategori-harta-umum" id="select-kategori-harta-umum" class="form-control">
                <option value="19">Deposito</option>
                <option value="25">Logam Mulia</option>
                <option value="21">Kendaraan</option>
                <option value="23">Properti</option>
            </select>
        </div>
    </div>
</div>
<div id="investasi-finansial" style="display:none">
    <div class="form-group row" id="kategori-investasi-finansial">
        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Investasi Finansial</label>
        <div class="col-sm-5">
            <select name="select-kategori-investasi-finansial" id="select-kategori-investasi-finansial" class="form-control">
                <option value="29">Saham Bursa Efek</option>
                <option value="36">Sertifikat Bank Indonesia</option>
                <option value="32">Reksadana</option>
                <option value="33">Asuransi dan komp. Investasi</option>
                <option value="39">Saham Non - Bursa Efek</option>
                <option value="40">Penyertaan Modal Tidak Dalam Saham</option>
                <option value="38">Obligasi Perusahaan</option>
                <option value="37">Obligasi Pemerintah</option>
                <option value="41">Surat Hutang Lainnya</option>
                <option value="42">Investasi Lainnya</option>
            </select>
        </div>
    </div>
</div>
<div id="harta-lainnya" style="display:none">
    <div class="form-group row" id="kategori-harta-lainnya">
        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Harta Lainnya</label>
        <div class="col-sm-5">
            <select name="select-kategori-harta-lainnya" id="select-kategori-harta-lainnya" class="form-control">
                <option value="44">Setara kas lainnya</option>
                <option value="45">Kapal pesiar, pesawat, jetski</option>
                <option value="46">Alat Transportasi Lainnya</option>
                <option value="47">Peralatan Elekronik dan Furniture</option>
                <option value="48">Harta Tidak Bergerak lainnya</option>
                <option value="49">Harta Bergerak Lainnya</option>
                <option value="50">Piutang</option>
                <option value="51">Piutang Afiliasi</option>
                <option value="52">Piutang Lainnya</option>
            </select>
        </div>
    </div>
</div>
