<div class="boxed-body w-100 text-left">
<form id="overseas-income-profile" class="form-data" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12 col-md-offset-2">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <div class="col-md-12" = align="center">
                                <label class="col-form-label col-form-label-lg"> Apakah anda mendapatkan penghasilan dari luar negeri? </label>
                                <br><br><br>

                                <button type="submit" class="btn btn-info btn-lg exc" name="Action" value="Ya" style="width:200px;">Ya</button>
                            <button type="submit" class="btn btn-danger btn-lg" name="Action" value="Tidak" style="width:200px;">Tidak</button>

                            <!-- <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-info btn-lg" style="width:200px;"> Ya </a> -->
                            <!-- <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-danger btn-lg" style="width:200px;"> Tidak </a> -->
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>
        </form>
    </div>    
         