<div class="boxed-body w-100 text-left">
    <form method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="submit" value="skip" class="btn btn-danger btn-sm pull-right"> Skip All <i class="fa fa-forward"></i></button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="text-center col-md-12">
                    <div class="btn-group-select btn-group-multi-select">
                        <div class="row">
                            <div class="col-md-4" align="center" style="margin-bottom:2%">
                                <button data-value="54" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['financial_institution_liability']) && $form_data['financial_institution_liability'] == true ? 'active' : '' }}" style="width: 90%; min-height:100px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Institusi Finansial Bank <br>dan Non Bank</p>
                                </button>
                                <input type="hidden" name="liability[]" id="liability" 
                                    value="{{ isset($form_data['financial_institution_liability']) && $form_data['financial_institution_liability'] == true ? '54' : '' }}">
                            </div>

                            <div class="col-md-4" align="center" style="margin-bottom:2%">
                                <button data-value="55" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['affiliate_liability']) && $form_data['affiliate_liability'] == true ? 'active' : '' }}" style="width: 90%; min-height:100px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Hutang Afiliasi</p>
                                </button>
                                
                                <input type="hidden" name="liability[]" id="liability" 
                                    value="{{ isset($form_data['affiliate_liability']) && $form_data['affiliate_liability'] == true ? '55' : '' }}">
                            </div>

                            <div class="col-md-4" align="center" style="margin-bottom:2%">
                                <button data-value="56" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['other_liability']) && $form_data['other_liability'] == true ? 'active' : '' }}" style="width: 90%; min-height:100px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Hutang Lainnya</p>
                                </button>
                                
                                <input type="hidden" name="liability[]" id="liability" 
                                    value="{{ isset($form_data['other_liability']) && $form_data['other_liability'] == true ? '56' : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="liabilities-profile-form-container">
                <div class="row">
                    <div class="col-sm-12">
                       <hr class="col-md-12">
                        <div class="btn-group" data-toggle="buttons" align="center">
                            <label class="btn btn-secondary col-sm-3 {{ isset($form_data['additional_profile'][0]['hutang_bank_non_bank']) && $form_data['additional_profile'][0]['hutang_bank_non_bank'] === true ? 'active' : '' }}" style="margin-right: 10px; white-space: normal;">
                                <input type="checkbox" name="liability-profile[]" value="1" style="vertical-align: bottom; position: relative; top: -1px;" {{ isset($form_data['additional_profile'][0]['hutang_bank_non_bank']) && $form_data['additional_profile'][0]['hutang_bank_non_bank'] === true ? 'checked' : '' }}> Institusi Finansial Bank dan Non Bank
                            </label>
                            <label class="btn btn-secondary col-sm-3 {{ isset($form_data['additional_profile'][0]['hutang_afiliasi']) && $form_data['additional_profile'][0]['hutang_afiliasi'] === true ? 'active' : '' }}" style="margin-right: 10px; white-space: normal;">
                                <input type="checkbox" name="liability-profile[]" value="3" style="vertical-align: bottom; position: relative; top: -1px;" {{ isset($form_data['additional_profile'][0]['hutang_afiliasi']) && $form_data['additional_profile'][0]['hutang_afiliasi'] === true ? 'checked' : '' }}> Hutang Afiliasi

                            </label>
                            <label class="btn btn-secondary col-sm-3 {{ isset($form_data['additional_profile'][0]['hutang_lainnya']) && $form_data['additional_profile'][0]['hutang_lainnya'] === true ? 'active' : '' }}" style="margin-right: 10px; white-space: normal;">
                                <input type="checkbox" name="liability-profile[]" value="4" style="vertical-align: bottom; position: relative; top: -1px;" {{ isset($form_data['additional_profile'][0]['hutang_lainnya']) && $form_data['additional_profile'][0]['hutang_lainnya'] === true ? 'checked' : '' }}> Hutang Lainnya
                            </label>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>-->

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>