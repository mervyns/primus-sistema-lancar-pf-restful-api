<div class="boxed-body w-700 text-left">
    <form id="land-and-building" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (empty($form_data['properties']))
                <input type="hidden" name="empty-signal" id="empty-signal" value="Mohon untuk mengisi data property sebelum melanjutkan">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ (count($form_data['building']) == 0) ? '1' : count($form_data['building']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="land-and-building-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['building']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['building'] as $building)
                                        <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['building']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row land-and-building-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="penanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak ditanggung
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="penanggung[0]" id="penanggung" value="Penyewa" checked=""><span class="custom-control-indicator"></span><span class="custom-control-description">Penyewa</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                            <input type="radio"
                                                                class="custom-control-input" name="penanggung[0]" id="penanggung" value="Pemilik"><span class="custom-control-indicator"></span><span class="custom-control-description">Pemilik</span>
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Sewa
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format rent-price"
                                                            id="rent-price" name="rent-price[]" value="" placeholder="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai sewa yang diterima
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format rent-price-received"
                                                            id="rent-price-received" name="rent-price-received[]" value="" placeholder="" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                       
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <input type="text"
                                                                        class="form-control form-control-sm input-money-format tax-final"
                                                                        id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <input type="text"
                                                                        class="form-control form-control-sm"
                                                                        id="tarif" name="tarif[]" 
                                                                        value="{{ isset($form_data['income']->tax_rate) ? ((float)$form_data['income']->tax_rate * 100).'%' : 0 }}" readonly="">
                                                                    <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                                                        value="{{ isset($form_data['income']->tax_rate) ? $form_data['income']->tax_rate : '0' }}">
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Dari Asset
                                                    </label>
                                                    <div class="col-sm-5">
                                                        @if(!empty($form_data['properties']))
                                                            <select class="form-control form-control-sm" id="from-asset" name="from-asset[]" style="width: 100%;" required>
                                                                @foreach ($form_data['properties'] as $property)
                                                                    <option value="{{ $property->id }}" data-country="{{ $property->country_id }}" data-asset="{{ $property->asset_id }}">{{ $property->property_type }} {{ ' - ' }} {{ $property->city }} </option>
                                                                @endforeach
                                                            </select>
                                                            <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['properties']) > 0 ? $form_data['properties'][0]['country_id'] : '' }}">
                                                            <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['properties']) > 0 ? $form_data['properties'][0]['asset_id'] : '' }}">
                                                        @endif
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['building'] as $building)
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                            <div class="row land-and-building-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="income_id[]" value="{{ isset($building['id']) ? $building['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="penanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak ditanggung
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="penanggung[{{ $loop->iteration - 1 }}]" id="penanggung" 
                                                                    value="Penyewa" {{ ($building['type'] == 'Penyewa') ? 'checked' : '' }}><span class="custom-control-indicator"></span><span class="custom-control-description">Penyewa</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio"
                                                                    class="custom-control-input" name="penanggung[{{ $loop->iteration - 1 }}]" id="penanggung" 
                                                                    value="Pemilik" {{ ($building['type'] == 'Pemilik') ? 'checked' : '' }}><span class="custom-control-indicator"></span><span class="custom-control-description">Pemilik</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Sewa
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format rent-price"
                                                                id="rent-price" name="rent-price[]" 
                                                                value="{{ isset($building['gross_value']) ? $building['gross_value'] : '' }}" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai sewa yang diterima
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format rent-price-received"
                                                                id="rent-price-received" name="rent-price-received[]" 
                                                                value="{{ isset($building['rent_price_received']) ? $building['rent_price_received'] : '' }}" placeholder="" readonly="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak final yang telah dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                        
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm input-money-format tax-final"
                                                                            id="tax-final" name="tax-final[]" 
                                                                            value="{{ isset($building['net_value']) ? $building['net_value'] : '' }}" placeholder="" readonly="">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm"
                                                                            id="tarif" name="tarif[]" value="{{ isset($building['tax_rate']) ? ((float)$building['tax_rate'] * 100) .'%' : '' }}" placeholder="" readonly="">
                                                                        <input type="hidden" id="tax_rate" name="tax_rate[]" value="{{ isset($building['tax_rate']) ? $building['tax_rate'] : '' }}">
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Dari Asset
                                                        </label>
                                                        <div class="col-sm-5">
                                                            @if(!empty($form_data['properties']))
                                                                <select class="form-control form-control-sm" id="from-asset" name="from-asset[]" style="width: 100%;" required>
                                                                    @foreach ($form_data['properties'] as $property)
                                                                        <option value="{{ $property->id }}" 
                                                                            data-country="{{ $property->country_id }}" data-asset="{{ $property->asset_id }}"
                                                                            {{ ($building['user_asset_id'] == $property->id) ? 'selected' : '' }}> {{ $property->property_type }} {{ ' - ' }} {{ $property->city }} </option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="hidden" name="country_id[]"  id="country_id" 
                                                                    value="{{ isset($building['country_id']) ? $building['country_id'] : '' }}">
                                                                <input type="hidden" name="asset_id[]" id="asset_id" 
                                                                    value="{{ isset($building['asset_id']) ? $building['asset_id'] : '' }}">
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>


<div id="land-and-building-form-template" class="row land-and-building-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="income_id[]" value="">
        <div class="form-group row">
            <label for="penanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pajak ditanggung
            </label>
            <div class="col-sm-5">
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="penanggung" value="Penyewa" checked=""><span class="custom-control-indicator"></span><span class="custom-control-description">Penyewa</span>
                </label>
                <label class="custom-control custom-radio" style="margin-top: 5px;">
                    <input type="radio"
                        class="custom-control-input" id="penanggung" value="Pemilik"><span class="custom-control-indicator"></span><span class="custom-control-description">Pemilik</span>
                </label>
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Sewa
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format rent-price"
                    id="rent-price" name="rent-price[]" value="" placeholder="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai sewa yang diterima
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format rent-price-received"
                    id="rent-price-received" name="rent-price-received[]" value="" placeholder="" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pajak final yang telah dipungut
            </label>
            <div class="col-sm-5">
                
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text"
                                class="form-control form-control-sm input-money-format tax-final"
                                id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                        </div>
                        <div class="col-md-4">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="tarif" name="tarif[]" 
                                value="{{ isset($form_data['income']->tax_rate) ? ((float)$form_data['income']->tax_rate * 100).'%' : '0' }}" readonly="">
                            <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                value="{{ isset($form_data['income']->tax_rate) ? $form_data['income']->tax_rate : '0' }}">
                        </div>
                    </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Dari Asset
            </label>
            <div class="col-sm-5">
                @if(!empty($form_data['properties']))
                    <select class="form-control form-control-sm" id="from-asset" name="from-asset[]" style="width: 100%;" required>
                        @foreach ($form_data['properties'] as $property)
                            <option value="{{ $property->id }}" data-country="{{ $property->country_id }}" data-asset="{{ $property->asset_id }}"> {{ $property->property_type }} {{ ' - ' }} {{ $property->city }} </option>
                        @endforeach
                    </select>
                    <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['properties']) > 0 ? $form_data['properties'][0]['country_id'] : '' }}">
                    <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['properties']) > 0 ? $form_data['properties'][0]['asset_id'] : '' }}">
                @endif
            </div>
        </div>
        
    </div>
</div>
