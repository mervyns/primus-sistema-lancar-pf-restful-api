<div class="boxed-body w-100 text-left">
    <form id="luxury-goods" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            {{ csrf_field() }}
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div id="form-luxury-goods-notification" style="{{ count($form_data['user_income']) > 0 ? 'display:none;' : '' }}">
                <div class="col-md-12 col-md-offset-2">
                    <div class="form-group row">
                        <div class="col-md-12" align="center">
                            <label class="col-form-label col-form-label-lg"> Apakah anda menerima bukti potongan atas pembelian barang mewah? </label>
                            <br><br><br>
                            <a href="javascript:void(0)" class="btn btn-info btn-lg" id="btn-luxury-goods-yes" style="width:200px;"> Ya </a>
                            <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-danger btn-lg" id="btn-luxury-goods-no" style="width:200px;"> Tidak </a>
                            <br><br><br><br>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                data-image="{{ url('/storages/ss-image/main-income/pph_yang_telah_dipotong.PNG') }}"
                                data-title="PPh yang Telah Dipotong"
                            >SS Image</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="form-luxury-goods-input" style="{{ count($form_data['user_income']) > 0 ? '' : 'display:none;' }}">
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ empty($form_data['user_charities']) ? '1' : count($form_data['user_charities']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                <div id="luxury-goods-form-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="tab-links" id="tabs">
                                    @if(empty($form_data['user_income']))
                                        <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                    @else
                                        @foreach($form_data['user_income'] as $income)
                                            <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                                <div class="tab-content" id="tab-content">
                                    @if(empty($form_data['user_income']))
                                        <div id="tab1" class="tab active">
                                            <div class="row luxury-goods-form">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                        </div>
                                                    </div>

                                                    <input type="hidden" id="id" name="income-id[]" value="">
                                                    <div class="form-group row">
                                                        <label for="income-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Barang Mewah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm income-name" id="income-name-1" name="income-name[]" style="width: 100%;">
                                                                @foreach($form_data['income'] as $row)
                                                                    <option value="{{ $row['id'] }}" data-taxrate="{{ $row['tax_rate'] }}">{{ $row['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="ppnbm" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Harga tidak termasuk PPN dan PPnBM
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input class="form-control form-control-sm input-money-format" id="ppnbm" name="ppnbm[]" value="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak PPh 22 yang Telah Dipungut
                                                        </label>
                                                        <div class="col-sm-5">
                                                        
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm input-money-format"
                                                                            id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <input type="text"
                                                                            class="form-control form-control-sm"
                                                                            id="tarif" name="tarif[]" 
                                                                            value="{{ count($form_data['income']) > 0 ? ((float)$form_data['income'][0]['tax_rate'] * 100).'%' : '0%' }}" readonly="">
                                                                        <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                                                            value="{{ count($form_data['income']) > 0 ? $form_data['income'][0]['tax_rate'] : 0 }}">
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Pemotong Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" value="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            NPWP Pemotong Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" value="" minlength="20" maxlength="20" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nomor Bukti Potong
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" value="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Tanggal Bukti Potong
                                                        </label>
                                                        <div class="col-sm-3">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" value="" required>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @foreach ($form_data['user_income'] as $income)
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                                <div class="row luxury-goods-form">
                                                    <div class="col-md-12 col-md-offset-2">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                            </div>
                                                        </div>

                                                        <input type="hidden" id="id" name="income-id[]" value="{{ isset($income['user_income_id']) ? $income['user_income_id'] : '' }}">
                                                        <div class="form-group row">
                                                            <label for="income-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Jenis Barang Mewah
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm income-name" id="income-name-{{ $loop->iteration }}" name="income-name[]" style="width: 100%;">
                                                                    @foreach($form_data['income'] as $row)
                                                                        <option value="{{ $row['id'] }}" data-taxrate="{{ $row['tax_rate'] }}" {{ $income['income_id'] == $row['id'] ? 'selected' : '' }}>{{ $row['name'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="ppnbm" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Harga tidak termasuk PPN dan PPnBM
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input class="form-control form-control-sm input-money-format" 
                                                                    id="ppnbm" name="ppnbm[]" 
                                                                    value="{{ !isset($income['gross_value']) ? '' : $income['gross_value'] }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Pajak PPh 22 yang Telah Dipungut
                                                            </label>
                                                            <div class="col-sm-5">
                                                            
                                                                    <div class="row">
                                                                        <div class="col-md-8">
                                                                            <input type="text"
                                                                                class="form-control form-control-sm input-money-format"
                                                                                id="tax-final" name="tax-final[]" 
                                                                                value="{{ !isset($income['value']) ? '' : $income['value'] }}" 
                                                                                placeholder="" readonly="">
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <input type="text"
                                                                                class="form-control form-control-sm"
                                                                                id="tarif" name="tarif[]" 
                                                                                value="{{ !isset($income['tax_rate']) ? '0%' : ($income['tax_rate'] * 100).'%' }}" readonly="">
                                                                            <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                                                                value="{{ !isset($income['tax_rate']) ? 0 : $income['tax_rate'] }}">
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Nama Pemotong Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" 
                                                                    value="{{ isset($income['withholder_name']) ? $income['withholder_name'] : '' }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                NPWP Pemotong Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" 
                                                                    value="{{ isset($income['withholder_npwp']) ? $income['withholder_npwp'] : '' }}" minlength="20" maxlength="20" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Nomor Bukti Potong
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" 
                                                                    value="{{ isset($income['withholding_slip_number']) ? $income['withholding_slip_number'] : '' }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Tanggal Bukti Potong
                                                            </label>
                                                            <div class="col-sm-3">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" 
                                                            value="{{ isset($income['withholding_slip_date']) ? date('d/m/Y', strtotime($income['withholding_slip_date'])) : '' }}" required>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div id="deleted"></div>
                
                <div class="row">
                    <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<div id="luxury-goods-form-template" class="row luxury-goods-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        
        <input type="hidden" id="id" name="income-id[]" value="">
        <div class="form-group row">
            <label for="income-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jenis Barang Mewah
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="income-name" name="income-name[]" style="width: 100%;">
                    @foreach($form_data['income'] as $row)
                        <option value="{{ $row['id'] }}" data-taxrate="{{ $row['tax_rate'] }}">{{ $row['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="ppnbm" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Harga tidak termasuk PPN dan PPnBM
            </label>
            <div class="col-sm-5">
                <input class="form-control form-control-sm input-money-format" id="ppnbm" name="ppnbm[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Pajak PPh 22 yang Telah Dipungut
            </label>
            <div class="col-sm-5">
            
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text"
                                class="form-control form-control-sm input-money-format"
                                id="tax-final" name="tax-final[]" value="" placeholder="" readonly="">
                        </div>
                        <div class="col-md-4">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="tarif" name="tarif[]" 
                                value="{{ count($form_data['income']) > 0 ? ((float)$form_data['income'][0]['tax_rate'] * 100).'%' : '0%' }}" readonly="">
                            <input type="hidden" id="tax_rate" name="tax_rate[]" 
                                value="{{ count($form_data['income']) > 0 ? $form_data['income'][0]['tax_rate'] : 0 }}">
                        </div>
                    </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nama Pemotong Pajak
            </label>
            <div class="col-sm-5">
                <input class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                NPWP Pemotong Pajak
            </label>
            <div class="col-sm-5">
                <input class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" value="" minlength="20" maxlength="20" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nomor Bukti Potong
            </label>
            <div class="col-sm-5">
                <input class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" value="">
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Tanggal Bukti Potong
            </label>
            <div class="col-sm-3">
            <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" value="" required>
            </div>
        </div>
        
    </div>
</div>

<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>