<div class="boxed-body w-100 text-left">
    <form  id="additional-regular-incomes" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" name="submit" value="skip" class="btn btn-danger btn-sm pull-right"> Skip All <i class="fa fa-forward"></i></button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="text-center col-md-12">
                    
                    <div class="btn-group-select btn-group-multi-select">
                        <div class="row">
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="58" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['sewa_tanah_bangunan']) && $form_data['sewa_tanah_bangunan'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Penghasilan Sewa <br> Tanah/Bangunan</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information " 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Penghasilan Sewa Tanah/Bangunan"
                                    data-message="Penghasilan yang diterima atau diperoleh orang pribadi atau badan dari persewaan tanah dan atau bangunan berupa tanah, rumah, rumah susun, apartemen, kondominium, gedung perkantoran, rumah kantor, gudang dan industri terutang pajak yang bersifat final.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['sewa_tanah_bangunan']) && $form_data['sewa_tanah_bangunan'] == true ? '58' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="59" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['dividen_saham']) && $form_data['dividen_saham'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Dividen dari <br> Saham/Investasi</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Dividen dari Saham/Investasi"
                                    data-message="Dividen adalah suatu hasil laba usaha yang dibagikan kepada pemegang saham dengan memperhatikan banyaknya saham yang dimiliki dari masing-masing pemegang saham.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['dividen_saham']) && $form_data['dividen_saham'] == true ? '59' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="60" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['laba_anggota']) && $form_data['laba_anggota'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Bagian Laba Anggota <br> Perseroan Komanditer tidak atas <br> Saham, Persekutuan, Perkumpulan, <br> Firma, Kongsi</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Bagian Laba Anggota Perseroan Komanditer tidak atas Saham, Persekutuan, Perkumpulan, Firma, Kongsi"
                                    data-message="Merupakan laba yang di berikan perusahaan selain dalam bentuk PT.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['laba_anggota']) && $form_data['laba_anggota'] == true ? '60' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="61" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['penghasilan_deposito']) && $form_data['penghasilan_deposito'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Penghasilan atas Deposito</p>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['penghasilan_deposito']) && $form_data['penghasilan_deposito'] == true ? '61' : ''  }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="62" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['bunga_sbi_sbn']) && $form_data['bunga_sbi_sbn'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Bunga/Diskonto Sertifikat <br> Bank Indonesia dan Surat <br> Berharga Negara (Sun,Sukuk,Ori)</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Bunga/Diskonto Sertifikat Bank Indonesia dan Surat Berharga Negara (Sun,Sukuk,Ori)"
                                    data-message="Merupakan penghasian yang diperoleh atas pembayaran bunga.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['bunga_sbi_sbn']) && $form_data['bunga_sbi_sbn'] == true ? '62' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="63" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['bunga_obligasi']) && $form_data['bunga_obligasi'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Bunga / Diskonto Obligasi <br> Pemerintah dan Perusahaan</p>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['bunga_obligasi']) && $form_data['bunga_obligasi'] == true ? '63' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="64" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['penjualan_saham']) && $form_data['penjualan_saham'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Penjualan Saham di <br> Bursa Efek</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Penjualan Saham di Bursa Efek"
                                    data-message="Hasil keseluruhan dari penjualan saham di Bursa Efek.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['penjualan_saham']) && $form_data['penjualan_saham'] == true ? '64' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="65" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['hak_kekayaan_intelektual']) && $form_data['hak_kekayaan_intelektual'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Royalti dari <br>Hak Kekayaan Intelektual</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Royalti dari Hak Kekayaan Intelektual"
                                    data-message="Royalti adalah suatu jumlah yang dibayarkan atau terutang dengan cara atau merek dagang, atau bentuk hak kekayaan intelektual / industrial.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['hak_kekayaan_intelektual']) && $form_data['hak_kekayaan_intelektual'] == true ? '65' : ''  }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="66" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['sewa_asset']) && $form_data['sewa_asset'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Pendapatan Sewa dari <br> Aset Diluar Tanah Bangunan</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Pendapatan Sewa dari Aset Diluar Tanah Bangunan"
                                    data-message="Merupakan pendapatan yang diperoleh dari Aset seperti sewa mobil, mesin.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['sewa_asset']) && $form_data['sewa_asset'] == true ? '66' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="67" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['bunga_hutang']) && $form_data['bunga_hutang'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Pendapatan Bunga sehubungan <br> dengan jaminan pengembalian <br> Hutang</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Pendapatan Bunga sehubungan dengan jaminan pengembalian Hutang"
                                    data-message="Suku bunga adalah pembayaran bunga tahunan dari suatu pinjaman, dalam bentuk persentase dari pinjaman yang diperoleh dari jumlah bunga yang diterima tiap tahun dibagi dengan jumlah pinjaman.">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['bunga_hutang']) && $form_data['bunga_hutang'] == true ? '67' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="68" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['tenaga_ahli']) && $form_data['tenaga_ahli'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Imbalan Kepada tenaga ahli</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Imbalan Kepada tenaga ahli"
                                    data-message="Merupakan imbalan kepada orang yang melakukan pekerjaan bebas (notaris, pengacara)">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['tenaga_ahli']) && $form_data['tenaga_ahli'] == true ? '68' : ''  }}">
                            </div>
                            <div class="col-md-3" style="margin-bottom:2%">
                                <button data-value="69" type="button" class="btn btn-lg btn-outline-success btn-click {{ isset($form_data['penghasilan_tambahan_lain']) && $form_data['penghasilan_tambahan_lain'] == true ? 'active' : ''  }}" style="width: 100%; min-height:120px; padding-left: 0px; padding-right: 0px;">
                                    <p style="margin-bottom: 0px; font-size: 0.9em;">Penghasilan tambahan  lainya</p>
                                </button>
                                <button type="button" id="btn-popup-information" 
                                    class="btn bttn-info btn-popup-information" 
                                    style="position: absolute; bottom: 0; right: 0; margin-right: 5%;background-color:#007bff;border-color:transparent"
                                    data-toggle="tooltip" title="Klik disini untuk informasi"
                                    data-title="Penghasilan tambahan  lainya"
                                    data-message="contoh laba dari valas, bitcoin, atau makelar tetapi tidak berkesinambungan">
                                    <i class="fa fa-info"></i>
                                </button>
                                <input type="hidden" name="invesment[]" id="invesment" 
                                    value="{{ isset($form_data['penghasilan_tambahan_lain']) && $form_data['penghasilan_tambahan_lain'] == true ? '69' : ''  }}">
                            </div>
                        </div>
                        
                    </div>
                    <br><br>

                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12 center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>