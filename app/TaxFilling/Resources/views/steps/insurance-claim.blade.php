<div class="boxed-body w-100 text-left">
    <form id="insurance-claim" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ count($form_data['user_income']) == 0 ? '1' : count($form_data['user_income']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>

            <div id="insurance-claim-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_income']) == 0)
                                    <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_income'] as $row)
                                        <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_income']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row insurance-claim-form">
                                            <div class="col-md-12 col-md-offset-2">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income-id[]" value="">
                                                <input type="hidden" = id="asset-id" name="asset-id[]" value="">
                                                <div class="form-group row">
                                                    <label for="insurance-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Asuransi
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="insurance-type" name="insurance-type[]" style="width: 100%;" required>
                                                            <option value="Kesehatan">Kesehatan</option>
                                                            <option value="Kecelakaan">Kecelakaan</option>
                                                            <option value="Jiwa">Jiwa</option>
                                                            <option value="Dwiguna">Dwiguna</option>
                                                            <option value="Pendidikan">Pendidikan</option>
                                                            <option value="Kendaraan">Kendaraan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nilai Klaim
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="company" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Perusahaan Asuransi
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" id="company" name="company[]" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Polis
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm account-number" id="account-number" name="account-number[]" style="width: 100%;" required>
                                                            <option value=""></option>
                                                            @foreach ($form_data['user_asset'] as $user_asset)
                                                                <option value="{{ $user_asset['id'] }}" data-company="{{ $user_asset['company_name'] }}" data-asset="{{ $user_asset['asset_id'] }}">{{ $user_asset['account_number'] . ' - ' . $user_asset['company_name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['user_income'] as $row)
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                            <div class="row insurance-claim-form">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="id" name="income-id[]" value="{{ isset($row['id']) ? $row['id'] : '' }}">
                                                    <input type="hidden" = id="asset-id" name="asset-id[]" value="{{ isset($row['asset_id']) ? $row['asset_id'] : ''}}">
                                                    <div class="form-group row">
                                                        <label for="insurance-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Jenis Asuransi
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="insurance-type" name="insurance-type[]" style="width: 100%;" required>
                                                                <option value="Kesehatan" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Kesehatan' ? 'selected' : '' }}>Kesehatan</option>
                                                                <option value="Kecelakaan" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Kecelakaan' ? 'selected' : '' }}>Kecelakaan</option>
                                                                <option value="Jiwa" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Jiwa' ? 'selected' : '' }}>Jiwa</option>
                                                                <option value="Dwiguna" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Dwiguna' ? 'selected' : '' }}>Dwiguna</option>
                                                                <option value="Pendidikan" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Pendidikan' ? 'selected' : '' }}>Pendidikan</option>
                                                                <option value="Kendaraan" {{ isset($row['insurance_type']) && $row['insurance_type'] == 'Kendaraan' ? 'selected' : '' }}>Kendaraan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ isset($row['currency']) && $row['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nilai Klaim
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" 
                                                                value="{{ isset($row['net_value']) ? $row['net_value'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" 
                                                                value="{{ isset($row['kurs_value']) ? $row['kurs_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jumlah Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                            value="{{ isset($row['currency_value']) ? $row['currency_value'] : '' }}" readonly="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="company" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nama Perusahaan Asuransi
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" id="company" name="company[]"
                                                            value="{{ isset($row['company_name']) ? $row['company_name'] : '' }}" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nomor Polis
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm account-number" id="account-number" name="account-number[]" style="width: 100%;" required>
                                                                <option value=""></option>
                                                                @foreach ($form_data['user_asset'] as $user_asset)
                                                                    <option value="{{ $user_asset['id'] }}" 
                                                                        data-company="{{ $user_asset['company_name'] }}" 
                                                                        data-asset="{{ $user_asset['asset_id'] }}"
                                                                        {{ isset($row['user_asset_id']) && $row['user_asset_id'] == $user_asset['id'] ? 'selected' : '' }}>{{ $user_asset['account_number'] . ' - ' . $user_asset['company_name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>

            <div id="deleted">
                
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>

<div id="insurance-claim-form-template" class="row insurance-claim-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>
        <input type="hidden" id="id" name="income-id[]" value="">
        <input type="hidden" = id="asset-id" name="asset-id[]" value="">
        <div class="form-group row">
            <label for="insurance-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Jenis Asuransi
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="insurance-type" name="insurance-type[]" style="width: 100%;" required>
                    <option value="Kesehatan">Kesehatan</option>
                    <option value="Kecelakaan">Kecelakaan</option>
                    <option value="Jiwa">Jiwa</option>
                    <option value="Dwiguna">Dwiguna</option>
                    <option value="Pendidikan">Pendidikan</option>
                    <option value="Kendaraan">Kendaraan</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="acquisition-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nilai Klaim
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" id="acquisition-value" name="acquisition-value[]" value="" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Kurs Pajak
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
            </div>
        </div>
        <div class="form-group row">
            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Jumlah Rupiah
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
            </div>
        </div>

        <div class="form-group row">
            <label for="company" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nama Perusahaan Asuransi
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm" id="company" name="company[]" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nomor Polis
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm account-number" id="account-number" name="account-number[]" style="width: 100%;" required>
                    <option value=""></option>
                    @foreach ($form_data['user_asset'] as $user_asset)
                        <option value="{{ $user_asset['id'] }}" data-company="{{ $user_asset['company_name'] }}" data-asset="{{ $user_asset['asset_id'] }}">{{ $user_asset['account_number'] . ' - ' . $user_asset['company_name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
    </div>
</div>
