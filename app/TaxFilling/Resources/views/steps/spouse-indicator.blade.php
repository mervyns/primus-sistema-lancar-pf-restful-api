<div class="boxed-body w-100 text-left">
        <form id="spouse-indicator" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12" align="center">
                        <h6>Anda mengindikasi bahwa pasangan anda memiliki NPWP sendiri.</h6>
                        <p>Would you like to invite your partner to use SPT GO?</p>
                        <br>
                        <a href="javascript:void(0)" class="btn btn-info btn-lg exc" name="yes" id="yes" style="width:200px;">Yes</a>
                        <a href="{!! $steps[$current_step['next']]['url'] !!}" class="btn btn-danger btn-lg" name="not-now" id="not-now" style="width:200px;">Not Now</a>
                    </div>
                </div>

            </div>
        </form>
    </div>

<div class="modal fade" id="modal_spouse_invite" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="invite-spouse" method="post">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Invite Suami/Istri Anda</h5>
                    <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
                </div>
                <div class="modal-body">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="spouse-email" class="col-sm-4 col-form-label col-form-label-sm">
                                    Email Istri/Suami
                                </label>
                                <div class="col-sm-8">
                                    <input type="email"
                                        class="form-control form-control-sm"
                                        id="spouse-email" name="spouse-email" value="{{$form_data['name']}}" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info">Invite Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>