<div class="boxed-body w-100 text-left">
        <form id="transfer-bulding-rights-form" class="main-form" method="post" action="{{ Request::url() }}">
            <div class="container">
                @if (!empty($form_data['is_final_steps_submited']))
                    <input type="hidden" name="close-signal" id="close-signal" value="1">
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>
    
                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab-transfer-building-rights" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br><br>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if (!empty($form_data['data']))  
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @else  
                                            <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                        @endif
                                        
                                    @endforeach   
                                @else
                                    <li id="li1"><a href="#tab1">#1</a></li>
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if (!empty($form_data['data']))
                                    @foreach ($form_data['data'] as  $i_key =>  $item)
                                        @if ($item['active'])
                                            <div id="tab{{$i_key + 1}}" class="tab active">
                                        @else
                                            <div id="tab{{$i_key + 1}}" class="tab">
                                        @endif
                                            <div class="row">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="pajak-ditanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Pajak ditanggung
                                                        </label>
                                                        <div class="col-sm-5"> 
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    @if (!empty($item['detail']['pajak_ditanggung']) && $item['detail']['pajak_ditanggung']=='penyewa')
                                                                        <input type="radio" class="custom-control-input" name="role[{{$i_key}}]" value="penyewa" checked>
                                                                    @else
                                                                        <input type="radio" class="custom-control-input" name="role[{{$i_key}}]" value="penyewa">
                                                                    @endif
                                                                    
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">Penyewa</span>
                                                            </label>
                                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                    @if (!empty($item['detail']['pajak_ditanggung']) && $item['detail']['pajak_ditanggung']=='pemilik')
                                                                        <input type="radio" class="custom-control-input" name="role[{{$i_key}}]" value="pemilik" checked>
                                                                    @else
                                                                        <input type="radio" class="custom-control-input" name="role[{{$i_key}}]" value="pemilik">
                                                                    @endif
                                                                   
                                                                        <span class="custom-control-indicator"></span>
                                                                        <span class="custom-control-description">Pemilik</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group has-error row">
                                                        <label for="transfer-bulding-rights-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Transaksi
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" class="form-control input-money-format" transaksi-id="{{$i_key}}" id="transfer-bulding-rights-value" name="value[{{$i_key}}]" value="{{$item['gross_value']}}" autocomplete="off" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label for="transfer-bulding-rights-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Alamat / Kota
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="transfer-bulding-rights-address" name="address[{{$i_key}}]" value="{{$item['detail']['alamat']}}" autocomplete="off" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai yang diterima
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <div class="row" style="width:100%">
                                                                <input type="text" class="form-control input-money-format" id="transfer-bulding-rights-received" name="received[{{$i_key}}]" value="{{(int)$item['net_value']}}" autocomplete="off" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak final yang telah dipungut
                                                        </label>
                                                        <div class="col-sm-4 exc6">
                                                            <div class="row">
                                                                <div class="col-sm-9" style="padding:0">
                                                                    <input type="text" class="form-control input-money-format" id="transfer-bulding-rights-final" name="final[{{$i_key}}]" value="{{(int)($item['gross_value'] - $item['net_value'])}}" autocomplete="off" readonly>
                                                                </div>
                                                                <div class="col-sm-2 pad">
                                                                    <label for="legacy-giver" class="col-form-label" style="text-align:right"><span style="font-size : 15px">Tarif</span></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <div class="row">
                                                                <input type="text" class="form-control" id="legacy-giver" name="pemberi[{{$i_key}}]" value="2,5%" autocomplete="off" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div id="tab1" class="tab active">
                                        <div class="row">
                                            <div class="col-md-12 col-md-offset-2">
                                                <input type="hidden" name="id[0]" id="id" value="0">
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="pajak-ditanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pajak ditanggung
                                                    </label>
                                                    <div class="col-sm-5"> 
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" class="custom-control-input" name="role[0]" value="penyewa">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Penyewa</span>
                                                        </label>
                                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                                <input type="radio" class="custom-control-input" name="role[0]" value="pemilik">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Pemilik</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group has-error row">
                                                    <label for="transfer-bulding-rights-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Transaksi
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" transaksi-id="1" id="transfer-bulding-rights-value" name="value[0]" value="" autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="transfer-bulding-rights-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Alamat / Kota
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="transfer-bulding-rights-address" name="address[0]" value="" autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai yang diterima
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="transfer-bulding-rights-received" name="received[0]" value="" autocomplete="off" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="legacy-giver" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-4 exc6">
                                                        <div class="row">
                                                            <div class="col-sm-9" style="padding:0">
                                                                <input type="text" class="form-control" id="transfer-bulding-rights-final" name="final[0]" value="" autocomplete="off" readonly>
                                                            </div>
                                                            <div class="col-sm-2 pad">
                                                                <label for="legacy-giver" class="col-form-label" style="text-align:right"><span style="font-size : 15px">Tarif</span></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <div class="row">
                                                            <input type="text" class="form-control" id="legacy-giver" name="pemberi[0]" value="2,5%" autocomplete="off"  readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                @endif  
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="deleted">
                    
                </div>
    
                @if (\Session::get('is_final_step'))
                    <hr class="col-md-12">
                    <div class="col-md-12 center" align="right">
                        <button type="submit" class="btn btn-info btn-lg">Save</button>
                    </div>
                @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                    {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                        <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                        <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                        {{--@endif</div>--}}
                    </div>
                </div>
                @endif
                
            </div>
        </form>
    </div>
    
    <div id="tab-transfer-building-rights-form-template" class="tab-transfer-building-rights-form" style="display: none">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                    </div>
                </div>
                <input type="hidden" name="id[0]" id="id" value="0">
                <div class="form-group row">
                    <label for="pajak-ditanggung" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Pajak ditanggung
                    </label>
                    <div class="col-sm-5"> 
                        <label class="custom-control custom-radio" style="margin-top: 5px">
                                <input type="radio" class="custom-control-input" name="role[0]" value="penyewa">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Penyewa</span>
                        </label>
                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                <input type="radio" class="custom-control-input" name="role[0]" value="pemilik">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Pemilik</span>
                        </label>
                    </div>
                </div>
                <div class="form-group has-error row">
                    <label for="transfer-bulding-rights-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai Transaksi
                    </label>
                    <div class="col-sm-5">
                        <div class="row" style="width:100%">
                            <input type="text" class="form-control" id="transfer-bulding-rights-value" name="value[0]" value="" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="transfer-bulding-rights-address" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Alamat / Kota
                    </label>
                    <div class="col-sm-5">
                        <div class="row" style="width:100%">
                            <input type="text" class="form-control" id="transfer-bulding-rights-address" name="address[0]" value="" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="transfer-bulding-rights-accepted" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Nilai yang diterima
                    </label>
                    <div class="col-sm-5">
                        <div class="row" style="width:100%">
                            <input type="text" class="form-control" id="transfer-bulding-rights-received" name="received[0]" value="" autocomplete="off" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="transfer-bulding-rights-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Pajak final yang telah dipungut
                    </label>
                    <div class="col-sm-4 exc6">
                        <div class="row">
                            <div class="col-sm-9" style="padding:0">
                                <input type="text" class="form-control" id="transfer-bulding-rights-final" name="final[0]" value="" autocomplete="off" readonly>
                            </div>
                            <div class="col-sm-2 pad">
                                <label for="legacy-giver" class="col-form-label" style="text-align:right;"><span style="font-size : 15px;">Tarif</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div>
                            <input type="text" class="form-control" id="legacy-giver" name="pemberi[0]" value="2,5%" autocomplete="off" style="text-align:center;" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>                      
    </div>