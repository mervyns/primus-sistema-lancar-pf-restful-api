<div class="boxed-body w-100 text-left">
    <form id="credit-card" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <!-- <br> -->

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ count($form_data['credit_card']) == 0 ? '1' : count($form_data['credit_card']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br>
                
                <div id="credit-card-form-container">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['credit_card']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['credit_card'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['credit_card']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['credit_card']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row credit-card-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="id" name="liability-id[]" value="">
                                                <input type="hidden" name="select_value[1]" value="">
                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Negara
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Mata Uang
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                                                            @foreach($form_data['country'] as $country)
                                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Pemakaian Per Akhir Tahun
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format balance" id="account-balance" name="account-balance[]" value="" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Bank
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm bank-name" id="bank-name-1" name="bank-name[]" style="width: 100%;" required select-id='1'>
                                                            @foreach($form_data['bank_list'] as $list)
                                                                <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                                                            @endforeach
                                                            <option value="-1">Bank Lainnya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="other-bank" name="other-bank[]" style="display:none;"
                                                                value="" placeholder="Nama Lainnya">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="credit-card-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Jenis Kartu Kredit
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm" id="credit-card-name" name="credit-card-name[]" style="width: 100%;" required>
                                                            <option value="Visa">Visa</option>
                                                            <option value="Mastercard">Mastercard</option>
                                                            <option value="American Express">American Express</option>
                                                            <option value="JCB">JCB</option>
                                                            <option value="Union Pay">Union Pay</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" class="form-control fom-control-sm" id="credit-card-type" name="credit-card-type[]" value="" placeholder="Ex: Gold Black">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Kurs Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Jumlah Rupiah
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['credit_card'] as $c_key => $card)
                                        @if (isset($card['active']))
                                            @if ($card['active'])
                                                <div id="tab{{$c_key + 1}}" class="tab active">
                                            @else
                                                <div id="tab{{$c_key + 1}}" class="tab">
                                            @endif
                                        @else
                                            <div id="tab{{ $c_key + 1 }}" class="tab {{ (($c_key +1) == 1) ? 'active' : '' }}">
                                        @endif
                                            <div class="row credit-card-form">
                                                <div class="col-md-12 col-md-offset-2">
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="id" name="liability-id[]" value="{{ isset($card['id']) ? $card['id'] : '' }}">
                                                    <input type="hidden" name="select_value[{{$loop->iteration}}]" value="{{$card['bank_name']}}">
                                                    <div class="form-group row">
                                                        <label for="country" class="text-right col-sm-5 col-form-label col-form-label-sm required">
                                                            Negara
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ $card['country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}" {{ $card['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Pemakaian Per Akhir Tahun
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format balance" 
                                                                id="account-balance" name="account-balance[]" 
                                                                value="{{ isset($card['currency_value']) ? $card['currency_value'] : '' }}" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nama Bank
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm bank-name" id="bank-name-{{ $loop->iteration }}" name="bank-name[]" style="width: 100%;" required select-id='{{$loop->iteration}}'>
                                                                @foreach($form_data['bank_list'] as $list)
                                                                    <option value="{{ $list['id'] }}" {{ ($card['bank_id'] == $list['id']) ? 'selected' : '' }}>{{ $list['name'] }}</option>
                                                                @endforeach
                                                                <option value="-1" {{ $card['bank_id'] == -1 ? 'selected' : '' }}>Bank Lainnya</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                    class="form-control form-control-sm"
                                                                    id="other-bank" name="other-bank[]" style="{{ ($card['bank_id'] == -1) ?: 'display:none' }}"
                                                                    value="{{ isset($card['other_bank']) ? $card['other_bank'] : '' }}" placeholder="Nama Lainnya">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="credit-card-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Jenis Kartu Kredit
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm" id="credit-card-name" name="credit-card-name[]" style="width: 100%;" required>
                                                                <option value="Visa" {{ ($card['credit_card_name'] == 'Visa') ? 'selected' : '' }}>Visa</option>
                                                                <option value="Mastercard" {{ ($card['credit_card_name'] == 'Mastercard') ? 'selected' : '' }}>Mastercard</option>
                                                                <option value="American Express" {{ ($card['credit_card_name'] == 'American Express') ? 'selected' : '' }}>American Express</option>
                                                                <option value="JCB" {{ ($card['credit_card_name'] == 'JCB') ? 'selected' : '' }}>JCB</option>
                                                                <option value="Union Pay {{ ($card['credit_card_name'] == 'Union Pay') ? 'selected' : '' }}">Union Pay</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text" class="form-control fom-control-sm" 
                                                                id="credit-card-type" name="credit-card-type[]" 
                                                                value="{{ isset($card['credit_card_type']) ? $card['credit_card_type'] : '' }}" 
                                                                autocomplete="off" placeholder="Ex: Gold Black">
                                                        </div>
                                                    </div>

                                                        <div class="form-group row">
                                                            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Kurs Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="kurs_pajak" name="kurs_pajak[]" 
                                                                    value="{{ isset($card['kurs_value']) ? $card['kurs_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group row">
                                                            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Jumlah Rupiah
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                                    id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                                    value="{{ isset($card['value']) ? $card['value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <br> -->
                </div>

            <div id="deleted">
                
            </div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>

<div id="credit-card-form-template" class="row credit-card-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <!-- <br> -->

        <input type="hidden" id="id" name="liability-id[]" value="">
        

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Negara
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Mata Uang
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
                </select>
            </div>
            </div>
            <!-- <br> -->

        <div class="form-group row">
            <label for="account-balance" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Pemakaian Per Akhir Tahun
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format balance" id="account-balance" name="account-balance[]" value="" required>
            </div>
    </div>
        <div class="form-group row">
            <label for="bank-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nama Bank
            </label>
            <div class="col-sm-3">
                <select class="form-control form-control-sm" id="bank-name" name="bank-name[]" style="width: 100%;" required>
                    @foreach($form_data['bank_list'] as $list)
                        <option value="{{ $list['id'] }}">{{ $list['name'] }}</option>
                    @endforeach
                    <option value="-1">Bank Lainnya</option>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text"
                        class="form-control form-control-sm"
                        id="other-bank" name="other-bank[]" style="display:none;"
                        value="" placeholder="Nama Lainnya">
            </div>
        </div>
        <div class="form-group row">
            <label for="credit-card-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Jenis Kartu Kredit
            </label>
            <div class="col-sm-3">
                <select class="form-control form-control-sm" id="credit-card-name" name="credit-card-name[]" style="width: 100%;" required>
                    <option value="Visa">Visa</option>
                    <option value="Mastercard">Mastercard</option>
                    <option value="American Express">American Express</option>
                    <option value="JCB">JCB</option>
                    <option value="Union Pay">Union Pay</option>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control fom-control-sm" id="credit-card-type" name="credit-card-type[]" value="" placeholder="Ex: Gold Black">
            </div>
            </div>

            <div class="form-group row">
                <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Kurs Pajak
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Jumlah Rupiah
                </label>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                </div>
            </div>

    </div>
</div>
