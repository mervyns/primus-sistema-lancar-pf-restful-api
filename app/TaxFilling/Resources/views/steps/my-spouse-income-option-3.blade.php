<div class="boxed-body w-100 text-left">
    <form id="spouse-income-3" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links" id="tabs">
                            @if (empty($form_data))
                                <li id="li1" class="active"><a href="#tab1">#1</a></li>
                            @else
                                @foreach ($form_data as $row)
                                    <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                        <div class="tab-content" id="tab-content">
                            @if (empty($form_data))
                                <div id="tab1" class="tab active">
                                    <div class="row spouse-income-3-form">
                                        <div class="col-md-12 col-md-offset-2">

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                </div>
                                            </div>
                                            <br>

                                            <input type="hidden" name="id[]" id="id">
                                            <div class="form-group row">
                                                <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    Nomor Bukti Potong
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" value="" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/nomor_bukti_potong.PNG') }}"
                                                            data-title="Nomor Bukti Potong"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    NPWP Pemotong Pajak
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class=row>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" value="" minlength="20" maxlength="20" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/npwp_pemotong.PNG') }}"
                                                            data-title="NPWP Pemotong Pajak"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    Nama Pemotong Pajak
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" value="" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/nama_pemotong.PNG') }}"
                                                            data-title="Nama Pemotong Pajak"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="net-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    Penghasilan Netto
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm input-money-format" id="net-income" name="net-income[]" value="" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/penghasilan_netto.PNG') }}"
                                                            data-title="Penghasilan Netto"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="withholding-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    PPh Yang Telah Dipotong
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm input-money-format" id="withholding-value" name="withholding-value[]" value="" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/pph_yang_telah_dipotong.PNG') }}"
                                                            data-title="PPh yang Telah Dipotong"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                    Tanggal Bukti Potong
                                                </label>
                                                <div class="col-sm-4">
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" autocomplete="off" required>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/spouse-income-option-3/tanggal_bukti_potong.PNG') }}"
                                                            data-title="Tanggal Buktii Potong"
                                                            >SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @foreach ($form_data as $row)
                                    <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        <div class="row spouse-income-3-form">
                                            <div class="col-md-12 col-md-offset-2">
                                                
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>
                                                
                                                <input type="hidden" name="id[]" id="id" value="{{ (empty($row['user_income_id'])) ? '' : $row['user_income_id'] }}">
                                                <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nomor Bukti Potong
                                                    </label>
                                                    <div class="col-sm-4">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" value="{{ (empty($row['withholding_slip_number'])) ? '' : $row['withholding_slip_number'] }}" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/nomor_bukti_potong.PNG') }}"
                                                                data-title="Nomor Bukti Potong"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        NPWP Pemotong Pajak
                                                    </label>
                                                    <div class="col-sm-4">
                                                        <div class=row>
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" value="{{ (empty($row['withholder_npwp'])) ? '' : $row['withholder_npwp'] }}" minlength="20" maxlength="20" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/npwp_pemotong.PNG') }}"
                                                                data-title="NPWP Pemotong Pajak"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Nama Pemotong Pajak
                                                    </label>
                                                    <div class="col-sm-4">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" value="{{ (empty($row['withholder_name'])) ? '' : $row['withholder_name'] }}" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/nama_pemotong.PNG') }}"
                                                                data-title="Nama Pemotong Pajak"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="net-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Penghasilan Netto
                                                    </label>
                                                    <div class="col-sm-4">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm input-money-format" id="net-income" name="net-income[]" value="{{ (empty($row['net_value'])) ? '' : $row['net_value'] }}" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/penghasilan_netto.PNG') }}"
                                                                data-title="Penghasilan Netto"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholding-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        PPh Yang Telah Dipotong
                                                    </label>
                                                    <div class="col-sm-4">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm input-money-format" id="withholding-value" name="withholding-value[]" value="{{ (empty($row['value'])) ? '' : $row['value'] }}" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/pph_yang_telah_dipotong.PNG') }}"
                                                                data-title="PPh yang Telah Dipotong"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Tanggal Bukti Potong
                                                    </label>
                                                    <div class="col-sm-4">
                                                        
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" value="{{ (empty($row['withholding_slip_date'])) ? '' : date('d-m-Y', strtotime($row['withholding_slip_date'])) }}" autocomplete="off" required>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                                data-image="{{ url('/storages/ss-image/spouse-income-option-3/tanggal_bukti_potong.PNG') }}"
                                                                data-title="Tanggal Buktii Potong"
                                                                >SS Image</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="deleted">
                
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>

<div id="spouse-income-3-form-template" class="row spouse-income-3-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" name="id[]" id="id">
        <div class="form-group row">
            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nomor Bukti Potong
            </label>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="withholding-slip-number" name="withholding-slip-number[]" value="" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/nomor_bukti_potong.PNG') }}"
                        data-title="Nomor Bukti Potong"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                NPWP Pemotong Pajak
            </label>
            <div class="col-sm-4">
                <div class=row>
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm input-npwp-format" id="withholder-npwp" name="withholder-npwp[]" value="" minlength="20" maxlength="20" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/npwp_pemotong.PNG') }}"
                        data-title="NPWP Pemotong Pajak"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Nama Pemotong Pajak
            </label>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="withholder-name" name="withholder-name[]" value="" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/nama_pemotong.PNG') }}"
                        data-title="Nama Pemotong Pajak"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="net-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Penghasilan Netto
            </label>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm input-money-format" id="net-income" name="net-income[]" value="" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/penghasilan_netto.PNG') }}"
                        data-title="Penghasilan Netto"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                PPh Yang Telah Dipotong
            </label>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm input-money-format" id="withholding-value" name="withholding-value[]" value="" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/pph_yang_telah_dipotong.PNG') }}"
                        data-title="PPh yang Telah Dipotong"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                Tanggal Bukti Potong
            </label>
            <div class="col-sm-4">
                
                <div class="row">
                    <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm input-picker-format" id="withholding-slip-date" name="withholding-slip-date[]" autocomplete="off" required>
                    </div>
                    <div class="col-sm-2">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/spouse-income-option-3/tanggal_bukti_potong.PNG') }}"
                        data-title="Tanggal Buktii Potong"
                        >SS Image</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>
