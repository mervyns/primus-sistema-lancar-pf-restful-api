<div class="boxed-body w-100 text-left">
    <form id="charity" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" style="margin-left: 10px" id="add-tab" data-ai="{{ empty($form_data['user_charities']) ? '1' : count($form_data['user_charities']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>

            <div id="charity-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(empty($form_data['user_charities']))
                                    <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_charities'] as $charity)
                                        <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(empty($form_data['user_charities']))
                                    <div id="tab1" class="tab active">
                                        <div class="row charity-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="charity-id[]" value="">
                                                <div class="form-group row">
                                                    <label for="institude-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Sumbangan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-money-format" id="charity-value" name="charity-value[]" value="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Institusi
                                                    </label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control form-control-sm charity-name" id="charity-name-1" name="charity-name[]" style="width: 100%;">
                                                            @foreach($form_data['charities'] as $row)
                                                                <option value="{{ $row['id'] }}">{{ $row['name'] }}</option>
                                                            @endforeach
                                                            <option value="-1">Institusi Lainnya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="other-charity" name="other-charity[]" style="display:none;"
                                                            value="" placeholder="Institusi Lainnya">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach ($form_data['user_charities'] as $charity)
                                        <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                            <div class="row charity-form">
                                                <div class="col-md-12 col-md-offset-2">


                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="charity-id[]" value="{{ isset($charity['id']) ? $charity['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="institude-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai Sumbangan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-money-format" 
                                                                id="charity-value" name="charity-value[]" 
                                                                value="{{ isset($charity['net_value']) ? $charity['net_value'] : '' }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Institusi
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm charity-name" id="charity-name-{{ $loop->iteration }}" name="charity-name[]" style="width: 100%;">
                                                                @foreach($form_data['charities'] as $row)
                                                                    <option value="{{ $row['id'] }}" {{ ($charity['charity_id'] == $row['id']) ? 'selected' : '' }}>{{ $row['name'] }}</option>
                                                                @endforeach
                                                                <option value="-1" {{ ($charity['charity_id'] == -1) ? 'selected' : '' }}>Institusi Lainnya</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="other-charity" name="other-charity[]" style="{{ ($charity['charity_id'] == -1) ?: 'display:none;' }}"
                                                                value="{{ isset($charity['other_charity']) ? $charity['other_charity'] : '' }}" placeholder="Institusi Lainnya">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
            </div>

            <div id="deleted">
                
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>

<div id="charity-form-template" class="row charity-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>
        <input type="hidden" id="id" name="charity-id[]" value="">
        <div class="form-group row">
            <label for="institude-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai Sumbangan
            </label>
            <div class="col-sm-5">
                <input type="text" class="form-control form-control-sm input-money-format" id="charity-value" name="charity-value[]" value="">
            </div>
        </div>
        <div class="form-group row">
            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nama Institusi
            </label>
            <div class="col-sm-3">
                <select class="form-control form-control-sm" id="charity-name" name="charity-name[]" style="width: 100%;">
                    @foreach($form_data['charities'] as $row)
                        <option value="{{ $row['id'] }}">{{ $row['name'] }}</option>
                    @endforeach
                    <option value="-1">Institusi Lainnya</option>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text"
                    class="form-control form-control-sm"
                    id="other-charity" name="other-charity[]" style="display:none;"
                    value="" placeholder="Institusi Lainnya">
            </div>
        </div>
        
    </div>
</div>
