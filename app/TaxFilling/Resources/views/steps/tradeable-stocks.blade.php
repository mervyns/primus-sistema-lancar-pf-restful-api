<div class="boxed-body w-100 text-left">
    <form id="tradeable-stocks" class="" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="tradeable-stocks-form-container">
                <div class="row">
                    @if(!empty($form_data['tradeable_stocks']))
                        @foreach($form_data['tradeable_stocks'] as $tradeableStocks)
                            <div class="col-md-12 col-md-offset-2 cloned-tradeable-stocks-data" id="clone-{{ $loop->iteration }}">
                                @if(!$loop->first)
                                    <hr class="col-md-10 offset-md-1">
                                @endif
                                <input type="hidden" name="tradeable-stocks-id[]" value="{{ $tradeableStocks['id'] }}">
                                <input type="hidden" name="deleted[]" value="">
                                <div class="form-group row">
                                    <label for="tradeable-stocks-acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Tahun Perolehan
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="number" class="form-control form-control-sm tradeable-stocks-acquisition-year" id="tradeable-stocks-acquisition-year-1" name="tradeable-stocks-acquisition-year[]" value="{{ isset($tradeableStocks['acquisition_year']) ? $tradeableStocks['acquisition_year'] : '' }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tradeable-stocks-country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Negara
                                    </label>
                                    <div class="col-sm-5">
                                        <select class="form-control form-control-sm" id="tradeable-stocks-country" name="tradeable-stocks-country[]" style="width: 100%;" {{ isset($form_data['tax_profile']['have_foreign_asset']) && $form_data['tax_profile']['have_foreign_asset'] == 1 ? '' : 'disabled' }} required>
                                            @foreach($form_data['country'] as $country)
                                                <option value="{{ $country['id'] }}" {{ isset($tradeableStocks['tradeable_stock_country']) && $tradeableStocks['tradeable_stock_country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tradeable-stocks-currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Mata Uang
                                    </label>
                                    <div class="col-sm-5">
                                        <select class="form-control form-control-sm tradeable-stocks-currency" id="tradeable-stocks-currency-{{ $loop->iteration }}" name="tradeable-stocks-currency[]" style="width: 100%;" required>
                                                @foreach($form_data['country'] as $country)
                                                    <option value="{{ $country['id'] }}" {{ isset($tradeableStocks['tradeable_stock_currency']) && $tradeableStocks['tradeable_stock_currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tradeable-stocks-foreign-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Nilai Holding dalam Valas per Akhir Tahun
                                    </label>
                                    <div class="col-sm-5">
                                         <input type="number" step="0.01" min = "1"
                                            class="form-control form-control-sm tradeable-stocks-foreign-value"
                                            id="tradeable-stocks-foreign-value-{{ $loop->iteration }}" name="tradeable-stocks-foreign-value[]" value="{{ isset($tradeableStocks['tradeable_stock_foreign_value']) ? $tradeableStocks['tradeable_stock_foreign_value'] : '' }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kurspajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Kurs Pajak
                                    </label>
                                    <div class="col-sm-5">
                                         <input type="text"
                                            class="form-control form-control-sm"
                                            id="kurspajak-{{ $loop->iteration }}" name="kurspajak[]" value="" disabled required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tradeable-stocks-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Nilai Holding dalam Rupiah per Akhir Tahun
                                    </label>
                                    <div class="col-sm-5">
                                        <input type="hidden"
                                            class="form-control form-control-sm"
                                            id="hidden-tradeable-stocks-value-{{ $loop->iteration }}" name="hidden-tradeable-stocks-value[]" value="">
                                         <input type="number" step="0.01" min = "1"
                                            class="form-control form-control-sm"
                                            id="tradeable-stocks-value-1" name="tradeable-stocks-value[]" value="{{ isset($tradeableStocks['idr_value']) ? $tradeableStocks['idr_value'] : '' }}" disabled required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="securities-companies-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Nama Perusahaan Sekuritas
                                    </label>
                                    <div class="col-sm-5">
                                         <input type="text"
                                            class="form-control form-control-sm"
                                            id="securities-companies" name="securities-companies[]" value="{{ isset($tradeableStocks['securities_companies']) ? $tradeableStocks['securities_companies'] : '' }}" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="trader-account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Nomor Rekening
                                    </label>
                                    <div class="col-sm-5">
                                         <input type="text"
                                            class="form-control form-control-sm"
                                            id="trader-account-number" name="trader-account-number[]" value="{{ isset($tradeableStocks['trader_account_number']) ? $tradeableStocks['trader_account_number'] : '' }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right remove-tradeable-stocks del" id="remove-tradeable-stocks-{{ $loop->iteration }}">
                                            Hapus
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                         <div class="col-md-12 col-md-offset-2 cloned-tradeable-stocks-data" id="clone-1">
                            <input type="hidden" name="tradeable-stocks-id[]" value="">
                            <input type="hidden" name="deleted[]" value="">
                            <div class="form-group row">
                                <label for="tradeable-stocks-acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Tahun Perolehan
                                </label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control form-control-sm tradeable-stocks-acquisition-year" id="tradeable-stocks-acquisition-year-1" name="tradeable-stocks-acquisition-year[]" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tradeable-stocks-country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Negara
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control form-control-sm" id="tradeable-stocks-country" name="tradeable-stocks-country[]" style="width: 100%;" {{ isset($form_data['tax_profile']['have_foreign_asset']) && $form_data['tax_profile']['have_foreign_asset'] == 1 ? '' : 'disabled' }} required>
                                        @foreach($form_data['country'] as $country)
                                            <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tradeable-stocks-currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Mata Uang
                                </label>
                                <div class="col-sm-5">
                                    <select class="form-control form-control-sm tradeable-stocks-currency" id="tradeable-stocks-currency-1" name="tradeable-stocks-currency[]" style="width: 100%;" required>
                                            @foreach($form_data['country'] as $country)
                                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tradeable-stocks-foreign-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Nilai Holding dalam Valas per Akhir Tahun
                                </label>
                                <div class="col-sm-5">
                                     <input type="number" step="0.01" min = "1"
                                        class="form-control form-control-sm tradeable-stocks-foreign-value"
                                        id="tradeable-stocks-foreign-value-1" name="tradeable-stocks-foreign-value[]" value="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="kurspajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Kurs Pajak
                                </label>
                                <div class="col-sm-5">
                                     <input type="text"
                                        class="form-control form-control-sm"
                                        id="kurspajak-1" name="kurspajak[]" value="" disabled required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tradeable-stocks-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Nilai Holding dalam Rupiah per Akhir Tahun
                                </label>
                                <div class="col-sm-5">
                                    <input type="hidden"
                                        class="form-control form-control-sm"
                                        id="hidden-tradeable-stocks-value-1" name="hidden-tradeable-stocks-value[]" value="">
                                     <input type="number" step="0.01" min = "1"
                                        class="form-control form-control-sm"
                                        id="tradeable-stocks-value-1" name="tradeable-stocks-value[]" value="" disabled required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="securities-companies-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Nama Perusahaan Sekuritas
                                </label>
                                <div class="col-sm-5">
                                     <input type="text"
                                        class="form-control form-control-sm"
                                        id="securities-companies" name="securities-companies[]" value="" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="trader-account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                    Nomor Rekening
                                </label>
                                <div class="col-sm-5">
                                     <input type="text"
                                        class="form-control form-control-sm"
                                        id="trader-account-number" name="trader-account-number[]" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right remove-tradeable-stocks del" id="remove-tradeable-stocks-1" style="display: none">
                                        Hapus
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info pull-right" style="margin-left: 10px" id="add-tradeable-stocks">
                        Tambah Data
                    </a>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
<div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>

<div class="col-md-12 col-md-offset-2 tradeable-stocks-data" style="display: none">
    <input type="hidden" name="tradeable-stocks-id[]" value="">
    <input type="hidden" name="deleted[]" value="">
    <div class="form-group row">
        <label for="tradeable-stocks-acquisition-year" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Tahun Perolehan
        </label>
        <div class="col-sm-5">
            <input type="number" class="form-control form-control-sm tradeable-stocks-acquisition-year" id="tradeable-stocks-acquisition-year" name="tradeable-stocks-acquisition-year[]" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="tradeable-stocks-country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Negara
        </label>
        <div class="col-sm-5">
            <select class="form-control form-control-sm" id="tradeable-stocks-country" name="tradeable-stocks-country[]" style="width: 100%;" {{ isset($form_data['tax_profile']['have_foreign_asset']) && $form_data['tax_profile']['have_foreign_asset'] == 1 ? '' : 'disabled' }} required>
                @foreach($form_data['country'] as $country)
                    <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="tradeable-stocks-currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Mata Uang
        </label>
        <div class="col-sm-5">
            <select class="form-control form-control-sm tradeable-stocks-currency" id="tradeable-stocks-currency" name="tradeable-stocks-currency[]" style="width: 100%;" required>
                    @foreach($form_data['country'] as $country)
                        <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                    @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="tradeable-stocks-foreign-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Nilai Holding dalam Valas per Akhir Tahun
        </label>
        <div class="col-sm-5">
             <input type="number" step="0.01" min = "1"
                class="form-control form-control-sm tradeable-stocks-foreign-value"
                id="tradeable-stocks-foreign-value" name="tradeable-stocks-foreign-value[]" value="" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="kurspajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Kurs Pajak
        </label>
        <div class="col-sm-5">
             <input type="text"
                class="form-control form-control-sm"
                id="kurspajak" name="kurspajak[]" value="" disabled required>
        </div>
    </div>
    <div class="form-group row">
        <label for="tradeable-stocks-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Nilai Holding dalam Rupiah per Akhir Tahun
        </label>
        <div class="col-sm-5">
            <input type="hidden"
                class="form-control form-control-sm"
                id="hidden-tradeable-stocks-value" name="hidden-tradeable-stocks-value[]" value="">
             <input type="number" step="0.01" min = "1"
                class="form-control form-control-sm"
                id="tradeable-stocks-value" name="tradeable-stocks-value[]" value="" disabled required>
        </div>
    </div>
    <div class="form-group row">
        <label for="securities-companies-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Nama Perusahaan Sekuritas
        </label>
        <div class="col-sm-5">
             <input type="text"
                class="form-control form-control-sm"
                id="securities-companies" name="securities-companies[]" value="" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="trader-account-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
            Nomor Rekening
        </label>
        <div class="col-sm-5">
             <input type="text"
                class="form-control form-control-sm"
                id="trader-account-number" name="trader-account-number[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-10">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right remove-tradeable-stocks del" id="remove-tradeable-stocks" style="display: none">
                Hapus
            </a>
        </div>
    </div>
</div>
