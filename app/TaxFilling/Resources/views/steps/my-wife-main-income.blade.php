<div class="boxed-body w-100 text-left">
    <form id="my-wife-main-income" class="" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    {{ csrf_field() }}
                    <input type="hidden" id="job_status_id" name="job_status_id" value="{{$form_data['job_status_id']}}">
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 2 ? 'none' : ''}} ">
                        <label for="wife-income-from-employer" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Penghasilan Netto dari Form 1721 A1
                        </label>
                        {{ old('wife-income-from-employer') }}
                        <div class="col-sm-5">
                            <input type="text"
                            	class="form-control form-control-sm"
                            	id="wife-income-from-employer" name="wife-income-from-employer" value="{{ !isset($form_data['net_value']) ? old('wife-income-from-employer') : $form_data['net_value'] }}" {{ $form_data['job_status_id'] != 2 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 2 ? 'none' : ''}}">
                        <label for="wife-income-tax-from-employer" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Pajak yang Telah Dipungut Pemberi Kerja
                        </label>
                        {{ old('wife-income-tax-from-employer') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="wife-income-tax-from-employer" name="wife-income-tax-from-employer" value="{{ !isset($form_data['net_value']) ? old('wife-income-from-employer') : $form_data['gross_value'] - $form_data['net_value'] }}" {{ $form_data['job_status_id'] != 2 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 4 ? 'none' : ''}}">
                        <label for="wife-other-income" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Total Penghasilan
                        </label>
                        {{ old('wife-other-income') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="wife-other-income" name="wife-other-income" value="{{ !isset($form_data['gross_value']) ? old('wife-other-income') : $form_data['gross_value'] }}" {{ $form_data['job_status_id'] != 4 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Nomor Bukti Potong
                        </label>
                        {{ old('withholding-slip-number') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="withholding-slip-number" name="withholding-slip-number" value="{{ !isset($form_data['withholding_slip_number']) ? old('withholding-slip-number') : $form_data['withholding_slip_number'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="withholder-npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            NPWP Pemotong Pajak
                        </label>
                        {{ old('withholder-npwp') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="withholder-npwp" name="withholder-npwp" value="{{ !isset($form_data['withholder_npwp']) ? old('withholder-npwp') : $form_data['withholder_npwp'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Nama Pemotong Pajak
                        </label>
                        {{ old('withholder-name') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="withholder-name" name="withholder-name" value="{{ !isset($form_data['withholder_name']) ? old('withholder-name') : $form_data['withholder_name'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Penghasilan Netto
                        </label>
                        {{ old('wife-net-income') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="wife-net-income" name="wife-net-income" value="{{ !isset($form_data['gross_value']) ? old('wife-net-income') : $form_data['gross_value'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            PPh Yang Telah Dipotong
                        </label>
                        {{ old('withholding-value') }}
                        <div class="col-sm-5">
                            <input type="text"
                                class="form-control form-control-sm"
                                id="withholding-value" name="withholding-value" value="{{ !isset($form_data['value']) ? old('withholding-value') : $form_data['value'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                    <div class="form-group row" style="display: {{ $form_data['job_status_id'] != 3 ? 'none' : ''}}">
                        <label for="npwp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                            Tanggal Bukti Potong
                        </label>
                        {{ old('withholding-value') }}
                        <div class="col-sm-5">
                            <input type="date"
                                class="form-control form-control-sm"
                                id="withholding-slip-date" name="withholding-slip-date" value="{{ !isset($form_data['withholding_slip_date']) ? old('withholding-slip-date') : $form_data['withholding_slip_date'] }}" {{ $form_data['job_status_id'] != 3 ? '' : 'required'}}>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
