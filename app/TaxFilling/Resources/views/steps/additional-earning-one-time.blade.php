<div class="boxed-body w-100 text-left">
    <form id="additional-earning-step" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="row" >
                        <span class="glyphicon glyphicon-envelope"></span>
                        <div class="col-md-4 col-xs-12" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Warisan / Hibah</button>
                                        <input name="71" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['warisan']) && $form_data['warisan'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>

                        <div class="col-md-4 col-xs-12" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal; white-space : normal;">Pesangon, Tunjangan Hari Tua dan Tebusan Pensiun yg dibayarkan sekaligus</button>
                                        <input name="74" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['pesangon_dan_tunjangan']) && $form_data['pesangon_dan_tunjangan'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right:5%"><i class="fa fa-info"></i></button>
                        </div>
                        <div class="col-md-4 col-xs-12" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Beasiswa</button>
                                        <input name="76" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['beasiswa']) && $form_data['beasiswa'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%"><i class="fa fa-info"></i></button>
                        </div>
                    </div> 
                    <div class="row" >
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Pengalihan Hak atas Tanah dan Bangunan</button>
                                        <input name="72" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['pengalihan_hak_tanah_bangunan']) && $form_data['pengalihan_hak_tanah_bangunan'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right:5%;"><i class="fa fa-info"></i></button>
                        </div>

                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Keuntungan dari Pengalihan Harta Selain Tanah dan Bangunan</button>
                                        <input name="73" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['keuntungan_pengalihan_selain_tanah_bangunan']) && $form_data['keuntungan_pengalihan_selain_tanah_bangunan'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Penjualan Saham Pendiri</button>
                                        <input name="77" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['penjualan_saham_pendiri']) && $form_data['penjualan_saham_pendiri'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right:5%;"><i class="fa fa-info"></i></button>
                        </div>
                    </div> 
                    <div class="row" >
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Hadiah Undian</button>
                                        <input name="75" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['hadiah_undian']) && $form_data['hadiah_undian'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>

                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Penghargaan dan Hadiah</button>
                                        <input name="81" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['penghargaan_dan_hadiah']) && $form_data['penghargaan_dan_hadiah'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Bantuan / Sumbangan</button>
                                        <input name="82" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['sumbangan']) && $form_data['sumbangan'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Honorarium atas Beban APBN/APBD</button>
                                        <input name="78" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['honorarium_apbn_apbd']) && $form_data['honorarium_apbn_apbd'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>

                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Upah pegawai tidak tetap atau tenaga kerja lepas</button>
                                        <input name="79" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['upah_pegawai_lepas']) && $form_data['upah_pegawai_lepas'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style="width : 90%; height : 120px; white-space : normal;">Honorarium komisaris yg tdk merangkap pegawai tetap</button>
                                        <input name="80" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['honorarium_komisaris']) && $form_data['honorarium_komisaris'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right:5%;"><i class="fa fa-info"></i></button>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-4 mrg1" style="text-align: center; margin-left: 16%;margin-bottom:2%">
                        <span class="button-checkbox">
                                <button type="button" class="btn btn-lg" data-color="primary" style=" width : 90%; height : 120px; white-space : normal;">Bangunan Yang Diterima dalam Rangka Bangun Guna Serah</button>
                                <input name="83" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['bangunan_guna_serah']) && $form_data['bangunan_guna_serah'] == true ? 'checked' : ''  }}/>
                        </span>
                        <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                        <div class="col-md-4" style="text-align: center;margin-bottom:2%">
                                <span class="button-checkbox">
                                        <button type="button" class="btn btn-lg" data-color="primary" style=" width : 90%; height : 120px; white-space : normal;">Penghasilan sekali terima lainya</button>
                                        <input name="84" type="checkbox" style="display: none" class="hidden" {{ isset($form_data['sekali_terima_lainnya']) && $form_data['sekali_terima_lainnya'] == true ? 'checked' : ''  }}/>
                                </span>
                                <button type="button" id="earning-info" class="btn bttn-info" style="position: absolute; bottom: 0; right: 0; margin-right: 5%;"><i class="fa fa-info"></i></button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12 center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                </div>
            </div>
        </div>
    </form>
</div>