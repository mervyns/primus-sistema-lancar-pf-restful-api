<div class="boxed-body w-700 text-left">
    <form id="profit-member" class="main-form" method="post" action="{{ Request::url() }}">
        {{ csrf_field() }}
        <div class="container">
            @if (empty($form_data['assets']))
                <input type="hidden" name="empty-signal" id="empty-signal" value="Mohon untuk mengisi data asset sebelum melanjutkan">
            @endif
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ (count($form_data['user_income']) == 0) ? '1' : count($form_data['user_income']) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}

            <div id="profit-member-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabs">
                            <ul class="tab-links" id="tabs">
                                @if(count($form_data['user_income']) == 0)
                                    <li class="active"><a href="#tab1">#1</a></li>
                                @else
                                    @foreach($form_data['user_income'] as $r_key => $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <li id="li{{$r_key + 1}}" class="active"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @else
                                                <li id="li{{$r_key + 1}}"><a href="#tab{{$r_key + 1}}">#{{$r_key + 1}}</a></li>
                                            @endif
                                        @else  
                                            @if(count($form_data['user_income']) == 0)
                                                <li class="active"><a href="#tab1">#1</a></li>
                                            @else
                                                <li id="li{{ $r_key + 1 }}" class="{{ (($r_key + 1) == 1) ? 'active' : '' }}"><a href="#tab{{ $r_key + 1 }}">#{{ $r_key + 1 }}</a></li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            <div class="tab-content" id="tab-content">
                                @if(count($form_data['user_income']) == 0)
                                    <div id="tab1" class="tab active">
                                        <div class="row profit-member-form">
                                            <div class="col-md-12 col-md-offset-2">

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                    </div>
                                                </div>
                                                <br>

                                                <input type="hidden" id="id" name="income_id[]" value="">
                                                <div class="form-group row">
                                                    <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm input-money-format"
                                                            id="net-value" name="net-value[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="perseroan-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Perseroan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <input type="text"
                                                            class="form-control form-control-sm"
                                                            id="perseroan-name" name="perseroan-name[]" value="" placeholder="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Dari Penyertaan
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <select class="form-control form-control-sm" id="user-asset" name="user-asset[]" style="width: 100%;" required>
                                                            @foreach ($form_data['assets'] as $asset)
                                                                <option value="{{ $asset->id }}" data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"> {{ $asset->company_name }} </option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['country_id'] : '' }}">
                                                        <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['asset_id'] : '' }}">
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    @foreach($form_data['user_income'] as $row)
                                        @if (isset($row['active']))
                                            @if ($row['active'])
                                                <div id="tab{{$r_key + 1}}" class="tab active">
                                            @else
                                                <div id="tab{{$r_key + 1}}" class="tab">
                                            @endif
                                        @else
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                        @endif
                                        {{-- <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}"> --}}
                                            <div class="row profit-member-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="income_id[]" value="{{ isset($row['id']) ? $row['id'] : '' }}">
                                                    <div class="form-group row">
                                                        <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format"
                                                                id="net-value" name="net-value[]" 
                                                                value="{{ isset($row['net_value']) ? $row['net_value'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="perseroan-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nama Perseroan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm"
                                                                id="perseroan-name" name="perseroan-name[]" 
                                                                value="{{ isset($row['perseroan_name']) ? $row['perseroan_name'] : '' }}" placeholder="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Dari Penyertaan
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="user-asset" name="user-asset[]" style="width: 100%;" required>
                                                                @foreach ($form_data['assets'] as $asset)
                                                                    <option value="{{ $asset->id }}" data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"
                                                                    {{ ($row['user_asset_id'] == $asset->id) ? 'selected' : '' }}> {{ $asset->company_name }} </option>
                                                                @endforeach
                                                            </select>
                                                            <input type="hidden" name="country_id[]"  id="country_id" 
                                                                value="{{ isset($row['country_id']) ? $row['country_id'] : '' }}">
                                                            <input type="hidden" name="asset_id[]" id="asset_id" 
                                                                value="{{ isset($row['asset_id']) ? $row['asset_id'] : '' }}">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="deleted"></div>
            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12" align="right">
                        <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg">Back</a>
                        <button type="submit" class="btn btn-info btn-lg" value="save" name="submit">Save &amp; Continue Later</button>
                        <button type="submit" class="btn btn-success btn-lg" value="next" name="submit">Next</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
</div>


<div id="profit-member-form-template" class="row profit-member-form" style="display: none">
    <div class="col-md-12 col-md-offset-2">
        
        <div class="form-group row">
            <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
            </div>
        </div>
        <br>

        <input type="hidden" id="id" name="income_id[]" value="">
        <div class="form-group row">
            <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nilai
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm input-money-format"
                    id="net-value" name="net-value[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="perseroan-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Nama Perseroan
            </label>
            <div class="col-sm-5">
                <input type="text"
                    class="form-control form-control-sm"
                    id="perseroan-name" name="perseroan-name[]" value="" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                Dari Penyertaan
            </label>
            <div class="col-sm-5">
                <select class="form-control form-control-sm" id="user-asset" name="user-asset[]" style="width: 100%;" required>
                    @foreach ($form_data['assets'] as $asset)
                        <option value="{{ $asset->id }}" data-country="{{ $asset->country_id }}" data-asset="{{ $asset->asset_id }}"> {{ $asset->company_name }} </option>
                    @endforeach
                </select>
                <input type="hidden" name="country_id[]"  id="country_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['country_id'] : '' }}">
                <input type="hidden" name="asset_id[]" id="asset_id" value="{{ count($form_data['assets']) > 0 ? $form_data['assets'][0]['asset_id'] : '' }}">
            </div>
        </div>
        
    </div>
</div>