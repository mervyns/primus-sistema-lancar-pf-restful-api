<div class="boxed-body w-100 text-left">
        <form id="overseas-income" class="main-form" method="post" action="{{ Request::url() }}">
            {{ csrf_field() }}
            <div class="container">
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <p><strong>Ada kesalahan:</strong></p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{!! $error !!}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <br>

                <div class="row">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab" data-ai="{{ empty($form_data['user_income']) ? '1' : count($form_data['user_income']) }}">
                            Tambah Data
                        </a>
                    </div>
                </div>
                <br>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="tab-links" id="tabs">
                                    @if (empty($form_data['user_income']))
                                        <li id="li1" class="active"><a href="#tab1">#1</a></li>
                                    @else
                                        @foreach ($form_data['user_income'] as $row)
                                            <li id="li{{ $loop->iteration }}" class="{{ ($loop->iteration == 1) ? 'active' : '' }}"><a href="#tab{{ $loop->iteration }}">#{{ $loop->iteration }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                                <div class="tab-content" id="tab-content">
                                    @if (empty($form_data['user_income']))
                                        <div id="tab1" class="tab active">
                                            <div class="row overseas-income-form">
                                                <div class="col-md-12 col-md-offset-2">

                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <input type="hidden" id="id" name="income-id[]" value="">
                                                    <div class="form-group row">
                                                        <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Negara Pemotong Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required="">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}">{{ $country['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Mata Uang
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                                                                @foreach($form_data['country'] as $country)
                                                                    <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nama Pemotong Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm"
                                                                id="withholder-name" name="withholder-name[]"
                                                                value="" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nomor Bukti Pemotongan Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm" 
                                                        id="withholding-slip-number" name="withholding-slip-number[]" 
                                                        value="" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                        Tanggal Pemotongan Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                        <input type="text" class="form-control form-control-sm input-picker-format" 
                                                        id="withholding-slip-date" name="withholding-slip-date[]" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="income-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Jenis Penghasilan
                                                        </label>
                                                        <div class="col-sm-3">
                                                            <select class="form-control form-control-sm" id="income-type" name="income-type[]" style="width: 100%;" required="">
                                                                <option value="Pekerjaan">Pekerjaan</option>
                                                                <option value="Bunga/Royalti">Bunga/Royalti</option>
                                                                <option value="Dividen">Dividen</option>
                                                                <option value="Penghasilan atas Usaha">Penghasilan atas Usaha</option>
                                                                <option value="Lain-lain">Lain-lain</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                        <input type="text" class="form-control form-control-sm" 
                                                        id="other-income-type" name="other-income-type[]" 
                                                        style="display:none;" placeholder="Lain-lain">
                                                </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Nilai Penghasilan Bruto
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format"
                                                                id="gross-value" name="gross-value[]" value="" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="tara-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Pajak yang Dipungut di Luar Negeri
                                                        </label>
                                                        <div class="col-sm-5">
                                                        <input type="text"
                                                        class="form-control form-control-sm input-money-format"
                                                        id="tara-value" name="tara-value[]" value="" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm  required">
                                                            Nilai Penghasilan Netto
                                                        </label>
                                                        <div class="col-sm-5">
                                                        <input type="text"
                                                        class="form-control form-control-sm input-money-format net-value balance"
                                                        id="net-value" name="net-value[]" value="" placeholder="" required="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Kurs Pajak
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai dalam Rupiah
                                                        </label>
                                                        <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        @foreach ($form_data['user_income'] as $income)
                                            <div id="tab{{ $loop->iteration }}" class="tab {{ ($loop->iteration == 1) ? 'active' : '' }}">
                                                <div class="row luxury-goods-form">
                                                    <div class="col-md-12 col-md-offset-2">
                                                        <div class="form-group row">
                                                            <div class="col-sm-12">
                                                                <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{ $loop->iteration }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                        <br>

                                                        <input type="hidden" id="id" name="income-id[]" value="{{ isset($income['user_income_id']) ? $income['user_income_id'] : '' }}">
                                                        <div class="form-group row">
                                                            <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Negara Pemotong Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required="">
                                                                    @foreach($form_data['country'] as $country)
                                                                        <option value="{{ $country['id'] }}" {{ $income['country_id'] == $country['id'] ? 'selected' : '' }}>{{ $country['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Mata Uang
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                                                                    @foreach($form_data['country'] as $country)
                                                                        <option value="{{ $country['id'] }}" {{ $income['currency'] == $country['id'] ? 'selected' : '' }}>{{ $country['currency'] }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Nama Pemotong Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                            id="withholder-name" name="withholder-name[]" 
                                                            value="{{ isset($income['withholder_name']) ? $income['withholder_name'] : '' }}" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Nomor Bukti Pemotongan Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm" 
                                                            id="withholding-slip-number" name="withholding-slip-number[]" 
                                                            value="{{ isset($income['withholding_slip_number']) ? $income['withholding_slip_number'] : '' }}" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                            Tanggal Pemotongan Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-picker-format" 
                                                            id="withholding-slip-date" name="withholding-slip-date[]" required=""
                                                            value="{{ isset($income['withholding_slip_date']) ? date('d/m/Y', strtotime($income['withholding_slip_date'])) : '' }}">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="income-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Jenis Penghasilan
                                                            </label>
                                                            <div class="col-sm-3">
                                                            <select class="form-control form-control-sm" id="income-type" name="income-type[]" style="width: 100%;" required="">
                                                            <option value="Pekerjaan" {{ isset($income['income_type']) && $income['income_type'] == 'Pekerjaan' ? 'selected' : '' }}>Pekerjaan</option>
                                                            <option value="Bunga/Royalti" {{ isset($income['income_type']) && $income['income_type'] == 'Bunga/Royalti' ? 'selected' : '' }}>Bunga/Royalti</option>
                                                            <option value="Dividen" {{ isset($income['income_type']) && $income['income_type'] == 'Dividen' ? 'selected' : '' }}>Dividen</option>
                                                            <option value="Penghasilan atas Usaha" {{ isset($income['income_type']) && $income['income_type'] == 'Penghasilan atas Usaha' ? 'selected' : '' }}>Penghasilan atas Usaha</option>
                                                            <option value="Lain-lain" {{ isset($income['income_type']) && $income['income_type'] == 'Lain-lain' ? 'selected' : '' }}>Lain-lain</option>
                                                        </select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                        <input type="text" class="form-control form-control-sm" 
                                                        id="other-income-type" name="other-income-type[]" 
                                                        value="{{ isset($income['other_income_type']) ? $income['other_income_type'] : '' }}"
                                                        style="{{ isset($income['income_type']) && $income['income_type'] == 'Lain-lain' ? '' : 'display:none;' }}" placeholder="Lain-lain">
                                                    </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Nilai Penghasilan Bruto
                                                            </label>
                                                            <div class="col-sm-5">
                                                                <input type="text"
                                                                class="form-control form-control-sm input-money-format"
                                                                id="gross-value" name="gross-value[]" 
                                                                value="{{ isset($income['gross_value']) ? $income['gross_value'] : '' }}" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="tara-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Pajak yang Dipungut di Luar Negeri
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text"
                                                                class="form-control form-control-sm input-money-format"
                                                                id="tara-value" name="tara-value[]" 
                                                                value="{{ isset($income['value']) ? $income['value'] : '' }}" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                                                                Nilai Penghasilan Netto
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text"
                                                            class="form-control form-control-sm input-money-format net-value balance"
                                                            id="net-value" name="net-value[]" 
                                                            value="{{ isset($income['net_value']) ? $income['net_value'] : '' }}" placeholder="" required="">
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                                Kurs Pajak
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                        id="kurs_pajak" name="kurs_pajak[]" 
                                                        value="{{ isset($income['kurs_value']) ? $income['kurs_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                            Nilai dalam Rupiah
                                                            </label>
                                                            <div class="col-sm-5">
                                                            <input type="text" class="form-control form-control-sm input-kurs-format" 
                                                        id="jumlah_rupiah" name="jumlah_rupiah[]" 
                                                        value="{{ isset($income['currency_value']) ? $income['currency_value'] : '' }}" readonly="">
                                                            </div>
                                                        </div>
                                                        
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="deleted">

                </div>

                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="overseas-income-form-template" class="row overseas-income-form" style="display: none">
            <div class="col-md-12 col-md-offset-2">
                          
                <div class="form-group row">
                    <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab">Hapus</a>
                    </div>
                </div>
                <br>

                <input type="hidden" id="id" name="income-id[]" value="">
                <div class="form-group row">
                    <label for="country" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Negara Pemotong Pajak
                    </label>
                    <div class="col-sm-5">
                        <select class="form-control form-control-sm" id="country" name="country[]" style="width: 100%;" required="">
                            @foreach($form_data['country'] as $country)
                                <option value="{{ $country['id'] }}">{{ $country['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="currency" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Mata Uang
                    </label>
                    <div class="col-sm-5">
                        <select class="form-control form-control-sm" id="currency" name="currency[]" style="width: 100%;" required="">
                            @foreach($form_data['country'] as $country)
                                <option value="{{ $country['id'] }}">{{ $country['currency'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="withholder-name" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Nama Pemotong Pajak
                    </label>
                    <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm" 
                    id="withholder-name" name="withholder-name[]" 
                    value="" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="withholding-slip-number" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Nomor Bukti Pemotongan Pajak
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm" 
                            id="withholding-slip-number" name="withholding-slip-number[]" 
                            value="" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="withholding-slip-date" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                    Tanggal Pemotongan Pajak
                    </label>
                    <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm input-picker-format" 
                    id="withholding-slip-date" name="withholding-slip-date[]" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="income-type" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Jenis Penghasilan
                    </label>
                    <div class="col-sm-3">
                        <select class="form-control form-control-sm" id="income-type" name="income-type[]" style="width: 100%;" required="">
                        <option value="Pekerjaan">Pekerjaan</option>
                        <option value="Bunga/Royalti">Bunga/Royalti</option>
                        <option value="Dividen">Dividen</option>
                        <option value="Penghasilan atas Usaha">Penghasilan atas Usaha</option>
                        <option value="Lain-lain">Lain-lain</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control form-control-sm" 
                        id="other-income-type" name="other-income-type[]" 
                        style="display:none;" placeholder="Lain-lain">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="gross-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Nilai Penghasilan Bruto
                    </label>
                    <div class="col-sm-5">
                    <input type="text"
                    class="form-control form-control-sm input-money-format"
                    id="gross-value" name="gross-value[]" value="" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tara-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Pajak yang Dipungut di Luar Negeri
                    </label>
                    <div class="col-sm-5">
                    <input type="text"
                    class="form-control form-control-sm input-money-format"
                    id="tara-value" name="tara-value[]" value="" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="net-value" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm required">
                        Nilai Penghasilan Netto
                    </label>
                    <div class="col-sm-5">
                    <input type="text"
                    class="form-control form-control-sm input-money-format net-value balance"
                    id="net-value" name="net-value[]" value="" placeholder="" required="">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kurs_pajak" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                        Kurs Pajak
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm input-kurs-format" id="kurs_pajak" name="kurs_pajak[]" value="1" readonly="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jumlah_rupiah" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Nilai dalam Rupiah
                    </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control form-control-sm input-kurs-format" id="jumlah_rupiah" name="jumlah_rupiah[]" value="0" readonly="">
                    </div>
                </div>

            </div>
        </div>
