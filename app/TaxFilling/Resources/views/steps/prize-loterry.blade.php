<div class="boxed-body w-100 text-left">
    <form id="prize-lottery-form" class="main-form" method="post" action="{{ Request::url() }}">
        <div class="container">
            @if (!empty($form_data['is_final_steps_submited']))
                <input type="hidden" name="close-signal" id="close-signal" value="1">
            @endif
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <br>

            <div class="row">
                <div class="col-md-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-info" id="add-tab-prize-lottery" data-ai="{{ empty($form_data) ? '1' : count($form_data) }}">
                        Tambah Data
                    </a>
                </div>
            </div>
            <br><br>
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs">
                        <ul class="tab-links" id="tabs">
                            @if (!empty($form_data['data']))  
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <li id="li{{$i_key + 1}}" class="active"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @else  
                                        <li id="li{{$i_key + 1}}"><a href="#tab{{$i_key + 1}}">#{{$i_key + 1}}</a></li>
                                    @endif
                                    
                                @endforeach   
                            @else
                                <li id="li1"><a href="#tab1">#1</a></li>
                            @endif
                        </ul>
                        <div class="tab-content" id="tab-content">
                            @if (!empty($form_data['data']))  
                                @foreach ($form_data['data'] as  $i_key =>  $item)
                                    @if ($item['active'])
                                        <div id="tab{{$i_key + 1}}" class="tab active">
                                    @else
                                        <div id="tab{{$i_key + 1}}" class="tab">
                                    @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" name="id[{{$i_key + 1}}]" id="id" value="{{$item['id']}}">
                                                {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="{{$i_key + 1}}">Hapus</a>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="prize-lottery-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nilai Bruto
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row">
                                                            <div class="col-sm-9" style="padding:0">
                                                                <input type="text" class="form-control input-money-format" brutto-id='{{$i_key}}' id="prize-lottery-brutto" name="brutto[{{$i_key}}]" value="{{$item['gross_value']}}" autocomplete="off" style="" required>
                                                            </div>
                                                            <div class="col-sm-2 pad">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                                                            data-title="Nilai Bruto" id="btn-bruto">SS Image</a>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="prize-lottery-corp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Nama Pemotong Pajak
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="prize-lottery-name" name="name[{{$i_key}}]" value="{{$item['detail']['nama_pemotong']}}" autocomplete="off" style="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="prize-lottery-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                       Nilai Netto
                                                    </label>
                                                    <div class="col-sm-4 exc6">
                                                        <div class="row">
                                                            <div class="col-sm-9" style="padding:0">
                                                            <input type="text" class="form-control input-money-format" netto-id='{{$i_key}}' id="prize-lottery-netto" name="netto[{{$i_key}}]" value="{{$item['net_value']}}" autocomplete="off" style=""readonly>
                                                            </div>
                                                            <div class="col-sm-2 pad">
                                                            <label for="legacy-giver" class="col-form-label" style="text-align:right"><span style="font-size : 15px;">Tarif</span></label>
                                                            </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                    <div>
                                                                <input id="tarif" type="text" class="form-control" id="legacy-giver" name="tarif[0]" value="25%" autocomplete="off"  disabled>
                                                                </div>
                    </div>
            </div>
                                                <div class="form-group row">
                                                    <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                        Pajak final yang telah dipungut
                                                    </label>
                                                    <div class="col-sm-5">
                                                        <div class="row" style="width:100%">
                                                            <input type="text" class="form-control" id="prize-lottery-final" name="final[{{$i_key}}]" value="{{$item['gross_value'] - $item['net_value']}}" autocomplete="off" style=""readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div id="tab1" class="tab active">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="id[0]" id="id" value="0">
                                            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove-tab" data-ai="1">Hapus</a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prize-lottery-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nilai Bruto
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <div class="col-sm-9" style="padding:0">
                                                            <input type="text" class="form-control input-money-format"  brutto-id='0' id="prize-lottery-brutto" name="brutto[0]" value="" autocomplete="off" style="" required>
                                                        </div>
                                                        <div class="col-sm-2 pad">
                                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                                                            data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                                                            data-title="Nilai Bruto" id="btn-bruto">SS Image</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prize-lottery-corp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Nama Pemotong Pajak
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <input type="text" class="form-control" id="prize-lottery-name" name="name[0]" value="" autocomplete="off" style="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prize-lottery-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                   Nilai Netto
                                                </label>
                                                <div class="col-sm-4 exc6">
                                                    <div class="row">
                                                        <div class="col-sm-9" style="padding:0">
                                                        <input type="text" class="form-control input-money-format" id="prize-lottery-netto" name="netto[0]" value="" autocomplete="off" style=""readonly>
                                                        </div>
                                                        <div class="col-sm-2 pad">
                                                        <label for="legacy-giver" class="col-form-label" style="text-align:right;"><span style="font-size : 15px">Tarif</span></label>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                            <div>
                                                            <input id="tarif" type="text" class="form-control" id="legacy-giver" name="tarif[0]" value="25%" autocomplete="off" disabled>
                                                            </div>
                    </div>
            </div>
                                            <div class="form-group row">
                                                <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                    Pajak final yang telah dipungut
                                                </label>
                                                <div class="col-sm-5">
                                                    <div class="row" style="width:100%">
                                                        <input type="text" class="form-control input-money-format" id="prize-lottery-final" name="final[0]" value="" autocomplete="off" style=""readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="deleted">
                
            </div>

            @if (\Session::get('is_final_step'))
                <hr class="col-md-12">
                <div class="col-md-12 center" align="right">
                    <button type="submit" class="btn btn-info btn-lg">Save</button>
                </div>
            @else
                <div class="row">
                    <hr class="col-md-12">
                    <div class="col-md-12  center" align="right">
                {{--@if (!empty($options['tooltip']['content']))<div class="margin">--}}
                    <a href="{!! $steps[$current_step['previous']]['url'] !!}" class="btn btn-success btn-lg btn-success-nav">Back</a>
                    <button type="submit" class="btn btn-info btn-lg btn-info-nav" value="save" name="submit">Save &amp; Continue<br class="no"> Later</button>
                    <button type="submit" class="btn btn-success btn-lg btn-success-nav" value="next" name="submit">Next</button>
                    {{--@endif</div>--}}
                    </div>
                </div>
            </div>
            @endif
        </div>
    </form>
</div>

<div id="tab-prize-lottery-template" class="tab-transfer-building-rights-form" style="display: none">
    <div class="row">
        <div class="col-md-12">
            <input type="hidden" name="id[0]" id="id" value="0">
            {{-- <input type="hidden" name="age" value="{{$form_data['age']}}"> --}}
            <div class="form-group row">
                <div class="col-sm-12">
                    <a href="javascript:void(0)" class="btn btn-xs btn-danger pull-right" id="remove_tab">Hapus</a>
                </div>
            </div>
            <div class="form-group row">
                <label for="prize-lottery-brutto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Nilai Bruto
                </label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-sm-9" style="padding:0">
                            <input type="text" class="form-control" id="prize-lottery-brutto" name="brutto[0]" value="" autocomplete="off" required>
                        </div>
                        <div class="col-sm-2 pad">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm preview-image"
                        data-image="{{ url('/storages/ss-image/main-income/nilai_bruto.PNG') }}"
                        data-title="Nilai Bruto" id="btn-bruto">SS Image</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="prize-lottery-corp" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Nama Pemotong Pajak
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <input type="text" class="form-control" id="prize-lottery-name" name="name[0]" value="" autocomplete="off"required>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="prize-lottery-netto" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                   Nilai Netto
                </label>
                <div class="col-sm-4 exc6">
                    <div class="row">
                    <div class="col-sm-9" style="padding:0">
                        <input type="text" class="form-control" id="prize-lottery-netto" name="netto[0]" value="" autocomplete="off" readonly>
                        </div>
                        <div class="col-sm-2 pad">
                        <label for="legacy-giver" class="col-form-label" style="text-align:right"><span style="font-size : 15px;">Tarif</span></label>
                    </div>
                    </div>
                </div>
                <div class="col-sm-1">
                        <div>
                            <input id="tarif" type="text" class="form-control" id="legacy-giver" name="tarif[0]" value="25%" autocomplete="off" style="text-align:center;" disabled>
                            </div>
                    </div>
            </div>
            <div class="form-group row">
                <label for="prize-lottery-final" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                    Pajak final yang telah dipungut
                </label>
                <div class="col-sm-5">
                    <div class="row" style="width:100%">
                        <input type="text" class="form-control" id="prize-lottery-final" name="final[0]" value="" autocomplete="off" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>        
</div>
<div class="modal" id="modal_preview_image" tabindex="-7" role="dialog" aria-labelledby="modalYear_label" 
    aria-hidden="true" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
    <div class="modal-dialog modal-lg" role="document" style="">
        <div class="modal-content" style="background-color: rgb(0,0,0);background-color: rgba(0,0,0,0.9);">
            <div class="modal-header" style="border-bottom: 0px">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-close" aria-hidden="true"></i></button>
            </div>
            <div class="modal-body">
                <img class="modal-content" id="display-image">
                <br>
                <h6><div id="caption-preview-image" style="color:#eaeaea;">Penghasilan Luar Negeri</div><h6>
            </div>
        </div>
    </div>
</div>