<?php

$router->middleware('web')
	   ->prefix('tax-filling')
       ->group(function() use ($router) {
	       $router->get( '/{step?}', 'TaxFillingController@index' )->middleware('Casp');
		   $router->post( '/{step?}', 'TaxFillingController@stepAction' )->middleware('Casp');
		   $router->delete( '/{step?}', 'TaxFillingController@delete')->middleware('Casp');
		   $router->get( '/{step?}/download', 'TaxFillingController@download' )->middleware('Casp');
	   });

$router->middleware('web')
	   ->prefix('tax-filling-final-steps')
       ->group(function() use ($router) {
		   $router->get( '/{step?}', 'TaxFillingController@indexFinalSteps' );
		   $router->get( 'edit/{step?}/{income_id?}', 'TaxFillingController@indexFinalSteps' );
		   $router->post( 'edit/{step?}/{income_id?}', 'TaxFillingController@stepAction' );
		   $router->post( '/{step?}', 'TaxFillingController@stepAction' );
		   $router->delete( '/{step?}', 'TaxFillingController@delete');
		   $router->get( '/{step?}/download', 'TaxFillingController@download' );
	   });
	   
$router->prefix('tax-report')
	   ->group(function() use ($router){
			$router->get('/report-1770s', 'TaxFillingController@report17770s');
	   });

/**
 * End of file
 */