<?php
namespace WebApp\TaxFilling\Middleware;

use Closure;

use Api\System\Models\SystemStepConstant;
use Api\System\Models\SystemStepConfig;
use Api\TaxFilling\Repositories\UserTaxFillingStepRepository;

class CheckAndSkipPage
{

	public function handle($request, Closure $next)
	{
		$userProfile = $request->user();
		$taxProfile = $request->user()->taxProfile;
		$requestPath = $request->path();
		$currentStep = intval(substr($requestPath, strpos($request->path(), '/') + 1));

		$stepConstants = [];
		foreach (SystemStepConstant::all() as $constant) {
			$stepConstants[$constant->step] = $constant->name;
		}

		if ($currentStep > 2){
			$userTaxFillingStep = new UserTaxFillingStepRepository();
			$userTaxFilling = $userTaxFillingStep->getWhere('user_id', $userProfile->id);
			
			$userStep = empty($userTaxFilling[0]->temp_steps) ? [] : json_decode($userTaxFilling[0]->temp_steps, true);
			$lastStep = empty($userTaxFilling[0]->current_step) ? 1 : $userTaxFilling[0]->current_step;

			if (count($userStep) > 0){
				if (!in_array((int)$currentStep, $userStep)){
					return redirect("/tax-filling/{$lastStep}");
				}
			}else{
				return redirect("/tax-filling/");
			}
		}

		// $checkedStep = [
		// 	'MY_WIFE_TAX_PROFILE',
		// 	'MY_HUSBAND_TAX_PROFILE',
		// 	'MY_WIFE_MAIN_INCOME',
		// ];

		// $stepName = isset($stepConstants[$currentStep]) ? $stepConstants[$currentStep] : 'START_FILLING';
		// if (in_array($stepName, $checkedStep)) {
		// 	$maritalStatus = $taxProfile->marital_status;
				
		// 	if ($maritalStatus != 2) {
		// 		$nextStep = $currentStep + 1;
		// 		return redirect("/tax-filling/{$nextStep}");
		// 	}
		// }

		return $next($request);
	}
}