<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SPT TAHUNAN 1770 S</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

<style>
@import url('https://fonts.googleapis.com/css?family=Roboto');
    html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed, 
        figure, figcaption, footer, header, hgroup, 
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
            font-family: sans-serif;
    }

    body{
        color: #000 !important;
        font-size: 7pt;
    }

    .row {
        width: 100%;
    }

    .header-left {
        float: left;
        display: inline-block;
        font-weight: bold;
        width: 25.2%;
    }

    .header-mid {
        float: left;
        display: inline-block;
        font-weight: bold;
        width: 53%;
        border-right: 2px solid #000;
        border-left: 2px solid #000;
    }

    .header-right {
        float: left;
        display: inline-block;
        {{--  width: 20.2%;  --}}
    }

    table.table {
        width: 100%;
        font-size: 7pt;
        border-collapse: separate;
        border-spacing: 0;
    }

    table.table-box-spt {
        font-size: 8pt;
    }

    .profile {
        padding-bottom: 5px;
    }

    table.table-box-year th{
        font-size: 24pt;
        font-weight: bold;
        text-align: center;
        vertical-align: middle;
        border: 1px dotted black;
        padding: 2px 8px;
    }

    table.table-box-spt td {
        font-size: 9pt;
        font-weight: normal;
        width: 18px;
        height: 21px;
        text-align: center;
        vertical-align: middle;
        border: 1px dotted black;
    }

    .content {
        font-size: 7pt;
    }

    .content-category {
        float: left;
        display: inline-block;
        font-size: 3pt;
        width: 7%;
        border-right: 1px solid #000;
    }

    .content-form {
        float: left;
        display: inline-block;
        width: 92.5%;
    }

    .content-form-number {
        float: left;
        display: inline-block;
        width: 2.5%;
        text-align: center;
    }

    .content-form-name {
        float: left;
        display: inline-block;
        width: 71%;
        /*border-right: 1px solid #000;*/
    }

    .content-form-table {
        float: right;
        display: inline-block;
        /*width: 30%;*/
    }

    .content-form-name-full {
        float: left;
        display: inline-block;
    }

    .content-text-sm {
        font-size: 5pt;
    }

    .content-table-number {
        border: 1px solid #000;
        text-align: center;
        vertical-align: middle;
        width: 10%;
        height: 25px;
    }

    .content-table-balance {
        width:90%;
        text-align: right;
        vertical-align: middle;
        border-top: 1px dotted #000;
        border-bottom: 1px dotted #000;
    }

    .lampiran .row {
        
    }

    table.table-title th {
        padding: 10px 3px;
        font-weight: bold;
        text-align: left;
    }

    table.table-dotted {
        border-top: 1px solid #000;
        /* border-left: 1px solid #000; */
    }

    table.table-dotted thead th {
        font-weight: normal;
        padding: 7px 0px;
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }

    table.table-dotted tbody td {
        padding: 7px 2px;
        border-bottom: 1px dotted #000;
        border-right: 1px solid #000;
        {{--  height: 25px;  --}}
        vertical-align: middle;
    }

    tbody tr.first-row td {
        text-align: center;
        padding: 1px;
        border-bottom: 1px solid #000;
        {{--  height: 15px;  --}}
    }

    tbody tr.last-row td {
        border-bottom: 1px solid #000;
    }

    table.table-dotted tfoot td {
        font-weight: bold;
        text-align: center;
        padding: 7px 2px;
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }

    table.table-dotted thead tr.subheader th {
        padding: 0;
        vertical-align: middle;
    }

    tfoot tr.footer-remarks td {
        padding: 0;
        border-style: none;
        font-size: 5.5pt;
        font-weight: normal;
        text-align: left;
    }

    table.table-solid {
        border-top: 1px solid #000;
        border-left: 1px solid #000;
    }

    table.table-solid thead th {
        font-weight: normal;
        padding: 7px 0px;
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }

    table.table-solid thead th {
        font-weight: normal;
        padding: 7px 0px;
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }

    table.table-solid tbody td {
        padding: 7px 2px;
        border-bottom: 1px solid #000;
        border-right: 1px solid #000;
    }

    .col {
        float: left;
        display: inline-block;
    }

</style>
</head>
<body>

<htmlpageheader name="MyHeader1">
    <div style="float: left; font-size: 1pt; background-color: #000; width: 15.11px; height: 11.33px;">1</div>
    <div style="float: right; font-size: 1pt; background-color: #000; width: 18.89px; height: 11.33px;">1</div>
</htmlpageheader>
<sethtmlpageheader name="MyHeader1" value="on" page="O" show-this-page="1" />

<htmlpagefooter name="footerSheet1">
    <div style="float: left; font-size: 1pt; background-color: #000; width: 15.11px; height: 11.33px;">1</div>
    <div style="float: right; font-size: 1pt; background-color: #000; width: 18.89px; height: 11.33px;">1</div>
</htmlpagefooter>
<sethtmlpagefooter name="footerSheet1" value="on" page="1"/>

<div class="row" style="border-bottom: 2px solid #000;">
    <div class="header-left">
        {{--  <p style="font-size: 28pt; text-align: center; padding: 5px;">1770 S</p>
        <p style="font-size: 8pt; text-align: center;">KEMENTERIAN KEUANGAN RI DIREKTORAT JENDERAL PAJAK</p>  --}}
        <div style="float: left; display: inline-block; width: 13px;">
            <table rotate="-90" cellspacing="0" style="font-sie: 15pt; margin-right: 30px;">
                <tr><th>FORMULIR</th></tr>
            </table>
        </div>
        <div style="float: left; display: inline-block; text-align: center;">
            <p style="font-size: 28pt; text-align: center; padding: 5px;">1770 S</p>
            <P style="font-size: 8pt;"> KEMENTERIAN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK </p>
        </div>
    </div>
    <div class="header-mid">
        <p style="font-size: 14pt; text-align: center; padding: 3px 0px;">SPT TAHUNAN</p>
        <p style="font-size: 10pt; text-align: center; border-bottom: 2px solid #000;">PAJAK PENGHASILAN WAJIB PAJAK  ORANG PRIBADI</p>
        {{--  <p style="font-size: 7pt; padding: 1px 2px;">BAGI WAJIB PAJAK YANG MEMPUNYAI PENGHASILAN :</p>
        <p style="font-size: 6pt; font-weight: normal;">DARI SATU ATAU LEBIH PEMBERI KERJA;</p>
        <p style="font-size: 6pt; font-weight: normal;">DALAM NEGERI LAINNYA; DAN/ATAU</p>
        <p style="font-size: 6pt; font-weight: normal;">YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL.</p>  --}}
        <p style="font-size: 7pt; padding: 1px 2px;">BAGI WAJIB PAJAK YANG MEMPUNYAI PENGHASILAN :</p>
        <ul style="margin-left: 14px; font-weight: normal;">
            <li>DARI SATU ATAU LEBIH PEMBERI KERJA;</li>
            <li>DALAM NEGERI LAINNYA; DAN/ATAU</li>
            <li>YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL.</li>
        </ul>
    </div>
    <div class="header-right">
        <div style="float: left; display: inline-block; width: 20px;">
            <table rotate="-90" cellspacing="0" style="font-sie: 15pt; margin-right: 16px;">
                <tr><th>TAHUN PAJAK</th></tr>
            </table>
        </div>
        <div style="float: left; display: inline-block; text-align: center;">
            <br>
            <table cellspacing="-1" class="table-box-year">
                <tr>
                    <th>{{ $year[0] }}</th>
                    <th>{{ $year[1] }}</th>
                    <th>{{ $year[2] }}</th>
                    <th>{{ $year[3] }}</th>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td width="20px" style="border: 1px solid #000;">&nbsp;</td>
                    <td style="vertical-align: middle;">&nbsp; SPT PEMBETULAN KE - ...</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row" style="padding: 2px;">
    <table class="table">
        <tr>
            <td width="12%" style="font-size: 8pt; text-align: right; vertical-align: middle; font-weight: bold;">PERHATIAN</td>
            <td width="27%" style="font-size: 5.5pt; text-align: right; vertical-align: middle"><ul><li>SEBELUM MENGISI BACA DAHULU PENTUNJUK PENGISIAN</li></ul></td>
            <td width="27%" style="font-size: 5.5pt; text-align: right; vertical-align: middle;"><ul><li>ISI DENGAN HURUF CETAK /DIKETIK DENGAN TINTA HITAM</li></ul></td>
            <td width="12%" style="font-size: 5.5pt; text-align: right; vertical-align: middle"><ul><li>BERI TANDA "X" PADA &nbsp;</li></ul></td>
            <td width="2%" style="border: 1px solid #000;">&nbsp;</td>
            <td style="font-size: 5.5pt; text-align: left; vertical-align: middle">&nbsp; (KOTAK PILIHAN) YANG SESUAI</td>
        </tr>
    </table>
</div>

<div class="row" style="border: 2px solid #000; height: 100px;">
    <div style="float: left; display: inline-block; width: 40px;">
        <table rotate="-90" style="font-size:9pt; margin-right: 60px;"><tr><th>IDENTITAS</th></tr></table>
    </div>
    <div style="float: left; display: inline-block; border-left: 2px solid #000;">
        <div style="padding: 6px;">
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">NPWP</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    @for ($i = 0; $i < strlen($npwp); $i++)
                        @php $alpha = substr($npwp, $i, 1); @endphp
                        @if ($alpha == "." || $alpha == "-")
                            <td style="border-style: hidden;">&nbsp;</td>
                        @else
                            <td>{{ $alpha  }}</td>
                        @endif
                    @endfor
                </tr>
            </table>
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">NAMA WAJIB PAJAK</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    @for ($i = 0; $i < strlen($name); $i++)
                        <td>{{ substr($name, $i, 1) }}</td>
                    @endfor
                </tr>
            </table>
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">PEKERJAAN</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    @for ($i = 0; $i < strlen($klu_name); $i++)
                        <td>{{ substr($klu_name, $i, 1) }}</td>
                    @endfor
                    <td style="font-size: 8pt; border-style: hidden; text-align: right; width: 36px;">KLU</td>
                    <td style="font-size: 8pt; border-style: hidden; text-align: left;">&nbsp;:</td>
                    @for ($i = 0; $i < strlen($klu_number); $i++)
                        <td>{{ substr($klu_number, $i, 1) }}</td>
                    @endfor
                </tr>
            </table>
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">NO. TELEPON</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    @for ($i = 0; $i < 4; $i++)
                        <td></td>
                    @endfor
                    <td style="border-style: hidden;"></td>
                    @for ($i = 0; $i < 8; $i++)
                        <td></td>
                    @endfor
                    <td style="font-size: 8pt; border-style: hidden; text-align: right; width: 72px;">NO. FAKS</td>
                    <td style="font-size: 8pt; border-style: hidden; text-align: left;">&nbsp;:</td>
                    @for ($i = 0; $i < 4; $i++)
                        <td></td>
                    @endfor
                    <td style="border-style: hidden;">-</td>
                    @for ($i = 0; $i < 8; $i++)
                        <td></td>
                    @endfor
                </tr>
            </table>
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">STATUS KEWAJIBAN PERPAJAKAN SUAMI-ISTERI</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    <td style="border: 1px solid #000; font-size: 13pt;">{{ $responsibility == 1 ? 'X' : '' }}</td>
                    <td style="font-size: 8pt; width: 36px; text-align: left; border-style: hidden;">&nbsp;KK</td>
                    <td style="border: 1px solid #000; font-size: 13pt;">{{ $responsibility == 2 ? 'X' : '' }}</td>
                    <td style="font-size: 8pt; width: 36px; text-align: left; border-style: hidden;">&nbsp;HB</td>
                    <td style="border: 1px solid #000; font-size: 13pt;">{{ $responsibility == 3 ? 'X' : '' }}</td>
                    <td style="font-size: 8pt; width: 36px; text-align: left; border-style: hidden;">&nbsp;PH</td>
                    <td style="border: 1px solid #000; font-size: 13pt;">{{ $responsibility == 4 ? 'X' : '' }}</td>
                    <td style="font-size: 8pt; width: 36px; text-align: left; border-style: hidden;">&nbsp;MT</td>
                </tr>
            </table>
            <table cellspacing="-1" class="table-box-spt profile">
                <tr>
                    <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden;">NPWP ISTERI/SUAMI</td>
                    <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                    @for ($i = 0; $i < strlen($spouse_npwp); $i++)
                        @php $alpha = substr($spouse_npwp, $i, 1); @endphp
                        @if ($alpha == "." || $alpha == "-")
                            <td style="border-style: hidden;">&nbsp;</td>
                        @else
                            <td>{{ $alpha  }}</td>
                        @endif
                    @endfor
                </tr>
            </table>
        </div>
        <div style="font-size: 6pt; font-weight: bold; text-align: center; border-top: 2px solid #000; padding: 3px 0px;">
            Permohonan perubahan data disampaikan terpisah dari pelaporan SPT Tahunan PPh Orang Pribadi ini, dengan menggunakan Formulir <br> Perubahan Data Wajib Pajak dan dilengkapi dokumen yang disyaratkan.
        </div>
    </div>
</div>

<div class="row" style="padding-top: 2px; padding-bottom: 1px;">
    <table cellspacing="-1" class="table">
        <tr>
            <td width="75%" style="font-size:6pt; padding-left: 15px; border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000;"><strong>*) </strong> Pengisian kolom-kolom yang berisi nilai rupiah harus tanpa nilai desimal (contoh penulisan lihat petunjuk pengisian halaman 3)</td>
            <td width="25%" style="font-size: 6pt; text-align: center; border: 1px solid #000;"><strong>RUPIAH</strong></td>
        </tr>
    </table>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 171px; border-top: 1px solid #000;">
            <table rotate="-90" style="margin-right: 26px;">
                <tr><th>A. PENGHASILAN NETTO</th></tr>
            </table>
        </div>
        <div class="content-form">
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    1
                </div>
                <div class="content-form-name">
                    <p>PENGHASILAN NETO DALAM NEGERI SEHUBUNGAN DENGAN PEKERJAAN ..........................................................................</p>
                    <p class="content-text-sm">[Diisi akumulasi jumlah penghasilan neto pada setiap Formulir 1721-A1 dan/atau 1721-A2 angka 14 yang dilampirkan atau Bukti Potong Lain]</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">1</td>
                            <td class="content-table-balance">{{ $SA1 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    2
                </div>
                <div class="content-form-name">
                    <p>PENGHASILAN NETO DALAM NEGERI LAINNYA ....................................................................................................................</p>
                    <p class="content-text-sm">[Diisi sesuai dengan Formulir 1770 S-I Jumlah Bagian A ] </p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">2</td>
                            <td class="content-table-balance">{{ $SA2 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    3
                </div>
                <div class="content-form-name">
                    <p>PENGHASILAN NETO LUAR NEGERI .....................................................................................................................................</p>
                    <p class="content-text-sm">[Apabila memiliki penghasilan dari luar negeri agar diisi dari Lampiran Tersendiri, lihat petunjuk pengisian]</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">3</td>
                            <td class="content-table-balance">{{ $SA3 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    4
                </div>
                <div class="content-form-name">
                    <p>JUMLAH PENGHASILAN NETO (1+2+3) ...............................................................................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">4</td>
                            <td class="content-table-balance">{{ $SA4 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    5
                </div>
                <div class="content-form-name">
                    <p>ZAKAT/SUMBANGAN KEAGAMAAN YANG SIFATNYA WAJIB ..................................................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">5</td>
                            <td class="content-table-balance">{{ $SA5 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    6
                </div>
                <div class="content-form-name">
                    <p>JUMLAH PENGHASILAN NETO SETELAH PENGURANGAN ZAKAT /SUMBANGAN KEAGAMAAN...............................................<br>YANG SIFATNYA WAJIB (4-5)</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">6</td>
                            <td class="content-table-balance">{{ $SA6 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 65px;">
            <table rotate="-90" style="margin-right: 1px;">
                <tr><th>B. PENGHA<br>SILAN <br> KENA PAJAK</th></tr>
            </table>
        </div>
        <div class="content-form">

            <div style="padding-top: 5px;">
                <div class="content-form-number">
                    <p style="margin-top: 4px;">7</p>
                </div>
                <div class="content-form-name">
                    <table style="width: 100%" cellspacing="-1">
                        <tr>
                            <td width="30%" style="height: 20px; vertical-align: middle;">PENGHASILAN TIDAK KENA PAJAK</td>
                            <td width="6%" style="vertical-align: middle; text-align: right;">TK/&nbsp;</td>
                            <td width="4%" style="height: 20px; border: 1px dotted #000; text-align:center; vertical-align: middle;">{{ $SB7['ptkp_status'] == 'TK' ? $SB7['ptkp_total'] : '&nbsp;' }}</td>
                            <td width="6%" style="vertical-align: middle; text-align: right;">K/&nbsp;</td>
                            <td width="4%" style="height: 20px; border: 1px dotted #000; text-align:center; vertical-align: middle;">{{ $SB7['ptkp_status'] == 'K' ? $SB7['ptkp_total'] : '&nbsp;' }}</td>
                            <td width="6%" style="vertical-align: middle; text-align: right;">K/I/&nbsp;</td>
                            <td width="4%" style="height: 20px; border: 1px dotted #000; text-align:center; vertical-align: middle;">{{ $SB7['ptkp_status'] == 'K/I' ? $SB7['ptkp_total'] : '&nbsp;' }}</td>

                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">7</td>
                            <td class="content-table-balance">{{ number_format($SB7['ptkp_value'],0,',','.') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    8
                </div>
                <div class="content-form-name">
                    <p>PENGHASILAN KENA PAJAK (6 - 7) .......................................................................................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">8</td>
                            <td class="content-table-balance">{{ $SB8 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 93px;">
            <table rotate="-90" style="margin-right: 1px;">
                <tr><th>C. PPh TERUTANG</th></tr>
            </table>
        </div>
        <div class="content-form">
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    9
                </div>
                <div class="content-form-name">
                    <p>PPh TERUTANG (TARIF PASAL 17 UU PPh x ANGKA 8) .........................................................................................................</p>
                    <p style="font-size: 6pt;">[Bagi Wajib Pajak dengan status PH atau MT diisi dari Lampiran Perhitungan PPh Terutang sebagaimana dimaksud dalam bagian G: <br> Lampiran huruf d]</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number" style="height: 35px;">9</td>
                            <td class="content-table-balance">{{ $SC9 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    10
                </div>
                <div class="content-form-name">
                    <p>PENGEMBALIAN / PENGURANGAN PPh PASAL 24 YANG TELAH DIKREDITKAN ......................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">10</td>
                            <td class="content-table-balance">{{ $SC10 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    11
                </div>
                <div class="content-form-name">
                    <p>JUMLAH PPh TERUTANG (9+10) ............................................................................................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">11</td>
                            <td class="content-table-balance">{{ $SC11 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 158px;">
            <table rotate="-90" style="margin-right: 32px;">
                <tr><th>D. KREDIT PAJAK</th></tr>
            </table>
        </div>
        <div class="content-form">
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    12
                </div>
                <div class="content-form-name">
                    <p>PPh YANG DIPOTONG/DIPUNGUT PIHAK LAIN/DITANGGUNG PEMERINTAH DAN/ATAU KREDIT PAJAK LUAR<br>NEGERI DAN/ATAU TERUTANG DI LUAR NEGERI <span style="font-size: 6pt;">[Diisi dari Formulir 1770 S-I Jumlah Bagian C Kolom (7)]</span></p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number" style="height: 35px;">12</td>
                            <td class="content-table-balance">{{ $SD12 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    <p style="margin-top: 3px;">13</p>
                </div>
                <div class="content-form-name">
                    <table style="width: 100%;" cellspacing=2>
                        <tr>
                            <td width="3%">a</td>
                            <td width="4%" style="border: 1px solid #000; text-align: center; vertical-align: middle; font-weight: bold;">X</td>
                            <td width="3%">&nbsp;</td>
                            <td width="35%" style="border-bottom: 1px solid #000; padding-left: 3px;">PPh YANG HARUS DIBAYAR SENDIRI</td>
                            <td rowspan="2" style="vertical-align: middle;"> &nbsp; (11-12)</td>
                        </tr>
                        <tr>
                            <td width="3%">b</td>
                            <td width="4%" style="border: 1px solid #000">&nbsp;</td>
                            <td width="2%">&nbsp;</td>
                            <td width="35%" style="padding-left: 3px;">PPh YANG LEBIH DIPOTONG/DIPUNGUT</td>

                        </tr>
                    </table>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number" style="height: 35px;">13</td>
                            <td class="content-table-balance">{{ $SD13 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    <p>14</p>
                </div>
                <div class="content-form-name">
                    <table style="width: 100%;" cellspacing=2>
                        <tr>
                            <td width="30%">PPh YANG DIBAYAR SENDIRI</td>
                            <td>a. PPh PASAL 25 ........................................................................................................</td>
                        </tr>
                    </table>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">14a</td>
                            <td class="content-table-balance">{{ number_format($SD14['a'],0,',','.') }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    &nbsp;
                </div>
                <div class="content-form-name">
                    <table style="width: 100%;" cellspacing=2>
                        <tr>
                            <td width="30%"></td>
                            <td>b. STP PPh Pasal 25 (Hanya Pokok Pajak) ..................................................................</td>
                        </tr>
                    </table>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">14b</td>
                            <td class="content-table-balance">{{ $SD14['b'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    15
                </div>
                <div class="content-form-name">
                    <p>JUMLAH KREDIT PAJAK (14a + 14b) ......................................................................................................................................</p>
                    <p class="content-text-sm">&nbsp;</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">15</td>
                            <td class="content-table-balance">{{ $SD15 }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 107px;">
            <table rotate="-90">
                <tr><th>E. PPh KURANG/<br>LEBIH BAYAR</th></tr>
            </table>
        </div>
        <div class="content-form">
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    <p>16</p>
                </div>
                <div class="content-form-name">
                    <div style="float:left; display:inline-block; width: 55%;">
                        <table style="width: 100%" cellspacing="2">
                            <tr>
                                <td width="7%" style="border: 1px solid #000;">{{ $SE16['a'] == true ? 'X' : '&nbsp;' }}</td>
                                <td width="3%"></td>
                                <td width="75%" style="border-bottom: 1px solid #000; padding-left: 4px;">a. PPh YANG KURANG DIBAYAR (PPh PASAL 29)</td>
                                <td rowspan="2" style="text-align: right; vertical-align: middle;">(13-15)</td>
                            </tr>
                            <tr>
                                <td width="7%" style="border: 1px solid #000;">{{ $SE16['b'] == true ? 'X' : '&nbsp;' }}</td>
                                <td width="3%"></td>
                                <td width="75%" style="padding-left: 4px;">b. PPh YANG LEBIH DIBAYAR (PPh PASAL 28 A)</td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; display:inline-block; width: 44%;">
                        <table style="border-collapse: separate; width: 100%;" cellspacing="-1">
                            <tr>
                                <td width="50"></td>
                                <td colspan="9" style="text-align: center; vertical-align: middle;">TGL LUNAS</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][4] }}</td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][5] }}</td>
                                <td width="15">&nbsp;</td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][2] }}</td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][3] }}</td>
                                <td width="15" >&nbsp;</td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][0] }}</td>
                                <td width="15" style="border: 1px dotted #000; height: 18px; text-align: center; vertical-align: middle;">{{ $SE16['date'][1] }}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="15" colspan="2" style="text-align: center; vertical-align: middle; padding-top: 4px; font-size: 5.5pt;">TGL</td>
                                <td width="15">&nbsp;</td>
                                <td width="15" colspan="2" style="text-align: center; vertical-align: middle; padding-top: 4px; font-size: 5.5pt;">BLN</td>
                                <td width="15">&nbsp;</td>
                                <td width="15" colspan="2" style="text-align: center; vertical-align: middle; padding-top: 4px; font-size: 5.5pt;">THN</td>
                                <td></td>

                            </tr>
                        </table>
                    </div>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number" style="height: 35px;">16</td>
                            <td class="content-table-balance">{{ $SE16['value'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 5px;">
                <div class="content-form-number">
                    17
                </div>
                <div class="content-form-name-full">
                    <p>PERMOHONAN : PPh Lebih Bayar pada 16b mohon :</p>
                    <table style="border-collapse: separate; width: 100%; margin-top: 2px;" cellspacing="2">
                        <tr>
                            <td width="2%">a.</td>
                            <td width="2.7%" style="border: 1px solid #000;">{{ $SE17['a'] == true ? 'X' : '&nbsp;'}}</td>
                            <td width="20%">DIRESTITUSIKAN</td>


                            <td width="2%">c.</td>
                            <td width="2.7%" style="border: 1px solid #000;">{{ $SE17['c'] == true ? 'X' : '&nbsp;'}}</td>
                            <td>DIKEMBALIKAN DENGAN SKPPKP PASAL 17C (WP dengan Kriteria Tertentu)</td>

                        </tr>
                        <tr>
                            <td width="2%">b.</td>
                            <td width="2.7%" style="border: 1px solid #000;">&nbsp;</td>
                            <td width="20%">DIPERHITUNGKAN DENGAN</td>


                            <td width="2%">d.</td>
                            <td width="2.7%" style="border: 1px solid #000;">{{ $SE17['d'] == true ? 'X' : '&nbsp;'}}</td>
                            <td>DIKEMBALIKAN DENGAN SKKPP PASAL 17D (WP yang Memenuhi Persyaratan Tertentu)</td>

                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td>UTANG PAJAK</td>
                            <td colspan="3">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 80px;">
            <table rotate="-90">
                <tr><th>F. ANGSURAN<br>PPh PASAL 25<br>TAHUN PAJAK<br>BERIKUTNYA</th></tr>
            </table>
        </div>
        <div class="content-form">
            <div style="padding-top: 2px;">
                <div class="content-form-number">
                    18
                </div>
                <div class="content-form-name">
                    <p>ANGSURAN PPh PASAL 25 TAHUN PAJAK BERIKUTNYA SEBESAR .........................................................................................<br>DIHITUNG BERDASARKAN :</p>
                </div>
                <div class="content-form-table">
                    <table class="table">
                        <tr>
                            <td class="content-table-number">18</td>
                            <td class="content-table-balance">{{ $SF18['value'] }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="padding-top: 5px;">
                <div class="content-form-number">
                    &nbsp;
                </div>
                <div class="content-form-name-full">
                    <table style="border-collapse: separate; width: 100%; margin-top: 2px;" cellspacing="2">
                        <tr>
                            <td width="2%">a.</td>
                            <td width="2.7%" style="border: 1px solid #000;">{{ $SF18['a'] == true ? 'X' : '&nbsp;' }}</td>
                            <td>1/12 x JUMLAH PADA ANGKA 13 </td>
                        </tr>
                        <tr>
                            <td width="2%">b.</td>
                            <td width="2.7%" style="border: 1px solid #000;">{{ $SF18['b'] == true ? 'X' : '&nbsp;' }}</td>
                            <td>PENGHITUNGAN DALAM LAMPIRAN TERSENDIRI</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
    <div class="container">
        <div class="content-category" style="height: 75px;">
            <table rotate="-90">
                <tr><th>G. LAMPIRAN</th></tr>
            </table>
        </div>
            <div style="padding-top: 5px;">
                <div class="content-form-name-full">
                    <table style="border-collapse: separate; width: 100%; margin-top: 2px;" cellspacing="2">
                    <tr>
                        <td width="2%" style="height: 20px;">a</td>
                        <td width="2.7%" style="border: 1px solid #000">{{ $SG19['a'] == true ? 'X' : '&nbsp;' }}</td>
                        <td width="44%" style="vertical-align:middle;">&nbsp; Fotokopi Formulir 1721-A1 atau 1721-A2 atau Bukti Potong PPh Pasal 21</td>

                        <td width="2%">d</td>
                        <td width="2.7%" style="border: 1px solid #000">{{ $SG19['d'] == true ? 'X' : '&nbsp;' }}</td>
                        <td style="vertical-align:middle;">&nbsp; Perhitungan PPh Terutang bagi Wajib Pajak dengan status perpajakan PH/MT</td>
                    </tr>
                    <tr>
                        <td width="2%" style="height: 20px;">b</td>
                        <td width="2.7%" style="border: 1px solid #000">{{ $SG19['b'] == true ? 'X' : '&nbsp;' }}</td>
                        <td width="44%" style="vertical-align:middle;">&nbsp; Surat Setoran Pajak Lembar Ke-3 PPh Pasal 29</td>

                        <td width="2%">e</td>
                        <td width="2.7%" style="border: 1px solid #000">{{ $SG19['e'] == true ? 'X' : '&nbsp;' }}</td>
                        <td style="vertical-align:middle;">&nbsp; ..............................................</td>
                    </tr>
                    <tr>
                        <td width="2%" style="height: 20px;">c</td>
                        <td width="2.7%" style="border: 1px solid #000">{{ $SG19['c'] == true ? 'X' : '&nbsp;' }}</td>
                        <td width="44%" style="vertical-align:middle;">&nbsp; Surat Kuasa Khusus (Bila dikuasakan)</td>
                    </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="background-color: #a6a6a6; font-weight: bold; text-align: center;">
    <span style="padding: 20px;">PERNYATAAN</span>
</div>

<div class="row" style="border: 1px solid #000;">
    <div style="float: left; display: inline-block; width: 69.5%;">
        <p style="font-weight: bold; font-size: 6pt;">Dengan menyadari sepenuhnya akan segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan peraturan perundang-undangan yang berlaku, saya menyatakan bahwa yang telah beritahukan diatas beserta lampiran-lampirannya adalah benar, lengkap dan jelas.</p>
        <table cellspacing="-1" class="table-box-spt profile" style="margin-left: 2px; margin-top: 5px;">
            <tr id="">
                <td style="border-style: hidden; width: 20px; text-align: left; font-size: 10px;"></td>
                <td style="border-style: solid; font-size: 14px;">X</td>
                <td style="border-style: hidden; width: 100px; text-align: left; font-size: 10px;">&nbsp;WAJIB PAJAK</td>
                <td style="border-style: solid; font-size: 10px;"></td>
                <td style="border-style: hidden; width: 100px; text-align: left; font-size: 10px;">&nbsp;KUASA</td>
                
            </tr>
        </table>
        <table cellspacing="-1" class="table-box-spt profile" style="margin-left: 2px;">
            <tr>
                <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 100px; border-style: hidden;">NAMA LENGKAP</td>
                <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                @for ($i = 0; $i < strlen($name) - 11; $i++)
                    <td>{{ substr($name, $i, 1) }}</td>
                @endfor
            </tr>
        </table>
        <table cellspacing="-1" class="table-box-spt profile" style="margin-left: 2px;">
            <tr>
                <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 100px; border-style: hidden;">NPWP</td>
                <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                @for ($i = 0; $i < strlen($npwp); $i++)
                    @php $alpha = substr($npwp, $i, 1); @endphp
                    @if ($alpha == "." || $alpha == "-")
                        <td style="border-style: hidden;">&nbsp;</td>
                    @else
                        <td>{{ $alpha  }}</td>
                    @endif
                @endfor
            </tr>
            </table>
    </div>
    <div style="float: left; display: inline-block; width: 30%; text-align: center;">
        <p style="font-weight: bold; padding: 5px;">TANDA TANGAN</p>
        <div style="border: 1px solid #000; height: 85px; margin: 3px; width: 100%;">
        &nbsp;
        </div>
    </div>
</div>
<div class="row" style="font-size: 6pt;">
F.1.1.32.18
</div>


@foreach ($S1BagianC['list'] as $key => $row)
<pagebreak>
<br>
<div class="lampiran">
    <div class='row' style="text-align: center; font-weight: bold; border-bottom: 2px solid #000;">
        <div class="header-left">
            <div style="float: left; display: inline-block; width: 20px; ">
                <table rotate="-90" cellspacing="0" style="font-sie: 15pt; margin-right: 15px;">
                    <tr><th>FORMULIR</th></tr>
                </table>
            </div>
            <div style="float: left; display: inline-block; text-align: center;">
                <p style="font-size: 22pt;">1770 S-I</p>
                <P style="font-size: 7pt;"> KEMENTERIAN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK </p>
            </div>
        </div>
        <div class="header-mid">
            <p style="font-size: 8pt;">LAMPIRAN I</p>
            <p style="font-size: 8.5pt; padding: 4px;">SPT TAHUNAN PPh WAJIB PAJAK ORANG PRIBADI</p>
            <ul style="font-size: 7pt; text-align: left; font-weight: normal; margin-top: 0; margin-bottom: -0.001rem; padding-left: 16.5px;">
                <li>PENGHASILAN NETO DALAM NEGERI LAINNYA</li>
                <li>PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</li>
                <li>DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</li>
            </ul>
        </div>
        <div class="header-right">
            <div style="float: left; display: inline-block; width: 20px;">
                <table rotate="-90" cellspacing="0" style="font-sie: 7pt; margin-right: 10px;">
                    <tr><th>TAHUN PAJAK</th></tr>
                </table>
            </div>
            <div style="float: left; display: inline-block; text-align: center; margin-top: 20px;">
                <br>
                <table cellspacing="-1" class="table-box-year">
                    <tr>
                        <th>{{ $year[0] }}</th>
                        <th>{{ $year[1] }}</th>
                        <th>{{ $year[2] }}</th>
                        <th>{{ $year[3] }}</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div class="row" style="border-left:2px solid #000; border-bottom: 2px solid #000; border-right: 2px solid #000;">
        <table cellspacing="-1" class="table-box-spt profile" style="margin-top: 6px;">
            <tr>
                <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden; padding-left:3px;">NPWP</td>
                <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                @for ($i = 0; $i < strlen($npwp); $i++)
                    @php $alpha = substr($npwp, $i, 1); @endphp
                    @if ($alpha == "." || $alpha == "-")
                        <td style="border-style: hidden;">&nbsp;</td>
                    @else
                        <td>{{ $alpha  }}</td>
                    @endif
                @endfor
            </tr>
        </table>
        <table cellspacing="-1" class="table-box-spt profile">
            <tr>
                <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden; padding-left:3px;">NAMA WAJIB PAJAK</td>
                <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
                @for ($i = 0; $i < strlen($name); $i++)
                    <td>{{ substr($name, $i, 1) }}</td>
                @endfor
            </tr>
        </table>
    </div>

    <br>

    <div class="row">
        <table class="table table-title">
            <tr>
                <th width="7%">BAGIAN A</th>
                <th width="3%" align="center">:</th>
                <th width="90%"><span>PENGHASILAN NETO DALAM NEGERI LAINNYA <br> (TIDAK TERMASUK PENGHASILAN DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL)</span></th>
            </tr>
        </table>

        <table class="table table-dotted">
            <thead>
                <tr>
                    <th style="border-left: 1px solid #000;">NO</th>
                    <th colspan="2">JENIS PENGHASILAN</th>
                    <th>JUMLAH PENGHASILAN</th>
                </tr>
            </thead>
            <tbody>
                <tr class="first-row">
                    <td style="border-left: 1px solid #000;">(1)</td>
                    <td colspan="2">(2)</td>
                    <td>(3)</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">1</td>
                    <td colspan="2">BUNGA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['bunga'] : '0' }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">2</td>
                    <td colspan="2">ROYALTI</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['royalti'] : '0' }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">3</td>
                    <td colspan="2">SEWA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['sewa'] : '0' }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">4</td>
                    <td colspan="2">PENGHARGAAN DAN HADIAH</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['hadiah'] : '0' }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">5</td>
                    <td colspan="2">KEUNTUNGAN DARI PENJUALAN/PENGALIHAN HARTA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['pengalihan_harta'] : '0' }}</td>
                </tr>
                <tr class="last-row">
                    <td align="center" class="last-row" style="border-left: 1px solid #000;">6</td>
                    <td colspan="2">PENGHASILAN LAINNYA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianA['list']['penghasilan_lainnya'] : '0' }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td width="5%" style="border-left: 1px solid #000;">&nbsp;</td>
                    <td width="60%">JUMLAH BAGIAN A</td>
                    <td width="5%">JBA</td>
                    <td width="30%" align="right">{{ $key == 0 ? $S1BagianA['jumlah_total_rp'] : '0' }}</td>
                </tr>
                <tr class="footer-remarks">
                    <td colspan="3"></td>
                    <td>Pindahkan Jumlah Bagian A ke Formulir Induk 1770 S Bagian A<br>angka (2)</td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="row">
        <table class="table table-title">
            <tr>
                <th width="7%">BAGIAN B</th>
                <th width="3%" align="center">:</th>
                <th width="90%"><span>PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</span></th>
            </tr>
        </table>

        <table class="table table-dotted">
            <thead>
                <tr>
                    <th style="border-left: 1px solid #000;">NO</th>
                    <th colspan="2">JENIS PENGHASILAN</th>
                    <th>JUMLAH PENGHASILAN</th>
                </tr>
            </thead>
            <tbody>
                <tr class="first-row">
                    <td style="border-left: 1px solid #000;">(1)</td>
                    <td colspan="2">(2)</td>
                    <td>(3)</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">1</td>
                    <td colspan="2">BANTUAN/SUMBANGAN/HIBAH</td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['sumbangan'] : 0 }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">2</td>
                    <td colspan="2">WARISAN</td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['warisan'] : 0 }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">3</td>
                    <td colspan="2"><span>BAGIAN LABA ANGGOTA PERSEROAN KOMANDITER TIDAK ATAS SAHAM,<br> PERSEKUTUAN, PERKUMPULAN, FIRMA, KONGSI</span></td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['laba_anggota'] : 0 }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">4</td>
                    <td colspan="2">KLAIM ASURANSI KESEHATAN, KECELAKAAN, JIWA, DWIGUNA, BEASISWA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['klaim_asuransi'] : 0 }}</td>
                </tr>
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">5</td>
                    <td colspan="2">BEASISWA</td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['beasiswa'] : 0 }}</td>
                </tr>
                <tr class="last-row">
                    <td align="center" style="border-left: 1px solid #000;">6</td>
                    <td colspan="2">PENGHASILAN LAINNYA YANG TIDAK TERMASUK OBJEK PAJAK</td>
                    <td align="right">{{ $key == 0 ? $S1BagianB['list']['penghasilan_lainnya'] : 0 }}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td width="5%" style="border-left: 1px solid #000;">&nbsp;</td>
                    <td width="60%">JUMLAH BAGIAN B</td>
                    <td width="5%">JBB</td>
                    <td width="30%" align="right">{{ $key == 0 ? $S1BagianB['jumlah_total_rp'] : 0 }}</td>
                </tr>
            </tfoot>
        </table>

        <br><br>

    </div>

    <div class="row">
        <table class="table table-title">
            <tr>
                <th width="7%">BAGIAN C</th>
                <th width="3%" align="center">:</th>
                <th width="90%"><span>DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</span></th>
            </tr>
        </table>

        <table class="table table-dotted">
            <thead>
                <tr>
                    <th rowspan="2" class="last-row" style="border-left: 1px solid #000;">NO</th>
                    <th rowspan="2">NAMA PEMOTONG/<br>PEMUNGUT PAJAK</th>
                    <th rowspan="2">NPWP PEMOTONG/<br>PEMUNGUT PAJAK</th>
                    <th colspan="2">BUKTI PEMOTONGAN/PEMUNGUTAN</th>
                    
                    <th rowspan="2" colspan="2">JENIS PAJAK:<br>PPh PASAL 21/<br>22/23/24/26/DTP*</th>

                    <th rowspan="2">JUMLAH PPh YANG<br>DIPOTONG/DIPUNGUT</th>
                </tr>
                <tr class="subheader">
                    <th>NOMOR</th>
                    <th>TANGGAL</th>
                </tr>
            </thead>
            <tbody>
            <tr class="first-row">
                    <td style="border-left: 1px solid #000;">(1)</td>
                    <td>(2)</td>
                    <td>(3)</td>
                    <td>(4)</td>
                    <td>(5)</td>
                    <td colspan="2">(6)</td>
                    <td>(7)</td>
                </tr>
                
                @foreach ($row as $val)
                    <tr class="{{ $loop->iteration == 5 ? 'last-row' : '' }}">
                        <td align="center" class="" style="border-left: 1px solid #000;">{{ $loop->iteration }}. {!! $loop->iteration == 5 ? '<br>dst' : '' !!}</td>
                        <td>{{ $val['name'] }}</td>
                        <td>{{ $val['npwp'] }}</td>
                        <td>{{ $val['slip_number'] }}</td>
                        <td align="center">{{ $val['slip_date'] }}</td>
                        <td colspan="2">{{ $val['pasal'] }}</td>
                        <td align="right">{{ $val['value'] }}</td>
                    </tr>
                @endforeach

                @if (count($S1BagianC['list']) != $loop->iteration)
                    <tr class="last-row">
                        <td align="center" class="" style="border-left: 1px solid #000;">5.<br> dst</td>
                        <td align="center">Dilanjutkan</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td colspan="2"></td>
                        <td></td>
                    </tr>
                @else
                    @for ($i = 1; $i <= (5 - count($row)); $i++)
                        <tr class="{{ count($row) + $i == 5 ? 'last-row' : '' }}">
                            <td align="center" class="" style="border-left: 1px solid #000;">{{ count($row) + $i }}. {!! count($row) + $i == 5 ? '<br>dst' : '' !!}</td>
                            <td align="center"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td></td>
                        </tr>
                    @endfor
                @endif
                
                
            </tbody>
            <tfoot>
                <tr>
                    <td class="last-row" style="border-left: 1px solid #000;"></td>
                    <td colspan="4">JUMLAH BAGAN C</td>
                    <td style="background-color: #a6a6a6;"></td>
                    <td>JBC</td>
                    <td align="right">{{ $loop->iteration == count($S1BagianC['list']) ? $S1BagianC['jumlah_total_rp'] : '' }}</td>
                </tr>
                <tr class="footer-remarks">
                    <td colspan="7"></td>
                    <td>Pindahkan Jumlah Bagian C ke Formulir<br>Induk 1770 S Bagian D angka 12</td>
                </tr>
                <tr class="footer-remarks">
                    <td width="5%"></td>
                    <td width="20%"></td>
                    <td width="15%"></td>
                    <td width="10%"></td>
                    <td width="10%"></td>
                    <td width="10%"></td>
                    <td width="5%"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="row">
        <table class="table" style="font-size: 6pt;">
            <tr>
                <td colspan="3" style="font-weight: bold;">Catatan</td>
            </tr>
            <tr>
                <td style="width: 1%; font-weight: bold; text-align: left;">*)</td>
                <td style="width: 2%; text-align: center;">-</td>
                <td style="width: 97%;">DTP : Ditanggung Pemerintah</td>
            </tr>
            <tr>
                <td style="width: 1%; font-weight: bold; text-align: left;"></td>
                <td style="width: 2%; text-align: center;">-</td>
                <td style="width: 97%;">Kolom (6) diisi dengan pilihan PPh Pasal 21/22/23/24/26/DTP (Contoh : ditulis 21, 22, 23, 24, 26, DTP)</td>
            </tr>
            <tr>
                <td style="width: 1%; font-weight: bold; text-align: left;"></td>
                <td style="width: 2%; text-align: center;">-</td>
                <td style="width: 97%;">Jika terdapat kredit pajak PPh Pasal 24, maka jumlah yang diisi adalah maksimum yang dapat dikreditkan sesuai lampiran tersendiri</td>
            </tr>
            <tr>
                <td style="width: 1%; font-weight: bold; text-align: left;"></td>
                <td style="width: 2%; text-align: center;"></td>
                <td style="width: 97%;">(lihat petunjuk pengisian tentang Lampiran I Bagian C dan Induk SPT angka 3)</td>
            </tr>  
        </table>
    </div>
    <div class="row" style="margin-top: 140px; margin-left: 20px;">
        <div class="col" style="width: 49%;">
            <p style="font-size: 6pt; font-weight: bold; margin-top: 5px; border: 1px solid #000; text-align: center; padding: 2px 0px;">
                JIKA FORMULIR INI TIDAK MENCUKUPI, DAPAT DIBUAT SENDIRI SESUAI DENGAN BENTUK INI
            </p>
        </div>
        <div class="col">
            <table style="font-size: 7pt; margin-left: 150px;">
                <tr>
                    <td style="vertical-align:middle;">Halaman ke- </td>
                    <td style="border: 1px dotted #000; padding: 4px 7px; vertical-align:middle;">{{ $loop->iteration }}</td>
                    <td style="vertical-align:middle;"> dari </td>
                    <td style="border: 1px dotted #000; padding: 4px 7px; vertical-align:middle;">{{ count($S1BagianC['list']) }}</td>
                    <td style="vertical-align:middle;"> halaman Lampiran-I </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endforeach

<pagebreak></pagebreak>

<br>

<div class='row' style="text-align: center; font-weight: bold; border-bottom: 2px solid #000;">
    <div class="header-left">
        <div style="float: left; display: inline-block; width: 20px; ">
            <table rotate="-90" cellspacing="0" style="font-sie: 15pt; margin-right: 15px;">
                <tr><th>FORMULIR</th></tr>
            </table>
        </div>
        <div style="float: left; display: inline-block; text-align: center;">
            <p style="font-size: 22pt;">1770 S-II</p>
            <P style="font-size: 7pt;"> KEMENTERIAN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK </p>
        </div>
    </div>
    <div class="header-mid">
        <p style="font-size: 8pt;">LAMPIRAN II</p>
        <p style="font-size: 8.5pt; padding: 4px; border-bottom: 1px solid #000;">SPT TAHUNAN PPh WAJIB PAJAK ORANG PRIBADI</p>
        <ul style="font-size: 7pt; text-align: left; font-weight: normal; margin-top: 0; margin-bottom: -0.001rem; padding-left: 16.5px;">
            <li>PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</li>
            <li>HARTA PADA AKHIR TAHUN</li>
            <li>KEWAJIBAN/UTANG PADA AKHIR TAHUN</li>
            <li>DAFTAR SUSUNAN ANGGOTA KELUARGA</li>
        </ul>
    </div>
    <div class="header-right">
        <div style="float: left; display: inline-block; width: 20px;">
            <table rotate="-90" cellspacing="0" style="font-sie: 7pt; margin-right: 10px;">
                <tr><th>TAHUN PAJAK</th></tr>
            </table>
        </div>
        <div style="float: left; display: inline-block; text-align: center; margin-top: 20px;">
            <br>
            <table cellspacing="-1" class="table-box-year">
                <tr>
                    <th>{{ $year[0] }}</th>
                    <th>{{ $year[1] }}</th>
                    <th>{{ $year[2] }}</th>
                    <th>{{ $year[3] }}</th>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="row" style="border-left:2px solid #000; border-bottom: 2px solid #000; border-right: 2px solid #000;">
    <table cellspacing="-1" class="table-box-spt profile" style="margin-top: 6px;">
        <tr>
            <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden; padding-left:3px;">NPWP</td>
            <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
            @for ($i = 0; $i < strlen($npwp); $i++)
                @php $alpha = substr($npwp, $i, 1); @endphp
                @if ($alpha == "." || $alpha == "-")
                    <td style="border-style: hidden;">&nbsp;</td>
                @else
                    <td>{{ $alpha  }}</td>
                @endif
            @endfor
        </tr>
    </table>
    <table cellspacing="-1" class="table-box-spt profile">
        <tr>
            <td style="font-size: 8pt; font-weight: normal; text-align: left; width: 150px; border-style: hidden; padding-left:3px;">NAMA WAJIB PAJAK</td>
            <td style="font-size: 8pt; font-weight: normal; width: 15px; border-style: hidden;">:</td>
            @for ($i = 0; $i < strlen($name); $i++)
                <td>{{ substr($name, $i, 1) }}</td>
            @endfor
        </tr>
    </table>
</div>

<div class="row">
    <table class="table table-title">
        <tr>
            <th width="7%">BAGIAN A</th>
            <th width="3%" align="center">:</th>
            <th width="90%">PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</th>
        </tr>
    </table>

    <table class="table table-dotted">
        <thead>
            <tr>
                <th style="border-left: 1px solid #000;">NO</th>
                <th>SUMBER/JENIS PENGHASILAN</th>
                <th colspan="2">DASAR PENGENAAN PAJAK/<br>PENGHASILAN BRUTO<br><span style="font-size: 6pt">(rupiah)</span></th>
                <th>PPh TERUTANG <br> <br> <span style="font-size: 6pt">(rupiah)</span></th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td style="border-left: 1px solid #000;">(1)</td>
                <td>(2)</td>
                <td colspan="2">(3)</td>
                <td>(4)</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">1</td>
                <td>BUNGA DEPOSITO, TABUNGAN, DISKONTO SBI, SURAT <br> BERHARGA NEGARA</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['tabungan']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['tabungan']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">2</td>
                <td>BUNGA/DISKONTO OBLIGASI </td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['obligasi']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['obligasi']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">3</td>
                <td>PENJUALAN SAHAM DI BURSA EFEK</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['penjualan_saham']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['penjualan_saham']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">4</td>
                <td>HADIAH UNDIAN</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['undian']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['undian']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">5</td>
                <td>PESANGON, TUNJANGAN HARI TUA DAN TEBUSAN PENSIUN <br> YANG DIBAYARKAN SEKALIGUS</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['pesangon']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['pesangon']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">6</td>
                <td>HONORARIUM ATAS BEBAN APBN/APBD</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['apbd_apbn']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['apbd_apbn']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">7</td>
                <td>PENGALIHAN HAK ATAS TANAH DAN/ATAU BANGUNAN</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['pengalihan_tanah']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['pengalihan_tanah']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">8</td>
                <td>SEWA ATAS TANAH DAN/ATAU BANGGUNAN</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['sewa_tanah']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['sewa_tanah']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">9</td>
                <td>BANGUNAN YANG DITERIMA DALAM RANGKA BANGUN GUNA <br> SERAH</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['guna_serah']['gross'],0,',','.') }}</td>
                <td align="right">{{ $S2BagianA['list']['guna_serah']['netto'] }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">10</td>
                <td>BUNGA SIMPANAN YANG DIBAYARKAN OLEH KOPERASI <br> KEPADA ANGGOTA KOPERASI</BR></td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['bunga_simpanan']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['bunga_simpanan']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">11</td>
                <td>PENGHASILAN DARI TRANSAKSI DERIVATIF</td>
                <td align="right" colspan="2" style="background-color: #a6a6a6;"></td>
                <td style="background-color: #a6a6a6;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">12</td>
                <td>DIVIDEN</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['dividen']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['dividen']['netto'],0,',','.') }}</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; text-align: center;">13</td>
                <td>PENGHASILAN ISTERI DARI SATU PEMBERI KERJA</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['penghasilan_istri']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['penghasilan_istri']['netto'],0,',','.') }}</td>
            </tr>
            <tr class="last-row">
                <td style="border-left: 1px solid #000; text-align: center;">14</td>
                <td>PENGHASILAN LAIN YANG DIKENAKAN PAJAK FINAL <br> DAN/ATAU BERSIFAT FINAL</td>
                <td align="right" colspan="2">{{ number_format($S2BagianA['list']['lainnya']['gross'],0,',','.') }}</td>
                <td align="right">{{ number_format($S2BagianA['list']['lainnya']['netto'],0,',','.') }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td width="5%" style="border-left: 1px solid #000;"></td>
                <td width="35%">JUMLAH BAGIAN A</td>
                <td width="20%" style="background-color: #a6a6a6;"></td>
                <td width="10%">JBA</td>
                <td width="30%" align="right">{{ $S2BagianA['jumlah_total_rp'] }}</td>
            </tr>
        </tfoot>
        
    </table>
</div>

<div class="row">
    <table class="table table-title">
        <tr>
            <th width="7%">BAGIAN B</th>
            <th width="3%" align="center">:</th>
            <th width="90%">HARTA PADA AKHIR TAHUN</th>
        </tr>
    </table>
    <table class="table table-dotted">
        <thead>
            <tr>
                <th style="border-left: 1px solid #000;">NO.</th>
                <th>KODE <br> HARTA</th>
                <th>NAMA HARTA</th>
                <th colspan="2">TAHUN PEROLEHAN</th>
                <th>HARGA PEROLEHAN <br> <span style="font-size: 6pt">(rupiah)</span> </th>
                <th>KETERANGAN</th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td style="border-left: 1px solid #000;">(1)</td>
                <td>(2)</td>
                <td>(3)</td>
                <td colspan="2">(4)</td>
                <td>(5)</td>
                <td>(6)</td>
            </tr>
            
            @foreach ($S2BagianB['list'] as $val)
                <tr>
                    <td style="border-left: 1px solid #000; text-align:center;">{{ $loop->iteration }}</td>
                    <td style="text-align: center;">{{ $val['code'] }}</td>
                    <td>{{ $val['name'] }}</td>
                    <td colspan="2">{{ $val['year'] }}</td>
                    <td style="text-align:right;">{{ number_format($val['value'],0,',','.') }}</td>
                    <td>{{ $val['remark'] }}</td>
                </tr>
            @endforeach
            
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" style="border-left: 1px solid #000;">JUMLAH BAGIAN B</td>
                <td style="background-color: #a6a6a6;"></td>
                <td>JBB</td>
                <td style="text-align:right;">{{ $S2BagianB['jumlah_total_rp'] }}</td>
                <td style="background-color: #a6a6a6;"></td>
            </tr>
            <tr class="footer-remarks">
                <td width="5%"></td>
                <td width="8%"></td>
                <td width="25%"></td>
                <td width="7%"></td>
                <td width="7%"></td>
                <td width="24%"></td>
                <td width="24%"></td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="row">
    <table class="table table-title">
        <tr>
            <th width="7%">BAGIAN C</th>
            <th width="3%" align="center">:</th>
            <th width="90%">KEWAJIBAN/UTANG PADA AKHIR TAHUN</th>
        </tr>
    </table>
    <table class="table table-dotted">
        <thead>
            <tr>
                <th style="border-left: 1px solid #000;">NO.</th>
                <th>KODE <br> HUTANG</th>
                <th>NAMA PEMBERI PINJAMAN</th>
                <th>ALAMAT<br>PEMBERI PINJAMAN</th>
                <th colspan="2">TAHUN PEMINJAMAN</th>
                <th>JUMLAH</th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td style="border-left: 1px solid #000;">(1)</td>
                <td>(2)</td>
                <td>(3)</td>
                <td>(4)</td>
                <td colspan="2">(5)</td>
                <td>(6)</td>
            </tr>
            {{--  @foreach ($s2BagC['list'] as $row)
                <tr class="{{ $loop->iteration == count($s2BagC['list']) ? 'last-row' : '' }}">
                    <td align="center" style="border-left: 1px solid #000;">{{ $loop->iteration }}</td>
                    <td align="center">{{ $row['code'] }}</td>
                    <td>{{ $row['lender_name'] }}</td>
                    <td>{{ $row['lender_address'] }}</td>
                    <td colspan="2">{{ $row['year'] }}</td>
                    <td align="right">{{ $row['value'] }}</td>
                </tr>
            @endforeach  --}}
            
            @foreach ($S2BagianC['list'] as $val)
                <tr>
                    <td align="center" style="border-left: 1px solid #000;">{{ $loop->iteration }}</td>
                    <td align="center">{{ $val['code'] }}</td>
                    <td>{{ $val['lender_name'] }}</td>
                    <td>{{ $val['lender_address'] }}</td>
                    <td colspan="2">{{ $val['year'] }}</td>
                    <td align="right">{{ number_format($val['value'],0,',','.') }}</td>
                </tr>
            @endforeach
            
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" style="border-left: 1px solid #000;">JUMLAH BAGIAN C</td>
                <td style="background-color: #a6a6a6;"></td>
                <td>JBC</td>
                <td align="right">{{ $S2BagianC['jumlah_total_rp'] }}</td>
            </tr>
            <tr class="footer-remarks">
                <td width="5%"></td>
                <td width="8%"></td>
                <td width="25%"></td>
                <td width="28%"></td>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="24%"></td>
            </tr>
        </tfoot>
    </table>
</div>

<div class="row">
    <table class="table table-title">
        <tr>
            <th width="7%">BAGIAN D</th>
            <th width="3%" align="center">:</th>
            <th width="90%">DAFTAR SUSUNAN ANGGOTA KELUARGA</th>
        </tr>
    </table>
    <table class="table table-dotted">
        <thead>
            <tr>
                <th width="5%" style="border-left: 1px solid #000;">No</th>
                <th width="33%">NAMA</th>
                <th width="19%">NIK</th>
                <th width="19%">HUBUNGAN KELUARGA</th>
                <th width="24%">PEKERJAAN</th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td style="border-left: 1px solid #000;">(1)</td>
                <td>(2)</td>
                <td>(3)</td>
                <td>(4)</td>
                <td>(5)</td>
            </tr>
            @foreach ($S2BagianD as $key => $row) 
                <tr class="{{ ($loop->iteration) == count($S2BagianD) ? 'last-row' : '' }}">
                    <td align="center" style="border-left: 1px solid #000;">{{ $loop->iteration }}</td>
                    <td>{{ $row['name'] }}</td>
                    <td>{{ $row['nik'] }}</td>
                    <td>{{ $row['relation'] }}</td>
                    <td>{{ $row['job'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if ($SB7['ptkp_status'] == 'K/I')
<pagebreak></pagebreak> 
<table class="table">
    <tr>
        <th width="2.5%" style="background-color: #000;">-</th>
        <th width="95%"></th>
        <th width="2.5%" style="background-color: #000;">-</th>
    </tr>
</table>

<br>

<div class="row" style="font-weight: bold; text-align:center;">
    <p>LEMBAR PENGHITUNGAN PAJAK PENGHASILAN TERUTANG</p>
    <p>BAGI WAJIB PAJAK YANG KAWIN DENGAN STATUS PERPAJAKAN SUAMI-ISTERI PISAH HARTA  DAN PENGHASILAN (PH) ATAU </p>
    <p>ISTERI YANG MENGHENDAKI UNTUK MENJALANKAN HAK DAN KEWAJIBAN PERPAJAKANNYA SENDIRI (MT)</p>
</div>

<br><br>

<div class="row">
    <table class="table table-solid">
        <thead>
            <tr>
                <th>No.</th>
                <th colspan="2">Uraian</th>
                <th>Penghasilan Neto Suami</th>
                <th>Penghasilan Neto Istri</th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td>(1)</td>
                <td colspan="2">(2)</td>
                <td>(3)</td>
                <td>(4)</td>
            </tr>
            <tr>
                <td rowspan="9" align="center">A</td>
                <td colspan="2">PENGHASILAN NETO</td>
                <td align="right" style="background-color: #a6a6a6;"></td>
                <td align="right" style="background-color: #a6a6a6;"></td>
            </tr>
            <tr>
                <td align="center">1</td>
                <td>
                    <p>PENGHASILAN NETO DALAM NEGERI DARI USAHA DAN/ATAU PEKERJAAN BEBAS</p>
                    <p style="font-size: 5.5pt;">[Diisi dari Formulir 1770 Bagian A angka 1]</p>
                </td>
                <td align="right">{{ $PHMT['A1']['husband'] }}</td>
                <td align="right">{{ $PHMT['A1']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">2</td>
                <td>
                    <p>PENGHASILAN NETO DALAM NEGERI SEHUBUNGAN DENGAN PEKERJAAN</p>
                    <p style="font-size: 5.5pt;">[Diisi dari Formulir 1770 Bagian A angka 2 atau Formulir 1770 S Bagian A angka 1]</p>
                </td>
                <td align="right">{{ $PHMT['A2']['husband'] }}</td>
                <td align="right">{{ $PHMT['A2']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">3</td>
                <td>
                    <p>PENGHASILAN NETO DALAM NEGERI LAINNYA</p>
                    <p style="font-size: 5.5pt;">[Diisi dari Formulir 1770 Bagian A angka 3 atau Formulir 1770 S Bagian A angka 2]</p>
                </td>
                <td align="right">{{ $PHMT['A3']['husband'] }}</td>
                <td align="right">{{ $PHMT['A3']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">4</td>
                <td>
                    <p>PENGHASILAN NETO LUAR NEGERI</p>
                    <p style="font-size: 5.5pt;">[Diisi dari Formulir 1770 Bagian A angka 4 atau Formulir 1770 S Bagian A angka 3]</p>
                </td>
                <td align="right">{{ $PHMT['A4']['husband'] }}</td>
                <td align="right">{{ $PHMT['A4']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">5</td>
                <td>
                    <p>ZAKAT / SUMBANGAN KEAGAMAAN YANG BERSIFAT WAJIB</p>
                    <p style="font-size: 5.5pt;">[Diisi dari Formulir 1770 Bagian A angka 6 atau Formulir 1770 S Bagian A angka 5]</p>
                </td>
                <td align="right">{{ $PHMT['A5']['husband'] }}</td>
                <td align="right">{{ $PHMT['A5']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">6</td>
                <td>
                    <p>JUMLAH ( 1 + 2 + 3 + 4 - 5 )</p>
                </td>
                <td align="right">{{ $PHMT['A6']['husband'] }}</td>
                <td align="right">{{ $PHMT['A6']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">7</td>
                <td>
                    <p>KOMPENSASI KERUGIAN</p>
                    <p style="font-size: 5.5pt;">[Khusus Bagi WP OP yang menyelenggarakan pembukuan. Diisi dari Formulir 1770 Bagian A angka 8]</p>
                </td>
                <td align="right">{{ $PHMT['A7']['husband'] }}</td>
                <td align="right">{{ $PHMT['A7']['wife'] }}</td>
            </tr>
            <tr>
                <td align="center">8</td>
                <td>
                    <p>JUMLAH PENGHASILAN NETO ( 6 - 7 )</p>
                </td>
                <td align="right">{{ $PHMT['A8']['husband'] }}</td>
                <td align="right">{{ $PHMT['A8']['wife'] }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr class="footer-remarks">
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="50%"></td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
        </tfoot>
    </table>
</div>
<br><br>

<div class="row">
    <table class="table table-solid">
        <thead>
            <tr>
                <th>No.</th>
                <th colspan="3">Uraian</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tbody>
            <tr class="first-row">
                <td>(1)</td>
                <td colspan="3">(2)</td>
                <td>(3)</td>
            </tr>
            <tr>
                <td align="center">B</td>
                <td colspan="3">JUMLAH PENGHASILAN NETO SUAMI DAN ISTERI <span style="font-weight: bold;">[ A.8.(3) + A.8.(4) ]</span></td>
                <td align="right">{{ $PHMT['B'] }}</td>
            </tr>
            <tr>
                <td align="center">C</td>
                <td colspan="2">PENGHASILAN TIDAK KENA PAJAK</td>
                <td><span style="font-weight: bold;">[ K / I / {{ $PHMT['C']['ptkp_total'] }} ]</span></td>
                <td align="right">{{ $PHMT['C']['ptkp_value'] }}</td>
            </tr>
            <tr>
                <td align="center">D</td>
                <td colspan="3">PENGHASILAN KENA PAJAK <span style="font-weight: bold;">[ B - C ]</span></td>
                <td align="right">{{ $PHMT['D'] }}</td>
            </tr>
            <tr>
                <td align="center">E</td>
                <td colspan="3">PAJAK PENGHASILAN TERUTANG (GABUNGAN)</td>
                <td rowspan="5" style="background-color: #a6a6a6;"></td>
            </tr>
            <tr>
                <td rowspan="4"></td>
                <td align="center">1</td>
                <td>5% x {{ isset($PHMT['E']['detail'][0]) ? number_format($PHMT['E']['detail'][0]['nominal'],0,',','.') : '' }}</td>
                <td align="right">{{ isset($PHMT['E']['detail'][0]) ? number_format($PHMT['E']['detail'][0]['value'],0,',','.') : '' }}</td>
            </tr>
            <tr>
                <td align="center">2</td>
                <td>15% x {{ isset($PHMT['E']['detail'][1]) ? number_format($PHMT['E']['detail'][1]['nominal'],0,',','.') : '' }}</td>
                <td align="right">{{ isset($PHMT['E']['detail'][1]) ? number_format($PHMT['E']['detail'][1]['value'],0,',','.') : '' }}</td>
            </tr>
            <tr>
                <td align="center">3</td>
                <td>25% x {{ isset($PHMT['E']['detail'][2]) ? number_format($PHMT['E']['detail'][2]['nominal'],0,',','.') : '' }}</td>
                <td align="right">{{ isset($PHMT['E']['detail'][2]) ? number_format($PHMT['E']['detail'][2]['value'],0,',','.') : '' }}</td>
            </tr>
            <tr>
                <td align="center">4</td>
                <td>30% x {{ isset($PHMT['E']['detail'][3]) ? number_format($PHMT['E']['detail'][3]['nominal'],0,',','.') : '' }}</td>
                <td align="right">{{ isset($PHMT['E']['detail'][3]) ? number_format($PHMT['E']['detail'][3]['value'],0,',','.') : '' }}</td>
            </tr>
            <tr>
                <td align="center">F</td>
                <td colspan="3">
                    <p>PPh TERUTANG YANG DITANGGUNG SUAMI <span style="font-weight: bold;">[ (A.8.(3) / B) x E ]</span></p>
                    <p style="font-size: 5.5pt;">[Pindahkan nilai pada bagian ini ke SPT Suami bagian C angka 12 Formulir 1770 atau ke bagian C angka 9 Formulir 1770 S]</p>
                </td>
                <td align="right">{{ $PHMT['F'] }}</td>
            </tr>
            <tr>
                <td align="center">G</td>
                <td colspan="3">
                    <p>PPh TERUTANG YANG DITANGGUNG ISTERI <span style="font-weight: bold;">[ (A.8.(4) / B) x E ]</span></p>
                    <p style="font-size: 5.5pt;">[Pindahkan nilai pada bagian ini ke SPT Isteri bagian C angka 12 Formulir 1770 atau ke bagian C angka 9 Formulir 1770 S]</p>
                </td>
                <td align="right">{{ $PHMT['G'] }}</td>
            </tr>
        </tbody>
        <tfoot>
            <tr class="footer-remarks">
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="50%"></td>
                <td width="20%"></td>
                <td width="20%"></td>
            </tr>
        </tfoot>
    </table>
</div>
<br>
<div class="row" style="text-align: right; padding: 0px 7px;">
    <p>22 November 2019</p>
</div>
<br>
<div class="row" style="padding: 0px 20px;">
    <div style="background-color: #a6a6a6; text-align: center; font-weight: bold; padding: 5px 0px;">
        SUAMI
    </div>
    <br><br>
    <div>
        <div style="float: left; display: inline-block; width: 5%;">
            NAMA
        </div>
        <div style="float: left; display: inline-block; width: 3%; text-align: center;">
            :
        </div>
        <div style="float: left; display: inline-block; width: 90%;">
            {{ $PHMT['name']['husband'] }}
        {{--  ................................................................................................................................................................................................  --}}
        </div>
    </div>
    <br>
    <div>
        <div style="float: left; display: inline-block; width: 5%;">
            NPWP
        </div>
        <div style="float: left; display: inline-block; width: 3%; text-align: center;">
            :
        </div>
        <div style="float: left; display: inline-block; width: 90%;">
        {{ $PHMT['npwp']['husband'] }}
        {{--  ................................................................................................................................................................................................  --}}
        </div>
    </div>
    <br>
    <div>
        <div style="float: left; display: inline-block; width: 15%;">
            Tanda Tangan
        </div>
        <div style="float: left; display: inline-block; width: 84%; height: 50px; border: 3px solid #000;">
            
        </div>
    </div>
</div>

<br><br>

<div class="row" style="padding: 0px 20px;">
    <div style="background-color: #a6a6a6; text-align: center; font-weight: bold; padding: 5px 0px;">
        ISTRI
    </div>
    <br><br>
    <div>
        <div style="float: left; display: inline-block; width: 5%;">
            NAMA
        </div>
        <div style="float: left; display: inline-block; width: 3%; text-align: center;">
            :
        </div>
        <div style="float: left; display: inline-block; width: 90%;">
            {{ $PHMT['name']['wife'] }}
            {{--  ................................................................................................................................................................................................  --}}
        </div>
    </div>
    <br>
    <div>
        <div style="float: left; display: inline-block; width: 5%;">
            NPWP
        </div>
        <div style="float: left; display: inline-block; width: 3%; text-align: center;">
            :
        </div>
        <div style="float: left; display: inline-block; width: 90%;">
            {{ $PHMT['npwp']['wife'] }}
            {{--  ................................................................................................................................................................................................  --}}
        </div>
    </div>
    <br>
    <div>
        <div style="float: left; display: inline-block; width: 15%;">
            Tanda Tangan
        </div>
        <div style="float: left; display: inline-block; width: 84%; height: 50px; border: 3px solid #000;">
            
        </div>
    </div>
</div>
@endif
</body>
</html>