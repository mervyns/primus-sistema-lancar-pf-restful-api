<!DOCTYPE html>
<html>
<head>
    <title>SPT 1770 SS</title>
    <style>
        html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
        }

        body{
            font-family: Arial, Helvetica, sans-serif;
            background-color: #fff;
            color: #000 !important;
            font-size: 10pt;
        }

        table.table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 0;
        }

        .table-profile td {
            padding-top: 15px;
        }

        .row {
            width: 100%;
        }

        .bold {
            font-weight: bold;
        }
        
        .col {
            float: left;
            display: inline-block;
        }

        .center {
            text-align: center;
        }

        .table-content {
            border-collapse: collapse;
        }

        .table-content td {
            padding-top: 7px;
            padding-bottom: 5px;
            font-size: 8pt;
            vertical-align: middle;
        }

        .content {
            border: 2px solid #000;
        }
    </style>
</head>
<body>

<htmlpageheader name="MyHeader1">
    <div style="float: left; font-size: 1pt; background-color: #000; width: 15.11px; height: 11.33px;">1</div>
    <div style="float: right; font-size: 1pt; background-color: #000; width: 18.89px; height: 11.33px;">1</div>
</htmlpageheader>
<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1"/>
<htmlpagefooter name="footerSheet1">
    <div style="float: left; font-size: 1pt; background-color: #000; width: 15.11px; height: 11.33px;">1</div>
    <div style="float: right; font-size: 1pt; background-color: #000; width: 18.89px; height: 11.33px;">1</div>
</htmlpagefooter>
<sethtmlpagefooter name="footerSheet1" value="on"/>

<div class="row">
    <p style="text-align: center; font-size: 14pt;">&nbsp;</p>
</div>

<div class="row" style="border-bottom: 1px solid #000;">
    <div class="col" style="width: 30.5%; padding-left: 15px;">
        <p class="bold" style="font-size: 40pt;">1770 SS</p>
        <p class="bold" style="margin: 0 0 5px 0;">PERHATIAN :</p>
        <ul style="font-size: 6pt;margin-left: 12px;">
            <li style="margin: 0 0 5px 0;">SEBELUM MENGISI BACA DAHULU PETUNJUK PENGISIAN</li>
            <li style="margin: 0 0 5px 0;">ISI DENGAN HURUF CETAK/DIKETIK DENGAN TINTA HITAM</li>
            <li style="margin: 0 0 5px 0;">BERI TANDA 'X' PADA <span style="border: 1px solid #000; font-size: 13px; margin-top: 100px;">&nbsp;&nbsp;&nbsp;&nbsp;</span> (KOTAK PILIHAN) YANG SESUAI</li>
        </ul>
    </div>
    <div class="col center" style="width: 38%; border-left: 2px solid #000; border-right: 2px solid #000;">
        <div class="col" style="width: 45px;">
            <img style="width: 35px;height: 37px;" src="https://image.ibb.co/dMZJTV/imgpsh-fullsize.jpg" alt="imgpsh-fullsize" border="0" />
        </div>
        <div class="col" style="padding-bottom: 5px;">
            <p class="bold" style="font-size: 13pt;">KEMENTRIAN KEUANGAN RI</p>
            <p>DIREKTORAT JENDRAL PAJAK</p>
        </div>
        <div>
            <p class="bold" style="font-size: 22pt; border-top: 1px solid #000;">SPT TAHUNAN</p>
            <p class="bold" style="font-size: 11pt">PAJAK PENGHASILAN <br> WAJIB PAJAK ORANG PRIBADI</p>
            <p style="margin-top: 10px; margin-bottom: 10px;">
                <span style="font-size: 7pt;">H.01 &nbsp;</span>
                <span style="border: 1px solid #000; font-size: 13px; margin-top: 100px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </span>
                <span>&nbsp;&nbsp;SPT PEMBETULAN KE&nbsp;&nbsp;</span>
                <span style="font-size: 7pt;">&nbsp;&nbsp;H.02</span>
                <span>&nbsp; - ...</span>
            </p>
        </div>
    </div>
    <div class="col" style="width: 29%">
        <div class="col">
            <p class="bold" style="font-size: 7pt; margin: 15px 0 0 10px">TAHUN PAJAK</p>
            <p style="font-size: 7pt;">H.03</p>
        </div>
        <div class="col"></div>

        <div style="border: 1px solid; margin-left: 20px;">

        </div>
    </div>
</div>

<div class="row">
    <p class="bold center"style="font-size: 6pt;">FORMULIR INI DIPERUNTUKKAN BAGI WP ORANG PRIBADI BERPENGHASILAN DARI SELAIN USAHA DAN/ATAU PEKERJAAN BEBAS DAN TIDAK LEBIH DARI Rp60 JUTA DALAM SATU TAHUN</p>
</div>

<div class="row" style="border: 2px solid #000;">
    <div class="col" style="width: 35px; margin-left: 15px; padding: 7px 0;">
        <table rotate="-90">
            <tr><th class="bold" style="font-size: 7pt;">IDENTITAS <br> WAJIB PAJAK</th></tr>
        </table>
    </div>
    <div class="col">
        <table class="table table-profile" style="margin-left: 10px;">
            <tr>
                <td width="18%" style="font-size: 9pt;">NPWP</td>
                <td width="5%" style="font-size: 8pt;">I.01 :</td>
                <td width="45%" style="border-bottom: 1px solid;"></td>
                <td width="3%" style="text-align: center;">-</td>
                <td width="10%" style="border-bottom: 1px solid;"></td>
                <td width="3%" style="text-align: center;">-</td>
                <td width="10%" style="border-bottom: 1px solid;"></td>
                <td></td>
            </tr>
            <tr>
                <td width="" style="font-size: 9pt;">NAMA WAJIB PAJAK</td>
                <td width="" style="font-size: 8pt;">I.02 :</td>
                <td colspan="5" width="" style="border-bottom: 1px solid;"></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="bold">
        <p style="margin-left: 30px;">Pengisian kolom-kolom yang berisi nilai rupiah harus tanpa nilai desimal</p>
    </div>
</div>

<div class="row">
    <div class="content">
        {{-- <p class="bold" style="padding-left: 10px;">A.PAJAK PENGHASILAN</p> --}}
        <table class="table table-content" style="border-spacing: 0 5px;">
            <tr>
                <td width="3%" class="bold" style="text-align:center; border-bottom: 2px solid #000;">A.</td>
                <td colspan="5" class="bold" style="border-bottom: 2px solid #000;">PAJAK PENGHASILAN</td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">1</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Penghasilan Bruto dalam Negeri Sehubungan dengan Pekerjaan dan Penghasilan Netto dalam Negeri Lainnya</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">1</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.01</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">2</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Pengurangan</p>
                    <p style="margin-left: 5px; font-size: 6pt;">(Diisi jumlah pengurangan dari Formulir 1721-A1 angka 13 atau 1721-A2 angka 13)</p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">2</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.02</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">3</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Penghasilan Tidak Kena Pajak</p>
                    <p style="margin-left: 5px; font-size: 6pt;">(Diisi jumlah PTKP dari Formulir 1721-A1 angka 17 atau 1721-A2 angka 16)</p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">3</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.03</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">4</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Penghasilan Kena Pajak ( 1 - 2 - 3 )</p>
                    <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">4</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.04</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">5</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Pajak Penghasilan Terutang</p>
                    <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">5</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.05</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">6</td>
                <td width="71%">
                    <p style="margin-left: 5px;"> Pajak Penghasilan yang telah Dipotong oleh Pihak Lain</p>
                    <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">6</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.06</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">7</td>
                <td width="71%">
                    <p style="margin-left: 5px;">
                        &nbsp; a. &nbsp;&nbsp;
                        <span style="border: 1px solid #000; font-size: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>&nbsp;&nbsp;Pajak Penghasilan yang harus Dibayar Sendiri *</span>
                    </p>
                    <br>
                    <p style="margin-left: 5px;">
                        &nbsp; b. &nbsp;&nbsp;
                        <span style="border: 1px solid #000; font-size: 15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>&nbsp;&nbsp;Pajak Penghasilan yang Lebih Dipotong</span>
                    </p>
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">7</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.07</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
        </table>
    </div>
    
    <br>

    <div class="content">
        <table class="table table-content" style="border-spacing: 0 5px;">
            <tr>
                <td width="3%" class="bold" style="text-align:center; border-bottom: 2px solid #000;">B.</td>
                <td colspan="5" class="bold" style="border-bottom: 2px solid #000;">PENGHASILAN  YANG  DIKENAKAN  PPh  FINAL  DAN  YANG  DIKECUALIKAN  DARI  OBJEK  PAJAK</td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">8</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Dasar Pengenaan Pajak/Penghasilan Bruto Pajak Penghasilan Final</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">8</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.08</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">9</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Pajak Penghasilan Final Terutang</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">9</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.09</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">10</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Penghasilan yang Dikecualikan dari Objek Pajak</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">10</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.10</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
        </table>
    </div>

    <br>

    <div class="content">
        <table class="table table-content" style="border-spacing: 0 5px;">
            <tr>
                <td width="3%" class="bold" style="text-align:center; border-bottom: 2px solid #000;">C.</td>
                <td colspan="5" class="bold" style="border-bottom: 2px solid #000;">DAFTAR HARTA DAN KEWAJIBAN</td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">11</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Jumlah Keseluruhan Harta yang Dimiliki pada Akhir Tahun Pajak</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">11</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.11</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
            <tr>
                <td width="3%" style="text-align:center;">12</td>
                <td width="71%">
                    <p style="margin-left: 5px;">Jumlah Keseluruhan Kewajiban/Utang pada Akhir Tahun Pajak</p>
                    {{-- <p style="margin-left: 5px; font-size: 6pt;">&nbsp;</p> --}}
                </td>
                <td width="2.8%" class="bold" style="text-align:center; border: 1px solid #000;">12</td>
                <td width="2.5%" style="font-size: 6.5pt; text-align: center;">A.12</td>
                <td width="20%" style="border: 1px solid #000; text-align: right; padding-right: 2px;">2.000.000.000</td>
                <td></td>
            </tr>
        </table>
    </div>

</div>

<br>

<div class="row" style="width: 100%; border: 2px solid #000;">
    <div class="bold" style="text-align: center; border-bottom: 2px solid #000; padding: 5px 0;">
        PERNYATAAN
    </div>
    <div style="padding: 10px 30px;">
        <br>
        <p style="font-size: 7.9pt;">Dengan menyadari sepenuhnya akan segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan peraturan perundang-undangan yang berlaku</p>
        <div class="col" style="width: 65%; font-size: 7.9pt">
            Saya menyatakan bahwa apa yang telah saya beritahukan di atas adalah benar, lengkap, jelas.
        </div>
        <div class="col" style="width: 33%;">
            <div style="border: 1px solid #000; height: 80px;">
                &nbsp;
            </div>
            <p style="font-size: 6.5pt">TANDA TANGAN</p>
        </div>
    </div>
</div>

<br>
<div class="row">
    <p class="bold" style="padding-left: 15px; font-size: 7.5pt;">* Apabila terdapat Pajak Penghasilan yang harus dibayar sendiri, Wajib Pajak harus melampirkan asli SSP lembar ke-3</p>
</div>

</body>
</html>