<?php
// $router->prefix('report')
//     ->group(function() use ($router){
//         $router->get('/', 'ReportController@index');
//         $router->get('/print', function(){
//             return view('report');
//         });
//     });
$router->prefix('generate')
    ->group(function() use ($router) {
        $router->get('/spt_1770S', 'ReportController@Generate1770S');
        $router->get('/spt_1770SS', 'ReportController@Generate1770SS');
    });