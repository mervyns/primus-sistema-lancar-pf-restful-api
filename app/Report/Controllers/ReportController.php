<?php

namespace WebApp\Report\Controllers;

use Auth;
use Log;
use Validator;
use PDF;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;

use Api\UserIncomes\Repositories\UserIncomeRepository;

use Api\Users\Repositories\UserRepository;
use Api\UserDependants\Repositories\DependantRelationRepository;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;

use Infrastructure\Helper\TaxFillingHelper;

class ReportController extends Controller
{
    private $repositories;
    private $services;

    private $config;

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('ctfp');

        $this->config = [
            'mode'                 => '',
            'format'               => [210, 330],
            'default_font_size'    => '9',
            'default_font'         => '',
            'margin_left'          => 0,
            'margin_right'         => 0,
            'margin_top'           => 0,
            'margin_bottom'        => 0,
            'margin_header'        => 0,
            'margin_footer'        => 5,
            'orientation'          => 'P',
            'title'                => 'SPT TAHUNAN 1770 S',
            'author'               => '',
            'watermark'            => '',
            'watermark_font'       => '',
            'display_mode'         => 'fullpage',
            'watermark_text_alpha' => 0.2,
            'show_watermark_image' => true,
            'show_watermark'       => true,
            'tempDir'              => base_path().'/public/mpdf/tmp',
        ];
        
        $this->repositories = [
            'user' => new UserRepository,
            'klu' => new KlasifikasiLapanganUsahaRepository,
            'dependant' => new DependantRelationRepository,
            'user_income' => new UserIncomeRepository,
            'help' => new TaxFillingHelper
        ];
    }

    public function GenerateSPT() {

    }

    public function Generate1770S() {
        $data = $this->DataReport(Auth::user()->id);
        // dd($data);
        $pdf = PDF::loadView('generate_1770S', $data, [], $this->config);
        return $pdf->stream('SPTGO_1770S.pdf');
    }

    public function Generate1770SS() {
        $data = $this->DataReport(Auth::user()->id);

        $pdf = PDF::loadView('generate_1770SS', $data, [], $this->config);
        return $pdf->stream('SPTGO_1770SS.pdf');
    }

    private function DataReport($userId) {
        $result = [];
        $my_data = $this->repositories['help']->GetAllUserData($userId);
        
        $userProfile = $this->BuildUserProfile($my_data);
        $taxProfile = $this->BuildTaxProfile($my_data['tax_profile']);

        $result = array_merge($userProfile, $taxProfile);
        $result['S1BagianA'] = $this->BuildS1BagianA($my_data['income']);
        $result['S1BagianB'] = $this->BuildS1BagianB($my_data['income']);
        $result['S1BagianC'] = $this->BuildS1BagianCSplitPage($my_data['income']);
        $result['S2BagianA'] = $this->BuildS2BagianA($my_data);
        $result['S2BagianB'] = $this->BuildS2BagianB($my_data['asset']);
        $result['S2BagianC'] = $this->BuildS2BagianC($my_data['liability']);
        $result['S2BagianD'] = $this->BuildS2BagianD($my_data['dependant']);
        

        $SA1 = $this->ProcessingMainIncome($my_data['income']);
        $SA2 = $result['S1BagianA']['jumlah_total'];
        $SA3 = $this->ProcessingOverseasIncome($my_data['income']);
        $SA4 = $SA1 + $SA2 + $SA3;
        $SA5 = $this->ProcessingCharity($my_data['income']);
        $SA6 = $SA4 - $SA5;

        $result['SA1'] = $this->repositories['help']->CurrencyFormat($SA1);
        $result['SA2'] = $this->repositories['help']->CurrencyFormat($SA2);
        $result['SA3'] = $this->repositories['help']->CurrencyFormat($SA3);
        $result['SA4'] = $this->repositories['help']->CurrencyFormat($SA4);
        $result['SA5'] = $this->repositories['help']->CurrencyFormat($SA5);
        $result['SA6'] = $this->repositories['help']->CurrencyFormat($SA6);
        
        $SB7 = $this->BuildPTKP($my_data);
        $SB8 = $SA6 - $SB7['ptkp_value'];
        
        $result['SB7'] = $SB7;
        $result['SB8'] = $this->repositories['help']->CurrencyFormat($SB8);

        $SC9 = $this->repositories['help']->ProgressiveSummary($SB8)['value'];
        $SC10 = $this->repositories['help']->PPh24Dikreditkan($my_data);
        $SC11 = $SC9 + $SC10;

        $result['SC9'] = $this->repositories['help']->CurrencyFormat($SC9);
        $result['SC10'] = $this->repositories['help']->CurrencyFormat($SC10);
        $result['SC11'] = $this->repositories['help']->CurrencyFormat($SC11);

        $SD12 = $result['S1BagianC']['jumlah_total'];
        $SD13 = 0;
        $SD14 = array('a' => $this->ProcessingPPh25($my_data['additional_profile'])['yearly'], 'b' => 0);
        $SD15 = $SD14['a'] + $SD14['b'];

        $result['SD12'] = $this->repositories['help']->CurrencyFormat($SD12);
        $result['SD13'] = $this->repositories['help']->CurrencyFormat($SD13);
        $result['SD14'] = $SD14;
        $result['SD15'] = $this->repositories['help']->CurrencyFormat($SD15);

        $SE16 = array('a' => false, 'b' => false, 'date' => str_split('180101', 1), 'value' => 0);
        $SE17 = array('a' => false, 'b' => false, 'c' => false, 'd' => false);

        $result['SE16'] = $SE16;
        $result['SE17'] = $SE17;

        $SF18 = array('a' => false, 'b' => false, 'value' => 0);

        $result['SF18'] = $SF18;

        $SG19 = array('a' => false, 'b' => false, 'c' => false, 'd' => false, 'e' => false);
        $result['SG19'] = $SG19;

        if ($SB7['ptkp_status'] == 'K/I') {
            $result['PHMT'] = $this->BuildAttachmentPHMT($my_data);
        }

        return $result;
    }

    private function BuildUserProfile($data) {
        $result['name'] = str_pad(strtoupper($data['name']), 31, " ");
        $result['name_footer'] = strtoupper($data['name']);
        $result['phone'] = $data['homenumber'];
        $result['fax']  = $data['faxnumber'];
        return $result;
    }

    private function BuildTaxProfile($data) {
        
        $result['year'] = str_split(date('Y') - 1, 1);
        $result['npwp'] = $this->repositories['help']->FormatNPWP($data['npwp']);
        $result['responsibility'] = $data['marital_status'] == 2 ? $data['tax_responsibility_status_id'] : 0;
        $result['klu_name'] = strtoupper($data['klus']['name']);
        $result['klu_number'] = str_pad(strtoupper($data['klus']['klu_number']), 6, " ");
        $result['spouse_npwp'] = '  .   .   . -   .   ';
    
        $result['foo_date'] = str_split('20180101', 1);

        return $result;
    }

    private function  BuildPTKP($data) {
        $ptkp = $this->repositories['help']->PTKP($data['tax_profile']['marital_status'], $data['tax_profile']['tax_responsibility_status_id'], count($data['dependant']));

        $result['ptkp_total'] = count($data['dependant']);
        $result['ptkp_status'] = $ptkp['status'];
        $result['ptkp_value'] = (double)$ptkp['value'];

        return $result;
    }

    private function BuildS1BagianA($data){
        $values = array(
            6 => 0,
            7 => 0,
            8 => 0,
            11 => 0,
            12 => 0,
            13 => 0,
            14 => 0,
            33 => 0
        );

        foreach ($data as $row) {
            $income_id = $row['income_id'];
            if (array_key_exists($income_id, $values) == true) {
                $net_value = $row['net_value'] == 0 ? $row['gross_value'] : $row['net_value'];
                $values[$income_id] += $net_value;
            }
        }
        
        return array(
            'list' => array(
                'bunga' => $this->repositories['help']->CurrencyFormat($values[6]),
                'royalti' => $this->repositories['help']->CurrencyFormat($values[7]),
                'sewa' => $this->repositories['help']->CurrencyFormat($values[8]),
                'hadiah' => $this->repositories['help']->CurrencyFormat(($values[11] + $values[12] + $values[13] + $values[14])),
                'pengalihan_harta' => $this->repositories['help']->CurrencyFormat($values[33]),
                'penghasilan_lainnya' => $this->repositories['help']->CurrencyFormat(0)
            ),
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat(array_sum($values)),
            'jumlah_total' => array_sum($values)
        );
    }

    private function BuildS1BagianB($data) {
        $values = [
            15 => 0,
            26 => 0,
            25 => 0,
            9 => 0,
            40 => 0,
            28 => 0
        ];

        foreach ($data as $row) {
            $income_id = $row['income_id'];
            if (array_key_exists($income_id, $values) == true) {
                $net_value = $row['net_value'] == 0 ? $row['gross_value'] : $row['net_value'];
                $values[$income_id] += $net_value;
            }
        }

        return array(
            'list' => array (
                'sumbangan' => $this->repositories['help']->CurrencyFormat(($values[15] + $values[26])),
                'warisan' => $this->repositories['help']->CurrencyFormat($values[25]),
                'laba_anggota' => $this->repositories['help']->CurrencyFormat($values[9]),
                'klaim_asuransi' => $this->repositories['help']->CurrencyFormat($values[40]),
                'beasiswa' => $this->repositories['help']->CurrencyFormat($values[28]),
                'penghasilan_lainnya' => $this->repositories['help']->CurrencyFormat(0)
            ),
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat(array_sum($values)),
            'jumlah_total' => array_sum($values)

        );
    }

    private function BuildS1BagianC($data) {
        $values = [];
        $total = 0;
        foreach ($data as $val) {
            if (empty($val['with_holding_tax'])) continue;
            $values[] = array(
                'npwp' => (empty($val['with_holding_tax']['withholder_npwp'])) ? '' : $this->repositories['help']->FormatNPWP($val['with_holding_tax']['withholder_npwp']),
                'name' => $val['with_holding_tax']['withholder_name'],
                'slip_number'=> $val['with_holding_tax']['withholding_slip_number'],
                'slip_date' => $val['with_holding_tax']['withholding_slip_date'],
                'pasal' => '',
                'value' => $this->repositories['help']->CurrencyFormat((float)$val['with_holding_tax']['value'])
            );
            $total += (float)$val['with_holding_tax']['value'];
        }
        
        return array(
            'list' => $values,
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat($total),
            'jumlah_total' => $total
        );
    }

    private function BuildS1BagianCSplitPage($data) {
        $values = $this->BuildS1BagianC($data);
        $arrayCount = count($values['list']);
        
        $index = 0;
        $result = array();

        foreach ($values['list'] as $key => $val) {
            
            if ($arrayCount > 5) {
                $mod = ($key) % 4;
                if ($key > 0 && $mod == 0) $index += 1;
            }
            $result[$index][] = $val;
        }
        
        $values['list'] = $result;
        return $values;
    }

    private function BuildS2BagianA($data){
        $income = $data['income'];
        $asset = $data['asset'];

        $banks = array_filter($asset, function($obj) {
            return $obj['asset_id'] == 2;
        });

        $tabungan = array('netto' => 0, 'gross' => 0);
        $koperasi = array('netto' => 0, 'gross' => 0);

        if (count($banks) > 0) {
            foreach ($banks as $row) {
                $detail = json_decode($row['detail'], true);
                $type = $detail['bank_type'];
                
                $interest = empty($detail['interests']) ? 0 : (double)$detail['interests'];
                $taxes = empty($detail['taxes']) ? 0 : (double)$detail['taxes'];
                
                if ($type == 'Tabungan/Giro Bank') {
                    $tabungan['netto'] += $interest;
                    $tabungan['gross'] += $taxes;
                }else{
                    $koperasi['netto'] += $interest;
                    $koperasi['gross'] += $taxes;
                }

            }
        }
        
        $values_income = array(
            38 => array('netto' => 0, 'gross' => 0),    /** deposit dalam negeri */
            39 => array('netto' => 0, 'gross' => 0),    /** deposit luar negeri */
            42 => array('netto' => 0, 'gross' => 0),    /** Bunga Diskonto Obligasi */
            4  => array('netto' => 0, 'gross' => 0),    /** Penjualan Saham di Bursa Efek */
            10 => array('netto' => 0, 'gross' => 0),    /** Hadiah Undian */
            20 => array('netto' => 0, 'gross' => 0),    /** Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua */
            21 => array('netto' => 0, 'gross' => 0),    
            22 => array('netto' => 0, 'gross' => 0),
            23 => array('netto' => 0, 'gross' => 0),
            24 => array('netto' => 0, 'gross' => 0),
            29 => array('netto' => 0, 'gross' => 0),    /** Honorarium atas Beban APBN/APBD Golongan I */
            30 => array('netto' => 0, 'gross' => 0),    /** Honorarium atas Beban APBN/APBD Golongan II */
            31 => array('netto' => 0, 'gross' => 0),    /** Honorarium atas Beban APBN/APBD Golongan III */
            32 => array('netto' => 0, 'gross' => 0),    /** Honorarium atas Beban APBN/APBD Golongan IV */
            33 => array('netto' => 0, 'gross' => 0),    /** Pengalihan Hak Atas Tanah dan Bangunan */
            35 => array('netto' => 0, 'gross' => 0),    /** Bangunan Yang Diterima Dalam Rangka Bangun Guna Serah */
            3  => array('netto' => 0, 'gross' => 0),    /** Dividen */
            37 => array('netto' => 0, 'gross' => 0),    /** Penghasilan istri dari 1 pemberi kerja */
            2  => array('netto' => 0, 'gross' => 0),
        );

        $total = 0;

        foreach ($income as $key => $val) {
            $income_id = $val['income_id'];
            if (array_key_exists($income_id, $values_income) == false) continue;
            
            $netto = $val['net_value'];
            $gross = $val['gross_value'];

            $values_income[$income_id]['netto'] += $netto;
            $values_income[$income_id]['gross'] += $gross;

            $total += $netto;
        }

        $result = array(
            'tabungan' => array(
                'netto' => (double)$values_income[38]['netto'] + (double)$values_income[39]['netto'] + $tabungan['netto'], 
                'gross' => (double)$values_income[38]['gross'] + (double)$values_income[39]['gross'] + $tabungan['gross']
            ),
            'obligasi' => $values_income[42],
            'penjualan_saham' => $values_income[4],
            'undian' => $values_income[10],
            'pesangon' => array(
                'netto' => (double)$values_income[20]['netto'] + (double)$values_income[21]['netto'] + (double)$values_income[22]['netto'] + (double)$values_income[23]['netto'] + (double)$values_income[24]['netto'],
                'gross' => (double)$values_income[20]['gross'] + (double)$values_income[21]['gross'] + (double)$values_income[22]['gross'] + (double)$values_income[23]['gross'] + (double)$values_income[24]['gross']
            ),
            'apbd_apbn' => array(
                'netto' => (double)$values_income[29]['netto'] + (double)$values_income[30]['netto'] + (double)$values_income[31]['netto'] + (double)$values_income[32]['netto'],
                'gross' => (double)$values_income[29]['gross'] + (double)$values_income[30]['gross'] + (double)$values_income[31]['gross'] + (double)$values_income[32]['gross']
            ),
            'pengalihan_tanah' => $values_income[33],
            'sewa_tanah' => $values_income[2],
            'guna_serah' => $values_income[35],
            'bunga_simpanan' => $koperasi,
            'dividen' => $values_income[3],
            'penghasilan_istri' => $values_income[37],
            'lainnya' =>  array(
                'netto' => 0,
                'gross' => 0
            )
        );

        return array(
            'list' => $result,
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat($total),
            'jumlah_total' => $total
        );
    }

    private function BuildS2BagianB($data) {
        $result = [];
        $total = 0;

        foreach ($data as $row) {
            $result[] = array(
                'code' => $row['asset']['asset_code'],
                'name' => $row['asset']['name'],
                'year' => $row['acquisition_year'],
                'value' => $row['idr_value'],
                'remark' => '' 
            );
            $total += (double)$row['idr_value'];
        }

        usort($result, function($a, $b) {
            return $a['code'] <=> $b['code'];
        });

        return array(
            'list' => $result,
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat($total),
            'jumlah_total' => $total
        );
    }

    private function BuildS2BagianC($data) {
        $result = [];
        $total = 0;

        foreach ($data as $row) {
            $result[] = array(
                'code' => $row['liabilities']['liability_code'],
                'name' => $row['liabilities']['name'],
                'lender_name' => $row['lender_name'],
                'lender_address' => $row['lender_address'],
                'year' => $row['acquisition_year'],
                'value' => $row['value']
            );
            $total += (double)$row['value'];
        }

        usort($result, function($a, $b) {
            return $a['code'] <=> $b['code'];
        });

        return array(
            'list' => $result,
            'jumlah_total_rp' => $this->repositories['help']->CurrencyFormat($total),
            'jumlah_total' => $total
        );
    }

    private function BuildS2BagianD($data) {
        $result = [];

        foreach ($data as $row) {
            $result[] = array(
                'name' => $row['name'],
                'nik' => $row['ktp_number'],
                'relation' => $row['dependants']['name'],
                'job' => $row['job']
            );
        }

        return $result;
    }

    private function  BuildAttachmentPHMT($data) {
        $result = array(
            'A1' => array('husband' => 0, 'wife' => 0),
            'A2' => array('husband' => 0, 'wife' => 0),
            'A3' => array('husband' => 0, 'wife' => 0),
            'A4' => array('husband' => 0, 'wife' => 0),
            'A5' => array('husband' => 0, 'wife' => 0),
            'A6' => array('husband' => 0, 'wife' => 0),
            'A7' => array('husband' => 0, 'wife' => 0),
            'A8' => array('husband' => 0, 'wife' => 0),
            'B' => 0,
            'C' => array(),
            'D' => 0,
            'E' => array(),
            'F' => 0,
            'G' => 0,
            'name' => array('husband' => '', 'wife' => ''),
            'npwp' => array('husband' => '', 'wife' => '')
        );
        $gender = $data['gender'];
        $husband_data = [];
        $wife_data = [];

        if ($gender === 'L') {
            $husband_data = $data;
            $wife_data = $this->repositories['help']->GetAllUserData($data['companion']['companion_id']);
        }else{
            $husband_data = $this->repositories['help']->GetAllUserData($data['companion']['companion_id']);
            $wife_data = $data;
        }

        $A1_Husband = 0;
        $A1_Wife = 0;
        $A2_Husband = $this->ProcessingMainIncome($husband_data['income']);
        $A2_Wife = $this->ProcessingMainIncome($wife_data['income']);
        $A3_Husband = $this->BuildS1BagianA($husband_data['income'])['jumlah_total'];
        $A3_Wife = $this->BuildS1BagianA($wife_data['income'])['jumlah_total'];
        $A4_Husband = $this->ProcessingOverseasIncome($husband_data['income']);
        $A4_Wife = $this->ProcessingOverseasIncome($wife_data['income']);
        $A5_Husband = $this->ProcessingCharity($husband_data['income']);
        $A5_Wife = $this->ProcessingCharity($wife_data['income']);
        $A6_Husband = $A1_Husband + $A2_Husband + $A3_Husband + $A4_Husband - $A5_Husband;
        $A6_Wife = $A1_Wife + $A2_Wife + $A3_Wife + $A4_Wife - $A5_Wife;
        $A7_Husband = 0;
        $A7_Wife = 0;
        $A8_Husband = $A6_Husband - $A7_Husband;
        $A8_Wife = $A6_Wife - $A6_Wife;

        $B = $A8_Husband + $A8_Wife;
        $C = $this->BuildPTKP($data);
        $D = $B + $C['ptkp_value'];
        $E = $this->repositories['help']->ProgressiveSummary($D);
        $F = ($A8_Husband / $B) * $E['value'];
        $G = ($A8_Wife / $B) * $E['value'];

        $result['A1']['husband'] = $this->repositories['help']->CurrencyFormat($A1_Husband);
        $result['A1']['wife'] = $this->repositories['help']->CurrencyFormat($A1_Wife);
        $result['A2']['husband'] = $this->repositories['help']->CurrencyFormat($A2_Husband);
        $result['A2']['wife'] = $this->repositories['help']->CurrencyFormat($A2_Wife);
        $result['A3']['husband'] = $this->repositories['help']->CurrencyFormat($A3_Husband);
        $result['A3']['wife'] = $this->repositories['help']->CurrencyFormat($A3_Wife);
        $result['A4']['husband'] = $this->repositories['help']->CurrencyFormat($A4_Husband);
        $result['A4']['wife'] = $this->repositories['help']->CurrencyFormat($A4_Wife);
        $result['A5']['husband'] = $this->repositories['help']->CurrencyFormat($A5_Husband);
        $result['A5']['wife'] = $this->repositories['help']->CurrencyFormat($A5_Wife);
        $result['A6']['husband'] = $this->repositories['help']->CurrencyFormat($A6_Husband);
        $result['A6']['wife'] = $this->repositories['help']->CurrencyFormat($A6_Wife);
        $result['A7']['husband'] = $this->repositories['help']->CurrencyFormat($A7_Husband);
        $result['A7']['wife'] = $this->repositories['help']->CurrencyFormat($A7_Wife);
        $result['A8']['husband'] = $this->repositories['help']->CurrencyFormat($A8_Husband);
        $result['A8']['wife'] = $this->repositories['help']->CurrencyFormat($A8_Wife);
        
        $result['B'] = $this->repositories['help']->CurrencyFormat($B);
        $result['C'] = array('ptkp_total' => $C['ptkp_total'], 'ptkp_value' => $this->repositories['help']->CurrencyFormat($C['ptkp_value']));
        $result['D'] = $this->repositories['help']->CurrencyFormat($D);
        $result['E'] = $E;
        $result['F'] = $this->repositories['help']->CurrencyFormat($F);
        $result['G'] = $this->repositories['help']->CurrencyFormat($G);

        $result['name']['husband'] = strtoupper($husband_data['name']);
        $result['name']['wife'] = strtoupper($wife_data['name']);

        $result['npwp']['husband'] = $this->repositories['help']->FormatNPWP($husband_data['tax_profile']['npwp']);
        $result['npwp']['wife'] = $this->repositories['help']->FormatNPWP($wife_data['tax_profile']['npwp']);
        return $result;
    }

    protected function ProcessingMainIncome($data) {
        $result = 0;
        $income = array_filter($data, function($obj) {
            return $obj['income_id'] == 1;
        });
        if (count($income) > 0) $result = array_sum(array_column($income, 'net_value'));
        return (double)$result;
    }

    protected function ProcessingOverseasIncome($data) {
        $result = 0;
        $income = array_filter($data, function($obj) {
            return $obj['income_id'] == 52;
        });
        if (count($income) > 0) {
            foreach ($income as $row) {
                $detail = json_decode($row['detail'], true);
                $value = (double)$row['net_value'] * (double)$detail['kurs_value'];
                $result += $value;
            }
        }
        return $result;
    }

    protected function ProcessingCharity($data) {
        $result = 0;
        $income = array_filter($data, function($obj) {
            return $obj['income_id'] == 53;
        });
        if (count($income) > 0) $result = array_sum(array_column($income, 'net_value'));
        return (double)$result;
    }

    protected function ProcessingPPh25($data) {
        $detail = json_decode($data['detail'],true);
        $yearly = isset($detail['pph25_yearly']) ? $detail['pph25_yearly'] : 0;
        $monthly = isset($detail['pph25_monthly']) ? $detail['pph25_monthly'] : 0;

        return array(
            'yearly' => $yearly,
            'monthly' => $monthly
        );
    }
}
