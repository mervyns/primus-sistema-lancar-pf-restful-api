<?php

$router->middleware(['auth', 'web'])
        ->prefix('pass-final')
        ->group(function() use ($router) {
           $router->get('/', 'PassFinal@get');
           $router->post('/', 'PassFinal@saveOrUpdate');
           $router->delete( '/', 'PassFinal@delete');
           $router->post('/continued', 'PassFinalContinued@saveOrUpdate');
           $router->get('/continued/download', 'PassFinalContinued@download');

        });

/**
 * End of file
 */

/**
 * End of file
 */