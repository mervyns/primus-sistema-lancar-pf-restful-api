<?php
namespace WebApp\PassFinal\Controllers;
use Illuminate\Http\Request;
use Auth;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\Liabilities\Repositories\LiabilityRepository;
use Api\PassFinal\Repositories\PassFinalRepository;
use Api\Assets\Repositories\AssetRepository;

class PassFinal {
	private $repositories;

	private function initialize()
	{
		$this->repositories = [
			'user' => Auth::user(),
			'passfinal' => new PassFinalRepository,
		];
	}

	public function get()
	{
		$this->initialize();
		$data = [];
		$asset = new AssetRepository();
		$data['asset'] = $asset->getModel()->get()->toArray();

		$userAssets = new PassFinalRepository();
		$data['user_assets'] = $userAssets->getModel()->where('flag', 1)->with('asset', 'asset.category')->get()->toArray();
		$data['user_hutang'] = $userAssets->getModel()->where('flag', 2)->with('asset', 'asset.category')->get()->toArray();
		$data['user'] = $this->repositories['user'];
		$nominalall = 0;

		if (sizeof($data['user_assets']) > 0) {
			foreach($data['user_assets'] as $kei => $vval) {
				$nominalall = $nominalall + $vval['nominal'];
			}
		}

		if (sizeof($data['user_hutang']) > 0) {
			foreach($data['user_hutang'] as $kei => $vval) {
				$nominalall = $nominalall + $vval['nominal'];
			}
		}

		$countryRepository = new CountryRepository();
		$data['country'] = $countryRepository->get()->toArray();

		$bankRepository = new BankRepository();
		$data['bank_list'] = $bankRepository->get()->toArray(); 

		$liability = new LiabilityRepository();
		$data['liability'] = $liability->get()->toArray(); 

		$data['nominalall'] = $nominalall;
		return view('pass-final', array(
            'form_data' => $data
        ));
	}

	public function saveOrUpdate(Request $req)
	{
		$data = $req->all();
		unset($data['_token']);
		$this->initialize();
		if (isset($data['id'])) {
			$this->repositories['passfinal']->update($data);
		} else {
			$this->repositories['passfinal']->create($data);
		}
		return redirect('/pass-final');
	}

	public function delete(Request $req) {
		$data = $req->all();
		$this->initialize();
		$delete = $this->repositories['passfinal']->deletedata($data);
		return response()->json(["status" => true]);
	}
}
