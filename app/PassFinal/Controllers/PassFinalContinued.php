<?php
namespace WebApp\PassFinal\Controllers;
use Api\PassFinal\Repositories\PassFinalRepository;
use Illuminate\Http\Request;

use Auth;

class PassFinalContinued {
	private $repositories;

	private function inintialize()
	{
		$this->repositories = [
			'user' => Auth::user(),
		];
	}

	public function get()
	{
		$data = [];
		$data['data'] = json_decode(\Session::get('data_pass_final'));
		return $data;
	}

	public function saveOrUpdate(Request $req)
	{
		$data = $req->all();
		\Session::put('data_pass_final', json_encode($data));
		$arr['data'] = json_decode(json_encode($data));
		return view('pass-final-continued', array(
            'form_data' => $arr
        ));
	}

	public function download() {
		$objPHPExcel = new \PHPExcel();
		$pengguna = json_decode(\Session::get('data_pass_final'));
		// dd($pengguna);
		$sheet = $objPHPExcel->getActiveSheet();
		$name = ['Induk', 'A1', 'A2', 'B1', 'B2', 'C1', 'C2'];
		// ======================================================
		$objPHPExcel = \PHPExcel_IOFactory::load(public_path() ."/template/indux.xlsx");
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('AD23', 111111111);
		// ======================================================
		// $objPHPExcel = \PHPExcel_IOFactory::load(public_path() ."/template/indux.xlsx");
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 1)
					->where('category', 'A1 - Harta Tidak Dilaporkan SPT')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(1)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->assesment_document_number,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->document_type,
						$value->document_number,
						$value->qty_assets,
						$value->unit,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(1)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// ========================================================================================
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 2)
					->where('category', 'A2 - Hutang Tidak Dilaporkan SPT')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(2)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->supporting_document,
						$value->acquisition_description,
						$value->document_type,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(2)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// ======================================================================================
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 1)
					->where('category', 'B1 - Harta Tidak Dilaporkan SPT')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(3)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->assesment_document_number,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->document_type,
						$value->document_number,
						$value->qty_assets,
						$value->unit,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(3)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// =========================================================================================
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 2)
					->where('category', 'B2 - Hutang Tidak Dilaporkan SPT')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(4)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->supporting_document,
						$value->acquisition_description,
						$value->document_type,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(4)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// ============================================================================================
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 1)
					->where('category', 'C1 – Harta dengan Nilai yang Tidak Sesuai')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(5)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->assesment_document_number,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->document_type,
						$value->document_number,
						$value->qty_assets,
						$value->unit,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(5)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// =========================================================================================
		$userAssets = new PassFinalRepository();
		$hutangA2 = $userAssets->getModel()
					->where('flag', 2)
					->where('category', 'C2 - Hutang dengan Nilai yang Tidak Sesuai')
					->with('asset', 'asset.category', 'country')
					->get()
					->toArray();
		$objPHPExcel->setActiveSheetIndex(6)
		->setCellValue('C4', $pengguna->nama)
		->setCellValue('C5', implode("",$pengguna->npwp));
		if (count($hutangA2) > 0) {
			$indx = 11;
			foreach($hutangA2 as $key => $value) {
				$value = json_decode(json_encode($value));
				$val = [$key, 
						$value->asset->asset_code,
						$value->asset->name,
						$value->acquisition_year,
						$value->nominal,
						$value->country->currency,
						$value->country->name,
						$value->address,
						$value->name,
						$value->NPWP,
						$value->supporting_document,
						$value->acquisition_description,
						$value->document_type,
						$value->detail,
						];
				$increment = 'A';
				foreach ($val as $key1 => $value1) {
					$objPHPExcel->setActiveSheetIndex(6)->setCellValue($increment.$indx, $value1);			
					$increment++;
				}
			}
		}
		// ==========================================================================================
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="export.xls"');
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
