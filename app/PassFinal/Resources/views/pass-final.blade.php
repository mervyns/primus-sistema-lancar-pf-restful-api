@extends('layout.main')

@section('content')
<div class="boxed-body w-100 text-left">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            <div id="pass-final-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <form action="" id="formdatadiri">
                            <div>
                                <div class="form-group row form-inline">
                                    <label class="text-md-right col-sm-2 col-form-label col-form-label-sm text-xs-left" style="display:block">
                                    NPWP : </label>
                                    <div class="col-sm-10">
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline " type="number" onKeyPress="if(this.value.length == 1) return false;" />
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>

                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    <input class="form-control npwp" name="npwp[]" style="margin-right: 30px;width:40px;display:inline" type="number" onKeyPress="if(this.value.length == 1) return false;"/>
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="text-md-right col-sm-2 col-form-label col-form-label-sm text-xs-left">NAMA :</label>
                                    <div class="col-sm-5">
                                    <input class="form-control col-lg-8" name="nama" />
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="text-md-right col-sm-2 col-form-label col-form-label-sm text-xs-left"> ALAMAT :</label>
                                    <div class="col-sm-5">
                                    <input class="form-control col-lg-8" name="alamat" />
                                    </div>
                                </div>
                                <div class="form-group row ">
                                    <label class="text-md-right text-sm-left col-sm-2 col-form-label col-form-label-sm"> IKUT TA : </label>
                                    <div class="col-sm-5">
                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                            <input type="radio"
                                                class="custom-control-input" name="ikutta" value="ya" checked required>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Tidak</span>
                                        </label>
                                        <label class="custom-control custom-radio" style="margin-top: 5px;">
                                            <input type="radio"
                                                class="custom-control-input" name="ikutta" value="Tidak" required>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Ya</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                         <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Jenis Aset</th>
                                <th>Nama Aset</th>
                                <th>Tahun Perolehan</th>
                                <th>Nominal</th>
                                <th>Kategori</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @php
                                    // dd($form_data['user_assets']);
                                @endphp
                                @foreach($form_data['user_assets'] as $key => $asset)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$asset['asset']['category']['name']}}</td>
                                    <td>{{$asset['asset']['name']}}</td>
                                    <td>{{$asset['acquisition_year']}}</td>
                                    <td>{{$asset['nominal']}}</td>
                                    <td>{{$asset['category']}}</td>
                                    <td>
                                        <button class="btn btn-warning" id="btneditpassfinalaset{{$key}}" data='{{json_encode($asset)}}'>
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <form method="DELETE" id="assetDelete{{$asset['id']}}" action="{{ Request::url() }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$asset['id']}}" />
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                            <button class="btn btn-success pull-right" id = "btnAddAsset">Add</button>
                         </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Jenis Hutang</th>
                                <th>Nama Hutang</th>
                                <th>Tahun Perolehan</th>
                                <th>Nominal</th>
                                <th>Kategori</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                @foreach($form_data['user_hutang'] as $key => $hutang)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td>{{$asset['asset']['category']['name']}}</td>
                                    <td>{{$asset['asset']['name']}}</td>
                                    <td>{{$hutang['acquisition_year']}}</td>
                                    <td>{{$hutang['nominal']}}</td>
                                    <td>{{$hutang['category']}}</td>
                                    <td>
                                        <button class="btn btn-warning" id="btneditpassfinalhutang{{$key}}" data='{{json_encode($hutang)}}'>
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                        <form method="DELETE" id="hutangDelete{{$hutang['id']}}" action="{{ Request::url() }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{$hutang['id']}}" />
                                            <button type="submit" class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <button class="btn btn-success pull-right" type="button" id="btnAddHutang">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <hr class="col-md-12">
                <div class="col-md-12" align="right">
                    @php
                        $next = Request::segment(2) + 1;
                    @endphp
                    <form action="{{url('/pass-final/continued')}}" method="POST" id="continuelater">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{$form_data['nominalall']}}" name="nominalall">
                        <button type="submit" class="btn btn-info btn-lg">Save</button>
                    </form>
                </div>
            </div>
        </div>
</div>
@php
    // dd($form_data);
@endphp
<div class="modal fade" id="modalassetpassfinal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalassetpassfinaltitle"></h5>
      </div>
      <form id="form_create_assets" class="" method="post" action="{{ Request::url() }}">
      <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Kategori Asset Pas Final</label>
                <div class="col-sm-5">
                    <select name="category" id="category" class="form-control select2">
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Asset</label>
                <div class="col-sm-5">
                    <select name="type" id="type" class="form-control select2">
                        @foreach($form_data['liability'] as $key => $htg)
                    <option value="{{$htg['id']}}">{{$htg['liability_code']}} - {{$htg['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Tahun Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="acquisition_year" id="acquisition_year"/>
                    <input type="hidden" name="flag" id="flag" value="1" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Nilai Hutang</label>
                <div class="col-sm-5">
                    <input class="form-control" name="nominal" id="nominal" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Negara</label>
                <div class="col-sm-5">
                    <select name="country_id" id="country_id" class="form-control select2">
                        @foreach($form_data['country'] as $key => $country)
                        <option value="{{$country['id']}}">{{$country['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Alamat</label>
                <div class="col-sm-5">
                    <input class="form-control" name="address" id="address" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Nama Pemberi Hutang</label>
                <div class="col-sm-5">
                    <input class="form-control" name="name" id="name"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">NPWP</label>
                <div class="col-sm-5">
                    <input class="form-control" name="npwp" id="npwp"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Dokumen Pendukung</label>
                <div class="col-sm-5">
                    <input class="form-control" name="supporting_document" id="supporting_document"/> 
                </div>
                <div class="tooltip">
                    <button type="button" class="btn btn-success pull-right">
                        <i class="fa fa-info"></i>
                    </button>
                    <span class="tooltiptext">Nomor register notaris atau bukti pendukung lainnya disertai nama notaris</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Terkait Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="acquisition_description" id="acquisition_description"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Bentuk Agunan / Jaminan yang di berikan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_type" id="document_type" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Keterangan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="detail" id="detail" />
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modalhutangpassfinal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalhutangpassfinaltitle"></h5>
      </div>
      <div class="modal-body">
        <form id="form_create_hutang_pass_final" class="" method="post">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Kategori Hutang Pas Final</label>
                <div class="col-sm-5">
                    <input type="hidden" id="flag" name="flag" value="2" />
                    <select name="category" id="category" class="form-control select2">
                        <option value="B2 - Hutang Tidak Dilaporkan SPT" class="opt following">B2 - Hutang Tidak Dilaporkan SPT</option>
                        <option value="A2 - Hutang Tidak Dilaporkan SPT" class="opt notfollowing">A2 - Hutang Tidak Dilaporkan SPT</option>
                        <option value="C2 - Hutang dengan Nilai yang Tidak Sesuai">C2 - Hutang dengan Nilai yang Tidak Sesuai</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Asset</label>
                <div class="col-sm-5">
                    <select name="type" id="type" class="form-control select2">
                        @foreach($form_data['asset'] as $key => $ast)
                            <option value="{{$ast['id']}}">{{$ast['asset_code']}} - {{$ast['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Tahun Perolehan</label>
                <div class="col-sm-5">
                    <input class="form-control" id="acquisition_year" name="acquisition_year" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Nilai Nominal</label>
                <div class="col-sm-5">
                    <input class="form-control" id="nominal" name="nominal" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Dokumen Penilaian (optional)</label>
                <div class="col-sm-5">
                    <input class="form-control" name="assesment_document_number" id="assesment_document_number" />
                </div>
                <div class="tooltip">
                    <button type="button" class="btn btn-success pull-right">
                        <i class="fa fa-info"></i>
                    </button>
                    <span class="tooltiptext">Nomor dokumen pedoman untuk menghitung nilai aset</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Negara</label>
                <div class="col-sm-5">
                    <select name="country_id" id="country_id" class="form-control select2">
                        @foreach($form_data['country'] as $key => $country)
                        <option value="{{$country['id']}}">{{$country['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Alamat</label>
                <div class="col-sm-5">
                    <input class="form-control" name="address" id="address"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Atas Nama</label>
                <div class="col-sm-5">
                    <input class="form-control" name="name" id="name" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">NPWP</label>
                <div class="col-sm-5">
                    <input class="form-control" name="npwp" id="npwp" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jenis Dokuman</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_type" id="document_type" />
                </div>
                <div class="tooltip">
                    <button type="button" class="btn btn-success pull-right">
                        <i class="fa fa-info"></i>
                    </button>
                    <span class="tooltiptext">Contohnya : STNK, BPKB, Akta, Bilyet, dst</span>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Nomor Dokumen</label>
                <div class="col-sm-5">
                    <input class="form-control" name="document_number" id="document_number"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Jumlah Kuantitas Aset</label>
                <div class="col-sm-5">
                    <input class="form-control" name="qty_assets" id="qty_assets" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Satuan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="unit" id="unit" />
                </div>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-5 col-form-label col-form-label-sm">Keterangan</label>
                <div class="col-sm-5">
                    <input class="form-control" name="detail" id="detail" />
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection