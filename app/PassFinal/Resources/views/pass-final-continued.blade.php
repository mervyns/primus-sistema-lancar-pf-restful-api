@extends('layout.main')

@section('content')
<div class="boxed-body w-100 text-left">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger text-danger">
                    <p><strong>Ada kesalahan:</strong></p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
                <br>
            @endif
            @php
                // dd($form_data);
            @endphp
            <div id="pass-final-form-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row col-lg-12 form-inline">
                            <label class="col-lg-3"> Jumlah Yang Harus Ditebus : </label>
                            <input class="form-control" readonly  value="{{$form_data['data']->nominalall}}" />
                        </div>
                        <div class="form-group row col-lg-12 form-inline">
                            @php
                                $url = Request::url() . '/download';
                            @endphp
                            <a href="{{$url}}">
                                <button type="button" class="btn btn-success">
                                    Unduh sebagai Excel
                                </button>
                            </a>
                            <button class="btn btn-warning">
                                Generate ID Billing
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection