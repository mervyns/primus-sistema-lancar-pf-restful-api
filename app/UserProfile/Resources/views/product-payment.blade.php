@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Product Payment</h3>
                </div>
                
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="form_product_payment_add" class="" method="post" action="{{ url('/profile/payment') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td width="30%">Nama Produk</td>
                                                <td>:</td>
                                                <td width="70%">{{ $user_products['data']->product->name }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Durasi</td>
                                                <td>:</td>
                                                <td width="70%"> {{ $user_products['data']->years }} {{ ' Tahun' }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Quota</td>
                                                <td>:</td>
                                                <td width="70%">{{ $user_products['data']->product->invites }} {{ ' Orang' }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Promo</td>
                                                <td>:</td>
                                                <td width="70%">{{ ($promo_status == true) ? 'Ya' : 'Tidak' }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Deskripsi Promo</td>
                                                <td>:</td>
                                                <td width="70%"> {{ '-' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td width="30%">Unique Code</td>
                                                <td>:</td>
                                                <td width="70%" class="text-right">{{ $unique }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Jumlah</td>
                                                <td>:</td>
                                                <td width="70%" class="text-right">{{ number_format($amount,0,',','.') }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Potongan</td>
                                                <td>:</td>
                                                <td width="70%" class="text-right">{{ number_format($discount,0,',','.') }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%"><b>Total</b></td>
                                                <td>:</td>
                                                <td width="70%" class="text-right"><b>{{ number_format($total_amount,0,',','.') }}</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Pilih Bank</label>
                                        <div class="col-sm-5">
                                            <select id="bank_account_id" name="bank_account_id" class="form-control">
                                                @foreach ($bank_accounts as $bank_account)
                                                    <option value="{{ $bank_account->id }}"> {{ $bank_account->banks->name }} {{ ' - ' }} {{ $bank_account->account_number }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="image-wrapper" style="padding:5px; height auto; width: 300px">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm">Choose a Image</label>
                                        <div class="col-sm-5">
                                            <input type="file" name="image_invoice_product" id="image_invoice_product" accept="image/x-png,image/jpeg">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label class="text-right col-sm-5 col-form-label col-form-label-sm"></label>
                                        <div class="col-sm-5">
                                            <input type="hidden" name="unique_code" id="unique_code" value="{{ $unique }}">
                                            <input type="hidden" name="amount" id="amount" value="{{ $amount }}">
                                            <input type="hidden" name="discount" id="discount" value="{{ $discount }}">
                                            <input type="hidden" name="total_amount" id="total_amount" value="{{ $total_amount }}">
                                            <button type="submit" class="btn btn-success" style="margin-right: 15px;">CONTINUE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection