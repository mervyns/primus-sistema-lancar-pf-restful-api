@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">User Invite</h3>
                    <div class="boxed-content">To invite a new user, simply type in the email address of the lucky one below and we will take care of the rest.</div>
                </div>
                <form id="registration" class="" method="post" action="{{ url('/profile/invite') }}">
                {{ csrf_field() }}
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        <p><b>WARNING</b></p>
                                        <hr>
                                        <ul>
                                            @foreach (json_decode(session('message.content'), true) as $row)
                                                <li>{{ $row }}</li>
                                            @endforeach
                                        </ul>
                                        
                                    </div>
                                @endif
                                
                                @for ($i = 0; $i < ($invites - 1); $i++)
                                    <div class="form-group row">
                                        <label for="useremail" class="text-right col-sm-2 col-form-label col-form-label-sm">
                                            Email
                                        </label>
                                        <div class="col-sm-4">
                                            <input type="email" class="form-control form-control-sm" id="useremail[]" name="useremail[]" value="{{ (empty($partners[$i])) ?: (empty($partners[$i]['email'])) ?'': $partners[$i]['email'] }}" placeholder="Email" {{ (empty($partners[$i])) ?: 'disabled' }}>
                                            <input type="hidden" id="status[]" name="status[]" value="{{ (empty($partners[$i])) ? 0 : 1 }}">
                                            <input type="hidden" name="id[]" id="id[]" value="{{ (empty($partners[$i])) ?: $partners[$i]['id'] }}">
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-checkbox" style="margin-top: 1px; margin-left: -20px;">
                                                <input type="hidden" name="wife_or_husband[]" value="{{ (empty($partners[$i])) ?: (empty($partners[$i]['wife_or_husband'])) ? 0 : $partners[$i]['wife_or_husband'] }}">
                                                <input class="custom-control-input" type="checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value" {{ (empty($partners[$i])) ?: (empty($partners[$i]['wife_or_husband'])) ?: ($partners[$i]['wife_or_husband'] != 1) ?: 'checked' }} {{ (empty($partners[$i])) ?: 'disabled' }}>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" style="margin-top: 5px;">Suami / Istri Saya</span>
                                            </label>
                                        </div>
                                        <div  class="col-sm-2">
                                            @if (empty($partners['user_id']))
                                                <span class="badge badge-info" style="margin-top: 1px; margin-left: -20px;">Pending</span>
                                            @else
                                                <span class="badge badge-success" style="margin-top: 1px; margin-left: -20px;">Confirmed</span>
                                            @endif

                                        </div>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="row">
                            <hr class="col-md-12">
                            <div class="col-md-12" align="right">
                                <button type="submit" class="btn btn-success btn-lg">Continue</button>
                                <a href="" class="btn btn-danger btn-lg">Lewati</a>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
