@extends('layout.main')
@if ($type == false)
@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Pilih Produk</h3>
                    <div class="boxed-content">Terima kasih telah bergabung bersama kami.<br>Untuk melanjutkan ketahap selanjutnya, silahkan pilih produk dibawah ini sesuai dengan yang anda butuhkan.</div>
                </div>

                <form id="formUserProduct" method="post">
                    {{ csrf_field() }}
                    <div class="boxed-body w-100 text-left">
                        <div class="container">
                            @if(session()->has('message.level'))
                                <div class="alert alert-{{ session('message.level') }}">
                                    {!! session('message.content') !!}
                                </div>
                            @endif
                            <div class="col-md-12 btn-group-select btn-group-single-select-5">
                                @forelse($products as $product)
                                    <hr>
                                    <div class="row">
                                        <div class="text-center col-md-2">
                                            <button data-value="{{ $product->id }}" data-promo="0" type="button" class="btn btn-lg btn-outline-info {{ ($productId != $product->id) ?: 'active' }}" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                                    <p><b>{{ $product->name }}</b></p>
                                                    <p><b>{{ $product->currency }}</b></p>
                                            </button>
                                        </div>
                                        <div class="col-md-7">
                                            <ul>
                                                @foreach($product->feature as $value)
                                                    <li>{{ $value }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            @if ($product->promo == 1)
                                            <button data-value="{{ $product->id }}" data-promo="1" type="button" class="btn btn-lg btn-outline-danger {{ ($promo == 0) ?: 'active' }}" style="width:100%; white-space: pre-wrap; padding-left: 12px; padding-right: 12px; padding-top: 0px; padding-bottom: 0px; font-size : 10px; font-weight: normal;">
                                                    <p>{{ $product->promo_description }}</p>
                                            </button>
                                            @endif
                                            
                                        </div>
                                    </div>
                                    <br />
                                @empty
                                @endforelse
                            </div>
                            <input type="hidden" name="productid" id="productid" value="{{ (empty($productId)) ? '' : $productId }}">
                            <input type="hidden" name="promo" id="promo" value="0">
                            <input type="hidden" name="producttype" id="producttype" value="pending">
                            <div class="row">
                                <hr class="col-md-12">
                                <div class="col-md-12" align="center">
                                    <a href="{{ url('/tax-filling') }}" class="btn btn-info btn-lg">USE NOW PAY LATER <br><br> <small>1770SS Gets full free access </small></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
@else
@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray"></h3>
                    <!-- <div class="boxed-content">Terima kasih telah bergabung bersama kami.<br>Untuk melanjutkan ketahap selanjutnya, silahkan pilih produk dibawah ini sesuai dengan yang anda butuhkan.</div> -->
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <div class="alert alert-success" role="alert">
                                    <h4 class="alert-heading text-center">Product Subscription</h4>
                                    <hr>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td width="30%">Nama Produk</td>
                                                <td>:</td>
                                                <td width="70%">{{ $detail->name }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Quota</td>
                                                <td>:</td>
                                                <td width="70%">{{ $detail->invites }} {{ ' Orang' }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Expired Date</td>
                                                <td>:</td>
                                                <td width="70%">{{ $expired }}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Produk Layanan</td>
                                                <td>:</td>
                                                <td width="70%">
                                                    <ul>
                                                        @foreach (json_decode($detail->description,true) as $value)
                                                            <li>{{ $value }}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">Status</td>
                                                <td>:</td>
                                                <td width="70%">
                                                    <span class="badge badge-{{ ($order->status == 'approved') ? 'danger' : 'info' }}">{{ $order->status }}</span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@endif