@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Register Produk</h3>
                    <div class="boxed-content">Anda sudah di undang oleh teman anda untuk bergabung di SPTGO. Untuk tahap selanjutnya silahkan melakukan registrasi pada form dibawah ini.</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="productRegister" class="" method="post" action="{{ url('/product/invite') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label for="email" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Email
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="email"
                                                   class="form-control form-control-sm"
                                                   id="confirmemail" name="confirmemail" value="" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Password
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="password"
                                                   class="form-control form-control-sm"
                                                   id="password" name="password" value="" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fullname" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Nama Lengkap
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   class="form-control form-control-sm"
                                                   id="fullname" name="fullname" value="" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="gender" class="text-right col-sm-5 col-form-label col-form-label-sm">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="L" checked required><span class="custom-control-indicator"></span><span class="custom-control-description">Laki-laki</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="custom-control custom-radio" style="margin-top: 5px; margin-left: -20px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="P" required><span class="custom-control-indicator"></span><span class="custom-control-description">Perempuan</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        {{-- <input type="hidden" name="email" value="{{ $receiver }}"> --}}
                                        <div class="col-md-12 col-md-offset-4" align="center"><button type="submit" class="btn btn-success" style="margin-right: 15px;">Register</button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection