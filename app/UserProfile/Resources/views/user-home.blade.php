@extends('layout.main')

@section('content')
<section id="home-profile" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Selamat Datang</h3>
                    <div class="boxed-content"></div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card text-center text-white bg-info" style="min-height:120px;">
                                    <div class="card-body">
                                        <h1 class="card-title">Dashboard SPT GO</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('/tax-filling?redirect=true') }}" class="btn btn-success btn-lg" 
                                    style="width:100%; min-height:120px;"> 
                                        Start Filling <i class="fa fa-forward"></i>
                                </a>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Penghasilan Utama</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['main_income'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Penghasilan Tambahan Rutin</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['regular_income'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Penghasilan Tambahan Sekali Terima</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['earning_income'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Asset Diluar Investasi</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['financial_investment'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Investasi Finansial</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['other_asset'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card text-center">
                                    <div class="card-header">
                                        total
                                    </div>
                                    <div class="card-body">
                                        <h6 class="card-title">Hutang Kewajiban</h6>
                                        <br>
                                        <h3>{{ 'Rp ' . number_format($data['liability'],0,',','.') }}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection