@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Selamat Datang</h3>
                    <div class="boxed-content">Terima kasih telah melakukan verifikasi akun.<br>Untuk mengamankan akun Anda, lengkapi profil Anda di bawah ini.</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <form id="registration" class="" method="post" action="{{ url('/profile/me') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label for="email" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            Nama
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text"
                                                   class="form-control form-control-sm"
                                                   id="name" name="name" value="{{$user_profile['name']}}" placeholder="col-form-label-sm" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="gender" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            Jenis Kelamin
                                        </label>
                                        <div class="col-sm-3">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="L" {{ $user_profile['gender'] != 'L' ?: 'checked' }} required><span class="custom-control-indicator"></span><span class="custom-control-description">Laki-laki</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="custom-control custom-radio" style="margin-top: 5px;">
                                                <input type="radio" 
                                                    class="custom-control-input" name="gender" id="gender" value="P" {{ $user_profile['gender'] != 'P' ?: 'checked' }} required><span class="custom-control-indicator"></span><span class="custom-control-description">Perempuan</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="birthdate" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            Tanggal Lahir
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="text" name="birthdate" id="birthdate" value="{{ !empty($user_profile['birthdate']) ? date('d-m-Y', strtotime($user_profile['birthdate'])) : '' }}" class="form-control input-picker-format" style="width: 100%;" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="mobilenumber" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            No Handphone
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="tel" name="mobilenumber" id="mobilenumber" value="{{$user_profile['mobilenumber']}}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="homenumber" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            No Telepon
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="tel" name="homenumber" id="homenumber" value="{{$user_profile['homenumber']}}" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="faxnumber" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            No Fax
                                        </label>
                                        <div class="col-sm-5">
                                            <input type="tel" name="faxnumber" id="faxnumber" value="{{$user_profile['faxnumber']}}" class="form-control">
                                        </div>
                                    </div>
                                    
                                    <div id="security_question" style="{{ (isset($user_profile['security_question_id1']) == false || isset($user_profile['security_question_id2']) == false) ? '' : 'display:none;' }}">
                                        <br>
                                        <hr>
                                        <div class="form-group row">
                                            <div class="col-md-12"><center><h6>SECURITY QUESTION</h6></center></div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="securityquestion1" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Pertanyaan Keamanan 1
                                            </label>
                                            <div class="col-sm-5">
                                                <select name="securityquestion1" id="securityquestion1" class="form-control" required>
                                                    <option value=""></option>
                                                    @foreach($security_questions as $security_question)
                                                        @if($user_profile['security_question_id1'] == $security_question['id'])
                                                            <option value={{$security_question['id']}} selected>
                                                                {{$security_question['name']}}
                                                            </option>
                                                        @else
                                                            <option value={{$security_question['id']}}>
                                                            {{$security_question['name']}}
                                                        </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="securityanswer1" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Jawaban Pertanyaan Keamanan 1
                                            </label>
                                            <div class="col-sm-5">
                                                <input type="text" name="securityanswer1" id="securityanswer1" value="{{$user_profile['security_question_answer1']}}" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="securityquestion2" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Pertanyaan Keamanan 2
                                            </label>
                                            <div class="col-sm-5">
                                                <select name="securityquestion2" id="securityquestion2" class="form-control" required>
                                                    <option value=""></option>
                                                    @foreach($security_questions as $security_question)
                                                        @if($user_profile['security_question_id2'] == $security_question['id'])
                                                            <option value={{$security_question['id']}} selected>
                                                                {{$security_question['name']}}
                                                            </option>
                                                        @else
                                                            <option value={{$security_question['id']}}>
                                                            {{$security_question['name']}}
                                                        </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="securityanswer2" class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                                Jawaban Pertanyaan Keamanan 2
                                            </label>
                                            <div class="col-sm-5">
                                                <input type="text" name="securityanswer2" id="securityanswer2" value="{{$user_profile['security_question_answer2']}}" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="col-md-12 col-md-offset-4" align="center"><button type="submit" class="btn btn-success">COMPLETE</button></div>
                                        <br>
                                        <div class="col-md-12 col-md-offset-4" align="center" style="{{ (isset($user_profile['security_question_id1']) == false || isset($user_profile['security_question_id2']) == false) ? 'display:none;' : '' }}"><a href="javascript:void(0)" id="btn-show-security-question" style="color: #033a73; text-align:left; text-decoration: underline;">Update Security Question!</a></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal_confirm_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <form id="form_security_question" action="">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">ENTER YOUR PASSWORD</h5>
            </div>
            <div class="modal-body">
                <center><p>Untuk mengakses security question, anda perlu mengkonfirmasi akun anda dengan cara memasukkan password akun anda dibawah ini.</p></center>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password" id="password" placeholder="enter your password" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Konfirmasi</button>
            </div>
            </div>
        </div>
    </form>
</div>
@endsection
