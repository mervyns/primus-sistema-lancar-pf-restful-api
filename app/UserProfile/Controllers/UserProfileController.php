<?php

namespace WebApp\UserProfile\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;
use Api\Users\Services\UserService;
use Api\UserSecurityQuestions\Repositories\UserSecurityQuestionRepository;
use Api\SecurityQuestions\Repositories\SecurityQuestionRepository;
use Api\Users\Repositories\UserRepository;

use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;

use WebApp\UserProfile\Requests\UserProfileRequest;
use WebApp\Home\LoginProxy;

class UserProfileController extends Controller
{
    private $userService;
    private $repositories;

    private $loginProxy;

    public function __construct(UserService $userService, LoginProxy $loginProxy)
    {
	    $this->middleware('auth');

        $this->userService = $userService;
        $this->loginProxy = $loginProxy;
        $this->repositories = [
            'user_asset' => new UserAssetRepository,
            'user_income' => new UserIncomeRepository,
            'user_liability' => new UserLiabilityRepository,
        ];
    }

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
    public function index(Request $request)
    {
	    /**
	     *  @todo: Never call repository directly from controller but use service instead. This is anti-pattern.
	     */
        $securityQuestionRepository = new SecurityQuestionRepository();
        $securityQuestion = $securityQuestionRepository->get();
        $userRepository = new UserRepository();
        $userProfile = $this->userService->getById(Auth::user()->id);
        
		return view('profile', [
            'user_profile' => $userProfile,
			'security_questions' => $securityQuestion,
		]);
    }

    public function update(UserProfileRequest $request)
    {
        $securityQuestionRepository = new SecurityQuestionRepository();
        $securityQuestion = $securityQuestionRepository->get();
        $userRepository = new UserRepository();
        $userProfile = $this->userService->getById(Auth::user()->id);

        $bday = explode('/', $request->get('birthdate'));

        $submit_user['name'] = $request->get('name');
        $submit_user['gender'] = $request->get('gender');
        $submit_user['birthdate'] = $bday[2].'-'.$bday[1].'-'.$bday[0];
        $submit_user['mobilenumber'] = $request->get('mobilenumber');
        $submit_user['homenumber'] = $request->get('homenumber');
        $submit_user['faxnumber'] = $request->get('faxnumber');
        $submit_security1['security_question_id'] = $request->get('securityquestion1');
        $submit_security1['answer'] = $request->get('securityanswer1');
        $submit_security1['user_id'] = Auth::user()->id;
        $submit_security2['security_question_id'] = $request->get('securityquestion2');
        $submit_security2['answer'] = $request->get('securityanswer2');
        $submit_security2['user_id'] = Auth::user()->id;
        $user_id = Auth::user()->id;

        $is_failed = 1;

        try{
            $this->userService->updateProfile($user_id, $submit_user);
            $this->userService->removeUserSecurityQuestion($user_id);
            $this->userService->createUserSecurityQuestion($submit_security1);
            $this->userService->createUserSecurityQuestion($submit_security2);

            $is_failed = 0;
        } catch (\Exception $e) {
            $is_failed = 1;
        }

        if ($is_failed == 1) {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, gagal mengupdate profil anda.');
            return redirect('profile/me');
        } else {
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Profil anda berhasil diupdate!');
            //return redirect('profile/product');
            return redirect('profile/me');
        }
        
    }

    public function dashboard(){
        $result = [
            'main_income'           => 0,
            'regular_income'        => 0,
            'earning_income'        => 0,
            'financial_investment'  => 0,
            'other_asset'           => 0,
            'liability'             => 0,
        ];
        
        $income = $this->repositories['user_income']->getModel()
            ->with('incomes')
            ->where('user_incomes.user_id', Auth::user()->id)
            ->get();

        $asset = $this->repositories['user_asset']->getModel()
            ->with('asset')
            ->where('user_assets.user_id', Auth::user()->id)
            ->get();

        $liability = $this->repositories['user_liability']->getModel()
            ->with('liabilities')
            ->where('user_liabilities.user_id', Auth::user()->id)
            ->get();
        
        if (count($income) > 0){
            foreach ($income->toArray() as $row){
                $detail = json_decode($row['detail'], true);
                if ($row['incomes']['income_category_id'] == 1){
                    $result['main_income'] += (float)$row['net_value'];
                    continue;
                }

                if ($row['incomes']['income_category_id'] == 2){
                    $result['regular_income'] += (float)$row['net_value'];
                    continue;
                }

                if ($row['incomes']['income_category_id'] == 3){
                    $result['earning_income'] += (float)$row['net_value'];
                }

                
            }
        }

        if (count($asset) > 0){
            $other_asset = [5, 6, 7, 8, 21, 25, 26, 27, 31];
            $financial = [9, 10, 11, 12, 13, 14, 16, 17, 32, 33];
            foreach ($asset->toArray() as $row){
                if (in_array($row['asset_id'], $other_asset)){
                    $result['other_asset'] += (float)$row['idr_value'];
                    continue;
                }
                if (in_array($row['asset_id'], $financial)) {
                    $result['financial_investment'] += (float)$row['idr_value'];
                }
            }
        }

        if (count($liability) > 0){
            foreach ($liability->toArray() as $row){
                $result['liability'] += (float)$row['value'];
            }
        }
        
        return view('user-home', ['data' => $result]);
    }

    public function passwordRequest(Request $request) {
        $email    = Auth::user()->email;
        $password = $request->get('password');
        
        $send = $this->loginProxy->attemptPassword( $email, $password );

        return $this->response($send);
    }
}
