<?php

namespace WebApp\UserProfile\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Api\Products\Services\ProductService;
use Api\Products\Repositories\ProductRepository;

use Api\UserProducts\Services\UserProductService;
use Api\UserProducts\Repositories\UserProductRepository;

use Api\UserProducts\Services\UserInviteService;
use Api\UserProducts\Repositories\UserInviteRepository;

use Api\Users\Services\UserService;
use Api\Users\Repositories\UserRepository;

use Api\BankAccounts\Services\BankAccountService;
use Api\BankAccounts\Repositories\BankAccountRepository;

use Api\UserOrders\Services\UserOrderService;
use Api\UserOrders\Repositories\UserOrderRepository;

class UserProductController extends Controller{
	private $productService;
    private $userProductService;
    private $userService;
    private $userInviteService;
    private $bankAccountService;
    private $userOrderService;

    private $repositories;

	public function __construct(ProductService $productService, UserProductService $userProductService, UserService $userService, UserInviteService $userInviteService, BankAccountService $bankAccountService, UserOrderService $userOrderService){
		$this->middleware('auth');
        $this->productService = $productService;
        $this->userProductService = $userProductService;
        $this->userService = $userService;
        $this->userInviteService = $userInviteService;
        $this->bankAccountService = $bankAccountService;
        $this->userOrderService = $userOrderService;

        $this->repositories = [
            'product' => new ProductRepository(),
            'user_product' => new UserProductRepository(),
            'user' => new UserRepository(),
            'user_order' => new UserOrderRepository()
        ];
	}

	public function index()
    {
        $blnRejected = false;
        $blnType = false;
        $productDetail = array();
        $data = array();
        $expired = '-';
        $productId = 0;
        $checkProduct;

        $product = $this->productService->getAll();
        foreach ($product as $key => $val){
            $data[$key] = $val;
            $data[$key]['currency'] = "Rp " . number_format($val->price,0,',','.');
            $data[$key]['feature'] = json_decode($val->description, true);
        }
        
        $check = $this->userProductService->checkProductSubscription(Auth::user()->id);
        if (!empty($check['data']->parent_id)){
            $checkProduct = $this->userProductService->checkProductSubscription($check['data']->parent_id);
        }else{
            $checkProduct = $check;
        }
        $blnRejected = (!empty($checkProduct['order']) && $checkProduct['order']->status == 'rejected') ? true : false;
        
        if ($blnRejected == false){
            $productDetail = (!empty($checkProduct['data']->product)) ? $checkProduct['data']->product : array();
            $blnType = (!empty($checkProduct['order']) && count($checkProduct['order']) > 0) ? true : false;
            $expired = (!empty($checkProduct['data']->expired_at)) ? $checkProduct['data']->expired_at : '-';
            $productId = (!empty($checkProduct['data']->product_id)) ? $checkProduct['data']->product_id : 0;
            $promo = (empty($checkProduct['data']->promo)) ? 0 : $checkProduct['data']->promo;
        }

        return view('user-product', array(
            'products' => $data,
            'productId' => $productId,
            'order' => $checkProduct['order'],
            'expired' => $expired,
            'type' => $blnType,
            'detail' => $productDetail,
            'partner' => $checkProduct['partner'],
            'promo' => $promo
        ));
    }

    public function changeProduct(Request $request){
        $product_id = $request->get('productid');
        $user_id = Auth::user()->id;
        $promo = $request->get('promo');
        $promo_description = $request->get('promo_description');
        $years = ($promo == 1) ? 2 : 1;
        
        $submit_user_product['user_id'] = $user_id;
        $submit_user_product['product_id'] = $product_id;
        $submit_user_product['years'] = $years;
        $submit_user_product['type'] = 'pending';
        $submit_user_product['wife_or_husband'] = 0;
        $submit_user_product['promo'] = $promo;
        $submit_user_product['promo_description'] = $promo_description;

        $submit_user_product_child['product_id'] = $product_id;
        $submit_user_product_child['years'] = $years;
        $submit_user_product_child['promo'] = $promo;
        $submit_user_product_child['promo_description'] = $promo_description;
        
        
        \DB::beginTransaction();
        try{
            $checkProduct = $this->userProductService->checkProductSubscription($user_id);
            if (!empty($checkProduct['data'])){
                $this->userProductService->update($checkProduct['data']->id, $submit_user_product);
                $this->userProductService->update($checkProduct['data']->id, $submit_user_product_child);
            }else{
                $this->userProductService->create($submit_user_product);
            }
            \DB::commit();
            $result = array('status' => true);
        }catch (\Exception $ex){
            \DB::rollBack();
            $result = array('status' => false, 'message' => 'Maaf, gagal order product.', 'error' => $ex->getMessage());
        }

        return response()->json($result);
    }

    public function userInvitePage(){
        $user_id = Auth::user()->id;
        $checkProduct = $this->userProductService->checkProductSubscription($user_id);
        

        $invites = (empty($checkProduct['data']->product->invites)) ? array() : $checkProduct['data']->product->invites;
        return view('user-invite', ['invites' => $invites, 'partners' => $checkProduct['partner']]);
    }

    public function userInviteSubmit(Request $request){
        $email = $request->input('useremail');
        $couple = $request->input('wife_or_husband');
        $status = $request->input('status');
        $user_id = Auth::user()->id;
        $message = array();

        $userProductDetail = $this->userProductService->checkProductSubscription($user_id);

        \DB::beginTransaction();
        try{
            if (count($email) > 0){
                for ($i = 0; $i < count($email); $i++){
                    if ($email[$i]){
                        if ($status[$i] == 0){
                            $checkEmailExists = $this->userInviteService->checkEmailInvited($email[$i]);
                            if ($checkEmailExists['status']['invited']['status'] == true){
                                $message[] = $checkEmailExists['status']['invited']['message'];
                                continue;
                            }
                            if ($checkEmailExists['status']['member']['status'] == true){
                                $message[] = $checkEmailExists['status']['member']['message'];
                                continue;
                            }

                            $submit_user_invite['parent_id'] = $userProductDetail['data']->id;
                            $submit_user_invite['years'] = $userProductDetail['data']->years;
                            $submit_user_invite['product_id'] = $userProductDetail['data']->product_id;
                            $submit_user_invite['type'] = $userProductDetail['data']->type;
                            if (!empty($userProductDetail['data']->start_at)) $submit_user_invite['start_at'] = $userProductDetail['data']->start_at;
                            if (!empty($userProductDetail['data']->expired_at)) $submit_user_invite['expired_at'] = $userProductDetail['data']->expired_at;
                            $submit_user_invite['user_parent_id'] = $user_id;
                            $submit_user_invite['email'] = $email[$i];
                            $submit_user_invite['wife_or_husband'] = $couple[$i];
                            $submit_user_invite['promo'] = $userProductDetail['data']->promo;
                            $submit_user_invite['promo_description'] = $userProductDetail['data']->promo_description;

                            $sender['id'] = $user_id;
                            $sender['email'] = Auth::user()->email;

                            $receiver['email'] = $email[$i];

                            $this->userProductService->create($submit_user_invite, $sender, $receiver, true);
                            
                            // $submit_user_invite['user_id'] = $user_id;
                            // $submit_user_invite['product_id'] = $userProductDetail['data']->product_id;
                            // $submit_user_invite['status'] = 'pending';
                            // $submit_user_invite['email'] = $email[$i];
                            // $submit_user_invite['wife_or_husband'] = $couple[$i];

                            // $param = array();
                            // $param['data'] = $submit_user_invite;
                            // $param['sender'] = array('id' => $user_id, 'email' => Auth::user()->email);
                            // $param['receiver'] = array('email' => $email[$i]);

                            // $this->userInviteService->create($param);
                        }
                    }
                }
            }
            \DB::commit();
        }catch (\Exception $ex){
            \DB::rollBack();
        }
        if (count($message) > 0){
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', json_encode($message));
            return redirect('profile/invite');
        }else{
            return redirect('profile/payment');
        }
    }

    public function registerProduct(Request $request){
        // $sender = Crypt::decrypt($request->sender);
        // $receiver = Crypt::decrypt($request->receiver);
        // $id = Crypt::decrypt($request->id);

        // return view('product-register', array('sender' => $sender, 'receiver' => $receiver, 'id' => $id));
        return view('product-register');
    }

    public function registerSubmit(Request $request){
        if ($request->get('email') != $request->get('confirmemail')){
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Email anda tidak sesuai.');
            return redirect()->back();//->with('query', $request->all());
        }

        $register['email'] = $request->get('confirmemail');
        $register['name'] = $request->get('fullname');
        $register['password']= $request->get('password');
        $register['gender'] = $request->get('gender');

        \DB::beginTransaction();
        try{
            $this->userService->create($register, 201);
            DB::commit();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Akun anda berhasil dibuat! Silahkan cek email anda untuk mengaktifkan akun anda.');
        }catch (Exception $ex){
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, anda gagal membuat akun.');
            Log::debug($ex->getMessage());
            DB::rollBack();
        }

        return redirect()->back();
    }

    private function promoStatus(){
        return true;
    }

    private function RandomUniqueCode($length){
        //$valid_chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
        $valid_chars = '1234567890';
       
        $random_string = "";
        $num_valid_chars = strlen($valid_chars);

        for ($i = 0; $i < $length; $i++) {
            $random_pick = mt_rand(1, $num_valid_chars);
            $random_char = $valid_chars[$random_pick - 1];
            $random_string.= $random_char;
        }
        return $random_string;
    }

    private function UniqueCode($amount){
        $result = '';
        $today = date("Y-m-d");
        $orders = $this->repositories['user_order']->getModel()
           
            ->where('total_amount', $amount)
            ->get();
        
        $blnBreak = false;
    
        while(true){
            $digit = substr($amount, -3);
            if ($digit == '000'){
                $unique = $this->RandomUniqueCode(3);
            }else{
                $unique = $this->RandomUniqueCode(2);
            }

            if ($unique == '000' || $unique == '00'){
                continue;
            }

            $blnContinue = true;
            if (empty($orders) || count($orders) == 0){
                $blnContinue = false;
            }else{
                foreach ($orders as $key => $val){
                    if ($val->unique_code == $unique){
                        $blnContinue = true;
                        break;
                    }else{
                        $blnContinue = false;
                    }
                }
            }
            if ($blnContinue == false){
                $result = $unique;
                break;
            }else{
                continue;
            }
        }
        return $result;
    }

    public function paymentIndex(Request $request){
        $user_id = Auth::user()->id;
        $checkProduct = $this->userProductService->checkProductSubscription($user_id);
        $bank_accounts = $this->bankAccountService->getAll();
        $price = $checkProduct['data']->product->price;
        $promo = $checkProduct['data']->promo;
        $years = $checkProduct['data']->years;
        $amount = $price * $years;
        $total_amount = $amount;
        $discount = 0;
        if ($years == true && $years > 1){
            $total_amount = $amount - ($price / 2);
            $discount = $price / 2;
        }
        $unique = $this->UniqueCode($total_amount);

        return view('product-payment', array(
            'bank_accounts' => $bank_accounts,
            'user_products' => $checkProduct,
            'promo_status' => $promo,
            'amount' => $amount,
            'total_amount' => $total_amount + $unique,
            'unique' => $unique,
            'discount' => $discount 
        ));
    }

    public function paymentSubmit(Request $request){
        $user_id = Auth::user()->id;
        $checkProduct = $this->userProductService->checkProductSubscription($user_id);
        $price = $checkProduct['data']->product->price;
        $promo = $checkProduct['data']->promo;
        $years = $checkProduct['data']->years;

        $amount = $price * $years;
        $total_amount = $amount;
        
        if ($years == true && $years > 1){
            $total_amount = $amount - ($price / 2);
            
        }
        $unique = $this->UniqueCode($total_amount);

        $fileName;

        $order_submit['user_product_id'] = $checkProduct['data']->id;
        $order_submit['bank_account_id'] = $request->input('bank_account_id');
        $order_submit['amount'] = $request->input('amount');
        $order_submit['total_amount'] = $request->input('total_amount');
        //$order_submit['promo'] = $promo;
        $order_submit['status'] = 'pending';
        $order_submit['unique_code'] = $request->input('unique_code');

        if ($request->hasFile('image_invoice_product')){
            if ($request->file('image_invoice_product')->isValid()){
                $fileName = rand(11111, 99999) . '.' . $request->file('image_invoice_product')->getClientOriginalExtension();
                $order_submit['attachment'] = $fileName;
            }
        }
        \DB::beginTransaction();
        try{
            $this->userOrderService->create($order_submit);
            if ($fileName){
                $request->file('image_invoice_product')->move("invoice", $fileName);
            }
            \DB::commit();
            return redirect('/tax-filling');
        }catch (\Exception $ex){
            \DB::rollBack();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', $ex->getMessage());
            Log::debug($ex->getMessage());
            return redirect('/profile/payment');
        }

    }

}