<?php

$router->get('/home', 'UserProfileController@dashboard');

$router->prefix('profile')
       ->group(function() use ($router) {
	       $router->get( '/me', 'UserProfileController@index' );
		   $router->post( '/me', 'UserProfileController@update' );
		   $router->post('/ajax/securityquestion', 'UserProfileController@passwordRequest');
		   $router->get('/payment', 'UserProductController@paymentIndex');
		   $router->post('/payment', 'UserProductController@paymentSubmit');

	   });


/**
 * End of file
 */

/**
 * End of file
 */