<?php

namespace WebApp\UserProfile\Requests;

use Infrastructure\Http\WebRequest;

class UserProfileRequest extends WebRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'gender' => 'required|string',
            'birthdate' => 'required|date_format:d/m/Y',
            'mobilenumber' => 'required|numeric',
            'securityquestion1' => 'required|integer',
            'securityanswer1' => 'required|string',
            'securityquestion2' => 'required|integer',
            'securityanswer2' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'fullname',
            'gender' => 'gender',
            'birthdate' => 'birth date',
            'mobilenumber' => 'mobile number',
            'homenumber' => 'home number',
            'faxnumber' => 'fax number',
            'securityanswer1' => 'security question answer 1',
            'securityanswer2' => 'securityanswer answer 2',
        ];
    }
}
