<?php

namespace WebApp\UserProduct\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infrastructure\Http\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Api\Products\Services\ProductService;
use Api\Products\Repositories\ProductRepository;

use Api\UserProducts\Services\UserProductService;
use Api\UserProducts\Repositories\UserProductRepository;

use Api\Users\Services\UserService;
use Api\Users\Repositories\UserRepository;

use Api\UserOrders\Services\UserOrderService;
use Api\UserOrders\Repositories\UserOrderRepository;

class UserProductController extends Controller{
	private $productService;
    private $userProductService;
    private $userService;
    private $userOrderService;
	
    private $repositories;

    public function __construct(ProductService $productService, 
            UserProductService $userProductService, UserService $userService, 
            UserOrderService $userOrderService){
		$this->middleware('auth');
        $this->productService = $productService;
        $this->userProductService = $userProductService;
        $this->userService = $userService;
        $this->userOrderService = $userOrderService;

        $this->repositories = [
            'product' => new ProductRepository(),
            'user_product' => new UserProductRepository(),
            'user' => new UserRepository(),
            'user_order' => new UserOrderRepository()
        ];
	}
    
    public function index(){
        $subscription = $this->userProductService->productSubscription('user_id', Auth::user()->id);
        $product = $this->repositories['product']->getModel()->where('invites', '>', 0)->get();
        
        return view('user-product', ['products' => empty($product) ? [] : $product->toArray(), 'subscription' => $subscription]);
    }

    public function productSubscription(){
        $data = $this->getProductSubscription();
        return view('user-product', $data);
    }

    public function selectProduct(Request $request){
        $parameter = [
            'product_id'        => $request->get('product_id'),
            'promo'             => $request->get('promo'),
            'promo_description' => $request->get('promo_description'),
            'user_id'           => Auth::user()->id,
            'years'             => $request->get('promo') == 1 ? 2 : 1,
            'wife_or_husband'   => 0,
            'type'              => 'accepted'
        ];

        \DB::beginTransaction();
        try{
            $subscription = $this->userProductService->productSubscription('user_id', Auth::user()->id);
            if (count($subscription) > 0){
                $this->userProductService->deleteByParent($subscription['id']);
                $this->userProductService->update($subscription['id'], $parameter);
            }else{
                $this->userProductService->create($parameter);
            }
            \DB::commit();
            return response()->json(['status' => true]);
        }catch (\Exception $ex){
            return response()->json(['status' => false, 'message' => $ex->getMessage()]);
            \DB::rollBack();
        }

        return response()->json($parameter);
    }

    public function checkProduct(){
        $userId = Auth::user()->id;
        $status = 0;
        $subscription = $this->userProductService->productSubscription('user_id', $userId);

        if (count($subscription) > 0){
            if (!is_null($subscription['parent_id'])){
                $status = 1;
            }
    
            if ($subscription['user_order']['status'] == 'paid'){
                $status = 2;
            }
        }
        return response()->json($status);
    }

    public function deleteUserProduct(Request $request){
        $user_product_id = $request->get('user_product_id');
        try{
            $delete = $this->userProductService->delete($user_product_id);

            return response()->json(true);
        }catch (\Exception $ex){
            return response()->json(false);
        }
    }

    public function inviteIndex(){
        $product_subscription = $this->userProductService->userProductSubscription(Auth::user()->id);
        $product_subscription['data']->product->invites;
        if ($product_subscription['status'] == false || $product_subscription['data']->product->invites == 1){
            return redirect('/product');
        }else{
            return view('user-invite', ['data' => $product_subscription]);
        }
    }

    public function inviteFriend(Request $request){
        $email = $request->get('email');
        $spouse = $request->get('spouse');

        try{
            $product_subscription = $this->userProductService->userProductSubscription(Auth::user()->id);
            if ( (((int)$product_subscription['data']->Product->invites - 1) - count($product_subscription['partners'])) == 0  ){
                return response()->json(['status' => false, 'message' => "Maaf, Kapasitas teman/keluarga sudah penuh. Anda tidak dapat melakukan invite."]);
            }

            $check_email = $this->userProductService->checkEmail($email);
            if ($check_email == true){
                return response()->json(['status' => false, 'message' => "Maaf, Email sudah terdaftar di produk SPT GO."]);
            }

            $submit_user_invite['parent_id'] = $product_subscription['data']->id;
            $submit_user_invite['years'] = $product_subscription['data']->years;
            $submit_user_invite['product_id'] = $product_subscription['data']->product_id;
            $submit_user_invite['type'] = $product_subscription['data']->type;
            if (!empty($product_subscription['data']->start_at)) $submit_user_invite['start_at'] = $product_subscription['data']->start_at;
            if (!empty($product_subscription['data']->expired_at)) $submit_user_invite['expired_at'] = $product_subscription['data']->expired_at;
            $submit_user_invite['user_parent_id'] = Auth::user()->id;
            $submit_user_invite['email'] = $email;
            $submit_user_invite['wife_or_husband'] = $spouse;
            $submit_user_invite['promo'] = $product_subscription['data']->promo;
            $submit_user_invite['promo_description'] = $product_subscription['data']->promo_description;
            $this->userProductService->create($submit_user_invite);

            return response()->json(['status' => true, 'message' => 'Invitation has been sent to '.$email.'.', 'data' => $product_subscription]);

        }catch (\Exception $ex){
            return reponse()->json(['status' => false, 'message' => 'Maaf, gagal melakukan invite SPT GO.']);
        }
    }
}