@extends('layout.main')
@section('content')
<section id="user-product-invite" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray">Invite a Friend</h3>
                    <!-- <div class="boxed-content">Terima kasih telah bergabung bersama kami.<br>Untuk melanjutkan ketahap selanjutnya, silahkan pilih produk dibawah ini sesuai dengan yang anda butuhkan.</div> -->
                </div>

                <form id="user-product-invite" method="post">
                    <div class="boxed-body w-100 text-left">
                        <div class="container">
                            @if(session()->has('message.level'))
                                <div class="alert alert-{{ session('message.level') }}">
                                    {!! session('message.content') !!}
                                </div>
                            @endif
                            <div class="boxed-header max-w-sm mx-auto tab-content">
                                <div class="alert alert-success" role="alert">
                                    <div id="invites-rest">
                                        <strong>You have {{ ((int)$data['data']->product->invites - 1) - count($data['partners'])  }} invites left.</strong>
                                    </div>
                                    <hr>
                                    <p>To invite a new user, simply type in the email address of the lucky one below and we will take care of the rest.</p>
                                </div>

                                <p id="success-message" style="display:none;"></p>
                                <p id="error-message" style="display:none;"></p>

                                <div id="invitation-layer" style="display:none;">
                                    <div class="form-group row">
                                    
                                        <div class="col-sm-8">
                                            <input type="text" name="email-friend" id="email-friend" value="" 
                                                class="form-control" placeholder="Email">
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="custom-control custom-checkbox" style="margin-top: 2px;">
                                                <input type="checkbox"
                                                    class="custom-control-input" 
                                                    name="myspouse" id="myspouse">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description" style="margin-top: 5px; font-size: 11px;">Istri/Suami Saya</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <button type="submit" id="send-invitation" class="btn btn-info btn-sm" style="display: none;">Send Invitation</button>
                                        <a class="btn btn-info btn-sm" href="javascript:void(0)" id="send-another">Send Another One</a>
                                    <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection