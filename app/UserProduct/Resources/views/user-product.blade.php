@extends('layout.main')
@section('content')
<div id="product-change" style="{{ count($subscription) > 0 ? 'display:none;' : '' }}">
    <section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
        <div class="row">
            <div class="col-md-12">
                <div class="boxed boxed-lg boxed--aside text-center">
                    <div class="boxed-header max-w-sm mx-auto">
                        <h3 class="h2 boxed-title text-gray">Pilih Produk</h3>
                        <div class="boxed-content">Terima kasih telah bergabung bersama kami.<br>Untuk melanjutkan ketahap selanjutnya, silahkan pilih produk dibawah ini sesuai dengan yang anda butuhkan.</div>
                    </div>

                    <form id="user-product" method="post">
                        {{ csrf_field() }}
                        <div class="boxed-body w-100 text-left"> 
                            <div class="container">
                                @if(session()->has('message.level'))
                                    <div class="alert alert-{{ session('message.level') }}">
                                        {!! session('message.content') !!}
                                    </div>
                                @endif
                                <div class="col-md-12 btn-group-select btn-group-single-select-5">
                                    <div class="row" align="center">
                                    @forelse($products as $product)
                                    <div class="col-md-4">    
                                    <div class="product-card" align="center">
                                            <div class="product-card-header">
                                            <h3 class="product-card-title">{{ $product['name'] }}</h3>
                                            </div>
                                            <div class="price-wrap">
                                                <span class="product-price">{{ $product['price'] }}</span>
                                            </div>
                                            <div class="product-card-footer">
                                                <div class="product-list">
                                                        @foreach(json_decode($product['description']) as $value)
                                                            <li>{{ $value }}</li>
                                                        @endforeach
                                                </div>
                                                <button data-value="{{ $product['id'] }}" 
                                                    data-promo="0" data-promo-description="" type="button" 
                                                    class="btn btn-outline-info btn-choose" 
                                                    >Choose</button>
                                            </div>
                                        </div>
                                        <!--<hr>
                                        <div class="row">
                                            <div class="text-center col-md-2">
                                                <button data-value="{{ $product['id'] }}" 
                                                    data-promo="0" data-promo-description="" type="button" 
                                                    class="btn btn-lg btn-outline-info" 
                                                    style="width: 100%; padding-left: 0px; padding-right: 0px;">
                                                        <p><b>{{ $product['name'] }}</b></p>
                                                        <p><b>{{ $product['price'] }}</b></p>
                                                </button>
                                            </div>
                                            <div class="col-md-7">
                                                <ul>
                                                    @foreach(json_decode($product['description']) as $value)
                                                        <li>{{ $value }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                @if ($product['promo'] == 1)
                                                <button data-value="{{ $product['id'] }}"
                                                data-promo="1" data-promo-description="{{ $product['promo_description'] }}"
                                                    type="button" class="btn btn-lg btn-outline-danger" 
                                                    style="width:100%; white-space: pre-wrap; padding-left: 12px; padding-right: 12px; padding-top: 0px; padding-bottom: 0px; font-size : 10px; font-weight: normal;">
                                                        <p>{{ $product['promo_description'] }}</p>
                                                </button>
                                                @endif
                                            </div>
                                        </div>
                                        <br />-->
</div>
                                    @empty
                                    @endforelse
                                </div>
                                </div>
                                <input type="hidden" name="productid" id="productid" value="">
                                <input type="hidden" name="promo" id="promo" value="0">
                                <input type="hidden" name="producttype" id="producttype" value="pending">
                                <div class="row">
                                    <hr class="col-md-12">
                                    <div class="col-md-12" align="center">
                                        <a href="{{ url('/tax-filling?redirect=true') }}" class="btn btn-info btn-lg">USE NOW PAY LATER</a>
                                        <br><br><small>1770SS Gets full free access </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div id="product-subscription" style="{{ count($subscription) > 0 ? '' : 'display:none;' }}">
    <section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
        <div class="row">
            <div class="col-md-12">
                    <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-gray text-center">Product Subscription</h3>
                    </div>
                    
                    <div class="boxed-body w-100 text-left">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-2">
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                            Nama Produk
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                                {{ isset($subscription['product']['name']) ? $subscription['product']['name'] : '' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Sisa Quota
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                        {{ isset($subscription['product']['invites']) ? (int)$subscription['product']['invites'] - 1 : '0' }} {{ 'Orang' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Deskripsi Produk
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                        @if (isset($subscription['product']['description']))
                                            @foreach (json_decode($subscription['product']['description'], true) as $description)
                                                <li style="padding-bottom:3%">{{ $description }}</li>
                                            @endforeach
                                        @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Status Promo
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                        {{ isset($subscription['promo']) ? ($subscription['promo'] == 1 ? 'Ya' : 'Tidak') : '' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Deskripsi Promo
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                        {{ isset($subscription['promo_description']) ? $subscription['promo_description'] : '' }}
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Status Pembayaran
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                            @if (isset($subscription['user_order']['status']))
                                                @if ($subscription['user_order']['status'] == 'paid')
                                                    <span class="badge badge-danger">Paid</span>
                                                @else
                                                    <span class="badge badge-info">Pending</span>
                                                @endif
                                                @else
                                                    -
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="text-md-right text-sm-left col-sm-5 col-form-label col-form-label-sm">
                                        Expired Date
                                        </label>
                                        <div class="col-sm-4 exc6">
                                            <div class="form-control form-control-sm">
                                        {{ !isset($subscription['expired_at']) ? '' :date('d-m-Y', strtotime($subscription['expired_at'])) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if (count($subscription) > 0)
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                            @foreach ($subscription['partner'] as $row)
                                            <tr id="email-subscription-{{ $row['id'] }}">
                                            <td width="70%">{{ $row['email'] }}</td>
                                            <td width="20%">
                                                @if ($row['wife_or_husband'] == 1)
                                                    Istri/Suami Saya
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td width="10%"> <a href="javascript:void(0)" class="btn btn-danger btn-sm remove-partner-subscription" data-id="{{ $row['id'] }}"> <i class="fa fa-trash"></i> </a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <hr>
                                    <div class="col-md-12 center" align="center">
                                    @if ($subscription['product']['invites'] > 1)
                                        <a class="btn btn-info btn-lg btn-3" href="{{ url('/product/invite') }}" style="min-width:200px;">Send an Invitation</a>
                                    @endif
                                    @if (empty($subscription['user_orer']))
                                        <a class="btn btn-danger btn-lg change-product-sptgo btn-3" href="javascript:void(0)" style="min-width:200px;">Change Product</a>
                                        <a class="btn btn-warning btn-lg btn-3" href="javascript:void(0)" style="min-width:200px">Pay now</a>
                                    @endif
                                    </div>
                                @endif
                                    <!--<div class="alert alert-success" role="alert">
                                        <hr>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td width="30%">Nama Produk</td>
                                                    <td>:</td>
                                                    <td width="70%">{{ isset($subscription['product']['name']) ? $subscription['product']['name'] : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Sisa Quota</td>
                                                    <td>:</td>
                                                    <td width="70%">{{ isset($subscription['product']['invites']) ? (int)$subscription['product']['invites'] - 1 : '0' }} {{ 'Orang' }} </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Deskripsi Produk</td>
                                                    <td>:</td>
                                                    <td width="70%">
                                                        <ul>
                                                            @if (isset($subscription['product']['description']))
                                                                @foreach (json_decode($subscription['product']['description'], true) as $description)
                                                                    <li style="margin-left: -25px;">{{ $description }}</li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Status Promo</td>
                                                    <td>:</td>
                                                    <td width="70%">{{ isset($subscription['promo']) ? ($subscription['promo'] == 1 ? 'Ya' : 'Tidak') : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Deskripsi Promo</td>
                                                    <td>:</td>
                                                    <td width="70%">{{ isset($subscription['promo_description']) ? $subscription['promo_description'] : '' }}</td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Status Pembayaran</td>
                                                    <td>:</td>
                                                    <td width="70%">
                                                        @if (isset($subscription['user_order']['status']))
                                                            @if ($subscription['user_order']['status'] == 'paid')
                                                                <span class="badge badge-danger">Paid</span>
                                                            @else
                                                                <span class="badge badge-info">Pending</span>
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="30%">Expired Date</td>
                                                    <td>:</td>
                                                    <td width="70%">{{ !isset($subscription['expired_at']) ? '' :date('d-m-Y', strtotime($subscription['expired_at'])) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        @if (count($subscription) > 0)
                                            <br>
                                            <h4 class="alert-heading text-center"></h4>
                                            <hr>
                                            <table class="table table-bordered">
                                                <tbody>
                                                    @foreach ($subscription['partner'] as $row)
                                                        <tr id="email-subscription-{{ $row['id'] }}">
                                                            <td width="70%">{{ $row['email'] }}</td>
                                                            <td width="20%">
                                                                @if ($row['wife_or_husband'] == 1)
                                                                    Istri/Suami Saya
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>
                                                            <td width="10%"> <a href="javascript:void(0)" class="btn btn-danger btn-sm remove-partner-subscription" data-id="{{ $row['id'] }}"> <i class="fa fa-trash"></i> </a></td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <hr>
                                            @if ($subscription['product']['invites'] > 1)
                                                <a class="btn btn-info btn-lg" href="{{ url('/product/invite') }}" style="min-width:200px;">Send an Invitation</a>
                                            @endif

                                            @if (empty($subscription['user_orer']))
                                                <a class="btn btn-danger btn-lg change-product-sptgo" href="javascript:void(0)" style="min-width:200px;">Change Product</a>
                                                <a class="btn btn-warning btn-lg" href="javascript:void(0)" style="min-width:200px">Pay now</a>
                                            @endif
                                        @endif

                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>
</div>
@endsection
