<?php
$router->prefix('product')
    ->group(function() use ($router){
        $router->get('/', 'UserProductController@index');
        $router->get('/subscription/', 'UserProductController@productSubscription');
        $router->post('/select', 'UserProductController@selectProduct');
        $router->post('/check', 'UserProductController@checkProduct');
        $router->delete('/delete', 'UserProductController@deleteUserProduct');

        $router->get('/invite', 'UserProductController@inviteIndex');
        $router->post('/invite', 'UserProductController@inviteFriend');
        $router->post('/invite/spouse', 'UserProductController@inviteSpouse');
    });
