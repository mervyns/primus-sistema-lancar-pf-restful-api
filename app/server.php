<?php
require_once 'vendor/autoload.php';

use Hoa\Websocket\Server as HoaServer;
use Hoa\Socket\Server as SocketServer;
use Hoa\Websocket\Connection as HoaConnection;

$websocket = new HoaServer(
    new SocketServer('ws://127.0.0.1:8889')
);
$websocket->on('open', function (Hoa\Event\Bucket $bucket) {
    echo 'new connection', "\n";

    return;
});

$websocket->on('message', function (Hoa\Event\Bucket $bucket) {
    $data = $bucket->getData();
    $currentNode =  $bucket->getSource()->getConnection()->getCurrentNode();
    $currentNodeId = $currentNode->getId();
    echo $currentNodeId;
    echo '> message ', $data['message'], "\n";
    $bucket->getSource()->broadcast($data['message']);
    echo '< echo', "\n";

    return;
});
$websocket->on('close', function (Hoa\Event\Bucket $bucket) {
    echo 'connection closed', "\n";

    return;
});
$websocket->run();
