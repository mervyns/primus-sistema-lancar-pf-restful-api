<?php

namespace WebApp\Home;

use Illuminate\Foundation\Application;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use Api\Users\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class LoginProxy
{
    private $auth;

    private $cookie;

    private $db;

    private $request;

    private $userRepository;

    public function __construct(Application $app, UserRepository $userRepository) {
        $this->userRepository = $userRepository;

        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     */
    public function attemptLogin($email, $password)
    {
	    if (Auth::attempt(['email' => $email, 'password' => $password])) {
	    	return Auth::user();
        }

        throw new InvalidCredentialsException();
    }

    public function attemptPassword($email, $password) {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
	    	return true;
        }
        return false;
    }

    public function logout()
    {
		Auth::logout();
    }
}
