<?php

namespace WebApp\Home\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Infrastructure\Http\Controller;
use Infrastructure\Auth\Exceptions\InvalidCredentialsException;
use WebApp\Home\LoginProxy;
use WebApp\Home\Requests\RegistrationRequest;
use WebApp\Home\Requests\LoginRequest;
use Api\Users\Services\UserService;
use Api\UserProducts\Services\UserInviteService;
use Api\UserProducts\Services\UserProductService;
use Auth;
class MainController extends Controller
{
    private $userService;
	private $loginProxy;
    private $userInviteService;
    private $userProductService;

    public function __construct(UserService $userService, LoginProxy $loginProxy, UserInviteService $userInviteService, userProductService $userProductService)
    {
	    $this->middleware('guest', ['except' => 'logout']);
        
        $this->userService = $userService;
	    $this->loginProxy = $loginProxy;
        $this->userInviteService = $userInviteService;
        $this->userProductService = $userProductService;
    }

    public function index()
    {
		return view('home');
    }

    public function productRegister(Request $request){

        $sender = Crypt::decrypt($request->sender);
        $receiver = Crypt::decrypt($request->receiver);
        $idsender = Crypt::decrypt($request->idsender);
        $idreceiver = Crypt::decrypt($request->idreceiver);

        //dd($sender, $receiver, $idsender, $idreceiver);

        $checkUserRegister = $this->userService->checkUserExits($receiver);
        if ($checkUserRegister){
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Anda telah menerian undangan dari '.$sender.'. Silahkan login untuk melanjutkan.');
            return redirect('login');
        }else{
            return view('product-register', 
                array('sender' => $sender, 'receiver' => $receiver, 'idsender' => $idsender, 'idreceiver' => $idreceiver)
            );
        }
    }

    public function productSubmit(Request $request){
        if ($request->get('email') != $request->get('confirmemail')){
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Email anda tidak sesuai.');
            return redirect()->back();//->with('query', $request->all());
        }

        try{
            $checkProduct = $this->userProductService->checkUserProduct($request->get('idsender'));

            $product['user_id'] = $request->get('idreceiver');
            $product['product_id'] = $checkProduct['data']->product_id;
            $product['type'] = $checkProduct['data']->type;
            $product['email'] = $request->get('confirmemail');
            $product['user_parent_id'] = $checkProduct['data']->user_id;

            if (!empty($checkProduct['data']->years)) $product['years'] = $checkProduct['data']->years;
            if (!empty($checkProduct['data']->start_at)) $product['start_at'] = $checkProduct['data']->start_at;
            if (!empty($checkProduct['data']->expired_at)) $product['expired_at'] = $checkProduct['data']->expired_at;

            $register['email'] = $request->get('confirmemail');
            $register['name'] = $request->get('fullname');
            $register['password']= $request->get('password');
            $register['gender'] = $request->get('gender');

            $invite['status'] = 'done';

            //dd($checkProduct, $product, $register, $invite, $request->get('idreceiver'));

            $this->userService->create($register);
            $this->userInviteService->update($request->get('idreceiver'), $invite);
            $this->userProductService->create($product);

            \DB::commit();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Akun anda berhasil dibuat! Silahkan cek email anda untuk mengaktifkan akun anda.');
        }catch (\Exception $ex){
            \DB::rollBack();
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Maaf, anda gagal membuat akun.');
            Log::debug($e->getMessage());
        }

        return redirect()->back();
    }

    public function doRegister(RegistrationRequest $request)
    {
    	$data['email'] = $request->get('email');
    	$data['name'] = $request->get('name');
    	$data['password']= $request->get('password');
    	$data['gender'] = $request->get('gender');

    	try {
		  	$this->userService->create($data, 201);
		    $is_failed = 0;
		}
		catch (\Exception $e) {
    		Log::debug($e->getMessage());
		    $is_failed = 1;
		}

    	if ($is_failed == 1) {
	        $request->session()->flash('message.level', 'danger');
	        $request->session()->flash('message.content', 'Maaf, anda gagal membuat akun.');
	    } else {
	        $request->session()->flash('message.level', 'success');
	        $request->session()->flash('message.content', 'Akun anda berhasil dibuat! Silahkan cek email anda untuk mengaktifkan akun anda.');
	    }

	    return redirect('/');

    }

    public function login()
    {
		return view('login');
    }

	public function doLogin(LoginRequest $request)
	{
        
		try {
			$email    = $request->get( 'email' );
			$password = $request->get( 'password' );

            $user = $this->loginProxy->attemptLogin( $email, $password );
            
            $request->session()->put('app.user', $user->toArray());
            
            if (empty($user->gender) || 
                empty($user->birthdate) || empty($user->homenumber) || 
                empty($user->mobilenumber) || empty($user->faxnumber)){

                if ( is_null( $user->last_logged_in_at ) ) {
                    $user->last_logged_in_at = date( 'Y-m-d H:i:s' );
                    $user->save();
                }
                return redirect( '/profile/me' );
            }
            return redirect('/home');
		} catch (InvalidCredentialsException $e) {
			Log::debug($e->getMessage());

			$request->session()->flash('message.level', 'danger');
			$request->session()->flash('message.content', 'Email dan password Anda salah.');

			return redirect('/login');
		}
	}

    public function forgotPassword()
    {
		return view('forgot-password');
    }

    public function doForgotPassword(Request $request){

    }

    public function activate()
    {
        $encrypted_id = request()->id;
        $false = 1;

        try {
            $uID = Crypt::decrypt($encrypted_id);
            $false = 0;
        }
        catch (\Exception $e) {
	        Log::debug($e->getMessage());
        }

        if($false == 0){
            try {
                $this->userService->activate($uID);
                $is_failed = 0;
            }
            catch (\Exception $e) {
                $is_failed = 1;
            }
        } else {
            $is_failed = 1;
        }

        if ($is_failed == 1) {
            session()->flash('message.level', 'danger');
            session()->flash('message.content', 'Maaf, anda gagal aktivasi akun.');
        } else {
            session()->flash('message.level', 'success');
            session()->flash('message.content', 'Akun anda berhasil diaktivasi! Silahkan login untuk mulai menggunakan SPTGO.');
        }

        return redirect('/login/');
    }

    public function logout()
    {
        $this->loginProxy->logout();
        session()->flash('message.level', 'success');
        session()->flash('message.content', 'You are now signed out. Thank you for using SPT GO.');
    	return redirect('/login/');
    }

    public function unauthorized()
    {
	    return view('unathorized');
    }

}
