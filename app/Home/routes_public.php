<?php
$router->middleware('web')
	->group(function() use ($router) {
		$router->get( '/', 'MainController@index' );
		$router->get('/login', 'MainController@login');
		$router->post('/login', 'MainController@doLogin');
		$router->post('/register', 'MainController@doRegister');
		$router->get('/activate', 'MainController@activate');
		$router->get('/forgot-password', 'MainController@forgotPassword');
		$router->post('/forgot-password', 'MainController@doForgotPassword');
		$router->get('/unauthorized', 'MainController@unauthorized');
		$router->get('/invite', 'MainController@productRegister');
		$router->post('/invite', 'MainController@productSubmit');
	});

// $router->middleware([])->group(function() use ($router) {
// 		$router->get( '/', 'MainController@index' );
// 		$router->get('/login', 'MainController@login');
// 		$router->post('/login', 'MainController@doLogin');
// 		$router->post('/register', 'MainController@doRegister');
// 		$router->get('/activate', 'MainController@activate');
// 		$router->get('/forgot-password', 'MainController@forgotPassword');
// 		$router->get('/unauthorized', 'MainController@unauthorized');
// 		$router->get('/invite', 'MainController@productRegister');
// 		$router->post('/invite', 'MainController@productSubmit');
// 	});

/**
 * End of file
 */
