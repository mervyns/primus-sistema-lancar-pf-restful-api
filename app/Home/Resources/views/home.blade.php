@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light">
    <div class="row">
        <div class="col-md-5">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}">
                            {!! session('message.content') !!}
                        </div>
                    @endif
                    <h3 class="h2 boxed-title text-gray">Buat Akun Baru</h3>
                    <div class="boxed-content">Mulai dengan mengisi beberapa informasi diri Anda.</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-2">
                                <form id="registration" class="form-horizontal" method="post" action="{{ url('/register') }}">
                                    {{ csrf_field() }}
                                    <div class="float form-group">
                                        <div class="col-md-12">
                                            <div>
                                                <label class="form-control-label">Email</label>
                                                <input type="text" name="email" value="" class="form-control" placeholder="Email" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float form-group">
                                        <div class="col-md-12">
                                            <div>
                                                <label class="form-control-label">Password</label>
                                                <input type="password" name="password" value="" class="form-control" placeholder="Password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float form-group">
                                        <div class="col-md-12">
                                            <div>
                                                <label class="form-control-label">Nama Lengkap</label>
                                                <div><input type="text" name="name" value="" class="form-control" placeholder="Nama Lengkap" required></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float form-group">
                                        <div class="container form-group">
                                            <div class="row"><label class="form-control-radio-mainlabel">Gender</label></div>
                                            <div class="row">
                                                <div class="col-md-12"><label class="custom-control custom-radio "><input type="radio" class="custom-control-input" name="gender" value="L" checked><span class="custom-control-indicator"></span><span class="custom-control-description">Laki-laki</span></label></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12"><label class="custom-control custom-radio "><input type="radio" class="custom-control-input" name="gender" value="P"><span class="custom-control-indicator"></span><span class="custom-control-description">Perempuan</span></label></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-md-offset-4"><button type="submit" class="btn btn-success" style="margin-right: 15px;">Buat Akun</button><a style="color: #34ea93; margin-left: 10px;" href="{{ url('/forgot-password') }}">Reset password</a></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-content-part--media col-md-7"><img src="http://placehold.it/772x600" class="js-object-fit object-fit--cover ready"></div>
    </div>
</section>
@endsection