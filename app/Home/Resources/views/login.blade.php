@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light">
    <div class="row">
        <div class="col-md-5">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}">
                            {!! session('message.content') !!}
                        </div>
                    @endif
                    <h3 class="h2 boxed-title text-gray">Masuk SPTGO</h3>
                    <div class="boxed-content">Silahkan Isi Informasi Kredensial Anda.</div>
                </div>
                <div class="boxed-body w-100 text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-2">
                                {{--@if ($errors->any())--}}
                                    {{--<div class="alert alert-danger">--}}
                                        {{--<ul>--}}
                                            {{--@foreach ($errors->all() as $error)--}}
                                                {{--<li>{{ $error }}</li>--}}
                                            {{--@endforeach--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                                <form class="form-horizontal" role="form" method="post" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    <div class="float form-group">
                                        <div class="col-md-12">
                                            <div>
                                                <label class="form-control-label">Email</label>
                                                <div><input type="email" name="email" value="" class="form-control" placeholder="Email" required></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="float form-group">
                                        <div class="col-md-12">
                                            <div>
                                                <label class="form-control-label">Password</label>
                                                <div><input type="password" name="password" value="" class="form-control" placeholder="Password" required></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-md-offset-4">
                                            <button type="submit" class="btn btn-success" style="margin-right: 15px;">MASUK</button>
                                            <a href="/forgot-password">Lupa Password?</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-content-part--media col-md-7"><img src="http://placehold.it/772x600" class="js-object-fit object-fit--cover ready"></div>
    </div>
</section>
@endsection