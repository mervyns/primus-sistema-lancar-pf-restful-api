@extends('layout.main')

@section('content')
<section id="create-an-account" class="section section--secondary pos-relative style-right bg-light main-app-section">
    <div class="row">
        <div class="col-md-12">
            <div class="boxed boxed-lg boxed--aside text-center">
                <div class="boxed-header max-w-sm mx-auto">
                    <h3 class="h2 boxed-title text-danger">Unauthorized</h3>
                    <div class="boxed-content text-danger">Anda tidak diijinkan untuk mengakses halaman ini.</div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection