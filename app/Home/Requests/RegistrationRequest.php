<?php

namespace WebApp\Home\Requests;

use Infrastructure\Http\WebRequest;

class RegistrationRequest extends WebRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|string',
            'password' => 'required',
            'gender' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'email',
            'name' => 'fullname',
            'password' => 'password',
            'gender' => 'gender',
        ];
    }
}
