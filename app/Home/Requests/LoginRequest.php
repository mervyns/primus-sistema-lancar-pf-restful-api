<?php

namespace WebApp\Home\Requests;

use Infrastructure\Http\WebRequest;

class LoginRequest extends WebRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'    => 'required|email',
            'password' => 'required'
        ];
    }
}
