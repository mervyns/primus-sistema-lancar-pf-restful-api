<?php

$router->post('/api/login', 'LoginController@login');
$router->post('/api/login/refresh', 'LoginController@refresh');