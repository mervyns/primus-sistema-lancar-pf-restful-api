<?php

namespace Infrastructure\Auth\Middleware;

use Closure;

class CheckAuthSession
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
//		dd($request);
		if (!$request->session()->has('users')) {
			return redirect('/login');
		}

		return $next($request);
	}
}