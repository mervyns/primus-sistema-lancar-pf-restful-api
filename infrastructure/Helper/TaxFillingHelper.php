<?php

namespace Infrastructure\Helper;

use Auth;
use Api\Users\Repositories\UserRepository;

class TaxFillingHelper{
    private $repositories;

    public function __construct() {
        $this->repositories = array(
            'user' => new UserRepository
        );
    }

    public function test() {
        return [];
    }

    public function GetAllUserData($userId) {
        $result = $this->repositories['user']->getModel()
            ->with('taxProfile.klus')
            ->with('additionalProfile')
            ->with('income.incomes.taxRateType')
            ->with('income.withHoldingTax')
            ->with('asset.asset')
            ->with('liability.liabilities')
            ->with('dependant.dependants')
            ->with('companion')
            ->with('spouseTaxProfile')
            ->find($userId);
        
        return empty($result) ? [] : $result->toArray();
    }

    public function PTKPList() {
        return array(
            'TK' => array(
                0 => 54000000,
                1 => 58500000,
                2 => 63000000,
                3 => 67500000
            ),
            'K' => array(
                0 => 58500000,
                1 => 63000000,
                2 => 67500000,
                3 => 72000000
            ),
            'K/I' => array(
                0 => 112500000,
                1 => 117000000,
                2 => 121500000,
                3 => 126000000
            )
        );
    }

    public function PTKP($marital_status, $responsibility_status, $total_dependant){
        $value = '';
        $status = '';

        if (empty($total_dependant)) $total_dependant = 0;
        if ($total_dependant > 3) $total_dependant = 3;
        $ptkp = $this->PTKPList();

        if ($marital_status == 2) {
            if ($responsibility_status == 3 || $responsibility_status == 4){
                $status = 'K/I';
            }else{
                $status = 'K';
            }
        }else{
            $status = 'TK';
        }

        $value = $ptkp[$status][$total_dependant];

        return array(
            'status' => $status,
            'value' => $value
        );
    }

    public function ProgressiveSummary($pkp_value) {
        $value = 0;
        $detail = array();
        
        if ($pkp_value >= 0 && $pkp_value <= 50000000) {
            $value = $pkp_value * 0.05;
            $detail = array(
                array(
                    'nominal' => (double)$pkp_value,
                    'value' => (double)$value
                )
            );
        }else if ($pkp_value > 50000000 && $pkp_value <= 250000000) {
            $value = 2500000 + ($pkp_value - 50000000) * 0.15;
            $detail = array(
                array(
                    'nominal' => 50000000,
                    'value' => 2500000
                ),
                array(
                    'nominal' => (double)($pkp_value - 50000000),
                    'value' => (double)$value
                )
            );
        }else if ($pkp_value > 250000000 && $pkp_value <= 500000000){
            $value = 32500000 + ($pkp_value - 250000000) * 0.25;
            $detail = array(
                array(
                    'nominal' => 50000000,
                    'value' => 2500000
                ),
                array (
                    'nominal' => 200000000,
                    'value' => 30000000
                ),
                array (
                    'nominal' => (double)($pkp_value - 250000000),
                    'value' => (double)$value
                )
            );
        } else {
            $value = 95000000 + ($pkp_value - 500000000) * 0.3;
            $detail = array (
                array(
                    'nominal' => 50000000,
                    'value' => 2500000
                ),
                array (
                    'nominal' => 200000000,
                    'value' => 30000000
                ),
                array (
                    'nominal' => 250000000,
                    'value' => 62500000
                ),
                array (
                    'nominal' => (double)($pkp_value - 500000000),
                    'value' => (double)$value
                )
            );
        }
        return array(
            'value' => $value,
            'detail' => $detail
        );
    }

    public function PPh25Netto($data) {
        $list_netto = array(
            1       => 0,   /* penghasilan utama */
            36      => 0,   /* penghasilan pasangan > 1 pemberi kerja */
            6       => 0,   /* Pendapatan Bunga Sehubungan dengan Jaminan Pengembalian Utang */
            45      => 0,   /* Imbalan Kepada Tenaga Ahli */
            54      => 0,   /* penghasilan rutin lainnya */
            5       => 0,   /* penjualan saham pendiri */
            43      => 0,   /* Honorarium Komisaris yang Tidak Merangkap Pegawai Tetap */
            11      => 0,   /* Penghargaan dan hadiah */
            12      => 0,   /* Penghargaan dan hadiah */
            13      => 0,   /* Penghargaan dan hadiah */
            14      => 0,   /* Penghargaan dan hadiah */
            55      => 0,   /* Penghargaan sekali terima lainnya */
            52      => 0,   /* Penghasilan Luar Negeri */
            53      => 0,   /** CHARIY */
        );

        if (!empty($data['income'])) {
            foreach ($data['income'] as $key => $val) {
                $income_id = $val['income_id'];
                if (array_key_exists($income_id, $list_netto)) {
                    $detail = json_decode($val['detail'], true);
                    $kurs = isset($detail['kurs_value']) ? $detail['kurs_value'] : 1;
                    $net = $val['net_value'] == 0 ? $val['gross_value'] : $val['net_value'];
                    if ($income_id == 53) $net = $net * -1;
                    if ($income_id == 52) $net = floor($net * $kurs);
                    $list_netto[$income_id] += $net;
                }
            }
        }

        return $list_netto;
    }

    public function PPh25Holding($data) {
        $list_holding = array(
            1       => 0,   /* penghasilan utama */
            36      => 0,   /* penghasilan pasangan > 1 pemberi kerja */
            6       => 0,   /* Pendapatan Bunga Sehubungan dengan Jaminan Pengembalian Utang */
            45      => 0,   /* Imbalan Kepada Tenaga Ahli */
            54      => 0,   /* penghasilan rutin lainnya */
            43      => 0,   /* Honorarium Komisaris yang Tidak Merangkap Pegawai Tetap */
            11      => 0,   /* Penghargaan dan hadiah */
            12      => 0,   /* Penghargaan dan hadiah */
            13      => 0,   /* Penghargaan dan hadiah */
            14      => 0,   /* Penghargaan dan hadiah */
            55      => 0,   /* Penghargaan sekali terima lainnya */
            46      => 0,   /** Barang Mewah */
            47      => 0,   /** Barang Mewah */
            48      => 0,   /** Barang Mewah */
            49      => 0,   /** Barang Mewah */
            50      => 0,   /** Barang Mewah */
            51      => 0,   /** Barang Mewah */
            52      => 0,   /* Penghasilan Luar Negeri */
        );

        if (!empty($data['income'])) {
            foreach ($data['income'] as $key => $val) {
                $income_id = $val['income_id'];
                if (array_key_exists($income_id, $list_holding)) {
                    $detail = json_decode($val['detail'], true);
                    $kurs = isset($detail['kurs_value']) ? $detail['kurs_value'] : 1;
                    
                    $netto = (double)$val['net_value'];
                    $gross = (double)$val['gross_value'];

                    $tax = $netto == 0 ? 0 : $gross - $netto;

                    if ($income_id == 52) $tax = floor($tax * $kurs);
                    $list_holding[$income_id] += $tax;
                }
            }
        }

        return $list_holding;
    }

    public function PPh24Dikreditkan($data) {
        $result = 0;
        $overseas = array_filter($data['income'], function($arr) {
            return $arr['income_id'] == 52;
        });

        if (count($overseas) > 0 ){
            $overseas_value = 0;
            foreach ($overseas as $row){
                $detail = json_decode($row['detail'], true);
                $netto = (double)$row['net_value'];
                $kurs = (double)$detail['kurs_value'];
                $overseas_value += ($netto * $kurs);
            }
            $pph25 = $this->PPh25Netto($data);
            $ptkp = $this->PTKP($data['tax_profile']['marital_status'], $data['tax_profile']['tax_responsibility_status_id'], count($data['dependant']));
            $pkp = array_sum($pph25) + $overseas_value - $ptkp['value'];
            if ($pkp < 0) $pkp = 0;
            $progresif = $this->ProgressiveSummary($pkp);

            $taxes = 0;
            foreach ($overseas as $row){
                $detail = json_decode($row['detail'], true);
                $kurs = (double)$detail['kurs_value'];
                $netto = (double)$row['net_value'];
                $gross = (double)$row['gross_value'];
                $tara = ($gross - $netto) * $kurs;

                $value = ($netto / $pkp) * $progresif['value'];
                
                if ($tara < $value) {
                    $taxes += $tara;
                }else{
                    $taxes += $value;
                }
            }
            $result = floor($progresif['value'] - $taxes);
        }

        return $result;
    }

    public function CurrencyFormat($num, $dec = 0, $ceknull = false) {
		if($ceknull and !isset($num))
			return null;
		return number_format($num,$dec,',','.');
    }

    public function DateIndonesian($yyymmdd)
    {
        $bulan['01'] = 'Januari';
        $bulan['02'] = 'Februari';
        $bulan['03'] = 'Maret';
        $bulan['04'] = 'April';
        $bulan['05'] = 'Mei';
        $bulan['06'] = 'Juni';
        $bulan['07'] = 'Juli';
        $bulan['08'] = 'Agustus';
        $bulan['09'] = 'September';
        $bulan['10'] = 'Oktober';
        $bulan['11'] = 'November';
        $bulan['12'] = 'Desember';
        $_tmp = explode('-', $yyymmdd);
        $tahun = $_tmp[0];
        $bulan = $bulan[$_tmp[1]];
        $tanggal = $_tmp[2];
        if(substr($tanggal,0,1) == "0") {
            $tanggal = substr($tanggal,1,1);
        }
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }

    public function FormatNPWP($npwp) {
        $npwp_fmt = substr($npwp, 0, 2);
        $npwp_fmt .= '.' .  substr($npwp, 2, 3);
        $npwp_fmt .= '.' . substr($npwp, 5, 3);
        $npwp_fmt .= '.' . substr($npwp, 8, 1);
        $npwp_fmt .= '-' . substr($npwp, 9, 3);
        $npwp_fmt .= '.' . substr($npwp, 12, 3);

        return $npwp_fmt;
    }

    public function FormatNPWP2($npwp) {
        
    }

    public function NavigationMenu() {
        return array(
			1 => array(
                'name' => 'User Profile', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 100,
                'submenu' => array()
            ),
			2 => array(
                'name' => 'Penghasilan Utama', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 100,
                'submenu' => array()
            ),
			3 => array(
                'name' => 'Aset dan Hutang', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 80,
                'submenu' => array()
            ),
			4 => array(
                'name' => 'Penghasilan Tambahan Rutin', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 60,
                'submenu' => array()
            ),
			5 => array(
                'name' => 'Penghasilan Sekali Terima', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 0,
                'submenu' => array()
            ),
			6 => array(
                'name' => 'Tax Amnesty', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 0,
                'submenu' => array()
            ),
			7 => array(
                'name' => 'Informasi Lainnya', 
                'focused' => false, 
                'link' => 'javascript:void(0)', 
                'status' => false, 
                'progress' => 0,
                'submenu' => array()
            )
		);
    }

    public function Navigation($steps, $currentStep) {
        $tableRecord = Auth::user()->step;
        $tmp_step = empty($tableRecord->temp_steps) ? [] : json_decode($tableRecord->temp_steps, true);
        $detail_step = empty($tableRecord->detail_step) ? [] : json_decode($tableRecord->detail_step, true);
        //dd($tmp_step, $detail_step);
        $menu = $this->NavigationMenu();
        foreach ($steps as $key => $val) {
            $nested = $val['nested'];
            if (empty($nested)) continue;

            $split = explode('.', $nested);
            $keymenu = $split[0];
            $keydetail = $split[1];
            $keysuper = isset($split[2]) ? $split[2] : '';

            $array_value = array(
                'id' => 0,
                'title' => '',
                'focused' => false,
                'filled' => false,
                'hidden' => false,
                'submenu' => array()
            );

            $array_value['id'] = (count($split) == 3) ? $keysuper : $keydetail;
            $array_value['title'] = $val['title'];

            if (array_key_exists($array_value['id'], $detail_step)) {
                if ($detail_step[$array_value['id']] == true){
                    $array_value['filled'] = true;
                }
                if (array_search($array_value['id'], $tmp_step) === false) {
                    $array_value['hidden'] = true;
                }
            }else{
                $array_value['hidden'] = true;
            }
            
            if (count($split) == 3){
                if ($currentStep == $keysuper) {
                    $menu[$keymenu]['focused'] = true;
                    $menu[$keymenu]['submenu'][$keydetail]['focused'] = true;
                    $array_value['focused'] = true;
                }
                $menu[$keymenu]['submenu'][$keydetail]['submenu'][$keysuper] = $array_value;
            }else{
                if ($currentStep == $keydetail) {
                    $menu[$keymenu]['focused'] = true;
                    $array_value['focused'] = true;
                }
                $menu[$keymenu]['submenu'][$keydetail] = $array_value;
            }
        }
        $rebuild = $this->RebuildProgresNav($menu);
        
        return $rebuild;
    }

    private function RebuildProgresNav($menu) {
        $result = $menu;
        $array_value = array();
        $total_jumlah = 0;
        $total_filled = 0;
        foreach ($menu as $key => $val) {
            $jumlah = 0;
            $filled = 0;
            foreach ($val['submenu'] as $k => $v) {
                if ($v['hidden'] == false) {
                    $jumlah += 1;
                }
                if ($v['filled'] == true){
                    $filled += 1;
                }
            }
            if ($jumlah > 0 && $jumlah == $filled) $result[$key]['status'] = true;

            $total_jumlah += $jumlah;
            $total_filled += $filled;
        }
        $progress = 0;
        if ($total_filled > 0) {
            $progress = floor(($total_filled / $total_jumlah) * 100);
        }
        $result[1]['progress'] = $progress;
        return $result;
    }
}