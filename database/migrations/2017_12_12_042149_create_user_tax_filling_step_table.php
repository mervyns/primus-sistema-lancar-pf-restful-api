<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaxFillingStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tax_filling_steps', function (Blueprint $table) {
	        $table->string('id')->unique();
	        $table->unsignedInteger('user_id')->nullable();
	        $table->integer('current_step')->nullable();
	        $table->text('unfilled_fields')->nullable();

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('user_tax_filling_steps');
    }
}
