<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetCompanionHasIdToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->integer('companion_has_npwp')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
	        $table->integer('companion_has_npwp')->nullable(false)->change();
        });
    }
}
