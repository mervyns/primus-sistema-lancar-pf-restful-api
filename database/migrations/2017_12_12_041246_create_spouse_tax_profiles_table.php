<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpouseTaxProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spouse_tax_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('job_status_id')->unsigned()->nullable();
            $table->integer('klu_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('nik');
            $table->string('npwp')->nullable();
            $table->double('final_income', 15,2);
            $table->double('nonfinal_income', 15,2);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('job_status_id')->references('id')->on('job_statuses');
            $table->foreign('klu_id')->references('id')->on('klasifikasi_lapangan_usahas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spouse_tax_profiles');
    }
}
