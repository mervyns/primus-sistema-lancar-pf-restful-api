<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserTaxProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->dropColumn('is_freelance');
            $table->integer('job_status_id')->unsigned()->nullable();
            $table->integer('companion_has_npwp');

            $table->foreign('job_status_id')->references('id')->on('job_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->integer('is_freelance'); 
            $table->dropForeign('user_tax_profiles_job_status_id_foreign');
            $table->dropColumn('job_status_id');
            $table->dropColumn('companion_has_npwp');
        });
    }
}
