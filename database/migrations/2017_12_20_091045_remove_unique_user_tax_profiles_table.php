<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueUserTaxProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->dropUnique('user_tax_profiles_nik_unique');
            $table->dropUnique('user_tax_profiles_npwp_unique');
            $table->dropUnique('user_tax_profiles_efin_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->unique('nik');
            $table->unique('npwp');
            $table->unique('efin');
        });
    }
}
