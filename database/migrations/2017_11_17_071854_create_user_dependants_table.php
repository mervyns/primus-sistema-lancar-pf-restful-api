<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDependantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dependants');

        Schema::create('user_dependants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('dependant_relation_id')->unsigned()->nullable();
            $table->string('ktp_number');
            $table->string('name');
            $table->integer('job');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('dependant_relation_id')->references('id')->on('dependant_relations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_dependants');

        Schema::create('dependants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ktp_number')->unique();
            $table->string('name');
            $table->integer('job');
            $table->timestamps();
        });
    }
}
