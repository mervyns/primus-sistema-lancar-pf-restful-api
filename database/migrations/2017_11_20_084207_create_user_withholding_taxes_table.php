<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWithholdingTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_withholding_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_income_id')->unsigned()->nullable();
            $table->integer('withholding_tax_slip_id')->unsigned()->nullable();
            $table->string('withholder_name');
            $table->string('withholder_npwp');
            $table->string('withholding_slip_number');
            $table->date('withholding_slip_date');
            $table->double('value', 15,2);
            $table->timestamps();

            $table->foreign('user_income_id')->references('id')->on('user_incomes');
            $table->foreign('withholding_tax_slip_id')->references('id')->on('withholding_tax_slips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_withholding_taxes');
    }
}
