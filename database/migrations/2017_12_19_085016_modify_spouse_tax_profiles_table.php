<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifySpouseTaxProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spouse_tax_profiles', function (Blueprint $table) {
            $table->dropColumn('final_income');
            $table->dropColumn('nonfinal_income');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spouse_tax_profiles', function (Blueprint $table) {
            $table->double('final_income', 15,2);
            $table->double('nonfinal_income', 15,2);
        });
    }
}
