<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxResponsibilityStatusOnUserTaxProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->integer('tax_responsibility_status_id')->unsigned()->nullable();

            $table->foreign('tax_responsibility_status_id')->references('id')->on('tax_responsibility_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->dropForeign('user_tax_profiles_tax_responsibility_status_id_foreign');
            $table->dropColumn('tax_responsibility_status_id');
        });
    }
}
