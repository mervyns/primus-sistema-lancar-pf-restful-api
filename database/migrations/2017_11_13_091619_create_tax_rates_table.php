<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     
    public function up()
    {
        Schema::create('tax_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('income_id')->unsigned()->nullable();
            $table->integer('tax_rate_type_id')->unsigned()->nullable();
            $table->double('lower_limit', 15,2);
            $table->double('upper_limit', 15,2);
            $table->decimal('rate', 5,2);
            $table->timestamps();

            $table->foreign('income_id')->references('id')->on('incomes');
            $table->foreign('tax_rate_type_id')->references('id')->on('tax_rate_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     
    public function down()
    {
        Schema::dropIfExists('tax_rates');
    }

    */
}
