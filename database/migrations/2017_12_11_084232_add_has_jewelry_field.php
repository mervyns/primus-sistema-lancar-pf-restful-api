<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasJewelryField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
	        $table->integer('have_jewelry');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
	        $table->dropColumn('have_jewelry');
        });
    }
}
