<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('income_id')->nullable()->unsigned();
            $table->integer('country_id')->nullable()->unsigned();
            $table->double('net_value',10,2);
            $table->double('gross_value',10,2);
            $table->string('detail');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('income_id')->references('id')->on('incomes');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_incomes');
    }
}
