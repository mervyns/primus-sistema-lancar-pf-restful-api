<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStepAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('assets', 'step')) 
        {
            $dbRaw = "
                UPDATE assets
                SET step = (CASE id WHEN 1 THEN 44
                    WHEN 4 THEN 19
                    WHEN 5 THEN 44
                    WHEN 6 THEN 50
                    WHEN 7 THEN 51
                    WHEN 8 THEN 52
                    WHEN 11 THEN 38
                    WHEN 12 THEN 37
                    WHEN 13 THEN 41
                    WHEN 14 THEN 32
                    WHEN 16 THEN 40
                    WHEN 19 THEN 21
                    WHEN 20 THEN 21
                    WHEN 21 THEN 46
                    WHEN 23 THEN 25
                    WHEN 24 THEN 25
                    WHEN 25 THEN 45
                    WHEN 26 THEN 47
                    WHEN 27 THEN 49
                    WHEN 28 THEN 23
                    WHEN 31 THEN 48
                    WHEN 32 THEN 33
                    WHEN 33 THEN 36
                END)
            ";
            
            \DB::statement($dbRaw);
        }else{
           
            Schema::table('assets', function($table) {
                $table->integer('step')->nullable();;
            });

            $dbRaw = "
                UPDATE assets
                SET step = (CASE id WHEN 1 THEN 44
                    WHEN 4 THEN 19
                    WHEN 5 THEN 44
                    WHEN 6 THEN 50
                    WHEN 7 THEN 51
                    WHEN 8 THEN 52
                    WHEN 11 THEN 38
                    WHEN 12 THEN 37
                    WHEN 13 THEN 41
                    WHEN 14 THEN 32
                    WHEN 16 THEN 40
                    WHEN 19 THEN 21
                    WHEN 20 THEN 21
                    WHEN 21 THEN 46
                    WHEN 23 THEN 25
                    WHEN 24 THEN 25
                    WHEN 25 THEN 45
                    WHEN 26 THEN 47
                    WHEN 27 THEN 49
                    WHEN 28 THEN 23
                    WHEN 31 THEN 48
                    WHEN 32 THEN 33
                    WHEN 33 THEN 36
                END)
            ";

            \DB::statement($dbRaw);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
