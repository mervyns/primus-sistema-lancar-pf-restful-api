<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLiabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_liabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('liability_id')->unsigned()->nullable();
            $table->integer('acquisition_year');
            $table->double('value', 15,2);
            $table->string('lender_name');
            $table->string('lender_address');
            $table->string('detail');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('liability_id')->references('id')->on('liabilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_liabilities');
    }
}
