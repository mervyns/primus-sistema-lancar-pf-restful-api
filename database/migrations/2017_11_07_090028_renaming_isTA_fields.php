<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamingIsTAFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_assets', function (Blueprint $table) {
            $table->renameColumn('isTA', 'is_ta');
        });

        Schema::table('user_foreign_assets', function (Blueprint $table) {
            $table->renameColumn('isTA', 'is_ta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_assets', function (Blueprint $table) {
            $table->renameColumn('is_ta', 'isTA');
        });

        Schema::table('user_foreign_assets', function (Blueprint $table) {
            $table->renameColumn('is_ta', 'isTA');
        });
    }
}
