<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTaxRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     
    public function up()
    {
        DB::statement('ALTER TABLE tax_rates CHANGE rate rate DECIMAL(6,3);');

        Schema::table('tax_rates', function (Blueprint $table) {
            $table->string('provision');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     
    public function down()
    {
        DB::statement('ALTER TABLE tax_rates CHANGE rate rate DECIMAL(5,2);');

        Schema::table('tax_rates', function (Blueprint $table) {
            $table->dropColumn('provision');
        });
    }
    */
}
