<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserDependantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_dependants', function (Blueprint $table) {
            $table->dropColumn('job');
        });

        Schema::table('user_dependants', function (Blueprint $table) {
            $table->string('job');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('user_dependants', function (Blueprint $table) {
            $table->dropColumn('job');
        });

        Schema::table('user_dependants', function (Blueprint $table) {
            $table->integer('job');
        });
    }
}
