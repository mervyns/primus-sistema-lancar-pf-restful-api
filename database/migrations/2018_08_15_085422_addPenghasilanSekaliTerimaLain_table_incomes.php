<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPenghasilanSekaliTerimaLainTableIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbRaw = "
        INSERT INTO `incomes` (`name`, `status`, `income_category_id`, `created_at`, `updated_at`, `lower_limit`, `upper_limit`, `tax_rate`, `tax_rate_type_id`, `tax_provision`)
        VALUES
	    ('Penghasilan Sekali Terima Lainnya', 1, 3, '2018-08-15 15:56:11', '2018-08-15 15:56:11', 0.00, 0.00, 0.00, NULL, '');

        ";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
