<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyValueColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE user_incomes CHANGE net_value net_value DOUBLE(15,2);');
        DB::statement('ALTER TABLE user_incomes CHANGE gross_value gross_value DOUBLE(15,2);');

        DB::statement('ALTER TABLE user_revealed_assets CHANGE value value DOUBLE(15,2);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE user_incomes CHANGE net_value net_value DOUBLE(10,2);');
        DB::statement('ALTER TABLE user_incomes CHANGE gross_value gross_value DOUBLE(10,2);');

        DB::statement('ALTER TABLE user_revealed_assets CHANGE value value DOUBLE(10,2);');
    }
}
