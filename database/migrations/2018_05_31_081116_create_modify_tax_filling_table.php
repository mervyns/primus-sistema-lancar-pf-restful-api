<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModifyTaxFillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->integer('ta_disclosure')->nullable();
            $table->integer('have_deposit')->nullable();
            $table->integer('have_credit_card')->nullable();
        });

        Schema::table('user_dependants', function (Blueprint $table) {
            $table->integer('not_have_ktp')->nullable();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tax_profiles', function (Blueprint $table) {
            $table->dropColumn('ta_disclosure');
        });

        Schema::table('user_dependants', function (Blueprint $table) {
            $table->dropColumn('not_have_ktp');
        });
    }
}
