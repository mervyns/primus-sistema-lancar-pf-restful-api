<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTaxRatesIncomesRelationship extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tax_rates'); 

        Schema::table('incomes', function (Blueprint $table) {
            $table->dropUnique('incomes_name_unique');
            $table->double('lower_limit', 15,2);
            $table->double('upper_limit', 15,2);
            $table->decimal('tax_rate');
            $table->integer('tax_rate_type_id')->unsigned()->nullable();
            $table->string('tax_provision');
            
            $table->foreign('tax_rate_type_id')->references('id')->on('tax_rate_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   Schema::dropIfExists('tax_rates'); 

        Schema::table('incomes', function (Blueprint $table) {
            $table->string('name')->unique()->change();
            $table->dropForeign('incomes_tax_rate_type_id_foreign');
            $table->dropColumn('lower_limit');
            $table->dropColumn('upper_limit');
            $table->dropColumn('tax_rate_type_id');
            $table->dropColumn('tax_rate');
            $table->dropColumn('tax_provision');
        });
    }
}
