<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemStepsConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::dropIfExists('system_steps_config');
        Schema::create('system_steps_config', function (Blueprint $table) {
	        $table->increments('id');
	        $table->integer('step_id')->nullable();
	        $table->string('title', 255)->nullable();
	        $table->text('sub_title')->nullable();
	        $table->string('icon', 10)->nullable();
	        $table->boolean('parent')->nullable();
	        $table->boolean('display_sticky_note')->nullable();
	        $table->boolean('display_knowledge_base')->nullable();
	        $table->boolean('display_progress_bar')->nullable();
	        $table->integer('previous')->nullable();
	        $table->integer('next')->nullable();
	        $table->string('view', 255)->nullable();
	        $table->text('validation_rules')->nullable();

	        $table->timestamps();

//	        $table->foreign('step_id')->references('id')->on('system_step_constants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_steps_config');
    }
}
