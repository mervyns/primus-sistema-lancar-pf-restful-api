<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaxProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tax_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('nik')->unique();
            $table->string('npwp')->unique();
            $table->string('efin')->unique();
            $table->integer('is_freelance');
            $table->integer('klu_id')->unsigned()->nullable();
            $table->integer('marital_status');
            $table->integer('have_dependant');
            $table->integer('have_vehicle');
            $table->integer('have_property');
            $table->integer('have_insurance');
            $table->integer('have_foreign_asset');
            $table->integer('following_tax_amnesty_2016');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('klu_id')->references('id')->on('klasifikasi_lapangan_usahas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tax_profiles');
    }
}
