<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRepatriatedAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_repatriated_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_revealed_asset_id')->nullable()->unsigned();
            $table->integer('investment_id')->nullable()->unsigned();
            $table->integer('gateway_id')->nullable()->unsigned();
            $table->double('will_repatriated_value',15,2);
            $table->double('has_repatriated_value',15,2);
            $table->date('investment_date');
            $table->timestamps();

            $table->foreign('user_revealed_asset_id')->references('id')->on('user_revealed_assets');
            $table->foreign('investment_id')->references('id')->on('investments');
            $table->foreign('gateway_id')->references('id')->on('gateways');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_repatriated_assets');
    }
}
