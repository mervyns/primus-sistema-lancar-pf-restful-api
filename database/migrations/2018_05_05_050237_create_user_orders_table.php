<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice', 25)->nullable();
            $table->integer('user_product_id')->unsigned();
            $table->integer('bank_account_id')->unsigned();
            $table->double('amount', 19, 0);
            $table->double('total_amount', 19, 0);
            $table->string('status', 10);
            $table->string('attachment')->nullable();
            $table->string('unique_code');
            $table->timestamps();

            $table->foreign('user_product_id')->references('id')->on('user_products');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('user_orders');
    }
}
