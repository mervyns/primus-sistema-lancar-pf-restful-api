<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PassFinalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pass_final', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category', 255);
            $table->integer('type');
            $table->integer('user_id');
            $table->integer('acquisition_year');
            $table->double('nominal');
            $table->string('assesment_document_number')->nullable();
            $table->integer('country_id');
            $table->string('address');
            $table->string('name');
            $table->string('NPWP');
            $table->string('supporting_document')->nullable();
            $table->string('document_type');
            $table->string('document_number');
            $table->string('acquisition_description');
            $table->integer('qty_assets')->nullable();
            $table->string('unit')->nullable();
            $table->text('detail')->nullable();
            $table->integer('flag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pass_final');
    }
}
