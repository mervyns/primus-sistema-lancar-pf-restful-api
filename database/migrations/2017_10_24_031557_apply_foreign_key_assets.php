<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplyForeignKeyAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->integer('asset_category_id')->nullable()->unsigned()->change();
            $table->foreign('asset_category_id')->references('id')->on('asset_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropForeign('assets_asset_category_id_foreign');
            $table->dropColumn('asset_category_id');
        });

        Schema::table('assets', function (Blueprint $table) {
            $table->integer('asset_category_id');
        });
    }
}
