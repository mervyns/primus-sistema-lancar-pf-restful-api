<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStepLiabilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('liabilities', 'step')) 
        {
            $dbRaw = "
                UPDATE liabilities
                SET step = (CASE id WHEN 1 THEN 54
                    WHEN 2 THEN 20
                    WHEN 3 THEN 55
                    WHEN 4 THEN 56
                END)
            ";
            
            \DB::statement($dbRaw);
        }else{
           
            Schema::table('liabilities', function($table) {
                $table->integer('step')->nullable();;
            });

            $dbRaw = "
                UPDATE liabilities
                SET step = (CASE id WHEN 1 THEN 54
                    WHEN 2 THEN 20
                    WHEN 3 THEN 55
                    WHEN 4 THEN 56
                END)
            ";

            \DB::statement($dbRaw);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
