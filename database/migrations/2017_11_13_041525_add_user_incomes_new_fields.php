<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIncomesNewFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->integer('user_asset_id')->nullable()->unsigned();
            $table->integer('user_foreign_asset_id')->nullable()->unsigned();
            $table->integer('asset_id')->nullable()->unsigned();

            $table->foreign('user_asset_id')->references('id')->on('user_assets');
            $table->foreign('user_foreign_asset_id')->references('id')->on('user_foreign_assets');
            $table->foreign('asset_id')->references('id')->on('assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_incomes', function (Blueprint $table) {
            $table->dropForeign('user_incomes_user_asset_id_foreign');
            $table->dropForeign('user_incomes_user_foreign_asset_id_foreign');
            $table->dropForeign('user_incomes_asset_id_foreign');
            $table->dropColumn('user_asset_id');
            $table->dropColumn('user_foreign_asset_id');
            $table->dropColumn('asset_id');
        });
    }
}
