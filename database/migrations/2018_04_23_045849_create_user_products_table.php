<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProductsTable extends Migration
{

    /*
        CREATED BY HALIM - JAVASIGN
        23 APR 18
    */


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned();
            $table->integer('years');
            $table->string('type', 10);
            $table->date('start_at')->nullable();
            $table->date('expired_at')->nullable();
            $table->integer('user_parent_id')->nullable();
            $table->string('email', 255)->nullable();
            $table->boolean('wife_or_husband');
            $table->boolean('promo');
            $table->string('promo_description')->nullable();
            $table->integer('parent_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('user_products');
    }
}
