<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStepIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('incomes', 'step')) 
        {
            $dbRaw = "
                UPDATE incomes
                SET step = (CASE id WHEN 1 THEN 9
                    WHEN 2 THEN 58
                    WHEN 5 THEN 64
                    WHEN 6 THEN 67
                    WHEN 7 THEN 65
                    WHEN 8 THEN 66
                    WHEN 10 THEN 75
                    WHEN 11 THEN 81
                    WHEN 12 THEN 81
                    WHEN 13 THEN 81
                    WHEN 14 THEN 81
                    WHEN 15 THEN 82
                    WHEN 16 THEN 74
                    WHEN 17 THEN 74
                    WHEN 18 THEN 74
                    WHEN 19 THEN 74
                    WHEN 20 THEN 74
                    WHEN 21 THEN 74
                    WHEN 22 THEN 74
                    WHEN 23 THEN 74
                    WHEN 24 THEN 74
                    WHEN 25 THEN 71
                    WHEN 26 THEN 71
                    WHEN 27 THEN 77
                    WHEN 28 THEN 76
                    WHEN 29 THEN 78
                    WHEN 30 THEN 78
                    WHEN 31 THEN 78
                    WHEN 32 THEN 78
                    WHEN 33 THEN 72
                    WHEN 34 THEN 73
                    WHEN 35 THEN 83
                    WHEN 38 THEN 61
                    WHEN 41 THEN 62
                    WHEN 42 THEN 63
                    WHEN 43 THEN 80
                    WHEN 44 THEN 79
                    WHEN 45 THEN 68
                    WHEN 54 THEN 69
                    WHEN 55 THEN 84
                END)
            ";
            
            \DB::statement($dbRaw);
        }else{
           
            Schema::table('incomes', function($table) {
                $table->integer('step')->nullable();;
            });

            $dbRaw = "
                UPDATE incomes
                SET step = (CASE id WHEN 1 THEN 9
                    WHEN 2 THEN 58
                    WHEN 5 THEN 64
                    WHEN 6 THEN 67
                    WHEN 7 THEN 65
                    WHEN 8 THEN 66
                    WHEN 10 THEN 75
                    WHEN 11 THEN 81
                    WHEN 12 THEN 81
                    WHEN 13 THEN 81
                    WHEN 14 THEN 81
                    WHEN 15 THEN 82
                    WHEN 16 THEN 74
                    WHEN 17 THEN 74
                    WHEN 18 THEN 74
                    WHEN 19 THEN 74
                    WHEN 20 THEN 74
                    WHEN 21 THEN 74
                    WHEN 22 THEN 74
                    WHEN 23 THEN 74
                    WHEN 24 THEN 74
                    WHEN 25 THEN 71
                    WHEN 26 THEN 71
                    WHEN 27 THEN 77
                    WHEN 28 THEN 76
                    WHEN 29 THEN 78
                    WHEN 30 THEN 78
                    WHEN 31 THEN 78
                    WHEN 32 THEN 78
                    WHEN 33 THEN 72
                    WHEN 34 THEN 73
                    WHEN 35 THEN 83
                    WHEN 38 THEN 61
                    WHEN 41 THEN 62
                    WHEN 42 THEN 63
                    WHEN 43 THEN 80
                    WHEN 44 THEN 79
                    WHEN 45 THEN 68
                    WHEN 54 THEN 69
                    WHEN 55 THEN 84
                END)
            ";

            \DB::statement($dbRaw);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
