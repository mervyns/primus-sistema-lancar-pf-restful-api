<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTooltipFeatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tooltips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('step_config_id')->unsigned();
            $table->string('judul', 255);
            $table->text('content');
            $table->string('link', 255);
            $table->timestamps();

            $table->foreign('step_config_id')->references('id')->on('system_steps_config');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tooltips');
    }
}
