<?php

use Illuminate\Database\Seeder;

class UserCompanionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_companions')->insert([
            'user_id' => 1,
            'companion_id' => 2,
            'requested_by' => 1
        ]);
    }
}
