<?php

use Illuminate\Database\Seeder;

class UserForeignAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_foreign_assets')->insert([
            'user_id' => 2,
            'asset_id' => 1,
            'user_revealed_asset_id' => 2,
            'country_id' => 2,
            'acquisition_year' => 2017,
            'value' => 30000,
            'idr_value' => 310644954.00,
            'is_ta' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
