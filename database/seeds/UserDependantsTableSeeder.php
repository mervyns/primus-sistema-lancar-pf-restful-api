<?php

use Illuminate\Database\Seeder;

class UserDependantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_dependants')->insert([
            'user_id' => 2,
            'dependant_relation_id' => 5,
            'ktp_number' => '01234',
            'name' => 'Andi',
            'job' => 'Pelajar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('user_dependants')->insert([
            'user_id' => 2,
            'dependant_relation_id' => 3,
            'ktp_number' => '56789',
            'name' => 'Bapak Budi',
            'job' => 'Tidak Bekerja/Pensiun',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('user_dependants')->insert([
            'user_id' => 2,
            'dependant_relation_id' => 4,
            'ktp_number' => '98765',
            'name' => 'Ibu Caca',
            'job' => 'Tidak Bekerja/Pensiun',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
