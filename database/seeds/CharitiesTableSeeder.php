<?php

use Illuminate\Database\Seeder;

class CharitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charities')->insert([
            'name' => 'Badan Amil Zakat Nasional',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('charities')->insert([
            'name' => 'LAZ Dompet Dhuafa Republika',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Amanah Takaful',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Pos Keadilan Peduli Umat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Baitul Maal Muamalat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Dana Sosial Al Falah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Baitul Maal Hidayatullah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Persatuan Islam',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Baitul Mal Umat Islam PT Bank Negara Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Bangun Sejahtera Mitra Umat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Dewan Da’wah Islamiyah Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Baitul Maal Bank Rakyat Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Baitul Maal wat Tamwil',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Baituzzakah Pertamina',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Dompet Peduli Umat Daarut Tauhiid (DUDT)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Yayasan Rumah Zakat Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZIS Muhammadiyah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZIS Nahdlatul Ulama (LAZIS NU)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZIS Ikatan Persaudaraan Haji Indonesia (LAZIS IPHI)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Lembaga Sumbangan Agama Kristen Indonesia (LEMSAKTI)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Nurul Hayat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'LAZ Inisiatif Zakat Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Lembaga Manajemen Infaq (LAZ LMI)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Yatim Mandiri Surabaya (LAZ Yatim Mandiri)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Pesantren Islam Al Azhar (LAZ Al Azhar)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Global Zakat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Perkumpulan Persatuan Islam (PERSIS)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Solo Peduli Ummat (LAZ Solo Peduli)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Dompet Amal Sejahtera Ibnu Abbas (LAZ Dasi)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Baitul Maal Forum Komunikasi Aktifis Masjid (LAZ FKAM)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Dana Peduli Ummat (DPU)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Dompet Sosial Madani (LAZ DSM)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Sinergi Foundation (LAZ Sinergi Foundation)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Yayasan Harapan Dhuafa Banten',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('charities')->insert([
            'name' => 'Badan Dharma Dana Nasional Yayasan Adikara Dharma Parisad (BDDN YADP)',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
