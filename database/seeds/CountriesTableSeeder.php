<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name' => 'Indonesia',
            'currency' => 'IDR',
            'currency_name' => 'Indonesian Rupiah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Amerika Serikat',
            'currency' => 'USD',
            'currency_name' => 'United States Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Australia',
            'currency' => 'AUD',
            'currency_name' => 'Australian Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Kanada',
            'currency' => 'CAD',
            'currency_name' => 'Canadian Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Denmark',
            'currency' => 'DKK',
            'currency_name' => 'Danish Krone',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Hongkong',
            'currency' => 'HKD',
            'currency_name' => 'Hongkong Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Malaysia',
            'currency' => 'MYR',
            'currency_name' => 'Malaysian Ringgit',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Selandia Baru',
            'currency' => 'NZD',
            'currency_name' => 'New Zealand Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Norwegia',
            'currency' => 'NOK',
            'currency_name' => 'Norwegian Krone',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Inggris',
            'currency' => 'GBP',
            'currency_name' => 'Pound Sterling',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Singapura',
            'currency' => 'SGD',
            'currency_name' => 'Singapore Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Swedia',
            'currency' => 'SEK',
            'currency_name' => 'Swedish Krone',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Swiss',
            'currency' => 'CHF',
            'currency_name' => 'Swiss Franc',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Jepang',
            'currency' => 'JPY',
            'currency_name' => 'Japanese Yen',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Myanmar',
            'currency' => 'MMK',
            'currency_name' => 'Burmese Kyat',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'India',
            'currency' => 'INR',
            'currency_name' => 'Indian Rupee',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Kuwait',
            'currency' => 'KWD',
            'currency_name' => 'Kuwaiti Dinar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Pakistan',
            'currency' => 'PKR',
            'currency_name' => 'Pakistani Rupee',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Filipina',
            'currency' => 'PHP',
            'currency_name' => 'Philipine Peso',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Saudi Arabia',
            'currency' => 'SAR',
            'currency_name' => 'Saudi Riyal',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Sri Lanka',
            'currency' => 'LKR',
            'currency_name' => 'Sri Lankan Rupee',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Thailand',
            'currency' => 'THB',
            'currency_name' => 'Thai Baht',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Brunei Darussalam',
            'currency' => 'BND',
            'currency_name' => 'Brunei Dollar',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Euro',
            'currency' => 'EUR',
            'currency_name' => 'Euro',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Renminbi Tiongkok',
            'currency' => 'CNY',
            'currency_name' => 'Chinese Yuan Renminbi',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('countries')->insert([
            'name' => 'Korea',
            'currency' => 'KRW',
            'currency_name' => 'Korean Won',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
