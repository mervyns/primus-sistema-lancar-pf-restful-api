<?php

use Illuminate\Database\Seeder;

class IncomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('incomes')->insert([
            'name' => 'Gaji',
            'status' => 2,
            'income_category_id' => 1,
            'tax_rate_type_id' => 2,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'step' => 9
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghasilan Sewa Tanah/Bangunan',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.1,
            'tax_rate_type_id' => 1,
            'step' => 58,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Dividen',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.1,
            'tax_rate_type_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penjualan Saham di Bursa Efek',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.001,
            'tax_rate_type_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penjualan Saham Pendiri di Bursa Efek',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.005,
            'tax_rate_type_id' => 1,
            'step' => 64,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pendapatan Bunga Sehubungan dengan Jaminan Pengembalian Utang',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 3,
            'step' => 67,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Royalti dan Hak Kekayaan Intelektual',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 3,
            'step' => 65,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pendapatan Sewa dari Aset Diluar Tanah Bangunan',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate' => 0.02,
            'tax_rate_type_id' => 3,
            'step' => 66,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Bagian Laba Anggota Perseroan Komanditer Tidak Atas Saham, Persekutuan, Perkumpulan, Firma, Kongsi',
            'status' => 3,
            'income_category_id' => 2,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Hadiah Undian',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.25,
            'tax_rate_type_id' => 1,
            'step' => 75,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghargaan dan Hadiah',
            'status' => 2,
            'income_category_id' => 3,
            'lower_limit' => 0,
            'upper_limit' => 50000000,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 2,
            'step' => 81,
            'tax_provision' => 'PPh Pasal 17',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghargaan dan Hadiah',
            'status' => 2,
            'income_category_id' => 3,
            'lower_limit' => 50000000,
            'upper_limit' => 250000000,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 2,
            'step' => 81,
            'tax_provision' => 'PPh Pasal 17',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghargaan dan Hadiah',
            'status' => 2,
            'income_category_id' => 3,
            'lower_limit' => 250000000,
            'upper_limit' => 500000000,
            'tax_rate' => 0.25,
            'tax_rate_type_id' => 2,
            'step' => 81,
            'tax_provision' => 'PPh Pasal 17',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghargaan dan Hadiah',
            'status' => 2,
            'income_category_id' => 3,
            'lower_limit' => 500000000,
            'tax_rate' => 0.30,
            'tax_rate_type_id' => 2,
            'step' => 81,
            'tax_provision' => 'PPh Pasal 17',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Bantuan/Sumbangan',
            'status' => 3,
            'income_category_id' => 3,
            'step' => 82,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pesangon',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 0,
            'upper_limit' => 50000000,
            'tax_rate' => 0,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pesangon',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 50000000,
            'upper_limit' => 100000000,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pesangon',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 100000000,
            'upper_limit' => 500000000,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pesangon',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 500000000,
            'tax_rate' => 0.25,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 0,
            'upper_limit' => 50000000,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 50000000,
            'upper_limit' => 250000000,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 250000000,
            'upper_limit' => 500000000,
            'tax_rate' => 0.25,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua',
            'status' => 1,
            'income_category_id' => 3,
            'lower_limit' => 500000000,
            'tax_rate' => 0.3,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Manfaat Pensiun, Tunjangan Hari Tua atau Jaminan Hari Tua',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 2,
            'step' => 74,
            'tax_provision' => 'PERATURAN MENTERI KEUANGAN NO 16 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Warisan',
            'status' => 3,
            'income_category_id' => 3,
            'step' => 71,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Hibah',
            'status' => 3,
            'income_category_id' => 3,
            'step' => 71,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penjualan Saham Pendiri Diluar Bursa Efek',
            'status' => 2,
            'income_category_id' => 3,
            'step' => 77,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Beasiswa',
            'status' => 3,
            'income_category_id' => 3,
            'step' => 76,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Honorarium atas Beban APBN/APBD Golongan I',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0,
            'tax_rate_type_id' => 1,
            'step' => 78,
            'tax_provision' => 'PERATURAN PEMERINTAH KEUANGAN NO 80 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Honorarium atas Beban APBN/APBD Golongan II',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0,
            'tax_rate_type_id' => 1,
            'step' => 78,
            'tax_provision' => 'PERATURAN PEMERINTAH KEUANGAN NO 80 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Honorarium atas Beban APBN/APBD Golongan III',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 1,
            'step' => 78,
            'tax_provision' => 'PERATURAN PEMERINTAH KEUANGAN NO 80 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Honorarium atas Beban APBN/APBD Golongan IV',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 1,
            'step' => 78,
            'tax_provision' => 'PERATURAN PEMERINTAH KEUANGAN NO 80 TAHUN 2010',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pengalihan Hak Atas Tanah dan Bangunan',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.025,
            'tax_rate_type_id' => 1,
            'step' => 72,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Keuntungan dari Pengalihan Harta Selain Tanah dan Bangunan',
            'status' => 2,
            'income_category_id' => 3,
            'step' => 73,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Bangunan Yang Diterima Dalam Rangka Bangun Guna Serah',
            'status' => 1,
            'income_category_id' => 3,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 1,
            'step' => 83,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Gaji Pasangan',
            'status' => 2,
            'income_category_id' => 1,
            'tax_rate_type_id' => 2,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghasilan Istri dari 1 Pemberi Kerja',
            'status' => 1,
            'income_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Tabungan/Deposito Dalam Negeri',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.2,
            'tax_rate_type_id' => 1,
            'step' => 61,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Tabungan/Deposito Luar Negeri',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate_type_id' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Klaim Asuransi',
            'status' => 3,
            'income_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Bunga/Diskonto Sertifikat Bank Indonesia dan Surat Berharga Negara',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.2,
            'tax_rate_type_id' => 3,
            'step' => 62,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Bunga/Diskonto Obligasi',
            'status' => 1,
            'income_category_id' => 2,
            'tax_rate' => 0.15,
            'tax_rate_type_id' => 3,
            'step' => 63,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Honorarium Komisaris yang Tidak Merangkap Pegawai Tetap',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate_type_id' => 2,
            'step' => 80,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Upah Pegawai Tidak Tetap atau Tenaga Kerja Lepas',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate_type_id' => 2,
            'step' => 79,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Imbalan Kepada Tenaga Ahli',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate_type_id' => 2,
            'step' => 68,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Pesawat terbang pribadi/helicopter pribadi',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Kapal Pesiar, Yacht, dan sejenisnya',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Tanah dan bangunan dengan harga > 5 Miliar dan luas lebih dari 400 meter persegi',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Apartemen , kondominium dan sejenisnya dengan harga > 5 Miliar dan luas > 150 meter persegi',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Kendaraan roda 4 pengangkutan kurang dari 10, berupa Sedan, Jeep, Sport Utility, Vehicle (SUV) MPV, Minibus dan sejenisnya, dengan harga jual > 2 Miliar, atau dengan kapasitas silinder lebih dari 3000 CC',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Kendaraan bermotor roda 2 dan 3 dengan harga jual lebih dari 300 Juta Rupiah atau dengan kapasitas silinder lebih dari 250',
            'status' => 1,
            'income_category_id' => 4,
            'tax_rate' => 0.05,
            'tax_rate_type_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghasilan Luar Negeri',
            'status' => 1,
            'income_category_id' => 5,
            'tax_rate' => 0,
            'tax_rate_type_id' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Charity',
            'status' => 1,
            'income_category_id' => 6,
            'tax_rate' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('incomes')->insert([
            'name' => 'Penghasilan Rutin Lainnya',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate' => 0,
            'step' => 69,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('incomes')->insert([
            'name' => 'Penghasilan Sekali Terima Lainnya',
            'status' => 2,
            'income_category_id' => 2,
            'tax_rate' => 0,
            'step' => 84,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

    }
}
