<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assets')->insert([
            'name' => 'Uang Tunai',
            'asset_code' => '011',
            'asset_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('assets')->insert([
            'name' => 'Tabungan',
            'asset_code' => '012',
            'asset_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Giro',
            'asset_code' => '013',
            'asset_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Deposito',
            'asset_code' => '014',
            'step' => 19,
            'asset_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Setara Kas',
            'asset_code' => '019',
            'step' => 44,
            'asset_category_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Piutang',
            'asset_code' => '021',
            'asset_category_id' => 2,
            'step' => 50,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Piutang Afiliasi',
            'asset_code' => '022',
            'asset_category_id' => 2,
            'step' => 51,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Piutang Lainnya',
            'asset_code' => '029',
            'asset_category_id' => 2,
            'step' => 52,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Saham yang Dibeli untuk Dijual Kembali',
            'asset_code' => '031',
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Saham',
            'asset_code' => '032',
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Obligasi Perusahaan',
            'asset_code' => '033',
            'asset_category_id' => 3,
            'step' => 38,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Obligasi Pemerintah Indonesia (Ori, Sukuk, dll',
            'asset_code' => '034',
            'asset_category_id' => 3,
            'step' => 37,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Surat Hutang Lainnya',
            'asset_code' => '035',
            'step' => 41,
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Reksadana',
            'step' => 32,
            'asset_code' => '036',
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Instrumen Derivatif',
            'asset_code' => '037',
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Penyertaan Modal Perusahaan Lain Diluar Saham (CV, Firma)',
            'asset_code' => '038',
            'asset_category_id' => 3,
            'step' => 40,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Inverstasi Lainnya',
            'asset_code' => '039',
            'asset_category_id' => 3,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Sepeda',
            'asset_code' => '041',
            'asset_category_id' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Sepeda Motor',
            'asset_code' => '042',
            'step' => 21,
            'asset_category_id' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Mobil',
            'asset_code' => '043',
            'step' => 21,
            'asset_category_id' => 4,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Alat Transportasi Lainnya',
            'asset_code' => '049',
            'asset_category_id' => 4,
            'step' => 46,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Logam Mulia (emas, perhiasan, platina, dll)',
            'asset_code' => '051',
            'step' => 25,
            'asset_category_id' => 5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Batu Mulia (intan, berlian, dll)',
            'asset_code' => '052',
            'step' => 25,
            'asset_category_id' => 5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Barang Seni dan Antik',
            'asset_code' => '053',
            'asset_category_id' => 5,
            'step' => 25,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Kapal Pesiar, Pesawat, Helikopter, Jetski, dan Peralatan Olahraga Khusus',
            'asset_code' => '054',
            'step' => 45,
            'asset_category_id' => 5,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Peralatan Elektronik, Furnitur',
            'asset_code' => '055',
            'asset_category_id' => 5,
            'step' => 47,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Harta Bergerak Lainnya',
            'asset_code' => '059',
            'asset_category_id' => 5,
            'step' => 49,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Tanah dan atau Bangunan untuk Tempat Tinggal',
            'asset_code' => '061',
            'asset_category_id' => 6,
            'step' => 23,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Tanah dan atau Bangunan untuk Usaha',
            'asset_code' => '062',
            'asset_category_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Tanah atau Lahan untuk Usaha',
            'asset_code' => '063',
            'asset_category_id' => 6,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Harta Tidak Bergerak Lainnya',
            'asset_code' => '064',
            'asset_category_id' => 6,
            'step' => 64,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Asuransi dengan Komponen Investasi',
            'asset_code' => '039',
            'asset_category_id' => 3,
            'step' => 39,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('assets')->insert([
            'name' => 'Sertifikat Bank Indonesia',
            'asset_code' => '012',
            'asset_category_id' => 1,
            'step' => 36,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
