<?php

use Illuminate\Database\Seeder;

class StepConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$steps = config('steps');
	    foreach ( $steps as $id => $config ) {
		    DB::table('system_steps_config')->insert([
			    'step_id' => $id,
			    'title' => $config['title'],
			    'sub_title' => $config['subtitle'],
			    'icon' => $config['icon'],
			    'parent' => $config['parent'],
			    'display_sticky_note' => $config['display_sticky_note'],
			    'display_knowledge_base' => $config['display_knowledge_base'],
			    'display_progress_bar' => $config['display_progress_bar'],
			    'previous' => $config['previous'],
			    'next' => $config['next'],
			    'view' => $config['view'],
			    'validation_rules' => json_encode($config['validations']),
			    'should_skip' => 0,
			    'created_at' => date('Y-m-d H:i:s'),
			    'updated_at' => date('Y-m-d H:i:s'),
		    ]);
        }
    }
}
