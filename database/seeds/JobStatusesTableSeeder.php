<?php

use Illuminate\Database\Seeder;

class JobStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('job_statuses')->insert([
            'name' => 'Tidak Bekerja',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('job_statuses')->insert([
            'name' => 'Bekerja dan mendapat form 1721 A1/A2 dari satu pemberi kerja',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('job_statuses')->insert([
            'name' => 'Bekerja dan mendapat form 1721 A1/A2 lebih dari satu pemberi kerja',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('job_statuses')->insert([
            'name' => 'Bekerja tanpa mendapat form 1721 A1/A2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        
        DB::table('job_statuses')->insert([
            'name' => 'Menjalankan usaha/pekerjaan bebas',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
