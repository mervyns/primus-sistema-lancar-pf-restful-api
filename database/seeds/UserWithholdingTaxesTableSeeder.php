<?php

use Illuminate\Database\Seeder;

class UserWithholdingTaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_withholding_taxes')->insert([
            'user_income_id' => 1,
            'withholding_tax_slip_id' => 1,
            'withholder_name' => 'Example01',
            'withholder_npwp' => '11223344',
            'withholding_slip_number' => 'WTH1',
            'withholding_slip_date' => '2017-11-01',
            'value' => 1800000,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
