<?php

use Illuminate\Database\Seeder;

class GatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gateways')->insert([
            'gateway_code' => 'BA01',
            'name' => 'PT BANK MANDIRI (PERSERO) TBK',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('gateways')->insert([
            'gateway_code' => 'BA02',
            'name' => 'PT BANK PAN INDONESIA, TBK',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('gateways')->insert([
            'gateway_code' => 'BA03',
            'name' => 'PT BANK DBS INDONESIA',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
