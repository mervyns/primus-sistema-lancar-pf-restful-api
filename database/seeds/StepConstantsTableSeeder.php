<?php

use Illuminate\Database\Seeder;

class StepConstantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$constants = config('constants')['steps'];
	    foreach ( $constants as $name => $step ) {
		    DB::table('system_step_constants')->insert([
			    'step' => $step,
			    'name' => $name,
		    ]);
        }
    }
}
