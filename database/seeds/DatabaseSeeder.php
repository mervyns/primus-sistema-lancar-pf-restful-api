<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//      DB::table('roles')->truncate();
//      DB::table('users')->truncate();
//	    DB::table('users_roles')->truncate();
        DB::table('asset_categories')->truncate();
        DB::table('assets')->truncate();
        DB::table('banks')->truncate();
        DB::table('security_questions')->truncate();
        DB::table('klasifikasi_lapangan_usahas')->truncate();
        DB::table('dependant_relations')->truncate();
        DB::table('user_dependants')->truncate();
        DB::table('income_categories')->truncate();
        DB::table('tax_rate_types')->truncate();
        DB::table('incomes')->truncate();
        DB::table('countries')->truncate();
        DB::table('investments')->truncate();
        DB::table('gateways')->truncate();
        DB::table('charities')->truncate();
        DB::table('tax_responsibility_statuses')->truncate();
        DB::table('liabilities')->truncate();
        DB::table('withholding_tax_slips')->truncate();
        DB::table('job_statuses')->truncate();
        DB::table('user_security_questions')->truncate();
        DB::table('user_tax_profiles')->truncate();
        DB::table('user_companions')->truncate();
        DB::table('user_revealed_assets')->truncate();
        DB::table('user_repatriated_assets')->truncate();
        DB::table('user_assets')->truncate();
        DB::table('user_incomes')->truncate();
        DB::table('user_foreign_assets')->truncate();
        DB::table('user_liabilities')->truncate();
        DB::table('user_withholding_taxes')->truncate();
        DB::table('system_step_constants')->truncate();
        DB::table('system_steps_config')->truncate();
        DB::table('products')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UsersRolesTableSeeder::class);
        $this->call(AssetCategoriesTableSeeder::class);
        $this->call(AssetsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(SecurityQuestionsTableSeeder::class);
        $this->call(KlasifikasiLapanganUsahaTableSeeder::class);
        $this->call(DependantRelationsTableSeeder::class);
        $this->call(UserDependantsTableSeeder::class);
        $this->call(IncomeCategoriesTableSeeder::class);
        $this->call(TaxRateTypesTableSeeder::class);
        $this->call(IncomesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(InvestmentsTableSeeder::class);
        $this->call(GatewaysTableSeeder::class);
        $this->call(CharitiesTableSeeder::class);
        $this->call(TaxResponsibilityStatusesTableSeeder::class);
        $this->call(LiabilitiesTableSeeder::class);
        $this->call(WithholdingTaxSlipsTableSeeder::class);
        $this->call(JobStatusesTableSeeder::class);
        $this->call(UserSecurityQuestionsTableSeeder::class);
        $this->call(UserTaxProfilesTableSeeder::class);
        $this->call(UserCompanionsTableSeeder::class);
        $this->call(UserRevealedAssetsTableSeeder::class);
        $this->call(UserRepatriatedAssetsTableSeeder::class);
        $this->call(UserAssetsTableSeeder::class);
        $this->call(UserIncomesTableSeeder::class);
        $this->call(UserForeignAssetsTableSeeder::class);
        $this->call(UserLiabilitiesTableSeeder::class);
        $this->call(UserWithholdingTaxesTableSeeder::class);
        $this->call(StepConstantsTableSeeder::class);
        $this->call(StepConfigTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
    }
}
