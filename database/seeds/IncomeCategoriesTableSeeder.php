<?php

use Illuminate\Database\Seeder;

class IncomeCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('income_categories')->insert([
            'name' => 'Penghasilan Utama',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('income_categories')->insert([
            'name' => 'Penghasilan Tambahan Rutin',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('income_categories')->insert([
            'name' => 'Penghasilan Tambahan Sekali Terima',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('income_categories')->insert([
            'name' => 'Penghasilan Atas Barang Mewah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('income_categories')->insert([
            'name' => 'Penghasilan Luar Negeri',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('income_categories')->insert([
            'name' => 'Pengeluaran Atas Penghasilan',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
