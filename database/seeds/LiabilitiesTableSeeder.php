<?php

use Illuminate\Database\Seeder;

class LiabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('liabilities')->insert([
            'liability_code' => '101',
            'name' => 'Utang Bank / Lembaga Keuangan Bukan Bank (KPR, Leasing Kendaraan
Bermotor, dan sejenisnya) - Kendaraan Bermotor',
            'step' => 54,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('liabilities')->insert([
            'liability_code' => '102',
            'name' => 'Kartu Kredit',
            'step' => 20,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('liabilities')->insert([
            'liability_code' => '103',
            'name' => 'Utang Afiliasi',
            'step' => 55,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('liabilities')->insert([
            'liability_code' => '109',
            'name' => 'Hutang Lainnya',
            'step' => 56,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
