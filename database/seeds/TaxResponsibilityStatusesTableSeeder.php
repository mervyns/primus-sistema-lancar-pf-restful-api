<?php

use Illuminate\Database\Seeder;

class TaxResponsibilityStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tax_responsibility_statuses')->insert([
            'code' => 'KK',
            'name' => 'Kepala Keluarga',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_responsibility_statuses')->insert([
            'code' => 'HB',
            'name' => 'Hidup Berpisah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_responsibility_statuses')->insert([
            'code' => 'PH',
            'name' => 'Pisah Harta',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_responsibility_statuses')->insert([
            'code' => 'MT',
            'name' => 'Manajemen Terpisah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('tax_responsibility_statuses')->insert([
            'code' => 'TP',
            'name' => 'Melaporkan Terpisah',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
