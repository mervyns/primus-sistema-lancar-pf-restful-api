<?php

use Illuminate\Database\Seeder;

class UserRevealedAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_revealed_assets')->insert([
            'user_id' => 2,
            'asset_id' => 1,
            'country_id' => 1,
            'value' => 10000000,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

       DB::table('user_revealed_assets')->insert([
            'user_id' => 2,
            'asset_id' => 1,
            'country_id' => 2,
            'value' => 30000,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

       DB::table('user_revealed_assets')->insert([
            'user_id' => 2,
            'asset_id' => 2,
            'country_id' => 4,
            'value' => 40000,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
