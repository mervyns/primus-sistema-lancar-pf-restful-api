<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
        	'name' => 'SINGLE NPWP',
        	'description' => '["Single individuals","Married couples who share one NPWP"]',
        	'invites' => '1',
        	'price' => '109000',
        	'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('products')->insert([
        	'name' => 'SINGLE NPWP',
        	'description' => '["Married couples who have separate NPWP","Married couples who have Separate Asset management or Separate Assets (PH/MT)"]',
        	'invites' => '2',
        	'price' => '149000',
        	'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('products')->insert([
        	'name' => 'MULTIPLE NPWP',
        	'description' => '["Sharing with friends and family"]',
        	'invites' => '4',
        	'price' => '199000',
        	'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
