<?php

use Illuminate\Database\Seeder;

class UserIncomesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_incomes')->insert([
            'user_id' => 2,
            'income_id' => 1,
            'country_id' => 1,
            'net_value' => 36000000,
            'gross_value' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('user_incomes')->insert([
            'user_id' => 2,
            'income_id' => 2,
            'country_id' => 1,
            'user_asset_id' => 2,
            'net_value' => 12000000,
            'gross_value' => 0,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
