<?php

use Illuminate\Database\Seeder;

class UserSecurityQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_security_questions')->insert([
            'user_id' => 2,
            'security_question_id' => 1,
            'answer' => 'Susi',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('user_security_questions')->insert([
            'user_id' => 2,
            'security_question_id' => 3,
            'answer' => 'Blackie',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
