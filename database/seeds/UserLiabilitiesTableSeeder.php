<?php

use Illuminate\Database\Seeder;

class UserLiabilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_liabilities')->insert([
            'user_id' => 2,
            'liability_id' => 2,
            'acquisition_year' => 2017,
            'value' => 12000000,
            'lender_name' => 'Bank Indonesia',
            'lender_address' => 'Indonesia',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
