<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('companies')->insert([
            // PERUSAHAAN AGEN REKSADANA
            [
                'name' => 'Citibank N.A. Cabang Indonesia',
                'address' => 'Citibank Tower lantai 9, Jl. Jend. Sudirman Kav. 54-55',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Aldiracita Sekuritas Indonesia',
                'address' => 'Sinarmas Land Plaza Menara 3 Lantai 11, Jalan MH Thamrin No 51 Kav. 22',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Sinarmas Tbk',
                'address' => 'Sinarmas Land Plaza Jl. MH. Thamrin No. 51',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Syariah Mandiri',
                'address' => 'Wisma Mandiri, Jl. M.H. Thamrin No. 5',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank ANZ Indonesia',
                'address' => 'ANZ Tower Lantai UG, Jl. Jend. Sudirman Kav. 33A, Retail & Wealth',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Bukopin Tbk',
                'address' => 'Gedung Bank Bukopin, Jl. Haryono MT. Kav. 50-51',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Central Asia Tbk',
                'address' => 'Menara BCA Grand Indonesia, Jl. M.H. Thamrin No.1, Unit Bisnis Wealth Management',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank CIMB Niaga Tbk',
                'address' => 'Menara Sentraya Lantai 30, Jl. Iskandarsyah I Blok M, Kebayoran Baru',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Commonwealth',
                'address' => 'Wisma Metropolitan II – 2nd Floor, Jl. Sudirman Kav. 29 – 31',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank CTBC Indonesia',
                'address' => 'Tamara Center, Lantai 15, Jalan Jendral Sudirman Kav. 24',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Danamon Tbk',
                'address' => 'Gedung Plaza Kuningan, Menara Utara Lantai 8, Jl. HR Rasuna Said Kav. C 11-14 Kuningan',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank DBS Indonesia',
                'address' => 'DBS Bank Tower, Lantai 33–37, Ciputra World 1, Jl. Prof. Dr. Satrio Kav. 3-5',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank HSBC Indonesia',
                'address' => 'Kantor Pusat World trade Center 1, Lantai 8, & 9, Jalan Jend. Sudirman Kav. 29-31',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank KEB Hana Indonesia',
                'address' => 'Wisma Mulia 32 & 52 Floor, Jalan Jend. Gatot Subroto No. 42',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Mandiri (Persero) Tbk',
                'address' => 'Wisma Mandiri II Lantai 8, Jl. Kebon Sirih No. 83',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Maybank Indonesia Tbk',
                'address' => 'Wealth Management & Funding Business, Gedung Sentral Senayan Lantai 19, Jl. Asia Afrika No. 8, Senayan - Gelora Bung Karno',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Mayora',
                'address' => 'Jalan Tomang Raya Kav. 21-23',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Mega Tbk',
                'address' => 'Menara Bank Mega, Jalan Kapten Tendean No. 12-14A, Divisi Liabilities Product Management',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Nationalnobu Tbk',
                'address' => 'Plaza Semanggi Lantai 9, Jalan Jendral Sudirman Kav. 50',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Negara Indonesia (Persero) Tbk',
                'address' => 'Gedung BNI 46 Lt. 23, Jl. Jend. Sudirman Kav. 1',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank OCBC NISP Tbk',
                'address' => 'OCBC NISP Tower, Jl. Prof.Dr Satrio Kav. 25',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Pan Indonesia Tbk',
                'address' => 'Gedung Bank Panin Pusat, Jl. Jend. Sudirman Senayan',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Pembangunan Daerah Jawa Barat dan Banten Tbk (bank bjb)',
                'address' => 'Divisi Dana dan Jasa Konsumer, Jl. Naripan No. 12 – 14',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Permata Tbk',
                'address' => 'PermataBank Tower III Lantai 5, Jl. MH. Thamrin Blok B1 No. 1 Bintaro Jaya Sektor VII',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank QNB Indonesia Tbk',
                'address' => 'QNB Kesawan Tower 18 Parc SCBD, Jalan Jend. Sudirman Kav. 52-53',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Rakyat Indonesia (Persero) Tbk',
                'address' => 'Jl. Jend. Sudirman No. 44-46',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Tabungan Negara (Persero) Tbk',
                'address' => 'Menara Bank BTN, Jl. Gajah Mada No. 1 Petojo Utara, Gambir',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank Tabungan Pensiunan Nasional Tbk',
                'address' => 'Menara BTPN, CBD Mega Kuningan, Jalan Dr. Ide Anak Agung Gde Agung Kav. 5.5 – 5.6',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bank UOB Indonesia',
                'address' => 'UOB Plaza, Jl. M.H. Thamrin No. 10',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bareksa Portal Investasi',
                'address' => 'Wisma Lembawai, Jl. Bangka Raya No.27 G-H, Kemang, Mampang Prapatan',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bibit Tumbuh Bersama',
                'address' => 'Gedung Kimia Sakti Kalista Lantai 3A Jalan Siantar No.15 Cideng, Jakarta Pusat 10150 Telp. 021 3855656 Faks. 021 3855655 / 3806422',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT BNI Sekuritas',
                'address' => 'Sudirman Plaza, Indofood Tower Lantai 16 Jalan Jend. Sudirman Kav. 76-78',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Ciptadana Sekuritas Asia',
                'address' => 'Plaza Asia (d/h ABDA) Office Park unit 2, Jalan Jend. Sudirman Kav. 59',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Danareksa Sekuritas',
                'address' => 'Jalan Medan Merdeka Selatan No. 14',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Henan Putihrai Sekuritas',
                'address' => 'Penthouse @Tamara Centre, Jalan Jenderal Sudirman 24',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Indo Capital Sekuritas',
                'address' => 'Jalan Persatuan Guru No. 41 A, Petojo Selatan',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Indo Premier Sekuritas',
                'address' => 'Wisma GKBI Lt. 7 Suite 718, Jl. Jend. Sudirman No. 28',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Investamart Principal Optima',
                'address' => 'Gedung Total Lt. 10 Jalan Letnan Jenderal S. Parman Kav. 106 A Jakarta Barat 11440',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Mandiri Sekuritas',
                'address' => 'Gedung Plaza Mandiri Lantai Dasar & Lantai 29, Jl. Jend. Gatot Subroto Kav. 36-38',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Mega Capital Sekuritas',
                'address' => 'Menara Bank Mega Lantai 2, Jl. Kapten P. Tendean Kav. 12-14 A',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Mirae Asset Sekuritas Indonesia',
                'address' => 'Equity Tower Lantai 50 Unit A,B,D&E SCBD Lot 9 Jalan Jenderal Sudirman Kav. 52-53 Jakarta Selatan 12190',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT MNC Sekuritas',
                'address' => 'MNC Financial Center , Lantai 14-16, Jalan Kebon Sirih No. 21-27, Jakarta 10340',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Nusantara Sejahtera Investama',
                'address' => 'Jalan Batu Tulis Raya No. 24 A RT/RW: 003/002, Kebon Kelapa Gambir',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Phillip Sekuritas Indonesia',
                'address' => 'ANZ Tower (d/h Wisma Standard Chartered Bank), Lt 23 B, Jl. Jend. Sudirman Kav 33 A',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Reliance Sekuritas Indonesia Tbk',
                'address' => 'Reliance Capital Building Jalan Pluit Sakti Raya No. 27 AB Pluit Jakarta 14450',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT RHB Sekuritas Indonesia',
                'address' => 'Gedung Wisma Mulia Lantai 20, Jl. Jend. Gatot Subroto No. 42',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Star Mercato Capitale',
                'address' => 'Aditya Medical Centre, 2nd Floor Unit 203 Jalan Adityawarman I No. 14 Melawai Kebayoran Baru',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Sucor Sekuritas',
                'address' => 'Gedung Sahid Sudirman Center LT. 12 Unit A, E, F, G Jalan Jend. Sudirman No. 86 Karet Tengsin, Tanah Abang',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Supermarket Reksa Dana Indonesia',
                'address' => 'Citilofts Sudirman Lt. 18 unit 1805 ,Jalan KH. Mas Mansyur Kav. 121',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Trimegah Sekuritas Indonesia Tbk',
                'address' => 'Gedung Artha Graha, Lt. 18, 19 & 31, Jl. Jend Sudirman Kav. 52-53',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Xdana Investa Indonesia',
                'address' => 'Sahid Sudirman Residence (LG Floor 06A), Jalan Jend. Sudirman No. 86',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Yuanta Sekuritas Indonesia',
                'address' => 'Equity Tower 10Th Floor, Unit E, F, G, H Jalan Jend. Sudirman Kav. 52-53',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'Standard Chartered Bank',
                'address' => 'Gedung World Trade Center II Lt. 5, Jalan Jenderal Sudirman Kav. 29-31',
                'type' => 'reksadana',
                'detail' => '[]',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],


            // PERUSAHAAN SEKURITAS
            [
                'name' => 'PT Aldiracita Sekuritas Indonesia',
                'address' => 'Sinarmas Land, MenaraI II Lantai 11, Jalan MH. Thamrin No. 51 Kav.22, Jakarta 10350, Telp. (021) 39834551, Fax. (021) 39834559 / 39834560',
                'type' => 'sekuritas',
                'detail' => json_encode([
                    'ab' => 'AB',
                    'pe_1' => 'PEE',
                    'pe_2' => 'PPE',
                    'mi' => '',
                    'pee_no_1' => 'KEP/01/PM/PEE/2003 14 Januari 2003',
                    'pee_no_2' => 'KEP-200/PM/1992 10 April 1992',
                    'skmi' => '',
                ]),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Amantara Sekuritas Indonesia',
                'address' => 'Sinar Mas Land Plaza Menara 3 Lantai 11, Jl. M.H Thamrin No. 51, Jakarta 10350, Telp. (021) 3929601-218-228, Fax. (021) 3929638-9588',
                'type' => 'sekuritas',
                'detail' => json_encode([
                    'ab' => 'AB',
                    'pe_1' => 'PEE',
                    'pe_2' => 'PEE',
                    'mi' => '',
                    'pee_no_1' => 'KEP.05/PM/PEE/96 8 Maret 1996',
                    'pee_no_2' => 'Kep-192/PM/1992 10 April 1992',
                    'skmi' => '',
                ]),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Anugerah Sekuritas Indonesia',
                'address' => 'Komp. Ruko Cempaka Mas  Blok M No. 1,2,3 Cempaka Putih, Jalan Letjen Suprapto - Cempaka Mas, Jakarta Pusat 10640, Telp. (021) 42800433, Fax. (021) 42800432',
                'type' => 'sekuritas',
                'detail' => json_encode([
                    'ab' => 'AB',
                    'pe_1' => '',
                    'pe_2' => 'PEE',
                    'mi' => '',
                    'pee_no_1' => '',
                    'pee_no_2' => 'KEP-100/PM/1992 03 Maret 1992',
                    'skmi' => '',
                ]),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Artha Sekuritas Indonesia',
                'address' => 'Equity Tower, Lantai 22 E-F, SCBD Lot. 9, Jl. Jend. Sudirman Kav. 52-53, Jakarta 12910, Telp. (021) 5152338, Fax. (021) 5152339',
                'type' => 'sekuritas',
                'detail' => json_encode([
                    'ab' => 'AB',
                    'pe_1' => 'PEE',
                    'pe_2' => 'PEE',
                    'mi' => '',
                    'pee_no_1' => 'KEP-01/BL/PEE/2006 26 Juli 2006',
                    'pee_no_2' => '',
                    'skmi' => '',
                ]),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
            [
                'name' => 'PT Bahana Sekuritas',
                'address' => 'Graha CIMB Niaga Lantai 19, Jalan Jenderal Sudirman Kav. 58, Jakarta 12190, Telp. (021) 2505080, Fax. (021) 2505070-71',
                'type' => 'sekuritas',
                'detail' => json_encode([
                    'ab' => 'AB',
                    'pe_1' => 'PEE',
                    'pe_2' => 'PEE',
                    'mi' => '',
                    'pee_no_1' => 'KEP-38/PM/1993 12 November 1993',
                    'pee_no_2' => 'KEP-51/PM/1992 20 Februari 1992',
                    'skmi' => '',
                ]),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ],
        ]);
    }
}
