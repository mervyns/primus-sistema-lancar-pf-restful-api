<?php

use Illuminate\Database\Seeder;

class TaxRateTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 4 ayat 2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 21',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 23',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 24',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 17',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('tax_rate_types')->insert([
            'name' => 'PPh Pasal 22',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
