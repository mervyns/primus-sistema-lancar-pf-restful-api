<?php

use Illuminate\Database\Seeder;

class UserTaxProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_tax_profiles')->insert([
            'user_id' => 2,
            'nik' => '1234567890',
            'npwp' => '0987654321',
            'efin' => '0912345678',
            'job_status_id' => 2,
            'companion_has_npwp' => 1,
            'klu_id' => 2,
            'tax_responsibility_status_id' => 1,
            'marital_status' => 1,
            'have_dependant' => 1,
            'have_vehicle' => 1,
            'have_property' => 1,
            'have_insurance' => 1,
            'have_foreign_asset' => 1,
            'following_tax_amnesty_2016' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
