<?php

use Illuminate\Database\Seeder;

class UserRepatriatedAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_repatriated_assets')->insert([
            'user_revealed_asset_id' => 2,
            'investment_id' => 2,
            'gateway_id' => 1,
            'will_repatriated_value' => 150000000,
            'has_repatriated_value' => 50000000,
            'investment_date' => '2017-01-15',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
