<?php

use Illuminate\Database\Seeder;

class UserAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_assets')->insert([
            'user_id' => 2,
            'asset_id' => 1,
            'user_revealed_asset_id' => 1,
            'acquisition_year' => 2017,
            'idr_value' => 10000000,
            'is_ta' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('user_assets')->insert([
            'user_id' => 2,
            'asset_id' => 29,
            'acquisition_year' => 2015,
            'idr_value' => 300000000,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
