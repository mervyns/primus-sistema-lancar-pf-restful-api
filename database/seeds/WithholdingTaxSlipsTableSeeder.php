<?php

use Illuminate\Database\Seeder;

class WithholdingTaxSlipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('withholding_tax_slips')->insert([
            'name' => '1721 A1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => '1721 A2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => '1721 P1',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => 'Bukti Potong PPh Pasal 23',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => 'Bukti Potong PPh Pasal 4 Ayat 2',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => 'Bukti Potong PPh Pasal 22',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => 'Bukti Potong PPh Pasal 24',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('withholding_tax_slips')->insert([
            'name' => 'Bukti Potong PPh Pasal 21',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
