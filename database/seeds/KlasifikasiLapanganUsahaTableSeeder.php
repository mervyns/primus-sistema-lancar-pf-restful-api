<?php

use Illuminate\Database\Seeder;

class KlasifikasiLapanganUsahaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('klasifikasi_lapangan_usahas')->insert([
            'klu_number' => '96301',
            'name' => 'Pegawai Negeri Sipil',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('klasifikasi_lapangan_usahas')->insert([
            'klu_number' => '96304',
            'name' => 'Pegawai Swasta',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
