process.traceDeprecation = true;

var webpack = require('webpack');
var path = require('path');
var copyWebpackPlugin = require('copy-webpack-plugin');
var extractTextPlugin = require('extract-text-webpack-plugin');
var MinifyPlugin = require('babel-minify-webpack-plugin');
var SweetAlert = require('sweetalert');

var inProject = path.resolve.bind(path, __dirname);
var inProjectSrc = function(file) { return inProject('resources/assets', file) };
var config = {
    entry: [
        inProjectSrc('entry.js'),
        __dirname + '/resources/assets/css/main.scss',
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: "main.js"
    },
    plugins: [
        new copyWebpackPlugin([
            { from: __dirname + '/resources/assets/images', to: __dirname + '/public/images' }
        ]),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        }),
        new extractTextPlugin({
            filename: "styles.css", disable: false, allChunks: true
        }),
        new MinifyPlugin()
    ],
    resolve: {
        modules: [
            'node_modules'
        ]
    },
    module: {
        rules: [
            {
                test: /\.(scss|sass)$/,
                use: extractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader!sass-loader"
                })
            },
            // {
            //     test: /\.js$/,
            //     loader: 'babel-loader',
            //     query: {
            //         presets: ['es2015']
            //     }
            // },
            // {
            //     test: /\.svg$/,
            //     loader: 'svg-inline-loader'
            // },
            {
                test: /\.(otf|eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            }
        ]
    }
};

module.exports = config;