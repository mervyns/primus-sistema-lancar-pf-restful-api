<?php

return [
    'namespaces' => [
        'Api' => base_path() . DIRECTORY_SEPARATOR . 'api',
        'WebApp' => base_path() . DIRECTORY_SEPARATOR . 'app',
        'Infrastructure' => base_path() . DIRECTORY_SEPARATOR . 'infrastructure'
    ],

    'protection_middleware' => [
	    'web',
//        'auth:api',
    ],

    'resource_namespace' => 'Resources',

    'language_folder_name' => 'lang',

    'view_folder_name' => 'views'
];
