<?php

$steps = config('constants')['steps'];

return[
	/* 1. START FILLING */
	$steps['START_FILLING'] => [
		'title' => 'Mulai Isi SPT Anda',
		'subtitle' => 'Silahkan klik tombol di bawah ini untuk mulai melakukan pengisian SPT pribadi Anda.',
		'icon' => false,
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => false,
		'previous' => false,
		'next' => $steps['WHAT_DESCRIBE_ME'],
		'view' => 'start-filling',
		'url' => '/tax-filling',
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	/* 2. PROFIL PERPAJAKAN */

	$steps['WHAT_DESCRIBE_ME'] => [
		'title' => 'Profil Perpajakan',
		'subtitle' => 'Welcome! Kita akan mulai dengan profil perpajakan Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => false,
		'next' => $steps['MY_TAX_PROFILE'],
		'view' => 'what-describe-me',
		'url' => '/tax-filling/' . $steps['WHAT_DESCRIBE_ME'],
		'validations' => [
			'rules' => [
				'status' => 'required',
				'spouse_has_tax_id' => 'required_if:status,2',
				'spouse_taxing_status' => 'required_if:status,2',
				'2016_ta_participation' => 'required',
				'pengungkapan' => 'required_if:2016_ta_participation,1',
				'have_foreign_asset' => 'required',
			],
			'messages' => [
				'status.required' => 'Anda harus memilih salah satu status pernikahan Anda di kolom <strong>Status Saya</strong>.',
				'spouse_has_tax_id.required_if' => 'Anda harus memilih status NPWP pasangan Anda di kolom <strong>Suami/Istri Saya</strong>.',
				'spouse_taxing_status.required_if' => 'Anda harus memilih salah satu kewajiban perpajakan pasangan pada kolom <strong>Status Kewajiban Perpajakan Suami-Istri</strong>.',
				'2016_ta_participation.required' => 'Anda harus memilih salah satu status keikutsertaan Anda pada Program Tax Amnesty 2016 di kolom <strong>Program Tax Amnesty 2016</strong>.',
				'pengungkapan.required_if' => 'Anda harus memilih salah satu status pengungkapan tax amnersty <strong> Tax Amnesty Pengungkapan </strong>.',
				'have_foreign_asset.required' => 'Anda harus memilih apakah mempunyai atau tidak mempunyai asset luar negeri di kolom <strong>Asset Luar Negeri</strong>.',
			],
		],
		'nested' => '1.2',
	],

	$steps['MY_TAX_PROFILE'] => [
		'title' => 'Profil  Identitas',
		'subtitle' => 'Silahkan isi informasi perpajakan Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['WHAT_DESCRIBE_ME'],
		'next' => $steps['MY_WIFE_TAX_PROFILE'],
		'view' => 'my-tax-profile',
		'url' => '/tax-filling/' . $steps['MY_TAX_PROFILE'],
		'validations' => [
			'rules' => [
				'npwp' => 'required_if:fillnpwp,null|digits:15',
				'efin' => 'required_if:fillefin,null|digits:10',
				'jobstatus' => 'required|integer',
				'klu' => 'required|integer',
			],
			'messages' => [
				'npwp.required_if' => '<strong>NPWP Kosong! </strong> NPWP harus berisi 15 digit angka.',
				'npwp.digits' => '<strong>NPWP Salah! </strong> NPWP harus berisi 15 digit angka.',
				'efin.required_if' => '<strong>EFIN Kosong! </strong> EFIN harus berisi 10 digit angka.',
				'efin.digits' => '<strong>EFIN Salah!</strong> EFIN harus berisi 10 digit angka.',
				'jobstatus.required' => 'Silahkan pilih keterangan pekerjaan anda',
				'klu.required' => 'Silahkan pilih jenis pekerjaan anda',
			],
		],
		'nested' => '1.3',
	],

	$steps['MY_WIFE_TAX_PROFILE'] => [
		'title' => 'Profil Identitas Istri',
		'subtitle' => 'Silahkan isi profil istri Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MY_TAX_PROFILE'],
		'next' => $steps['SPOUSE_INDICATOR'],
		'view' => 'my-wife-tax-profile',
		'url' => '/tax-filling/' . $steps['MY_WIFE_TAX_PROFILE'],
		'validations' => [
			'rules' => [
				'name' => 'required',
				'nik' => 'required_if:fillktp,null|digits:16',
				'jobstatus' => 'required',
				'klu' => 'required_unless:jobstatus,1'
			],
			'messages' => [
				'name.required' => '<strong>Nama Kosong!</strong> Silahkan isi nama istri Anda.',
				'nik.required_if' => '<strong>NIK Kosong! </strong>Silahkan isi nomor KTP istri Anda (16 digit angka).',
				'nik.digit' => '<strong>NIK Salah!</strong> Silahkan isi nomor KTP istri Anda (16 digit angka).',
				'jobstatus.required' => 'Silahkan pilih status pekerjaan istri Anda.',
				'klu.required_unless' => '<strong>Pekerjaan Belum Dipilih! </strong> Silahkan isi pekerjaan istri Anda.',
			],
		],
		'nested' => '1.4',
	],

	$steps['MY_HUSBAND_TAX_PROFILE'] => [
		'title' => 'Profil Identitas Suami',
		'subtitle' => 'Silahkan isi profil suami Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MY_TAX_PROFILE'],
		'next' => $steps['SPOUSE_INDICATOR'],
		'view' => 'my-husband-tax-profile',
		'url' => '/tax-filling/' . $steps['MY_HUSBAND_TAX_PROFILE'],
		'validations' => [
			'rules' => [
				'name' => 'required',
				'nik' => 'required_if:fillnik,null|digits:16',
				'jobstatus' => 'required'
			],
			'messages' => [
				'name.required' => '<strong>Nama Kosong!</strong> Silahkan isi nama suami Anda.',
				'nik.required_if' => '<strong>NIK Kosong! </strong>Silahkan isi nomor KTP suami Anda (16 digit angka).',
				'nik.digit' => '<strong>NIK Salah!</strong> Silahkan isi nomor KTP suami Anda (16 digit angka).',
				'jobstatus.required' => 'Silahkan pilih status pekerjaan suami Anda.',
			],
		],
		'nested' => '1.5',
	],

	$steps['SPOUSE_INDICATOR'] => [
		'title' => '',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => false,
		'previous' => $steps['DEPENDANTS'],
		'next' => $steps['SPOUSE_INDICATOR'],
		'view' => 'spouse-indicator',
		'url' => '/tax-filling/' . $steps['SPOUSE_INDICATOR'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	$steps['MY_SPOUSE_TAX_PROFILE'] => [
		'title' => 'Profil Perpajakan Istri/Suami',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SPOUSE_INDICATOR'],
		'next' => $steps['MY_SPOUSE_TAX_PROFILE'],
		'view' => 'my-spouse-tax-profile',
		'url' => '/tax-filling/' . $steps['MY_SPOUSE_TAX_PROFILE'],
		'validations' => [
			'rules' => [
				'npwp' => 'required_if:fillnpwp,null|digits:15',
				'jobstatus' => 'required|integer',
				'klu' => 'required|integer',
			],
			'messages' => [
				'npwp.required_if' => '<strong>NPWP Kosong! </strong> NPWP harus berisi 15 digit angka.',
				'npwp.digits' => '<strong>NPWP Salah! </strong> NPWP harus berisi 15 digit angka.',
				'jobstatus.required' => 'Silahkan pilih keterangan pekerjaan anda',
				'klu.required' => 'Silahkan pilih jenis pekerjaan anda',
			],
		],
		'nested' => '1.7',
	],

	$steps['DEPENDANTS'] => [
		'title' => 'Tanggungan',
		'subtitle' => 'Silahkan isi informasi tanggungan Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MY_SPOUSE_TAX_PROFILE'],
		'next' => $steps['MAIN_INCOME'],
		'view' => 'dependants',
		'url' => '/tax-filling/' . $steps['DEPENDANTS'],
		'validations' => [
			'rules' => [
				'dependant-relation.*' => 'required_with:name.*', 
				'job.*' => 'required_with:dependant-relation.*',	
				'nik.*' => 'required_with:name.*|required_if:nothavektp.*, null',
			],
			'messages' => [
				'dependant-relation.*.required_with' => '<strong>Hubungan Belum Dipilih! </strong> Silahkan pilih opsi <strong>Hubungan</strong>.',
				'job.*.required_with' => '<strong>Pekerjaan Belum Dipilih! </strong> Silahkan pilih opsi <strong>Pekerjaan</strong>.',
				'nik.*.required_with' => '<strong>NIK Kosong! </strong> Silahkan isi NIK sesuai tanggungan anda.',
				'nik.*.required_if' => '<strong>NIK Kosong! </strong> Silahkan isi NIK sesuai tanggungan anda.',
			],
		],
		'nested' => '1.8',
	],

	/* 3. PENGHASILAN UTAMA */

	$steps['MAIN_INCOME'] => [
		'title' => 'Penghasilan Utama',
		'subtitle' => 'Sekarang, kita akan mengisi bagian penghasilan utama anda. <br> Informasi Penghasilan Utama Anda dari form 1721-A1/ 1721-A2 (Tahunan)',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DEPENDANTS'],
		'next' => $steps['MY_SPOUSE_INCOME_OPTION_1'],
		'view' => 'main-income',
		'url' => '/tax-filling/' . $steps['MAIN_INCOME'],
		'validations' => [
			'rules' => [
				
			],
			'messages' => [
				
			],
		],
		'nested' => '2.9',
	],

	$steps['MY_SPOUSE_INCOME_OPTION_1'] => [
		'title' => 'Penghasilan Istri/Suami',
		'subtitle' => '<h6>Satu 1721-A1 / 1721-A2</h6>Informasi Penghasilan Pasangan Anda <strong>dari satu</strong> Pemberi Kerja',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MAIN_INCOME'],
		'next' => $steps['OVERSEAS_INCOME_PROFILE'],
		'view' => 'my-spouse-income-option-1',
		'url' => '/tax-filling/' . $steps['MY_SPOUSE_INCOME_OPTION_1'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '2.10',
	],

	$steps['MY_SPOUSE_INCOME_OPTION_2'] => [
		'title' => 'Penghasilan Istri/Suami',
		'subtitle' => '<h6>Tanpa Mendapat 1721-A1 / 1721-A2</h6>Informasi Penghasilan Pasangan Anda dari Pemberi Kerja',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MAIN_INCOME'],
		'next' => $steps['OVERSEAS_INCOME_PROFILE'],
		'view' => 'my-spouse-income-option-2',
		'url' => '/tax-filling/' . $steps['MY_SPOUSE_INCOME_OPTION_2'],
		'validations' => [
			'rules' => [

			],
			'messages' => [],
		],
		'nested' => '2.11',
	],

	$steps['MY_SPOUSE_INCOME_OPTION_3'] => [
		'title' => 'Penghasilan Istri/Suami',
		'subtitle' => '<h6>Lebih dari Satu 1721-A1 / 1721-A2</h6>Informasi Penghasilan Pasangan Anda <strong>lebih dari satu</strong> Pemberi Kerja',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MAIN_INCOME'],
		'next' => $steps['OVERSEAS_INCOME_PROFILE'],
		'view' => 'my-spouse-income-option-3',
		'url' => '/tax-filling/' . $steps['MY_SPOUSE_INCOME_OPTION_3'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '2.12',
	],

	$steps['MY_SPOUSE_INCOME_OPTION_4'] => [
		'title' => 'Penghasilan Istri/Suami',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MAIN_INCOME'],
		'next' => $steps['OVERSEAS_INCOME_PROFILE'],
		'view' => 'my-spouse-income-option-4',
		'url' => '/tax-filling/' . $steps['MY_SPOUSE_INCOME_OPTION_4'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '2.13',
	],

	/* 4. PENGHASILAN LUAR NEGERI */

	$steps['OVERSEAS_INCOME_PROFILE'] => [
		'title' => '',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => false,
		'previous' => $steps['CASH'],
		'next' => $steps['OVERSEAS_INCOME'],
		'view' => 'overseas-income-profile',
		'url' => '/tax-filling/' . $steps['OVERSEAS_INCOME_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	$steps['OVERSEAS_INCOME'] => [
		'title' => 'Penghasilan Luar Negeri',
		'subtitle' => 'Silahkan mengisi penghasilan luar negeri Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MY_SPOUSE_INCOME_OPTION_4'],
		'next' => $steps['CASH'],
		'view' => 'overseas-income',
		'url' => '/tax-filling/' . $steps['OVERSEAS_INCOME'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '2.15',
	],

	/* 5. UANG TUNAI DAN NON TUNAI */

	$steps['CASH'] => [
		'title' => 'Uang Tunai',
		'subtitle' => 'Kita mulai tahap pengisian daftar Asset dan Hutang yang di kategorikan dari dasar kecairan. Harap <br> <strong>juga</strong> mengisi daftar Asset dari program Tax Amnesty dan Pas Final yang telah disetujui KPP.<br> Kita mulai dengan Uang Tunai termasuk E-Money.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OVERSEAS_INCOME'],
		'next' => $steps['BANK'],
		'view' => 'cash',
		'url' => '/tax-filling/' . $steps['CASH'],
		'validations' => [
			'rules' => [
				'currency.*' => 'required|numeric',
				'kurs_pajak.*' => 'required|numeric',
				'jumlah_rupiah.*' => 'required|numeric',
			],
			'messages' => [
				'currency.*.required' => 'Silahkan mengisi <strong> Uang Tunai</strong>.',
				'currency.*.numeric' => 'Silahkan mengisi <strong> Uang Tunai</strong>.',
				'kurs_pajak.*.required' => '<strong>Kurs Pajak Kosong!</strong> Silahkan memilih mata uang.',
				'kurs_pajak.*.numeric' => '<strong>Kurs Pajak Kosong!</strong> Silahkan memilih mata uang.',
				'jumlah_rupiah.*.required' => '<strong>Jumlah Rupiah Kosong!</strong> Silahkan memilih mata uang.',
				'jumlah_rupiah.*.numeric' => '<strong>Jumlah Rupiah Kosong!</strong> Silahkan memilih mata uang.',
			],
		],
		'nested' => '3.16',
	],

	$steps['BANK'] => [
		'title' => 'Tabungan',
		'subtitle' => 'Silahkan isi informasi tabungan yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CASH'],
		'next' => $steps['COMMON_ASSETS'],
		'view' => 'bank',
		'url' => '/tax-filling/' . $steps['BANK'],
		'validations' => [
			'rules' => [
				'currency.*' => 'required|numeric',
				'kurs_pajak.*' => 'required|numeric',
				'jumlah_rupiah.*' => 'required|numeric',
			],
			'messages' => [
				'currency.*.required' => 'Silahkan mengisi <strong> Uang Tunai</strong>.',
				'currency.*.numeric' => 'Silahkan mengisi <strong> Uang Tunai</strong>.',
				'kurs_pajak.*.required' => '<strong>Kurs Pajak Kosong!</strong> Silahkan memilih mata uang.',
				'kurs_pajak.*.numeric' => '<strong>Kurs Pajak Kosong!</strong> Silahkan memilih mata uang.',
				'jumlah_rupiah.*.required' => '<strong>Jumlah Rupiah Kosong!</strong> Silahkan memilih mata uang.',
				'jumlah_rupiah.*.numeric' => '<strong>Jumlah Rupiah Kosong!</strong> Silahkan memilih mata uang.',
			],
		],
		'nested' => '3.17',
	],

	/* 6. ASET UMUM */

	$steps['COMMON_ASSETS'] => [
		'title' => 'Common Assets',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['BANK'],
		'next' => $steps['DEPOSIT'],
		'view' => 'common-assets',
		'url' => '/tax-filling/' . $steps['COMMON_ASSETS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18',
	],

	$steps['DEPOSIT'] => [
		'title' => 'Deposito',
		'subtitle' => 'Silahkan isi informasi Deposito yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['COMMON_ASSETS'],
		'next' => $steps['CREDIT_CARD'],
		'view' => 'deposit',
		'url' => '/tax-filling/' . $steps['DEPOSIT'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.19',
	],

	$steps['CREDIT_CARD'] => [
		'title' => 'Kartu Kredit',
		'subtitle' => 'Silahkan isi informasi kartu kredit yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DEPOSIT'],
		'next' => $steps['VEHICLE'],
		'view' => 'credit-card',
		'url' => '/tax-filling/' . $steps['CREDIT_CARD'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.20',
	],

	$steps['VEHICLE'] => [
		'title' => 'Kendaraan',
		'subtitle' => 'Silahkan isi informasi kendaraan yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CREDIT_CARD'],
		'next' => $steps['VEHICLE_LIABILITY'],
		'view' => 'vehicle',
		'url' => '/tax-filling/' . $steps['VEHICLE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.21',
	],

	$steps['VEHICLE_LIABILITY'] => [
		'title' => 'Kredit Kendaraan',
		'subtitle' => 'Silahkan isi informasi kredit kendaraan yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['VEHICLE'],
		'next' => $steps['PROPERTY'],
		'view' => 'vehicle-liability',
		'url' => '/tax-filling/' . $steps['VEHICLE_LIABILITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.22',
	],
	
	$steps['PROPERTY'] => [
		'title' => 'Properti',
		'subtitle' => 'Silahkan isi informasi properti yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['VEHICLE_LIABILITY'],
		'next' => $steps['PROPERTY_LIABILITY'],
		'view' => 'property',
		'url' => '/tax-filling/' . $steps['PROPERTY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.23',
	],

	$steps['PROPERTY_LIABILITY'] => [
		'title' => 'Kredit Properti',
		'subtitle' => 'Silahkan isi informasi kredit properti yang anda miliki.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PROPERTY'],
		'next' => $steps['PRECIOUS_ASSETS_PROFILE'],
		'view' => 'property-liability',
		'url' => '/tax-filling/' . $steps['PROPERTY_LIABILITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.24',
	],

	$steps['PRECIOUS_ASSETS_PROFILE'] => [
		'title' => 'Barang Berharga',
		'subtitle' => 'Silahkan pilih salah satu cara pelaporan barang berharga Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PROPERTY_LIABILITY'],
		'next' => $steps['PRECIOUS_ASSETS_GLOBAL'],
		'view' => 'precious-assets-profile',
		'url' => '/tax-filling/' . $steps['PRECIOUS_ASSETS_PROFILE'],
		'validations' => [
			'rules' => [
				'precious-asset' => 'required'
			],
			'messages' => [
				'precious-asset.required' => 'Silahkan pilih pelaporan barang berharga.'
			],
		],
		'nested' => '3.18.25',
	],

	$steps['PRECIOUS_ASSETS_GLOBAL'] => [
		'title' => 'Barang Berharga Global',
		'subtitle' => 'Silahkan isi pelaporan barang berharga global Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PRECIOUS_ASSETS_PROFILE'],
		'next' => $steps['PRECIOUS_ASSETS_DETAIL'],
		'view' => 'precious-assets-global',
		'url' => '/tax-filling/' . $steps['PRECIOUS_ASSETS_GLOBAL'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.26',
	],

	$steps['PRECIOUS_ASSETS_DETAIL'] => [
		'title' => 'Barang Berharga Detail',
		'subtitle' => 'Silahkan isi pelaporan barang berharga detail Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PRECIOUS_ASSETS_GLOBAL'],
		'next' => $steps['FINANCIAL_INVESTMENTS_PROFILE'],
		'view' => 'precious-assets-detail',
		'url' => '/tax-filling/' . $steps['PRECIOUS_ASSETS_DETAIL'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.18.27',
	],

	/* 7. INVESTASI FINANSIAL */

	$steps['FINANCIAL_INVESTMENTS_PROFILE'] => [
		'title' => 'Investasi Finansial',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PRECIOUS_ASSETS_DETAIL'],
		'next' => $steps['MUTUAL_FUNDS'],
		'view' => 'financial-investments-profile',
		'url' => '/tax-filling/' . $steps['FINANCIAL_INVESTMENTS_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28',
	],

	$steps['EXCHANGE_STOCK_PROFILE'] => [
		'title' => 'Saham di Bursa Efek (Perusahaan Sekuritas)',
		'subtitle' => 'Silahkan pilih cara pelaporan saham anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['FINANCIAL_INVESTMENTS_PROFILE'],
		'next' => $steps['EXCHANGE_STOCK_PROFILE'],
		'view' => 'exchange-stock-profile',
		'url' => '/tax-filling/' . $steps['EXCHANGE_STOCK_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.29',
	],

	$steps['EXCHANGE_STOCK_GLOBAL'] => [
		'title' => 'Saham di Bursa Efek',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['EXCHANGE_STOCK_PROFILE'],
		'next' => $steps['EXCHANGE_STOCK_GLOBAL'],
		'view' => 'exchange-stock-global',
		'url' => '/tax-filling/' . $steps['EXCHANGE_STOCK_GLOBAL'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.30',
	],

	$steps['EXCHANGE_STOCK_DETAIL'] => [
		'title' => 'Saham di Bursa Efek',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['EXCHANGE_STOCK_GLOBAL'],
		'next' => $steps['EXCHANGE_STOCK_DETAIL'],
		'view' => 'exchange-stock-detail',
		'url' => '/tax-filling/' . $steps['EXCHANGE_STOCK_DETAIL'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.31',
	],

	$steps['MUTUAL_FUNDS'] => [
		'title' => 'Reksadana',
		'subtitle' => 'Silahkan isi informasi reksadana anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MUTUAL_FUNDS'],
		'next' => $steps['INSURANCE'],
		'view' => 'mutual-funds',
		'url' => '/tax-filling/' . $steps['MUTUAL_FUNDS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.32',
	],

	$steps['INSURANCE'] => [
		'title' => 'Asuransi dengan Komponen Investasi',
		'subtitle' => 'Silahkan isi informasi asuransi anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['MUTUAL_FUNDS'],
		'next' => $steps['INDONESIAN_BANK_CERTIFICATE'],
		'view' => 'insurance',
		'url' => '/tax-filling/' . $steps['INSURANCE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.33',
	],

	$steps['INSURANCE_CLAIM_PROFILE'] => [
		'title' => 'Asuransi dengan Komponen Investasi',
		'subtitle' => 'Silahkan isi informasi asuransi anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INSURANCE'],
		'next' => $steps['INSURANCE_CLAIM_PROFILE'],
		'view' => 'insurance-claim-profile',
		'url' => '/tax-filling/' . $steps['INSURANCE_CLAIM_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.34',
	],

	$steps['INSURANCE_CLAIM'] => [
		'title' => 'Asuransi dengan Komponen Investasi',
		'subtitle' => 'Silahkan isi informasi asuransi anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INSURANCE'],
		'next' => $steps['INSURANCE_CLAIM'],
		'view' => 'insurance-claim',
		'url' => '/tax-filling/' . $steps['INSURANCE_CLAIM'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.35',
	],

	$steps['INDONESIAN_BANK_CERTIFICATE'] => [
		'title' => 'Sertifikat Bank Indonesia',
		'subtitle' => 'Silahkan isi informasi Sertifikat Bank Indonesia anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INDONESIAN_BANK_CERTIFICATE'],
		'next' => $steps['GOVERNMENT_BONDS'],
		'view' => 'indonesian-bank-certificate',
		'url' => '/tax-filling/' . $steps['INDONESIAN_BANK_CERTIFICATE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.36',
	],

	$steps['GOVERNMENT_BONDS'] => [
		'title' => 'Obligasi Pemerintah',
		'subtitle' => 'Silahkan isi informasi obligasi pemerintah anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['GOVERNMENT_BONDS'],
		'next' => $steps['CORPORATE_BONDS'],
		'view' => 'government-bonds',
		'url' => '/tax-filling/' . $steps['GOVERNMENT_BONDS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.37',
	],

	$steps['CORPORATE_BONDS'] => [
		'title' => 'Obligasi Perusahaan',
		'subtitle' => 'Silahkan isi informasi obligasi perusahaan anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CORPORATE_BONDS'],
		'next' => $steps['NON_TRADEABLE_STOCKS'],
		'view' => 'corporate-bonds',
		'url' => '/tax-filling/' . $steps['CORPORATE_BONDS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.38',
	],

	$steps['NON_TRADEABLE_STOCKS'] => [
		'title' => 'Saham Non Bursa Efek',
		'subtitle' => 'Silahkan isi informasi saham non bursa efek anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['NON_TRADEABLE_STOCKS'],
		'next' => $steps['CAPITAL_INVESTMENT_COMPANIES'],
		'view' => 'non-tradeable-stocks',
		'url' => '/tax-filling/' . $steps['NON_TRADEABLE_STOCKS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.39',
	],

	$steps['CAPITAL_INVESTMENT_COMPANIES'] => [
		'title' => 'Penyertaan Modal Dalam Perusahaan Lain Yang Tidak Dalam Bentuk Saham (CV,Firma,dan Sejenisnya)',
		'subtitle' => 'Silahkan isi informasi penyertaan modal anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CAPITAL_INVESTMENT_COMPANIES'],
		'next' => $steps['OTHER_DEBENTURES'],
		'view' => 'capital-investment-companies',
		'url' => '/tax-filling/' . $steps['CAPITAL_INVESTMENT_COMPANIES'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.40',
	],

	$steps['OTHER_DEBENTURES'] => [
		'title' => 'Surat Hutang Lainnya',
		'subtitle' => 'Silahkan isi informasi surat hutang anda lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_DEBENTURES'],
		'next' => $steps['OTHER_INVESTMENTS'],
		'view' => 'other-debentures',
		'url' => '/tax-filling/' . $steps['OTHER_DEBENTURES'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.41',
	],

	$steps['OTHER_INVESTMENTS'] => [
		'title' => 'Investasi Lainnya',
		'subtitle' => 'Silahkan isi informasi investasi anda lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_INVESTMENTS'],
		'next' => $steps['OTHER_ASSETS_PROFILE'],
		'view' => 'other-investments',
		'url' => '/tax-filling/' . $steps['OTHER_INVESTMENTS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.28.42',
	],

	/* 8. ASET LAIN-LAIN */

	$steps['OTHER_ASSETS_PROFILE'] => [
		'title' => 'Aset Lainnya',
		'subtitle' => 'Silahkan isi informasi aset anda lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_ASSETS_PROFILE'],
		'next' => $steps['OTHER_CASH_EQUIVALENTS'],
		'view' => 'other-assets-profile',
		'url' => '/tax-filling/' . $steps['OTHER_ASSETS_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43',
	],

	$steps['OTHER_CASH_EQUIVALENTS'] => [
		'title' => 'Setara Kas Lainnya',
		'subtitle' => 'Silahkan isi informasi aset setara kas anda lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_ASSETS_PROFILE'],
		'next' => $steps['SPECIAL_TRANSPORTATIONS'],
		'view' => 'other-cash-equivalents',
		'url' => '/tax-filling/' . $steps['OTHER_CASH_EQUIVALENTS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.44',
	],

	$steps['SPECIAL_TRANSPORTATIONS'] => [
		'title' => 'Kapal Pesiar, Pesawat Terbang, Helikopter, Jetski, Peralatan Olahraga Khusus',
		'subtitle' => 'Silahkan isi informasi Kapal, Pesawat Terbang, Helikopter,Jetski, atau Peralatan Olahraga Khusus Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_CASH_EQUIVALENTS'],
		'next' => $steps['OTHER_TRANSPORTATIONS'],
		'view' => 'special-transportations',
		'url' => '/tax-filling/' . $steps['SPECIAL_TRANSPORTATIONS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.45',
	],

	$steps['OTHER_TRANSPORTATIONS'] => [
		'title' => 'Alat Transportasi Lainnya',
		'subtitle' => 'Silahkan isi Alat Transportasi Anda Lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SPECIAL_TRANSPORTATIONS'],
		'next' => $steps['ELECTRONICS_FURNITURES'],
		'view' => 'other-transportations',
		'url' => '/tax-filling/' . $steps['OTHER_TRANSPORTATIONS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.46',
	],

	$steps['ELECTRONICS_FURNITURES'] => [
		'title' => 'Elektronik dan Furnitur',
		'subtitle' => 'Silahkan isi Informasi Elektronik/Furnitur Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_TRANSPORTATIONS'],
		'next' => $steps['OTHER_IMMOVABLE_PROPERTY'],
		'view' => 'electronics-furnitures',
		'url' => '/tax-filling/' . $steps['ELECTRONICS_FURNITURES'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.47',
	],

	$steps['OTHER_IMMOVABLE_PROPERTY'] => [
		'title' => 'Harta Tidak Bergerak Lainnya',
		'subtitle' => 'Silahkan isi Informasi Harta Tidak Bergerak Anda Lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['ELECTRONICS_FURNITURES'],
		'next' => $steps['OTHER_MOVABLE_PROPERTY'],
		'view' => 'other-immovable-property',
		'url' => '/tax-filling/' . $steps['OTHER_IMMOVABLE_PROPERTY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.48',
	],

	$steps['OTHER_MOVABLE_PROPERTY'] => [
		'title' => 'Harta Bergerak Lainnya',
		'subtitle' => 'Silahkan isi Informasi Harta Bergerak Anda Lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_IMMOVABLE_PROPERTY'],
		'next' => $steps['ACCOUNT_RECEIVABLE'],
		'view' => 'other-movable-property',
		'url' => '/tax-filling/' . $steps['OTHER_MOVABLE_PROPERTY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.49',
	],

	$steps['ACCOUNT_RECEIVABLE'] => [
		'title' => 'Piutang',
		'subtitle' => 'Silahkan isi Informasi Piutang Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_MOVABLE_PROPERTY'],
		'next' => $steps['AFFILIATE_RECEIVABLE'],
		'view' => 'account-receivable',
		'url' => '/tax-filling/' . $steps['ACCOUNT_RECEIVABLE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.50',
	],

	$steps['AFFILIATE_RECEIVABLE'] => [
		'title' => 'Piutang Afiliasi',
		'subtitle' => 'Silahkan isi Informasi Piutang Afiliasi Anda.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['ACCOUNT_RECEIVABLE'],
		'next' => $steps['OTHER_RECEIVABLE'],
		'view' => 'affiliate-receivable',
		'url' => '/tax-filling/' . $steps['AFFILIATE_RECEIVABLE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.51',
	],

	$steps['OTHER_RECEIVABLE'] => [
		'title' => 'Piutang Lainnya',
		'subtitle' => 'Silahkan isi Informasi Piutang Anda Lainnya.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['AFFILIATE_RECEIVABLE'],
		'next' => $steps['LIABILITIES_PROFILE'],
		'view' => 'other-receivable',
		'url' => '/tax-filling/' . $steps['OTHER_RECEIVABLE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.43.52',
	],

	/* 9. HUTANG */

	$steps['LIABILITIES_PROFILE'] => [
		'title' => 'Kewajiban/Hutang',
		'subtitle' => 'Silahkan isi Informasi Kewajiban/Hutang Anda Selain Kredit Kendaraan, Properti, dan Kartu Kredit.',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_RECEIVABLE'],
		'next' => $steps['FINANCIAL_INSTITUTION_LIABILITY'],
		'view' => 'liabilities-profile',
		'url' => '/tax-filling/' . $steps['LIABILITIES_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.53',
	],

	$steps['FINANCIAL_INSTITUTION_LIABILITY'] => [
		'title' => 'Hutang dari Institusi Finansial dan Non-Bank',
		'subtitle' => 'Silahkan isi Informasi Kewajiban/Hutang Anda dari Institusi Finansial Maupun Non-Bank Anda',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['LIABILITIES_PROFILE'],
		'next' => $steps['AFFILIATE_LIABILITY'],
		'view' => 'financial-institution-liability',
		'url' => '/tax-filling/' . $steps['FINANCIAL_INSTITUTION_LIABILITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.53.54',
	],

	$steps['AFFILIATE_LIABILITY'] => [
		'title' => 'Hutang Afiliasi',
		'subtitle' => 'Silahkan isi Informasi Kewajiban/Hutang Afiliasi Anda',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['FINANCIAL_INSTITUTION_LIABILITY'],
		'next' => $steps['OTHER_LIABILITY'],
		'view' => 'affiliate-liability',
		'url' => '/tax-filling/' . $steps['AFFILIATE_LIABILITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.53.55',
	],

	$steps['OTHER_LIABILITY'] => [
		'title' => 'Hutang Lainnya',
		'subtitle' => 'Silahkan isi Informasi Kewajiban/Hutang Anda Lainnya',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['AFFILIATE_LIABILITY'],
		'next' => $steps['ADDITIONAL_REGULAR_INCOMES_PROFILE'],
		'view' => 'other-liability',
		'url' => '/tax-filling/' . $steps['OTHER_LIABILITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '3.53.56',
	],

	/* 10. PENGHASILAN TAMBAHAN RUTIN */

	$steps['ADDITIONAL_REGULAR_INCOMES_PROFILE'] => [
		'title' => 'Penghasilan Tambahan Rutin',
		'subtitle' => 'Silahkan isi Informasi Penghasilan Tambahan Rutin Anda',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['OTHER_LIABILITY'],
		'next' => $steps['LAND_AND_BUILDING'],
		'view' => 'additional-regular-incomes-profile',
		'url' => '/tax-filling/' . $steps['ADDITIONAL_REGULAR_INCOMES_PROFILE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.57',
	],

	$steps['LAND_AND_BUILDING'] => [
		'title' => 'Sewa Tanah dan Bangunan',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['ADDITIONAL_REGULAR_INCOMES_PROFILE'],
		'next' => $steps['DIVIDEND_FROM_INVESTATION'],
		'view' => 'land-and-building',
		'url' => '/tax-filling/' . $steps['LAND_AND_BUILDING'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.58',
	],

	$steps['DIVIDEND_FROM_INVESTATION'] => [
		'title' => 'Dividen dari Saham/Investasi',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['LAND_AND_BUILDING'],
		'next' => $steps['COMMANDITAIRE_PROFIT_MEMBER'],
		'view' => 'dividend-from-investation',
		'url' => '/tax-filling/' . $steps['DIVIDEND_FROM_INVESTATION'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.59',
	],

	$steps['COMMANDITAIRE_PROFIT_MEMBER'] => [
		'title' => 'Bagian Laba Anggota Perseroan Komanditer Tidak Atas Saham',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DIVIDEND_FROM_INVESTATION'],
		'next' => $steps['INCOME_FROM_DEPOSIT'],
		'view' => 'commanditaire-profit-member',
		'url' => '/tax-filling/' . $steps['COMMANDITAIRE_PROFIT_MEMBER'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.60',
	],

	$steps['INCOME_FROM_DEPOSIT'] => [
		'title' => 'Penghasilan Dari Deposito',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['COMMANDITAIRE_PROFIT_MEMBER'],
		'next' => $steps['DISCOUNT_RATE_SBI_SBN'],
		'view' => 'income-from-deposit',
		'url' => '/tax-filling/' . $steps['INCOME_FROM_DEPOSIT'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.61',
	],

	$steps['DISCOUNT_RATE_SBI_SBN'] => [
		'title' => 'Bunga Diskonto SBI dan SBN',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INCOME_FROM_DEPOSIT'],
		'next' => $steps['DISCOUNT_RATE_BOND'],
		'view' => 'discount-rate-sbi-sbn',
		'url' => '/tax-filling/' . $steps['DISCOUNT_RATE_SBI_SBN'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.62',
	],

	$steps['DISCOUNT_RATE_BOND'] => [
		'title' => 'Bunga Diskonto Obligasi Pemerintah dan Perusahaan',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DISCOUNT_RATE_SBI_SBN'],
		'next' => $steps['SALE_OF_SHARES'],
		'view' => 'discount-rate-bond',
		'url' => '/tax-filling/' . $steps['DISCOUNT_RATE_BOND'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.63',
	],

	$steps['SALE_OF_SHARES'] => [
		'title' => 'Penjualan Saham di Bursa Efek',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DISCOUNT_RATE_BOND'],
		'next' => $steps['INTELLECTUAL_PROPERTY_RIGHTS'],
		'view' => 'sale-of-shares',
		'url' => '/tax-filling/' . $steps['SALE_OF_SHARES'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.64',
	],

	$steps['INTELLECTUAL_PROPERTY_RIGHTS'] => [
		'title' => 'Royalti dari Hak Kekayaan Intelektual',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SALE_OF_SHARES'],
		'next' => $steps['LEASE_ASSET_OUTSIDE_BUILDINGS'],
		'view' => 'intellectual-property-rights',
		'url' => '/tax-filling/' . $steps['INTELLECTUAL_PROPERTY_RIGHTS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.65',
	],

	$steps['LEASE_ASSET_OUTSIDE_BUILDINGS'] => [
		'title' => 'Sewa Aset di Luar Tanah dan Bangunan',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INTELLECTUAL_PROPERTY_RIGHTS'],
		'next' => $steps['INTEREST_FROM_DEBT'],
		'view' => 'lease-asset-outside-buildings',
		'url' => '/tax-filling/' . $steps['LEASE_ASSET_OUTSIDE_BUILDINGS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.66',
	],

	$steps['INTEREST_FROM_DEBT'] => [
		'title' => 'Pendapatan Bunga Sehubungan dengan Pengembalian Hutang',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INTELLECTUAL_PROPERTY_RIGHTS'],
		'next' => $steps['REWARDS_TO_EXPERTS'],
		'view' => 'interest-from-debt',
		'url' => '/tax-filling/' . $steps['INTEREST_FROM_DEBT'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.67',
	],

	$steps['REWARDS_TO_EXPERTS'] => [
		'title' => 'Imbalan Kepada Tenaga Ahli',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INTEREST_FROM_DEBT'],
		'next' => $steps['OTHER_REGULAR_INCOME'],
		'view' => 'rewards-to-experts',
		'url' => '/tax-filling/' . $steps['REWARDS_TO_EXPERTS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.68',
	],

	$steps['OTHER_REGULAR_INCOME'] => [
		'title' => 'Pendapatan Rutin Lainnya',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['INTEREST_FROM_DEBT'],
		'next' => $steps['OTHER_REGULAR_INCOME'],
		'view' => 'other-regular-income',
		'url' => '/tax-filling/' . $steps['OTHER_REGULAR_INCOME'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '4.69',
	],

	/* 11. PENGHASILAN SEKALI TERIMA */

	$steps['ADDITIONAL_EARNING_ONE_TIME'] => [
		'title' => 'Penghasilan Tambahan - Penghasilan Tambahan Sekali Terima',
		'subtitle' => 'Melainkan penghasilan dari pekerjaan utama Anda, silahkan pilih penghasilan tambahan yang bersifat
		tidak rutin',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DIVIDEND_FROM_INVESTATION'],
		'next' => $steps['ADDITIONAL_EARNING_ONE_TIME'],
		'view' => 'additional-earning-one-time',
		'url' => '/tax-filling/' . $steps['ADDITIONAL_EARNING_ONE_TIME'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '5.70',
	],

	$steps['LEGACY'] => [
		'title' => 'Warisan atau Hibah',
		'subtitle' => 'Penghasilan Tambahan - Warisan/Hibah',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DIVIDEND_FROM_INVESTATION'],
		'next' => $steps['ADDITIONAL_EARNING_ONE_TIME'],
		'view' => 'legacy',
		'url' => '/tax-filling/' . $steps['LEGACY'],
		'validations' => [
			'rules' => [
				'nilai.*' => 'required',	
				'pemberi.*' => 'required',		
				'type.*' => 'required',		
			],
			'messages' => [
	
			],
		],
		'nested' => '5.71',
	],

	$steps['TRANSFER_BUILDING_RIGHTS'] => [
		'title' => 'Pengalihan Hak atas Tanah Bangunan',
		'subtitle' => 'Penghasilan Tambahan - Pengalihan Hak atas Tanah Bangunan',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['LEGACY'],
		'next' => $steps['TRANSFER_BUILDING_RIGHTS'],
		'view' => 'transfer-building-rights',
		'url' => '/tax-filling/' . $steps['TRANSFER_BUILDING_RIGHTS'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.72',
	],

	$steps['NON_BUILDING_TRANSFER_PROFIT'] => [
		'title' => 'Keuntungan dari Pengalihan Harta selain Tanah Bangunan',
		'subtitle' => 'Penghasilan Tambahan - Keuntungan dari Pengalihan Harta selain Tanah Bangunan',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['LEGACY'],
		'next' => $steps['NON_BUILDING_TRANSFER_PROFIT'],
		'view' => 'non-building-transfer-profit',
		'url' => '/tax-filling/' . $steps['NON_BUILDING_TRANSFER_PROFIT'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.73',
	],

	$steps['SEVERANCE_PAY_ONCE'] => [
		'title' => 'Pesangon/Tunjangan Hari Tua yang Dibayarkan Sekaligus',
		'subtitle' => 'Penghasilan Tambahan - Pesangon/Tunjangan Hari Tua yang Dibayarkan Sekaligus',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SEVERANCE_PAY_ONCE'],
		'next' => $steps['SEVERANCE_PAY_ONCE'],
		'view' => 'severance-pay-once',
		'url' => '/tax-filling/' . $steps['SEVERANCE_PAY_ONCE'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.74',
	],

	$steps['PRIZE_LOTERRY'] => [
		'title' => 'Hadiah Undian',
		'subtitle' => 'Penghasilan Tambahan - Hadiah Undian',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PRIZE_LOTERRY'],
		'next' => $steps['PRIZE_LOTERRY'],
		'view' => 'prize-loterry',
		'url' => '/tax-filling/' . $steps['PRIZE_LOTERRY'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.75',
	],

	$steps['SCHOLARSHIP'] => [
		'title' => 'Beasiswa',
		'subtitle' => 'Penghasilan Tambahan - Beasiswa',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SCHOLARSHIP'],
		'next' => $steps['SCHOLARSHIP'],
		'view' => 'scholarship',
		'url' => '/tax-filling/' . $steps['SCHOLARSHIP'],
		'validations' => [
			'rules' => [
				'value.*' => 'required|integer',
				'giver.*' => 'required'
			],
			'messages' => [
				'value.*' => 'Form Nilai harus diisi dengan <strong>Angka</strong>.',
				'giver.*' => 'Form Nama Pemberi harus diisi.',
			]
		],
		'nested' => '5.76',
	],

	$steps['SALE_OF_SHARES_FOUNDER'] => [
		'title' => 'Penjualan Saham Pendiri',
		'subtitle' => 'Penghasilan Tambahan - Penjualan Saham Pendiri',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['SALE_OF_SHARES_FOUNDER'],
		'next' => $steps['SALE_OF_SHARES_FOUNDER'],
		'view' => 'sale-of-shares-founder',
		'url' => '/tax-filling/' . $steps['SALE_OF_SHARES_FOUNDER'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.77',
	],

	$steps['HONORARIUM_OF_APBDN_EXPENSES'] => [
		'title' => 'Honorarium atas Beban APBN/APBD',
		'subtitle' => 'Penghasilan Tambahan - Honorarium atas Beban APBN/APBD',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['HONORARIUM_OF_APBDN_EXPENSES'],
		'next' => $steps['HONORARIUM_OF_APBDN_EXPENSES'],
		'view' => 'honorarium-of-apbdn-expenses',
		'url' => '/tax-filling/' . $steps['HONORARIUM_OF_APBDN_EXPENSES'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.78',
	],

	$steps['CASUAL_LABORER_WAGE'] => [
		'title' => 'Upah Pegawai Tidak Tetap atau Pekerja Lepas',
		'subtitle' => 'Penghasilan Tambahan - Upah Pegawai Tidak Tetap atau Pekerja Lepas',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CASUAL_LABORER_WAGE'],
		'next' => $steps['CASUAL_LABORER_WAGE'],
		'view' => 'casual-laborer-wage',
		'url' => '/tax-filling/' . $steps['CASUAL_LABORER_WAGE'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.79',
	],

	$steps['HONORARIUM_COMMISSIONER'] => [
		'title' => 'Honorarium Komisaris yang Tidak Merangkap Pegawai Tetap',
		'subtitle' => 'Penghasilan Tambahan - Honorarium Komisaris yang Tidak Merangkap Pegawai Tetap',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['HONORARIUM_COMMISSIONER'],
		'next' => $steps['HONORARIUM_COMMISSIONER'],
		'view' => 'honorarium-commissioner',
		'url' => '/tax-filling/' . $steps['HONORARIUM_COMMISSIONER'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.80',
	],

	$steps['AWARDS_AND_GIFT'] => [
		'title' => 'Penghargaan dan Hadiah',
		'subtitle' => 'Penghasilan Tambahan - Penghargaan dan Hadiah',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['AWARDS_AND_GIFT'],
		'next' => $steps['AWARDS_AND_GIFT'],
		'view' => 'awards-and-gift',
		'url' => '/tax-filling/' . $steps['AWARDS_AND_GIFT'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.81',
	],

	$steps['HELP_OR_DONATION'] => [
		'title' => 'Bantuan atau Sumbangan',
		'subtitle' => 'Penghasilan Tambahan - Bantuan atau Sumbangan',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['HELP_OR_DONATION'],
		'next' => $steps['HELP_OR_DONATION'],
		'view' => 'help-or-donation',
		'url' => '/tax-filling/' . $steps['HELP_OR_DONATION'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.82',
	],

	$steps['ACCEPTED_BUILDING'] => [
		'title' => 'Bangunan yang Diterima Dalam Rangka Bangun/Guna/Serah',
		'subtitle' => 'Penghasilan Tambahan - Bangunan yang Diterima Dalam Rangka Bangun/Guna/Serah',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['ACCEPTED_BUILDING'],
		'next' => $steps['ACCEPTED_BUILDING'],
		'view' => 'accepted-building',
		'url' => '/tax-filling/' . $steps['ACCEPTED_BUILDING'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.83',
	],

	$steps['ANOTHER_ONCE_EARNING'] => [
		'title' => 'Penghasilan Sekali Terima Lainnya',
		'subtitle' => 'Penghasilan Tambahan - Penghasilan Sekali Terima Lainnya',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['ANOTHER_ONCE_EARNING'],
		'next' => $steps['ANOTHER_ONCE_EARNING'],
		'view' => 'another-once-earning',
		'url' => '/tax-filling/' . $steps['ANOTHER_ONCE_EARNING'],
		'validations' => [
			'rules' => [

			],
			'messages' => [
	
			],
		],
		'nested' => '5.84',
	],

	/* 12. TAX AMNESTY */

	$steps['TAX_AMNESTY_DISCLOSURE'] => [
		'title' => 'Tax Amnesty Pengungkapan',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DIVIDEND_FROM_INVESTATION'],
		'next' => $steps['TAX_AMNESTY_DISCLOSURE'],
		'view' => 'tax-amnesty-disclosure',
		'url' => '/tax-filling/' . $steps['TAX_AMNESTY_DISCLOSURE'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '6.85',
	],

	$steps['TAX_AMNESTY_REPATRIATION'] => [
		'title' => 'Tax Amnesty Repatriasi',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['DIVIDEND_FROM_INVESTATION'],
		'next' => $steps['TAX_AMNESTY_REPATRIATION'],
		'view' => 'tax-amnesty-repatriation',
		'url' => '/tax-filling/' . $steps['TAX_AMNESTY_REPATRIATION'],
		'validations' => [
			'rules' => [
				'revealed_params' => 'required|check_repatriation_data:1'
			],
			'messages' => [
				'check_repatriation_data.a' => null,
				'check_repatriation_data.b' => null,
			],
		],
		'nested' => '6.86',
	],

	/* 13. INFORMASI LAINNYA */

	$steps['CHARITY'] => [
		'title' => 'Zakat',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['TAX_AMNESTY_REPATRIATION'],
		'next' => $steps['EVIDENCE_DISCOUNT_LUXURY_GOODS'],
		'view' => 'charity',
		'url' => '/tax-filling/' . $steps['CHARITY'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '7.87',
	],

	$steps['EVIDENCE_DISCOUNT_LUXURY_GOODS'] => [
		'title' => 'Bukti Potong Atas Pembelian Barang Mewah',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['CHARITY'],
		'next' => $steps['EVIDENCE_DISCOUNT_LUXURY_GOODS'],
		'view' => 'evidence-discount-luxury-goods',
		'url' => '/tax-filling/' . $steps['EVIDENCE_DISCOUNT_LUXURY_GOODS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '7.88',
	],
	
	$steps['PPH25_INFORMATION_1'] => [
		'title' => 'Informasi PPh 25',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['EVIDENCE_DISCOUNT_LUXURY_GOODS'],
		'next' => $steps['PPH25_INFORMATION_1'],
		'view' => 'pph25-information-1',
		'url' => '/tax-filling/' . $steps['PPH25_INFORMATION_1'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '7.89',
	],

	$steps['PPH25_INFORMATION_2'] => [
		'title' => 'Informasi PPh 25',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PPH25_INFORMATION_1'],
		'next' => $steps['FILL_REQUIRED_DATA_1'],
		'view' => 'pph25-information-2',
		'url' => '/tax-filling/' . $steps['PPH25_INFORMATION_2'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '7.90',
	],

	$steps['FILL_REQUIRED_DATA_1'] => [
		'title' => 'Data Wajib yang Belum Diisi',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PPH25_INFORMATION_2'],
		'next' => $steps['FINAL_STEPS'],
		'view' => 'fill-required-data-1',
		'url' => '/tax-filling/' . $steps['PPH25_INFORMATION_2'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	$steps['FILL_REQUIRED_DATA_2'] => [
		'title' => 'Data Wajib yang Belum Diisi',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['FILL_REQUIRED_DATA_1'],
		'next' => $steps['FINAL_STEPS'],
		'view' => 'fill-required-data-1',
		'url' => '/tax-filling/' . $steps['FILL_REQUIRED_DATA_2'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	$steps['FINAL_STEPS'] => [
		'title' => 'Summary SPT Anda',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['FILL_REQUIRED_DATA_1'],
		'next' => $steps['FILL_REQUIRED_DATA_2'],
		'view' => 'final-steps',
		'url' => '/tax-filling/' . $steps['FINAL_STEPS'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],

	$steps['PREVIEW_SPT'] => [
		'title' => 'Preview SPT Anda',
		'subtitle' => '',
		'icon' => 'fa-user',
		'parent' => false,
		'display_sticky_note' => false,
		'display_knowledge_base' => false,
		'display_progress_bar' => true,
		'previous' => $steps['PREVIEW_SPT'],
		'next' => $steps['PREVIEW_SPT'],
		'view' => 'preview-spt',
		'url' => '/tax-filling/' . $steps['PREVIEW_SPT'],
		'validations' => [
			'rules' => [],
			'messages' => [],
		],
		'nested' => '',
	],
	
];