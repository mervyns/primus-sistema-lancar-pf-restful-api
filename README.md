# Project Franklin Restful API
[![CircleCI](https://circleci.com/bb/primus-sistema-lancar/pf-restful-api.svg?style=svg)](https://circleci.com/bb/primus-sistema-lancar/pf-restful-api)

1. Software Requirements
    - Web server like Apache, Nginx, or Lighttpd. But you can also just use command `$ php artisan serve` to run the development web server.
    - PHP 5.6 or later
    - MySQL 5.6 or MariaDB 10.1
    - Composer 1.5.1
    - Laravel 5.4
    
2. How To Install
    - Clone the source code from this repository by running command: `$ git clone git@bitbucket.org:primus-sistema-lancar/pf-restful-api.git`
    - Change current directory to the cloned repository: `$ cd pf-restul-api`
    - Run composer to install the dependencies: `$ composer update`
    - Create new _.env_ file in the project root directory and paste the following configurations:
      > APP_ENV=local
      > APP_DEBUG=true
      > APP_KEY=SomeRandomString
      > APP_URL=http://localhost
      > 
      > DB_CONNECTION=mysql
      > DB_HOST=127.0.0.1
      > DB_PORT=3306
      > DB_DATABASE=<DB NAME>
      > DB_USERNAME=<DB USER>
      > DB_PASSWORD=<DB PASS>
      
    - Run this command to generate application key: `$ php artisan key:generate`. This command will automatically replace _SomeRandomString_ with generated key and you can skip the next step.
    - Copy and paste the output of the above command to **APP_KEY** variable in _.env_ file (replace the _SomeRandomString_).
    - Install migration files: `$ php artisan migrate`
    - Then you need to generate default OAuth clients using this command: `$ php artisan passport install`. 
      Please refer to [the Laravel Passport documentation](https://laravel.com/docs/5.4/passport) for more information about Passport commands.
    - You're ready to go!
    
3. Artisan Commands
    - Creating a new user via artisan command: `$ php artisan user:add "<FULLNAME>" <EMAIL> <PASSWORD>`.
    - _Coming soon_.