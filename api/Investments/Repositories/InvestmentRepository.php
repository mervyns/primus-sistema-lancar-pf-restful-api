<?php

namespace Api\Investments\Repositories;

use Api\Investments\Models\Investment;
use Infrastructure\Database\Eloquent\Repository;

class InvestmentRepository extends Repository
{
    public function getModel()
    {
        return new Investment();
    }

    public function create(array $data)
    {
        $investment = $this->getModel();

        $investment->fill($data);
        
        $investment->save();

        return $investment;
    }

    public function update(Investment $investment, array $data)
    {

        $investment->fill($data);

        $investment->save();

        return $investment;
    }
}
