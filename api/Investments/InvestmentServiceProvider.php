<?php

namespace Api\Investments;

use Infrastructure\Events\EventServiceProvider;
use Api\Investments\Events\InvestmentWasCreated;
use Api\Investments\Events\InvestmentWasDeleted;
use Api\Investments\Events\InvestmentWasUpdated;

class InvestmentServiceProvider extends EventServiceProvider
{
    protected $listen = [
        InvestmentWasCreated::class => [
            // listeners for when a investment is created
        ],
        InvestmentWasDeleted::class => [
            // listeners for when a investment is deleted
        ],
        InvestmentWasUpdated::class => [
            // listeners for when a investment is updated
        ],
    ];
}
