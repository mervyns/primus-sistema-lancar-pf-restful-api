<?php

namespace Api\Investments\Events;

use Infrastructure\Events\Event;
use Api\Investments\Models\Investment;

class InvestmentWasDeleted extends Event
{
    public $investment;

    public function __construct(Investment $investment)
    {
        $this->investment = $investment;
    }
}
