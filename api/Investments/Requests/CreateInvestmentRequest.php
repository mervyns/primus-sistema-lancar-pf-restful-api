<?php

namespace Api\Investments\Requests;

use Infrastructure\Http\ApiRequest;

class CreateInvestmentRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'investment' => 'array|required',
            'investment.investment_code' => 'required|string',
            'investment.name' => 'required|string',
            'investment.asset_code' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'investment.name' => 'the investment\'s name'
        ];
    }
}
