<?php

namespace Api\Investments\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InvestmentNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The investment was not found.');
    }
}
