<?php

namespace Api\Investments\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Investments\Exceptions\InvestmentNotFoundException;
use Api\Investments\Events\InvestmentWasCreated;
use Api\Investments\Events\InvestmentWasDeleted;
use Api\Investments\Events\InvestmentWasUpdated;
use Api\Investments\Repositories\InvestmentRepository;

class InvestmentService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $investmentRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        InvestmentRepository $investmentRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->investmentRepository = $investmentRepository;
    }

    public function getAll($options = [])
    {
        return $this->investmentRepository->get($options);
    }

    public function getById($investmentId, array $options = [])
    {
        $investment = $this->getRequestedInvestment($investmentId);

        return $investment;
    }

    public function create($data)
    {
        $investment = $this->investmentRepository->create($data);

        $this->dispatcher->fire(new InvestmentWasCreated($investment));

        return $investment;
    }

    public function update($investmentId, array $data)
    {
        $investment = $this->getRequestedInvestment($investmentId);

        $this->investmentRepository->update($investment, $data);

        $this->dispatcher->fire(new InvestmentWasUpdated($investment));

        return $investment;
    }

    public function delete($investmentId)
    {
        $investment = $this->getRequestedInvestment($investmentId);

        $this->investmentRepository->delete($investmentId);

        $this->dispatcher->fire(new InvestmentWasDeleted($investment));

        return true;
    }

    private function getRequestedInvestment($investmentId)
    {
        $investment = $this->investmentRepository->getById($investmentId);

        if (is_null($investment)) {
            throw new InvestmentNotFoundException();
        }

        return $investment;
    }
}
