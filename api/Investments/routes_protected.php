<?php

$router->get('/api/investments', 'InvestmentController@getAll');
$router->get('/api/investments/{id}', 'InvestmentController@getById');
$router->post('/api/investments', 'InvestmentController@create');
$router->put('/api/investments/{id}', 'InvestmentController@update');
$router->delete('/api/investments/{id}', 'InvestmentController@delete');