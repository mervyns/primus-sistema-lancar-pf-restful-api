<?php

namespace Api\Investments\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Investments\Requests\CreateInvestmentRequest;
use Api\Investments\Services\InvestmentService;

class InvestmentController extends Controller
{
    private $investmentService;

    public function __construct(InvestmentService $investmentService)
    {
        $this->investmentService = $investmentService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->investmentService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'investments');

        return $this->response($parsedData);
    }

    public function getById($investmentId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->investmentService->getById($investmentId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'investment');

        return $this->response($parsedData);
    }

    public function create(CreateInvestmentRequest $request)
    {
        $data = $request->get('investment', []);

        return $this->response($this->investmentService->create($data), 201);
    }

    public function update($investmentId, Request $request)
    {
        $data = $request->get('investment', []);

        return $this->response($this->investmentService->update($investmentId, $data));
    }

    public function delete($investmentId)
    {
        return $this->response($this->investmentService->delete($investmentId));
    }
}
