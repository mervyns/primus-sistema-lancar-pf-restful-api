<?php

namespace Api\System\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\System\Repositories\SystemStepConfigRepository;

class SystemStepConfigService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $systemStepRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        SystemStepConfigRepository $systemStepRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->systemStepRepository = $systemStepRepository;
    }

    public function getAll($options = [])
    {
        return $this->systemStepRepository->get($options);
    }
}
