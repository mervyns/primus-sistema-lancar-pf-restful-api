<?php

namespace Api\System\Models;

use Illuminate\Database\Eloquent\Model;

class SystemStepConfig extends Model
{
	protected $table = 'system_steps_config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_id', 'title', 'sub_title', 'icon', 'parent', 'display_sticky_note',
	    'display_knowledge_base', 'display_progress_bar', 'previous', 'next', 'view',
	    'validation_rules',
    ];
}
