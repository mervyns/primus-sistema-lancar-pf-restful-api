<?php

namespace Api\System\Models;

use Illuminate\Database\Eloquent\Model;

class SystemStepConstant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	    'step', 'name',
    ];
}
