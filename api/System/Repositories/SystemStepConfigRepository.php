<?php

namespace Api\System\Repositories;

use Api\System\Models\SystemStepConfig;
use Infrastructure\Database\Eloquent\Repository;

class SystemStepConfigRepository extends Repository
{
    public function getModel()
    {
        return new SystemStepConfig();
    }

    public function create(array $data)
    {
        $config = $this->getModel();

        $config->fill($data);
        
        $config->save();

        return $config;
    }

    public function update(SystemStepConfig $config, array $data)
    {

        $config->fill($data);

        $config->save();

        return $config;
    }

    public function saveOrUpdate($userId, array $data)
    {
    	$config = $this->getModel()->where('user_id', '=', $userId)->first();

    	if (is_null($config)) {
    		$config = $this->getModel();

		    $data['user_id'] = $userId;
	    }

	    $config->fill($data);
	    $config->save();

	    return $config;
    }
}
