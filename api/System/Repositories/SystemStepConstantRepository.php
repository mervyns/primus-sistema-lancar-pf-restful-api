<?php

namespace Api\System\Repositories;

use Api\System\Models\SystemStepConstant;
use Infrastructure\Database\Eloquent\Repository;

class SystemStepConstantRepository extends Repository
{
    public function getModel()
    {
        return new SystemStepConstant();
    }

    public function create(array $data)
    {
        $constant = $this->getModel();

        $constant->fill($data);
        
        $constant->save();

        return $constant;
    }

    public function update(SystemStepConstant $constant, array $data)
    {

        $constant->fill($data);

        $constant->save();

        return $constant;
    }

    public function saveOrUpdate($userId, array $data)
    {
    	$constant = $this->getModel()->where('user_id', '=', $userId)->first();

    	if (is_null($constant)) {
    		$constant = $this->getModel();

		    $data['user_id'] = $userId;
	    }

	    $constant->fill($data);
	    $constant->save();

	    return $constant;
    }
}
