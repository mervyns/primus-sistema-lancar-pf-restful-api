<?php

namespace Api\UserAssets;

use Infrastructure\Events\EventServiceProvider;
use Api\UserAssets\Events\UserAssetWasCreated;
use Api\UserAssets\Events\UserAssetWasDeleted;
use Api\UserAssets\Events\UserAssetWasUpdated;

class UserAssetServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserAssetWasCreated::class => [
            // listeners for when a user asset is created
        ],
        UserAssetWasDeleted::class => [
            // listeners for when a user asset is deleted
        ],
        UserAssetWasUpdated::class => [
            // listeners for when a user asset is updated
        ],
    ];
}
