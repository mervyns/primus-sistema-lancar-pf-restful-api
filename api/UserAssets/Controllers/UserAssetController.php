<?php

namespace Api\UserAssets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserAssets\Requests\CreateUserAssetRequest;
use Api\UserAssets\Services\UserAssetService;

class UserAssetController extends Controller
{
    private $userAssetService;

    public function __construct(UserAssetService $userAssetService)
    {
        $this->userAssetService = $userAssetService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userAssetService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_assets');

        return $this->response($parsedData);
    }

    public function getById($userAssetId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userAssetService->getById($userAssetId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_asset');

        return $this->response($parsedData);
    }

    public function getByAssetCategoryId($slug = 'user-assets', $userAssetId, $category_id)
    {
        $data = $this->userAssetService->getByAssetCategoryId($userAssetId, $category_id, $slug);

        return $data;
    }

    public function getRepatriationTable($asset_id)
    {
        $data = $this->userAssetService->getRepatriationTable($asset_id);

        return $data;
    }

    public function create(CreateUserAssetRequest $request)
    {
        $data = $request->get('user_asset', []);

        return $this->response($this->userAssetService->create($data), 201);
    }

    public function update($userAssetId, Request $request)
    {
        $data = $request->get('user_asset', []);

        return $this->response($this->userAssetService->update($userAssetId, $data));
    }

    public function delete($userAssetId)
    {
        return $this->response($this->userAssetService->delete($userAssetId));
    }
}
