<?php

$router->get('/api/user-assets', 'UserAssetController@getAll');
$router->get('/api/user-assets/{id}', 'UserAssetController@getById');
$router->get('/api/{slug}/{id}/category/{c_id}', 'UserAssetController@getByAssetCategoryId');
$router->get('/api/user-assets/repatriation/{a_id}', 'UserAssetController@getRepatriationTable');
$router->post('/api/user-assets', 'UserAssetController@create');
$router->put('/api/user-assets/{id}', 'UserAssetController@update');
$router->delete('/api/user-assets/{id}', 'UserAssetController@delete');