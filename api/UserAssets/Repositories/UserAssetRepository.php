<?php

namespace Api\UserAssets\Repositories;

use Api\UserAssets\Models\UserAsset;
use Infrastructure\Database\Eloquent\Repository;

class UserAssetRepository extends Repository
{
    public function getModel()
    {
        return new UserAsset();
    }

    public function create(array $data)
    {
        $userAsset = $this->getModel();

        $userAsset->fill($data);
        
        $userAsset->save();

        return $userAsset;
    }

    public function update(UserAsset $userAsset, array $data)
    {

        $userAsset->fill($data);

        $userAsset->save();

        return $userAsset;
    }
}
