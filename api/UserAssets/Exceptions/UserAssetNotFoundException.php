<?php

namespace Api\UserAssets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserAssetNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user\'s asset was not found.');
    }
}
