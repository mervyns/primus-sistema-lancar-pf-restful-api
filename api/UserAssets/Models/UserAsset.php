<?php

namespace Api\UserAssets\Models;

use Illuminate\Database\Eloquent\Model;


class UserAsset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'asset_id', 'user_revealed_asset_id', 'acquisition_year', 'idr_value', 'detail', 'is_ta'
    ];

    public function asset()
    {
        return $this->belongsTo('Api\Assets\Models\Asset','asset_id');
    }

    public function assetsQuery($id)
    {
        return \DB::table('users')
            ->select([
                'user_assets.*',
                'assets.name as asset_name',
                'asset_categories.name as asset_category_name',
                'assets.step as asset_step'
            ])
            ->join('user_assets', 'users.id', '=', 'user_assets.user_id')
            ->join('assets', 'assets.id', '=', 'user_assets.asset_id')
            ->join('asset_categories', 'asset_categories.id', '=', 'assets.asset_category_id')
            // ->orderBy('incomes.name','asc')
            ->where('users.id',$id);
    }
}
