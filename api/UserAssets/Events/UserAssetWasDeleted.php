<?php

namespace Api\UserAssets\Events;

use Infrastructure\Events\Event;
use Api\UserAssets\Models\UserAsset;

class UserAssetWasDeleted extends Event
{
    public $user_asset;

    public function __construct(UserAsset $user_asset)
    {
        $this->user_asset = $user_asset;
    }
}
