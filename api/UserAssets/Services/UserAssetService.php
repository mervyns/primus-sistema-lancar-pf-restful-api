<?php

namespace Api\UserAssets\Services;

use Exception;
use View;
use Carbon\Carbon;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserAssets\Exceptions\UserAssetNotFoundException;
use Api\UserAssets\Events\UserAssetWasCreated;
use Api\UserAssets\Events\UserAssetWasDeleted;
use Api\UserAssets\Events\UserAssetWasUpdated;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\Users\Repositories\UserRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\Assets\Repositories\AssetCategoryRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\Gateways\Repositories\GatewayRepository;
use Api\Investments\Repositories\InvestmentRepository;
use Api\KursPajak\Controllers\KursPajakController;
use WebApp\TaxFilling\FormData\FinalSteps;

class UserAssetService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userAssetRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserAssetRepository $userAssetRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userAssetRepository = $userAssetRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();

        $userAsset = $this->userAssetRepository->get($options);
        $userAsset->isEmpty() ? [] : $userAsset = $userAsset->toArray();
        $i = 0;

        foreach ($userAsset as $uA) {
            $user = $userRepository->getById($uA['user_id']);
            $asset = $assetRepository->getById($uA['asset_id']);

            $userAsset[$i]['user_name'] = $user['name'];
            $userAsset[$i]['asset_name'] = $asset['name'];

            $i++;
        }

        return $userAsset;
    }

    public function getById($userAssetId, array $options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();

        $userAsset = $this->getRequestedUserAsset($userAssetId);
      
        $user = $userRepository->getById($userAsset->user_id);
        $asset = $assetRepository->getById($userAsset->asset_id);

        $userAsset['user_name'] = $user['name'];
        $userAsset['asset_name'] = $asset['name'];

        return $userAsset;
    }

    public function getByAssetCategoryId($userAssetId, $category_id, $slug)
    {   
        $response = [];
        $filtered = [];
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();



        $user = $userRepository->getById($userAssetId);
        $user_assets = $user->asset()->where('is_ta',0)->get();

        if(!$user_assets->isEmpty()){
            $filtered = $user_assets->filter(function ($value) use ($category_id) {
                return ($value->asset->asset_category_id == $category_id);
            });
        }
        // dd($filtered);
        $html = View::make("partial.table_{$slug}", compact('filtered'))->render();
        return response()->json(compact('html'));
    }

    public function getRepatriationTable($asset_id){
      
        $data = [
            'assets' => null,
            'gateways' => null,
        ];
        $gateways = [];
        $asset_repository = new AssetRepository();
        $user_asset_repository = new UserAssetRepository();
        $gateway_repository = new GatewayRepository();
        $investment_repository = new InvestmentRepository();
        $country_repository = new CountryRepository();

        $user_assets = $user_asset_repository->getModel()->where('id',$asset_id)->with('asset')->first();
        $gateways = $gateway_repository->getModel()->all();
        $investments = $investment_repository->getModel()->all();
        $currencies = $country_repository->getModel()->all();
      
        empty($user_assets) ? null : $user_assets = $user_assets->toArray();
        empty($gateways) ? null : $gateways = $gateways->toArray();
        empty($investments) ? null : $investments = $investments->toArray();
        empty($currencies) ? null : $currencies = $currencies->toArray();
       
        $data['assets'] = $user_assets;
        $data['gateways'] = $gateways;
        $data['investments'] = $investments;
        $data['currencies'] = $currencies;
        // dd($data);
        // $html = View::make("partial.table_repatriation", compact('data'))->render();
        $html = View::make("partial.form_repatriation", compact('data'))->render();
        return response()->json(compact('html'));
    }

    public function create($data)
    {
        $userAsset = $this->userAssetRepository->create($data);

        $this->dispatcher->fire(new UserAssetWasCreated($userAsset));

        return $userAsset;
    }

    public function update($userAssetId, array $data)
    {
        $userAsset = $this->getRequestedUserAsset($userAssetId);

        $this->userAssetRepository->update($userAsset, $data);

        $this->dispatcher->fire(new UserAssetWasUpdated($userAsset));

        return $userAsset;
    }

    public function delete($userAssetId)
    {
        $userAsset = $this->getRequestedUserAsset($userAssetId);

        $this->userAssetRepository->delete($userAssetId);

        $this->dispatcher->fire(new UserAssetWasDeleted($userAsset));

        return true;
    }

    private function getRequestedUserAsset($userAssetId)
    {
        $userAsset = $this->userAssetRepository->getById($userAssetId);

        if (is_null($userAsset)) {
            throw new UserAssetNotFoundException();
        }

        return $userAsset;
    }

    public function getHarta($id){
        try{

			$result = [
				'harta' => []
            ];
            
			$user_assets = $this->userAssetRepository->getModel()->assetsQuery($id);
            $countryRepository = new CountryRepository();
         
			$finalStep = new FinalSteps();
            $result = $finalStep->mapingDataForAjax($user_assets, 'harta');
		}catch(\Exception $e){
			dd($e);
		}
		

		return [
			'data' => $result
		];
    }

    private function getCurrency($code){
        $kurs = new KursPajakController();
        $request = new \Illuminate\Http\Request();
        $request->merge(['month' => 12]);
        $request->merge(['date' => 31]);
        $request->merge(['year' => Carbon::now('Asia/Jakarta')->subYears(1)->format('Y')]);
        $request->merge(['currency' => $code]);
        
        $result = json_decode($kurs->getKurs($request),true);

        if(!empty($result)){
            return $result;
        }

        return false;

    }
}
