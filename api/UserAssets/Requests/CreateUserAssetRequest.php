<?php

namespace Api\UserAssets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserAssetRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_asset' => 'array|required',
            'user_asset.user_id' => 'required|integer',
            'user_asset.asset_id' => 'required|integer',
            'user_asset.user_revealed_asset_id' => 'integer',
            'user_asset.acquisition_year' => 'required|integer',
            'user_asset.idr_value' => 'required|between:0,9999999999.99',
            'user_asset.detail' => 'string',
            'user_asset.is_ta' => 'integer',
        ];
    }

    
    public function attributes()
    {
        return [
            'user_asset.detail' => 'the user asset\'s detail'
        ];
    }
}
