<?php

namespace Api\SpouseTaxProfiles\Models;

use Illuminate\Database\Eloquent\Model;

class SpouseTaxProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'job_status_id', 'klu_id', 'name', 'nik'
    ];
}
