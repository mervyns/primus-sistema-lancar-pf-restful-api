<?php

namespace Api\SpouseTaxProfiles;

use Infrastructure\Events\EventServiceProvider;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasCreated;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasDeleted;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasUpdated;

class SpouseTaxProfileServiceProvider extends EventServiceProvider
{
    protected $listen = [
        SpouseTaxProfileWasCreated::class => [
            // listeners for when a spouse tax profile is created
        ],
        SpouseTaxProfileWasDeleted::class => [
            // listeners for when a spouse tax profile is deleted
        ],
        SpouseTaxProfileWasUpdated::class => [
            // listeners for when a spouse tax profile is updated
        ],
    ];
}
