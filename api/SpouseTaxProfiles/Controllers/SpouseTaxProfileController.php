<?php

namespace Api\SpouseTaxProfiles\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\SpouseTaxProfiles\Requests\CreateSpouseTaxProfileRequest;
use Api\SpouseTaxProfiles\Services\SpouseTaxProfileService;

class SpouseTaxProfileController extends Controller
{
    private $spouseTaxProfileService;

    public function __construct(SpouseTaxProfileService $spouseTaxProfileService)
    {
        $this->spouseTaxProfileService = $spouseTaxProfileService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->spouseTaxProfileService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'spouse_tax_profiles');

        return $this->response($parsedData);
    }

    public function getById($spouseTaxProfileServiceId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->spouseTaxProfileService->getById($spouseTaxProfileServiceId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'spouse_tax_profile');

        return $this->response($parsedData);
    }

    public function create(CreateSpouseTaxProfileRequest $request)
    {
        $data = $request->get('spouse_tax_profile', []);

        return $this->response($this->spouseTaxProfileService->create($data), 201);
    }

    public function update($spouseTaxProfileServiceId, Request $request)
    {
        $data = $request->get('spouse_tax_profile', []);

        return $this->response($this->spouseTaxProfileService->update($spouseTaxProfileServiceId, $data));
    }

    public function delete($spouseTaxProfileServiceId)
    {
        return $this->response($this->spouseTaxProfileService->delete($spouseTaxProfileServiceId));
    }
}
