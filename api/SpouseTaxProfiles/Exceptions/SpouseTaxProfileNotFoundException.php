<?php

namespace Api\SpouseTaxProfiles\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SpouseTaxProfileNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The spouse tax profile was not found.');
    }
}
