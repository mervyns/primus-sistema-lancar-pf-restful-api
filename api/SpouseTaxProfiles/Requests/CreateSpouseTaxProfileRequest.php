<?php

namespace Api\SpouseTaxProfiles\Requests;

use Infrastructure\Http\ApiRequest;

class CreateSpouseTaxProfileRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'spouse_tax_profile' => 'array|required',
            'spouse_tax_profile.user_id' => 'required|integer',
            'spouse_tax_profile.job_status_id' => 'integer',
            'spouse_tax_profile.klu_id' => 'integer',
            'spouse_tax_profile.name' => 'required|string',
            'spouse_tax_profile.nik' => 'required|numeric'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_tax_profile.name' => 'the spouse\'s name'
        ];
    }
}
