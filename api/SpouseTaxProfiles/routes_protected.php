<?php

$router->get('/api/spouse-tax-profiles', 'SpouseTaxProfileController@getAll');
$router->get('/api/spouse-tax-profiles/{id}', 'SpouseTaxProfileController@getById');
$router->post('/api/spouse-tax-profiles', 'SpouseTaxProfileController@create');
$router->put('/api/spouse-tax-profiles/{id}', 'SpouseTaxProfileController@update');
$router->delete('/api/spouse-tax-profiles/{id}', 'SpouseTaxProfileController@delete');