<?php

namespace Api\SpouseTaxProfiles\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\SpouseTaxProfiles\Exceptions\SpouseTaxProfileNotFoundException;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasCreated;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasDeleted;
use Api\SpouseTaxProfiles\Events\SpouseTaxProfileWasUpdated;
use Api\SpouseTaxProfiles\Repositories\SpouseTaxProfileRepository;
use Api\Users\Repositories\UserRepository;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;

class SpouseTaxProfileService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $spouseTaxProfileRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        SpouseTaxProfileRepository $spouseTaxProfileRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->spouseTaxProfileRepository = $spouseTaxProfileRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $kluRepository = new KlasifikasiLapanganUsahaRepository();


        $spouseTaxProfile = $this->spouseTaxProfile->get($options);

        $i=0;
        foreach ($spouseTaxProfile as $sTP) {
            $user = $userRepository->getById($sTP['user_id']);
            $klu = $kluRepository->getById($sTP['klu_id']);

            $spouseTaxProfile[$i]['user_name'] = $user['name'];
            $spouseTaxProfile[$i]['klu_name'] = $klu['name'];

            $i++;
        }

        return $spouseTaxProfile;
    }

    public function getById($spouseTaxProfileId, array $options = [])
    {
        $userRepository = new UserRepository();
        $kluRepository = new KlasifikasiLapanganUsahaRepository();

        $spouseTaxProfile = $this->getRequestedSpouseTaxProfile($spouseTaxProfileId);

        $user = $userRepository->getById($spouseTaxProfile['user_id']);
        $klu = $kluRepository->getById($spouseTaxProfile['klu_id']);

        $spouseTaxProfile['user_name'] = $user['name'];
        $spouseTaxProfile['klu_name'] = $klu['name'];

        return $spouseTaxProfile;
    }

    public function create($data)
    {
        $spouseTaxProfile = $this->spouseTaxProfileRepository->create($data);

        $this->dispatcher->fire(new SpouseTaxProfileWasCreated($spouseTaxProfile));

        return $spouseTaxProfile;
    }

    public function update($spouseTaxProfileId, array $data)
    {
        $spouseTaxProfile = $this->getRequestedSpouseTaxProfile($spouseTaxProfileId);

        $this->spouseTaxProfileRepository->update($spouseTaxProfile, $data);

        $this->dispatcher->fire(new SpouseTaxProfileWasUpdated($spouseTaxProfile));

        return $spouseTaxProfile;
    }

    public function delete($spouseTaxProfileId)
    {
        $spouseTaxProfile = $this->getRequestedSpouseTaxProfile($spouseTaxProfileId);

        $this->spouseTaxProfileRepository->delete($spouseTaxProfileId);

        $this->dispatcher->fire(new SpouseTaxProfileWasDeleted($spouseTaxProfile));

        return true;
    }

    private function getRequestedSpouseTaxProfile($spouseTaxProfileId)
    {
        $spouseTaxProfile = $this->spouseTaxProfileRepository->getById($spouseTaxProfileId);

        if (is_null($spouseTaxProfile)) {
            throw new SpouseTaxProfileNotFoundException();
        }

        return $spouseTaxProfile;
    }
}
