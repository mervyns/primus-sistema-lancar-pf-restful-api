<?php

namespace Api\SpouseTaxProfiles\Repositories;

use Api\SpouseTaxProfiles\Models\SpouseTaxProfile;
use Infrastructure\Database\Eloquent\Repository;

class SpouseTaxProfileRepository extends Repository
{
    public function getModel()
    {
        return new SpouseTaxProfile();
    }

    public function create(array $data)
    {
        $spouseTaxProfile = $this->getModel();

        $spouseTaxProfile->fill($data);
        
        $spouseTaxProfile->save();

        return $spouseTaxProfile;
    }

    public function update(SpouseTaxProfile $spouseTaxProfile, array $data)
    {
        $spouseTaxProfile->fill($data);

        $spouseTaxProfile->save();

        return $spouseTaxProfile;
    }
}
