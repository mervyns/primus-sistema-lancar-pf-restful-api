<?php

namespace Api\SpouseTaxProfiles\Events;

use Infrastructure\Events\Event;
use Api\SpouseTaxProfiles\Models\SpouseTaxProfile;

class SpouseTaxProfileWasUpdated extends Event
{
    public $spouseTaxProfile;

    public function __construct(SpouseTaxProfile $spouseTaxProfile)
    {
        $this->spouseTaxProfile = $spouseTaxProfile;
    }
}
