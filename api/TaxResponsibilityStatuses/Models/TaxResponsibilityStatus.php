<?php

namespace Api\TaxResponsibilityStatuses\Models;

use Illuminate\Database\Eloquent\Model;

class TaxResponsibilityStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name'
    ];
}
