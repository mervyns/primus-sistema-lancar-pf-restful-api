<?php

namespace Api\TaxResponsibilityStatuses\Requests;

use Infrastructure\Http\ApiRequest;

class CreateTaxResponsibilityStatusRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tax_responsibility_status' => 'array|required',
            'tax_responsibility_status.code' => 'required|string',
            'tax_responsibility_status.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'tax_responsibility_status.name' => 'the tax responsibility status\' name'
        ];
    }
}
