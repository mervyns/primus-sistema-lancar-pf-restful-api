<?php

namespace Api\TaxResponsibilityStatuses;

use Infrastructure\Events\EventServiceProvider;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasCreated;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasDeleted;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasUpdated;

class TaxResponsibilityStatusServiceProvider extends EventServiceProvider
{
    protected $listen = [
        TaxResponsibilityStatusWasCreated::class => [
            // listeners for when a tax responsibility status is created
        ],
        TaxResponsibilityStatusWasDeleted::class => [
            // listeners for when a tax responsibility status is deleted
        ],
        TaxResponsibilityStatusWasUpdated::class => [
            // listeners for when a tax responsibility status is updated
        ],
    ];
}
