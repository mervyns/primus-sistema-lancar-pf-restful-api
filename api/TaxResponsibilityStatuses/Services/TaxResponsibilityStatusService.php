<?php

namespace Api\TaxResponsibilityStatuses\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\TaxResponsibilityStatuses\Exceptions\TaxResponsibilityStatusNotFoundException;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasCreated;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasDeleted;
use Api\TaxResponsibilityStatuses\Events\TaxResponsibilityStatusWasUpdated;
use Api\TaxResponsibilityStatuses\Repositories\TaxResponsibilityStatusRepository;

class TaxResponsibilityStatusService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $taxResponsibilityStatusRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        TaxResponsibilityStatusRepository $taxResponsibilityStatusRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->taxResponsibilityStatusRepository = $taxResponsibilityStatusRepository;
    }

    public function getAll($options = [])
    {
        return $this->taxResponsibilityStatusRepository->get($options);
    }

    public function getById($taxResponsibilityStatusId, array $options = [])
    {
        $taxResponsibilityStatus = $this->getRequestedTaxResponsibilityStatus($taxResponsibilityStatusId);

        return $taxResponsibilityStatus;
    }

    public function create($data)
    {
        $taxResponsibilityStatus = $this->taxResponsibilityStatusRepository->create($data);

        $this->dispatcher->fire(new TaxResponsibilityStatusWasCreated($taxResponsibilityStatus));

        return $taxResponsibilityStatus;
    }

    public function update($taxResponsibilityStatusId, array $data)
    {
        $taxResponsibilityStatus = $this->getRequestedTaxResponsibilityStatus($taxResponsibilityStatusId);

        $this->taxResponsibilityStatusRepository->update($taxResponsibilityStatus, $data);

        $this->dispatcher->fire(new TaxResponsibilityStatusWasUpdated($taxResponsibilityStatus));

        return $taxResponsibilityStatus;
    }

    public function delete($taxResponsibilityStatusId)
    {
        $taxResponsibilityStatus = $this->getRequestedTaxResponsibilityStatus($taxResponsibilityStatusId);

        $this->taxResponsibilityStatusRepository->delete($taxResponsibilityStatusId);

        $this->dispatcher->fire(new TaxResponsibilityStatusWasDeleted($taxResponsibilityStatus));

        return true;
    }

    private function getRequestedTaxResponsibilityStatus($taxResponsibilityStatusId)
    {
        $taxResponsibilityStatus = $this->taxResponsibilityStatusRepository->getById($taxResponsibilityStatusId);

        if (is_null($taxResponsibilityStatus)) {
            throw new TaxResponsibilityStatusNotFoundException();
        }

        return $taxResponsibilityStatus;
    }
}
