<?php

namespace Api\TaxResponsibilityStatuses\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaxResponsibilityStatusNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The tax responsibility status was not found.');
    }
}
