<?php

namespace Api\TaxResponsibilityStatuses\Repositories;

use Api\TaxResponsibilityStatuses\Models\TaxResponsibilityStatus;
use Infrastructure\Database\Eloquent\Repository;

class TaxResponsibilityStatusRepository extends Repository
{
    public function getModel()
    {
        return new TaxResponsibilityStatus();
    }

    public function create(array $data)
    {
        $taxResponsibilityStatus = $this->getModel();

        $taxResponsibilityStatus->fill($data);
        
        $taxResponsibilityStatus->save();

        return $taxResponsibilityStatus;
    }

    public function update(TaxResponsibilityStatus $taxResponsibilityStatus, array $data)
    {

        $taxResponsibilityStatus->fill($data);

        $taxResponsibilityStatus->save();

        return $taxResponsibilityStatus;
    }
}
