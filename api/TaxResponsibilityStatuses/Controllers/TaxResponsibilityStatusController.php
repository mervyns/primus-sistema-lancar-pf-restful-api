<?php

namespace Api\TaxResponsibilityStatuses\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\TaxResponsibilityStatuses\Requests\CreateTaxResponsibilityStatusRequest;
use Api\TaxResponsibilityStatuses\Services\TaxResponsibilityStatusService;

class TaxResponsibilityStatusController extends Controller
{
    private $taxResponsibilityStatusService;

    public function __construct(TaxResponsibilityStatusService $taxResponsibilityStatusService)
    {
        $this->taxResponsibilityStatusService = $taxResponsibilityStatusService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxResponsibilityStatusService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_responsibility_statuses');

        return $this->response($parsedData);
    }

    public function getById($taxResponsibilityStatusId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxResponsibilityStatusService->getById($taxResponsibilityStatusId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_responsibility_status');

        return $this->response($parsedData);
    }

    public function create(CreateTaxResponsibilityStatusRequest $request)
    {
        $data = $request->get('tax_responsibility_status', []);

        return $this->response($this->taxResponsibilityStatusService->create($data), 201);
    }

    public function update($taxResponsibilityStatusId, Request $request)
    {
        $data = $request->get('tax_responsibility_status', []);

        return $this->response($this->taxResponsibilityStatusService->update($taxResponsibilityStatusId, $data));
    }

    public function delete($taxResponsibilityStatusId)
    {
        return $this->response($this->taxResponsibilityStatusService->delete($taxResponsibilityStatusId));
    }
}
