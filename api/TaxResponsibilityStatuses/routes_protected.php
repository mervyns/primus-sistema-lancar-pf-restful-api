<?php

$router->get('/api/tax-responsibility-statuses', 'TaxResponsibilityStatusController@getAll');
$router->get('/api/tax-responsibility-statuses/{id}', 'TaxResponsibilityStatusController@getById');
$router->post('/api/tax-responsibility-statuses', 'TaxResponsibilityStatusController@create');
$router->put('/api/tax-responsibility-statuses/{id}', 'TaxResponsibilityStatusController@update');
$router->delete('/api/tax-responsibility-statuses/{id}', 'TaxResponsibilityStatusController@delete');