<?php

namespace Api\TaxResponsibilityStatuses\Events;

use Infrastructure\Events\Event;
use Api\TaxResponsibilityStatuses\Models\TaxResponsibilityStatus;

class TaxResponsibilityStatusWasUpdated extends Event
{
    public $taxResponsibilityStatus;

    public function __construct(TaxResponsibilityStatus $taxResponsibilityStatus)
    {
        $this->taxResponsibilityStatus = $taxResponsibilityStatus;
    }
}
