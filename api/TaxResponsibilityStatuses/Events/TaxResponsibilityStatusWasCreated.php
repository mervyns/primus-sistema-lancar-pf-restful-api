<?php

namespace Api\TaxResponsibilityStatuses\Events;

use Infrastructure\Events\Event;
use Api\TaxResponsibilityStatuses\Models\TaxResponsibilityStatus;

class TaxResponsibilityStatusWasCreated extends Event
{
    public $taxResponsibilityStatus;

    public function __construct(TaxResponsibilityStatus $taxResponsibilityStatus)
    {
        $this->taxResponsibilityStatus = $taxResponsibilityStatus;
    }
}
