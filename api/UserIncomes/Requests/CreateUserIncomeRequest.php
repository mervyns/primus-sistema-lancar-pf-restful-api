<?php

namespace Api\UserIncomes\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserIncomeRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_income' => 'array|required',
            'user_income.user_id' => 'required|integer',
            'user_income.income_id' => 'required|integer',
            'user_income.country_id' => 'required|integer',
            'user_income.user_asset_id' => 'integer',
            'user_income.user_foreign_asset_id' => 'integer',
            'user_income.asset_id' => 'integer',
            'user_income.net_value' => 'between:0,9999999999.99',
            'user_income.gross_value' => 'between:0,9999999999.99',
            'user_income.detail' => 'string'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_income.detail' => 'the user income\'s detail'
        ];
    }
}
