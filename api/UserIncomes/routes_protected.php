<?php

$router->get('/api/user-incomes', 'UserIncomeController@getAll');
$router->get('/api/user-incomes/penghasilan/{user_id}', 'UserIncomeController@getPenghasilan');
$router->get('/api/user-incomes/{id}', 'UserIncomeController@getById');
$router->post('/api/user-incomes', 'UserIncomeController@create');
$router->put('/api/user-incomes/{id}', 'UserIncomeController@update');
$router->delete('/api/user-incomes/{id}', 'UserIncomeController@delete');