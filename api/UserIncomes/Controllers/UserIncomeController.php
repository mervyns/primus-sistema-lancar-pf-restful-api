<?php

namespace Api\UserIncomes\Controllers;

use View;
use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserIncomes\Requests\CreateUserIncomeRequest;
use Api\UserIncomes\Services\UserIncomeService;

class UserIncomeController extends Controller
{
    private $userIncomeService;

    public function __construct(UserIncomeService $userIncomeService)
    {
        $this->userIncomeService = $userIncomeService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userIncomeService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_incomes');

        return $this->response($parsedData);
    }

    public function getById($userIncomeId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userIncomeService->getById($userIncomeId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_income');

        return $this->response($parsedData);
    }

    public function create(CreateUserIncomeRequest $request)
    {
        $data = $request->get('user_income', []);

        return $this->response($this->userIncomeService->create($data), 201);
    }

    public function update($userIncomeId, Request $request)
    {
        $data = $request->get('user_income', []);

        return $this->response($this->userIncomeService->update($userIncomeId, $data));
    }

    public function delete($userIncomeId)
    {
        return $this->response($this->userIncomeService->delete($userIncomeId));
    }

    public function getPenghasilan($userId){
        $data = $this->userIncomeService->getPenghasilan($userId);
        $html = View::make("partial.table_final_step_penghasilan", compact('data'))->render();
        return response()->json(compact('html'));
    }
}
