<?php

namespace Api\UserIncomes\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserIncomeNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user income was not found.');
    }
}
