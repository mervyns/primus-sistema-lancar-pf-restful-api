<?php

namespace Api\UserIncomes\Models;

use Illuminate\Database\Eloquent\Model;

class UserIncome extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'income_id', 'country_id', 'user_asset_id', 'user_foreign_asset_id', 'asset_id','net_value', 'gross_value', 'detail'
    ];

    public function withHoldingTax()
    {
        return $this->hasOne('Api\UserWithholdingTaxes\Models\UserWithholdingTax', 'user_income_id');
    }

    public function incomes()
    {
        return $this->belongsTo('Api\Incomes\Models\Income','income_id');
    }

    public function incomesQuery($id)
    {
        return \DB::table('users')
            ->select([
                'user_incomes.*',
                'user_incomes.id AS user_income_id', 
                'incomes.id AS income_id', 
                'incomes.name AS income_name', 
                'incomes.step AS income_step', 
                'income_categories.id AS income_categories_id', 
                'income_categories.name AS income_categories_name', 
            ])
            ->join('user_incomes', 'users.id', '=', 'user_incomes.user_id')
            ->join('incomes', 'incomes.id', '=', 'user_incomes.income_id')
            ->join('income_categories', 'income_categories.id', '=', 'incomes.income_category_id')
            ->orderBy('incomes.name','asc')
            ->where('users.id',$id);
    }

}
