<?php

namespace Api\UserIncomes;

use Infrastructure\Events\EventServiceProvider;
use Api\UserIncomes\Events\UserIncomeWasCreated;
use Api\UserIncomes\Events\UserIncomeWasDeleted;
use Api\UserIncomes\Events\UserIncomeWasUpdated;

class CountryServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserIncomeWasCreated::class => [
            // listeners for when a user income is created
        ],
        UserIncomeWasDeleted::class => [
            // listeners for when a user income is deleted
        ],
        UserIncomeWasUpdated::class => [
            // listeners for when a user income is updated
        ],
    ];
}
