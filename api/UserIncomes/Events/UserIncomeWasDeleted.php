<?php

namespace Api\UserIncomes\Events;

use Infrastructure\Events\Event;
use Api\UserIncomes\Models\UserIncome;

class UserIncomeWasDeleted extends Event
{
    public $userIncome;

    public function __construct(UserIncome $userIncome)
    {
        $this->userIncome = $userIncome;
    }
}
