<?php

namespace Api\UserIncomes\Repositories;

use Api\UserIncomes\Models\UserIncome;
use Infrastructure\Database\Eloquent\Repository;

class UserIncomeRepository extends Repository
{
    public function getModel()
    {
        return new UserIncome();
    }

    public function create(array $data)
    {
        $userIncome = $this->getModel();

        $userIncome->fill($data);
        
        $userIncome->save();

        return $userIncome;
    }

    public function update(UserIncome $userIncome, array $data)
    {

        $userIncome->fill($data);

        $userIncome->save();

        return $userIncome;
    }
}
