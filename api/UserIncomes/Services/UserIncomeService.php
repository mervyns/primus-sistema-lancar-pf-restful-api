<?php

namespace Api\UserIncomes\Services;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserIncomes\Exceptions\UserIncomeNotFoundException;
use Api\UserIncomes\Events\UserIncomeWasCreated;
use Api\UserIncomes\Events\UserIncomeWasDeleted;
use Api\UserIncomes\Events\UserIncomeWasUpdated;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\Users\Repositories\UserRepository;
use Api\Incomes\Repositories\IncomeRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserForeignAssets\Repositories\UserForeignAssetRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\KursPajak\Controllers\KursPajakController;
use WebApp\TaxFilling\FormData\FinalSteps;

class UserIncomeService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userIncomeRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserIncomeRepository $userIncomeRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userIncomeRepository = $userIncomeRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $incomeRepository = new IncomeRepository();
        $countryRepository = new CountryRepository();
        $userAssetRepository = new UserAssetRepository();
        $userForeignAssetRepository = new UserForeignAssetRepository();
        $assetRepository = new AssetRepository();

        $userIncome = $this->userIncomeRepository->get($options);

        $i=0;
        foreach ($userIncome as $uI) {
            $user = $userRepository->getById($uI['user_id']);
            $income = $incomeRepository->getById($uI['income_id']);
            $country = $countryRepository->getById($uI['country_id']);
            if(!empty($uI['user_asset_id'])){
                $userAsset = $userAssetRepository->getById($uI['user_asset_id']);
                $asset = $assetRepository->getById($userAsset['asset_id']);
            }
                
            if(!empty($uI['user_foreign_asset_id'])){
                $userForeignAsset = $userForeignAssetRepository->getById($uI['user_foreign_asset_id']);
                $asset = $assetRepository->getById($userForeignAsset['asset_id']);
            }
            
            if(!empty($uI['asset_id'])){
                 $asset = $assetRepository->getById($uI['asset_id']);
            }

            $userIncome[$i]['user_name'] = $user['name'];
            $userIncome[$i]['income_name'] = $income['name'];
            $userIncome[$i]['country_name'] = $country['name'];
            $userIncome[$i]['currency'] = $country['currency'];
            $userIncome[$i]['currency_name'] = $country['currency_name'];
            if(!empty($asset['name']))
                $userIncome[$i]['user_asset_source_name'] = $asset['name'];

            $i++;
        }

        return $userIncome;
    }

    public function getById($userIncomeId, array $options = [])
    {
        $userRepository = new UserRepository();
        $incomeRepository = new IncomeRepository();
        $countryRepository = new CountryRepository();
        $userAssetRepository = new UserAssetRepository();
        $userForeignAssetRepository = new UserForeignAssetRepository();
        $assetRepository = new AssetRepository();

        $userIncome = $this->getRequestedUserIncome($userIncomeId);

        $user = $userRepository->getById($userIncome['user_id']);
        $income = $incomeRepository->getById($userIncome['income_id']);
        $country = $countryRepository->getById($userIncome['country_id']);
        if(!empty($userIncome['user_asset_id'])){
            $userAsset = $userAssetRepository->getById($userIncome['user_asset_id']);
            $asset = $assetRepository->getById($userAsset['asset_id']);
        }
            
        if(!empty($userIncome['user_foreign_asset_id'])){
            $userForeignAsset = $userForeignAssetRepository->getById($userIncome['user_foreign_asset_id']);
            $asset = $assetRepository->getById($userForeignAsset['asset_id']);
        }
        
        if(!empty($userIncome['asset_id'])){
             $asset = $assetRepository->getById($userIncome['asset_id']);
        }

        $userIncome['user_name'] = $user['name'];
        $userIncome['income_name'] = $income['name'];
        $userIncome['country_name'] = $country['name'];
        $userIncome['currency'] = $country['currency'];
        $userIncome['currency_name'] = $country['currency_name'];
        if(!empty($asset['name']))
            $userIncome['user_asset_source_name'] = $asset['name'];

        return $userIncome;
    }

    public function create($data)
    {
        $userIncome = $this->userIncomeRepository->create($data);

        $this->dispatcher->fire(new UserIncomeWasCreated($userIncome));

        return $userIncome;
    }

    public function update($userIncomeId, array $data)
    {
        $userIncome = $this->getRequestedUserIncome($userIncomeId);

        $this->userIncomeRepository->update($userIncome, $data);

        $this->dispatcher->fire(new UserIncomeWasUpdated($userIncome));

        return $userIncome;
    }

    public function delete($userIncomeId)
    {
        $userIncome = $this->getRequestedUserIncome($userIncomeId);

        $this->userIncomeRepository->delete($userIncomeId);

        $this->dispatcher->fire(new UserIncomeWasDeleted($userIncome));

        return true;
    }

    private function getRequestedUserIncome($userIncomeId)
    {
        $userIncome = $this->userIncomeRepository->getById($userIncomeId);

        if (is_null($userIncome)) {
            throw new UserIncomeNotFoundException();
        }

        return $userIncome;
    }

    public function getPenghasilan($id){
        try{
        
            $userRepository = new UserRepository();
            $incomeRepository = new IncomeRepository();
            $countryRepository = new CountryRepository();
            $userAssetRepository = new UserAssetRepository();
            $userForeignAssetRepository = new UserForeignAssetRepository();
            $assetRepository = new AssetRepository();

			$result = [
				'penghasilan' => []
            ];
            
			$user_incomes = $this->userIncomeRepository->getModel()->incomesQuery($id);
		
			$finalStep = new FinalSteps();
            $result = $finalStep->mapingDataForAjax($user_incomes, 'penghasilan');
            
		}catch(\Exception $e){
			dd($e);
		}
		

		return [
			'data' => $result
		];
    }

    private function getCurrency($code){
        $kurs = new KursPajakController();
        $request = new \Illuminate\Http\Request();
        $request->merge(['month' => 12]);
        $request->merge(['date' => 31]);
        $request->merge(['year' => Carbon::now('Asia/Jakarta')->subYears(1)->format('Y')]);
        $request->merge(['currency' => $code]);
        
        $result = json_decode($kurs->getKurs($request),true);

        if(!empty($result)){
            return $result;
        }

        return false;

    }
}
