<?php

$router->get('/api/roles', 'RoleController@getAll');
$router->get('/api/roles/{id}', 'RoleController@getById');
$router->post('/api/roles', 'RoleController@create');
$router->put('/api/roles/{id}', 'RoleController@update');
$router->delete('/api/roles/{id}', 'RoleController@delete');
