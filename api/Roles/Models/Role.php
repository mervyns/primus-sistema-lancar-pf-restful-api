<?php

namespace Api\Roles\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'alias',
    ];

    public function users()
    {
        return $this->belongsToMany('Api\Users\Models\User');
    }
}
