<?php

namespace Api\Incomes\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Incomes\Requests\CreateIncomeCategoryRequest;
use Api\Incomes\Services\IncomeCategoryService;

class IncomeCategoryController extends Controller
{
    private $incomeCategoryService;

    public function __construct(IncomeCategoryService $incomeCategoryService)
    {
        $this->incomeCategoryService = $incomeCategoryService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->incomeCategoryService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'income_categories');

        return $this->response($parsedData);
    }

    public function getById($incomeCategoryId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->incomeCategoryService->getById($incomeCategoryId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'income_category');

        return $this->response($parsedData);
    }

    public function create(CreateIncomeCategoryRequest $request)
    {
        $data = $request->get('income_category', []);

        return $this->response($this->incomeCategoryService->create($data), 201);
    }

    public function update($incomeCategoryId, Request $request)
    {
        $data = $request->get('income_category', []);

        return $this->response($this->incomeCategoryService->update($incomeCategoryId, $data));
    }

    public function delete($incomeCategoryId)
    {
        return $this->response($this->incomeCategoryService->delete($incomeCategoryId));
    }
}
