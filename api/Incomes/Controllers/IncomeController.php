<?php

namespace Api\Incomes\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Incomes\Requests\CreateIncomeRequest;
use Api\Incomes\Services\IncomeService;

class IncomeController extends Controller
{
    private $incomeService;

    public function __construct(IncomeService $incomeService)
    {
        $this->incomeService = $incomeService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->incomeService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'incomes');

        return $this->response($parsedData);
    }

    public function getById($incomeId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->incomeService->getById($incomeId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'income');

        return $this->response($parsedData);
    }

    public function create(CreateIncomeRequest $request)
    {
        $data = $request->get('income', []);

        return $this->response($this->incomeService->create($data), 201);
    }

    public function update($incomeId, Request $request)
    {
        $data = $request->get('income', []);

        return $this->response($this->incomeService->update($incomeId, $data));
    }

    public function assignToCategory($incomeId, Request $request)
    {
        $category = $request->get('category');

        return $this->response($this->incomeService->assignToCategory($incomeId, $category));
    }

    public function delete($incomeId)
    {
        return $this->response($this->incomeService->delete($incomeId));
    }
}
