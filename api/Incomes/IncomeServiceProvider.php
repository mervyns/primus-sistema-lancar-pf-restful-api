<?php

namespace Api\Incomes;

use Infrastructure\Events\EventServiceProvider;
use Api\Incomes\Events\IncomeWasCreated;
use Api\Incomes\Events\IncomeWasDeleted;
use Api\Incomes\Events\IncomeWasUpdated;
use Api\Incomes\Events\IncomeWasAssignedToCategory;
use Api\Incomes\Events\IncomeCategoryWasCreated;
use Api\Incomes\Events\IncomeCategoryWasDeleted;
use Api\Incomes\Events\IncomeCategoryWasUpdated;

class IncomeServiceProvider extends EventServiceProvider
{
    protected $listen = [
        IncomeWasCreated::class => [
            // listeners for when a income is created
        ],
        IncomeWasDeleted::class => [
            // listeners for when a income is deleted
        ],
        IncomeWasUpdated::class => [
            // listeners for when a income is updated
        ],
        IncomeWasAssignedToCategory::class => [
            // listeners for when a income was assigned to category
        ],
        IncomeCategoryWasCreated::class => [
            // listeners for when a income category is created
        ],
        IncomeCategoryWasDeleted::class => [
            // listeners for when a income category is deleted
        ],
        IncomeCategoryWasUpdated::class => [
            // listeners for when a income category is updated
        ],
    ];
}
