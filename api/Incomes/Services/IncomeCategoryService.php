<?php

namespace Api\Incomes\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Incomes\Exceptions\IncomeCategoryNotFoundException;
use Api\Incomes\Events\IncomeCategoryWasCreated;
use Api\Incomes\Events\IncomeCategoryWasDeleted;
use Api\Incomes\Events\IncomeCategoryWasUpdated;
use Api\Incomes\Repositories\IncomeCategoryRepository;

class IncomeCategoryService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $incomeCategoryRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        IncomeCategoryRepository $incomeCategoryRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->incomeCategoryRepository = $incomeCategoryRepository;
    }

    public function getAll($options = [])
    {
        return $this->incomeCategoryRepository->get($options);
    }

    public function getById($incomeCategoryId, array $options = [])
    {
        $incomeCategory = $this->getRequestedIncomeCategory($incomeCategoryId);

        return $incomeCategory;
    }

    public function create($data)
    {
        $incomeCategory = $this->incomeCategoryRepository->create($data);

        $this->dispatcher->fire(new IncomeCategoryWasCreated($incomeCategory));

        return $incomeCategory;
    }

    public function update($incomeCategoryId, array $data)
    {
        $incomeCategory = $this->getRequestedIncomeCategory($incomeCategoryId);

        $this->incomeCategoryRepository->update($incomeCategory, $data);

        $this->dispatcher->fire(new IncomeCategoryWasUpdated($incomeCategory));

        return $incomeCategory;
    }

    public function delete($incomeCategoryId)
    {
        $incomeCategory = $this->getRequestedIncomeCategory($incomeCategoryId);

        $this->incomeCategoryRepository->delete($incomeCategoryId);

        $this->dispatcher->fire(new IncomeCategoryWasDeleted($incomeCategory));

        return true;
    }

    private function getRequestedIncomeCategory($incomeCategoryId)
    {
        $incomeCategory = $this->incomeCategoryRepository->getById($incomeCategoryId);

        if (is_null($incomeCategory)) {
            throw new IncomeCategoryNotFoundException();
        }

        return $incomeCategory;
    }
}
