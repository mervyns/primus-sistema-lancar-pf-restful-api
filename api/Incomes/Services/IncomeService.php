<?php

namespace Api\Incomes\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Incomes\Exceptions\IncomeNotFoundException;
use Api\Incomes\Events\IncomeWasCreated;
use Api\Incomes\Events\IncomeWasDeleted;
use Api\Incomes\Events\IncomeWasUpdated;
use Api\Incomes\Events\IncomeWasAssignedToCategory;
use Api\Incomes\Repositories\IncomeRepository;

class IncomeService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $incomeRepository;

    public function __construct(
    //    AuthManager $auth,
        DatabaseManager $database,
        Dispatcher $dispatcher,
        IncomeRepository $incomeRepository
    ) {
    //    $this->auth = $auth;
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->incomeRepository = $incomeRepository;
    }

    public function getAll($options = [])
    {
        return $this->incomeRepository->get($options);
    }

    public function getById($incomeId, array $options = [])
    {
        $income = $this->getRequestedIncome($incomeId);

        return $income;
    }

    public function create($data)
    {
        $income = $this->incomeRepository->create($data);

        $this->dispatcher->fire(new IncomeWasCreated($income));

        return $income;
    }

    public function update($incomeId, array $data)
    {
        $income = $this->getRequestedIncome($incomeId);

        $this->incomeRepository->update($income, $data);

        $this->dispatcher->fire(new IncomeWasUpdated($income));

        return $income;
    }

    public function delete($incomeId)
    {
        $income = $this->getRequestedIncome($incomeId);

        $this->incomeRepository->delete($incomeId);

        $this->dispatcher->fire(new IncomeWasDeleted($income));

        return true;
    }

    private function getRequestedIncome($incomeId)
    {
        $income = $this->incomeRepository->getById($incomeId);

        if (is_null($income)) {
            throw new IncomeNotFoundException();
        }

        return $income;
    }

    public function assignToCategory($incomeId, $category)
    {
        $income = $this->getRequestedIncome($incomeId);

        $this->dispatcher->fire(new IncomeWasAssignedToCategory($income));

        return $this->incomeRepository->assignCategoryToIncome($income, $category);
    }
}
