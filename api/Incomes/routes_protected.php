<?php

$router->get('/api/incomes', 'IncomeController@getAll');
$router->get('/api/incomes/{id}', 'IncomeController@getById');
$router->post('/api/incomes', 'IncomeController@create');
$router->put('/api/incomes/{id}', 'IncomeController@update');
$router->put('/api/incomes/{id}/assign-category', 'IncomeController@assignToCategory');
$router->delete('/api/incomes/{id}', 'IncomeController@delete');

$router->get('/api/incomes/categories', 'IncomeCategoryController@getAll');
$router->get('/api/incomes/categories/{id}', 'IncomeCategoryController@getById');
$router->post('/api/incomes/categories', 'IncomeCategoryController@create');
$router->put('/api/incomes/categories/{id}', 'IncomeCategoryController@update');
$router->delete('/api/incomes/categories/{id}', 'IncomeCategoryController@delete');
