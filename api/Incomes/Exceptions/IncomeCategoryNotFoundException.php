<?php

namespace Api\Incomes\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IncomeCategoryNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The income category was not found.');
    }
}
