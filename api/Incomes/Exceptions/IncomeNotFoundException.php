<?php

namespace Api\Incomes\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IncomeNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The income was not found.');
    }
}
