<?php

namespace Api\Incomes\Events;

use Infrastructure\Events\Event;
use Api\Incomes\Models\IncomeCategory;

class IncomeCategoryWasDeleted extends Event
{
    public $incomeCategory;

    public function __construct(IncomeCategory $incomeCategory)
    {
        $this->incomeCategory = $incomeCategory;
    }
}
