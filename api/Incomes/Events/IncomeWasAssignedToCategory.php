<?php

namespace Api\Incomes\Events;

use Infrastructure\Events\Event;
use Api\Incomes\Models\Income;

class IncomeWasAssignedToCategory extends Event
{
    public $income;

    public function __construct(Income $income)
    {
        $this->income = $income;
    }
}
