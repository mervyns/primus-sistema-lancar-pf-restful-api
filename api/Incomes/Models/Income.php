<?php

namespace Api\Incomes\Models;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'tax_rate', 'tax_rate_type_id', 'lower_limit', 'upper_limit', 'tax_provision'
    ];

    public function category()
    {
        return $this->belongsTo('Api\Incomes\Models\IncomeCategory','income_category_id');
    }

    public function taxRateType() {
        return $this->belongsTo('Api\TaxRateTypes\Models\TaxRateType');
    }
}
