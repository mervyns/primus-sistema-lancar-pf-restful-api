<?php

namespace Api\Incomes\Models;

use Illuminate\Database\Eloquent\Model;

class IncomeCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function incomes()
    {
        return $this->hasMany('Api\Incomes\Models\Income');
    }
}
