<?php

namespace Api\Incomes\Repositories;

use Api\Incomes\Models\IncomeCategory;
use Infrastructure\Database\Eloquent\Repository;

class IncomeCategoryRepository extends Repository
{
    public function getModel()
    {
        return new IncomeCategory();
    }

    public function create(array $data)
    {
        $incomeCategory = $this->getModel();

        $incomeCategory->fill($data);
        
        $incomeCategory->save();

        return $incomeCategory;
    }

    public function update(IncomeCategory $incomeCategory, array $data)
    {

        $incomeCategory->fill($data);

        $incomeCategory->save();

        return $incomeCategory;
    }
}
