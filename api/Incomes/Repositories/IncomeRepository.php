<?php

namespace Api\Incomes\Repositories;

use Api\Incomes\Models\Income;
use Api\Incomes\Models\IncomeCategory;
use Infrastructure\Database\Eloquent\Repository;

class IncomeRepository extends Repository
{
    public function getModel()
    {
        return new Income();
    }

    public function create(array $data)
    {
        $income = $this->getModel();

        $income->fill($data);
        
        $income->save();

        return $income;
    }

    public function update(Income $income, array $data)
    {

        $income->fill($data);

        $income->save();

        return $income;
    }

    public function assignCategoryToIncome(Income $income, $category)
    {
        $incomeCategoryModel = new IncomeCategory();

        if($incomeCategoryModel->where('id', '=', $category)->count() > 0){
            $income->income_category_id = $category;

            $income->save();
        }

        return $income;
    }
}
