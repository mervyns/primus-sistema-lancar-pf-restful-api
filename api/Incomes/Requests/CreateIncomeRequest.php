<?php

namespace Api\Incomes\Requests;

use Infrastructure\Http\ApiRequest;

class CreateIncomeRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'income' => 'array|required',
            'income.name' => 'required|string',
            'income.status' => 'required|integer',
            'income.tax_rate' => 'between:0,100.000',
            'income.tax_rate_type_id' => 'integer',
            'income.lower_limit' => 'between:0,9999999999999.00',
            'income.upper_limit' => 'between:0,9999999999999.00',
            'income.tax_provision' => 'string',
        ];
    }

    
    public function attributes()
    {
        return [
            'income.name' => 'the income\'s name'
        ];
    }
}
