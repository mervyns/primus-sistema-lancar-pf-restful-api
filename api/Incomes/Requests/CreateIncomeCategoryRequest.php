<?php

namespace Api\Incomes\Requests;

use Infrastructure\Http\ApiRequest;

class CreateIncomeCategoryRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'income_category' => 'array|required',
            'income_category.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'income_category.name' => 'the income category\'s name'
        ];
    }
}
