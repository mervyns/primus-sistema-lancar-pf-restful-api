<?php

namespace Api\TaxRates\Repositories;

use Api\TaxRates\Models\TaxRate;
use Infrastructure\Database\Eloquent\Repository;

class TaxRateRepository extends Repository
{
    public function getModel()
    {
        return new TaxRate();
    }

    public function create(array $data)
    {
        $taxRate = $this->getModel();

        $taxRate->fill($data);
        
        $taxRate->save();

        return $taxRate;
    }

    public function update(TaxRate $taxRate, array $data)
    {

        $taxRate->fill($data);

        $taxRate->save();

        return $taxRate;
    }
}
