<?php

namespace Api\TaxRates\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\TaxRates\Requests\CreateTaxRateRequest;
use Api\TaxRates\Services\TaxRateService;

class TaxRateController extends Controller
{
    private $taxRateService;

    public function __construct(TaxRateService $taxRateService)
    {
        $this->taxRateService = $taxRateService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxRateService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_rates');

        return $this->response($parsedData);
    }

    public function getById($taxRateId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxRateService->getById($taxRateId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_rate');

        return $this->response($parsedData);
    }

    public function create(CreateTaxRateRequest $request)
    {
        $data = $request->get('tax_rate', []);

        return $this->response($this->taxRateService->create($data), 201);
    }

    public function update($taxRateId, Request $request)
    {
        $data = $request->get('tax_rate', []);

        return $this->response($this->taxRateService->update($taxRateId, $data));
    }

    public function delete($taxRateId)
    {
        return $this->response($this->taxRateService->delete($taxRateId));
    }
}
