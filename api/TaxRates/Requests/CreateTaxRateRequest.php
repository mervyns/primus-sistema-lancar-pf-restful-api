<?php

namespace Api\TaxRates\Requests;

use Infrastructure\Http\ApiRequest;

class CreateTaxRateRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tax_rate' => 'array|required',
            'tax_rate.income_id' => 'required|integer',
            'tax_rate.tax_rate_type_id' => 'required|integer',
            'tax_rate.lower_limit' => 'between:0,9999999999.99',
            'tax_rate.upper_limit' => 'between:0,9999999999.99',
            'tax_rate.rate' => 'required|between:0,100.000',
            'tax_rate.provision' => 'string',
        ];
    }

    
    public function attributes()
    {
        return [
            'tax_rate.provision' => 'the tax rate\'s provision'
        ];
    }
}
