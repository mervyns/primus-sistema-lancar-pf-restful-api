<?php

namespace Api\TaxRates\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaxRateNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The tax rate was not found.');
    }
}
