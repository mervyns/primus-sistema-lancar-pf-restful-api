<?php

namespace Api\TaxRates\Events;

use Infrastructure\Events\Event;
use Api\TaxRates\Models\TaxRate;

class TaxRateWasUpdated extends Event
{
    public $taxRate;

    public function __construct(TaxRate $taxRate)
    {
        $this->taxRate = $taxRate;
    }
}
