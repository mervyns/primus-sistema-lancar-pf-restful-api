<?php

namespace Api\TaxRates\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\TaxRates\Exceptions\TaxRateNotFoundException;
use Api\TaxRates\Events\TaxRateWasCreated;
use Api\TaxRates\Events\TaxRateWasDeleted;
use Api\TaxRates\Events\TaxRateWasUpdated;
use Api\TaxRates\Repositories\TaxRateRepository;
use Api\Incomes\Repositories\IncomeRepository;
use Api\TaxRateTypes\Repositories\UserTaxFillingStepRepository;

class TaxRateService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $taxRateRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        TaxRateRepository $taxRateRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->taxRateRepository = $taxRateRepository;
    }

    public function getAll($options = [])
    {
        $incomeRepository = new IncomeRepository();
        $taxRateTypeRepository = new UserTaxFillingStepRepository();

        $taxRate = $this->taxRateRepository->get($options);

        $i = 0;
        foreach ($taxRate as $tr) { 
            $income = $incomeRepository->getById($tr['income_id']);
            $taxRateType = $taxRateTypeRepository->getById($tr['tax_rate_type_id']);

            $taxRate[$i]['income_name'] = $income['name'];
            $taxRate[$i]['tax_rate_type_name'] = $taxRateType['name'];

            $i++;
        }

        return $taxRate;
    }

    public function getById($taxRateId, array $options = [])
    {
        $incomeRepository = new IncomeRepository();
        $taxRateTypeRepository = new UserTaxFillingStepRepository();

        $taxRate = $this->getRequestedTaxRate($taxRateId);

        $income = $incomeRepository->getById($taxRate['income_id']);
        $taxRateType = $taxRateTypeRepository->getById($taxRate['tax_rate_type_id']);

        $taxRate['income_name'] = $income['name'];
        $taxRate['tax_rate_type_name'] = $taxRateType['name'];

        return $taxRate;
    }

    public function create($data)
    {
        $taxRate = $this->taxRateRepository->create($data);

        $this->dispatcher->fire(new TaxRateWasCreated($taxRate));

        return $taxRate;
    }

    public function update($taxRateId, array $data)
    {
        $taxRate = $this->getRequestedTaxRate($taxRateId);

        $this->taxRateRepository->update($taxRate, $data);

        $this->dispatcher->fire(new TaxRateWasUpdated($taxRate));

        return $taxRate;
    }

    public function delete($taxRateId)
    {
        $taxRate = $this->getRequestedTaxRate($taxRateId);

        $this->taxRateRepository->delete($taxRateId);

        $this->dispatcher->fire(new TaxRateWasDeleted($taxRate));

        return true;
    }

    private function getRequestedTaxRate($taxRateId)
    {
        $taxRate = $this->taxRateRepository->getById($taxRateId);

        if (is_null($taxRate)) {
            throw new TaxRateNotFoundException();
        }

        return $taxRate;
    }
}
