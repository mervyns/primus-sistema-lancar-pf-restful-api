<?php

namespace Api\TaxRates\Models;

use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'income_id', 'tax_rate_type_id', 'lower_limit', 'upper_limit', 'rate', 'provision'
    ];
}
