<?php

namespace Api\TaxRates;

use Infrastructure\Events\EventServiceProvider;
use Api\TaxRates\Events\TaxRateWasCreated;
use Api\TaxRates\Events\TaxRateWasDeleted;
use Api\TaxRates\Events\TaxRateWasUpdated;

class TaxRateServiceProvider extends EventServiceProvider
{
    protected $listen = [
        TaxRateWasCreated::class => [
            // listeners for when a tax rate is created
        ],
        TaxRateWasDeleted::class => [
            // listeners for when a tax rate is deleted
        ],
        TaxRateWasUpdated::class => [
            // listeners for when a tax rate is updated
        ],
    ];
}
