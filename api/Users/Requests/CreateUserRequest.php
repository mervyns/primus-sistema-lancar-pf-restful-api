<?php

namespace Api\Users\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user' => 'array|required',
            'user.email' => 'required|email|unique:users,email',
            'user.name' => 'required|string',
            'user.password' => 'required',
            'user.gender' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'user.email' => 'email',
            'user.name' => 'fullname',
            'user.password' => 'password',
            'user.gender' => 'gender',
        ];
    }
}
