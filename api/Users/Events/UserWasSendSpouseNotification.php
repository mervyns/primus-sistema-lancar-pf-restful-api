<?php

namespace Api\Users\Events;
use Infrastructure\Events\Event;
class UserWasSendSpouseNotification extends Event
{
    public $parameter;
    public function __construct(array $parameter)
    {
        $this->parameter = $parameter;
    }
}
