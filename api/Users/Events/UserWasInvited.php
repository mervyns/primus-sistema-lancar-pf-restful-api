<?php

namespace Api\Users\Events;
use Infrastructure\Events\Event;
class UserWasInvited extends Event
{
    public $sender;
    public $receiver;
    public function __construct(array $sender, array $receiver)
    {
        $this->sender = $sender;
        $this->receiver = $receiver;
    }
}
