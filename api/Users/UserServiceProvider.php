<?php

namespace Api\Users;

use Infrastructure\Events\EventServiceProvider;
use Api\Users\Events\UserWasCreated;
use Api\Users\Events\UserWasDeleted;
use Api\Users\Events\UserWasUpdated;
use Api\Users\Events\UserWasActivated;
use Api\Users\Events\UserWasAssignedToRoles;
use Api\Users\Events\UserWasInvited;
use Api\UserOrders\Events\UserOrderWasAccepted;
use Api\UserOrders\Events\UserWasOrder;

use Api\Users\Listeners\SendRegistrationMail;
use Api\Users\Listeners\SendUpdatedMail;
use Api\Users\Listeners\SendDeletedMail;
use Api\Users\Listeners\SendActivatedMail;
use Api\users\Listeners\SendInvitedMail;
use Api\UserOrders\Listeners\SendAcceptedEmail;
use Api\UserOrders\Listeners\SendOrderEmail;

class UserServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserWasCreated::class => [
            // listeners for when a user is created
            SendRegistrationMail::class,
        ],
        UserWasDeleted::class => [
            // listeners for when a user is deleted
            SendDeletedMail::class,
        ],
        UserWasUpdated::class => [
            // listeners for when a user is updated
            SendUpdatedMail::class,
        ],
        UserWasActivated::class => [
            // listeners for when a user is activated
            SendActivatedMail::class,
        ],
        UserWasAssignedToRoles::class => [
            // listeners for when a user is assign to roles
        ],
        UserWasInvited::class => [
            SendInvitedMail::class,
        ],
        UserOrderWasAccepted::class => [
            SendAcceptedEmail::class,
        ],
        UserWasOrder::class => [
            SendOrderEmail::class,
        ],

    ];
}
