<?php

$router->get('/api/users', 'UserController@getAll');
$router->get('/api/users/{id}', 'UserController@getById');
$router->put('/api/users/{id}', 'UserController@update');
$router->put('/api/users/{id}/assign-roles', 'UserController@assignToRoles');
$router->delete('/api/users/{id}', 'UserController@delete');

/**
 * End of file
 */