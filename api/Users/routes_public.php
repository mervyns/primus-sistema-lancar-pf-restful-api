<?php

$router->post('/api/users', 'UserController@create');
$router->get('/api/users/activate/{id}', 'UserController@activate');


/**
 * End of file
 */
