<?php

namespace Api\Users\Listeners;

use Log;
use Api\Users\Events\UserWasUpdated;

class SendUpdatedMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasUpdated $userWasUpdated)
    {
        $user = $userWasUpdated->user;
        Log::info("User with ID {$user->id} was updated!");


    }
}
