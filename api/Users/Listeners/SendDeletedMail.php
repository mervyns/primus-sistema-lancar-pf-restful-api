<?php

namespace Api\Users\Listeners;

use Log;
use Api\Users\Events\UserWasDeleted;

class SendDeletedMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasDeleted $userWasDeleted)
    {
        $user = $userWasDeleted->user;
        Log::info("User with ID {$user->id} was deleted!");


    }
}
