<?php

namespace Api\Users\Listeners;

use Log;
use Mail;
use Api\Users\Events\UserWasActivated;

class SendActivatedMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasActivated $userWasActivated)
    {
        $user = $userWasActivated->user;
        Log::info("User with ID {$user->id} was activated!");

        Mail::send('emails.user_activation_successful_info', [
            'email' => $user->email,
            'name' => $user->name,
            'id' => $user->id
        ], function($message) use ($user) {
            $message->from('no-reply@sptgo.com');
            $message->to($user->email);
            $message->subject("Sukses Melakukan Aktivasi Akun di SPTGO.com");
        });
    }
}
