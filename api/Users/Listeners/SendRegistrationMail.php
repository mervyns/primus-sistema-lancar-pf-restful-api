<?php

namespace Api\Users\Listeners;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

use Api\Users\Events\UserWasCreated;

class SendRegistrationMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasCreated $userWasCreated)
    {
        $user = $userWasCreated->user;
        Log::info("User with email {$user->email} was created!");

        Mail::send('emails.user_registration_info', [
            'email' => $user->email,
            'id' => Crypt::encrypt($user->id),
            'name' => $user->name
        ], function($message) use ($user) {
            $message->from('no-reply@sptgo.com');
            $message->to($user->email);
            $message->subject("Selamat Anda telah terdaftar di SPTGO.com");
        });
    }
}
