<?php

namespace Api\Users\Listeners;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

use Api\Users\Events\UserWasSendSpouseNotification;

class SendSpouseNotificationMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasSendSpouseNotification $userWasSendSpouseNotification){
        
        $parameter = [
            'user_sender_product_id' => $userWasSendSpouseNotification['parent_id'],
            'user_receiver_product_id' => $userWasSendSpouseNotification['user_product_id'],
            'user_sender_email' => $userWasSendSpouseNotification['email_sender'],
            'user_receiver_email' => $userWasSendSpouseNotification['email_receiver']

        ];

        Mail::send('emails.spouse_notification', [
            'parameter' => $parameter
        ], function ($message) use ($userWasSendSpouseNotification) {
            $message->from('no-reply@sptgo.com');
            $message->to('');
            $message->subject('UDANGAN MENGISI SPTGO');
        });

        // $sender =  $userWasInvited->sender;
        // $receiver = $userWasInvited->receiver;
        // $emailReceiver = $receiver['email'];
        // $emailSender = $sender['email'];
        // $idReceiver = $receiver['id'];
        // $idSender = $sender['id'];

        // $title = '';
        // $subtitle = '';

        // Log::info("User with email {{ @emailReceiver }} was created!");

        // Mail::send('emails.user_product_invited', [
        //     'emailsender' => Crypt::encrypt($sender['email']),
        //     'emailinvited' => Crypt::encrypt($receiver['email']),
        //     'idsender' => Crypt::encrypt($idSender),
        //     'idreceiver' => Crypt::encrypt($idReceiver),
        //     'subtitle' => Crypt::encrypt($subtitle)
        // ], function($message) use ($receiver) {
        //     $message->from('no-reply@sptgo.com');
        //     $message->to($receiver['email']);
        //     $message->subject($title);
        // });
    }
}
