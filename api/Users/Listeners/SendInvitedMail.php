<?php

namespace Api\Users\Listeners;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;

use Api\Users\Events\UserWasInvited;

class SendInvitedMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasInvited $userWasInvited){
        $sender =  $userWasInvited->sender;
        $receiver = $userWasInvited->receiver;
        $emailReceiver = $receiver['email'];
        $emailSender = $sender['email'];
        $idReceiver = $receiver['id'];
        $idSender = $sender['id'];

        Log::info("User with email {{ @emailReceiver }} was created!");

        Mail::send('emails.user_product_invited', [
            'emailsender' => Crypt::encrypt($sender['email']),
            'emailinvited' => Crypt::encrypt($receiver['email']),
            'idsender' => Crypt::encrypt($idSender),
            'idreceiver' => Crypt::encrypt($idReceiver)
        ], function($message) use ($receiver) {
            $message->from('no-reply@sptgo.com');
            $message->to($receiver['email']);
            $message->subject("PERMINTAAN BERGABUNG DI SPTGO");
        });
    }
}
