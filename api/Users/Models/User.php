<?php

namespace Api\Users\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class UserAuthenticatable extends Model implements
	AuthenticatableContract,
	AuthorizableContract,
	CanResetPasswordContract
{
	use Authenticatable, Authorizable, CanResetPassword;
}

class User extends UserAuthenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','gender','birthdate', 'homenumber', 'mobilenumber', 'faxnumber', 'isActive',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('Api\Roles\Models\Role', 'users_roles');
    }

    public function step()
    {
	    return $this->hasOne( 'Api\TaxFilling\Models\UserTaxFillingStep' );
    }

    public function taxProfile()
    {
	    return $this->hasOne('Api\UserTaxProfiles\Models\UserTaxProfile');
    }

    public function additionalProfile()
    {
        return $this->hasOne('Api\UserAdditionalProfiles\Models\UserAdditionalProfile');
    }

    public function income()
    {
        return $this->hasMany('Api\UserIncomes\Models\UserIncome');
    }

    public function securityQuestion()
    {
        return $this->hasMany('Api\UserSecurityQuestions\Models\UserSecurityQuestion');
    }

    public function revealedAsset()
    {
        return $this->hasMany('Api\UserRevealedAssets\Models\UserRevealedAsset');
    }

    public function repatriatedAsset()
    {
        return $this->hasMany('Api\UserRepatriatedAssets\Models\UserRepatriatedAsset');
    }

    public function asset()
    {
        return $this->hasMany('Api\UserAssets\Models\UserAsset');
    }

    public function foreignAsset()
    {
        return $this->hasMany('Api\UserForeignAssets\Models\UserForeignAsset');
    }

    public function liability()
    {
        return $this->hasMany('Api\UserLiabilities\Models\UserLiability');
    }

    public function dependant()
    {
        return $this->hasMany('Api\UserDependants\Models\UserDependant');
    }

    public function companion()
    {
        return $this->hasOne('Api\UserCompanions\Models\UserCompanion');
    }

    public function spouseTaxProfile()
    {
        return $this->hasOne('Api\SpouseTaxProfiles\Models\SpouseTaxProfile');
    }

    public function userProduct(){
        return $this->hasMany('Api\UserProducts\Models\UserProduct');
    }

    public function userInvites(){
        return $this->hasMany('Api\UserInvites\Models\UserInvite');
    }
}
