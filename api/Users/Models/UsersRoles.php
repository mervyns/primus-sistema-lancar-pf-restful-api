<?php

namespace Api\Users\Models;

use Illuminate\Database\Eloquent\Model;

class UsersGroup extends Model
{
    protected $table = 'users_roles';
}
