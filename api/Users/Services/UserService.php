<?php

namespace Api\Users\Services;

use Api\Users\Events\UserWasAssignedToRoles;
use Log;
use DB;
use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Users\Exceptions\UserNotFoundException;
use Api\Users\Events\UserWasCreated;
use Api\Users\Events\UserWasDeleted;
use Api\Users\Events\UserWasUpdated;
use Api\Users\Events\UserWasActivated;
use Api\Users\Repositories\UserRepository;
use Api\UserSecurityQuestions\Repositories\UserSecurityQuestionRepository;

class UserService
{
    private $auth;

    private $database;

    private $dispatcher;

    private $userRepository;

    public function __construct(
        AuthManager $auth,
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserRepository $userRepository
    ) {
        $this->auth = $auth;
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userRepository = $userRepository;
    }

    public function getAll($options = [])
    {
        $usersData = [];
        $users = $this->userRepository->get($options);
        foreach ($users as $user) {
            $data = $user;
            $data->roles = $user->roles;
            $usersData[] = $data;
        }
        return $usersData;
    }

    public function getById($userId, array $options = [])
    {
        $user = $this->getRequestedUser($userId);
        $userSecurityQuestion = DB::table('user_security_questions')->where('user_id', '=', $userId)->get();

        $i = 1;
        foreach ($userSecurityQuestion as $uSQ) {

            $user['security_question_id'.$i] = $uSQ->security_question_id;
            $user['security_question_answer'.$i] = $uSQ->answer;

            $i++;
        }

        return $user;
    }

    public function checkUserExits(string $email) : bool{
        $result = false;
        $user = $this->userRepository->getModel()->where('email', $email)->first();
        
        if ($user)
        {
            $result = true;
        }
        return $result;
    }

    public function create($data)
    {
        $user = $this->userRepository->create($data);

        $this->dispatcher->fire(new UserWasCreated($user));

        return $user;
    }

    public function update($userId, array $data)
    {
        $user = $this->getRequestedUser($userId);

        $this->userRepository->update($user, $data);

        $this->dispatcher->fire(new UserWasUpdated($user));

        return $user;
    }

    public function delete($userId)
    {
        $user = $this->getRequestedUser($userId);

        $this->userRepository->delete($userId);

        $this->dispatcher->fire(new UserWasDeleted($user));

        return true;
    }

    public function activate($userId)
    {
        $user = $this->getRequestedUser($userId);

        $this->userRepository->activate($user, ['isActive' => 1, 'password' => $user->password]);

        $this->dispatcher->fire(new UserWasActivated($user));

        return $user;
    }

    public function updateProfile($userId, array $data)
    {
        $user = $this->getRequestedUser($userId);

        $this->userRepository->updateProfile($user, $data);

        $this->dispatcher->fire(new UserWasUpdated($user));

        return $user;
    }


    public function assignToRoles($userId, $roles = [])
    {
        $user = $this->getRequestedUser($userId);

        $this->dispatcher->fire(new UserWasAssignedToRoles($user));

        return $this->userRepository->assignRolesToUser($user, $roles);
    }

    private function getRequestedUser($userId)
    {
        $user = $this->userRepository->getById($userId);

        if (is_null($user)) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    public function createUserSecurityQuestion(array $data)
    {  
        $userSecurityQuestionRepository = new UserSecurityQuestionRepository();

        $userSecurityQuestionRepository->create($data);

        return $userSecurityQuestionRepository;
    }

    public function removeUserSecurityQuestion($userId){

        DB::table('user_security_questions')->where('user_id', '=', $userId)->delete();

        return true;
    }
}
