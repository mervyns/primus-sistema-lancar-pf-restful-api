<?php

namespace Api\UserCompanions\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserCompanionRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_companion' => 'array|required',
            'user_companion.user_id' => 'required|integer',
            'user_companion.companion_id' => 'required|integer',
            'user_companion.requested_by' => 'required|integer'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_companion.user_id' => 'the user companion user\'s id'
        ];
    }
}
