<?php

namespace Api\UserCompanions\Models;

use Illuminate\Database\Eloquent\Model;

class UserCompanion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'companion_id', 'requested_by'
    ];

    public $timestamps = false;
}
