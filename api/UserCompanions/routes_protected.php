<?php

$router->get('/api/user-companions', 'UserCompanionController@getAll');
$router->get('/api/user-companions/{id}', 'UserCompanionController@getById');
$router->post('/api/user-companions', 'UserCompanionController@create');
$router->put('/api/user-companions/{id}', 'UserCompanionController@update');
$router->delete('/api/user-companions/{id}', 'UserCompanionController@delete');