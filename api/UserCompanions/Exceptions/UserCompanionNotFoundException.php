<?php

namespace Api\UserCompanions\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserCompanionNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user companion was not found.');
    }
}
