<?php

namespace Api\UserCompanions\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserCompanions\Exceptions\UserCompanionNotFoundException;
use Api\UserCompanions\Events\UserCompanionWasCreated;
use Api\UserCompanions\Events\UserCompanionWasDeleted;
use Api\UserCompanions\Events\UserCompanionWasUpdated;
use Api\UserCompanions\Repositories\UserCompanionRepository;
use Api\Users\Repositories\UserRepository;

class UserCompanionService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userCompanionRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserCompanionRepository $userCompanionRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userCompanionRepository = $userCompanionRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();

        $userCompanion = $this->userCompanionRepository->get($options);

        $i=0;
        foreach ($userCompanion as $uC) {
            $user = $userRepository->getById($uC['user_id']);
            $companion = $userRepository->getById($uC['companion_id']);
            $requestedUser = $userRepository->getById($uC['requested_by']);

            $userCompanion[$i]['user_name'] = $user['name'];
            $userCompanion[$i]['companion_name'] = $companion['name'];
            $userCompanion[$i]['requested_by_name'] = $requestedUser['name'];
            $i++;
        }

        return $userCompanion;
    }

    public function getById($userCompanionId, array $options = [])
    {
        $userRepository = new UserRepository();

        $userCompanion = $this->getRequestedUserCompanion($userCompanionId);

        $user = $userRepository->getById($userCompanion['user_id']);
        $companion = $userRepository->getById($userCompanion['companion_id']);
        $requestedUser = $userRepository->getById($userCompanion['requested_by']);

        $userCompanion['user_name'] = $user['name'];
        $userCompanion['companion_name'] = $companion['name'];
        $userCompanion['requested_by_name'] = $requestedUser['name'];

        return $userCompanion;
    }

    public function create($data)
    {
        $userCompanion = $this->userCompanionRepository->create($data);

        $this->dispatcher->fire(new UserCompanionWasCreated($userCompanion));

        return $userCompanion;
    }

    public function update($userCompanionId, array $data)
    {
        $userCompanion = $this->getRequestedUserCompanion($userCompanionId);

        $this->userCompanionRepository->update($userCompanion, $data);

        $this->dispatcher->fire(new UserCompanionWasUpdated($userCompanion));

        return $userCompanion;
    }

    public function delete($userCompanionId)
    {
        $userCompanion = $this->getRequestedUserCompanion($userCompanionId);

        $this->userCompanionRepository->delete($userCompanionId);

        $this->dispatcher->fire(new UserCompanionWasDeleted($userCompanion));

        return true;
    }

    private function getRequestedUserCompanion($userCompanionId)
    {
        $userCompanion = $this->userCompanionRepository->getById($userCompanionId);

        if (is_null($userCompanion)) {
            throw new UserCompanionNotFoundException();
        }

        return $userCompanion;
    }
}
