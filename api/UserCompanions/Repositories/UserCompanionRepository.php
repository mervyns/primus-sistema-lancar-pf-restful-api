<?php

namespace Api\UserCompanions\Repositories;

use Api\UserCompanions\Models\UserCompanion;
use Infrastructure\Database\Eloquent\Repository;

class UserCompanionRepository extends Repository
{
    public function getModel()
    {
        return new UserCompanion();
    }

    public function create(array $data)
    {
        $userCompanion = $this->getModel();

        $userCompanion->fill($data);
        
        $userCompanion->save();

        return $userCompanion;
    }

    public function update(UserCompanion $userCompanion, array $data)
    {

        $userCompanion->fill($data);

        $userCompanion->save();

        return $userCompanion;
    }
}
