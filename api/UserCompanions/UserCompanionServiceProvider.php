<?php

namespace Api\UserCompanions;

use Infrastructure\Events\EventServiceProvider;
use Api\UserCompanions\Events\UserCompanionWasCreated;
use Api\UserCompanions\Events\UserCompanionWasDeleted;
use Api\UserCompanions\Events\UserCompanionWasUpdated;

class UserCompanionServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserCompanionWasCreated::class => [
            // listeners for when a user companion is created
        ],
        UserCompanionWasDeleted::class => [
            // listeners for when a user companion is deleted
        ],
        UserCompanionWasUpdated::class => [
            // listeners for when a user companion is updated
        ],
    ];
}
