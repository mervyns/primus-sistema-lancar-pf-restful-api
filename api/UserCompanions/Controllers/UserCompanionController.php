<?php

namespace Api\UserCompanions\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserCompanions\Requests\CreateUserCompanionRequest;
use Api\UserCompanions\Services\UserCompanionService;

class UserCompanionController extends Controller
{
    private $userCompanionService;

    public function __construct(UserCompanionService $userCompanionService)
    {
        $this->userCompanionService = $userCompanionService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userCompanionService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_companions');

        return $this->response($parsedData);
    }

    public function getById($userCompanionId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userCompanionService->getById($userCompanionId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_companion');

        return $this->response($parsedData);
    }

    public function create(CreateUserCompanionRequest $request)
    {
        $data = $request->get('user_companion', []);

        return $this->response($this->userCompanionService->create($data), 201);
    }

    public function update($userCompanionId, Request $request)
    {
        $data = $request->get('user_companion', []);

        return $this->response($this->userCompanionService->update($userCompanionId, $data));
    }

    public function delete($userCompanionId)
    {
        return $this->response($this->userCompanionService->delete($userCompanionId));
    }
}
