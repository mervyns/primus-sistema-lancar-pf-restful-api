<?php

namespace Api\UserCompanions\Events;

use Infrastructure\Events\Event;
use Api\UserCompanions\Models\UserCompanion;

class UserCompanionWasCreated extends Event
{
    public $userCompanion;

    public function __construct(UserCompanion $userCompanion)
    {
        $this->userCompanion = $userCompanion;
    }
}
