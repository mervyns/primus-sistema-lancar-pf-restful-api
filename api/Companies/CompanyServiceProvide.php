<?php

namespace Api\Companies;

use Infrastructure\Events\EventServiceProvider;
use Api\Charities\Events\CompanyWasCreated;
use Api\Charities\Events\CompanyWasDeleted;
use Api\Charities\Events\CompanyWasUpdated;

class CompanyServiceProvide extends EventServiceProvider
{
    protected $listen = [
        CompanyWasCreated::class => [
            // listeners for when a company is created
        ],
        CompanyWasDeleted::class => [
            // listeners for when a company is deleted
        ],
        CompanyWasUpdated::class => [
            // listeners for when a company is updated
        ],
    ];
}
