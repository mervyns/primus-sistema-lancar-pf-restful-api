<?php

namespace Api\Companies\Requests;

use Infrastructure\Http\ApiRequest;

class CreateCompanyRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company' => 'array|required',
            'company.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'company.name' => 'the company\'s name'
        ];
    }
}
