<?php

namespace Api\Companies\Services;

use Exception;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Companies\Exceptions\CompanyNotFoundException;
use Api\Companies\Events\CompanyWasCreated;
use Api\Companies\Events\CompanyWasDeleted;
use Api\Companies\Events\CompanyWasUpdated;
use Api\Companies\Repositories\CompanyRepository;

class CompanyService
{
    private $database;
    private $dispatcher;
    private $companyRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        CompanyRepository $companyRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->companyRepository = $companyRepository;
    }

    public function getAll($options = []) {
        return $this->companyRepository->get($options);
    }

    public function getSekuritas($term, $options = []) {
        return $this->companyRepository->getModel()
            ->where('type', 'sekuritas')
            ->where('name', 'like', '%'.$term.'%')
            ->limit(15)->get();
    }

    public function getReksadana($term, $options = []) {
        return $this->companyRepository->getModel()
            ->where('type', 'reksadana')
            ->where('name', 'like', '%'.$term.'%')
            ->limit(15)->get();
    }

    public function getById($companyId, array $option = []) {
        $company = $this->getRequestedCompany($companyId);
        return $company;
    }

    public function create($data) {
        $company = $this->companyRepositoy->create($data);
        $this->dispatcher->fire(new CompanyWasCreated($company));
        return $company;
    }

    public function update ($companyId, array $data) {
        $company = $this->getRequestedCompany($companyId);
        $this->companyRepositoy->update($company, $data);
        $this->dispatcher->fire(new CompanyWasUpdated($company));
        return  $company;
    }

    public function delete($companyId) {
        $company = $this->getRequestedCompany($companyId);
        $this->companyRepositoy->delete($companyId);
        $this->dispatcher->fire(new CompanyWasDeleted($company));
        return true;
    }

    private function getRequestedCompany($companyId) {
        $company = $this->companyRepositoy->getById($companyId);
        if (is_null($company)) {
            throw new CompanyNotFoundException();
        }
        return $company;
    }
}
