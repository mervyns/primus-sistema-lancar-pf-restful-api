<?php
$router->get('/api/companies', 'CompanyController@getAll');
$router->get('/api/companies/reksadana', 'CompanyController@getReksadana');
$router->get('/api/companies/sekuritas', 'CompanyController@getSekuritas');