<?php

namespace Api\Companies\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Companies\Requests\CreateCompanyRequest;
use Api\Companies\Services\CompanyService;
use Api\Companies\Repositories\CompanyRepository;

class CompanyController extends Controller
{
    private $companyService;

    public function __construct(CompanyService $companyService) {
        $this->companyService = $companyService;
    }

    public function getAll() {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->companyService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'companies');

        return $this->response($parsedData);
    }

    public function getSekuritas(Request $request) {
        $term = $request->get('term', '');
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->companyService->getSekuritas($term, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'companies');

        return $this->response($parsedData);
    }

    public function getReksadana(Request $request) {
        $term = $request->get('term', '');
        $resourceOptions = $this->parseResourceOptions();
        
        $data = $this->companyService->getReksadana($term, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'companies');

        return $this->response($parsedData);
    }
}
