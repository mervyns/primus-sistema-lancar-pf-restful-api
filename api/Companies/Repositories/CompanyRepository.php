<?php

namespace Api\Companies\Repositories;

use Api\Companies\Models\Company;
use Infrastructure\Database\Eloquent\Repository;

class CompanyRepository extends Repository
{

    public function getModel() {
        return new Company();
    }

    public function create(array $data) {
        $company = $this->getModel();
        $company->fill($data);
        $company->save();
        return $company;
    }

    public function update(Company $company, array $data) {
        $company->fill($data);
        $company->save();
        return $company;
    }
}
