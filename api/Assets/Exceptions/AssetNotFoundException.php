<?php

namespace Api\Assets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssetNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The asset was not found.');
    }
}
