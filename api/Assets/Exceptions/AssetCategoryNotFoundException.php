<?php

namespace Api\Assets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssetCategoryNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The asset category was not found.');
    }
}
