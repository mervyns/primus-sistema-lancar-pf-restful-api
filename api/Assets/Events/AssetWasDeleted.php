<?php

namespace Api\Assets\Events;

use Infrastructure\Events\Event;
use Api\Assets\Models\Asset;

class AssetWasDeleted extends Event
{
    public $asset;

    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }
}
