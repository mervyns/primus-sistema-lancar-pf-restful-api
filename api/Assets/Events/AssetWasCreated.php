<?php

namespace Api\Assets\Events;

use Infrastructure\Events\Event;
use Api\Assets\Models\Asset;

class AssetWasCreated extends Event
{
    public $asset;

    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }
}
