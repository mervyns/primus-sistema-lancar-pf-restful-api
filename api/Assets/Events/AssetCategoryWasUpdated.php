<?php

namespace Api\Assets\Events;

use Infrastructure\Events\Event;
use Api\Assets\Models\AssetCategory;

class AssetCategoryWasUpdated extends Event
{
    public $assetCategory;

    public function __construct(AssetCategory $assetCategory)
    {
    	$this->assetCategory = $assetCategory;  	
    }
}
