<?php

namespace Api\Assets\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Assets\Exceptions\AssetCategoryNotFoundException;
use Api\Assets\Events\AssetCategoryWasCreated;
use Api\Assets\Events\AssetCategoryWasDeleted;
use Api\Assets\Events\AssetCategoryWasUpdated;
use Api\Assets\Repositories\AssetCategoryRepository;

class AssetCategoryService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $assetCategoryRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        AssetCategoryRepository $assetCategoryRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->assetCategoryRepository = $assetCategoryRepository;
    }

    public function getAll($options = [])
    {
        return $this->assetCategoryRepository->get($options);
    }

    public function getById($assetCategoryId, array $options = [])
    {
        $assetCategory = $this->getRequestedAssetCategory($assetCategoryId);

        return $assetCategory;
    }

    public function create($data)
    {
        $assetCategory = $this->assetCategoryRepository->create($data);

        $this->dispatcher->fire(new AssetCategoryWasCreated($assetCategory));

        return $assetCategory;
    }

    public function update($assetCategoryId, array $data)
    {
        $assetCategory = $this->getRequestedAssetCategory($assetCategoryId);

        $this->assetCategoryRepository->update($assetCategory, $data);

        $this->dispatcher->fire(new AssetCategoryWasUpdated($assetCategory));

        return $assetCategory;
    }

    public function delete($assetCategoryId)
    {
        $assetCategory = $this->getRequestedAssetCategory($assetCategoryId);

        $this->assetCategoryRepository->delete($assetCategoryId);

        $this->dispatcher->fire(new AssetCategoryWasDeleted($assetCategory));

        return true;
    }

    private function getRequestedAssetCategory($assetCategoryId)
    {
        $assetCategory = $this->assetCategoryRepository->getById($assetCategoryId);

        if (is_null($assetCategory)) {
            throw new AssetCategoryNotFoundException();
        }

        return $assetCategory;
    }
}
