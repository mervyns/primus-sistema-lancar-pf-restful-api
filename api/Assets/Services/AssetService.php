<?php

namespace Api\Assets\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Assets\Exceptions\AssetNotFoundException;
use Api\Assets\Events\AssetWasCreated;
use Api\Assets\Events\AssetWasDeleted;
use Api\Assets\Events\AssetWasUpdated;
use Api\Assets\Events\AssetWasAssignedToCategory;
use Api\Assets\Repositories\AssetRepository;
use Api\Assets\Repositories\AssetCategoryRepository;

class AssetService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $assetRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        AssetRepository $assetRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->assetRepository = $assetRepository;
    }

    public function getAll($options = [])
    {
        $assetCategoryRepository = new AssetCategoryRepository();

        $asset = $this->assetRepository->get($options);

        $i = 0;
        foreach ($asset as $a) {
            $assetCategory = $assetCategoryRepository->getById($a['asset_category_id']);

            $asset[$i]['asset_category_name'] = $assetCategory['name'];

            $i++;
        }

        return $asset;
    }

    public function getById($assetId, array $options = [])
    {
        $assetCategoryRepository = new AssetCategoryRepository();

        $asset = $this->getRequestedAsset($assetId);

        $assetCategory = $assetCategoryRepository->getById($asset['asset_category_id']);

        $asset['asset_category_name'] = $assetCategory['name'];

        return $asset;
    }

    public function create($data)
    {
        $asset = $this->assetRepository->create($data);

        $this->dispatcher->fire(new AssetWasCreated($asset));

        return $asset;
    }

    public function update($assetId, array $data)
    {
        $asset = $this->getRequestedAsset($assetId);

        $this->assetRepository->update($asset, $data);

        $this->dispatcher->fire(new AssetWasUpdated($asset));

        return $asset;
    }

    public function delete($assetId)
    {
        $asset = $this->getRequestedAsset($assetId);

        $this->assetRepository->delete($assetId);

        $this->dispatcher->fire(new AssetWasDeleted($asset));

        return true;
    }

    private function getRequestedAsset($assetId)
    {
        $asset = $this->assetRepository->getById($assetId);

        if (is_null($asset)) {
            throw new AssetNotFoundException();
        }

        return $asset;
    }

    public function assignToCategory($assetId, $category)
    {
        $asset = $this->getRequestedAsset($assetId);

        $this->dispatcher->fire(new AssetWasAssignedToCategory($asset));

        return $this->assetRepository->assignCategoryToAsset($asset, $category);
    }
}
