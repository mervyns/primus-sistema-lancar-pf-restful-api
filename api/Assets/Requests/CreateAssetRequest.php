<?php

namespace Api\Assets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateAssetRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'asset' => 'array|required',
            'asset.name' => 'required|string',
            'asset.asset_code' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'asset.name' => 'the asset\'s name'
        ];
    }
}
