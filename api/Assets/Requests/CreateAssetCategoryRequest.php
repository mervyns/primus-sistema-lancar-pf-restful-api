<?php

namespace Api\Assets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateAssetCategoryRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'asset_category' => 'array|required',
            'asset_category.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'asset_category.name' => 'the asset category\'s name'
        ];
    }
}
