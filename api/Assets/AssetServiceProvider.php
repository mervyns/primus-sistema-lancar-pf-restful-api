<?php

namespace Api\Assets;

use Infrastructure\Events\EventServiceProvider;
use Api\Assets\Events\AssetWasCreated;
use Api\Assets\Events\AssetWasDeleted;
use Api\Assets\Events\AssetWasUpdated;
use Api\Assets\Events\AssetCategoryWasCreated;
use Api\Assets\Events\AssetCategoryWasDeleted;
use Api\Assets\Events\AssetCategoryWasUpdated;
use Api\Assets\Events\AssetWasAssignedToCategory;

class AssetCategoryServiceProvider extends EventServiceProvider
{
    protected $listen = [
        AssetWasCreated::class => [
            // listeners for when a asset is created
        ],
        AssetWasDeleted::class => [
            // listeners for when a asset is deleted
        ],
        AssetWasUpdated::class => [
            // listeners for when a asset is updated
        ],
        AssetCategoryWasCreated::class => [
            // listeners for when a asset category is created
        ],
        AssetCategoryWasDeleted::class => [
            // listeners for when a asset category is deleted
        ],
        AssetCategoryWasUpdated::class => [
            // listeners for when a asset category is updated
        ],
        AssetWasAssignedToCategory::class => [
            // listeners for when a asset category is updated
        ],
    ];
}
