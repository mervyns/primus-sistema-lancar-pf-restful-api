<?php

$router->get('/api/assets/categories', 'AssetCategoryController@getAll');
$router->get('/api/assets/categories/{id}', 'AssetCategoryController@getById');
$router->post('/api/assets/categories', 'AssetCategoryController@create');
$router->put('/api/assets/categories/{id}', 'AssetCategoryController@update');
$router->delete('/api/assets/categories/{id}', 'AssetCategoryController@delete');

$router->get('/api/assets', 'AssetController@getAll');
$router->get('/api/assets/{id}', 'AssetController@getById');
$router->post('/api/assets', 'AssetController@create');
$router->put('/api/assets/{id}', 'AssetController@update');
$router->delete('/api/assets/{id}', 'AssetController@delete');
$router->put('/api/assets/{id}/assign-category', 'AssetController@assignToCategory');
