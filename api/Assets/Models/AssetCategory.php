<?php

namespace Api\Assets\Models;

use Illuminate\Database\Eloquent\Model;

class AssetCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function assets()
    {
        return $this->hasMany('Api\Assets\Models\Asset');
    }
}
