<?php

namespace Api\Assets\Models;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'asset_code'
    ];

    public function category()
    {
        return $this->belongsTo('Api\Assets\Models\AssetCategory','asset_category_id');
    }
}
