<?php

namespace Api\Assets\Repositories;

use Api\Assets\Models\Asset;
use Api\Assets\Models\AssetCategory;
use Infrastructure\Database\Eloquent\Repository;

class AssetRepository extends Repository
{
    public function getModel()
    {
        return new Asset();
    }

    public function create(array $data)
    {
        $asset = $this->getModel();

        $asset->fill($data);
        
        $asset->save();

        return $asset;
    }

    public function update(Asset $asset, array $data)
    {

        $asset->fill($data);

        $asset->save();

        return $asset;
    }



    public function assignCategoryToAsset(Asset $asset, $category)
    {
        $assetCategoryModel = new AssetCategory();

        if($assetCategoryModel->where('id', '=', $category)->count() > 0){
            $asset->asset_category_id = $category;

            $asset->save();
        }

        return $asset;
    }
}
