<?php

namespace Api\Assets\Repositories;

use Api\Assets\Models\AssetCategory;
use Infrastructure\Database\Eloquent\Repository;

class AssetCategoryRepository extends Repository
{
    public function getModel()
    {
        return new AssetCategory();
    }

    public function create(array $data)
    {
        $assetCategory = $this->getModel();

        $assetCategory->fill($data);
        
        $assetCategory->save();

        return $assetCategory;
    }

    public function update(AssetCategory $assetCategory, array $data)
    {

        $assetCategory->fill($data);

        $assetCategory->save();

        return $assetCategory;
    }

    public function getByCategoryId($id, array $options = [])
    {   
        $query = $this->createBaseBuilder($options);
        return $query->find($id);
    }
}
