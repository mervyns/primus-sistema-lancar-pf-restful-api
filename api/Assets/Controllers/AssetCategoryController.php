<?php

namespace Api\Assets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Assets\Requests\CreateAssetCategoryRequest;
use Api\Assets\Services\AssetCategoryService;

class AssetCategoryController extends Controller
{
    private $assetCategoryService;

    public function __construct(AssetCategoryService $assetCategoryService)
    {
        $this->assetCategoryService = $assetCategoryService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->assetCategoryService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'asset_categories');

        return $this->response($parsedData);
    }

    public function getById($assetCategoryId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->assetCategoryService->getById($assetCategoryId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'asset_category');

        return $this->response($parsedData);
    }

    public function create(CreateAssetCategoryRequest $request)
    {
        $data = $request->get('asset_category', []);

        return $this->response($this->assetCategoryService->create($data), 201);
    }

    public function update($assetCategoryId, Request $request)
    {
        $data = $request->get('asset_category', []);

        return $this->response($this->assetCategoryService->update($assetCategoryId, $data));
    }

    public function delete($assetCategoryId)
    {
        return $this->response($this->assetCategoryService->delete($assetCategoryId));
    }
}
