<?php

namespace Api\Assets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Assets\Requests\CreateAssetRequest;
use Api\Assets\Services\AssetService;

class AssetController extends Controller
{
    private $assetService;

    public function __construct(AssetService $assetService)
    {
        $this->assetService = $assetService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->assetService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'assets');

        return $this->response($parsedData);
    }

    public function getById($assetId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->assetService->getById($assetId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'asset');

        return $this->response($parsedData);
    }

    public function create(CreateAssetRequest $request)
    {
        $data = $request->get('asset', []);

        return $this->response($this->assetService->create($data), 201);
    }

    public function update($assetId, Request $request)
    {
        $data = $request->get('asset', []);

        return $this->response($this->assetService->update($assetId, $data));
    }

    public function assignToCategory($assetId, Request $request)
    {
        $category = $request->get('category');

        return $this->response($this->assetService->assignToCategory($assetId, $category));
    }

    public function delete($assetId)
    {
        return $this->response($this->assetService->delete($assetId));
    }
}
