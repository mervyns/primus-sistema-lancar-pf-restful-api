<?php

namespace Api\Gateways\Models;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gateway_code', 'name'
        
    ];
}
