<?php

namespace Api\Gateways\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GatewayNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The gateway was not found.');
    }
}
