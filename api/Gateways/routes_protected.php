<?php

$router->get('/api/gateways', 'GatewayController@getAll');
$router->get('/api/gateways/{id}', 'GatewayController@getById');
$router->post('/api/gateways', 'GatewayController@create');
$router->put('/api/gateways/{id}', 'GatewayController@update');
$router->delete('/api/gateways/{id}', 'GatewayController@delete');