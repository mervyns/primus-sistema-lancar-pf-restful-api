<?php

namespace Api\Gateways\Requests;

use Infrastructure\Http\ApiRequest;

class CreateGatewayRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'gateway' => 'array|required',
            'gateway.gateway_code' => 'required|string',
            'gateway.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'gateway.name' => 'the gateway\'s name'
        ];
    }
}
