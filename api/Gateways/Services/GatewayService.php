<?php

namespace Api\Gateways\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Gateways\Exceptions\GatewayNotFoundException;
use Api\Gateways\Events\GatewayWasCreated;
use Api\Gateways\Events\GatewayWasDeleted;
use Api\Gateways\Events\GatewayWasUpdated;
use Api\Gateways\Repositories\GatewayRepository;

class GatewayService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $gatewayRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        GatewayRepository $gatewayRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->gatewayRepository = $gatewayRepository;
    }

    public function getAll($options = [])
    {
        return $this->gatewayRepository->get($options);
    }

    public function getById($gatewayId, array $options = [])
    {
        $gateway = $this->getRequestedGateway($gatewayId);

        return $gateway;
    }

    public function create($data)
    {
        $gateway = $this->gatewayRepository->create($data);

        $this->dispatcher->fire(new GatewayWasCreated($gateway));

        return $gateway;
    }

    public function update($gatewayId, array $data)
    {
        $gateway = $this->getRequestedGateway($gatewayId);

        $this->gatewayRepository->update($gateway, $data);

        $this->dispatcher->fire(new GatewayWasUpdated($gateway));

        return $gateway;
    }

    public function delete($gatewayId)
    {
        $gateway = $this->getRequestedGateway($gatewayId);

        $this->gatewayRepository->delete($gatewayId);

        $this->dispatcher->fire(new GatewayWasDeleted($gateway));

        return true;
    }

    private function getRequestedGateway($gatewayId)
    {
        $gateway = $this->gatewayRepository->getById($gatewayId);

        if (is_null($gateway)) {
            throw new GatewayNotFoundException();
        }

        return $gateway;
    }
}
