<?php

namespace Api\Gateways\Events;

use Infrastructure\Events\Event;
use Api\Gateways\Models\Gateway;

class GatewayWasUpdated extends Event
{
    public $gateway;

    public function __construct(Gateway $gateway)
    {
        $this->gateway = $gateway;
    }
}
