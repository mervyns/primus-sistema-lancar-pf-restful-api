<?php

namespace Api\Gateways\Repositories;

use Api\Gateways\Models\Gateway;
use Infrastructure\Database\Eloquent\Repository;

class GatewayRepository extends Repository
{
    public function getModel()
    {
        return new Gateway();
    }

    public function create(array $data)
    {
        $gateway = $this->getModel();

        $gateway->fill($data);
        
        $gateway->save();

        return $gateway;
    }

    public function update(Gateway $gateway, array $data)
    {

        $gateway->fill($data);

        $gateway->save();

        return $gateway;
    }
}
