<?php

namespace Api\Gateways\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Gateways\Requests\CreateGatewayRequest;
use Api\Gateways\Services\GatewayService;

class GatewayController extends Controller
{
    private $gatewayService;

    public function __construct(GatewayService $gatewayService)
    {
        $this->gatewayService = $gatewayService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->gatewayService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'gateways');

        return $this->response($parsedData);
    }

    public function getById($gatewayId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->gatewayService->getById($gatewayId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'gateway');

        return $this->response($parsedData);
    }

    public function create(CreateGatewayRequest $request)
    {
        $data = $request->get('gateway', []);

        return $this->response($this->gatewayService->create($data), 201);
    }

    public function update($gatewayId, Request $request)
    {
        $data = $request->get('gateway', []);

        return $this->response($this->gatewayService->update($gatewayId, $data));
    }

    public function delete($gatewayId)
    {
        return $this->response($this->gatewayService->delete($gatewayId));
    }
}
