<?php

namespace Api\Gateways;

use Infrastructure\Events\EventServiceProvider;
use Api\Gateways\Events\GatewayWasCreated;
use Api\Gateways\Events\GatewayWasDeleted;
use Api\Gateways\Events\GatewayWasUpdated;

class GatewayServiceProvider extends EventServiceProvider
{
    protected $listen = [
        GatewayWasCreated::class => [
            // listeners for when a gateway is created
        ],
        GatewayWasDeleted::class => [
            // listeners for when a gateway is deleted
        ],
        GatewayWasUpdated::class => [
            // listeners for when a gateway is updated
        ],
    ];
}
