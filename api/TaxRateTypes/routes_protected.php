<?php

$router->get('/api/tax-rate-types', 'TaxRateTypeController@getAll');
$router->get('/api/tax-rate-types/{id}', 'TaxRateTypeController@getById');
$router->post('/api/tax-rate-types', 'TaxRateTypeController@create');
$router->put('/api/tax-rate-types/{id}', 'TaxRateTypeController@update');
$router->delete('/api/tax-rate-types/{id}', 'TaxRateTypeController@delete');