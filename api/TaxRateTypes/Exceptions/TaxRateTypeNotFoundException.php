<?php

namespace Api\TaxRateTypes\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaxRateTypeNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The tax rate type was not found.');
    }
}
