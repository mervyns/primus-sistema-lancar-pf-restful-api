<?php

namespace Api\TaxRateTypes;

use Infrastructure\Events\EventServiceProvider;
use Api\TaxRateTypes\Events\TaxRateTypeWasCreated;
use Api\TaxRateTypes\Events\TaxRateTypeWasDeleted;
use Api\TaxRateTypes\Events\TaxRateTypeWasUpdated;

class TaxRateTypeServiceProvider extends EventServiceProvider
{
    protected $listen = [
        TaxRateTypeWasCreated::class => [
            // listeners for when a tax rate type is created
        ],
        TaxRateTypeWasDeleted::class => [
            // listeners for when a tax rate type is deleted
        ],
        TaxRateTypeWasUpdated::class => [
            // listeners for when a tax rate type is updated
        ],
    ];
}
