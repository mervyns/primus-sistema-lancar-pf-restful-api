<?php

namespace Api\TaxRateTypes\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\TaxRateTypes\Requests\CreateTaxRateTypeRequest;
use Api\TaxRateTypes\Services\TaxRateTypeService;

class TaxRateTypeController extends Controller
{
    private $taxRateTypeService;

    public function __construct(TaxRateTypeService $taxRateTypeService)
    {
        $this->taxRateTypeService = $taxRateTypeService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxRateTypeService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_rate_types');

        return $this->response($parsedData);
    }

    public function getById($taxRateTypeId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->taxRateTypeService->getById($taxRateTypeId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'tax_rate_type');

        return $this->response($parsedData);
    }

    public function create(CreateTaxRateTypeRequest $request)
    {
        $data = $request->get('tax_rate_type', []);

        return $this->response($this->taxRateTypeService->create($data), 201);
    }

    public function update($taxRateTypeId, Request $request)
    {
        $data = $request->get('tax_rate_type', []);

        return $this->response($this->taxRateTypeService->update($taxRateTypeId, $data));
    }

    public function delete($taxRateTypeId)
    {
        return $this->response($this->taxRateTypeService->delete($taxRateTypeId));
    }
}
