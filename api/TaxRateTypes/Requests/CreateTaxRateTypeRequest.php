<?php

namespace Api\TaxRateTypes\Requests;

use Infrastructure\Http\ApiRequest;

class CreateTaxRateTypeRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'tax_rate_type' => 'array|required',
            'tax_rate_type.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'tax_rate_type.name' => 'the tax rate type\'s name'
        ];
    }
}
