<?php

namespace Api\TaxRateTypes\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\TaxRateTypes\Exceptions\TaxRateTypeNotFoundException;
use Api\TaxRateTypes\Events\TaxRateTypeWasCreated;
use Api\TaxRateTypes\Events\TaxRateTypeWasDeleted;
use Api\TaxRateTypes\Events\TaxRateTypeWasUpdated;
use Api\TaxRateTypes\Repositories\UserTaxFillingStepRepository;

class TaxRateTypeService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $taxRateTypeRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserTaxFillingStepRepository $taxRateTypeRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->taxRateTypeRepository = $taxRateTypeRepository;
    }

    public function getAll($options = [])
    {
        return $this->taxRateTypeRepository->get($options);
    }

    public function getById($taxRateTypeId, array $options = [])
    {
        $taxRateType = $this->getRequestedTaxRateType($taxRateTypeId);

        return $taxRateType;
    }

    public function create($data)
    {
        $taxRateType = $this->taxRateTypeRepository->create($data);

        $this->dispatcher->fire(new TaxRateTypeWasCreated($taxRateType));

        return $taxRateType;
    }

    public function update($taxRateTypeId, array $data)
    {
        $taxRateType = $this->getRequestedTaxRateType($taxRateTypeId);

        $this->taxRateTypeRepository->update($taxRateType, $data);

        $this->dispatcher->fire(new TaxRateTypeWasUpdated($taxRateType));

        return $taxRateType;
    }

    public function delete($taxRateTypeId)
    {
        $taxRateType = $this->getRequestedTaxRateType($taxRateTypeId);

        $this->taxRateTypeRepository->delete($taxRateTypeId);

        $this->dispatcher->fire(new TaxRateTypeWasDeleted($taxRateType));

        return true;
    }

    private function getRequestedTaxRateType($taxRateTypeId)
    {
        $taxRateType = $this->taxRateTypeRepository->getById($taxRateTypeId);

        if (is_null($taxRateType)) {
            throw new TaxRateTypeNotFoundException();
        }

        return $taxRateType;
    }
}
