<?php

namespace Api\TaxRateTypes\Repositories;

use Api\TaxRateTypes\Models\TaxRateType;
use Infrastructure\Database\Eloquent\Repository;

class TaxRateTypeRepository extends Repository
{
    public function getModel()
    {
        return new TaxRateType();
    }

    public function create(array $data)
    {
        $taxRateType = $this->getModel();

        $taxRateType->fill($data);
        
        $taxRateType->save();

        return $taxRateType;
    }

    public function update(TaxRateType $taxRateType, array $data)
    {

        $taxRateType->fill($data);

        $taxRateType->save();

        return $taxRateType;
    }
}
