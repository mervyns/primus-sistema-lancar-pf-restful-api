<?php

namespace Api\TaxRateTypes\Models;

use Illuminate\Database\Eloquent\Model;

class TaxRateType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
