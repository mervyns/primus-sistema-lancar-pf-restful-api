<?php

namespace Api\TaxRateTypes\Events;

use Infrastructure\Events\Event;
use Api\TaxRateTypes\Models\TaxRateType;

class TaxRateTypeWasDeleted extends Event
{
    public $taxRateType;

    public function __construct(TaxRateType $taxRateType)
    {
        $this->taxRateType = $taxRateType;
    }
}
