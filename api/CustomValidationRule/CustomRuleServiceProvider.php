<?php

namespace Api\CustomValidationRule;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class CustomRuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('check_repatriation_data', function ($attribute, $value, $parameters, $validator) {
            $data = json_decode($value,true);
    
            if(!empty($data)){

                foreach ($data as $d_key => $element) {
                    $rules = [
                        'user_id' => 'required|exists:users,id',
                        'value' => 'required|numeric',
                        'asset_id' => 'required|exists:assets,id',
                        'nilai_dialihkan' => 'required|numeric',
                        'tanggal_investasi' => 'required|date_format:Y-m-d',
                        'nilai' => 'required|numeric',
                        'gateway_id' => 'required|exists:gateways,id',
                        'investment_id' => 'required|exists:investments,id',
                        'currency_id' => 'required|exists:countries,id',
                    ];

                    $validator = Validator::make($element, $rules);

                    if(!empty($validator->errors()->all())){
                        $string = implode(' ',$validator->errors()->all());
                        $this->message = $validator->errors()->all();
                        return false;
                    }
                }
                
            }
         
            return true;
        });

        Validator::replacer('check_repatriation_data', function ($message, $attribute, $rule, $parameters) {
          return  json_encode($this->message);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}