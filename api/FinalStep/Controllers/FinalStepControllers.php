<?php

namespace Api\FinalStep\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Infrastructure\Http\Controller;
use Api\UserIncomes\Services\UserIncomeService;
use Api\UserLiabilities\Services\UserLiabilityService;
use Api\UserAssets\Services\UserAssetService;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\UserAssets\Repositories\UserAssetRepository;


class FinalStepControllers extends Controller
{
   
    private $database;

    private $dispatcher;

    private $userIncomeRepository;

    private $LiabilityRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserIncomeRepository $userIncomeRepository,
        UserLiabilityRepository $userLiabilityRepository,
        UserAssetRepository $userAssetRepository,
        Request $request)
    {
        $this->userIncomeService = new UserIncomeService($database, $dispatcher, $userIncomeRepository);
        $this->userLiabilityService = new UserLiabilityService($database, $dispatcher, $userLiabilityRepository);
        $this->userAssetService = new UserAssetService($database, $dispatcher, $userAssetRepository);
        $this->request = $request;
    }

    public function finalStepRefresh($user_id){
        $result = [
            'penghasilan' => $this->getPenghasilan($user_id),
            'hutang' => $this->getHutang($user_id),
            'harta' => $this->getHarta($user_id)
        ];
        return $result;
    }

    public function getPenghasilan($userId){
        $data = $this->userIncomeService->getPenghasilan($userId);
        $html = View::make("partial.table_final_step_penghasilan", compact('data'))->render();
        return compact('html');
    }

    public function getHutang($userId){
        $data = $this->userLiabilityService->getHutang($userId);
        $html = View::make("partial.table_final_step_hutang", compact('data'))->render();
        return compact('html');
    }

    public function getHarta($userId){
        $data = $this->userAssetService->getHarta($userId);
        $html = View::make("partial.table_final_step_harta", compact('data'))->render();
        return compact('html');
    }
}
