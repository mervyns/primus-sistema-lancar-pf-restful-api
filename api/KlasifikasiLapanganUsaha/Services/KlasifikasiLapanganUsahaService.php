<?php

namespace Api\KlasifikasiLapanganUsaha\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\KlasifikasiLapanganUsaha\Exceptions\KlasifikasiLapanganUsahaNotFoundException;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasCreated;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasDeleted;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasUpdated;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;

class KlasifikasiLapanganUsahaService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $klasifikasiLapanganUsahaRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        KlasifikasiLapanganUsahaRepository $klasifikasiLapanganUsahaRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->klasifikasiLapanganUsahaRepository = $klasifikasiLapanganUsahaRepository;
    }

    public function getAll($options = [])
    {
        return $this->klasifikasiLapanganUsahaRepository->get($options);
    }

    public function getById($klasifikasiLapanganUsahaId, array $options = [])
    {
        $klasifikasiLapanganUsaha = $this->getRequestedKlasifikasiLapanganUsaha($klasifikasiLapanganUsahaId);

        return $klasifikasiLapanganUsaha;
    }

    public function create($data)
    {
        $klasifikasiLapanganUsaha = $this->klasifikasiLapanganUsahaRepository->create($data);

        $this->dispatcher->fire(new KlasifikasiLapanganUsahaWasCreated($klasifikasiLapanganUsaha));

        return $klasifikasiLapanganUsaha;
    }

    public function update($klasifikasiLapanganUsahaId, array $data)
    {
        $klasifikasiLapanganUsaha = $this->getRequestedKlasifikasiLapanganUsaha($klasifikasiLapanganUsahaId);

        $this->klasifikasiLapanganUsahaRepository->update($klasifikasiLapanganUsaha, $data);

        $this->dispatcher->fire(new KlasifikasiLapanganUsahaWasUpdated($klasifikasiLapanganUsaha));

        return $klasifikasiLapanganUsaha;
    }

    public function delete($klasifikasiLapanganUsahaId)
    {
        $klasifikasiLapanganUsaha = $this->getRequestedKlasifikasiLapanganUsaha($klasifikasiLapanganUsahaId);

        $this->klasifikasiLapanganUsahaRepository->delete($klasifikasiLapanganUsahaId);

        $this->dispatcher->fire(new KlasifikasiLapanganUsahaWasDeleted($klasifikasiLapanganUsaha));

        return true;
    }

    private function getRequestedKlasifikasiLapanganUsaha($klasifikasiLapanganUsahaId)
    {
        $klasifikasiLapanganUsaha = $this->klasifikasiLapanganUsahaRepository->getById($klasifikasiLapanganUsahaId);

        if (is_null($klasifikasiLapanganUsaha)) {
            throw new klasifikasiLapanganUsahaNotFoundException();
        }

        return $klasifikasiLapanganUsaha;
    }
}
