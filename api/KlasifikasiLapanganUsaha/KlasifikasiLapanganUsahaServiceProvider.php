<?php

namespace Api\KlasifikasiLapanganUsaha;

use Infrastructure\Events\EventServiceProvider;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasCreated;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasDeleted;
use Api\KlasifikasiLapanganUsaha\Events\KlasifikasiLapanganUsahaWasUpdated;

class KlasifikasiLapanganUsahaServiceProvider extends EventServiceProvider
{
    protected $listen = [
        KlasifikasiLapanganUsahaWasCreated::class => [
            // listeners for when a klu is created
        ],
        KlasifikasiLapanganUsahaWasDeleted::class => [
            // listeners for when a klu is deleted
        ],
        KlasifikasiLapanganUsahaWasUpdated::class => [
            // listeners for when a klu is updated
        ],
    ];
}
