<?php

$router->get('/api/klu', 'KlasifikasiLapanganUsahaController@getAll');
$router->get('/api/klu/{id}', 'KlasifikasiLapanganUsahaController@getById');
$router->post('/api/klu', 'KlasifikasiLapanganUsahaController@create');
$router->put('/api/klu/{id}', 'KlasifikasiLapanganUsahaController@update');
$router->delete('/api/klu/{id}', 'KlasifikasiLapanganUsahaController@delete');
