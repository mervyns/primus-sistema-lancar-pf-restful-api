<?php

namespace Api\KlasifikasiLapanganUsaha\Models;

use Illuminate\Database\Eloquent\Model;

class KlasifikasiLapanganUsaha extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'klu_number', 'name',
    ];
}
