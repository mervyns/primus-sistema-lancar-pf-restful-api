<?php

namespace Api\KlasifikasiLapanganUsaha\Repositories;

use Api\KlasifikasiLapanganUsaha\Models\KlasifikasiLapanganUsaha;
use Infrastructure\Database\Eloquent\Repository;

class KlasifikasiLapanganUsahaRepository extends Repository
{
    public function getModel()
    {
        return new KlasifikasiLapanganUsaha();
    }

    public function create(array $data)
    {
        $klu = $this->getModel();

        $klu->fill($data);
        
        $klu->save();

        return $klu;
    }

    public function update(KlasifikasiLapanganUsaha $klu, array $data)
    {

        $klu->fill($data);

        $klu->save();

        return $klu;
    }
}
