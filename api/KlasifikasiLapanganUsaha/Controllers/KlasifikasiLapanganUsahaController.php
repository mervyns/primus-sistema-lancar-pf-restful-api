<?php

namespace Api\KlasifikasiLapanganUsaha\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\KlasifikasiLapanganUsaha\Requests\CreateKlasifikasiLapanganUsahaRequest;
use Api\KlasifikasiLapanganUsaha\Services\KlasifikasiLapanganUsahaService;

class KlasifikasiLapanganUsahaController extends Controller
{
    private $kluService;

    public function __construct(KlasifikasiLapanganUsahaService $kluService)
    {
        $this->kluService = $kluService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->kluService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'klasifikasi_lapangan_usaha');

        return $this->response($parsedData);
    }

    public function getById($kluId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->kluService->getById($kluId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'klasifikasi_lapangan_usaha');

        return $this->response($parsedData);
    }

    public function create(CreateKlasifikasiLapanganUsahaRequest $request)
    {
        $data = $request->get('klasifikasi_lapangan_usaha', []);

        return $this->response($this->kluService->create($data), 201);
    }

    public function update($kluId, Request $request)
    {
        $data = $request->get('klasifikasi_lapangan_usaha', []);

        return $this->response($this->kluService->update($kluId, $data));
    }

    public function delete($kluId)
    {
        return $this->response($this->kluService->delete($kluId));
    }
}
