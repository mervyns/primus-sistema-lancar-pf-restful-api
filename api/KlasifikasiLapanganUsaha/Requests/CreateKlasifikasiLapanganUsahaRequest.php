<?php

namespace Api\KlasifikasiLapanganUsaha\Requests;

use Infrastructure\Http\ApiRequest;

class CreateKlasifikasiLapanganUsahaRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'klasifikasi_lapangan_usaha' => 'array|required',
            'klasifikasi_lapangan_usaha.klu_number' => 'required|string',
            'klasifikasi_lapangan_usaha.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'klasifikasi_lapangan_usaha.name' => 'the klasifikasi lapangan usaha\'s name'
        ];
    }
}
