<?php

namespace Api\KlasifikasiLapanganUsaha\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KlasifikasiLapanganUsahaNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The KLU was not found.');
    }
}
