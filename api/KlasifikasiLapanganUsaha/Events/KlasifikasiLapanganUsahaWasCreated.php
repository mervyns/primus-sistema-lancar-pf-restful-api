<?php

namespace Api\KlasifikasiLapanganUsaha\Events;

use Infrastructure\Events\Event;
use Api\KlasifikasiLapanganUsaha\Models\KlasifikasiLapanganUsaha;

class KlasifikasiLapanganUsahaWasCreated extends Event
{
    public $klu;

    public function __construct(KlasifikasiLapanganUsaha $klu)
    {
        $this->klu = $klu;
    }
}
