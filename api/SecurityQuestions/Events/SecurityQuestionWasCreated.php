<?php

namespace Api\SecurityQuestions\Events;

use Infrastructure\Events\Event;
use Api\SecurityQuestions\Models\SecurityQuestion;

class SecurityQuestionWasCreated extends Event
{
    public $securityQuestion;

    public function __construct(SecurityQuestion $securityQuestion)
    {
        $this->securityQuestion = $securityQuestion;
    }
}
