<?php

namespace Api\SecurityQuestions\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SecurityQuestionNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The security question was not found.');
    }
}
