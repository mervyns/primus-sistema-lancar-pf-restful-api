<?php

namespace Api\SecurityQuestions;

use Infrastructure\Events\EventServiceProvider;
use Api\SecurityQuestions\Events\SecurityQuestionWasCreated;
use Api\SecurityQuestions\Events\SecurityQuestionWasDeleted;
use Api\SecurityQuestions\Events\SecurityQuestionWasUpdated;

class SecurityQuestionServiceProvider extends EventServiceProvider
{
    protected $listen = [
        SecurityQuestionWasCreated::class => [
            // listeners for when a security question is created
        ],
        SecurityQuestionWasDeleted::class => [
            // listeners for when a security question is deleted
        ],
        SecurityQuestionWasUpdated::class => [
            // listeners for when a security question is updated
        ],
    ];
}
