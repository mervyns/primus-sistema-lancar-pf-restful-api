<?php

namespace Api\SecurityQuestions\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
