<?php

namespace Api\SecurityQuestions\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\SecurityQuestions\Exceptions\SecurityQuestionNotFoundException;
use Api\SecurityQuestions\Events\SecurityQuestionWasCreated;
use Api\SecurityQuestions\Events\SecurityQuestionWasDeleted;
use Api\SecurityQuestions\Events\SecurityQuestionWasUpdated;
use Api\SecurityQuestions\Repositories\SecurityQuestionRepository;

class SecurityQuestionService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $securityQuestionRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        SecurityQuestionRepository $securityQuestionRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->securityQuestionRepository = $securityQuestionRepository;
    }

    public function getAll($options = [])
    {
        return $this->securityQuestionRepository->get($options);
    }

    public function getById($securityQuestionId, array $options = [])
    {
        $securityQuestion = $this->getRequestedSecurityQuestion($securityQuestionId);

        return $securityQuestion;
    }

    public function create($data)
    {
        $securityQuestion = $this->securityQuestionRepository->create($data);

        $this->dispatcher->fire(new SecurityQuestionWasCreated($securityQuestion));

        return $securityQuestion;
    }

    public function update($securityQuestionId, array $data)
    {
        $securityQuestion = $this->getRequestedSecurityQuestion($securityQuestionId);

        $this->securityQuestionRepository->update($securityQuestion, $data);

        $this->dispatcher->fire(new SecurityQuestionWasUpdated($securityQuestion));

        return $securityQuestion;
    }

    public function delete($securityQuestionId)
    {
        $securityQuestion = $this->getRequestedSecurityQuestion($securityQuestionId);

        $this->securityQuestionRepository->delete($securityQuestionId);

        $this->dispatcher->fire(new SecurityQuestionWasDeleted($securityQuestion));

        return true;
    }

    private function getRequestedSecurityQuestion($securityQuestionId)
    {
        $securityQuestion = $this->securityQuestionRepository->getById($securityQuestionId);

        if (is_null($securityQuestion)) {
            throw new SecurityQuestionNotFoundException();
        }

        return $securityQuestion;
    }
}
