<?php

namespace Api\SecurityQuestions\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\SecurityQuestions\Requests\CreateSecurityQuestionRequest;
use Api\SecurityQuestions\Services\SecurityQuestionService;

class SecurityQuestionController extends Controller
{
    private $securityQuestionService;

    public function __construct(SecurityQuestionService $securityQuestionService)
    {
        $this->securityQuestionService = $securityQuestionService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->securityQuestionService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'security_questions');

        return $this->response($parsedData);
    }

    public function getById($securityQuestionId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->securityQuestionService->getById($securityQuestionId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'security_question');

        return $this->response($parsedData);
    }

    public function create(CreateSecurityQuestionRequest $request)
    {
        $data = $request->get('security_question', []);

        return $this->response($this->securityQuestionService->create($data), 201);
    }

    public function update($securityQuestionId, Request $request)
    {
        $data = $request->get('security_question', []);

        return $this->response($this->securityQuestionService->update($securityQuestionId, $data));
    }

    public function delete($securityQuestionId)
    {
        return $this->response($this->securityQuestionService->delete($securityQuestionId));
    }
}
