<?php

namespace Api\SecurityQuestions\Requests;

use Infrastructure\Http\ApiRequest;

class CreateSecurityQuestionRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'security_question' => 'array|required',
            'security_question.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'security_question.name' => 'the security question\'s name'
        ];
    }
}
