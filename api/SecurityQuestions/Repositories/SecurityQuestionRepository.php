<?php

namespace Api\SecurityQuestions\Repositories;

use Api\SecurityQuestions\Models\SecurityQuestion;
use Infrastructure\Database\Eloquent\Repository;

class SecurityQuestionRepository extends Repository
{
    public function getModel()
    {
        return new SecurityQuestion();
    }

    public function create(array $data)
    {
        $securityQuestion = $this->getModel();

        $securityQuestion->fill($data);
        
        $securityQuestion->save();

        return $securityQuestion;
    }

    public function update(SecurityQuestion $securityQuestion, array $data)
    {

        $securityQuestion->fill($data);

        $securityQuestion->save();

        return $securityQuestion;
    }
}
