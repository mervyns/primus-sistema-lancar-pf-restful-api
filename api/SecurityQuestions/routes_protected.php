<?php

$router->get('/api/security-questions', 'SecurityQuestionController@getAll');
$router->get('/api/security-questions/{id}', 'SecurityQuestionController@getById');
$router->post('/api/security-questions', 'SecurityQuestionController@create');
$router->put('/api/security-questions/{id}', 'SecurityQuestionController@update');
$router->delete('/api/security-questions/{id}', 'SecurityQuestionController@delete');


/**
 * End of file
 */
