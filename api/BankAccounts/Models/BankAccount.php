<?php

namespace Api\BankAccounts\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
	protected $table = 'bank_accounts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id',
        'account_number',
        'account_under_name',
        'status'
    ];

    public function banks(){
        return $this->belongsTo('Api\Banks\Models\Bank', 'bank_id');
    }

    public function userOrders(){
        return $this->hasMany('Api\UserOrders\Models\UserOrder');
    }
}
