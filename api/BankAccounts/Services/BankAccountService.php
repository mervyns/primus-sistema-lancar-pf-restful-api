<?php

namespace Api\BankAccounts\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\BankAccounts\Exceptions\BankAccountNotFoundException;
use Api\BankAccounts\Events\BankAccountWasCreated;
use Api\BankAccounts\Events\BankAccountWasDeleted;
use Api\BankAccounts\Events\BankAccountWasUpdated;
use Api\BankAccounts\Repositories\BankAccountRepository;
use Api\BankAccounts\Models\BankAccount;

class BankAccountService
{
    private $database;
    private $dispatcher;
    private $bankAccountRepository;

    public function __construct(
    	DatabaseManager $database, 
    	Dispatcher $dispatcher, 
    	BankAccountRepository $bankAccountRepository){
    	$this->database = $database;
    	$this->dispatcher = $dispatcher;
    	$this->bankAccountRepository = $bankAccountRepository;
    }

    public function getAll(array $options = array()){
        $bankAccountData = array();
        $bankAccounts = $this->bankAccountRepository->get($options);

        foreach ($bankAccounts as $bankAccount){
            $data = $bankAccount;
            $data->banks = $bankAccount->banks;
            $bankAccountData[] = $data;
        }
        return $bankAccountData;
    }

    public function getAllActive(array $options = array()){
        $bankAccountData = array();
        $bankAccounts = $this->getRequestBankAccount->getWhere('status', 1, $options);

        foreach ($bankAccounts as $bankAccount){
            $data = $bankAccount;
            $data->banks = $bankAccount->banks;
            $bankAccountData[] = $data;
        }
        return $bankAccountData;
    }

    public function getById(int $bankAccountId, array $options = array()){
    	$bankAccount = $this->getRequestBankAccount($bankAccountId);
    	return $bankAccount;
    }

    public function create(array $data){
    	$bankAccount = $this->bankAccountRepository->create($data);
    	$this->dispatcher->fire(new BankAccountWasCreated($bankAccount));
    	return $bankAccount;
    }

    public function update(int $bankAccountId, array $data){
    	$bankAccount = $this->getRequestBankAccount($bankAccountId);
    	$this->bankAccountRepository->update($bankAccount, $data);
    	$this->dispatcher->fire(new BankAccountWasUpdated($bankAccount));
    	return $bankAccount;
    }

    public function delete(int $bankAccountId){
    	$bankAccount = $this->getRequestBankAccount($bankAccountId);
    	$this->bankAccountRepository->delete($bankAccountId);
    	$this->dispatcher->fire(new BankAccountWasDeleted($bankAccount));
    	return $bankAccount;
    }

    private function getRequestBankAccount(int $bankAccountId){
    	$bankAccount = $this->bankAccountRepository->getById($bankAccountId);
    	if (is_null($bankAccount)){
    		throw new BankAccountNotFoundException();
    	}
    	return $bankAccount;
    }

}
