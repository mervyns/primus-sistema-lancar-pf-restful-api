<?php

namespace Api\BankAccounts\Repositories;

use Api\BankAccounts\Models\BankAccount;
use Infrastructure\Database\Eloquent\Repository;

class BankAccountRepository extends Repository{
	public function getModel(){
		return new BankAccount();
	}

	public function create(array $data){
		$bankAccount = $this->getModel();
		$bankAccount->fill($data);
		$bankAccount->save();
		return $bankAccount;
	}

	public function update(BankAccount $bankAccount, array $data){
		$bankAccount->fill($data);
		$bankAccount->save();
		return $bankAccount;
	}
}