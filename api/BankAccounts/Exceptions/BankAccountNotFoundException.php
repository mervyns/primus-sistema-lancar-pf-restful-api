<?php

namespace Api\BankAccounts\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BankAccountNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('bank account was not found.');
    }
}
