<?php

namespace Api\BankAccounts\Events;

use Infrastructure\Events\Event;
use Api\BankAccounts\Models\BankAccount;

class BankAccountWasUpdated extends Event
{
    public $bankAccount;

    public function __construct(BankAccount $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }
}
