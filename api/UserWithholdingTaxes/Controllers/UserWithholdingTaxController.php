<?php

namespace Api\UserWithholdingTaxes\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserWithholdingTaxes\Requests\CreateUserWithholdingTaxRequest;
use Api\UserWithholdingTaxes\Services\UserWithholdingTaxService;

class UserWithholdingTaxController extends Controller
{
    private $userWithholdingTaxService;

    public function __construct(UserWithholdingTaxService $userWithholdingTaxService)
    {
        $this->userWithholdingTaxService = $userWithholdingTaxService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userWithholdingTaxService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_withholding_taxes');

        return $this->response($parsedData);
    }

    public function getById($userWithholdingTaxServiceId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userWithholdingTaxService->getById($userWithholdingTaxServiceId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_withholding_tax');

        return $this->response($parsedData);
    }

    public function create(CreateUserWithholdingTaxRequest $request)
    {
        $data = $request->get('user_withholding_tax', []);

        return $this->response($this->userWithholdingTaxService->create($data), 201);
    }

    public function update($userWithholdingTaxServiceId, Request $request)
    {
        $data = $request->get('user_withholding_tax', []);

        return $this->response($this->userWithholdingTaxService->update($userWithholdingTaxServiceId, $data));
    }

    public function delete($userWithholdingTaxServiceId)
    {
        return $this->response($this->userWithholdingTaxService->delete($userWithholdingTaxServiceId));
    }
}
