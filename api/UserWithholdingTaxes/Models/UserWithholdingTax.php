<?php

namespace Api\UserWithholdingTaxes\Models;

use Illuminate\Database\Eloquent\Model;

class UserWithholdingTax extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_income_id', 'withholding_tax_slip_id', 'withholder_name', 'withholder_npwp', 'withholding_slip_number', 'withholding_slip_date', 'value' 
    ];
}
