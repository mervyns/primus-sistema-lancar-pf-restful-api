<?php

namespace Api\WithholdingTaxSlips\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserWithholdingTaxNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user withholding tax was not found.');
    }
}
