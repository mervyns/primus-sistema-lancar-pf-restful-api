<?php

namespace Api\UserWithholdingTaxes\Repositories;

use Api\UserWithholdingTaxes\Models\UserWithholdingTax;
use Infrastructure\Database\Eloquent\Repository;

class UserWithholdingTaxRepository extends Repository
{
    public function getModel()
    {
        return new UserWithholdingTax();
    }

    public function create(array $data)
    {
        $userWithholdingTax = $this->getModel();

        $userWithholdingTax->fill($data);
        
        $userWithholdingTax->save();

        return $userWithholdingTax;
    }

    public function update(UserWithholdingTax $userWithholdingTax, array $data)
    {

        $userWithholdingTax->fill($data);

        $userWithholdingTax->save();

        return $userWithholdingTax;
    }
}
