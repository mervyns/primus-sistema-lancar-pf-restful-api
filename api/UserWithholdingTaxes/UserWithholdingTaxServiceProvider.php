<?php

namespace Api\UserWithholdingTaxes;

use Infrastructure\Events\EventServiceProvider;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasCreated;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasDeleted;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasUpdated;

class UserWithholdingTaxServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserWithholdingTaxWasCreated::class => [
            // listeners for when user's withholding tax is created
        ],
        UserWithholdingTaxWasDeleted::class => [
            // listeners for when user's withholding tax is deleted
        ],
        UserWithholdingTaxWasUpdated::class => [
            // listeners for when user's withholding tax is updated
        ],
    ];
}
