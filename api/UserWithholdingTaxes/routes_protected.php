<?php

$router->get('/api/user-withholding-taxes', 'UserWithholdingTaxController@getAll');
$router->get('/api/user-withholding-taxes/{id}', 'UserWithholdingTaxController@getById');
$router->post('/api/user-withholding-taxes', 'UserWithholdingTaxController@create');
$router->put('/api/user-withholding-taxes/{id}', 'UserWithholdingTaxController@update');
$router->delete('/api/user-withholding-taxes/{id}', 'UserWithholdingTaxController@delete');