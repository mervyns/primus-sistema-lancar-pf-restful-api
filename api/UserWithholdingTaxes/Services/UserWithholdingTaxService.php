<?php

namespace Api\UserWithholdingTaxes\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserWithholdingTaxes\Exceptions\UserWithholdingTaxNotFoundException;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasCreated;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasDeleted;
use Api\UserWithholdingTaxes\Events\UserWithholdingTaxWasUpdated;
use Api\UserWithholdingTaxes\Repositories\UserWithholdingTaxRepository;
use Api\UserIncomes\Repositories\UserIncomeRepository;
use Api\Users\Repositories\UserRepository;
use Api\Incomes\Repositories\IncomeRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\UserAssets\Repositories\UserAssetRepository;
use Api\UserForeignAssets\Repositories\UserForeignAssetRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\WithholdingTaxSlips\Repositories\WithholdingTaxSlipRepository;

class UserWithholdingTaxService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userWithholdingTaxRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserWithholdingTaxRepository $userWithholdingTaxRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userWithholdingTaxRepository = $userWithholdingTaxRepository;
    }

    public function getAll($options = [])
    {
        $userIncomeRepository = new UserIncomeRepository();
        $userRepository = new UserRepository();
        $incomeRepository = new IncomeRepository();
        $countryRepository = new CountryRepository();
        $userAssetRepository = new UserAssetRepository();
        $userForeignAssetRepository = new UserForeignAssetRepository();
        $assetRepository = new AssetRepository();
        $withholdingTaxSlipRepository = new WithholdingTaxSlipRepository();

        $userWithholdingTax = $this->userWithholdingTaxRepository->get($options);

        $i=0;
        foreach ($userWithholdingTax as $uWT) {
            $userIncome = $userIncomeRepository->getById($uWT['user_income_id']);
            $user = $userRepository->getById($userIncome['user_id']);
            $income = $incomeRepository->getById($userIncome['income_id']);
            $country = $countryRepository->getById($userIncome['country_id']);
            if(!empty($userIncome['user_asset_id'])){
                $userAsset = $userAssetRepository->getById($userIncome['user_asset_id']);
                $asset = $assetRepository->getById($userAsset['asset_id']);
            }
                
            if(!empty($userIncome['user_foreign_asset_id'])){
                $userForeignAsset = $userForeignAssetRepository->getById($userIncome['user_foreign_asset_id']);
                $asset = $assetRepository->getById($userForeignAsset['asset_id']);
            }
            
            if(!empty($userIncome['asset_id'])){
                 $asset = $assetRepository->getById($userIncome['asset_id']);
            }
            $withholdingTaxSlip = $withholdingTaxSlipRepository->getById($uWT['withholding_tax_slip_id']);

            $userWithholdingTax[$i]['user_name'] = $user['name'];
            $userWithholdingTax[$i]['income_name'] = $income['name'];
            $userWithholdingTax[$i]['income_net_value'] = $userIncome['net_value'];
            $userWithholdingTax[$i]['income_gross_value'] = $userIncome['gross_value'];
            $userWithholdingTax[$i]['country_name'] = $country['name'];
            $userWithholdingTax[$i]['currency'] = $country['currency'];
            $userWithholdingTax[$i]['currency_name'] = $country['currency_name'];
            if(!empty($asset['name']))
                $userWithholdingTax[$i]['user_asset_source_name'] = $asset['name'];
            $userWithholdingTax[$i]['withholding_tax_slip_name'] = $withholdingTaxSlip['name'];

            $i++;
        }

        return $userWithholdingTax;
    }

    public function getById($userWithholdingTaxId, array $options = [])
    {
        $userIncomeRepository = new UserIncomeRepository();
        $userRepository = new UserRepository();
        $incomeRepository = new IncomeRepository();
        $countryRepository = new CountryRepository();
        $userAssetRepository = new UserAssetRepository();
        $userForeignAssetRepository = new UserForeignAssetRepository();
        $assetRepository = new AssetRepository();
        $withholdingTaxSlipRepository = new WithholdingTaxSlipRepository();

        $userWithholdingTax = $this->getRequestedUserWithholdingTax($userWithholdingTaxId);

        $userIncome = $userIncomeRepository->getById($userWithholdingTax['user_income_id']);
        $user = $userRepository->getById($userIncome['user_id']);
        $income = $incomeRepository->getById($userIncome['income_id']);
        $country = $countryRepository->getById($userIncome['country_id']);
        if(!empty($userIncome['user_asset_id'])){
            $userAsset = $userAssetRepository->getById($userIncome['user_asset_id']);
            $asset = $assetRepository->getById($userAsset['asset_id']);
        }
            
        if(!empty($userIncome['user_foreign_asset_id'])){
            $userForeignAsset = $userForeignAssetRepository->getById($userIncome['user_foreign_asset_id']);
            $asset = $assetRepository->getById($userForeignAsset['asset_id']);
        }
        
        if(!empty($userIncome['asset_id'])){
             $asset = $assetRepository->getById($userIncome['asset_id']);
        }
        $withholdingTaxSlip = $withholdingTaxSlipRepository->getById($userWithholdingTax['withholding_tax_slip_id']);

        $userWithholdingTax['user_name'] = $user['name'];
        $userWithholdingTax['income_name'] = $income['name'];
        $userWithholdingTax['income_net_value'] = $userIncome['net_value'];
        $userWithholdingTax['income_gross_value'] = $userIncome['gross_value'];
        $userWithholdingTax['country_name'] = $country['name'];
        $userWithholdingTax['currency'] = $country['currency'];
        $userWithholdingTax['currency_name'] = $country['currency_name'];
        if(!empty($asset['name']))
            $userWithholdingTax['user_asset_source_name'] = $asset['name'];
        $userWithholdingTax['withholding_tax_slip_name'] = $withholdingTaxSlip['name'];

        return $userWithholdingTax;
    }

    public function create($data)
    {
        $userWithholdingTax = $this->userWithholdingTaxRepository->create($data);

        $this->dispatcher->fire(new UserWithholdingTaxWasCreated($userWithholdingTax));

        return $userWithholdingTax;
    }

    public function update($userWithholdingTaxId, array $data)
    {
        $userWithholdingTax = $this->getRequestedUserWithholdingTax($userWithholdingTaxId);

        $this->userWithholdingTaxRepository->update($userWithholdingTax, $data);

        $this->dispatcher->fire(new UserWithholdingTaxWasUpdated($userWithholdingTax));

        return $userWithholdingTax;
    }

    public function delete($userWithholdingTaxId)
    {
        $userWithholdingTax = $this->getRequestedUserWithholdingTax($userWithholdingTaxId);

        $this->userWithholdingTaxRepository->delete($userWithholdingTaxId);

        $this->dispatcher->fire(new UserWithholdingTaxWasDeleted($userWithholdingTax));

        return true;
    }

    private function getRequestedUserWithholdingTax($userWithholdingTaxId)
    {
        $userWithholdingTax = $this->userWithholdingTaxRepository->getById($userWithholdingTaxId);

        if (is_null($userWithholdingTax)) {
            throw new UserWithholdingTaxNotFoundException();
        }

        return $userWithholdingTax;
    }
}
