<?php

namespace Api\UserWithholdingTaxes\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserWithholdingTaxRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_withholding_tax' => 'array|required',
            'user_withholding_tax.user_income_id' => 'required|integer',
            'user_withholding_tax.withholding_tax_slip_id' => 'required|integer',
            'user_withholding_tax.withholder_name' => 'required|string',
            'user_withholding_tax.withholder_npwp' => 'required|numeric',
            'user_withholding_tax.withholding_slip_number' => 'required|string',
            'user_withholding_tax.withholding_slip_date' => 'required|date_format:Y-m-d',
            'user_withholding_tax.value' => 'required|between:0,9999999999999.99',
        ];
    }

    
    public function attributes()
    {
        return [
            'user_withholding_tax.withholder_name' => 'the tax withholder\'s name'
        ];
    }
}
