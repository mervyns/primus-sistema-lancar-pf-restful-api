<?php

namespace Api\UserWithholdingTaxes\Events;

use Infrastructure\Events\Event;
use Api\UserWithholdingTaxes\Models\UserWithholdingTax;

class UserWithholdingTaxWasUpdated extends Event
{
    public $userWithholdingTax;

    public function __construct(UserWithholdingTax $userWithholdingTax)
    {
        $this->userWithholdingTax = $userWithholdingTax;
    }
}
