<?php

namespace Api\KursPajak\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class KursPajakController extends Controller
{
    public function getKurs(Request $request)
    {
        $data=$request->all();

        $month = $data['month'];
        $date = $data['date'];
        $year = $data['year'];
        $id = $month . '/' . $date . '/' . $year;
        $currency = $data['currency'];

        //start crawling data
        $client = new Client();

        try {
            $res = $client->request('POST', 'http://www.fiskal.kemenkeu.go.id/dw-kurs-db.asp', [
                'form_params' => [
                    'strDate' => $year.$month.$date,
                    'id' => $id,
                ]
            ]);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $res = $client->request('GET', 'http://www.fiskal.kemenkeu.go.id/dw-kurs-db.asp');
        }

        $htmlData = $res->getBody()->getContents();

        $dom = HtmlDomParser::str_get_html($htmlData);
        $tableData = $dom->find('td');
        $kursPajak = array();

        $i = 0;
        foreach($tableData as $td)
        {
            if(strpos( $td->plaintext, $currency ) !== false){
                $kursPajak['kurspajak_currency_name'] = $td->plaintext;

                 $kursPajak['kurspajak_value'] = (double) preg_replace("/([^0-9\\.])/i", "", $tableData[$i + 1]->plaintext);

                if(preg_match('/JPY/',$td->plaintext) > 0){
                    $kursPajak['kurspajak_value'] = ($kursPajak['kurspajak_value']/100);
                }
               
                //$kursPajakValue = str_replace('.', '', $tableData[$i + 1]->plaintext);
                //$kursPajakValue = str_replace(',', '.', $kursPajakValue);
                //$kursPajak['kurspajak_value'] = $kursPajakValue;

                break;
            }

            $i++;
        }

        //end of crawling data

        $kursPajak = json_encode($kursPajak);

        if(!empty($kursPajak)){
            return $kursPajak;
        }

        return false;
    }
}
