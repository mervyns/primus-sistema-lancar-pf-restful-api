<?php

namespace Api\UserLiabilities;

use Infrastructure\Events\EventServiceProvider;
use Api\UserLiabilities\Events\UserLiabilityWasCreated;
use Api\UserLiabilities\Events\UserLiabilityWasDeleted;
use Api\UserLiabilities\Events\UserLiabilityWasUpdated;

class UserLiabilityServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserLiabilityWasCreated::class => [
            // listeners for when a user liability is created
        ],
        UserLiabilityWasDeleted::class => [
            // listeners for when a user liability is deleted
        ],
        UserLiabilityWasUpdated::class => [
            // listeners for when a user liability is updated
        ],
    ];
}
