<?php

namespace Api\UserLiabilities\Models;

use Illuminate\Database\Eloquent\Model;

class UserLiability extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'liability_id', 'acquisition_year','value', 'lender_name', 'lender_address', 'detail'
    ];

    public function liabilities()
    {
        return $this->belongsTo('Api\Liabilities\Models\Liability','liability_id');
    }

    public function liabilitiesQuery($id)
    {
        return \DB::table('users')
            ->select([
                'user_liabilities.*',
                'user_liabilities.lender_name as lender_name',
                'liabilities.name as liability_name',
                'liabilities.step as liability_step'

            ])
            ->join('user_liabilities', 'users.id', '=', 'user_liabilities.user_id')
            ->join('liabilities', 'liabilities.id', '=', 'user_liabilities.liability_id')
            // ->orderBy('incomes.name','asc')
            ->where('users.id',$id);
    }
}
