<?php

namespace Api\UserLiabilities\Repositories;

use Api\UserLiabilities\Models\UserLiability;
use Infrastructure\Database\Eloquent\Repository;

class UserLiabilityRepository extends Repository
{
    public function getModel()
    {
        return new UserLiability();
    }

    public function create(array $data)
    {
        $userLiability = $this->getModel();

        $userLiability->fill($data);
        
        $userLiability->save();

        return $userLiability;
    }

    public function update(UserLiability $userLiability, array $data)
    {

        $userLiability->fill($data);

        $userLiability->save();

        return $userLiability;
    }
}
