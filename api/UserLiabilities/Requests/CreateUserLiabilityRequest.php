<?php

namespace Api\UserLiabilities\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserLiabilityRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_liability' => 'array|required',
            'user_liability.user_id' => 'required|integer',
            'user_liability.liability_id' => 'required|integer',
            'user_liability.acquisition_year' => 'required|integer',
            'user_liability.value' => 'required|between:0,999999999999.99',
            'user_liability.lender_name' => 'required|string',
            'user_liability.lender_address' => 'required|string',
            'user_liability.detail' => 'string',
        ];
    }

    
    public function attributes()
    {
        return [
            'user_liability.name' => 'the user liability\'s name'
        ];
    }
}
