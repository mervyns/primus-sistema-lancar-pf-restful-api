<?php

namespace Api\UserLiabilities\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserLiabilityNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user liability was not found.');
    }
}
