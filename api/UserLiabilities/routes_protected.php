<?php

$router->get('/api/user-liabilities', 'UserLiabilityController@getAll');
$router->get('/api/user-liabilities/{id}', 'UserLiabilityController@getById');
$router->post('/api/user-liabilities', 'UserLiabilityController@create');
$router->put('/api/user-liabilities/{id}', 'UserLiabilityController@update');
$router->delete('/api/user-liabilities/{id}', 'UserLiabilityController@delete');