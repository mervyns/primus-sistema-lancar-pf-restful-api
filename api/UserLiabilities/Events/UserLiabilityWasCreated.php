<?php

namespace Api\UserLiabilities\Events;

use Infrastructure\Events\Event;
use Api\UserLiabilities\Models\UserLiability;

class UserLiabilityWasCreated extends Event
{
    public $userLiability;

    public function __construct(UserLiability $userLiability)
    {
        $this->userLiability = $userLiability;
    }
}
