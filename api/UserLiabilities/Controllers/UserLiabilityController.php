<?php

namespace Api\UserLiabilities\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserLiabilities\Requests\CreateUserLiabilityRequest;
use Api\UserLiabilities\Services\UserLiabilityService;

class UserLiabilityController extends Controller
{
    private $userLiabilityService;

    public function __construct(UserLiabilityService $userLiabilityService)
    {
        $this->userLiabilityService = $userLiabilityService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userLiabilityService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_liabilities');

        return $this->response($parsedData);
    }

    public function getById($userLiabilityId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userLiabilityService->getById($userLiabilityId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_liability');

        return $this->response($parsedData);
    }

    public function create(CreateUserLiabilityRequest $request)
    {
        $data = $request->get('user_liability', []);

        return $this->response($this->userLiabilityService->create($data), 201);
    }

    public function update($userLiabilityId, Request $request)
    {
        $data = $request->get('user_liability', []);

        return $this->response($this->userLiabilityService->update($userLiabilityId, $data));
    }

    public function delete($userLiabilityId)
    {
        return $this->response($this->userLiabilityService->delete($userLiabilityId));
    }
}
