<?php

namespace Api\UserLiabilities\Services;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserLiabilities\Exceptions\UserLiabilityNotFoundException;
use Api\UserLiabilities\Events\UserLiabilityWasCreated;
use Api\UserLiabilities\Events\UserLiabilityWasDeleted;
use Api\UserLiabilities\Events\UserLiabilityWasUpdated;
use Api\UserLiabilities\Repositories\UserLiabilityRepository;
use Api\Users\Repositories\UserRepository;
use Api\Liabilities\Repositories\LiabilityRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\Banks\Repositories\BankRepository;
use Api\UserIncomes\Services\UserIncomeService;
use Api\UserLiabilities\Services\UserLiabilityService;
use Api\UserAssets\Services\UserAssetService;
use Api\KursPajak\Controllers\KursPajakController;
use WebApp\TaxFilling\FormData\FinalSteps;

class UserLiabilityService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userLiabilityRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserLiabilityRepository $userLiabilityRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userLiabilityRepository = $userLiabilityRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $liabilityRepository = new LiabilityRepository();

        $userLiability = $this->userLiabilityRepository->get($options);

        $i=0;
        foreach ($userLiability as $uL) {
            $user = $userRepository->getById($uL['user_id']);
            $liability = $liabilityRepository->getById($uL['liability_id']);

            $userLiability[$i]['user_name'] = $user['name'];
            $userLiability[$i]['liability_name'] = $liability['name'];
            $i++;
        }

        return $userLiability;
    }

    public function getById($userLiabilityId, array $options = [])
    {
        $userRepository = new UserRepository();
        $liabilityRepository = new LiabilityRepository();

        $userLiability = $this->getRequestedUserLiability($userLiabilityId);

        $user = $userRepository->getById($userLiability['user_id']);
        $liability = $liabilityRepository->getById($userLiability['liability_id']);

        $userLiability['user_name'] = $user['name'];
        $userLiability['liability_name'] = $liability['name'];

        return $userLiability;
    }

    public function create($data)
    {
        $userLiability = $this->userLiabilityRepository->create($data);

        $this->dispatcher->fire(new UserLiabilityWasCreated($userLiability));

        return $userLiability;
    }

    public function update($userLiabilityId, array $data)
    {
        $userLiability = $this->getRequestedUserLiability($userLiabilityId);

        $this->userLiabilityRepository->update($userLiability, $data);

        $this->dispatcher->fire(new UserLiabilityWasUpdated($userLiability));

        return $userLiability;
    }

    public function delete($userLiabilityId)
    {
        $userLiability = $this->getRequestedUserLiability($userLiabilityId);

        $this->userLiabilityRepository->delete($userLiabilityId);

        $this->dispatcher->fire(new UserLiabilityWasDeleted($userLiability));

        return true;
    }

    private function getRequestedUserLiability($userLiabilityId)
    {
        $userLiability = $this->userLiabilityRepository->getById($userLiabilityId);

        if (is_null($userLiability)) {
            throw new UserLiabilityNotFoundException();
        }

        return $userLiability;
    }

    public function getHutang($id){
        try{

			$result = [
				'hutang' => []
            ];
            
			$user_liabilities = $this->userLiabilityRepository->getModel()->liabilitiesQuery($id);
            $countryRepository = new CountryRepository();
            $bankRepository = new BankRepository();
            $finalStep = new FinalSteps();
            $result = $finalStep->mapingDataForAjax($user_liabilities, 'hutang');

		}catch(\Exception $e){
			dd($e);
		}
		

		return [
			'data' => $result
		];
    }

    private function getCurrency($code){
        $kurs = new KursPajakController();
        $request = new \Illuminate\Http\Request();
        $request->merge(['month' => 12]);
        $request->merge(['date' => 31]);
        $request->merge(['year' => Carbon::now('Asia/Jakarta')->subYears(1)->format('Y')]);
        $request->merge(['currency' => $code]);
        
        $result = json_decode($kurs->getKurs($request),true);

        if(!empty($result)){
            return $result;
        }

        return false;

    }

}
