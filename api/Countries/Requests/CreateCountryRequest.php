<?php

namespace Api\Countries\Requests;

use Infrastructure\Http\ApiRequest;

class CreateCountryRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'country' => 'array|required',
            'country.name' => 'required|string',
            'country.currency' => 'required|string',
            'country.currency_name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'country.name' => 'the country\'s name'
        ];
    }
}
