<?php

namespace Api\Countries\Events;

use Infrastructure\Events\Event;
use Api\Countries\Models\Country;

class CountryWasDeleted extends Event
{
    public $country;

    public function __construct(Country $country)
    {
        $this->country = $country;
    }
}
