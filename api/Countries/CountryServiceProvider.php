<?php

namespace Api\Countries;

use Infrastructure\Events\EventServiceProvider;
use Api\Countries\Events\CountryWasCreated;
use Api\Countries\Events\CountryWasDeleted;
use Api\Countries\Events\CountryWasUpdated;

class CountryServiceProvider extends EventServiceProvider
{
    protected $listen = [
        CountryWasCreated::class => [
            // listeners for when a country is created
        ],
        CountryWasDeleted::class => [
            // listeners for when a country is deleted
        ],
        CountryWasUpdated::class => [
            // listeners for when a country is updated
        ],
    ];
}
