<?php

namespace Api\Countries\Repositories;

use Api\Countries\Models\Country;
use Infrastructure\Database\Eloquent\Repository;

class CountryRepository extends Repository
{
    public function getModel()
    {
        return new Country();
    }

    public function create(array $data)
    {
        $country = $this->getModel();

        $country->fill($data);
        
        $country->save();

        return $country;
    }

    public function update(Country $country, array $data)
    {

        $country->fill($data);

        $country->save();

        return $country;
    }
}
