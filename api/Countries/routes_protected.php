<?php

$router->get('/api/countries', 'CountryController@getAll');
$router->get('/api/countries/{id}', 'CountryController@getById');
$router->post('/api/countries', 'CountryController@create');
$router->put('/api/countries/{id}', 'CountryController@update');
$router->delete('/api/countries/{id}', 'CountryController@delete');