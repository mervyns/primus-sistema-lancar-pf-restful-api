<?php

namespace Api\Countries\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Countries\Requests\CreateCountryRequest;
use Api\Countries\Services\CountryService;

class CountryController extends Controller
{
    private $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->countryService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'countries');

        return $this->response($parsedData);
    }

    public function getById($countryId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->countryService->getById($countryId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'country');

        return $this->response($parsedData);
    }

    public function create(CreateCountryRequest $request)
    {
        $data = $request->get('country', []);

        return $this->response($this->countryService->create($data), 201);
    }

    public function update($countryId, Request $request)
    {
        $data = $request->get('country', []);

        return $this->response($this->countryService->update($countryId, $data));
    }

    public function delete($countryId)
    {
        return $this->response($this->countryService->delete($countryId));
    }
}
