<?php

namespace Api\Countries\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CountryNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The country was not found.');
    }
}
