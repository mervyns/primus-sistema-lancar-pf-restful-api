<?php

namespace Api\Countries\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Countries\Exceptions\CountryNotFoundException;
use Api\Countries\Events\CountryWasCreated;
use Api\Countries\Events\CountryWasDeleted;
use Api\Countries\Events\CountryWasUpdated;
use Api\Countries\Repositories\CountryRepository;

class CountryService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $countryRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        CountryRepository $countryRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->countryRepository = $countryRepository;
    }

    public function getAll($options = [])
    {
        return $this->countryRepository->get($options);
    }

    public function getById($countryId, array $options = [])
    {
        $country = $this->getRequestedCountry($countryId);

        return $country;
    }

    public function create($data)
    {
        $country = $this->countryRepository->create($data);

        $this->dispatcher->fire(new CountryWasCreated($country));

        return $country;
    }

    public function update($countryId, array $data)
    {
        $country = $this->getRequestedCountry($countryId);

        $this->countryRepository->update($country, $data);

        $this->dispatcher->fire(new CountryWasUpdated($country));

        return $country;
    }

    public function delete($countryId)
    {
        $country = $this->getRequestedCountry($countryId);

        $this->countryRepository->delete($countryId);

        $this->dispatcher->fire(new CountryWasDeleted($country));

        return true;
    }

    private function getRequestedCountry($countryId)
    {
        $country = $this->countryRepository->getById($countryId);

        if (is_null($country)) {
            throw new CountryNotFoundException();
        }

        return $country;
    }
}
