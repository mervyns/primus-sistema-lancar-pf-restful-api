<?php

namespace Api\UserProducts\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserProductNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('User product was not found.');
    }
}
