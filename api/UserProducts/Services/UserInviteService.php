<?php

namespace Api\UserProducts\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserProducts\Exceptions\UserInviteNotFoundException;
use Api\UserProducts\Repositories\UserInviteRepository;
use Api\Users\Events\UserWasInvited;

class UserInviteService
{
    private $database;
    private $dispatcher;
    private $userInviteRepository;

    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, UserInviteRepository $userInviteRepository){
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userInviteRepository = $userInviteRepository;
    }

    public function getAll(array $options = array()){
        return $this->userInviteRepository->get($options);
    }

    public function getById(int $userInviteId, array $options = array()){
        $userInvite = $this->getRequestedUserInvite($userInviteId);
        return $userInvite;
    }

    public function create(array $data){
        $userInvite = $this->userInviteRepository->create($data['data']);
        $data['receiver']['id'] = $userInvite->id;
        $this->dispatcher->fire(new UserWasInvited($data['sender'], $data['receiver']));
        return $userInvite;
    }

    public function update(int $userInviteId, array $data){
        $userInvite = $this->getRequestedUserInvite($userInviteId);
        $this->userInviteRepository->update($userInvite, $data);
        return $userInvite;
    }

    public function delete(int $userInviteId){
        $userInvite = $this->getRequestedUserInvite($userInviteId);
        $this->userInviteRepository->delete($userInviteId);
        return $userInvite;
    }

    public function checkEmailInvited(string $email){
        $checkEmail = $this->userInviteRepository->checkEmailInvited($email);
        return $checkEmail;
    }

    private function getRequestedUserInvite(int $userInviteId){
        $userInvite = $this->userInviteRepository->getById($userInviteId);
        if (is_null($userInvite)){
            throw new UserInviteNotFoundException();
        }
        return $userInvite;
    }

}
