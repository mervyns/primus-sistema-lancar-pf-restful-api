<?php

namespace Api\UserProducts\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserProducts\Repositories\UserProductRepository;
use Api\UserProducts\Exceptions\UserProductNotFoundException;
use Api\UserProducts\Exceptions\UserInviteNotFoundException;
use Api\Users\Events\UserWasInvited;
use Api\Users\Events\UserWasSendSpouseNotification;

use Api\UserProducts\Models\UserProduct;
use Api\UserOrders\Models\UserOrder;

class UserProductService
{
    private $database;
    private $dispatcher;
    private $userProductRepository;

    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, UserProductRepository $userProductRepository){
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userProductRepository = $userProductRepository;
    }

    public function getAll(array $options = array()){
        return $this->userProductRepository->get($options);
    }

    public function getById(int $userProductId, array $options = array()){
        $userProduct = $this->getRequestedUserProduct($userProductId);
        return $userProduct;
    }

    // public function create(array $data, array $emailSender = array(), array $emailReceiver = array(), bool $invite = false){
    //     $userProduct = $this->userProductRepository->create($data);
    //     $emailReceiver['id'] = $userProduct->id;
    //     if ($invite == true){
    //         $this->dispatcher->fire(new UserWasInvited($emailSender, $emailReceiver));
    //     }
    //     return $userProduct;
    // }

    private function sendEmail(array $data, bool $toSpouse = false){
        $title = 'Undangan Mengisi SPTGO';
        $subtitle = 'Permintaan mengisi kewajiban pajak di SPTGO';
        $message = 'Pasangan anda mengajak untuk mengisi kewajiban perpajakan di SPTGO';

        $parameter = [
            'title'             => $title,
            'subtitle'          => $subtitle,
            'message'           => $message,
            'parent_id'         => $data['parent_id'],
            'user_parent_id'    => $data['user_parent_id'],
            'user_product_id'   => $data['id']
        ];

        $this->dispatcher->fire(new UserWasSendSpouseNotification($parameter));
    }

    public function create(array $data){
        $userProduct = UserProduct::insert($data);
        $userProduct = $this->userProductRepository->create($data);
        return $userProduct;
    }

    public function update(int $userProductId, array $data){
        $userProduct = $this->getRequestedUserProduct($userProductId);
        $this->userProductRepository->update($userProduct, $data);
        return $userProduct;
    }

    public function updateChild(int $userProductId, array $data){
        $userProduct = $this->userProductRepository->getWhere('parent_id', $userProductId);
        $this->userProductRepository->update($userProductId, $data);
        return $userProduct;
    }

    public function updateStatus($data){
        $this->userProductRepository->updateStatus($data);
        return true;
    }

    public function delete(int $userProductId){
        $userProduct = $this->getRequestedUserProduct($userProductId);
        $this->userProductRepository->delete($userProductId);
        return true;
    }

    public function deleteByParent(int $parent_id){
        return $this->userProductRepository->deleteByParent($parent_id);
    }

    public function userProductSubscription(int $userId){
        $productSubscription = $this->userProductRepository->userProductSubscription($userId);
        return $productSubscription;
    }

    public function checkEmail(string $email){
        $status = $this->userProductRepository->checkEmail($email);
        return $status;
    }

    private function getRequestedUserProduct(int $userProductId){
        $userProduct = $this->userProductRepository->getById($userProductId);
        if (is_null($userProduct)){
            throw new UserProductNotFoundException();
        }
        return $userProduct;
    }

    private function subscription(string $column, string $value) : array {
        $data = UserProduct::with('users')->with('product')->with('userOrder')
            ->where($column, $value)
            ->where(function ($query){
                $query->orWhereDate('expired_at', '<=', date('Y-m-d'));
                $query->orWhereNull('expired_at');
            })
            ->orderBy('expired_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->first();
        
        if (!is_null($data)) $data = $data->toArray();
        
        if (isset($data['user_order']['status']) && $data['user_order']['status'] == 'rejected'){
            $data = [];
        }

        return empty($data) ? [] : $data;
    }

    public function productSubscriptionExistingEmail(string $email){
        $data = $this->subscription('email', $email);
        return count($data) > 0 ? true : false;
    }

    public function productSubscription(string $column, string $value) : array{
        $data = $this->subscription($column, $value);
        if (count($data) > 0){
            $data['partner'] = null;
            if (empty($data['parent_id'])){
                $id = $data['id'];
                $partner = UserProduct::where('parent_id', $id)->get();
                if (!empty($partner)) $data['partner'] = $partner->toArray();
            }else{
                $parent_id = $data['parent_id'];
                $order = UserOrder::where('user_product_id', $parent_id)->first();
                if (!empty($order)) $data['user_order'] = $order->toArray();
            }
        }

        return $data;
    }

}
