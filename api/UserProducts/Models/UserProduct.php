<?php

namespace Api\UserProducts\Models;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    //
    protected $table = 'user_products';

    protected $fillable = [
        'user_id',
        'product_id',
        'type',
        'start_at',
        'expired_at',
        'user_parent_id',
        'years',
        'wife_or_husband',
        'promo',
        'promo_description',
        'parent_id',
        'email'
    ];

    public function users()
    {
        return $this->belongsTo('Api\Users\Models\User','user_id');
    }
    
    public function product(){
        return $this->belongsTo('Api\Products\Models\Product');
    }

    public function userOrder(){
        return $this->hasOne('Api\UserOrders\Models\UserOrder');
    }

}
