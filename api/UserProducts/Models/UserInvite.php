<?php

namespace Api\UserProducts\Models;

use Illuminate\Database\Eloquent\Model;

class UserInvite extends Model
{
    //
    protected $table = 'user_invites';

    protected $fillable = [
        'user_id',
        'product_id',
        'status',
        'wife_or_husband',
        'email'
    ];
    
    public function users()
    {
        return $this->belongsTo('Api\Users\Models\User');
    }

    // public function userProduct(){
    //     return $this->belongsTo('Api\UserProducts\Models\UserProduct');
    // }
    
}
