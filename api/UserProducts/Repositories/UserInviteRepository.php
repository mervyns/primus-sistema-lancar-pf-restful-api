<?php

namespace Api\UserProducts\Repositories;

use Illuminate\Support\Facades\DB;
use Api\UserProducts\Models\UserInvite;
use Api\UserProducts\Models\UserProduct;
use Api\Users\Models\User;
use Infrastructure\Database\Eloquent\Repository;

class UserInviteRepository extends Repository
{
    public function getModel()
    {
        return new UserInvite();
    }

    public function create(array $data){
    	$userInvite = $this->getModel();
    	$userInvite->fill($data);
    	$userInvite->save();
    	return $userInvite;
    }

    public function update(UserInvite $userInvite, array $data){
    	$userInvite->fill($data);
    	$userInvite->save();
    	return $userInvite;
    }
    
    public function checkEmailInvited(string $email) : array{
        $valRegister = false;
        $valInvited = false;
        $valMember = false;

        $result = array();

        $register = User::where('email', $email)->first();
        if (isset($register) == true) $valRegister = true;
        
        // $invited = UserInvite::where('email', $email)
        //     ->where('status', 'pending')
        //     ->first();
        // if (isset($invited) == true) $valInvited = true;
        
        // $member = UserProduct::join('users', 'user_products.user_id', 'users.id')->whereNull('user_parent_id')
        //     ->where('type', 'paid')
        //     ->whereNotNull('expired_at')
        //     ->whereDate('expired_at', '>=', date('Y-m-d'))
        //     ->first();
        // if (isset($member) == true) $valMember = true;

        $invited = UserProduct::where('email', $email)
            ->where('type', 'pending')
            ->whereNull('user_id')
            ->first();

        return array(
            'email' => $email,
            'status' => array(
                'register' => array('status' => $valRegister, 'message' => 'Email '. $email .' sudah terdaftar.'),
                'invited' => array('status' => $valInvited, 'message' => 'Email '. $email .' sudah diundang.'),
                'member' => array('status' => $valMember, 'message' => 'Email '. $email .' sudah memiliki produk.')
            )
        );
    }
}
