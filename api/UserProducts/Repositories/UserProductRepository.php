<?php

namespace Api\UserProducts\Repositories;

use Api\UserProducts\Models\UserProduct;
use Api\UserProducts\Models\UserInvite;
use Api\UserOrders\Models\UserOrder;
use Api\Users\Models\User;
use Infrastructure\Database\Eloquent\Repository;

class UserProductRepository extends Repository
{
    public function getModel()
    {
        return new UserProduct();
    }

    public function create(array $data){
    	$userProduct = $this->getModel();
    	$userProduct->fill($data);
    	$userProduct->save();
    	return $userProduct;
    }

    public function update(UserProduct $userProduct, array $data){
    	$userProduct->fill($data);
    	$userProduct->save();
    	return $userProduct;
    }

    public function deleteByParent($parent_id){
        $userProduct = $this->getModel();
        $delete = parent::deleteWhere('parent_id', $parent_id);
        return $delete;
    }

    public function updateStatus(int $id){
        $result = array();
        $status = false;

        $userProductParent = UserProduct::find($id);
        $userProductParent->type = "paid";
        $userProductParent->save();
        return true;
    }

    public function userProductSubscription(int $userId){
        $result['status'] = false;
        $result['data'] = [];
        $result['partners']  = [];

        $status_subscription = UserProduct::with('users')->with('product')->with('userOrder')
            ->where('user_id', $userId)
            ->where(function ($query){
                $query->orWhereDate('expired_at', '<=', date('Y-m-d'));
                $query->orWhereNull('expired_at');
            })
            ->orderBy('expired_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->first();

        $status_payment = (isset($status_subscription['data']->status)) ? $status_subscription['data']->status : '';
        
        if (!empty($status_subscription)){
            if ($status_payment != 'rejected'){
                $user_product_id = $status_subscription->id;
                $result['status'] = true;
                $result['data'] = $status_subscription;
                $result['partners'] = UserProduct::with('users')->with('product')->where('parent_id', $user_product_id)->get();
            }
        }

        return $result;
    }

    public function checkEmail(string $email){
        $status_subscription = UserProduct::where('email', $email)
            ->where(function ($query){
                $query->orWhereDate('expired_at', '<=', date('Y-m-d'));
                $query->orWhereNull('expired_at');
            })
            ->orderBy('expired_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->first();
        $result = true;

        if (is_null($status_subscription)){
            $result = false;
        }

        return $result;
    }

    // public function checkProductSubscription(int $userId){
    //     $status = false;
    //     $data = array();
    //     $dataOrder = array();
    //     $dataPartner = array();

    //     $productStatus = UserProduct::with('users')->with('product')
    //         ->where('user_id', $userId)
    //         ->where(function ($query){
    //             $query->orWhereDate('expired_at', '<=', date('Y-m-d'));
    //             $query->orWhereNull('expired_at');
    //         })
    //         ->orderBy('expired_at', 'desc')
    //         ->orderBy('created_at', 'desc')
    //         ->first();
        
    //     if (!is_null($productStatus)){
    //         $status = true;
    //         $idUserProductOrder = (empty($productStatus->parent_id)) ? $productStatus->id : $productStatus->parent_id;
    //         $checkOrder = UserOrder::with('bankAccounts')->with('bankAccounts.banks')
    //             ->where('user_product_id', $idUserProductOrder)
    //             ->first();
    //         $checkPartner = UserProduct::with('users')->with('product')
    //             ->where('parent_id', $productStatus->id)
    //             ->get();

    //         $data = $productStatus;
    //         $dataOrder = (empty($checkOrder)) ? array() : $checkOrder;
    //         $dataPartner = (empty($checkPartner)) ? array() : $checkPartner;
    //     }

    //     return array('status' => $status, 'data' => $data, 'order' => $dataOrder, 'partner' => $dataPartner);
    // }

    // public function checkUserProduct(int $userId) {
    //     $result = array();
    //     $status = true;
    //     $data = array();
    //     $partners = array();
    //     $orders = array();
        
    //     $productPaid = UserProduct::with('product')->with('users')
    //         ->where('user_id', $userId)->where('type', 'paid')->whereNotNull('expired_at')
    //         ->whereDate('expired_at', '>=', date('Y-m-d'))
    //         ->orderBy('expired_at', 'desc')->orderBy('user_parent_id', 'desc')->first();
        
    //     if (!empty($productPaid)){
    //         $data = $productPaid;

    //         $partners = UserInvite::where('user_id', $userId)
    //             ->where('product_id', $productPaid->product_id)
    //             ->where('created_at', '>=', date($productPaid->created_at))
    //             ->where('created_at', '<=', date($productPaid->expired_at))
    //              ->get();

    //         $orders = UserOrder::with('bankAccounts')->with('bankAccounts.banks')
    //             ->where('user_product_id', $productPaid->id)->first();

    //     }else{
    //         $data = UserProduct::with('product')->with('users')
    //             ->where('user_id', $userId)->where('type', 'pending')
    //             ->orderBy('created_at', 'desc')->first();

    //         if (empty($data)){
    //             $status = false;
    //         }else{
    //             $partners = UserInvite::where('user_id', $userId)
    //                 ->where('product_id', $data->product_id)
    //                 ->where('created_at', '>=', date($data->created_at))
    //                 ->get();
                    
    //             $orders = UserOrder::with('bankAccounts')->with('bankAccounts.banks')
    //                 ->where('user_product_id', $data->id)->first();
    //         }
    //     }

    //     $result = array(
    //         'status' => $status,
    //         'data' => $data,
    //         'partner' => $partners,
    //         'order' => $orders
    //     );

    //     return $result;

    // }

    // public function checkUserProductStatus(int $userId) : ?array{
    //     $valStatus = false;
    //     $valData = array();
    //     $valChild = array();

    //     $status = $this->getModel()->where('user_id', $userId)->get();
    //     if (isset($status) == true){
    //         $valStatus = true;
    //         $productPaid = $this->getModel()->where('user_id', $userId)
    //             ->where('type', 'paid')
    //             ->whereNotNull('expired_at')
    //             ->whereDate('expired_at', '>=', date('Y-m-d'))
    //             ->orderBy('expired_at', 'desc')
    //             ->first();
    //         if (isset($productPaid) == true){
    //             $valData = $productPaid;
    //             $child = $this->getModel()->where('user_parent_id', $userId)
    //                 ->where('type', 'paid')
    //                 ->whereNotNull('expired_at')
    //                 ->whereDate('expired_at', '>=', date('Y-m-d'))
    //                 ->orderBy('expired_at', 'desc')
    //                 ->get();
    //             $valChild = $child;
    //         }else{
    //             $productPending = $this->getModel()->where('user_id', $userId)
    //                 ->where('type', 'pending')
    //                 ->orderBy('created_at', 'desc')
    //                 ->first();
    //             if (isset($productPending) == true) {
    //                 $valData = $productPending;
    //                 $child = UserInvite::where('user_id', $userId)
    //                     ->where('status', 'pending')
    //                     ->get();
    //                 $valChild = $child;
    //             }
    //         }
    //     }

    //     $result = array(
    //         'status' => $valStatus,
    //         'data' => $valData,
    //         'partner' => $valChild
    //     );
    //     return $result;
    // }

    // public function userProductHistories(int $userId) : ?array{
    //     $histories = $this->getModel()->where('user_id', $userId)
    //         ->where('type', 'paid')
    //         ->get();
    //     return $histories;
    // }

    
}
