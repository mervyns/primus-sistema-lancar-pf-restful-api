<?php

namespace Api\TaxFilling\Models;

use Illuminate\Database\Eloquent\Model;

class UserTaxFillingStep extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'current_step', 'unfilled_fields', 'detail_step'
    ];
}
