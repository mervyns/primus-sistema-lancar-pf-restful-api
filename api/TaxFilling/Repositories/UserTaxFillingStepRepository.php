<?php

namespace Api\TaxFilling\Repositories;

use Api\Users\Repositories\UserRepository;
use Api\TaxFilling\Models\UserTaxFillingStep;
use Infrastructure\Database\Eloquent\Repository;

class UserTaxFillingStepRepository extends Repository
{
    public function getModel()
    {
        return new UserTaxFillingStep();
    }

    public function create(array $data)
    {
        $userStep = $this->getModel();

        $userStep->fill($data);
        
        $userStep->save();

        return $userStep;
    }

    public function update(UserTaxFillingStep $userStep, array $data)
    {

        $userStep->fill($data);

        $userStep->save();

        return $userStep;
    }

    public function saveOrUpdate($userId, array $data)
    {
        $steps = $data['current_step'];
        $userStep = $this->getModel()->where('user_id', '=', $userId)->first();
        
        $current_step = empty($userStep->current_step) ? 1 : $userStep->current_step;
        if ((int)$data['current_step'] < (int)$current_step) $data['current_step'] = $current_step;
        
        $detail_step = $this->parseDetailStep($userStep, $steps);

    	if (is_null($userStep)) {
    		$userStep = $this->getModel();
            $data['user_id'] = $userId;
        }
        $data['detail_step'] = json_encode($detail_step);

	    $userStep->fill($data);
        $userStep->save();
        
	    return $userStep;
    }

    private function parseDetailStep($data, $current_step) {
        $detail_step = empty($data->detail_step) ? [] : json_decode($data['detail_step'], true);
        $detail_step[$current_step] = true;

        return $detail_step;
    }

    public function setDirection($user_id,$step){
        try{
            $user = new UserRepository;
            $user = $user->getModel()->find($user_id);
            if(!empty($user->step->temp_steps)){
                $selected_steps = json_decode($user->step->temp_steps);
                if(!empty($selected_steps)){
                    $key = array_search($step, $selected_steps);
                    
                    if($key !== false){
                        $direction = [
                            'next' => $step,
                            'prev' => $step
                        ];
                        
                        while (key($selected_steps) !== $key){
                            if(!next($selected_steps)){
                                break;
                            }
                        } 
        
                        if(!empty(next($selected_steps))){
                            $direction['next'] = current($selected_steps);
                            reset($selected_steps);
                        }

                        while (key($selected_steps) !== $key){
                            if(!next($selected_steps)){
                                break;
                            }
                        }

                        if(!empty(prev($selected_steps))){
                            $direction['prev'] = current($selected_steps);
                            reset($selected_steps);
                        }
                    
                        config(['steps.'.$step.'.next' => $direction['next']]);
                        config(['steps.'.$step.'.previous' => $direction['prev']]);
                        config(['steps.'.$direction['next'].'.previous' => $step]);  
                    
                    }
                    
                }
            }
            return true;

        }catch(\Exception $e){
            dd($e);
        }
    }
}
