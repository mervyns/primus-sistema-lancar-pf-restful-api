<?php

namespace Api\Banks\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Banks\Requests\CreateBankRequest;
use Api\Banks\Services\BankService;

class BankController extends Controller
{
    private $bankService;

    public function __construct(BankService $bankService)
    {
        $this->bankService = $bankService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->bankService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'banks');

        return $this->response($parsedData);
    }

    public function getById($bankId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->bankService->getById($bankId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'bank');

        return $this->response($parsedData);
    }

    public function create(CreateBankRequest $request)
    {
        $data = $request->get('bank', []);

        return $this->response($this->bankService->create($data), 201);
    }

    public function update($bankId, Request $request)
    {
        $data = $request->get('bank', []);

        return $this->response($this->bankService->update($bankId, $data));
    }

    public function delete($bankId)
    {
        return $this->response($this->bankService->delete($bankId));
    }
}
