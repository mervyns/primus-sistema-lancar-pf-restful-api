<?php

namespace Api\Banks;

use Infrastructure\Events\EventServiceProvider;
use Api\Banks\Events\BankWasCreated;
use Api\Banks\Events\BankWasDeleted;
use Api\Banks\Events\BankWasUpdated;

class BankServiceProvider extends EventServiceProvider
{
    protected $listen = [
        BankWasCreated::class => [
            // listeners for when a bank is created
        ],
        BankWasDeleted::class => [
            // listeners for when a bank is deleted
        ],
        BankWasUpdated::class => [
            // listeners for when a bank is updated
        ],
    ];
}
