<?php

namespace Api\Banks\Repositories;

use Api\Banks\Models\Bank;
use Infrastructure\Database\Eloquent\Repository;

class BankRepository extends Repository
{
    public function getModel()
    {
        return new Bank();
    }

    public function create(array $data)
    {
        $bank = $this->getModel();

        $bank->fill($data);

        $bank->save();

        return $bank;
    }

    public function update(Bank $bank, array $data)
    {

        $bank->fill($data);

        $bank->save();

        return $bank;
    }
}
