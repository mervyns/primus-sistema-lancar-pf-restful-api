<?php

namespace Api\Banks\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Banks\Exceptions\BankNotFoundException;
use Api\Banks\Events\BankWasCreated;
use Api\Banks\Events\BankWasDeleted;
use Api\Banks\Events\BankWasUpdated;
use Api\Banks\Repositories\BankRepository;

class BankService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $bankRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        BankRepository $bankRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->bankRepository = $bankRepository;
    }

    public function getAll($options = [])
    {
        return $this->bankRepository->get($options);
    }

    public function getById($bankId, array $options = [])
    {
        $bank = $this->getRequestedBank($bankId);

        return $bank;
    }

    public function create($data)
    {
        $bank = $this->bankRepository->create($data);

        $this->dispatcher->fire(new BankWasCreated($bank));

        return $bank;
    }

    public function update($bankId, array $data)
    {
        $bank = $this->getRequestedBank($bankId);

        $this->bankRepository->update($bank, $data);

        $this->dispatcher->fire(new BankWasUpdated($bank));

        return $bank;
    }

    public function delete($bankId)
    {
        $bank = $this->getRequestedBank($bankId);

        $this->bankRepository->delete($bankId);

        $this->dispatcher->fire(new BankWasDeleted($bank));

        return true;
    }

    private function getRequestedBank($bankId)
    {
        $bank = $this->bankRepository->getById($bankId);

        if (is_null($bank)) {
            throw new BankNotFoundException();
        }

        return $bank;
    }
}
