<?php

namespace Api\Banks\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function bankAccount(){
        return $this->hasMany('Api\BankAccounts\Models\BankAccount');
    }
}
