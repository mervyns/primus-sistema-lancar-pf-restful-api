<?php

namespace Api\Banks\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BankNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The bank was not found.');
    }
}
