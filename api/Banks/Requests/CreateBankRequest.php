<?php

namespace Api\Banks\Requests;

use Infrastructure\Http\ApiRequest;

class CreateBankRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'bank' => 'array|required',
            'bank.name' => 'required|string',
        ];
    }


    public function attributes()
    {
        return [
            'bank.name' => 'the bank\'s name'
        ];
    }
}
