<?php

namespace Api\Banks\Events;

use Infrastructure\Events\Event;
use Api\Banks\Models\Bank;

class BankWasDeleted extends Event
{
    public $bank;

    public function __construct(Bank $bank)
    {
        $this->bank = $bank;
    }
}
