<?php

namespace Api\UserAdditionalProfiles\Models;

use Illuminate\Database\Eloquent\Model;

class UserAdditionalProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'detail'
    ];
}
