<?php

namespace Api\Banks;

use Infrastructure\Events\EventServiceProvider;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasCreated;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasDeleted;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasUpdated;

class UserAdditionalProfileServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserAdditionalProfileWasCreated::class => [
            // listeners for when an additional profile is created
        ],
        UserAdditionalProfileWasDeleted::class => [
            // listeners for when an additional profile is deleted
        ],
        UserAdditionalProfileWasUpdated::class => [
            // listeners for when an additional profile is updated
        ],
    ];
}
