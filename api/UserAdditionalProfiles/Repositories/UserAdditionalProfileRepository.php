<?php

namespace Api\UserAdditionalProfiles\Repositories;

use Api\UserAdditionalProfiles\Models\UserAdditionalProfile;
use Infrastructure\Database\Eloquent\Repository;

class UserAdditionalProfileRepository extends Repository
{
    public function getModel()
    {
        return new UserAdditionalProfile();
    }

    public function create(array $data)
    {
        $userAdditionalProfile = $this->getModel();

        $userAdditionalProfile->fill($data);

        $userAdditionalProfile->save();

        return $userAdditionalProfile;
    }

    public function update(UserAdditionalProfile $userAdditionalProfile, array $data)
    {

        $userAdditionalProfile->fill($data);

        $userAdditionalProfile->save();

        return $userAdditionalProfile;
    }
}
