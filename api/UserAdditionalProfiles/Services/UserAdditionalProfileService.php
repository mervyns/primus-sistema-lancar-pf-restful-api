<?php

namespace Api\UserAdditionalProfiles\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserAdditionalProfiles\Exceptions\UserAdditionalProfileNotFoundException;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasCreated;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasDeleted;
use Api\UserAdditionalProfiles\Events\UserAdditionalProfileWasUpdated;
use Api\UserAdditionalProfiles\Repositories\UserAdditionalProfileRepository;

class BankService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userAdditionalProfileRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserAdditionalProfileRepository $userAdditionalProfileRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->UserAdditionalProfileRepository = $userAdditionalProfileRepository;
    }

    public function getAll($options = [])
    {
        return $this->UserAdditionalProfileRepository->get($options);
    }

    public function getById($userAdditionalProfileId, array $options = [])
    {
        $userAdditionalProfile = $this->getRequestedUserAdditionalProfile($userAdditionalProfileId);

        return $userAdditionalProfile;
    }

    public function create($data)
    {
        $userAdditionalProfile = $this->UserAdditionalProfileRepository->create($data);

        $this->dispatcher->fire(new UserAdditionalProfileWasCreated($userAdditionalProfile));

        return $userAdditionalProfile;
    }

    public function update($userAdditionalProfileId, array $data)
    {
        $userAdditionalProfile = $this->getRequestedUserAdditionalProfile($userAdditionalProfileId);

        $this->UserAdditionalProfileRepository->update($userAdditionalProfile, $data);

        $this->dispatcher->fire(new UserAdditionalProfileWasUpdated($userAdditionalProfile));

        return $userAdditionalProfile;
    }

    public function delete($userAdditionalProfileId)
    {
        $userAdditionalProfile = $this->getRequestedUserAdditionalProfile($userAdditionalProfileId);

        $this->UserAdditionalProfileRepository->delete($userAdditionalProfileId);

        $this->dispatcher->fire(new UserAdditionalProfileWasDeleted($userAdditionalProfile));

        return true;
    }

    private function getRequestedUserAdditionalProfile($userAdditionalProfileId)
    {
        $userAdditionalProfile = $this->UserAdditionalProfileRepository->getById($userAdditionalProfileId);

        if (is_null($userAdditionalProfile)) {
            throw new UserAdditionalProfileNotFoundException();
        }

        return $userAdditionalProfile;
    }
}
