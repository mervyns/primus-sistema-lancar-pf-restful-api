<?php

namespace Api\UserAdditionalProfiles\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserAdditionalProfileRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'useradditionalprofile' => 'array|required',
            'useradditionalprofile.user_id' => 'required|integer',
            'useradditionalprofile.detail' => 'required|string',
        ];
    }


    public function attributes()
    {
        return [
            'useradditionalprofile.detail' => 'the user additional profile\'s detail'
        ];
    }
}
