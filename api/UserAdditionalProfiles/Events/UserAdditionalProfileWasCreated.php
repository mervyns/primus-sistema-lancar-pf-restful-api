<?php

namespace Api\UserAdditionalProfiles\Events;

use Infrastructure\Events\Event;
use Api\UserAdditionalProfiles\Models\UserAdditionalProfile;

class UserAdditionalProfileWasCreated extends Event
{
    public $userAdditionalProfile;

    public function __construct(UserAdditionalProfile $userAdditionalProfile)
    {
        $this->userAdditionalProfile = $userAdditionalProfile;
    }
}
