<?php

namespace Api\UserAdditionalProfiles\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserAdditionalProfileNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user additional profile was not found.');
    }
}
