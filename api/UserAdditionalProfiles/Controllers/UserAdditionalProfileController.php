<?php

namespace Api\UserAdditionalProfiles\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserAdditionalProfiles\Requests\CreateUserAdditionalProfileRequest;
use Api\UserAdditionalProfiles\Services\UserAdditionalProfileService;

class UserAdditionalProfileController extends Controller
{
    private $userAdditionalProfileService;

    public function __construct(UserAdditionalProfileService $userAdditionalProfileService)
    {
        $this->userAdditionalProfileService = $userAdditionalProfileService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userAdditionalProfileService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_additional_profiles');

        return $this->response($parsedData);
    }

    public function getById($userAdditionalProfileId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userAdditionalProfileService->getById($userAdditionalProfileId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_additional_profile');

        return $this->response($parsedData);
    }

    public function create(CreateUserAdditionalProfileRequest $request)
    {
        $data = $request->get('user_additional_profiles', []);

        return $this->response($this->userAdditionalProfileService->create($data), 201);
    }

    public function update($userAdditionalProfileId, Request $request)
    {
        $data = $request->get('user_additional_profiles', []);

        return $this->response($this->userAdditionalProfileService->update($userAdditionalProfileId, $data));
    }

    public function delete($userAdditionalProfileId)
    {
        return $this->response($this->userAdditionalProfileService->delete($userAdditionalProfileId));
    }
}
