<?php

namespace Api\Charities\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Charities\Requests\CreateCharityRequest;
use Api\Charities\Services\CharityService;

class CharityController extends Controller
{
    private $charityService;

    public function __construct(CharityService $charityService)
    {
        $this->charityService = $charityService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->charityService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'charities');

        return $this->response($parsedData);
    }

    public function getById($charityId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->charityService->getById($charityId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'charity');

        return $this->response($parsedData);
    }

    public function create(CreateCharityRequest $request)
    {
        $data = $request->get('charity', []);

        return $this->response($this->charityService->create($data), 201);
    }

    public function update($charityId, Request $request)
    {
        $data = $request->get('charity', []);

        return $this->response($this->charityService->update($charityId, $data));
    }

    public function delete($charityId)
    {
        return $this->response($this->charityService->delete($charityId));
    }
}
