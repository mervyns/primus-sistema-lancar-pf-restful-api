<?php

namespace Api\Charities\Events;

use Infrastructure\Events\Event;
use Api\Charities\Models\Charity;

class CharityWasUpdated extends Event
{
    public $charity;

    public function __construct(Charity $charity)
    {
        $this->charity = $charity;
    }
}
