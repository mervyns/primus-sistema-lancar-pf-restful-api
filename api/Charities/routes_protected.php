<?php

$router->get('/api/charities', 'CharityController@getAll');
$router->get('/api/charities/{id}', 'CharityController@getById');
$router->post('/api/charities', 'CharityController@create');
$router->put('/api/charities/{id}', 'CharityController@update');
$router->delete('/api/charities/{id}', 'CharityController@delete');