<?php

namespace Api\Charities\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Charities\Exceptions\CharityNotFoundException;
use Api\Charities\Events\CharityWasCreated;
use Api\Charities\Events\CharityWasDeleted;
use Api\Charities\Events\CharityWasUpdated;
use Api\Charities\Repositories\CharityRepository;

class CharityService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $charityRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        CharityRepository $charityRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->charityRepository = $charityRepository;
    }

    public function getAll($options = [])
    {
        return $this->charityRepository->get($options);
    }

    public function getById($charityId, array $options = [])
    {
        $charity = $this->getRequestedCharity($charityId);

        return $charity;
    }

    public function create($data)
    {
        $charity = $this->charityRepository->create($data);

        $this->dispatcher->fire(new CharityWasCreated($charity));

        return $charity;
    }

    public function update($charityId, array $data)
    {
        $charity = $this->getRequestedCharity($charityId);

        $this->charityRepository->update($charity, $data);

        $this->dispatcher->fire(new CharityWasUpdated($charity));

        return $charity;
    }

    public function delete($charityId)
    {
        $charity = $this->getRequestedCharity($charityId);

        $this->charityRepository->delete($charityId);

        $this->dispatcher->fire(new CharityWasDeleted($charity));

        return true;
    }

    private function getRequestedCharity($charityId)
    {
        $charity = $this->charityRepository->getById($charityId);

        if (is_null($charity)) {
            throw new CharityNotFoundException();
        }

        return $charity;
    }
}
