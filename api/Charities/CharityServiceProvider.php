<?php

namespace Api\Charities;

use Infrastructure\Events\EventServiceProvider;
use Api\Charities\Events\CharityWasCreated;
use Api\Charities\Events\CharityWasDeleted;
use Api\Charities\Events\CharityWasUpdated;

class CharityServiceProvider extends EventServiceProvider
{
    protected $listen = [
        CharityWasCreated::class => [
            // listeners for when a charity is created
        ],
        CharityWasDeleted::class => [
            // listeners for when a charity is deleted
        ],
        CharityWasUpdated::class => [
            // listeners for when a charity is updated
        ],
    ];
}
