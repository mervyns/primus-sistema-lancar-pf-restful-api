<?php

namespace Api\Charities\Repositories;

use Api\Charities\Models\Charity;
use Infrastructure\Database\Eloquent\Repository;

class CharityRepository extends Repository
{
    public function getModel()
    {
        return new Charity();
    }

    public function create(array $data)
    {
        $charity = $this->getModel();

        $charity->fill($data);
        
        $charity->save();

        return $charity;
    }

    public function update(Charity $charity, array $data)
    {

        $charity->fill($data);

        $charity->save();

        return $charity;
    }
}
