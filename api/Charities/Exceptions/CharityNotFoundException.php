<?php

namespace Api\Charities\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CharityNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The charity was not found.');
    }
}
