<?php

namespace Api\Charities\Requests;

use Infrastructure\Http\ApiRequest;

class CreateCharityRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'charity' => 'array|required',
            'charity.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'charity.name' => 'the charity\'s name'
        ];
    }
}
