<?php

namespace Api\Charities\Models;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
