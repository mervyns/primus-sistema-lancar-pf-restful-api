<?php

namespace Api\UserSecurityQuestions\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserSecurityQuestions\Exceptions\UserSecurityQuestionNotFoundException;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasCreated;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasDeleted;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasUpdated;
use Api\UserSecurityQuestions\Repositories\UserSecurityQuestionRepository;
use Api\Users\Repositories\UserRepository;
use Api\SecurityQuestions\Repositories\SecurityQuestionRepository;

class UserSecurityQuestionService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userSecurityQuestionRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserSecurityQuestionRepository $userSecurityQuestionRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userSecurityQuestionRepository = $userSecurityQuestionRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $securityQuestionRepository = new SecurityQuestionRepository();

        $userSecurityQuestion = $this->userSecurityQuestionRepository->get($options);

        $i = 0;
        foreach ($userSecurityQuestion as $usq) {
            $user = $userRepository->getById($usq['user_id']);
            $securityQuestion = $securityQuestionRepository->getById($usq['security_question_id']);

            $userSecurityQuestion[$i]['user_name'] = $user['name'];
            $userSecurityQuestion[$i]['security_question_name'] = $securityQuestion['name'];

            $i++;
        }

        return $userSecurityQuestion;
    }

    public function getById($userSecurityQuestionId, array $options = [])
    {
        $userRepository = new UserRepository();
        $securityQuestionRepository = new SecurityQuestionRepository();

        $userSecurityQuestion = $this->getRequestedUserSecurityQuestion($userSecurityQuestionId);

        $user = $userRepository->getById($userSecurityQuestion['user_id']);
        $securityQuestion = $securityQuestionRepository->getById($userSecurityQuestion['security_question_id']);

        $userSecurityQuestion['user_name'] = $user['name'];
        $userSecurityQuestion['security_question_name'] = $securityQuestion['name'];

        return $userSecurityQuestion;
    }

    public function create($data)
    {
        $userSecurityQuestion = $this->userSecurityQuestionRepository->create($data);

        $this->dispatcher->fire(new UserSecurityQuestionWasCreated($userSecurityQuestion));

        return $userSecurityQuestion;
    }

    public function update($userSecurityQuestionId, array $data)
    {
        $userSecurityQuestion = $this->getRequestedUserSecurityQuestion($userSecurityQuestionId);

        $this->userSecurityQuestionRepository->update($userSecurityQuestion, $data);

        $this->dispatcher->fire(new UserSecurityQuestionWasUpdated($userSecurityQuestion));

        return $userSecurityQuestion;
    }

    public function delete($userSecurityQuestionId)
    {
        $userSecurityQuestion = $this->getRequestedUserSecurityQuestion($userSecurityQuestionId);

        $this->userSecurityQuestionRepository->delete($userSecurityQuestionId);

        $this->dispatcher->fire(new UserSecurityQuestionWasDeleted($userSecurityQuestion));

        return true;
    }

    private function getRequestedUserSecurityQuestion($userSecurityQuestionId)
    {
        $userSecurityQuestion = $this->userSecurityQuestionRepository->getById($userSecurityQuestionId);

        if (is_null($userSecurityQuestion)) {
            throw new UserSecurityQuestionNotFoundException();
        }

        return $userSecurityQuestion;
    }
}
