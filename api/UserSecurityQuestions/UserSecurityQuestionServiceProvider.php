<?php

namespace Api\UserSecurityQuestions;

use Infrastructure\Events\EventServiceProvider;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasCreated;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasDeleted;
use Api\UserSecurityQuestions\Events\UserSecurityQuestionWasUpdated;

class UserSecurityQuestionServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserSecurityQuestionWasCreated::class => [
            // listeners for when a user security question is created
        ],
        UserSecurityQuestionWasDeleted::class => [
            // listeners for when a user security question is deleted
        ],
        UserSecurityQuestionWasUpdated::class => [
            // listeners for when a user security question is updated
        ],
    ];
}
