<?php

namespace Api\UserSecurityQuestions\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserSecurityQuestionNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user security question was not found.');
    }
}
