<?php

namespace Api\UserSecurityQuestions\Models;

use Illuminate\Database\Eloquent\Model;

class UserSecurityQuestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'security_question_id', 'answer'
    ];
}
