<?php

namespace Api\UserSecurityQuestions\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserSecurityQuestionRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_security_question' => 'array|required',
            'user_security_question.user_id' => 'required|integer',
            'user_security_question.security_question_id' => 'required|integer',
            'user_security_question.answer' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'user_security_question.answer' => 'the user\'s security question'
        ];
    }
}
