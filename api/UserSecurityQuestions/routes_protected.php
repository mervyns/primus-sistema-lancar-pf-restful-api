<?php

$router->get('/api/user-security-questions', 'UserSecurityQuestionController@getAll');
$router->get('/api/user-security-questions/{id}', 'UserSecurityQuestionController@getById');
$router->post('/api/user-security-questions', 'UserSecurityQuestionController@create');
$router->put('/api/user-security-questions/{id}', 'UserSecurityQuestionController@update');
$router->delete('/api/user-security-questions/{id}', 'UserSecurityQuestionController@delete');