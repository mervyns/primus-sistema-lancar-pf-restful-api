<?php

namespace Api\UserSecurityQuestions\Events;

use Infrastructure\Events\Event;
use Api\UserSecurityQuestions\Models\UserSecurityQuestion;

class UserSecurityQuestionWasDeleted extends Event
{
    public $userSecurityQuestion;

    public function __construct(UserSecurityQuestion $userSecurityQuestion)
    {
        $this->userSecurityQuestion = $userSecurityQuestion;
    }
}
