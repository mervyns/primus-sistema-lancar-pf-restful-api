<?php

namespace Api\UserSecurityQuestions\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserSecurityQuestions\Requests\CreateUserSecurityQuestionRequest;
use Api\UserSecurityQuestions\Services\UserSecurityQuestionService;

class UserSecurityQuestionController extends Controller
{
    private $userSecurityQuestionService;

    public function __construct(UserSecurityQuestionService $userSecurityQuestionService)
    {
        $this->userSecurityQuestionService = $userSecurityQuestionService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userSecurityQuestionService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_security_questions');

        return $this->response($parsedData);
    }

    public function getById($userSecurityQuestionId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userSecurityQuestionService->getById($userSecurityQuestionId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_security_question');

        return $this->response($parsedData);
    }

    public function create(CreateUserSecurityQuestionRequest $request)
    {
        $data = $request->get('user_security_question', []);

        return $this->response($this->userSecurityQuestionService->create($data), 201);
    }

    public function update($userSecurityQuestionId, Request $request)
    {
        $data = $request->get('user_security_question', []);

        return $this->response($this->userSecurityQuestionService->update($userSecurityQuestionId, $data));
    }

    public function delete($userSecurityQuestionId)
    {
        return $this->response($this->userSecurityQuestionService->delete($userSecurityQuestionId));
    }
}
