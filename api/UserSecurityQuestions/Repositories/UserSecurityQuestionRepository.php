<?php

namespace Api\UserSecurityQuestions\Repositories;

use Api\UserSecurityQuestions\Models\UserSecurityQuestion;
use Infrastructure\Database\Eloquent\Repository;

class UserSecurityQuestionRepository extends Repository
{
    public function getModel()
    {
        return new UserSecurityQuestion();
    }

    public function create(array $data)
    {
        $userSecurityQuestion = $this->getModel();

        $userSecurityQuestion->fill($data);
        
        $userSecurityQuestion->save();

        return $userSecurityQuestion;
    }

    public function update(UserSecurityQuestion $userSecurityQuestion, array $data)
    {

        $userSecurityQuestion->fill($data);

        $userSecurityQuestion->save();

        return $userSecurityQuestion;
    }
}
