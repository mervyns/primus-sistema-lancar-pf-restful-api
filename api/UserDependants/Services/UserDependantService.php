<?php

namespace Api\UserDependants\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserDependants\Exceptions\UserDependantNotFoundException;
use Api\UserDependants\Events\UserDependantWasCreated;
use Api\UserDependants\Events\UserDependantWasDeleted;
use Api\UserDependants\Events\UserDependantWasUpdated;
use Api\UserDependants\Repositories\UserDependantRepository;
use Api\Users\Repositories\UserRepository;
use Api\UserDependants\Repositories\DependantRelationRepository;

class UserDependantService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userDependantRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserDependantRepository $userDependantRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userDependantRepository = $userDependantRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $dependantRelationRepository = new DependantRelationRepository();

        $userDependant = $this->userDependantRepository->get($options);

        $i=0;
        foreach ($userDependant as $uD) {
            $user = $userRepository->getById($uD['user_id']);
            $dependantRelation = $dependantRelationRepository->getById($uD['dependant_relation_id']);

            $userDependant[$i]['user_name'] = $user['name'];
            $userDependant[$i]['dependant_relation_name'] = $dependantRelation['name'];

            $i++;
        }

        return $userDependant;
    }

    public function getById($userDependantId, array $options = [])
    {
        $userRepository = new UserRepository();
        $dependantRelationRepository = new DependantRelationRepository();

        $userDependant = $this->getRequestedUserDependant($userDependantId);

        $user = $userRepository->getById($userDependant['user_id']);
        $dependantRelation = $dependantRelationRepository->getById($userDependant['dependant_relation_id']);

        $userDependant['user_name'] = $user['name'];
        $userDependant['dependant_relation_name'] = $dependantRelation['name'];

        return $userDependant;
    }

    public function create($data)
    {
        $userDependant = $this->userDependantRepository->create($data);

        $this->dispatcher->fire(new UserDependantWasCreated($userDependant));

        return $userDependant;
    }

    public function update($userDependantId, array $data)
    {
        $userDependant = $this->getRequestedUserDependant($userDependantId);

        $this->userDependantRepository->update($userDependant, $data);

        $this->dispatcher->fire(new UserDependantWasUpdated($userDependant));

        return $userDependant;
    }

    public function delete($userDependantId)
    {
        $userDependant = $this->getRequestedUserDependant($userDependantId);

        $this->userDependantRepository->delete($userDependantId);

        $this->dispatcher->fire(new UserDependantWasDeleted($userDependant));

        return true;
    }

    private function getRequestedUserDependant($userDependantId)
    {
        $userDependant = $this->userDependantRepository->getById($userDependantId);

        if (is_null($userDependant)) {
            throw new DependantNotFoundException();
        }

        return $userDependant;
    }
}
