<?php

namespace Api\UserDependants\Repositories;

use Api\UserDependants\Models\UserDependant;
use Infrastructure\Database\Eloquent\Repository;

class UserDependantRepository extends Repository
{
    public function getModel()
    {
        return new UserDependant();
    }

    public function create(array $data)
    {
        $userDependant = $this->getModel();

        $userDependant->fill($data);
        
        $userDependant->save();

        return $userDependant;
    }

    public function update(UserDependant $userDependant, array $data)
    {
        $userDependant->fill($data);

        $userDependant->save();

        return $userDependant;
    }
}
