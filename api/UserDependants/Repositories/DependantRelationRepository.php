<?php

namespace Api\UserDependants\Repositories;

use Api\UserDependants\Models\DependantRelation;
use Infrastructure\Database\Eloquent\Repository;

class DependantRelationRepository extends Repository
{
    public function getModel()
    {
        return new DependantRelation();
    }

    public function create(array $data)
    {
        $dependantRelation = $this->getModel();

        $dependantRelation->fill($data);
        
        $dependantRelation->save();

        return $dependantRelation;
    }

    public function update(DependantRelation $dependantRelation, array $data)
    {

        $dependantRelation->fill($data);

        $dependantRelation->save();

        return $dependantRelation;
    }
}
