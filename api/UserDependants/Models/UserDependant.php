<?php

namespace Api\UserDependants\Models;

use Illuminate\Database\Eloquent\Model;

class UserDependant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','dependant_relation_id', 'ktp_number', 'name', 'job', 'not_have_ktp'
    ];

    public function dependants()
    {
        return $this->belongsTo('Api\UserDependants\Models\DependantRelation','dependant_relation_id');
    }
}
