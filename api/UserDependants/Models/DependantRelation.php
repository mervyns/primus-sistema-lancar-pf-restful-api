<?php

namespace Api\UserDependants\Models;

use Illuminate\Database\Eloquent\Model;

class DependantRelation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
