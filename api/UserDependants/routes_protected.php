<?php

$router->get('/api/user-dependants', 'UserDependantController@getAll');
$router->get('/api/user-dependants/{id}', 'UserDependantController@getById');
$router->post('/api/user-dependants', 'UserDependantController@create');
$router->put('/api/user-dependants/{id}', 'UserDependantController@update');
$router->delete('/api/user-dependants/{id}', 'UserDependantController@delete');

$router->get('/api/user-dependants/relations', 'DependantRelationController@getAll');
$router->get('/api/user-dependants/relations/{id}', 'DependantRelationController@getById');
$router->post('/api/user-dependants/relations', 'DependantRelationController@create');
$router->put('/api/user-dependants/relations/{id}', 'DependantRelationController@update');
$router->delete('/api/user-dependants/relations/{id}', 'DependantRelationController@delete');
