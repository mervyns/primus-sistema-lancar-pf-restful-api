<?php

namespace Api\UserDependants\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserDependantNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user\'s dependant was not found.');
    }
}
