<?php

namespace Api\UserDependants\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserDependantRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_dependant' => 'array|required',
            'user_dependant.user_id' => 'required|integer',
            'user_dependant.dependant_relation_id' => 'required|integer',
            'user_dependant.ktp_number' => 'string',
            'user_dependant.name' => 'string',
            'user_dependant.job' => 'integer'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_dependant.name' => 'the user dependant\'s name'
        ];
    }
}
