<?php

namespace Api\UserDependants\Requests;

use Infrastructure\Http\ApiRequest;

class CreateDependantRelationRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'dependant_relation' => 'array|required',
            'dependant_relation.name' => 'required|string'
        ];
    }

    
    public function attributes()
    {
        return [
            'dependant_relation.name' => 'the dependant relation\'s name'
        ];
    }
}
