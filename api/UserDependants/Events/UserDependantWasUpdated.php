<?php

namespace Api\UserDependants\Events;

use Infrastructure\Events\Event;
use Api\UserDependants\Models\UserDependant;

class UserDependantWasUpdated extends Event
{
    public $userDependant;

    public function __construct(UserDependant $userDependant)
    {
        $this->userDependant = $userDependant;
    }
}
