<?php

namespace Api\UserDependants\Events;

use Infrastructure\Events\Event;
use Api\UserDependants\Models\DependantRelation;

class DependantRelationWasUpdated extends Event
{
    public $dependantRelation;

    public function __construct(DependantRelation $dependantRelation)
    {
        $this->dependantRelation = $dependantRelation;
    }
}
