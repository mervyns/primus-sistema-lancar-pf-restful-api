<?php

namespace Api\UserDependants\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserDependants\Requests\CreateUserDependantRequest;
use Api\UserDependants\Services\UserDependantService;

class UserDependantController extends Controller
{
    private $userDependantService;

    public function __construct(UserDependantService $userDependantService)
    {
        $this->userDependantService = $userDependantService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userDependantService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_dependants');

        return $this->response($parsedData);
    }

    public function getById($userDependantId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userDependantService->getById($userDependantId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_dependant');

        return $this->response($parsedData);
    }

    public function create(CreateUserDependantRequest $request)
    {
        $data = $request->get('user_dependant', []);

        return $this->response($this->userDependantService->create($data), 201);
    }

    public function update($userDependantId, Request $request)
    {
        $data = $request->get('user_dependant', []);

        return $this->response($this->userDependantService->update($userDependantId, $data));
    }

    public function delete($userDependantId)
    {
        return $this->response($this->userDependantService->delete($userDependantId));
    }
}
