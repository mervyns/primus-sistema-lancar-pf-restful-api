<?php

namespace Api\UserDependants\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserDependants\Requests\CreateDependantRelationRequest;
use Api\UserDependants\Services\DependantRelationService;

class DependantRelationController extends Controller
{
    private $dependantRelationService;

    public function __construct(DependantRelationService $dependantRelationService)
    {
        $this->dependantRelationService = $dependantRelationService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->dependantRelationService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'dependant_relations');

        return $this->response($parsedData);
    }

    public function getById($dependantRelationId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->dependantRelationService->getById($dependantRelationId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'dependant_relation');

        return $this->response($parsedData);
    }

    public function create(CreateDependantRelationRequest $request)
    {
        $data = $request->get('dependant_relation', []);

        return $this->response($this->dependantRelationService->create($data), 201);
    }

    public function update($dependantRelationId, Request $request)
    {
        $data = $request->get('dependant_relation', []);

        return $this->response($this->dependantRelationService->update($dependantRelationId, $data));
    }

    public function delete($dependantRelationId)
    {
        return $this->response($this->dependantRelationService->delete($dependantRelationId));
    }
}
