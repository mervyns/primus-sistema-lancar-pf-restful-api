<?php

namespace Api\UserDependants;

use Infrastructure\Events\EventServiceProvider;
use Api\UserDependants\Events\UserDependantWasCreated;
use Api\UserDependants\Events\UserDependantWasDeleted;
use Api\UserDependants\Events\UserDependantWasUpdated;
use Api\UserDependants\Events\DependantRelationWasCreated;
use Api\UserDependants\Events\DependantRelationWasDeleted;
use Api\UserDependants\Events\DependantRelationWasUpdated;

class UserDependantServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserDependantWasCreated::class => [
            // listeners for when a user's dependant is created
        ],
        UserDependantWasDeleted::class => [
            // listeners for when a user's dependant is deleted
        ],
        UserDependantWasUpdated::class => [
            // listeners for when a user's dependant is updated
        ],
        DependantRelationWasCreated::class => [
            // listeners for when a dependant relation is created
        ],
        DependantRelationWasDeleted::class => [
            // listeners for when a dependant relation is deleted
        ],
        DependantRelationWasUpdated::class => [
            // listeners for when a dependant relation is updated
        ],
    ];
}
