<?php

namespace Api\PassFinal\Repositories;

use Api\PassFinal\Models\PassFinal;
use Infrastructure\Database\Eloquent\Repository;

class PassFinalRepository extends Repository
{
    public function getModel()
    {
        return new PassFinal();
    }

    public function create(array $data)
    {
        $passFinal = $this->getModel();

        $passFinal->fill($data);
        
        $passFinal->save();

        return $passFinal;
    }

    public function deletedata(array $data) {
        $delete = $this->getModel()->where('id', '=', $data['id'])->delete();
        return $delete;
    }

    public function update(array $data)
    {
        $id = $data['id'];
        unset($data['id']);
        unset($data['_token']);
        $delete = $this->getModel()->where('id', '=', $id)->update($data);
        return true;
    }

    public function saveOrUpdate($userId, array $data)
    {
    	// $userStep = $this->getModel()->where('user_id', '=', $userId)->first();

    	// if (is_null($userStep)) {
    	// 	$userStep = $this->getModel();

		//     $data['user_id'] = $userId;
	    // }

	    // $userStep->fill($data);
	    // $userStep->save();

	    // return $userStep;
    }
}
