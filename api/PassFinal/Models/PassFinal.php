<?php

namespace Api\PassFinal\Models;

use Illuminate\Database\Eloquent\Model;

class PassFinal extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pass_final';
    protected $fillable = [
        'asset_name', 'category', 'type', 'nominal', 'acquisition_year', 'assesment_document_number', 'country_id', 'address', 'name', 'NPWP', 'supporting_document', 'document_type', 'document_number', 'qty_assets', 'unit', 'detail', 'flag', 'acquisition_description'
    ];

    public function asset(){
        return $this->belongsTo('Api\Assets\Models\Asset', 'type');
    }

    public function country(){
        return $this->belongsTo('Api\Countries\Models\Country', 'country_id');
    }
}
