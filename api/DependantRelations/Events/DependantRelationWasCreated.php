<?php

namespace Api\DependantRelations\Events;

use Infrastructure\Events\Event;
use Api\DependantRelations\Models\DependantRelation;

class DependantRelationWasCreated extends Event
{
    public $dependantRelation;

    public function __construct(DependantRelation $dependantRelation)
    {
        $this->dependantRelation = $dependantRelation;
    }
}
