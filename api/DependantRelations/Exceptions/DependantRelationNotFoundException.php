<?php

namespace Api\DependantRelations\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DependantRelationNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The dependant relation was not found.');
    }
}
