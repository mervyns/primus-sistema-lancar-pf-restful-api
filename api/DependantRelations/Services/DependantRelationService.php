<?php

namespace Api\DependantRelations\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\DependantRelations\Exceptions\DependantRelationNotFoundException;
use Api\DependantRelations\Events\DependantRelationWasCreated;
use Api\DependantRelations\Events\DependantRelationWasDeleted;
use Api\DependantRelations\Events\DependantRelationWasUpdated;
use Api\DependantRelations\Repositories\DependantRelationRepository;

class DependantRelationService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $dependantRelationRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        DependantRelationRepository $dependantRelationRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->dependantRelationRepository = $dependantRelationRepository;
    }

    public function getAll($options = [])
    {
        return $this->dependantRelationRepository->get($options);
    }

    public function getById($dependantRelationId, array $options = [])
    {
        $dependantRelation = $this->getRequestedDependantRelation($dependantRelationId);

        return $dependantRelation;
    }

    public function create($data)
    {
        $dependantRelation = $this->dependantRelationRepository->create($data);

        $this->dispatcher->fire(new DependantRelationWasCreated($dependantRelation));

        return $dependantRelation;
    }

    public function update($dependantRelationId, array $data)
    {
        $dependantRelation = $this->getRequestedDependantRelation($dependantRelationId);

        $this->dependantRelationRepository->update($dependantRelation, $data);

        $this->dispatcher->fire(new DependantRelationWasUpdated($dependantRelation));

        return $dependantRelation;
    }

    public function delete($dependantRelationId)
    {
        $dependantRelation = $this->getRequestedDependantRelation($dependantRelationId);

        $this->dependantRelationRepository->delete($dependantRelationId);

        $this->dispatcher->fire(new DependantRelationWasDeleted($dependantRelation));

        return true;
    }

    private function getRequestedDependantRelation($dependantRelationId)
    {
        $dependantRelation = $this->dependantRelationRepository->getById($dependantRelationId);

        if (is_null($dependantRelation)) {
            throw new DependantRelationNotFoundException();
        }

        return $dependantRelation;
    }
}
