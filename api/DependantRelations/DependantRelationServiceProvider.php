<?php

namespace Api\Countries;

use Infrastructure\Events\EventServiceProvider;
use Api\DependantRelations\Events\DependantRelationWasCreated;
use Api\DependantRelations\Events\DependantRelationWasDeleted;
use Api\DependantRelations\Events\DependantRelationWasUpdated;

class DependantRelationServiceProvider extends EventServiceProvider
{
    protected $listen = [
        DependantRelationWasCreated::class => [
            // listeners for when a dependant relation is created
        ],
        DependantRelationWasDeleted::class => [
            // listeners for when a dependant relation is deleted
        ],
        DependantRelationWasUpdated::class => [
            // listeners for when a dependant relation is updated
        ],
    ];
}
