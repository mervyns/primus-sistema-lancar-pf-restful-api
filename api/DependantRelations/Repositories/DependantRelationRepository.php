<?php

namespace Api\DependantRelations\Repositories;

use Api\DependantRelations\Models\DependantRelation;
use Infrastructure\Database\Eloquent\Repository;

class DependantRelationRepository extends Repository
{
    public function getModel()
    {
        return new DependantRelation();
    }

    public function create(array $data)
    {
        $dependantRelation = $this->getModel();

        $dependantRelation->fill($data);
        
        $dependantRelation->save();

        return $dependantRelation;
    }

    public function update(DependantRelation $country, array $data)
    {

        $country->fill($data);

        $country->save();

        return $country;
    }
}
