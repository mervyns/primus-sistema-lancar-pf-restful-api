<?php

namespace Api\UserRevealedAssets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRevealedAssetNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user\'s revealed asset was not found.');
    }
}
