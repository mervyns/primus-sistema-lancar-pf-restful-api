<?php

namespace Api\UserRevealedAssets;

use Infrastructure\Events\EventServiceProvider;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasCreated;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasDeleted;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasUpdated;

class UserRevealedAssetServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserRevealedAssetWasCreated::class => [
            // listeners for when a user revealed asset is created
        ],
        UserRevealedAssetWasDeleted::class => [
            // listeners for when a user revealed asset is deleted
        ],
        UserRevealedAssetWasUpdated::class => [
            // listeners for when a user revealed asset is updated
        ],
    ];
}
