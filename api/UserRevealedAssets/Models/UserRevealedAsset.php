<?php

namespace Api\UserRevealedAssets\Models;

use Illuminate\Database\Eloquent\Model;

class UserRevealedAsset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'asset_id', 'country_id', 'value'
    ];
}
