<?php

namespace Api\UserRevealedAssets\Repositories;

use Api\UserRevealedAssets\Models\UserRevealedAsset;
use Infrastructure\Database\Eloquent\Repository;

class UserRevealedAssetRepository extends Repository
{
    public function getModel()
    {
        return new UserRevealedAsset();
    }

    public function create(array $data)
    {
        $userRevealedAsset = $this->getModel();

        $userRevealedAsset->fill($data);
        
        $userRevealedAsset->save();

        return $userRevealedAsset;
    }

    public function update(UserRevealedAsset $userRevealedAsset, array $data)
    {

        $userRevealedAsset->fill($data);

        $userRevealedAsset->save();

        return $userRevealedAsset;
    }
}
