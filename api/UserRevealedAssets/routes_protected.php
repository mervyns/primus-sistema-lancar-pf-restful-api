<?php

$router->get('/api/user-revealed-assets', 'UserRevealedAssetController@getAll');
$router->get('/api/user-revealed-assets/{id}', 'UserRevealedAssetController@getById');
$router->post('/api/user-revealed-assets', 'UserRevealedAssetController@create');
$router->post('/api/user-revealed-assets/multi', 'UserRevealedAssetController@multiCreate');
$router->put('/api/user-revealed-assets/{id}', 'UserRevealedAssetController@update');
$router->delete('/api/user-revealed-assets/{id}', 'UserRevealedAssetController@delete');