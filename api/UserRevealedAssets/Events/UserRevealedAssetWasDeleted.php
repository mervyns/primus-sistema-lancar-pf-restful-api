<?php

namespace Api\UserRevealedAssets\Events;

use Infrastructure\Events\Event;
use Api\UserRevealedAssets\Models\UserRevealedAsset;

class UserRevealedAssetWasDeleted extends Event
{
    public $userRevealedAsset;

    public function __construct(UserRevealedAsset $userRevealedAsset)
    {
        $this->userRevealedAsset = $userRevealedAsset;
    }
}
