<?php

namespace Api\UserRevealedAssets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserRevealedAssetRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_revealed_asset' => 'array|required',
            'user_revealed_asset.user_id' => 'required|integer',
            'user_revealed_asset.asset_id' => 'required|integer',
            'user_revealed_asset.country_id' => 'required|integer',
            'user_revealed_asset.value' => 'between:0,9999999999.99'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_revealed_asset.value' => 'the user revealed asset\'s value'
        ];
    }
}
