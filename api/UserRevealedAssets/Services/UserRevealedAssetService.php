<?php

namespace Api\UserRevealedAssets\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserRevealedAssets\Exceptions\UserRevealedAssetNotFoundException;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasCreated;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasDeleted;
use Api\UserRevealedAssets\Events\UserRevealedAssetWasUpdated;
use Api\UserRevealedAssets\Repositories\UserRevealedAssetRepository;
use Api\Users\Repositories\UserRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\Countries\Repositories\CountryRepository;

class UserRevealedAssetService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userRevealedAssetRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserRevealedAssetRepository $userRevealedAssetRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userRevealedAssetRepository = $userRevealedAssetRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();

        $userRevealedAsset = $this->userRevealedAssetRepository->get($options);

        $i=0;
        foreach ($userRevealedAsset as $uRA) {
            $user = $userRepository->getById($uRA['user_id']);
            $asset = $assetRepository->getById($uRA['asset_id']);
            $country = $countryRepository->getById($uRA['country_id']);

            $userRevealedAsset[$i]['user_name'] = $user['name'];
            $userRevealedAsset[$i]['asset_name'] = $asset['name'];
            $userRevealedAsset[$i]['country_name'] = $country['name'];

            $i++;
        }

        return $userRevealedAsset;
    }

    public function getById($userRevealedAssetId, array $options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();

        $userRevealedAsset = $this->getRequestedUserRevealedAsset($userRevealedAssetId);

        $user = $userRepository->getById($userRevealedAsset['user_id']);
        $asset = $assetRepository->getById($userRevealedAsset['asset_id']);
        $country = $countryRepository->getById($userRevealedAsset['country_id']);

        $userRevealedAsset['user_name'] = $user['name'];
        $userRevealedAsset['asset_name'] = $asset['name'];
        $userRevealedAsset['country_name'] = $country['name'];

        return $userRevealedAsset;
    }

    public function create($data)
    {
        $userRevealedAsset = $this->userRevealedAssetRepository->create($data);

        $this->dispatcher->fire(new UserRevealedAssetWasCreated($userRevealedAsset));

        return $userRevealedAsset;
    }

    public function update($userRevealedAssetId, array $data)
    {
        $userRevealedAsset = $this->getRequestedUserRevealedAsset($userRevealedAssetId);

        $this->userRevealedAssetRepository->update($userRevealedAsset, $data);

        $this->dispatcher->fire(new UserRevealedAssetWasUpdated($userRevealedAsset));

        return $userRevealedAsset;
    }

    public function delete($userRevealedAssetId)
    {
        $userRevealedAsset = $this->getRequestedUserRevealedAsset($userRevealedAssetId);

        $this->userRevealedAssetRepository->delete($userRevealedAssetId);

        $this->dispatcher->fire(new UserRevealedAssetWasDeleted($userRevealedAsset));

        return true;
    }

    private function getRequestedUserRevealedAsset($userRevealedAssetId)
    {
        $userRevealedAsset = $this->userRevealedAssetRepository->getById($userRevealedAssetId);

        if (is_null($userRevealedAsset)) {
            throw new UserRevealedAssetNotFoundException();
        }

        return $userRevealedAsset;
    }
}
