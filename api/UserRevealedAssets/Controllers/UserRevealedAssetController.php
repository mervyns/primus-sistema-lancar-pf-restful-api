<?php

namespace Api\UserRevealedAssets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserRevealedAssets\Requests\CreateUserRevealedAssetRequest;
use Api\UserRevealedAssets\Services\UserRevealedAssetService;

class UserRevealedAssetController extends Controller
{
    private $userRevealedAssetService;

    public function __construct(UserRevealedAssetService $userRevealedAssetService)
    {
        $this->userRevealedAssetService = $userRevealedAssetService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userRevealedAssetService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_revealed_assets');

        return $this->response($parsedData);
    }

    public function getById($userRevealedAssetId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userRevealedAssetService->getById($userRevealedAssetId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_revealed_asset');

        return $this->response($parsedData);
    }

    public function create(CreateUserRevealedAssetRequest $request)
    {
        $data = $request->get('user_revealed_asset', []);

        return $this->response($this->userRevealedAssetService->create($data), 201);
    }

    public function update($userRevealedAssetId, Request $request)
    {
        $data = $request->get('user_revealed_asset', []);

        return $this->response($this->userRevealedAssetService->update($userRevealedAssetId, $data));
    }

    public function delete($userRevealedAssetId)
    {
        return $this->response($this->userRevealedAssetService->delete($userRevealedAssetId));
    }
}
