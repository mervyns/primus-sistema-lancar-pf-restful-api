<?php

namespace Api\Products\Requests;

use Infrastructure\Http\ApiRequest;

class CreateProductRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product' => 'array|required',
            'product.name' => 'required|string',
            'product.description' => 'required|string',
            'product.invites' => 'required|integer',
            'product.price' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'product.name' => 'name',
            'product.description' => 'description',
            'product.invites' => 'invites',
            'product.price' => 'price',
        ];
    }
}
