<?php

namespace Api\Products\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Products\Exceptions\CharityNotFoundException;
use Api\Products\Events\ProductWasCreated;
use Api\Products\Events\ProductWasDeleted;
use Api\Products\Events\ProductWasUpdated;
use Api\Products\Repositories\ProductRepository;

class ProductService
{
    private $database;
    private $dispatcher;
    private $productRepository;

    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, ProductRepository $productRepository){
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->productRepository = $productRepository;
    }

    public function getAll(array $options = []){
        return $this->productRepository->get($options);
    }

    public function getById(int $productId, array $options = []){
        $product = $this->getRequestedProduct($productId);
        return $product;
    }

    public function create(array $data){
        $product = $this->productRepository->create($data);
        $this->dispatcher->fire(new ProductWasCreated($product));
        return $product;
    }

    public function update(int $productId, array $data){
        $product = $this->getRequestedProduct($productId);
        $this->productRepository->update($product, $data);
        $this->dispatcher->fire(new ProductWasUpdated($product));
        return $product;
    }

    public function delete(int $productId){
        $product = $this->getRequestedProduct($productId);
        $this->productRepository->delete($productId);
        $this->dispatcher->fire(new ProductWasDeleted($product));
        return true;
    }

    private function getRequestedProduct(int $productId)
    {
        $product = $this->productRepository->getById($productId);
        if (is_null($product)) {
            throw new ProductNotFoundException();
        }
        return $product;
    }
}
