<?php

namespace Api\Products\Events;

use Infrastructure\Events\Event;
use Api\Products\Models\Product;

class ProductWasUpdated extends Event
{
    public $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }
}
