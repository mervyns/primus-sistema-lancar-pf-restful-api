<?php

$router->get('/api/products', 'ProductController@getAll');
$router->get('/api/products/{id}', "ProductController@getById");
$router->post('/api/products', 'ProductController@create');
$router->put('/api/products/{id}', 'ProductController@update');
$router->delete('/api/products/{id}', 'ProductController@delete()');