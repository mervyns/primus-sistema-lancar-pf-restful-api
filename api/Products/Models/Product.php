<?php

namespace Api\Products\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";

    protected $fillable = [
        'name',
        'description',
        'invites',
        'price',
        'promo',
        'promo_description'
    ];

    // public function userProduct(){
    // 	return $this->hasMany('Api\UserProducts\Models\UserProduct');
    // }

}
