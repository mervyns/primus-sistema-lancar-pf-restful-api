<?php

namespace Api\UserForeignAssets\Models;

use Illuminate\Database\Eloquent\Model;

class UserForeignAsset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'asset_id', 'user_revealed_asset_id', 'country_id', 'acquisition_year', 'value', 'idr_value', 'detail', 'is_ta'
    ];
}
