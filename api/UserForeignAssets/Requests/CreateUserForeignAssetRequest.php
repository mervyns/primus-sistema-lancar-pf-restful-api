<?php

namespace Api\UserForeignAssets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserForeignAssetRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_foreign_asset' => 'array|required',
            'user_foreign_asset.user_id' => 'required|integer',
            'user_foreign_asset.asset_id' => 'required|integer',
            'user_foreign_asset.user_revealed_asset_id' => 'integer',
            'user_foreign_asset.country_id' => 'integer',
            'user_foreign_asset.acquisition_year' => 'required|integer',
            'user_foreign_asset.value' => 'required|between:0,9999999999.99',
            'user_foreign_asset.idr_value' => 'required|between:0,9999999999.99',
            'user_foreign_asset.detail' => 'string',
            'user_foreign_asset.is_ta' => 'integer',
        ];
    }

    
    public function attributes()
    {
        return [
            'user_foreign_asset.detail' => 'the user foreign asset\'s detail'
        ];
    }
}
