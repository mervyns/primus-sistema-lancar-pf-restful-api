<?php

namespace Api\UserForeignAssets\Events;

use Infrastructure\Events\Event;
use Api\UserForeignAssets\Models\UserForeignAsset;

class UserForeignAssetWasUpdated extends Event
{
    public $userForeignAsset;

    public function __construct(UserForeignAsset $userForeignAsset)
    {
        $this->userForeignAsset = $userForeignAsset;
    }
}
