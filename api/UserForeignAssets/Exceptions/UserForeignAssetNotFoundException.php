<?php

namespace Api\UserForeignAssets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserForeignAssetNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user\'s foreign asset was not found.');
    }
}
