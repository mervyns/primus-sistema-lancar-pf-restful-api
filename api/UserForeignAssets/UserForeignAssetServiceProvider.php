<?php

namespace Api\UserForeignAssets;

use Infrastructure\Events\EventServiceProvider;
use Api\UserForeignAssets\Events\UserForeignAssetWasCreated;
use Api\UserForeignAssets\Events\UserForeignAssetWasDeleted;
use Api\UserForeignAssets\Events\UserForeignAssetWasUpdated;

class UserAssetServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserForeignAssetWasCreated::class => [
            // listeners for when a user foreign asset is created
        ],
        UserForeignAssetWasDeleted::class => [
            // listeners for when a user foreign asset is deleted
        ],
        UserForeignAssetWasUpdated::class => [
            // listeners for when a user foreign asset is updated
        ],
    ];
}
