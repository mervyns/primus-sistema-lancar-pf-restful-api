<?php

namespace Api\UserForeignAssets\Repositories;

use Api\UserForeignAssets\Models\UserForeignAsset;
use Infrastructure\Database\Eloquent\Repository;

class UserForeignAssetRepository extends Repository
{
    public function getModel()
    {
        return new UserForeignAsset();
    }

    public function create(array $data)
    {
        $userForeignAsset = $this->getModel();

        $userForeignAsset->fill($data);
        
        $userForeignAsset->save();

        return $userForeignAsset;
    }

    public function update(UserForeignAsset $userForeignAsset, array $data)
    {

        $userForeignAsset->fill($data);

        $userForeignAsset->save();

        return $userForeignAsset;
    }
}
