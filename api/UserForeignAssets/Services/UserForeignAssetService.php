<?php

namespace Api\UserForeignAssets\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserForeignAssets\Exceptions\UserForeignAssetNotFoundException;
use Api\UserForeignAssets\Events\UserForeignAssetWasCreated;
use Api\UserForeignAssets\Events\UserForeignAssetWasDeleted;
use Api\UserForeignAssets\Events\UserForeignAssetWasUpdated;
use Api\UserForeignAssets\Repositories\UserForeignAssetRepository;
use Api\Users\Repositories\UserRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\Countries\Repositories\CountryRepository;

class UserForeignAssetService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userForeignAssetRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserForeignAssetRepository $userForeignAssetRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userForeignAssetRepository = $userForeignAssetRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();
        
        $userForeignAsset = $this->userForeignAssetRepository->get($options);

        $i=0;
        foreach ($userForeignAsset as $uFA) {
            $user = $userRepository->getById($uFA['user_id']);
            $asset = $assetRepository->getById($uFA['asset_id']);
            $country = $countryRepository->getById($uFA['country_id']);

            $userForeignAsset[$i]['user_name'] = $user['name'];
            $userForeignAsset[$i]['asset_name'] = $asset['name'];
            $userForeignAsset[$i]['country_name'] = $country['name'];
            $userForeignAsset[$i]['currency'] = $country['currency'];
            $userForeignAsset[$i]['currency_name'] = $country['currency_name'];

            $i++;
        }

        return $userForeignAsset;
    }

    public function getById($userForeignAssetId, array $options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();

        $userForeignAsset = $this->getRequestedUserAsset($userForeignAssetId);

        $user = $userRepository->getById($userForeignAsset['user_id']);
        $asset = $assetRepository->getById($userForeignAsset['asset_id']);
        $country = $countryRepository->getById($userForeignAsset['country_id']);

        $userForeignAsset['user_name'] = $user['name'];
        $userForeignAsset['asset_name'] = $asset['name'];
        $userForeignAsset['country_name'] = $country['name'];
        $userForeignAsset['currency'] = $country['currency'];
        $userForeignAsset['currency_name'] = $country['currency_name'];

        return $userForeignAsset;
    }

    public function create($data)
    {
        $userForeignAsset = $this->userForeignAssetRepository->create($data);

        $this->dispatcher->fire(new UserForeignAssetWasCreated($userForeignAsset));

        return $userForeignAsset;
    }

    public function update($userForeignAssetId, array $data)
    {
        $userForeignAsset = $this->getRequestedUserAsset($userForeignAssetId);

        $this->userForeignAssetRepository->update($userForeignAsset, $data);

        $this->dispatcher->fire(new UserForeignAssetWasUpdated($userForeignAsset));

        return $userForeignAsset;
    }

    public function delete($userForeignAssetId)
    {
        $userForeignAsset = $this->getRequestedUserAsset($userForeignAssetId);

        $this->userForeignAssetRepository->delete($userForeignAssetId);

        $this->dispatcher->fire(new UserForeignAssetWasDeleted($userForeignAsset));

        return true;
    }

    private function getRequestedUserAsset($userForeignAssetId)
    {
        $userForeignAsset = $this->userForeignAssetRepository->getById($userForeignAssetId);

        if (is_null($userForeignAsset)) {
            throw new UserForeignAssetNotFoundException();
        }

        return $userForeignAsset;
    }
}
