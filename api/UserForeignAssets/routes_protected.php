<?php

$router->get('/api/user-foreign-assets', 'UserForeignAssetController@getAll');
$router->get('/api/user-foreign-assets/{id}', 'UserForeignAssetController@getById');
$router->post('/api/user-foreign-assets', 'UserForeignAssetController@create');
$router->put('/api/user-foreign-assets/{id}', 'UserForeignAssetController@update');
$router->delete('/api/user-foreign-assets/{id}', 'UserForeignAssetController@delete');