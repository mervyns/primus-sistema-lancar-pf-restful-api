<?php

namespace Api\UserForeignAssets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserForeignAssets\Requests\CreateUserForeignAssetRequest;
use Api\UserForeignAssets\Services\UserForeignAssetService;

class UserForeignAssetController extends Controller
{
    private $userForeignAssetService;

    public function __construct(UserForeignAssetService $userForeignAssetService)
    {
        $this->userForeignAssetService = $userForeignAssetService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userForeignAssetService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_foreign_assets');

        return $this->response($parsedData);
    }

    public function getById($userForeignAssetId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userForeignAssetService->getById($userForeignAssetId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_foreign_asset');

        return $this->response($parsedData);
    }

    public function create(CreateUserForeignAssetRequest $request)
    {
        $data = $request->get('user_foreign_asset', []);

        return $this->response($this->userForeignAssetService->create($data), 201);
    }

    public function update($userForeignAssetId, Request $request)
    {
        $data = $request->get('user_foreign_asset', []);

        return $this->response($this->userForeignAssetService->update($userForeignAssetId, $data));
    }

    public function delete($userForeignAssetId)
    {
        return $this->response($this->userForeignAssetService->delete($userForeignAssetId));
    }
}
