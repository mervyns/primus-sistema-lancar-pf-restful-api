<?php

namespace Api\UserInvites\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserInviteNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('User Invite was not found.');
    }
}
