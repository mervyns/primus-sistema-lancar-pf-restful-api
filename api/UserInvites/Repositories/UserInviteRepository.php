<?php

namespace Api\UserInvites\Repositories;

use Api\UserInvites\Models\UserInvite;
use Api\UserProducts\Models\UserProduct;
use Api\Users\Models\User;

use Infrastructure\Database\Eloquent\Repository;

class UserInviteRepository extends Repository
{
    public function getModel()
    {
        return new UserInvite();
    }

    public function create(array $data){
    	$userInvite = $this->getModel();
    	$userInvite->fill($data);
    	$userInvite->save();
    	return $userInvite;
    }

    public function update(UserInvite $userInvite, array $data){
    	$userInvite->fill($data);
    	$userInvite->save();
    	return $userInvite;
    }

    

}
