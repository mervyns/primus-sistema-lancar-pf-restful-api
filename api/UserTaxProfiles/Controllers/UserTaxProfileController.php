<?php

namespace Api\UserTaxProfiles\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserTaxProfiles\Requests\CreateUserTaxProfileRequest;
use Api\UserTaxProfiles\Services\UserTaxProfileService;

class UserTaxProfileController extends Controller
{
    private $userTaxProfileService;

    public function __construct(UserTaxProfileService $userTaxProfileService)
    {
        $this->userTaxProfileService = $userTaxProfileService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userTaxProfileService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_tax_profiles');

        return $this->response($parsedData);
    }

    public function getById($userTaxProfileId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userTaxProfileService->getById($userTaxProfileId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_tax_profile');

        return $this->response($parsedData);
    }

    public function create(CreateUserTaxProfileRequest $request)
    {
        $data = $request->get('user_tax_profile', []);

        return $this->response($this->userTaxProfileService->create($data), 201);
    }

    public function update($userTaxProfileId, Request $request)
    {
        $data = $request->get('user_tax_profile', []);

        return $this->response($this->userTaxProfileService->update($userTaxProfileId, $data));
    }

    public function delete($userTaxProfileId)
    {
        return $this->response($this->userTaxProfileService->delete($userTaxProfileId));
    }
}
