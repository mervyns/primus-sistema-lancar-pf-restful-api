<?php

namespace Api\UserTaxProfiles\Events;

use Infrastructure\Events\Event;
use Api\UserTaxProfiles\Models\UserTaxProfile;

class UserTaxProfileWasDeleted extends Event
{
    public $userTaxProfile;

    public function __construct(UserTaxProfile $userTaxProfile)
    {
        $this->userTaxProfile = $userTaxProfile;
    }
}
