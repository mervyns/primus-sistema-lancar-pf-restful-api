<?php

namespace Api\UserTaxProfiles\Events;

use Infrastructure\Events\Event;
use Api\UserTaxProfiles\Models\UserTaxProfile;

class UserTaxProfileWasUpdated extends Event
{
    public $userTaxProfile;

    public function __construct(UserTaxProfile $userTaxProfile)
    {
        $this->userTaxProfile = $userTaxProfile;
    }
}
