<?php

namespace Api\UserTaxProfiles\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserTaxProfileRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_tax_profile' => 'array|required',
            'user_tax_profile.user_id' => 'required|integer',
            'user_tax_profile.nik' => 'numeric',
            'user_tax_profile.npwp' => 'numeric',
            'user_tax_profile.efin' => 'string',
            'user_tax_profile.job_status_id' => 'integer',
            'user_tax_profile.klu_id' => 'integer',
            'user_tax_profile.have_dependant' => 'integer',
            'user_tax_profile.have_vehicle' => 'integer',
            'user_tax_profile.have_property' => 'integer',
            'user_tax_profile.have_insurance' => 'integer',
            'user_tax_profile.have_foreign_asset' => 'integer',
            'user_tax_profile.following_tax_amnesty_2016' => 'integer',
            'user_tax_profile.companion_has_npwp' => 'integer',
            'user_tax_profile.tax_responsibility_status_id' => 'integer'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_tax_profile.nik' => 'the user\'s NIK'
        ];
    }
}
