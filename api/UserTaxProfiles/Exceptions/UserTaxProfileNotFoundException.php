<?php

namespace Api\UserTaxProfiles\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserTaxProfileNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user tax profile was not found.');
    }
}
