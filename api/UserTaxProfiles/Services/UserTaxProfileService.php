<?php

namespace Api\UserTaxProfiles\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserTaxProfiles\Exceptions\UserTaxProfileNotFoundException;
use Api\UserTaxProfiles\Events\UserTaxProfileWasCreated;
use Api\UserTaxProfiles\Events\UserTaxProfileWasDeleted;
use Api\UserTaxProfiles\Events\UserTaxProfileWasUpdated;
use Api\UserTaxProfiles\Repositories\UserTaxProfileRepository;
use Api\Users\Repositories\UserRepository;
use Api\KlasifikasiLapanganUsaha\Repositories\KlasifikasiLapanganUsahaRepository;
use Api\TaxResponsibilityStatuses\Repositories\TaxResponsibilityStatusRepository;

class UserTaxProfileService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userTaxProfileRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserTaxProfileRepository $userTaxProfileRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userTaxProfileRepository = $userTaxProfileRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $kluRepository = new KlasifikasiLapanganUsahaRepository();
        $taxResponsibilityStatusRepository = new TaxResponsibilityStatusRepository();


        $userTaxProfile = $this->userTaxProfileRepository->get($options);

        $i=0;
        foreach ($userTaxProfile as $uTP) {
            $user = $userRepository->getById($uTP['user_id']);
            $klu = $kluRepository->getById($uTP['klu_id']);
            $taxResponsibilityStatus = $taxResponsibilityStatusRepository->getById($uTP['tax_responsibility_status_id']);

            $userTaxProfile[$i]['user_name'] = $user['name'];
            $userTaxProfile[$i]['klu_name'] = $klu['name'];
            $userTaxProfile[$i]['tax_responsibility_status_code'] = $taxResponsibilityStatus['code'];
            $userTaxProfile[$i]['tax_responsibility_status_name'] = $taxResponsibilityStatus['name'];

            $i++;
        }

        return $userTaxProfile;
    }

    public function getById($userTaxProfileId, array $options = [])
    {
        $userRepository = new UserRepository();
        $kluRepository = new KlasifikasiLapanganUsahaRepository();
        $taxResponsibilityStatusRepository = new TaxResponsibilityStatusRepository();

        $userTaxProfile = $this->getRequestedUserTaxProfile($userTaxProfileId);

        $user = $userRepository->getById($userTaxProfile['user_id']);
        $klu = $kluRepository->getById($userTaxProfile['klu_id']);
        $taxResponsibilityStatus = $taxResponsibilityStatusRepository->getById($userTaxProfile['tax_responsibility_status_id']);

        $userTaxProfile['user_name'] = $user['name'];
        $userTaxProfile['klu_name'] = $klu['name'];
        $userTaxProfile['tax_responsibility_status_code'] = $taxResponsibilityStatus['code'];
        $userTaxProfile['tax_responsibility_status_name'] = $taxResponsibilityStatus['name'];

        return $userTaxProfile;
    }

    public function create($data)
    {
        $userTaxProfile = $this->userTaxProfileRepository->create($data);

        $this->dispatcher->fire(new UserTaxProfileWasCreated($userTaxProfile));

        return $userTaxProfile;
    }

    public function update($userTaxProfileId, array $data)
    {
        $userTaxProfile = $this->getRequestedUserTaxProfile($userTaxProfileId);

        $this->userTaxProfileRepository->update($userTaxProfile, $data);

        $this->dispatcher->fire(new UserTaxProfileWasUpdated($userTaxProfile));

        return $userTaxProfile;
    }

    public function delete($userTaxProfileId)
    {
        $userTaxProfile = $this->getRequestedUserTaxProfile($userTaxProfileId);

        $this->userTaxProfileRepository->delete($userTaxProfileId);

        $this->dispatcher->fire(new UserTaxProfileWasDeleted($userTaxProfile));

        return true;
    }

    private function getRequestedUserTaxProfile($userTaxProfileId)
    {
        $userTaxProfile = $this->userTaxProfileRepository->getById($userTaxProfileId);

        if (is_null($userTaxProfile)) {
            throw new UserTaxProfileNotFoundException();
        }

        return $userTaxProfile;
    }
}
