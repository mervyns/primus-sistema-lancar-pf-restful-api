<?php

namespace Api\UserTaxProfiles;

use Infrastructure\Events\EventServiceProvider;
use Api\UserTaxProfiles\Events\UserTaxProfileWasCreated;
use Api\UserTaxProfiles\Events\UserTaxProfileWasDeleted;
use Api\UserTaxProfiles\Events\UserTaxProfileWasUpdated;

class UserTaxProfileServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserTaxProfileWasCreated::class => [
            // listeners for when a user tax profile is created
        ],
        UserTaxProfileWasDeleted::class => [
            // listeners for when a user tax profile is deleted
        ],
        UserTaxProfileWasUpdated::class => [
            // listeners for when a user tax profile is updated
        ],
    ];
}
