<?php

$router->get('/api/user-tax-profiles', 'UserTaxProfileController@getAll');
$router->get('/api/user-tax-profiles/{id}', 'UserTaxProfileController@getById');
$router->post('/api/user-tax-profiles', 'UserTaxProfileController@create');
$router->put('/api/user-tax-profiles/{id}', 'UserTaxProfileController@update');
$router->delete('/api/user-tax-profiles/{id}', 'UserTaxProfileController@delete');