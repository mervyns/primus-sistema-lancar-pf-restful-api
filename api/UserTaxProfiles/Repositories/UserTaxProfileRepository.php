<?php

namespace Api\UserTaxProfiles\Repositories;

use Api\UserTaxProfiles\Models\UserTaxProfile;
use Infrastructure\Database\Eloquent\Repository;

class UserTaxProfileRepository extends Repository
{
    public function getModel()
    {
        return new UserTaxProfile();
    }

    public function create(array $data)
    {
        $userTaxProfile = $this->getModel();

        $userTaxProfile->fill($data);
        
        $userTaxProfile->save();

        return $userTaxProfile;
    }

    public function update(UserTaxProfile $userTaxProfile, array $data)
    {

        $userTaxProfile->fill($data);

        $userTaxProfile->save();

        return $userTaxProfile;
    }
}
