<?php

namespace Api\UserTaxProfiles\Models;

use Illuminate\Database\Eloquent\Model;

class UserTaxProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'nik', 'npwp','efin', 'job_status_id', 'klu_id', 'marital_status', 'have_dependant', 'have_vehicle', 'have_property', 'have_insurance', 'have_foreign_asset', 'has_jewelry', 'following_tax_amnesty_2016', 'tax_responsibility_status_id', 'companion_has_npwp'
    ];

    public function klus()
    {
        return $this->belongsTo('Api\KlasifikasiLapanganUsaha\Models\KlasifikasiLapanganUsaha','klu_id');
    }

}
