<?php

namespace Api\UserOrders\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserOrderNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('User order was not found.');
    }
}
