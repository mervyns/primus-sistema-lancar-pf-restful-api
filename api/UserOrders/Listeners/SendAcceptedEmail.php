<?php

namespace Api\UserOrders\Listeners;

use Log;
use Mail;
use Api\UserOrders\Events\UserOrderWasAccepted;

class SendAcceptedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserOrderWasAccepted $userWasAccepted)
    {
        $order = $userWasAccepted->userOrder;
        Mail::send('emails.user_order_response', [
            'message' => $order['message']
        ], function($message) use ($order) {
            $message->from('no-reply@sptgo.com');
            $message->to($order['email']);
            $message->subject("Order Status");
        });

    }
}
