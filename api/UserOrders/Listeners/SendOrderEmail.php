<?php

namespace Api\UserOrders\Listeners;

use Log;
use Mail;
use Api\UserOrders\Events\UserWasOrder;

class SendOrderEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserWasOrder $userWasOrder)
    {
        $user = $userWasOrder->userOrder;
        Log::info("Order user with ID {$user->id} was proccessed");

        Mail::send('emails.user_order_confirmation', [
            'email' => $user->email,
            'id' => $user->id
        ], function($message) use ($user) {
            $message->from('no-reply@sptgo.com');
            $message->to($user->email);
            $message->subject("Order Success");
        });

    }
}
