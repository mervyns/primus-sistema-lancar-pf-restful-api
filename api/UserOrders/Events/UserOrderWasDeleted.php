<?php

namespace Api\UserOrders\Events;

use Infrastructure\Events\Event;
use Api\UserOrders\Models\UserOrder;

class UserOrderWasDeleted extends Event
{
    public $userOrder;

    public function __construct(UserOrder $userOrder)
    {
        $this->userOrder = $userOrder;
    }
}
