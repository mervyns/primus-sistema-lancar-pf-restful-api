<?php

namespace Api\UserOrders\Events;
use Infrastructure\Events\Event;
use Api\UserOrders\Models\UserOrder;
class UserOrderWasAccepted extends Event
{
    public $userOrder;
    public function __construct(array $userOrder)
    {
        $this->userOrder = $userOrder;
    }
}
