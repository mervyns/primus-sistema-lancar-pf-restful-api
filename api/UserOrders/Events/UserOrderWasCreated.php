<?php

namespace Api\UserOrders\Events;

use Infrastructure\Events\Event;
use Api\UserOrders\Models\UserOrder;

class UserOrderWasCreated extends Event
{
    public $userOrder;

    public function __construct(UserOrder $userOrder)
    {
        $this->userOrder = $userOrder;
    }
}
