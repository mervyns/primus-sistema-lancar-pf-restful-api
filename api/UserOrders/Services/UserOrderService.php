<?php

namespace Api\UserOrders\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserOrders\Repositories\UserOrderRepository;
use Api\UserOrders\Exceptions\UserOrderNotFoundException;
use Api\UserOrders\Models\UserOrder;
use Api\UserOrders\Events\UserOrderWasAccepted;


class UserOrderService
{
    private $database;
    private $dispatcher;
    private $userOrderRepository;

    public function __construct(DatabaseManager $database, Dispatcher $dispatcher, UserOrderRepository $userOrderRepository){
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userOrderRepository = $userOrderRepository;
    }

    public function getAll(array $options = array()){
        $dataResult = array();
        $userOrders = $this->userOrderRepository->get($options);
        foreach ($userOrders as $userOrder){
            $data = $userOrder;
            $data->bankAccounts = $userOrder->bankAccounts;
            $data->userProducts = $userOrder->userProducts;
            $data->banks = $userOrder->bankAccounts->banks;
            $data->users = $userOrder->userProducts->users;
            $dataResult[] = $data;
        }
        return $dataResult;
    }

    public function getById(int $userOrderId, array $options = array()){
        $dataResult = UserOrder::with('bankAccounts')
            ->with('userProducts')
            ->with('bankAccounts.banks')
            ->with('userProducts.users')
            ->with('userProducts.product')
            ->find($userOrderId);
        return $dataResult;
    }

    public function create(array $data){
        $userOrder = $this->userOrderRepository->create($data);
        return $userOrder;
    }

    public function update(int $userOrderId, array $data, array $paramEmail = array()){
        $userOrder = $this->getRequestUserOrder($userOrderId);
        $this->userOrderRepository->update($userOrder, $data);
        $this->dispatcher->fire(new UserOrderWasAccepted($paramEmail));
        return $userOrder;
    }

    public function delete(int $userOrderId){
        $userOrder = $this->getRequestUserOrder($userOrderId);
        $this->userOrderRepository->delete($userOrderId);
        return true;
    }

    private function getRequestUserOrder(int $userOrderId){
        $userOrder = $this->userOrderRepository->getById($userOrderId);
        if (is_null($userOrder)){
            throw new UserOrderNotFoundException();
        }
        return $userOrder;
    }

    public function Invoice(){
        $result = $this->userOrderRepository->Invoice();
        return $result;
    }

}
