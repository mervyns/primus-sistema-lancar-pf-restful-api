<?php
 
 namespace Api\UserOrders\Repositories;

use Illuminate\Support\Facades\DB;
use Api\Users\Models\User;
use Api\UserOrders\Models\UserOrder;
use Infrastructure\Database\Eloquent\Repository;

class UserOrderRepository extends Repository{
	public function getModel(){
		return new UserOrder();
	}

	public function create(array $data){
    	$userOrder = $this->getModel();
    	$userOrder->fill($data);
    	$userOrder->save();
    	return $userOrder;
	}

	public function update(UserOrder $userOrder, array $data){
		$userOrder->fill($data);
    	$userOrder->save();
    	return $userOrder;
	}

	public function Invoice(){
		$year = date('Y');
		$month = date('m');
		$increment = 1;
		$result;

		$lastInvoice = UserOrder::whereRaw('SUBSTRING(invoice, 1, 4) = ?', [$year.$month])
			->orderBy('invoice', 'desc')
			->first();
		
		if (empty($lastInvoice->invoice)){
			$result = $year.$month.'-'.str_pad($increment, 4, '0', STR_PAD_LEFT );
		}else{
			$split = explode('-', $lastInvoice->invoice);
			$increment = ((int)$split[1]) + 1;
			$result = $year.$month.'-'.str_pad($increment, 4, '0', STR_PAD_LEFT );
		}
		return $result;
	}


}