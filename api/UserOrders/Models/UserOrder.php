<?php

namespace Api\UserOrders\Models;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{
    //
    protected $table = 'user_orders';

    protected $fillable = [
    	'user_product_id',
    	'bank_account_id',
    	'amount',
    	'total_amount',
    	'promo',
    	'promo_description',
    	'status',
    	'attachment',
    	'unique_code'
    ];
    
    public function bankAccounts(){
    	return $this->belongsTo('Api\BankAccounts\Models\BankAccount', 'bank_account_id');
    }
    
    public function userProducts(){
        return $this->belongsTo('Api\UserProducts\Models\UserProduct', 'user_product_id');
    }
}
