<?php

namespace Api\Tooltips\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Tooltips\Exceptions\TooltipNotFoundException;
use Api\Tooltips\Events\TooltipWasCreated;
use Api\Tooltips\Events\TooltipWasDeleted;
use Api\Tooltips\Events\TooltipWasUpdated;
use Api\Tooltips\Repositories\TooltipRepository;
use Api\Tooltips\Models\Tooltips;

class TooltipService
{
    private $database;
    private $dispatcher;
    private $tooltipRepository;

    public function __construct(
    	DatabaseManager $database, 
    	Dispatcher $dispatcher, 
    	TooltipRepository $tooltipRepository){
    	$this->database = $database;
    	$this->dispatcher = $dispatcher;
    	$this->tooltipRepository = $tooltipRepository;
    }

    public function getAll(array $options = array()){
        $tooltipsData = array();
        $tooltips = $this->tooltipRepository->get($options);

        foreach ($tooltips as $tooltip){
            $data = $tooltip;
            $data->systemStepConfig = $tooltip->systemStepConfig;
            $tooltipsData[] = $data;
        }
        return $tooltipsData;
    }

    public function getById(int $tooltipId, array $options = array()){
    	$tooltip = $this->getRequestTooltip($tooltipId);
    	return $tooltip;
    }

    public function create(array $data){
    	$tooltip = $this->tooltipRepository->create($data);
    	$this->dispatcher->fire(new TooltipWasCreated($tooltip));
    	return $tooltip;
    }

    public function update(int $tooltipId, array $data){
        $tooltip = $this->getRequestTooltip($tooltipId);
    	$this->tooltipRepository->update($tooltip, $data);
    	$this->dispatcher->fire(new TooltipWasUpdated($tooltip));
    	return $tooltip;
    }

    public function delete(int $tooltipId){
    	$tooltip = $this->getRequestTooltip($tooltipId);
    	$this->tooltipRepository->delete($tooltipId);
    	$this->dispatcher->fire(new TooltipWasDeleted($tooltip));
    	return $tooltip;
    }

    private function getRequestTooltip(int $tooltipId){
    	$tooltip = $this->tooltipRepository->getById($tooltipId);
    	if (is_null($tooltip)){
    		throw new TooltipNotFoundException();
    	}
    	return $tooltip;
    }

}
