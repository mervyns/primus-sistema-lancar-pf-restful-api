<?php

namespace Api\Tooltips\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TooltipNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('tooltip was not found.');
    }
}
