<?php

namespace Api\Tooltips\Events;

use Infrastructure\Events\Event;
use Api\Tooltips\Models\Tooltips;

class TooltipWasCreated extends Event
{
    public $tooltip;

    public function __construct(Tooltips $tooltip)
    {
        $this->tooltip = $tooltip;
    }
}
