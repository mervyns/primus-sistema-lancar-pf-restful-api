<?php

namespace Api\Tooltips\Models;

use Illuminate\Database\Eloquent\Model;

class Tooltips extends Model
{
	protected $table = 'tooltips';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_config_id',
        'judul',
        'content',
        'link'
    ];

    public function systemStepConfig(){
        return $this->belongsTo('Api\System\Models\SystemStepConfig', 'step_config_id');
    }
}
