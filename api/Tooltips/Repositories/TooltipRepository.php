<?php

namespace Api\Tooltips\Repositories;

use Api\Tooltips\Models\Tooltips;
use Infrastructure\Database\Eloquent\Repository;

class TooltipRepository extends Repository{
	public function getModel(){
		return new Tooltips();
	}

	public function create(array $data){
		$tooltip = $this->getModel();
		$tooltip->fill($data);
		$tooltip->save();
		return $tooltip;
	}

	public function update(Tooltips $tooltip, array $data){
		$tooltip->fill($data);
		$tooltip->save();
		return $tooltip;
	}
}