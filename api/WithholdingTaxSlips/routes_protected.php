<?php

$router->get('/api/withholding-tax-slips', 'WithholdingTaxSlipController@getAll');
$router->get('/api/withholding-tax-slips/{id}', 'WithholdingTaxSlipController@getById');
$router->post('/api/withholding-tax-slips', 'WithholdingTaxSlipController@create');
$router->put('/api/withholding-tax-slips/{id}', 'WithholdingTaxSlipController@update');
$router->delete('/api/withholding-tax-slips/{id}', 'WithholdingTaxSlipController@delete');