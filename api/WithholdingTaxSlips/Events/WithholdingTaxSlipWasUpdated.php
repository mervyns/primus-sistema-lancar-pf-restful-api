<?php

namespace Api\WithholdingTaxSlips\Events;

use Infrastructure\Events\Event;
use Api\WithholdingTaxSlips\Models\WithholdingTaxSlip;

class WithholdingTaxSlipWasUpdated extends Event
{
    public $withholdingTaxSlip;

    public function __construct(WithholdingTaxSlip $withholdingTaxSlip)
    {
        $this->withholdingTaxSlip = $withholdingTaxSlip;
    }
}
