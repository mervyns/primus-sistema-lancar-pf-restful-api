<?php

namespace Api\WithholdingTaxSlips;

use Infrastructure\Events\EventServiceProvider;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasCreated;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasDeleted;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasUpdated;

class WithholdingTaxSlipServiceProvider extends EventServiceProvider
{
    protected $listen = [
        WithholdingTaxSlipWasCreated::class => [
            // listeners for when a withholding tax slip is created
        ],
        WithholdingTaxSlipWasDeleted::class => [
            // listeners for when a withholding tax slip is deleted
        ],
        WithholdingTaxSlipWasUpdated::class => [
            // listeners for when a withholding tax slip is updated
        ],
    ];
}
