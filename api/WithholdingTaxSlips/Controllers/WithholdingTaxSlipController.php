<?php

namespace Api\WithholdingTaxSlips\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\WithholdingTaxSlips\Requests\CreateWithholdingTaxSlipRequest;
use Api\WithholdingTaxSlips\Services\WithholdingTaxSlipService;

class WithholdingTaxSlipController extends Controller
{
    private $withholdingTaxSlipService;

    public function __construct(WithholdingTaxSlipService $withholdingTaxSlipService)
    {
        $this->withholdingTaxSlipService = $withholdingTaxSlipService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->withholdingTaxSlipService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'withholding_tax_slips');

        return $this->response($parsedData);
    }

    public function getById($withholdingTaxSlipId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->withholdingTaxSlipService->getById($withholdingTaxSlipId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'withholding_tax_slip');

        return $this->response($parsedData);
    }

    public function create(CreateWithholdingTaxSlipRequest $request)
    {
        $data = $request->get('withholding_tax_slip', []);

        return $this->response($this->withholdingTaxSlipService->create($data), 201);
    }

    public function update($withholdingTaxSlipId, Request $request)
    {
        $data = $request->get('withholding_tax_slip', []);

        return $this->response($this->withholdingTaxSlipService->update($withholdingTaxSlipId, $data));
    }

    public function delete($withholdingTaxSlipId)
    {
        return $this->response($this->withholdingTaxSlipService->delete($withholdingTaxSlipId));
    }
}
