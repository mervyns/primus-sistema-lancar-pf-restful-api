<?php

namespace Api\WithholdingTaxSlips\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\WithholdingTaxSlips\Exceptions\WithholdingTaxSlipNotFoundException;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasCreated;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasDeleted;
use Api\WithholdingTaxSlips\Events\WithholdingTaxSlipWasUpdated;
use Api\WithholdingTaxSlips\Repositories\WithholdingTaxSlipRepository;

class WithholdingTaxSlipService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $withholdingTaxSlipRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        WithholdingTaxSlipRepository $withholdingTaxSlipRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->withholdingTaxSlipRepository = $withholdingTaxSlipRepository;
    }

    public function getAll($options = [])
    {
        return $this->withholdingTaxSlipRepository->get($options);
    }

    public function getById($withholdingTaxSlipId, array $options = [])
    {
        $withholdingTaxSlip = $this->getRequestedWithholdingTaxSlip($withholdingTaxSlipId);

        return $withholdingTaxSlip;
    }

    public function create($data)
    {
        $withholdingTaxSlip = $this->withholdingTaxSlipRepository->create($data);

        $this->dispatcher->fire(new WithholdingTaxSlipWasCreated($withholdingTaxSlip));

        return $withholdingTaxSlip;
    }

    public function update($withholdingTaxSlipId, array $data)
    {
        $withholdingTaxSlip = $this->getRequestedWithholdingTaxSlip($withholdingTaxSlipId);

        $this->withholdingTaxSlipRepository->update($withholdingTaxSlip, $data);

        $this->dispatcher->fire(new WithholdingTaxSlipWasUpdated($withholdingTaxSlip));

        return $withholdingTaxSlip;
    }

    public function delete($withholdingTaxSlipId)
    {
        $withholdingTaxSlip = $this->getRequestedWithholdingTaxSlip($withholdingTaxSlipId);

        $this->withholdingTaxSlipRepository->delete($withholdingTaxSlipId);

        $this->dispatcher->fire(new WithholdingTaxSlipWasDeleted($withholdingTaxSlip));

        return true;
    }

    private function getRequestedWithholdingTaxSlip($withholdingTaxSlipId)
    {
        $withholdingTaxSlip = $this->withholdingTaxSlipRepository->getById($withholdingTaxSlipId);

        if (is_null($withholdingTaxSlip)) {
            throw new WithholdingTaxSlipNotFoundException();
        }

        return $withholdingTaxSlip;
    }
}
