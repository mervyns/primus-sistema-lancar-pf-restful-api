<?php

namespace Api\WithholdingTaxSlips\Repositories;

use Api\WithholdingTaxSlips\Models\WithholdingTaxSlip;
use Infrastructure\Database\Eloquent\Repository;

class WithholdingTaxSlipRepository extends Repository
{
    public function getModel()
    {
        return new WithholdingTaxSlip();
    }

    public function create(array $data)
    {
        $withholdingTaxSlip = $this->getModel();

        $withholdingTaxSlip->fill($data);
        
        $withholdingTaxSlip->save();

        return $withholdingTaxSlip;
    }

    public function update(WithholdingTaxSlip $withholdingTaxSlip, array $data)
    {

        $withholdingTaxSlip->fill($data);

        $withholdingTaxSlip->save();

        return $withholdingTaxSlip;
    }
}
