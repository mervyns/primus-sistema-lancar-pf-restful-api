<?php

namespace Api\WithholdingTaxSlips\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WithholdingTaxSlipNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The withholding tax slip was not found.');
    }
}
