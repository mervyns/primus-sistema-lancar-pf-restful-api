<?php

namespace Api\WithholdingTaxSlips\Requests;

use Infrastructure\Http\ApiRequest;

class CreateWithholdingTaxSlipRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'withholding_tax_slip' => 'array|required',
            'withholding_tax_slip.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'withholding_tax_slip.name' => 'the withholding tax slip\'s name'
        ];
    }
}
