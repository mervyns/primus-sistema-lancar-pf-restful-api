<?php

namespace Api\JobStatuses\Events;

use Infrastructure\Events\Event;
use Api\JobStatuses\Models\JobStatus;

class JobStatusWasUpdated extends Event
{
    public $jobStatus;

    public function __construct(JobStatus $jobStatus)
    {
        $this->jobStatus = $jobStatus;
    }
}
