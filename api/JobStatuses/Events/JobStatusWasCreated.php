<?php

namespace Api\JobStatuses\Events;

use Infrastructure\Events\Event;
use Api\JobStatuses\Models\JobStatus;

class JobStatusWasCreated extends Event
{
    public $jobStatus;

    public function __construct(JobStatus $jobStatus)
    {
        $this->jobStatus = $jobStatus;
    }
}
