<?php

namespace Api\JobStatuses;

use Infrastructure\Events\EventServiceProvider;
use Api\JobStatuses\Events\JobStatusWasCreated;
use Api\JobStatuses\Events\JobStatusWasDeleted;
use Api\JobStatuses\Events\JobStatusUpdated;

class JobStatusServiceProvider extends EventServiceProvider
{
    protected $listen = [
        JobStatusWasCreated::class => [
            // listeners for when a job status is created
        ],
        JobStatusWasDeleted::class => [
            // listeners for when a job status is deleted
        ],
        JobStatusUpdated::class => [
            // listeners for when a job status is updated
        ],
    ];
}
