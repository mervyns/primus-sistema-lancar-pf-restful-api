<?php

namespace Api\JobStatuses\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class JobStatusNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The job status was not found.');
    }
}
