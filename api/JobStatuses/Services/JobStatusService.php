<?php

namespace Api\JobStatuses\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\JobStatuses\Exceptions\JobStatusNotFoundException;
use Api\JobStatuses\Events\JobStatusWasCreated;
use Api\JobStatuses\Events\JobStatusWasDeleted;
use Api\JobStatuses\Events\JobStatusUpdated;
use Api\JobStatuses\Repositories\JobStatusRepository;

class JobStatusService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $jobStatusRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        JobStatusRepository $jobStatusRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->jobStatusRepository = $jobStatusRepository;
    }

    public function getAll($options = [])
    {
        return $this->jobStatusRepository->get($options);
    }

    public function getById($jobStatusId, array $options = [])
    {
        $jobStatus = $this->getRequestedJobStatus($jobStatusId);

        return $jobStatus;
    }

    public function create($data)
    {
        $jobStatus = $this->jobStatusRepository->create($data);

        $this->dispatcher->fire(new JobStatusWasCreated($jobStatus));

        return $jobStatus;
    }

    public function update($jobStatusId, array $data)
    {
        $jobStatus = $this->getRequestedJobStatus($jobStatusId);

        $this->jobStatusRepository->update($jobStatus, $data);

        $this->dispatcher->fire(new JobStatusUpdated($jobStatus));

        return $jobStatus;
    }

    public function delete($jobStatusId)
    {
        $jobStatus = $this->getRequestedJobStatus($jobStatusId);

        $this->jobStatusRepository->delete($jobStatusId);

        $this->dispatcher->fire(new JobStatusWasDeleted($jobStatus));

        return true;
    }

    private function getRequestedJobStatus($jobStatusId)
    {
        $jobStatus = $this->jobStatusRepository->getById($jobStatusId);

        if (is_null($jobStatus)) {
            throw new JobStatusNotFoundException();
        }

        return $jobStatus;
    }
}
