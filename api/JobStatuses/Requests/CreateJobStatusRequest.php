<?php

namespace Api\JobStatuses\Requests;

use Infrastructure\Http\ApiRequest;

class CreateJobStatusRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'job_status' => 'array|required',
            'job_status.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'job_status.name' => 'the job status\' name'
        ];
    }
}
