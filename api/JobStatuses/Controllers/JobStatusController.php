<?php

namespace Api\JobStatuses\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\JobStatuses\Requests\CreateJobStatusRequest;
use Api\JobStatuses\Services\JobStatusService;

class JobStatusController extends Controller
{
    private $jobStatusService;

    public function __construct(JobStatusService $jobStatusService)
    {
        $this->jobStatusService = $jobStatusService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->jobStatusService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'job_statuses');

        return $this->response($parsedData);
    }

    public function getById($jobStatusId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->jobStatusService->getById($jobStatusId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'job_status');

        return $this->response($parsedData);
    }

    public function create(CreateJobStatusRequest $request)
    {
        $data = $request->get('job_status', []);

        return $this->response($this->jobStatusService->create($data), 201);
    }

    public function update($jobStatusId, Request $request)
    {
        $data = $request->get('job_status', []);

        return $this->response($this->jobStatusService->update($jobStatusId, $data));
    }

    public function delete($jobStatusId)
    {
        return $this->response($this->jobStatusService->delete($jobStatusId));
    }
}
