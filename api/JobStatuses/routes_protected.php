<?php

$router->get('/api/job-statuses', 'JobStatusController@getAll');
$router->get('/api/job-statuses/{id}', 'WithholdingTaxSlipController@getById');
$router->post('/api/job-statuses', 'JobStatusController@create');
$router->put('/api/job-statuses/{id}', 'JobStatusController@update');
$router->delete('/api/job-statuses/{id}', 'JobStatusController@delete');