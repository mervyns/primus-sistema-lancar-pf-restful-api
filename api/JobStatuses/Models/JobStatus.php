<?php

namespace Api\JobStatuses\Models;

use Illuminate\Database\Eloquent\Model;

class JobStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
