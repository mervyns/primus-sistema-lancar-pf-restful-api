<?php

namespace Api\JobStatuses\Repositories;

use Api\JobStatuses\Models\JobStatus;
use Infrastructure\Database\Eloquent\Repository;

class JobStatusRepository extends Repository
{
    public function getModel()
    {
        return new JobStatus();
    }

    public function create(array $data)
    {
        $jobStatus = $this->getModel();

        $jobStatus->fill($data);
        
        $jobStatus->save();

        return $jobStatus;
    }

    public function update(JobStatus $jobStatus, array $data)
    {

        $jobStatus->fill($data);

        $jobStatus->save();

        return $jobStatus;
    }
}
