<?php

namespace Api\Liabilities\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LiabilityNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The liability was not found.');
    }
}
