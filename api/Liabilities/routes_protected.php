<?php

$router->get('/api/liabilities', 'LiabilityController@getAll');
$router->get('/api/liabilities/{id}', 'LiabilityController@getById');
$router->post('/api/liabilities', 'LiabilityController@create');
$router->put('/api/liabilities/{id}', 'LiabilityController@update');
$router->delete('/api/liabilities/{id}', 'LiabilityController@delete');