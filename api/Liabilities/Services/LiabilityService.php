<?php

namespace Api\Liabilities\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\Liabilities\Exceptions\LiabilityNotFoundException;
use Api\Liabilities\Events\LiabilityWasCreated;
use Api\Liabilities\Events\LiabilityWasDeleted;
use Api\Liabilities\Events\LiabilityWasUpdated;
use Api\Liabilities\Repositories\LiabilityRepository;

class LiabilityService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $liabilityRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        LiabilityRepository $liabilityRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->liabilityRepository = $liabilityRepository;
    }

    public function getAll($options = [])
    {
        return $this->liabilityRepository->get($options);
    }

    public function getById($liabilityId, array $options = [])
    {
        $liability = $this->getRequestedLiability($liabilityId);

        return $liability;
    }

    public function create($data)
    {
        $liability = $this->liabilityRepository->create($data);

        $this->dispatcher->fire(new LiabilityWasCreated($liability));

        return $liability;
    }

    public function update($liabilityId, array $data)
    {
        $liability = $this->getRequestedLiability($liabilityId);

        $this->liabilityRepository->update($liability, $data);

        $this->dispatcher->fire(new LiabilityWasUpdated($liability));

        return $liability;
    }

    public function delete($liabilityId)
    {
        $liability = $this->getRequestedLiability($liabilityId);

        $this->liabilityRepository->delete($liabilityId);

        $this->dispatcher->fire(new LiabilityWasDeleted($liability));

        return true;
    }

    private function getRequestedLiability($liabilityId)
    {
        $liability = $this->liabilityRepository->getById($liabilityId);

        if (is_null($liability)) {
            throw new LiabilityNotFoundException();
        }

        return $liability;
    }
}
