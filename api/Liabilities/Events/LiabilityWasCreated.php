<?php

namespace Api\Liabilities\Events;

use Infrastructure\Events\Event;
use Api\Liabilities\Models\Liability;

class LiabilityWasCreated extends Event
{
    public $liability;

    public function __construct(Liability $liability)
    {
        $this->liability = $liability;
    }
}
