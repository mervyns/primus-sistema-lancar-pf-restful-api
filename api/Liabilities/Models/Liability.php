<?php

namespace Api\Liabilities\Models;

use Illuminate\Database\Eloquent\Model;

class Liability extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'liability_code', 'name'
    ];
}
