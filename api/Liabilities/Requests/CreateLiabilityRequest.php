<?php

namespace Api\Liabilities\Requests;

use Infrastructure\Http\ApiRequest;

class CreateLiabilityRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'liability' => 'array|required',
            'liability.liability_code' => 'required|string',
            'liability.name' => 'required|string',
        ];
    }

    
    public function attributes()
    {
        return [
            'liability.name' => 'the liability\'s name'
        ];
    }
}
