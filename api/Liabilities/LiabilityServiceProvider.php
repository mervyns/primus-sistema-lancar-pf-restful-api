<?php

namespace Api\Liabilities;

use Infrastructure\Events\EventServiceProvider;
use Api\Liabilities\Events\LiabilityWasCreated;
use Api\Liabilities\Events\LiabilityWasDeleted;
use Api\Liabilities\Events\LiabilityWasUpdated;

class LiabilityServiceProvider extends EventServiceProvider
{
    protected $listen = [
        LiabilityWasCreated::class => [
            // listeners for when a liability is created
        ],
        LiabilityWasDeleted::class => [
            // listeners for when a liability is deleted
        ],
        LiabilityWasUpdated::class => [
            // listeners for when a liability is updated
        ],
    ];
}
