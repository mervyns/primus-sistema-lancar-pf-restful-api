<?php

namespace Api\Liabilities\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\Liabilities\Requests\CreateLiabilityRequest;
use Api\Liabilities\Services\LiabilityService;

class LiabilityController extends Controller
{
    private $liabilityService;

    public function __construct(LiabilityService $liabilityService)
    {
        $this->liabilityService = $liabilityService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->liabilityService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'liabilities');

        return $this->response($parsedData);
    }

    public function getById($liabilityId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->liabilityService->getById($liabilityId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'liability');

        return $this->response($parsedData);
    }

    public function create(CreateLiabilityRequest $request)
    {
        $data = $request->get('liability', []);

        return $this->response($this->liabilityService->create($data), 201);
    }

    public function update($liabilityId, Request $request)
    {
        $data = $request->get('liability', []);

        return $this->response($this->liabilityService->update($liabilityId, $data));
    }

    public function delete($liabilityId)
    {
        return $this->response($this->liabilityService->delete($liabilityId));
    }
}
