<?php

namespace Api\Liabilities\Repositories;

use Api\Liabilities\Models\Liability;
use Infrastructure\Database\Eloquent\Repository;

class LiabilityRepository extends Repository
{
    public function getModel()
    {
        return new Liability();
    }

    public function create(array $data)
    {
        $liability = $this->getModel();

        $liability->fill($data);
        
        $liability->save();

        return $liability;
    }

    public function update(Liability $liability, array $data)
    {

        $liability->fill($data);

        $liability->save();

        return $liability;
    }
}
