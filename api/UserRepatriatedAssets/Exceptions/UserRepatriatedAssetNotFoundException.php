<?php

namespace Api\UserRepatriatedAssets\Exceptions;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRepatriatedAssetNotFoundException extends NotFoundHttpException
{
    public function __construct()
    {
        parent::__construct('The user\'s repatriated asset was not found.');
    }
}
