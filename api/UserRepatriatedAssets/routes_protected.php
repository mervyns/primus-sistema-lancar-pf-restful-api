<?php

$router->get('/api/user-repatriated-assets', 'UserRepatriatedAssetController@getAll');
$router->get('/api/user-repatriated-assets/{id}', 'UserRepatriatedAssetController@getById');
$router->post('/api/user-repatriated-assets', 'UserRepatriatedAssetController@create');
$router->put('/api/user-repatriated-assets/{id}', 'UserRepatriatedAssetController@update');
$router->delete('/api/user-repatriated-assets/{id}', 'UserRepatriatedAssetController@delete');