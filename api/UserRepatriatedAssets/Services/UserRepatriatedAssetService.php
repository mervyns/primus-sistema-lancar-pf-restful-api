<?php

namespace Api\UserRepatriatedAssets\Services;

use Exception;
//use Illuminate\Auth\AuthManager;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Api\UserRepatriatedAssets\Exceptions\UserRepatriatedAssetNotFoundException;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasCreated;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasDeleted;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasUpdated;
use Api\UserRepatriatedAssets\Repositories\UserRepatriatedAssetRepository;
use Api\UserRevealedAssets\Repositories\UserRevealedAssetRepository;
use Api\Users\Repositories\UserRepository;
use Api\Assets\Repositories\AssetRepository;
use Api\Countries\Repositories\CountryRepository;
use Api\Investments\Repositories\InvestmentRepository;
use Api\Gateways\Repositories\GatewayRepository;

class UserRepatriatedAssetService
{
    //private $auth;

    private $database;

    private $dispatcher;

    private $userRepatriatedAssetRepository;

    public function __construct(
        DatabaseManager $database,
        Dispatcher $dispatcher,
        UserRepatriatedAssetRepository $userRepatriatedAssetRepository
    ) {
        $this->database = $database;
        $this->dispatcher = $dispatcher;
        $this->userRepatriatedAssetRepository = $userRepatriatedAssetRepository;
    }

    public function getAll($options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();
        $userRevealedAssetRepository = new UserRevealedAssetRepository();
        $investmentRepository = new InvestmentRepository();
        $gatewayRepository = new GatewayRepository();

        $userRepatriatedAsset = $this->userRepatriatedAssetRepository->get($options);

        $i=0;
        foreach ($userRepatriatedAsset as $uRA) {
            $userRevealedAsset = $userRevealedAssetRepository->getById($uRA['user_revealed_asset_id']);
            $user = $userRepository->getById($userRevealedAsset['user_id']);
            $asset = $assetRepository->getById($userRevealedAsset['asset_id']);
            $country = $countryRepository->getById($userRevealedAsset['country_id']);
            $investment = $investmentRepository->getById($uRA['investment_id']);
            $gateway = $gatewayRepository->getById($uRA['gateway_id']);

            $userRepatriatedAsset[$i]['user_name'] = $user['name'];
            $userRepatriatedAsset[$i]['user_revealed_asset_name'] = $asset['name'];
            $userRepatriatedAsset[$i]['user_revealed_asset_country'] = $country['name'];
            $userRepatriatedAsset[$i]['investment_name'] = $investment['name'];
            $userRepatriatedAsset[$i]['gateway_name'] = $gateway['name'];

            $i++;   
        }

        return $userRepatriatedAsset;
    }

    public function getById($userRepatriatedAssetId, array $options = [])
    {
        $userRepository = new UserRepository();
        $assetRepository = new AssetRepository();
        $countryRepository = new CountryRepository();
        $userRevealedAssetRepository = new UserRevealedAssetRepository();
        $investmentRepository = new InvestmentRepository();
        $gatewayRepository = new GatewayRepository();

        $userRepatriatedAsset = $this->getRequestedUserRepatriatedAsset($userRepatriatedAssetId);

        $userRevealedAsset = $userRevealedAssetRepository->getById($userRepatriatedAsset['user_revealed_asset_id']);
        $user = $userRepository->getById($userRevealedAsset['user_id']);
        $asset = $assetRepository->getById($userRevealedAsset['asset_id']);
        $country = $countryRepository->getById($userRevealedAsset['country_id']);
        $investment = $investmentRepository->getById($userRepatriatedAsset['investment_id']);
        $gateway = $gatewayRepository->getById($userRepatriatedAsset['gateway_id']);

        $userRepatriatedAsset['user_name'] = $user['name'];
        $userRepatriatedAsset['user_revealed_asset_name'] = $asset['name'];
        $userRepatriatedAsset['user_revealed_asset_country'] = $country['name'];
        $userRepatriatedAsset['investment_name'] = $investment['name'];
        $userRepatriatedAsset['gateway_name'] = $gateway['name'];

        return $userRepatriatedAsset;
    }

    public function create($data)
    {
        $userRepatriatedAsset = $this->userRepatriatedAssetRepository->create($data);

        $this->dispatcher->fire(new UserRepatriatedAssetWasCreated($userRepatriatedAsset));

        return $userRepatriatedAsset;
    }

    public function update($userRepatriatedAssetId, array $data)
    {
        $userRepatriatedAsset = $this->getRequestedUserRepatriatedAsset($userRepatriatedAssetId);

        $this->userRepatriatedAssetRepository->update($userRepatriatedAsset, $data);

        $this->dispatcher->fire(new UserRepatriatedAssetWasUpdated($userRepatriatedAsset));

        return $userRepatriatedAsset;
    }

    public function delete($userRepatriatedAssetId)
    {
        $userRepatriatedAsset = $this->getRequestedUserRepatriatedAsset($userRepatriatedAssetId);

        $this->userRepatriatedAssetRepository->delete($userRepatriatedAssetId);

        $this->dispatcher->fire(new UserRepatriatedAssetWasDeleted($userRepatriatedAsset));

        return true;
    }

    private function getRequestedUserRepatriatedAsset($userRepatriatedAssetId)
    {
        $userRepatriatedAsset = $this->userRepatriatedAssetRepository->getById($userRepatriatedAssetId);

        if (is_null($userRepatriatedAsset)) {
            throw new UserRepatriatedAssetNotFoundException();
        }

        return $userRepatriatedAsset;
    }
}
