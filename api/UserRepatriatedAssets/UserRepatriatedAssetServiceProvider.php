<?php

namespace Api\UserRepatriatedAssets;

use Infrastructure\Events\EventServiceProvider;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasCreated;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasDeleted;
use Api\UserRepatriatedAssets\Events\UserRepatriatedAssetWasUpdated;

class UserRepatriatedAssetServiceProvider extends EventServiceProvider
{
    protected $listen = [
        UserRepatriatedAssetWasCreated::class => [
            // listeners for when a user repatriated asset is created
        ],
        UserRepatriatedAssetWasDeleted::class => [
            // listeners for when a user repatriated asset is deleted
        ],
        UserRepatriatedAssetWasUpdated::class => [
            // listeners for when a user repatriated asset is updated
        ],
    ];
}
