<?php

namespace Api\UserRepatriatedAssets\Repositories;

use Api\UserRepatriatedAssets\Models\UserRepatriatedAsset;
use Infrastructure\Database\Eloquent\Repository;

class UserRepatriatedAssetRepository extends Repository
{
    public function getModel()
    {
        return new UserRepatriatedAsset();
    }

    public function create(array $data)
    {
        $userRepatriatedAsset = $this->getModel();

        $userRepatriatedAsset->fill($data);
        
        $userRepatriatedAsset->save();

        return $userRepatriatedAsset;
    }

    public function update(UserRepatriatedAsset $userRepatriatedAsset, array $data)
    {

        $userRepatriatedAsset->fill($data);

        $userRepatriatedAsset->save();

        return $userRepatriatedAsset;
    }
}
