<?php

namespace Api\UserRepatriatedAssets\Controllers;

use Illuminate\Http\Request;
use Infrastructure\Http\Controller;
use Api\UserRepatriatedAssets\Requests\CreateUserRepatriatedAssetRequest;
use Api\UserRepatriatedAssets\Services\UserRepatriatedAssetService;

class UserRepatriatedAssetController extends Controller
{
    private $userRepatriatedAssetService;

    public function __construct(UserRepatriatedAssetService $userRepatriatedAssetService)
    {
        $this->userRepatriatedAssetService = $userRepatriatedAssetService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userRepatriatedAssetService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_repatriated_assets');

        return $this->response($parsedData);
    }

    public function getById($userRepatriatedAssetId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userRepatriatedAssetService->getById($userRepatriatedAssetId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user_repatriated_asset');

        return $this->response($parsedData);
    }

    public function create(CreateUserRepatriatedAssetRequest $request)
    {
        $data = $request->get('user_repatriated_asset', []);

        return $this->response($this->userRepatriatedAssetService->create($data), 201);
    }

    public function update($userRepatriatedAssetId, Request $request)
    {
        $data = $request->get('user_repatriated_asset', []);

        return $this->response($this->userRepatriatedAssetService->update($userRepatriatedAssetId, $data));
    }

    public function delete($userRepatriatedAssetId)
    {
        return $this->response($this->userRepatriatedAssetService->delete($userRepatriatedAssetId));
    }
}
