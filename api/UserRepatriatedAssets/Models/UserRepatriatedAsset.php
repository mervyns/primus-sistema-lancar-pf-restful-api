<?php

namespace Api\UserRepatriatedAssets\Models;

use Illuminate\Database\Eloquent\Model;

class UserRepatriatedAsset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_revealed_asset_id', 'investment_id', 'gateway_id', 'will_repatriated_value', 'has_repatriated_value', 'investment_date'
    ];
}
