<?php

namespace Api\UserRepatriatedAssets\Events;

use Infrastructure\Events\Event;
use Api\UserRepatriatedAssets\Models\UserRepatriatedAsset;

class UserRepatriatedAssetWasUpdated extends Event
{
    public $userRepatriatedAsset;

    public function __construct(UserRepatriatedAsset $userRepatriatedAsset)
    {
        $this->userRepatriatedAsset = $userRepatriatedAsset;
    }
}
