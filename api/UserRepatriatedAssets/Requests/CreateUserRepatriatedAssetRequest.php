<?php

namespace Api\UserRepatriatedAssets\Requests;

use Infrastructure\Http\ApiRequest;

class CreateUserRepatriatedAssetRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_repatriated_asset' => 'array|required',
            'user_repatriated_asset.user_revealed_asset_id' => 'required|integer',
            'user_repatriated_asset.investment_id' => 'required|integer',
            'user_repatriated_asset.gateway_id' => 'required|integer',
            'user_repatriated_asset.will_repatriated_value' => 'between:0,9999999999999.99',
            'user_repatriated_asset.has_repatriated_value' => 'between:0,9999999999999.99',
            'user_repatriated_asset.investment_date' => 'required|date_format:Y-m-d'
        ];
    }

    
    public function attributes()
    {
        return [
            'user_repatriated_asset.value' => 'the user repatriated asset\'s value'
        ];
    }
}
